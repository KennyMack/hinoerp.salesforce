﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Configuration;

namespace Carga.console
{
    class Program
    {
        static void Main(string[] args)
        {
            var connectionERP = ConfigurationManager.ConnectionStrings["ERP"].ConnectionString;
            var connectionSALESFORCE = ConfigurationManager.ConnectionStrings["SALESFORCE"].ConnectionString;

            OracleConnection orclERP = new OracleConnection(connectionERP);
            OracleConnection orclCRM = new OracleConnection(connectionSALESFORCE);

            System.Console.WriteLine("Abrindo conexões");
            orclERP.Open();
            orclCRM.Open();
            /*
            var sqlType = @"SELECT SUBSTR(GEPRODUCTSTYPE.DESCRIPTION, 0, 3) TYPEERP, GEPRODUCTSTYPE.* 
                              FROM GEPRODUCTSTYPE
                             WHERE GEPRODUCTSTYPE.ESTABLISHMENTKEY = 'f48c8bd0-a048-4ae4-a988-75b791fdd80b'";

            var dtType = new DataTable();

            OracleCommand cmd = new OracleCommand(sqlType, orclCRM);
            dtType.Load(cmd.ExecuteReader());

            var sqlFamily = @"SELECT SUBSTR(GEPRODUCTSFAMILY.DESCRIPTION, 0, 4) FAMILY, GEPRODUCTSFAMILY.* 
                                FROM GEPRODUCTSFAMILY
                               WHERE ESTABLISHMENTKEY = 'f48c8bd0-a048-4ae4-a988-75b791fdd80b'";

            var dtsqlFamily = new DataTable();

            cmd = new OracleCommand(sqlFamily, orclCRM);
            dtsqlFamily.Load(cmd.ExecuteReader());


            var sqlAplic = @"SELECT * 
                                 FROM GEPRODUCTAPLIC
                                WHERE ESTABLISHMENTKEY = 'f48c8bd0-a048-4ae4-a988-75b791fdd80b'";

            var dtAplic = new DataTable();

            cmd = new OracleCommand(sqlAplic, orclCRM);
            dtAplic.Load(cmd.ExecuteReader());


            var sqlUnit = @"SELECT * 
                              FROM GEPRODUCTSUNIT
                             WHERE ESTABLISHMENTKEY = 'f48c8bd0-a048-4ae4-a988-75b791fdd80b'";

            var dtUnit = new DataTable();

            cmd = new OracleCommand(sqlUnit, orclCRM);
            dtUnit.Load(cmd.ExecuteReader());


            var sqlProduto = @"SELECT IDAPI, UNIQUEKEY, CODPRODUTO, IMAGE, 
                                   NAME, DESCRIPTION, PERCIPI, LIMITEDESCONTO, 
                                   VALORUNITARIO, SALDO, STATUS, NCM, CODUNIDADE, 
                                   PESOBRUTO, QTDEPOREMB, UMVENDA, FATORUMVENDA, 
                                   FAMILY, TYPEPROD, SALEUNITID, UNITID, 
                                   NCMID, APLICATIONID, CODORIGMERC, CODESTAB, 
                                   DATAMODIFICACAO, STATUSSINC
                              FROM GEVWSINCPRODUTO
                             WHERE GEVWSINCPRODUTO.CODESTAB = 1
AND CODPRODUTO in ('33.387.000',
'PR-ES-294',
'Q-EL-557',
'Q-EL-479',
'PR-ES-297',
'Q-EL-538',
'PR-ES-310')";

            using (var cmdERP = new OracleCommand(sqlProduto, orclERP))
            using (var orclReader = cmdERP.ExecuteReader())
            {
                while(orclReader.Read())
                {
                    long Id;
                    var uniqueKey = Guid.NewGuid().ToString();

                    var sqlId = @"SELECT SEQ_GEPRODUCTS.NEXTVAL FROM DUAL";

                    using (var cmdId = new OracleCommand(sqlId, orclCRM))
                        Id = Convert.ToInt64(cmdId.ExecuteScalar());


                    var sqlInsert = @"INSERT INTO GEPRODUCTS
                                            (ID, ESTABLISHMENTKEY, UNIQUEKEY, PRODUCTKEY, 
                                            IMAGE, NAME, DESCRIPTION, TYPEID, FAMILYID, 
                                            PERCIPI, PERCMAXDISCOUNT, VALUE, STOCKBALANCE, 
                                            STATUS, CREATED, MODIFIED, ISACTIVE, NCM, 
                                            NCMID, UNITID, WEIGHT, PACKAGEQTD, SALEUNITID, 
                                            SALEFACTOR, APLICATIONID, CODORIGMERC)
                                            VALUES
                                            (:pID, :pESTABLISHMENTKEY, :pUNIQUEKEY, :pPRODUCTKEY, 
                                            :pIMAGE, :pNAME, :pDESCRIPTION, :pTYPEID, :pFAMILYID, 
                                            :pPERCIPI, :pPERCMAXDISCOUNT, :pVALUE, :pSTOCKBALANCE, 
                                            :pSTATUS, :pCREATED, :pMODIFIED, :pISACTIVE, :pNCM, 
                                            :pNCMID, :pUNITID, :pWEIGHT, :pPACKAGEQTD, :pSALEUNITID, 
                                            :pSALEFACTOR, :pAPLICATIONID, :pCODORIGMERC)";
                    using (var cmdInsert = new OracleCommand(sqlInsert, orclCRM))
                    {

                        var rowsType = dtType.Select($"[TYPEERP] = '{orclReader["TYPEPROD"]}'", "TYPEERP").First();
                        var rowsFamily = dtsqlFamily.Select($"[FAMILY] = '{orclReader["FAMILY"]}'", "FAMILY").First();
                        var rowsAplic = dtAplic.Select($"[IDERP] = '{orclReader["APLICATIONID"]}'", "IDERP").First();
                        var rowsUnit = dtUnit.Select($"[UNIT] = '{orclReader["UNITID"]}'", "UNIT").First();

                        cmdInsert.Parameters.Add("pID", OracleDbType.Int64).Value = Id;
                        cmdInsert.Parameters.Add("pESTABLISHMENTKEY", OracleDbType.Varchar2).Value = "f48c8bd0-a048-4ae4-a988-75b791fdd80b";
                        cmdInsert.Parameters.Add("pUNIQUEKEY", OracleDbType.Varchar2).Value = uniqueKey;
                        cmdInsert.Parameters.Add("pPRODUCTKEY", OracleDbType.Varchar2).Value = orclReader["CODPRODUTO"].ToString();
                        cmdInsert.Parameters.Add("pIMAGE", OracleDbType.Varchar2).Value = orclReader["IMAGE"].ToString();
                        cmdInsert.Parameters.Add("pNAME", OracleDbType.Varchar2).Value = orclReader["NAME"].ToString();
                        cmdInsert.Parameters.Add("pDESCRIPTION", OracleDbType.Varchar2).Value = orclReader["DESCRIPTION"].ToString();
                        cmdInsert.Parameters.Add("pTYPEID", OracleDbType.Int64).Value = Convert.ToInt64(rowsType["ID"]);
                        cmdInsert.Parameters.Add("pFAMILYID", OracleDbType.Int64).Value = Convert.ToInt64(rowsFamily["ID"]);
                        cmdInsert.Parameters.Add("pPERCIPI", OracleDbType.Decimal).Value = Convert.ToDecimal(orclReader["PERCIPI"]);
                        cmdInsert.Parameters.Add("pPERCMAXDISCOUNT", OracleDbType.Decimal).Value = 0;
                        cmdInsert.Parameters.Add("pVALUE", OracleDbType.Decimal).Value = 0;
                        cmdInsert.Parameters.Add("pSTOCKBALANCE", OracleDbType.Decimal).Value = 0;
                        cmdInsert.Parameters.Add("pSTATUS", OracleDbType.Int32).Value = orclReader["STATUS"].ToString();
                        cmdInsert.Parameters.Add("pCREATED", OracleDbType.Date).Value = DateTime.Now;
                        cmdInsert.Parameters.Add("pMODIFIED", OracleDbType.Date).Value = DateTime.Now;
                        cmdInsert.Parameters.Add("pISACTIVE", OracleDbType.Int32).Value = 1;
                        cmdInsert.Parameters.Add("pNCM", OracleDbType.Varchar2).Value = orclReader["NCM"].ToString();
                        cmdInsert.Parameters.Add("pNCMID", OracleDbType.Int64).Value = 1;
                        cmdInsert.Parameters.Add("pUNITID", OracleDbType.Int64).Value = Convert.ToInt64(rowsUnit["ID"]);
                        cmdInsert.Parameters.Add("pWEIGHT", OracleDbType.Decimal).Value = Convert.ToDecimal(orclReader["PESOBRUTO"]);
                        cmdInsert.Parameters.Add("pPACKAGEQTD", OracleDbType.Decimal).Value = Convert.ToDecimal(orclReader["QTDEPOREMB"]);
                        cmdInsert.Parameters.Add("pSALEUNITID", OracleDbType.Int64).Value = null;
                        cmdInsert.Parameters.Add("pSALEFACTOR", OracleDbType.Decimal).Value = Convert.ToDecimal(orclReader["FATORUMVENDA"]);
                        cmdInsert.Parameters.Add("pAPLICATIONID", OracleDbType.Int64).Value = Convert.ToInt64(rowsAplic["ID"]);
                        cmdInsert.Parameters.Add("pCODORIGMERC", OracleDbType.Int64).Value = Convert.ToInt64(orclReader["CODORIGMERC"]);


                        cmdInsert.ExecuteNonQuery();
                    }


                    var sqlUpdate = @"UPDATE FSPRODUTOPARAMESTAB
                                        SET FSPRODUTOPARAMESTAB.IDAPI = :pIDAPI,
                                            FSPRODUTOPARAMESTAB.UNIQUEKEY = :pUNIQUEKEY
                                        WHERE FSPRODUTOPARAMESTAB.CODPRODUTO = :pCODPRODUTO
                                        AND FSPRODUTOPARAMESTAB.CODESTAB  = 1";
                    using (var cmdUpdate = new OracleCommand(sqlUpdate, orclERP))
                    {
                        cmdUpdate.Parameters.Add("pIDAPI", OracleDbType.Int64).Value = Id;
                        cmdUpdate.Parameters.Add("pUNIQUEKEY", OracleDbType.Varchar2).Value = uniqueKey;
                        cmdUpdate.Parameters.Add("pCODPRODUTO", OracleDbType.Varchar2).Value = orclReader["CODPRODUTO"].ToString();

                        cmdUpdate.ExecuteNonQuery();
                    }

                    var sqlUpdatePcp = @"UPDATE FSPRODUTOPCP
                                        SET FSPRODUTOPCP.IDAPI = :pIDAPI,
                                            FSPRODUTOPCP.UNIQUEKEY = :pUNIQUEKEY
                                      WHERE FSPRODUTOPCP.CODPRODUTO = :pCODPRODUTO
                                        AND FSPRODUTOPCP.CODESTAB  = 1";
                    using (var cmdUpdate = new OracleCommand(sqlUpdatePcp, orclERP))
                    {
                        cmdUpdate.Parameters.Add("pIDAPI", OracleDbType.Int64).Value = Id;
                        cmdUpdate.Parameters.Add("pUNIQUEKEY", OracleDbType.Varchar2).Value = uniqueKey;
                        cmdUpdate.Parameters.Add("pCODPRODUTO", OracleDbType.Varchar2).Value = orclReader["CODPRODUTO"].ToString();

                        cmdUpdate.ExecuteNonQuery();
                    }
                }
            }*/


            var sqlEmpresas = @"SELECT GEENTERPRISES.UNIQUEKEY, GEENTERPRISES.ID,
       GEENTERPRISES.IDERP 
  FROM GEENTERPRISES
WHERE ESTABLISHMENTKEY = 'f48c8bd0-a048-4ae4-a988-75b791fdd80b'";

            using (var cmdCRM = new OracleCommand(sqlEmpresas, orclCRM))
            using (var orclReader = cmdCRM.ExecuteReader())
            {
                while (orclReader.Read())
                {
                    var sqlUpdateEmp = @"UPDATE GEEMPRESAPARAMESTAB
                                        SET GEEMPRESAPARAMESTAB.IDAPI = :pIDAPI,
                                            GEEMPRESAPARAMESTAB.UNIQUEKEY = :pUNIQUEKEY
                                      WHERE GEEMPRESAPARAMESTAB.CODEMPRESA = :pCODEMPRESA
                                        AND GEEMPRESAPARAMESTAB.CODESTAB  = 1";
                    using (var cmdUpdate = new OracleCommand(sqlUpdateEmp, orclERP))
                    {
                        cmdUpdate.Parameters.Add("pIDAPI", OracleDbType.Int64).Value = Convert.ToInt64(orclReader["ID"]);
                        cmdUpdate.Parameters.Add("pUNIQUEKEY", OracleDbType.Varchar2).Value = Convert.ToString(orclReader["UNIQUEKEY"]);
                        cmdUpdate.Parameters.Add("pCODEMPRESA", OracleDbType.Int64).Value = Convert.ToInt64(orclReader["IDERP"]);

                        cmdUpdate.ExecuteNonQuery();
                    }
                }
            }
        }
    }
}
