﻿using Hino.Salesforce.API.Handlers;
using System.Web.Http;
using System.Web.Http.ExceptionHandling;

namespace Hino.Salesforce.API
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {

            // Web API configuration and services
            config.Services.Replace(typeof(IExceptionLogger), new UnhandledExceptionHandler());

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApiSalesforce",
                routeTemplate: "api/{controller}/{action}/{pEstablishmentKey}"
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApiAdminSalesforce",
                routeTemplate: "api/admin/{controller}/{action}"
            );
        }
    }
}
