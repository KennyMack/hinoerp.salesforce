﻿using Hino.Salesforce.API.HttpUtils;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Cache.Auth;
using Hino.Salesforce.Infra.Cross.Identity.Configurations;
using Hino.Salesforce.Infra.Cross.Identity.Token;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace Hino.Salesforce.API.Attributes
{
    public class AuthJWTAttribute : Attribute, IAuthenticationFilter
    {
        public bool AllowMultiple => false;
        public string Realm { get; set; }
        public ERoles _role;

        public AuthJWTAttribute(ERoles role)
        {
            _role = role;
        }

        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            if (!context.ActionContext.ActionDescriptor.GetCustomAttributes<IgnoreAuthJWTAttribute>().Any())
            {
                var request = context.Request;
                var authorization = request.Headers.Authorization;

                if (authorization == null || authorization.Scheme != "Bearer")
                {
                    context.ErrorResult = new HttpActionErrorResult(System.Net.HttpStatusCode.Unauthorized,
                        JsonConvert.SerializeObject(new
                        {
                            ErrorCode = (int)EExceptionErrorCodes.UnauthorizedRequest,
                            Field = "Token",
                            Value = "Token",
                            Messages = new[] { "Token não informado." }
                        },
                        new JsonSerializerSettings
                        {
                            Formatting = Formatting.Indented,
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        }));
                    return Task.FromResult<object>(null);
                }

                if (string.IsNullOrEmpty(authorization.Parameter))
                {
                    context.ErrorResult = new HttpActionErrorResult(System.Net.HttpStatusCode.Unauthorized,
                        JsonConvert.SerializeObject(new
                        {
                            ErrorCode = (int)EExceptionErrorCodes.UnauthorizedRequest,
                            Field = "Token",
                            Value = "Token",
                            Messages = new[] { "Token não informado." }
                        },
                        new JsonSerializerSettings
                        {
                            Formatting = Formatting.Indented,
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        }));
                    return Task.FromResult<object>(null);
                }

                var token = authorization.Parameter;
                var principal = AuthenticateJwtToken(context, token);

                if (principal == null)
                    context.ErrorResult = new HttpActionErrorResult(System.Net.HttpStatusCode.Unauthorized,
                        JsonConvert.SerializeObject(new
                        {
                            ErrorCode = (int)EExceptionErrorCodes.UnauthorizedRequest,
                            Field = "Token",
                            Value = "Token",
                            Messages = new[] { "Token inválido." }
                        },
                        new JsonSerializerSettings
                        {
                            Formatting = Formatting.Indented,
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        }));
                else
                    context.Principal = principal;

            }
            return Task.FromResult<object>(null);
        }

        protected IPrincipal AuthenticateJwtToken(HttpAuthenticationContext context, string token)
        {
            var result = ValidateToken(context, token);

            string sessionId = result.SessionId;

            if (result.Valid)
            {
                // based on username to get more information from database in order to build local identity
                var claims = new List<Claim>
                {
                    new Claim(ClaimTypes.Name, sessionId)
                    // Add more claims if needed: Roles, ...
                };

                var identity = new ClaimsIdentity(claims, "Jwt");
                IPrincipal user = new ClaimsPrincipal(identity);
                try
                {
                    context.Request.Properties.Add(new KeyValuePair<string, object>("SESSIONIDKEY", user));
                }
                catch (Exception)
                {

                }

                return user;
            }

            return null;
        }

        private ValidateJWTTokenResult ValidateToken(HttpAuthenticationContext context, string token)
        {
            var sessionId = "";


            IIdentity simplePrinciple = null;
            try
            {
                simplePrinciple = context.Principal.Identity;
            }
            catch (Exception)
            {
                return new ValidateJWTTokenResult
                {
                    Valid = false,
                    SessionId = "",
                    Message = Infra.Cross.Resources.ErrorMessagesResource.SessionInvalidOrExpired
                };
            }

            if (!(simplePrinciple is ClaimsIdentity identity))
                return new ValidateJWTTokenResult
                {
                    Valid = false,
                    SessionId = "",
                    Message = Infra.Cross.Resources.ErrorMessagesResource.SessionInvalidOrExpired
                };

            if (!identity.IsAuthenticated)
                return new ValidateJWTTokenResult
                {
                    Valid = false,
                    SessionId = "",
                    Message = Infra.Cross.Resources.ErrorMessagesResource.UserNotAuthenticated
                };

            var sessionIdClaim = identity.FindFirst(TokenClaim.SessionId);
            sessionId = sessionIdClaim?.Value;

            if (string.IsNullOrEmpty(sessionId))
                return new ValidateJWTTokenResult
                {
                    Valid = false,
                    SessionId = "",
                    Message = Infra.Cross.Resources.ErrorMessagesResource.SessionInvalidOrExpired
                };

            var SessionId = sessionId;

            var SessionTokenCache = (SessionTokenCache)context.Request.GetDependencyScope().GetService(typeof(SessionTokenCache));

            GEUsersVM geUsers = null;
            try
            {
                geUsers = JsonConvert.DeserializeObject<GEUsersVM>(SessionTokenCache.Get<string>(SessionId));

                if ((int)geUsers.UserType < (int)_role)
                {
                    return new ValidateJWTTokenResult
                    {
                        Valid = false,
                        SessionId = "",
                        Message = Infra.Cross.Resources.ErrorMessagesResource.PermissionRequired
                    };
                }
            }
            catch (Exception ex)
            {
                Logging.Info("Problema ao buscar os dados do usuario no Redis");
                Logging.Exception(ex);
            }

            var TokenConfigurations = (TokenConfigurations)Startup.DependencyResolver.GetService(typeof(TokenConfigurations));
            TimeSpan finalExpiration = TimeSpan.FromSeconds(TokenConfigurations.FinalExpiration);

            if (geUsers != null)
            {
                SessionTokenCache.SetTime(sessionId, finalExpiration);
                try
                {
                    context.Request.Properties.Add(new KeyValuePair<string, object>("USER", geUsers));
                }
                catch (Exception ex)
                {
                    Logging.Info("Problema ao setar o USER na requisição");
                    Logging.Exception(ex);

                }
            }
            else
                return new ValidateJWTTokenResult
                {
                    Valid = false,
                    SessionId = "",
                    Message = Infra.Cross.Resources.ErrorMessagesResource.SessionInvalidOrExpired
                };

            return new ValidateJWTTokenResult
            {
                SessionId = sessionId,
                Valid = true,
                Message = "OK"
            };
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            Challenge(context);
            return Task.FromResult(0);
        }

        private void Challenge(HttpAuthenticationChallengeContext context)
        {
            string parameter = null;

            if (!string.IsNullOrEmpty(Realm))
                parameter = "realm=\"" + Realm + "\"";


            context.ChallengeWith("Bearer", parameter);
        }
    }
    class ValidateJWTTokenResult
    {
        public bool Valid { get; set; }
        public string SessionId { get; set; }
        public string Message { get; set; }
    }

    [AttributeUsage(AttributeTargets.Method, Inherited = true)]
    public class IgnoreAuthJWTAttribute : Attribute
    {

    }
}