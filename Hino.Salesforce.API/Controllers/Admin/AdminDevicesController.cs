﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Admin
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/Admin/Devices")]
    public class AdminDevicesController : BaseController
    {
        private readonly IGEEstabDevicesAS _IGEEstabDevicesAS;
        private readonly IGEEstablishmentsAS _IGEEstablishmentsAS;

        public AdminDevicesController(IGEEstabDevicesAS pIGEEstabDevicesAS,
            IGEEstablishmentsAS pIGEEstablishmentsAS)
        {
            _IGEEstabDevicesAS = pIGEEstabDevicesAS;
            _IGEEstablishmentsAS = pIGEEstablishmentsAS;
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var Results =
                Mapper.Map<PagedResult<GEEstabDevices>, PagedResult<GEEstabDevicesVM>>
                (
                await _IGEEstabDevicesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEEstabDevicesAS.Errors);

            return RequestOK(Results);
        }

        [Route("establishment/{pEstablishmentKey}/key/{pUniqueKey}/id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetByEstablishmentId(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            var Results =
                Mapper.Map<IEnumerable<GEEstabDevices>, IEnumerable<GEEstabDevicesVM>>
                (
                await _IGEEstablishmentsAS.GetDevicesEstablishment(pEstablishmentKey, pUniqueKey, pId));

            if (Results == null)
                return InvalidRequest(null, _IGEEstabDevicesAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEEstabDevices>, PagedResult<GEEstabDevicesVM>>
                (
                await _IGEEstabDevicesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEEstabDevicesAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEEstabDevicesVM pGEEstabDevicesVM)
        {
            _IGEEstabDevicesAS.Errors.Clear();
            pGEEstabDevicesVM.Id = id;
            pGEEstabDevicesVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEEstabDevicesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEstabDevicesVM, ModelState);

            var Result = _IGEEstabDevicesAS.Update(Mapper.Map<GEEstabDevicesVM, GEEstabDevices>(pGEEstabDevicesVM));

            await _IGEEstabDevicesAS.SaveChanges();

            if (_IGEEstabDevicesAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEEstabDevicesAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEEstabDevicesVM pGEEstabDevicesVM)
        {
            _IGEEstabDevicesAS.Errors.Clear();
            ValidateModelState(pGEEstabDevicesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEstabDevicesVM, ModelState);

            await _IGEEstabDevicesAS.CreateDeviceAsync(pGEEstabDevicesVM);

            if (_IGEEstabDevicesAS.Errors.Count > 0)
                return InvalidRequest(pGEEstabDevicesVM, _IGEEstabDevicesAS.Errors);

            return RequestOK(pGEEstabDevicesVM);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEEstabDevicesAS.Errors.Clear();
            var estabDevice = Mapper.Map<GEEstabDevices, GEEstabDevicesVM>(
                await _IGEEstabDevicesAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            if (_IGEEstabDevicesAS.Errors.Count > 0)
                return InvalidRequest(null, _IGEEstabDevicesAS.Errors);

            return RequestOK(estabDevice);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEstabDevicesAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
