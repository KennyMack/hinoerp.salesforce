﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.Email;
using Hino.Salesforce.Application.Interfaces.Email.General;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Admin
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/Admin/Establishments")]
    public class AdminEstablishmentsController : BaseController
    {
        private readonly IGEEstablishmentsAS _IGEEstablishmentsAS;
        private readonly ISendEmailAS _ISendEmailAS;
        private readonly IEstablishmentEmailAS _IEstablishmentEmailAS;
        public AdminEstablishmentsController(IGEEstablishmentsAS pIGEEstablishmentsAS,
            ISendEmailAS pISendEmailAS,
            IEstablishmentEmailAS pIEstablishmentEmailAS)
        {
            _IGEEstablishmentsAS = pIGEEstablishmentsAS;
            _ISendEmailAS = pISendEmailAS;
            _IEstablishmentEmailAS = pIEstablishmentEmailAS;
        }

        [Route("all")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Get()
        {
            var Results =
                Mapper.Map<PagedResult<GEEstablishments>, PagedResult<GEEstablishmentsVM>>(
                    await _IGEEstablishmentsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                        r => !string.IsNullOrEmpty(r.EstablishmentKey),
                        s => s.GEEstabDevices));

            if (Results == null)
                return InvalidRequest(null, _IGEEstablishmentsAS.Errors);

            return RequestOK(Results);
        }

        [Route("{pEstablishmentKey}/key/{pUniqueKey}/id/{pId}")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            var Results =
                Mapper.Map<GEEstablishments, GEEstablishmentsVM>(
                    await _IGEEstablishmentsAS.GetByIdAsync(
                        pId,
                        pEstablishmentKey,
                        pUniqueKey));

            if (Results == null)
                return InvalidRequest(null, _IGEEstablishmentsAS.Errors);

            return RequestOK(Results);
        }

        [HttpPost]
        [Route("save")]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Post([FromBody] GEEstablishmentsCreateVM pGEEstablishmentsCreateVM)
        {
            _IGEEstablishmentsAS.Errors.Clear();
            ValidateModelState(pGEEstablishmentsCreateVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEstablishmentsCreateVM, ModelState);

            var Estab = await _IGEEstablishmentsAS.CreateEstablishmentAsync(pGEEstablishmentsCreateVM);

            if (_IGEEstablishmentsAS.Errors.Count > 0)
                return InvalidRequest(pGEEstablishmentsCreateVM, _IGEEstablishmentsAS.Errors);
            else
                await _IEstablishmentEmailAS.SendCreate(Estab, "hinosistemas@gmail.com", Estab.Email);

            return RequestOK(Mapper.Map<GEEstablishments, GEEstablishmentsVM>(Estab));
        }

        [HttpPut]
        [Route("id/{pId}/save")]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Put(long pId, [FromBody] GEEstablishmentsCreateVM pGEEstablishmentsCreateVM)
        {
            _IGEEstablishmentsAS.Errors.Clear();
            ValidateModelState(pGEEstablishmentsCreateVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEstablishmentsCreateVM, ModelState);

            var Estab = await _IGEEstablishmentsAS.UpdateEstablishmentAsync(pGEEstablishmentsCreateVM);

            if (_IGEEstablishmentsAS.Errors.Count > 0)
                return InvalidRequest(pGEEstablishmentsCreateVM, _IGEEstablishmentsAS.Errors);

            return RequestOK(Mapper.Map<GEEstablishments, GEEstablishmentsVM>(Estab));
        }

        [HttpDelete]
        [Route("{pEstablishmentKey}/key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEEstablishmentsAS.Errors.Clear();

            var Estab = Mapper.Map<GEEstablishments, GEEstablishmentsVM>(
                await _IGEEstablishmentsAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            if (_IGEEstablishmentsAS.Errors.Count > 0)
                return InvalidRequest(null, _IGEEstablishmentsAS.Errors);

            return RequestOK(Estab);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEstablishmentsAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}