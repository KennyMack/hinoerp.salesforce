﻿using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Utils.Settings;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Admin
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/Admin/Users")]
    public class AdminUsersController : BaseController
    {
        private readonly IGEUsersAS _IGEUsersAS;
        public AdminUsersController(IGEUsersAS pIGEUsersAS)
        {
            _IGEUsersAS = pIGEUsersAS;
        }

        [HttpPost]
        [Route("save")]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Post([FromBody] GEUsersVM pGEUsersVM)
        {
            _IGEUsersAS.Errors.Clear();
            ValidateModelState(pGEUsersVM);


            if (!ModelState.IsValid)
                return InvalidRequest(pGEUsersVM, ModelState);

            var HinoId = AppSettings.Hino_Id;
            // ConfigurationManager.AppSettings["Hino_Id"].ToString();

            await _IGEUsersAS.CreateAdminUserAsync(HinoId, pGEUsersVM);

            if (_IGEUsersAS.Errors.Count > 0)
                return InvalidRequest(pGEUsersVM, _IGEUsersAS.Errors);

            return RequestOK(pGEUsersVM);
        }

        [HttpPost]
        [Route("change-password")]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> PostPassword([FromBody] GEUsersPasswordVM pGEUsersVM)
        {
            _IGEUsersAS.Errors.Clear();
            ValidateModelState(pGEUsersVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEUsersVM, ModelState);

            pGEUsersVM = await _IGEUsersAS.ChangePassword(pGEUsersVM);

            if (_IGEUsersAS.Errors.Count > 0)
                return InvalidRequest(pGEUsersVM, _IGEUsersAS.Errors);

            return RequestOK(pGEUsersVM);
        }
    }
}
