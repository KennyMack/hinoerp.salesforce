﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.Auth;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Cache.Auth;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Identity.Configurations;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Auth
{
    [RoutePrefix("api/Auth/Mordor")]
    public class MordorController : BaseController
    {
        private readonly IGEUsersAS _IGEUsersAS;
        private readonly IGEEstablishmentsAS _IGEEstablishmentsAS;
        private readonly IAUExpiredTokenAS _IAUExpiredTokenAS;
        private readonly IAURefreshTokenAS _AURefreshTokenAS;
        private readonly SessionTokenCache _SessionTokenCache;
        private readonly TokenConfigurations _TokenConfigurations;

        public MordorController(IGEUsersAS pIGEUsersAS,
            IAURefreshTokenAS pAURefreshTokenAS,
            IAUExpiredTokenAS pIAUExpiredTokenAS,
            IGEEstablishmentsAS pIGEEstablishmentsAS,
            SigningConfigurations pSigningConfigurations,
            TokenConfigurations pTokenConfigurations,
            SessionTokenCache pSessionTokenCache)
        {
            _TokenConfigurations = pTokenConfigurations;
            _IGEEstablishmentsAS = pIGEEstablishmentsAS;
            _IGEUsersAS = pIGEUsersAS;
            _AURefreshTokenAS = pAURefreshTokenAS;
            _SessionTokenCache = pSessionTokenCache;
            _IAUExpiredTokenAS = pIAUExpiredTokenAS;
        }

        /*[HttpPost]
        [Route("{pEstablishmentKey}/login")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Login([FromBody]LoginVM model)
        {
            var token = await _IGEUsersAS.ValidateAuthenticationAsync(model, false);

            if (_IGEUsersAS.Errors.Count > 0)
                return InvalidRequest(model, _IGEUsersAS.Errors);

            return RequestOK(token);
        }*/

        [HttpDelete]
        [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
        [Route("logout")]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Logout()
        {
            try
            {
                var identity = this.ActionContext.RequestContext.Principal.Identity;
                if (identity != null &&
                    identity is ClaimsIdentity identityLogin &&
                    identityLogin.IsAuthenticated)
                {
                    Request.Properties.TryGetValue("USER", out object user);
                    var SessionId = identityLogin.Name;
                    var GeUser = Mapper.Map<GEUsers>(user);

                    _IAUExpiredTokenAS.Add(new Infra.Cross.Entities.Auth.AUExpiredToken
                    {
                        EstablishmentKey = GeUser.EstablishmentKey,
                        TokenValue = SessionId
                    });

                    var refresh = (await _AURefreshTokenAS.QueryAsync(r => r.SessionID == SessionId)).First();

                    if (refresh != null)
                        _AURefreshTokenAS.Remove(refresh);

                    _SessionTokenCache.Remove(SessionId);

                    await _AURefreshTokenAS.SaveChanges();
                    await _IAUExpiredTokenAS.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                Logging.Exception(ex);
            }

            return RequestOK(Infra.Cross.Resources.ValidationMessagesResource.LoggedOutSuccess);
        }

        [HttpGet]
        [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
        [Route("me")]
        [IgnoreValidateEstablishmentKey]
        public IHttpActionResult Me()
        {
            try
            {
                if (!Request.Properties.TryGetValue("USER", out object user))
                    return UnauthorizedRequest();

                return RequestOK(user);
            }
            catch (Exception e)
            {
                return InvalidRequest("", string.Concat(e.Message, ", ", e.StackTrace));
            }
        }

        [HttpGet]
        [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
        [Route("me/auth")]
        [IgnoreValidateEstablishmentKey]
        public IHttpActionResult MeAuth()
        {
            try
            {

                if (!Request.Properties.TryGetValue("USER", out object user))
                    return UnauthorizedRequest();

                GEUsersVM UserDB = (GEUsersVM)user;

                if (UserDB.GEEstablishments != null)
                {
                    var Establishment = _IGEEstablishmentsAS.GetById(
                        UserDB.GEEstablishments.Id,
                        UserDB.EstablishmentKey,
                        UserDB.GEEstablishments.UniqueKey, 
                        s => s.FSFiscalOper,
                        pf => pf.PfFiscalGroup,
                        pj => pj.PjFiscalGroup,
                        pr => pr.DefaultProduct);
                    UserDB.GEEstablishments = Mapper.Map<GEEstablishmentsVM>(Establishment);
                }

                var identity = this.ActionContext.RequestContext.Principal.Identity;
                if (identity != null &&
                    identity is ClaimsIdentity identityLogin &&
                    identityLogin.IsAuthenticated)
                {
                    var SessionId = identityLogin.Name;
                    TimeSpan finalExpiration = TimeSpan.FromSeconds(_TokenConfigurations.FinalExpiration);
                    _SessionTokenCache.Set(SessionId,
                    JsonConvert.SerializeObject(UserDB), finalExpiration);
                }

                return RequestOK(UserDB);
            }
            catch (Exception e)
            {
                return InvalidRequest("", string.Concat(e.Message, ", ", e.StackTrace));
            }
        }

    }
}
