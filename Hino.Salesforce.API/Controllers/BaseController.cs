﻿using Hino.Salesforce.API.HttpUtils;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Hino.Salesforce.API.Controllers
{
    public class BaseController : ApiController
    {
        private Regex _RegDate = new Regex(@"^(\d{2})(\d{2})(\d{4})(\d{2})(\d{2})(\d{2})$", RegexOptions.Compiled);

        protected JsonSerializerSettings _JsonSerializerSettings = new JsonSerializerSettings
        {
            Culture = new System.Globalization.CultureInfo("pt-BR"),
            Formatting = Formatting.Indented,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };

        private JsonResult GetContent(object obj, object error, HttpStatusCode responseCode)
        {
            var result = new JsonResult();
            result.Data = new
            {
                status = responseCode,
                success = IsSuccessReponse(responseCode),
                data = obj ?? new JObject(),
                error = error ?? new JArray()
            };
            result.JsonRequestBehavior = JsonRequestBehavior.AllowGet;
            return result;
        }

        protected IHttpActionResult RequestOK(object obj, HttpStatusCode responseCode = HttpStatusCode.OK) =>
            new HttpActionResult(responseCode,
                JsonConvert.SerializeObject(GetContent(obj, null, responseCode),
                _JsonSerializerSettings));

        protected IHttpActionResult NotFoundRequest() =>
            new HttpActionResult(HttpStatusCode.NotFound,
                JsonConvert.SerializeObject(GetContent(null, "Recurso não encontrado", HttpStatusCode.NotFound),
                _JsonSerializerSettings));

        protected IHttpActionResult InvalidRequest(object obj, object error)
        {
            if (error.GetType().Name == "ModelStateDictionary")
            {
                var errors = new List<ModelException>();
                ((System.Web.Http.ModelBinding.ModelStateDictionary)error).ToList()
                    .ForEach(r =>
                    {
                        if (r.Value.Errors.FirstOrDefault() != null)
                        {
                            var MErr = new ModelException
                            {
                                ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                                Field = r.Key,//.Contains(".") ? r.Key.Split('.')[1] : r.Key,
                                Messages = new[] { r.Value.Errors.FirstOrDefault()?.ErrorMessage },
                                Value = r.Value?.Value?.ToString()
                            };

                            errors.Add(MErr);
                        }
                    });

                return new HttpActionResult(HttpStatusCode.BadRequest,
                    JsonConvert.SerializeObject(GetContent(obj, errors, HttpStatusCode.BadRequest),
                    _JsonSerializerSettings));
            }

            return new HttpActionResult(HttpStatusCode.BadRequest,
                JsonConvert.SerializeObject(GetContent(obj, error, HttpStatusCode.BadRequest),
                _JsonSerializerSettings));

        }

        protected IHttpActionResult UnauthorizedRequest() =>
            new HttpActionResult(HttpStatusCode.Unauthorized,
                JsonConvert.SerializeObject(GetContent(null, "Acesso negado", HttpStatusCode.Unauthorized),
                _JsonSerializerSettings));

        private bool IsSuccessReponse(HttpStatusCode code)
        {
            if (IsInformational(code))
                return true;
            else if (IsSuccess(code))
                return true;
            else if (IsRedirect(code))
                return true;
            else if (IsClientError(code))
                return false;
            else if (IsServerError(code))
                return false;

            return false;

        }

        private bool IsInformational(HttpStatusCode code)
        {
            return (int)code >= 100 && (int)code <= 199;
        }

        private bool IsRedirect(HttpStatusCode code)
        {
            return (int)code >= 300 && (int)code <= 399;
        }

        private bool IsSuccess(HttpStatusCode code)
        {
            return (int)code >= 200 && (int)code <= 299;
        }

        private bool IsClientError(HttpStatusCode code)
        {
            return (int)code >= 400 && (int)code <= 499;
        }

        private bool IsServerError(HttpStatusCode code)
        {
            return (int)code >= 500 && (int)code <= 599;
        }

        protected int GetPageNumber()
        {
            var page = -1;

            try
            {
                var query = HttpContext.Current.Request.QueryString;
                var value = query.Get("page");
                if (!int.TryParse(value, out page))
                    page = -1;
            }
            catch (Exception)
            {
                page = -1;
            }

            return page;
        }

        protected int GetLimitNumber()
        {
            var limit = 10;

            try
            {
                var query = HttpContext.Current.Request.QueryString;
                var value = query.Get("limit");
                if (!int.TryParse(value, out limit))
                    limit = 10;
            }
            catch (Exception)
            {
                limit = 10;
            }

            return limit;
        }

        protected string GetQueryFilter()
        {
            string filter;

            try
            {
                var query = HttpContext.Current.Request.QueryString;
                filter = query.Get("filter").ToUpper();
            }
            catch (Exception)
            {
                filter = "";
            }
            return filter;
        }

        protected string GetSearchPosition()
        {
            string filter;

            try
            {
                var query = HttpContext.Current.Request.QueryString;
                filter = query.Get("pos").ToUpper();
            }
            catch (Exception)
            {
                filter = "0";
            }
            return filter;
        }

        protected string GetQueryColumn()
        {
            string column;

            try
            {
                var query = HttpContext.Current.Request.QueryString;
                column = query.Get("column");
            }
            catch (Exception)
            {
                column = "";
            }
            return column;
        }

        #region Validate Model State
        protected void ValidateModelState<TEntity>(TEntity pModel) where TEntity : class
        {
            ModelState.Clear();
            Validate(pModel);
        }
        #endregion

        protected DateTime? ConvertDateUrl(string pDateStr)
        {
            try
            {
                var Matches = _RegDate.Match(pDateStr);
                var Date = new DateTime(
                    Convert.ToInt32(Matches.Groups[3].Value),
                    Convert.ToInt32(Matches.Groups[2].Value),
                    Convert.ToInt32(Matches.Groups[1].Value),
                    Convert.ToInt32(Matches.Groups[4].Value),
                    Convert.ToInt32(Matches.Groups[5].Value),
                    Convert.ToInt32(Matches.Groups[6].Value));

                return Date;
            }
            catch (Exception)
            {

            }
            return null;
        }
    }
}