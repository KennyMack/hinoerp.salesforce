﻿using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.Interfaces.General.Demograph;
using Hino.Salesforce.Application.Interfaces.Route;
using Hino.Salesforce.Application.ViewModels;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers
{
    [RoutePrefix("api/Cep")]
    public class CepController : BaseController
    {
        private readonly ICEPAS _ICEPAS;
        private readonly IMapBox _IMapBox;
        private readonly IHereMaps _IHereMaps;
        private readonly IGECountriesAS _IGECountriesAS;

        public CepController(
            ICEPAS pICEPAS,
            IMapBox pIMapBox,
            IHereMaps pIHereMaps,
            IGECountriesAS pIGECountriesAS)
        {
            _ICEPAS = pICEPAS;
            _IMapBox = pIMapBox;
            _IHereMaps = pIHereMaps;
            _IGECountriesAS = pIGECountriesAS;
        }

        [Route("{pCep}")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Get(string pCep)
        {
            var cepvm = new CEPVM();
            try
            {
                var number = "0";
                try
                {
                    var query = HttpContext.Current.Request.QueryString;
                    number = query.Get("number") ?? "0";
                }
                catch (Exception)
                {
                    number = "0";
                }

                var allInfo = "none";
                try
                {
                    var query = HttpContext.Current.Request.QueryString;
                    allInfo = query.Get("allinfo") ?? "none";
                }
                catch (Exception)
                {
                    allInfo = "none";
                }

                var country = "BR";
                try
                {
                    var query = HttpContext.Current.Request.QueryString;
                    country = (query.Get("country") ?? "BR").ToUpper();
                }
                catch (Exception)
                {
                    country = "BR";
                }

                cepvm = await _ICEPAS.GetCEPAddressAsync(pCep, number, country, allInfo != "none");

                if (_ICEPAS.Errors.Any())
                    return InvalidRequest(null, _ICEPAS.Errors);

                return RequestOK(cepvm);
            }
            catch (Exception)
            {
                return InvalidRequest(cepvm, "CEP Inválido.");
            }

        }
    }
}
