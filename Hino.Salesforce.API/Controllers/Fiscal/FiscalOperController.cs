﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.Fiscal;
using Hino.Salesforce.Application.ViewModels.Fiscal;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Fiscal
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/Fiscal/Operation/{pEstablishmentKey}")]
    public class FiscalOperController : BaseController
    {
        private readonly IFSFiscalOperAS _IFSFiscalOperAS;
        public FiscalOperController(IFSFiscalOperAS pIFSFiscalOperAS)
        {
            _IFSFiscalOperAS = pIFSFiscalOperAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<FSFiscalOper>, PagedResult<FSFiscalOperVM>>
                (
                await _IFSFiscalOperAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IFSFiscalOperAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] FSFiscalOperVM[] syncDataVM)
        {
            _IFSFiscalOperAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);
            _IFSFiscalOperAS.DontSendToQueue = true;
            var Result = await _IFSFiscalOperAS.SyncData(pEstablishmentKey, Mapper.Map<FSFiscalOperVM[], FSFiscalOper[]>(syncDataVM));
            _IFSFiscalOperAS.DontSendToQueue = false;
            if (_IFSFiscalOperAS.Errors.Count > 0)
                return InvalidRequest(Result, _IFSFiscalOperAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<FSFiscalOper>, PagedResult<FSFiscalOperVM>>
                (
                await _IFSFiscalOperAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                FSFiscalOperVM.GetDefaultFilter(pEstablishmentKey, filter)));

            if (Results == null)
                return InvalidRequest(null, _IFSFiscalOperAS.Errors);

            return RequestOK(Results);
        }


        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<FSFiscalOper>, PagedResult<FSFiscalOperVM>>
                (
                await _IFSFiscalOperAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IFSFiscalOperAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, FSFiscalOperVM pFSFiscalOperVM)
        {
            _IFSFiscalOperAS.Errors.Clear();
            pFSFiscalOperVM.Id = id;
            pFSFiscalOperVM.UniqueKey = pUniqueKey;

            ValidateModelState(pFSFiscalOperVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pFSFiscalOperVM, ModelState);

            var Result = _IFSFiscalOperAS.Update(Mapper.Map<FSFiscalOperVM, FSFiscalOper>(pFSFiscalOperVM));

            await _IFSFiscalOperAS.SaveChanges();

            if (_IFSFiscalOperAS.Errors.Count > 0)
                return InvalidRequest(Result, _IFSFiscalOperAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] FSFiscalOperVM pFSFiscalOperVM)
        {
            _IFSFiscalOperAS.Errors.Clear();
            ValidateModelState(pFSFiscalOperVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pFSFiscalOperVM, ModelState);

            var Result = _IFSFiscalOperAS.Add(Mapper.Map<FSFiscalOperVM, FSFiscalOper>(pFSFiscalOperVM));

            await _IFSFiscalOperAS.SaveChanges();

            if (_IFSFiscalOperAS.Errors.Count > 0)
                return InvalidRequest(Result, _IFSFiscalOperAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IFSFiscalOperAS.Errors.Clear();
            var RegionSale = Mapper.Map<FSFiscalOper, FSFiscalOperVM>(
                await _IFSFiscalOperAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            await _IFSFiscalOperAS.SaveChanges();

            if (_IFSFiscalOperAS.Errors.Count > 0)
                return InvalidRequest(null, _IFSFiscalOperAS.Errors);

            return RequestOK(RegionSale);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IFSFiscalOperAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
