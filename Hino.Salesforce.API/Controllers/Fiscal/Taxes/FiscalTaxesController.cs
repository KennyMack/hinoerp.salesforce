﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.Fiscal.Taxes;
using Hino.Salesforce.Application.ViewModels.Fiscal.Taxes;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Fiscal.Taxes
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/Fiscal/Taxes/{pEstablishmentKey}")]
    public class FiscalTaxesController : BaseController
    {
        private readonly IFSICMSSTAliqUFAS _IFSICMSSTAliqUFAS;
        public FiscalTaxesController(IFSICMSSTAliqUFAS pIFSICMSSTAliqUFAS)
        {
            _IFSICMSSTAliqUFAS = pIFSICMSSTAliqUFAS;
        }
        [Route("sync")]
        [HttpPost]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] FSICMSSTAliqUFVM[] syncDataVM)
        {
            _IFSICMSSTAliqUFAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);

            _IFSICMSSTAliqUFAS.DontSendToQueue = true;
            var Result = await _IFSICMSSTAliqUFAS.SyncData(pEstablishmentKey, Mapper.Map<FSICMSSTAliqUFVM[], FSICMSSTAliqUF[]>(syncDataVM));
            _IFSICMSSTAliqUFAS.DontSendToQueue = false;

            if (_IFSICMSSTAliqUFAS.Errors.Count > 0)
                return InvalidRequest(Result, _IFSICMSSTAliqUFAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<FSICMSSTAliqUF>, PagedResult<FSICMSSTAliqUFVM>>
                (
                await _IFSICMSSTAliqUFAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    FSICMSSTAliqUFVM.GetDefaultFilter(pEstablishmentKey, filter)));

            if (Results == null)
                return InvalidRequest(null, _IFSICMSSTAliqUFAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<FSICMSSTAliqUF>, PagedResult<FSICMSSTAliqUFVM>>
                (
                await _IFSICMSSTAliqUFAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IFSICMSSTAliqUFAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, FSICMSSTAliqUFVM pFSICMSSTAliqUFVM)
        {
            _IFSICMSSTAliqUFAS.Errors.Clear();
            pFSICMSSTAliqUFVM.Id = id;
            pFSICMSSTAliqUFVM.UniqueKey = pUniqueKey;

            ValidateModelState(pFSICMSSTAliqUFVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pFSICMSSTAliqUFVM, ModelState);

            var Result = _IFSICMSSTAliqUFAS.Update(Mapper.Map<FSICMSSTAliqUFVM, FSICMSSTAliqUF>(pFSICMSSTAliqUFVM));

            await _IFSICMSSTAliqUFAS.SaveChanges();

            if (_IFSICMSSTAliqUFAS.Errors.Count > 0)
                return InvalidRequest(Result, _IFSICMSSTAliqUFAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public IHttpActionResult Post(string pEstablishmentKey, [FromBody] FSICMSSTAliqUFVM pFSICMSSTAliqUFVM)
        {
            _IFSICMSSTAliqUFAS.Add(Mapper.Map<FSICMSSTAliqUFVM, FSICMSSTAliqUF>(pFSICMSSTAliqUFVM));

            return InvalidRequest(null, _IFSICMSSTAliqUFAS.Errors);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IFSICMSSTAliqUFAS.Errors.Clear();
            var Result = await _IFSICMSSTAliqUFAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

            if (Result != null && !_IFSICMSSTAliqUFAS.Errors.Any())
                RequestOK(Mapper.Map<FSICMSSTAliqUF, FSICMSSTAliqUFVM>(Result));

            return RequestOK(Result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IFSICMSSTAliqUFAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
