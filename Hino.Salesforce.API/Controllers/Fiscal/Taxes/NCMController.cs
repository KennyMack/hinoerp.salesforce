﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.Fiscal.Taxes;
using Hino.Salesforce.Application.ViewModels.Fiscal.Taxes;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using Hino.Salesforce.Infra.Cross.Utils.Settings;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Fiscal.Taxes
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/Fiscal/NCM/{pEstablishmentKey}")]
    public class NCMController : BaseController
    {
        private readonly IFSNCMAS _IFSNCMAS;
        public NCMController(IFSNCMAS pIFSNCMAS)
        {
            _IFSNCMAS = pIFSNCMAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<FSNCM>, PagedResult<FSNCMVM>>
                (
                await _IFSNCMAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IFSNCMAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        [IgnoreValidateEstablishmentKey]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] FSNCMVM[] syncDataVM)
        {
            _IFSNCMAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);
            _IFSNCMAS.DontSendToQueue = true;
            var Result = await _IFSNCMAS.SyncData(pEstablishmentKey, Mapper.Map<FSNCMVM[], FSNCM[]>(syncDataVM));
            _IFSNCMAS.DontSendToQueue = false;
            if (_IFSNCMAS.Errors.Count > 0)
                return InvalidRequest(Result, _IFSNCMAS.Errors);

            return RequestOK(Result);
        }

        [Route("load")]
        [HttpPost]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> PostLoadStates(string pEstablishmentKey, [FromBody] FSNCMLoadVM[] loadNCMs)
        {
            _IFSNCMAS.Errors.Clear();

            await _IFSNCMAS.LoadNCMsAsync(loadNCMs);

            if (_IFSNCMAS.Errors.Count > 0)
                return InvalidRequest(null, _IFSNCMAS.Errors);

            return RequestOK(new { Success = true });
        }

        [Route("all")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {

            var Results =
                Mapper.Map<PagedResult<FSNCM>, PagedResult<FSNCMVM>>
                (
                await _IFSNCMAS.GetAllPagedAsync(GetPageNumber(), GetLimitNumber()));

            if (Results == null)
                return InvalidRequest(null, _IFSNCMAS.Errors);

            return RequestOK(Results);
        }

        [Route("search")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> GetSearch(string pEstablishmentKey)
        {
            var EstablishmentKey = AppSettings.Hino_Id;

            var filter = GetQueryFilter();
            var Results =
                Mapper.Map<PagedResult<FSNCM>, PagedResult<FSSearchNCM>>
                (
                    await _IFSNCMAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                        FSNCMVM.GetDefaultFilter(EstablishmentKey, filter)
                    )
                );

            if (Results == null)
                return InvalidRequest(null, _IFSNCMAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<FSNCM>, PagedResult<FSNCMVM>>
                (
                await _IFSNCMAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId));

            if (Results == null)
                return InvalidRequest(null, _IFSNCMAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, FSNCMVM pFSNCMVM)
        {
            _IFSNCMAS.Errors.Clear();
            pFSNCMVM.Id = id;
            pFSNCMVM.UniqueKey = pUniqueKey;

            ValidateModelState(pFSNCMVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pFSNCMVM, ModelState);

            var Result = _IFSNCMAS.Update(Mapper.Map<FSNCMVM, FSNCM>(pFSNCMVM));

            await _IFSNCMAS.SaveChanges();

            if (_IFSNCMAS.Errors.Count > 0)
                return InvalidRequest(Result, _IFSNCMAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] FSNCMVM pFSNCMVM)
        {
            _IFSNCMAS.Errors.Clear();
            ValidateModelState(pFSNCMVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pFSNCMVM, ModelState);

            var Result = _IFSNCMAS.Add(Mapper.Map<FSNCMVM, FSNCM>(pFSNCMVM));

            await _IFSNCMAS.SaveChanges();

            if (_IFSNCMAS.Errors.Count > 0)
                return InvalidRequest(Result, _IFSNCMAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IFSNCMAS.Errors.Clear();
            var Result = await _IFSNCMAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

            if (Result != null && !_IFSNCMAS.Errors.Any())
                RequestOK(Mapper.Map<FSNCM, FSNCMVM>(Result));

            return RequestOK(Result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IFSNCMAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}