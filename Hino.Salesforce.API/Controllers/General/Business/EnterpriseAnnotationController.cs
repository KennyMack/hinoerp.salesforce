﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General.Business
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Business/Enterprises/Annotations/{pEstablishmentKey}")]
    public class EnterpriseAnnotationController : BaseController
    {
        private readonly IGEEnterpriseAnnotationAS _IGEEnterpriseAnnotationAS;
        public EnterpriseAnnotationController(IGEEnterpriseAnnotationAS pIGEEnterpriseAnnotationAS)
        {
            _IGEEnterpriseAnnotationAS = pIGEEnterpriseAnnotationAS;
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEEnterpriseAnnot>, PagedResult<GEEnterpriseAnnotationVM>>
                (
                await _IGEEnterpriseAnnotationAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterpriseAnnotationAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [Route("Enterprise/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetByEnterpriseId(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEEnterpriseAnnot>, PagedResult<GEEnterpriseAnnotationVM>>
                (
                await _IGEEnterpriseAnnotationAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EnterpriseId == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterpriseAnnotationAS.Errors);

            return RequestOK(Results);
        }
    }
}