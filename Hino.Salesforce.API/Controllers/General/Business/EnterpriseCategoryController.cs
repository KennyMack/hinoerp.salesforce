﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General.Business
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Business/Enterprises/Category/{pEstablishmentKey}")]
    public class EnterpriseCategoryController : BaseController
    {
        private readonly IGEEnterpriseCategoryAS _IGEEnterpriseCategoryAS;
        public EnterpriseCategoryController(IGEEnterpriseCategoryAS pIGEEnterpriseCategoryAS)
        {
            _IGEEnterpriseCategoryAS = pIGEEnterpriseCategoryAS;
        }

        [Route("sync")]
        [HttpPost]
        public async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] GEEnterpriseCategoryVM[] syncDataVM)
        {
            _IGEEnterpriseCategoryAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);

            _IGEEnterpriseCategoryAS.DontSendToQueue = true;
            var Result = await _IGEEnterpriseCategoryAS.SyncData(pEstablishmentKey, Mapper.Map<GEEnterpriseCategoryVM[], GEEnterpriseCategory[]>(syncDataVM));
            _IGEEnterpriseCategoryAS.DontSendToQueue = false;

            if (_IGEEnterpriseCategoryAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEEnterpriseCategoryAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();

            /*
             r => r.EstablishmentKey == pEstablishmentKey &&
                   (
                     (
                        (r.Id.ToString().Contains(filter)) ||
                        (r.Description.ToUpper().Contains(filter)) ||
                        (r.Identifier.ToUpper().Contains(filter))
                     ) ||
                     (filter == "")
                   )
             */

            var Results =
                Mapper.Map<PagedResult<GEEnterpriseCategory>, PagedResult<GEEnterpriseCategoryVM>>
                (
                await _IGEEnterpriseCategoryAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    GEEnterpriseCategoryVM.GetDefaultFilter(pEstablishmentKey, filter)
                ));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterpriseCategoryAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEEnterpriseCategory>, PagedResult<GEEnterpriseCategoryVM>>
                (
                await _IGEEnterpriseCategoryAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterpriseCategoryAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEEnterpriseCategoryVM pGEEnterpriseCategoryVM)
        {
            _IGEEnterpriseCategoryAS.Errors.Clear();
            pGEEnterpriseCategoryVM.Id = id;
            pGEEnterpriseCategoryVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEEnterpriseCategoryVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterpriseCategoryVM, ModelState);

            var Enterprise = _IGEEnterpriseCategoryAS.Update(Mapper.Map<GEEnterpriseCategoryVM, GEEnterpriseCategory>(pGEEnterpriseCategoryVM));

            if (_IGEEnterpriseCategoryAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEEnterpriseCategoryAS.Errors);
            else
                await _IGEEnterpriseCategoryAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEEnterpriseCategoryVM pGEEnterpriseCategoryVM)
        {
            _IGEEnterpriseCategoryAS.Errors.Clear();
            ValidateModelState(pGEEnterpriseCategoryVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterpriseCategoryVM, ModelState);

            var Enterprise = _IGEEnterpriseCategoryAS.Add(Mapper.Map<GEEnterpriseCategoryVM, GEEnterpriseCategory>(pGEEnterpriseCategoryVM));

            if (_IGEEnterpriseCategoryAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEEnterpriseCategoryAS.Errors);
            else
                await _IGEEnterpriseCategoryAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEEnterpriseCategoryAS.Errors.Clear();
            var Result = await _IGEEnterpriseCategoryAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

            if (Result != null && !_IGEEnterpriseCategoryAS.Errors.Any())
            {
                await _IGEEnterpriseCategoryAS.SaveChanges();
                return RequestOK(Mapper.Map<GEEnterpriseCategory, GEEnterpriseCategoryVM>(Result));
            }

            return InvalidRequest(Result, _IGEEnterpriseCategoryAS.Errors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEnterpriseCategoryAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
