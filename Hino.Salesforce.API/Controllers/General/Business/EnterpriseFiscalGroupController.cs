﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General.Business
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Business/Enterprises/Fiscal/Group/{pEstablishmentKey}")]
    public class EnterpriseFiscalGroupController : BaseController
    {
        private readonly IGEEnterpriseFiscalGroupAS _IGEEnterpriseFiscalGroupAS;
        public EnterpriseFiscalGroupController(IGEEnterpriseFiscalGroupAS pIGEEnterpriseFiscalGroupAS)
        {
            _IGEEnterpriseFiscalGroupAS = pIGEEnterpriseFiscalGroupAS;
        }

        [Route("sync")]
        [HttpPost]
        public async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] GEEnterpriseFiscalGroupVM[] syncDataVM)
        {
            _IGEEnterpriseFiscalGroupAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);

            _IGEEnterpriseFiscalGroupAS.DontSendToQueue = true;
            var Result = await _IGEEnterpriseFiscalGroupAS.SyncData(pEstablishmentKey, Mapper.Map<GEEnterpriseFiscalGroupVM[], GEEnterpriseFiscalGroup[]>(syncDataVM));
            _IGEEnterpriseFiscalGroupAS.DontSendToQueue = false;

            if (_IGEEnterpriseFiscalGroupAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEEnterpriseFiscalGroupAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();

            /*
             r => r.EstablishmentKey == pEstablishmentKey &&
                   (
                     (
                        (r.Id.ToString().Contains(filter)) ||
                        (r.Description.ToUpper().Contains(filter)) ||
                        (r.Type.ToUpper().Contains(filter))
                     ) ||
                     (filter == "")
                   )
             */

            var Results =
                Mapper.Map<PagedResult<GEEnterpriseFiscalGroup>, PagedResult<GEEnterpriseFiscalGroupVM>>
                (
                await _IGEEnterpriseFiscalGroupAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    GEEnterpriseFiscalGroupVM.GetDefaultFilter(pEstablishmentKey, filter)));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterpriseFiscalGroupAS.Errors);

            return RequestOK(Results);
        }

        [Route("client")]
        [HttpGet]
        public async Task<IHttpActionResult> GetClients(string pEstablishmentKey)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<GEEnterpriseFiscalGroup>, PagedResult<GEEnterpriseFiscalGroupVM>>
                (
                await _IGEEnterpriseFiscalGroupAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                  (r.Type == "C" ||
                  r.Type == "X")));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterpriseFiscalGroupAS.Errors);

            return RequestOK(Results);
        }

        [Route("prospect")]
        [HttpGet]
        public async Task<IHttpActionResult> GetProspects(string pEstablishmentKey)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<GEEnterpriseFiscalGroup>, PagedResult<GEEnterpriseFiscalGroupVM>>
                (
                await _IGEEnterpriseFiscalGroupAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                  (r.Type == "P" ||
                  r.Type == "X")));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterpriseFiscalGroupAS.Errors);

            return RequestOK(Results);
        }

        [Route("carrier")]
        [HttpGet]
        public async Task<IHttpActionResult> GetCarriers(string pEstablishmentKey)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<GEEnterpriseFiscalGroup>, PagedResult<GEEnterpriseFiscalGroupVM>>
                (
                await _IGEEnterpriseFiscalGroupAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                  (r.Type == "T" ||
                  r.Type == "X")));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterpriseFiscalGroupAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEEnterpriseFiscalGroup>, PagedResult<GEEnterpriseFiscalGroupVM>>
                (
                await _IGEEnterpriseFiscalGroupAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterpriseFiscalGroupAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEEnterpriseFiscalGroupVM pGEEnterpriseGeoVM)
        {
            _IGEEnterpriseFiscalGroupAS.Errors.Clear();
            pGEEnterpriseGeoVM.Id = id;
            pGEEnterpriseGeoVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEEnterpriseGeoVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterpriseGeoVM, ModelState);

            var Enterprise = _IGEEnterpriseFiscalGroupAS.Update(Mapper.Map<GEEnterpriseFiscalGroupVM, GEEnterpriseFiscalGroup>(pGEEnterpriseGeoVM));

            if (_IGEEnterpriseFiscalGroupAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEEnterpriseFiscalGroupAS.Errors);
            else
                await _IGEEnterpriseFiscalGroupAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEEnterpriseFiscalGroupVM pGEEnterpriseGeoVM)
        {
            _IGEEnterpriseFiscalGroupAS.Errors.Clear();
            ValidateModelState(pGEEnterpriseGeoVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterpriseGeoVM, ModelState);

            var Enterprise = _IGEEnterpriseFiscalGroupAS.Add(Mapper.Map<GEEnterpriseFiscalGroupVM, GEEnterpriseFiscalGroup>(pGEEnterpriseGeoVM));

            if (_IGEEnterpriseFiscalGroupAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEEnterpriseFiscalGroupAS.Errors);
            else
                await _IGEEnterpriseFiscalGroupAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEEnterpriseFiscalGroupAS.Errors.Clear();
            var Result = await _IGEEnterpriseFiscalGroupAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

            if (Result != null && !_IGEEnterpriseFiscalGroupAS.Errors.Any())
                RequestOK(Mapper.Map<GEEnterpriseFiscalGroup, GEEnterpriseFiscalGroupVM>(Result));

            return InvalidRequest(Result, _IGEEnterpriseFiscalGroupAS.Errors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEnterpriseFiscalGroupAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
