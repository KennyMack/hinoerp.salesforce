﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Globalization;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General.Business
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Business/Enterprises/Geo/{pEstablishmentKey}")]
    public class EnterpriseGeoController : BaseController
    {
        private readonly IGEEnterpriseGeoAS _IGEEnterpriseGeoAS;
        public EnterpriseGeoController(IGEEnterpriseGeoAS pIGEEnterpriseGeoAS)
        {
            _IGEEnterpriseGeoAS = pIGEEnterpriseGeoAS;
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();

            /*
             r => r.EstablishmentKey == pEstablishmentKey &&
                   (
                     (
                        (r.Id.ToString().Contains(filter)) ||
                        (r.Address.ToUpper().Contains(filter)) ||
                        (r.ZipCode.ToUpper().Contains(filter)) ||
                        (r.CNPJCPF.ToUpper().Contains(filter)) ||
                        (r.Email.ToUpper().Contains(filter))
                     ) ||
                     (filter == "")
                   )
             */

            var Results =
                Mapper.Map<PagedResult<GEEnterpriseGeo>, PagedResult<GEEnterpriseGeoVM>>
                (
                await _IGEEnterpriseGeoAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    GEEnterpriseGeoVM.GetDefaultFilter(pEstablishmentKey, filter),
                s => s.GEEnterprises));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterpriseGeoAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEEnterpriseGeo>, PagedResult<GEEnterpriseGeoVM>>
                (
                await _IGEEnterpriseGeoAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterpriseGeoAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEEnterpriseGeoVM pGEEnterpriseGeoVM)
        {
            _IGEEnterpriseGeoAS.Errors.Clear();
            pGEEnterpriseGeoVM.Id = id;
            pGEEnterpriseGeoVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEEnterpriseGeoVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterpriseGeoVM, ModelState);

            var Enterprise = _IGEEnterpriseGeoAS.Update(Mapper.Map<GEEnterpriseGeoVM, GEEnterpriseGeo>(pGEEnterpriseGeoVM));

            if (_IGEEnterpriseGeoAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEEnterpriseGeoAS.Errors);
            else
                await _IGEEnterpriseGeoAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEEnterpriseGeoVM pGEEnterpriseGeoVM)
        {
            _IGEEnterpriseGeoAS.Errors.Clear();
            ValidateModelState(pGEEnterpriseGeoVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterpriseGeoVM, ModelState);

            var Enterprise = _IGEEnterpriseGeoAS.Add(Mapper.Map<GEEnterpriseGeoVM, GEEnterpriseGeo>(pGEEnterpriseGeoVM));

            if (_IGEEnterpriseGeoAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEEnterpriseGeoAS.Errors);
            else
                await _IGEEnterpriseGeoAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEEnterpriseGeoAS.Errors.Clear();
            var Enterprise = Mapper.Map<GEEnterpriseGeo, GEEnterpriseGeoVM>(
                await _IGEEnterpriseGeoAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            if (_IGEEnterpriseGeoAS.Errors.Count > 0)
                return InvalidRequest(null, _IGEEnterpriseGeoAS.Errors);

            await _IGEEnterpriseGeoAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEnterpriseGeoAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
