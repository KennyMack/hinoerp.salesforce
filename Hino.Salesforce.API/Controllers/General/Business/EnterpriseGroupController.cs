﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General.Business
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Business/Enterprises/Group/{pEstablishmentKey}")]
    public class EnterpriseGroupController : BaseController
    {
        private readonly IGEEnterpriseGroupAS _IGEEnterpriseGroupAS;
        public EnterpriseGroupController(IGEEnterpriseGroupAS pIGEEnterpriseGroupAS)
        {
            _IGEEnterpriseGroupAS = pIGEEnterpriseGroupAS;
        }

        [Route("sync")]
        [HttpPost]
        public async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] GEEnterpriseGroupVM[] syncDataVM)
        {
            _IGEEnterpriseGroupAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);

            _IGEEnterpriseGroupAS.DontSendToQueue = true;
            var Result = await _IGEEnterpriseGroupAS.SyncData(pEstablishmentKey, Mapper.Map<GEEnterpriseGroupVM[], GEEnterpriseGroup[]>(syncDataVM));
            _IGEEnterpriseGroupAS.DontSendToQueue = false;

            if (_IGEEnterpriseGroupAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEEnterpriseGroupAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();

            /*
             r => r.EstablishmentKey == pEstablishmentKey &&
                   (
                     (
                        (r.Id.ToString().Contains(filter)) ||
                        (r.Description.ToUpper().Contains(filter)) ||
                        (r.Identifier.ToUpper().Contains(filter))
                     ) ||
                     (filter == "")
                   )
             */

            var Results =
                Mapper.Map<PagedResult<GEEnterpriseGroup>, PagedResult<GEEnterpriseGroupVM>>
                (
                await _IGEEnterpriseGroupAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    GEEnterpriseGroupVM.GetDefaultFilter(pEstablishmentKey, filter)
                ));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterpriseGroupAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEEnterpriseGroup>, PagedResult<GEEnterpriseGroupVM>>
                (
                await _IGEEnterpriseGroupAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterpriseGroupAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEEnterpriseGroupVM pGEEnterpriseGroupVM)
        {
            _IGEEnterpriseGroupAS.Errors.Clear();
            pGEEnterpriseGroupVM.Id = id;
            pGEEnterpriseGroupVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEEnterpriseGroupVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterpriseGroupVM, ModelState);

            var Enterprise = _IGEEnterpriseGroupAS.Update(Mapper.Map<GEEnterpriseGroupVM, GEEnterpriseGroup>(pGEEnterpriseGroupVM));

            if (_IGEEnterpriseGroupAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEEnterpriseGroupAS.Errors);
            else
                await _IGEEnterpriseGroupAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEEnterpriseGroupVM pGEEnterpriseGroupVM)
        {
            _IGEEnterpriseGroupAS.Errors.Clear();
            ValidateModelState(pGEEnterpriseGroupVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterpriseGroupVM, ModelState);

            var Enterprise = _IGEEnterpriseGroupAS.Add(Mapper.Map<GEEnterpriseGroupVM, GEEnterpriseGroup>(pGEEnterpriseGroupVM));

            if (_IGEEnterpriseGroupAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEEnterpriseGroupAS.Errors);
            else
                await _IGEEnterpriseGroupAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEEnterpriseGroupAS.Errors.Clear();
            var Result = await _IGEEnterpriseGroupAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

            if (Result != null && !_IGEEnterpriseGroupAS.Errors.Any())
            {
                await _IGEEnterpriseGroupAS.SaveChanges();
                return RequestOK(Mapper.Map<GEEnterpriseGroup, GEEnterpriseGroupVM>(Result));
            }

            return InvalidRequest(Result, _IGEEnterpriseGroupAS.Errors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEnterpriseGroupAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
