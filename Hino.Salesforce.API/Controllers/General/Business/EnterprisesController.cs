﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using Hino.Salesforce.Infra.Cross.Utils;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General.Business
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Business/Enterprises/{pEstablishmentKey}")]
    public class EnterprisesController : BaseController
    {
        private readonly IGEEnterprisesAS _IGEEnterprisesAS;
        private readonly IGEUserEnterprisesAS _IGEUserEnterprisesAS;
        public EnterprisesController(IGEEnterprisesAS pIGEEnterprisesAS,
            IGEUserEnterprisesAS pIGEUserEnterprisesAS)
        {
            _IGEEnterprisesAS = pIGEEnterprisesAS;
            _IGEUserEnterprisesAS = pIGEUserEnterprisesAS;
        }

        [Route("sync/date/{pDate}/user/{pUserId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate, int pUserId)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEEnterprisesVM>>
                (
                await _IGEEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date,
                s => s.GEPaymentCondition,
                x => x.GEPaymentCondition.GEPaymentType,
                g => g.GEEnterpriseGeo,
                t => t.GEEnterpriseContacts,
                j => j.GEEnterpriseCategory,
                o => o.GEEnterpriseGroup,
                s => s.VETypeSale,
                h => h.GEEnterpriseFiscalGroup));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEEnterprisesVM>>
                (
                await _IGEEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date,
                s => s.GEPaymentCondition,
                x => x.GEPaymentCondition.GEPaymentType,
                g => g.GEEnterpriseGeo,
                t => t.GEEnterpriseContacts,
                j => j.GEEnterpriseCategory,
                o => o.GEEnterpriseGroup,
                s => s.VETypeSale,
                h => h.GEEnterpriseFiscalGroup));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Results);
        }

        [Route("push/new")]
        [HttpGet]
        public async Task<IHttpActionResult> GetPushNew(string pEstablishmentKey)
        {
            var Results =
                Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEEnterprisesVM>>
                (
                    await _IGEEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                        r => r.EstablishmentKey == pEstablishmentKey &&
                        r.StatusSinc == Infra.Cross.Utils.Enums.EStatusSinc.Sincronized,
                        s => s.GEPaymentCondition,
                        x => x.GEPaymentCondition.GEPaymentType,
                        g => g.GEEnterpriseGeo,
                        t => t.GEEnterpriseContacts,
                        j => j.GEEnterpriseCategory,
                        o => o.GEEnterpriseGroup,
                        s => s.VETypeSale,
                        h => h.GEEnterpriseFiscalGroup)
                );

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] GESyncEnterprisesVM[] syncDataVM)
        {
            _IGEEnterprisesAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);

            _IGEEnterprisesAS.DontSendToQueue = true;
            var Result = await _IGEEnterprisesAS.SyncData(pEstablishmentKey, Mapper.Map<GESyncEnterprisesVM[], GEEnterprises[]>(syncDataVM));
            _IGEEnterprisesAS.DontSendToQueue = false;

            if (_IGEEnterprisesAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEEnterprisesAS.Errors);

            return RequestOK(Result);
        }

        [Route("sync/erp")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncERPData(string pEstablishmentKey, [FromBody] GESyncEnterprisesVM[] syncDataVM)
        {
            _IGEEnterprisesAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);

            _IGEEnterprisesAS.DontSendToQueue = true;
            var Result = await _IGEEnterprisesAS.SyncDataERP(pEstablishmentKey, Mapper.Map<GESyncEnterprisesVM[], GEEnterprises[]>(syncDataVM));
            _IGEEnterprisesAS.DontSendToQueue = false;

            if (_IGEEnterprisesAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEEnterprisesAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();
            var column = GetQueryColumn();

            var Results =
                Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEEnterprisesVM>>
                (
                    await _IGEEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                        GEEnterprisesVM.GetDefaultFilter(pEstablishmentKey, filter, column),
                        s => s.GEPaymentCondition,
                        x => x.GEPaymentCondition.GEPaymentType,
                        g => g.GEEnterpriseGeo,
                        t => t.GEEnterpriseContacts,
                        j => j.GEEnterpriseCategory,
                        o => o.GEEnterpriseGroup,
                        s => s.VETypeSale,
                        h => h.GEEnterpriseFiscalGroup)
                );

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Results);
        }

        [Route("search/clients/prospects")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSearchClientsOrProspects(string pEstablishmentKey)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();
            var column = GetQueryColumn();

            var Results =
                Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEClientsVM>>
                (
                    await _IGEEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    GEEnterprisesVM.GetDefaultFilterClientOrProspect(pEstablishmentKey, filter, column),
                    g => g.GEEnterpriseGeo
                ));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Results);
        }
        //ok
        [Route("all/clients/prospects")]
        [HttpGet]
        public async Task<IHttpActionResult> GetClientsOrProspects(string pEstablishmentKey)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();
            var column = GetQueryColumn();

            var Results =
                Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEClientsVM>>
                (
                await _IGEEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                GEEnterprisesVM.GetDefaultFilterClientOrProspect(pEstablishmentKey, filter, column)
                ));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Results);
        }

        [Route("search/clients/prospects/user/{pUserId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSearchClientsOrProspectsByUserId(string pEstablishmentKey, long pUserId)
        {
            var filter = GetQueryFilter();
            var column = GetQueryColumn();
            var UserResults = await _IGEUserEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                GEUserEnterprisesVM.GetDefaultFilterClientOrProspect(pEstablishmentKey, filter, pUserId, column),
                    g => g.GEEnterprises.GEEnterpriseGeo
            );

            var Results = new PagedResult<GEEnterprises>
            {
                CurrentPage = UserResults.CurrentPage,
                PageCount = UserResults.PageCount,
                PageSize = UserResults.PageSize,
                RowCount = UserResults.RowCount
            };

            foreach (var item in UserResults.Results)
                Results.Results.Add(item.GEEnterprises);

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEClientsVM>>(Results));
        }

        [Route("all/clients/prospects/user/{pUserId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetClientsOrProspectsByUserId(string pEstablishmentKey, long pUserId)
        {
            var filter = GetQueryFilter();
            var column = GetQueryColumn();

            var UserResults = await _IGEUserEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                GEUserEnterprisesVM.GetDefaultFilterClientOrProspect(pEstablishmentKey, filter, pUserId, column),
                    s => s.GEEnterprises.GEPaymentCondition,
                    x => x.GEEnterprises.GEPaymentCondition.GEPaymentType,
                    g => g.GEEnterprises.GEEnterpriseGeo,
                    t => t.GEEnterprises.GEEnterpriseContacts,
                    j => j.GEEnterprises.GEEnterpriseCategory,
                    o => o.GEEnterprises.GEEnterpriseGroup,
                    s => s.GEEnterprises.VETypeSale,
                    h => h.GEEnterprises.GEEnterpriseFiscalGroup
            );

            var Results = new PagedResult<GEEnterprises>
            {
                CurrentPage = UserResults.CurrentPage,
                PageCount = UserResults.PageCount,
                PageSize = UserResults.PageSize,
                RowCount = UserResults.RowCount
            };

            foreach (var item in UserResults.Results)
                Results.Results.Add(item.GEEnterprises);

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEEnterprisesVM>>(Results));
        }
        //ok
        [Route("all/clients")]
        [HttpGet]
        public async Task<IHttpActionResult> GetClients(string pEstablishmentKey)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();
            var column = GetQueryColumn();

            var Results =
                Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEMainClientsVM>>
                (
                    await _IGEEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    GEEnterprisesVM.GetDefaultFilterByClassification(pEstablishmentKey, filter, column, Infra.Cross.Utils.Enums.EEnterpriseClassification.Client),
                    g => g.GEEnterpriseGeo
                    )
                );

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Results);
        }

        [Route("search/clients")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSearchClients(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();
            var column = GetQueryColumn();
            var pageNum = GetPageNumber();
            var Limit = GetLimitNumber();
            var filterq = GEEnterprisesVM.GetDefaultFilterByClassification(pEstablishmentKey, filter, column, Infra.Cross.Utils.Enums.EEnterpriseClassification.Client);

            var rows = await _IGEEnterprisesAS.QueryPagedAsync(pageNum, Limit, filterq);

            var Results =
                Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEClientsVM>>
                (
                    rows
                );

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Results);
        }
        //ok
        [Route("all/clients/user/{pUserId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetClientsByUserId(string pEstablishmentKey, long pUserId)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();
            var column = GetQueryColumn();

            var UserResults = await _IGEUserEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                GEUserEnterprisesVM.GetDefaultFilterByClassification(pEstablishmentKey, filter, pUserId, column, Infra.Cross.Utils.Enums.EEnterpriseClassification.Client),
                    g => g.GEEnterprises.GEEnterpriseGeo
            );

            var Results = new PagedResult<GEEnterprises>
            {
                CurrentPage = UserResults.CurrentPage,
                PageCount = UserResults.PageCount,
                PageSize = UserResults.PageSize,
                RowCount = UserResults.RowCount
            };

            foreach (var item in UserResults.Results)
                Results.Results.Add(item.GEEnterprises);

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEMainClientsVM>>(Results));
        }

        [Route("search/clients/user/{pUserId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSearchClientsByUserId(string pEstablishmentKey, long pUserId)
        {
            var filter = GetQueryFilter();
            var column = GetQueryColumn();

            var UserResults = await _IGEUserEnterprisesAS.QuerySearchPagedAsync(GetPageNumber(), GetLimitNumber(),
                GEUserEnterprisesVM.GetDefaultFilterByClassification(pEstablishmentKey, filter, pUserId, column, Infra.Cross.Utils.Enums.EEnterpriseClassification.Client),
                    g => g.GEEnterprises.GEEnterpriseGeo
            );

            var Results = new PagedResult<GEEnterprises>
            {
                CurrentPage = UserResults.CurrentPage,
                PageCount = UserResults.PageCount,
                PageSize = UserResults.PageSize,
                RowCount = UserResults.RowCount
            };

            foreach (var item in UserResults.Results)
                Results.Results.Add(item.GEEnterprises);

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEClientsVM>>(Results));
        }
        //ok
        [Route("all/prospects")]
        [HttpGet]
        public async Task<IHttpActionResult> GetProspects(string pEstablishmentKey)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();
            var column = GetQueryColumn();

            var Results =
                Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEMainClientsVM>>
                (
                await _IGEEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                  GEEnterprisesVM.GetDefaultFilterByClassification(pEstablishmentKey, filter, column, Infra.Cross.Utils.Enums.EEnterpriseClassification.Prospect),
                    g => g.GEEnterpriseGeo
                    ));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Results);
        }

        [Route("all/prospects/user/{pUserId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetProspectsByUserId(string pEstablishmentKey, long pUserId)
        {
            var filter = GetQueryFilter();
            var column = GetQueryColumn();

            var UserResults = await _IGEUserEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                GEUserEnterprisesVM.GetDefaultFilterByClassification(pEstablishmentKey, filter, pUserId, column, Infra.Cross.Utils.Enums.EEnterpriseClassification.Prospect),
                    g => g.GEEnterprises.GEEnterpriseGeo
            );

            var Results = new PagedResult<GEEnterprises>
            {
                CurrentPage = UserResults.CurrentPage,
                PageCount = UserResults.PageCount,
                PageSize = UserResults.PageSize,
                RowCount = UserResults.RowCount
            };

            foreach (var item in UserResults.Results)
                Results.Results.Add(item.GEEnterprises);

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEMainClientsVM>>(Results));
        }

        //ok
        [Route("all/search/carriers")]
        [HttpGet]
        public async Task<IHttpActionResult> GetCarriersSearch(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();
            var column = GetQueryColumn();

            var Results =
                Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GECarriersVM>>
                (
                await _IGEEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    GEEnterprisesVM.GetDefaultFilterByClassification(pEstablishmentKey, filter, column, Infra.Cross.Utils.Enums.EEnterpriseClassification.Carrier)));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Results);
        }


        //ok
        [Route("all/carriers")]
        [HttpGet]
        public async Task<IHttpActionResult> GetCarriers(string pEstablishmentKey)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();
            var column = GetQueryColumn();

            var Results =
                Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEMainCarriersVM>>
                (
                await _IGEEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    GEEnterprisesVM.GetDefaultFilterByClassification(pEstablishmentKey, filter, column, Infra.Cross.Utils.Enums.EEnterpriseClassification.Carrier),
                g => g.GEEnterpriseGeo));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Results);
        }

        [Route("all/carriers/user/{pUserId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetCarriersByUserId(string pEstablishmentKey, long pUserId)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();
            var column = GetQueryColumn();

            var Results =
                Mapper.Map<PagedResult<GEEnterprises>, PagedResult<GEMainCarriersVM>>
                (
                await _IGEEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    GEEnterprisesVM.GetDefaultFilterByClassification(pEstablishmentKey, filter, column, Infra.Cross.Utils.Enums.EEnterpriseClassification.Carrier),
                g => g.GEEnterpriseGeo));

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}/detail")]
        [HttpGet]
        public async Task<IHttpActionResult> GetDetailById(string pEstablishmentKey, long pId)
        {
            var ret = await _IGEEnterprisesAS.QueryAsync(
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey,
                g => g.GEEnterpriseGeo
                );

            if (!ret.Any())
                return NotFoundRequest();

            var Result = Mapper.Map<GEEnterprises, GEViewEnterpriseDetailVM>
                (
                    ret.First()
                );

            return RequestOK(Result);
        }

        //ok
        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results = await _IGEEnterprisesAS.GetEnterprise(pEstablishmentKey, pId);

            if (Results == null)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            if (!Results.Results.Any())
                return NotFoundRequest();

            return RequestOK(Results);
        }

        //ok
        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, [FromBody] GEEnterprisesVM pGEEnterprisesVM)
        {
            _IGEEnterprisesAS.Errors.Clear();
            pGEEnterprisesVM.Id = id;
            pGEEnterprisesVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEEnterprisesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterprisesVM, ModelState);

            var Enterprise = await _IGEEnterprisesAS.UpdateEnterprise(pGEEnterprisesVM);

            if (_IGEEnterprisesAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEEnterprisesAS.Errors);

            return RequestOK(Enterprise);
        }

        //ok
        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/convert")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> PutConvert(string pUniqueKey, long id, [FromBody] GEEnterprisesVM pGEEnterprisesVM)
        {
            _IGEEnterprisesAS.Errors.Clear();

            var Enterprise = await _IGEEnterprisesAS.UpdateToClient(pUniqueKey, id, pGEEnterprisesVM);

            if (_IGEEnterprisesAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEEnterprisesAS.Errors);

            return RequestOK(Enterprise);
        }
        //ok
        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEEnterprisesVM pGEEnterprisesVM)
        {
            _IGEEnterprisesAS.Errors.Clear();
            ValidateModelState(pGEEnterprisesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEnterprisesVM, ModelState);

            var Enterprise = await _IGEEnterprisesAS.CreateEnterprise(pGEEnterprisesVM);

            if (_IGEEnterprisesAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEEnterprisesAS.Errors);

            return RequestOK(Enterprise);
        }
        //ok
        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEEnterprisesAS.Errors.Clear();
            var Enterprise = Mapper.Map<GEEnterprises, GEEnterprisesVM>(
                await _IGEEnterprisesAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            if (_IGEEnterprisesAS.Errors.Count > 0)
                return InvalidRequest(null, _IGEEnterprisesAS.Errors);

            return RequestOK(Enterprise);
        }

        //ok
        [HttpGet]
        [Route("client/exists/cnpj/{pCNPJ}")]
        public async Task<IHttpActionResult> DocumentAlreadyExists(string pEstablishmentKey, string pCnpj)
        {
            string cnpjSearch;

            if (!pCnpj.IsCPFOrCNPJ())
                return InvalidRequest(pCnpj, Hino.Salesforce.Infra.Cross.Resources.ValidationMessagesResource.InvalidCNPJCPF);

            cnpjSearch = pCnpj.FormataCPFCNPJ();

            var documentExists = await _IGEEnterprisesAS.EnterpriseExists(pEstablishmentKey, cnpjSearch, Infra.Cross.Utils.Enums.EEnterpriseClassification.Client, 0);

            return RequestOK(documentExists);
        }

        //ok
        [HttpGet]
        [Route("search/cnpj/{pCNPJ}")]
        public async Task<IHttpActionResult> SearchEnterpriseData(string pEstablishmentKey, string pCNPJ)
        {
            _IGEEnterprisesAS.Errors.Clear();

            var result = await _IGEEnterprisesAS.SearchEnterpriseAsync(pEstablishmentKey, pCNPJ);

            if (_IGEEnterprisesAS.Errors.Any())
                return InvalidRequest(pCNPJ, _IGEEnterprisesAS.Errors);

            if (result != null)
                return RequestOK(result);

            return InvalidRequest(pCNPJ, Infra.Cross.Resources.ValidationMessagesResource.InvalidCNPJ);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEnterprisesAS.Dispose();
                _IGEUserEnterprisesAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}