﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General.Business
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Business/Payment/Condition/{pEstablishmentKey}")]
    public class PaymentConditionController : BaseController
    {
        private readonly IGEPaymentConditionAS _IGEPaymentConditionAS;
        public PaymentConditionController(IGEPaymentConditionAS pIGEPaymentConditionAS)
        {
            _IGEPaymentConditionAS = pIGEPaymentConditionAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<GEPaymentCondition>, PagedResult<GEPaymentConditionVM>>
                (
                await _IGEPaymentConditionAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date,
                s => s.GEPaymentType
                ));

            if (Results == null)
                return InvalidRequest(null, _IGEPaymentConditionAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] GEPaymentConditionVM[] syncDataVM)
        {
            _IGEPaymentConditionAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);
            _IGEPaymentConditionAS.DontSendToQueue = true;
            var Result = await _IGEPaymentConditionAS.SyncData(pEstablishmentKey, Mapper.Map<GEPaymentConditionVM[], GEPaymentCondition[]>(syncDataVM));
            _IGEPaymentConditionAS.DontSendToQueue = false;
            if (_IGEPaymentConditionAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEPaymentConditionAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<GEPaymentCondition>, PagedResult<GEPaymentConditionVM>>
                (
                await _IGEPaymentConditionAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                GEPaymentConditionVM.GetDefaultFilter(pEstablishmentKey, filter),
                s => s.GEPaymentType,
                g => g.GEPaymentCondInstallments));

            if (Results == null)
                return InvalidRequest(null, _IGEPaymentConditionAS.Errors);

            return RequestOK(Results);
        }

        [Route("user/{pUserId}/all")]
        [HttpGet]
        public async Task<IHttpActionResult> GetByUser(string pEstablishmentKey, long pUserId)
        {
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<GEPaymentCondition>, PagedResult<GEPaymentConditionVM>>
                (
                    await _IGEPaymentConditionAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                        GEPaymentConditionVM.GetFilterByUser(pEstablishmentKey, pUserId, filter),
                        s => s.GEPaymentType,
                        g => g.GEPaymentCondInstallments)
                );

            if (Results == null)
                return InvalidRequest(null, _IGEPaymentConditionAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEPaymentCondition>, PagedResult<GEPaymentConditionVM>>
                (
                 await _IGEPaymentConditionAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey,
                s => s.GEPaymentType,
                g => g.GEPaymentCondInstallments));

            if (Results == null)
                return InvalidRequest(null, _IGEPaymentConditionAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEPaymentConditionCreateVM pGEPaymentConditionVM)
        {
            _IGEPaymentConditionAS.Errors.Clear();
            pGEPaymentConditionVM.Id = id;
            pGEPaymentConditionVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEPaymentConditionVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEPaymentConditionVM, ModelState);

            var Result = await _IGEPaymentConditionAS.ChangeAsync(Mapper.Map<GEPaymentConditionCreateVM, GEPaymentCondition>(pGEPaymentConditionVM));

            if (_IGEPaymentConditionAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEPaymentConditionAS.Errors);
            else
                await _IGEPaymentConditionAS.SaveChanges();

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEPaymentConditionCreateVM pGEPaymentConditionVM)
        {
            _IGEPaymentConditionAS.Errors.Clear();
            ValidateModelState(pGEPaymentConditionVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEPaymentConditionVM, ModelState);

            var Result = _IGEPaymentConditionAS.Add(Mapper.Map<GEPaymentConditionCreateVM, GEPaymentCondition>(pGEPaymentConditionVM));

            if (_IGEPaymentConditionAS.Errors.Any())
                return InvalidRequest(Result, _IGEPaymentConditionAS.Errors);
            else
                await _IGEPaymentConditionAS.SaveChanges();

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEPaymentConditionAS.Errors.Clear();
            var Result = Mapper.Map<GEPaymentCondition, GEPaymentConditionVM>(
                await _IGEPaymentConditionAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            if (_IGEPaymentConditionAS.Errors.Count > 0)
                return InvalidRequest(null, _IGEPaymentConditionAS.Errors);

            await _IGEPaymentConditionAS.SaveChanges();
            return RequestOK(Result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEPaymentConditionAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}