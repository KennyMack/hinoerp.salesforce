﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General.Business
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Business/Payment/Type/{pEstablishmentKey}")]
    public class PaymentTypeController : BaseController
    {
        private readonly IGEPaymentTypeAS _IGEPaymentTypeAS;
        public PaymentTypeController(IGEPaymentTypeAS pIGEPaymentTypeAS)
        {
            _IGEPaymentTypeAS = pIGEPaymentTypeAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<GEPaymentType>, PagedResult<GEPaymentTypeVM>>
                (
                await _IGEPaymentTypeAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date,
                s => s.GEPaymentCondition));

            if (Results == null)
                return InvalidRequest(null, _IGEPaymentTypeAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] GEPaymentTypeVM[] syncDataVM)
        {
            _IGEPaymentTypeAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);
            _IGEPaymentTypeAS.DontSendToQueue = true;

            var Result = await _IGEPaymentTypeAS.SyncData(pEstablishmentKey, Mapper.Map<GEPaymentTypeVM[], GEPaymentType[]>(syncDataVM));
            _IGEPaymentTypeAS.DontSendToQueue = false;

            if (_IGEPaymentTypeAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEPaymentTypeAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();

            /*
             * r => r.EstablishmentKey == pEstablishmentKey &&
                    (
                        (
                            (r.Description.ToUpper().Contains(filter))
                        ) ||
                        (filter == "")
                    )
             */

            var Results =
                Mapper.Map<PagedResult<GEPaymentType>, PagedResult<GEPaymentTypeVM>>
                (
                await _IGEPaymentTypeAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    GEPaymentTypeVM.GetDefaultFilter(pEstablishmentKey, filter),
                s => s.GEPaymentCondition));

            if (Results == null)
                return InvalidRequest(null, _IGEPaymentTypeAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEPaymentType>, PagedResult<GEPaymentTypeVM>>
                (
                await _IGEPaymentTypeAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey,
                s => s.GEPaymentCondition));

            if (Results == null)
                return InvalidRequest(null, _IGEPaymentTypeAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEPaymentTypeVM pGEPaymentTypeVM)
        {
            _IGEPaymentTypeAS.Errors.Clear();
            pGEPaymentTypeVM.Id = id;
            pGEPaymentTypeVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEPaymentTypeVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEPaymentTypeVM, ModelState);

            var Result = _IGEPaymentTypeAS.Update(Mapper.Map<GEPaymentTypeVM, GEPaymentType>(pGEPaymentTypeVM));

            await _IGEPaymentTypeAS.SaveChanges();

            if (_IGEPaymentTypeAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEPaymentTypeAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEPaymentTypeVM pGEPaymentTypeVM)
        {
            _IGEPaymentTypeAS.Errors.Clear();
            ValidateModelState(pGEPaymentTypeVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEPaymentTypeVM, ModelState);

            var Result = _IGEPaymentTypeAS.Add(Mapper.Map<GEPaymentTypeVM, GEPaymentType>(pGEPaymentTypeVM));

            await _IGEPaymentTypeAS.SaveChanges();

            if (_IGEPaymentTypeAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEPaymentTypeAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEPaymentTypeAS.Errors.Clear();
            var Result = Mapper.Map<GEPaymentType, GEPaymentTypeVM>(
                await _IGEPaymentTypeAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            if (_IGEPaymentTypeAS.Errors.Count > 0)
                return InvalidRequest(null, _IGEPaymentTypeAS.Errors);

            await _IGEPaymentTypeAS.SaveChanges();
            return RequestOK(Result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEPaymentTypeAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}