﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General.Business
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Business/User/Enterprises/{pEstablishmentKey}")]
    public class UserEnterprisesController : BaseController
    {
        private readonly IGEUserEnterprisesAS _IGEUserEnterprisesAS;
        public UserEnterprisesController(IGEUserEnterprisesAS pIGEUserEnterprisesAS)
        {
            _IGEUserEnterprisesAS = pIGEUserEnterprisesAS;
        }

        [Route("user/{pUserId}/all")]
        [HttpGet]
        public async Task<IHttpActionResult> GetByUser(string pEstablishmentKey, long pUserId)
        {
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<GEUserEnterprises>, PagedResult<GEUserEnterprisesVM>>
                (
                await _IGEUserEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                   (
                     (
                        (r.Id.ToString().Contains(filter)) ||
                        (r.UserId.ToString().Contains(filter)) ||
                        (r.EnterpriseId.ToString().Contains(filter))
                     ) ||
                     (filter == "")
                   ),
                s => s.GEEnterprises,
                t => t.GEUsers));

            if (Results == null)
                return InvalidRequest(null, _IGEUserEnterprisesAS.Errors);

            return RequestOK(Results);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<GEUserEnterprises>, PagedResult<GEUserEnterprisesVM>>
                (
                await _IGEUserEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                   (
                     (
                        (r.Id.ToString().Contains(filter)) ||
                        (r.UserId.ToString().Contains(filter)) ||
                        (r.EnterpriseId.ToString().Contains(filter))
                     ) ||
                     (filter == "")
                   ),
                s => s.GEEnterprises,
                t => t.GEUsers));

            if (Results == null)
                return InvalidRequest(null, _IGEUserEnterprisesAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEUserEnterprises>, PagedResult<GEUserEnterprisesVM>>
                (
                await _IGEUserEnterprisesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId && 
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEUserEnterprisesAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEUserEnterprisesVM pGEUserEnterprisesVM)
        {
            _IGEUserEnterprisesAS.Errors.Clear();
            pGEUserEnterprisesVM.Id = id;
            pGEUserEnterprisesVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEUserEnterprisesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEUserEnterprisesVM, ModelState);

            var Enterprise = _IGEUserEnterprisesAS.Update(Mapper.Map<GEUserEnterprisesVM, GEUserEnterprises>(pGEUserEnterprisesVM));

            if (_IGEUserEnterprisesAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEUserEnterprisesAS.Errors);
            else
                await _IGEUserEnterprisesAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEUserEnterprisesVM pGEUserEnterprisesVM)
        {
            _IGEUserEnterprisesAS.Errors.Clear();
            ValidateModelState(pGEUserEnterprisesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEUserEnterprisesVM, ModelState);

            var Enterprise = await _IGEUserEnterprisesAS.CreateOrUpdateAsync(pGEUserEnterprisesVM);

            if (_IGEUserEnterprisesAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEUserEnterprisesAS.Errors);
            else
                await _IGEUserEnterprisesAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpPost]
        [Route("list/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> PostList(string pEstablishmentKey, [FromBody] GEUserEnterprisesListVM pGEUserEnterprisesVM)
        {
            _IGEUserEnterprisesAS.Errors.Clear();
            ValidateModelState(pGEUserEnterprisesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEUserEnterprisesVM, ModelState);

            var Enterprise = await _IGEUserEnterprisesAS.CreateOrUpdateListAsync(pEstablishmentKey, pGEUserEnterprisesVM.results);

            if (_IGEUserEnterprisesAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEUserEnterprisesAS.Errors);
            else
                await _IGEUserEnterprisesAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEUserEnterprisesAS.Errors.Clear();
            var Result = await _IGEUserEnterprisesAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

            if (Result != null && !_IGEUserEnterprisesAS.Errors.Any())
            {
                await _IGEUserEnterprisesAS.SaveChanges();
                return RequestOK(Mapper.Map<GEUserEnterprises, GEUserEnterprisesVM>(Result));
            }

            return InvalidRequest(Result, _IGEUserEnterprisesAS.Errors);
        }

        [HttpPost]
        [Route("list/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> PostDelete(string pEstablishmentKey, [FromBody] GEUserEnterprisesListVM pGEUserEnterprisesVM)
        {
            _IGEUserEnterprisesAS.Errors.Clear();
            var Result = await _IGEUserEnterprisesAS.RemoveListAsync(pEstablishmentKey, pGEUserEnterprisesVM.results);

            if (Result != null && !_IGEUserEnterprisesAS.Errors.Any())
            {
                await _IGEUserEnterprisesAS.SaveChanges();
                return RequestOK(Mapper.Map<GEUserEnterprises[], GEUserEnterprisesVM[]>(Result));
            }

            return InvalidRequest(Result, _IGEUserEnterprisesAS.Errors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEUserEnterprisesAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}