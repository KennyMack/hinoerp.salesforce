﻿using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.Application.Interfaces.General.Business;
using System.Threading.Tasks;
using System.Linq;
using System.Web.Http;
using AutoMapper;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.API.Filter;

namespace Hino.Salesforce.API.Controllers.General.Business
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Business/Payment/Type/User/{pEstablishmentKey}")]
    public class UserPayTypeController : BaseController
    {
        private readonly IGEUserPayTypeAS _IGEUserPayTypeAS;

        public UserPayTypeController(IGEUserPayTypeAS pIGEUserPayTypeAS)
        {
            _IGEUserPayTypeAS = pIGEUserPayTypeAS;
        }

        [Route("user/{pUserId}/all")]
        [HttpGet]
        public async Task<IHttpActionResult> GetByUser(string pEstablishmentKey, long pUserId)
        {
            var userPayTypes = await _IGEUserPayTypeAS.QueryPagedAsync(
                GetPageNumber(),
                GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey && 
                     r.UserId == pUserId,
                p => p.GEPaymentType,
                u => u.GEUsers);

            if (userPayTypes == null)
                return InvalidRequest(null, _IGEUserPayTypeAS.Errors);

            var result = Mapper.Map<PagedResult<GEUserPayType>, PagedResult<GEUserPayTypeVM>>(userPayTypes);

            return RequestOK(result);
        }

        [Route("payment/type/{pPaymentTypeId}/all")]
        [HttpGet]
        public async Task<IHttpActionResult> GetByPaymentType(string pEstablishmentKey, long pPaymentTypeId)
        {
            var filter = GetQueryFilter();

            var userPayTypes = await _IGEUserPayTypeAS.QueryPagedAsync(
                GetPageNumber(),
                GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey && 
                     r.PaymentTypeId == pPaymentTypeId && 
                     r.GEUsers.Name.Contains(filter),
                p => p.GEPaymentType,
                u => u.GEUsers);

            if (userPayTypes == null)
                return InvalidRequest(null, _IGEUserPayTypeAS.Errors);

            var result = Mapper.Map<PagedResult<GEUserPayType>, PagedResult<GEUserPayTypeVM>>(userPayTypes);

            return RequestOK(result);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAll(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();

            var userPayTypes = await _IGEUserPayTypeAS.QueryPagedAsync(
                GetPageNumber(),
                GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey && 
                     r.GEUsers.Name.Contains(filter), 
                p => p.GEPaymentType,
                u => u.GEUsers);

            if (userPayTypes == null)
                return InvalidRequest(null, _IGEUserPayTypeAS.Errors);

            var result = Mapper.Map<PagedResult<GEUserPayType>, PagedResult<GEUserPayTypeVM>>(userPayTypes);

            return RequestOK(result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEUserPayTypeVM pGEUserPayTypeVM)
        {
            _IGEUserPayTypeAS.Errors.Clear();
            ValidateModelState(pGEUserPayTypeVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEUserPayTypeVM, ModelState);

            var Enterprise = await _IGEUserPayTypeAS.CreateOrUpdateAsync(pGEUserPayTypeVM);

            if (_IGEUserPayTypeAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEUserPayTypeAS.Errors);
            else
                await _IGEUserPayTypeAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpPost]
        [Route("list/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> PostList(string pEstablishmentKey, [FromBody] GEUserPayTypeListVM pGEUserPayTypeVM)
        {
            _IGEUserPayTypeAS.Errors.Clear();
            ValidateModelState(pGEUserPayTypeVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEUserPayTypeVM, ModelState);

            var Enterprise = await _IGEUserPayTypeAS.CreateOrUpdateListAsync(pEstablishmentKey, pGEUserPayTypeVM.results);

            if (_IGEUserPayTypeAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IGEUserPayTypeAS.Errors);
            else
                await _IGEUserPayTypeAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpPost]
        [Route("list/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> PostDelete(string pEstablishmentKey, [FromBody] GEUserPayTypeListVM pGEUserPayTypeListVM)
        {
            _IGEUserPayTypeAS.Errors.Clear();
            var Result = await _IGEUserPayTypeAS.RemoveListAsync(pEstablishmentKey, pGEUserPayTypeListVM.results);

            if (Result != null && !_IGEUserPayTypeAS.Errors.Any())
            {
                await _IGEUserPayTypeAS.SaveChanges();
                return RequestOK(Mapper.Map<GEUserPayType[], GEUserPayTypeVM[]>(Result));
            }

            return InvalidRequest(Result, _IGEUserPayTypeAS.Errors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEUserPayTypeAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}