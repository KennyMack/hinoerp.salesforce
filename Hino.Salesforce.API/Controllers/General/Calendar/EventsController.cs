﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General.Events;
using Hino.Salesforce.Application.ViewModels.General.Events;
using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General.Calendar
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Calendar/Events/{pEstablishmentKey}")]
    public class EventsController : BaseController
    {
        private readonly IGEUserCalendarAS _IGEUserCalendarAS;
        private readonly IGEEstabCalendarAS _IGEEstabCalendarAS;
        private readonly IGEEnterpriseEventAS _IGEEnterpriseEventAS;
        private readonly IGEEventsAS _IGEEventsAS;

        public EventsController(IGEUserCalendarAS pIGEUserCalendarAS,
            IGEEstabCalendarAS pIGEEstabCalendarAS,
            IGEEnterpriseEventAS pIGEEnterpriseEventAS,
            IGEEventsAS pIGEEventsAS)
        {
            _IGEUserCalendarAS = pIGEUserCalendarAS;
            _IGEEstabCalendarAS = pIGEEstabCalendarAS;
            _IGEEnterpriseEventAS = pIGEEnterpriseEventAS;
            _IGEEventsAS = pIGEEventsAS;
        }

        public DateTime GetStartDate(NameValueCollection pQuery)
        {
            DateTime date;
            try
            {
                date = ConvertDateUrl(pQuery.Get("start")) ?? new DateTime(2000, 1, 1);
            }
            catch (Exception)
            {
                date = new DateTime(2000, 1, 1);
            }

            return date;
        }

        public DateTime GetEndDate(NameValueCollection pQuery)
        {
            DateTime date;
            try
            {
                date = ConvertDateUrl(pQuery.Get("end")) ?? new DateTime(2999, 1, 1);
            }
            catch (Exception)
            {
                date = new DateTime(2999, 1, 1);
            }

            return date;
        }

        public bool GetIsDay(NameValueCollection pQuery)
        {
            bool isDay;
            try
            {
                isDay = Convert.ToBoolean(pQuery.Get("isDay"));
            }
            catch (Exception)
            {
                isDay = false;
            }

            return isDay;
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAll(string pEstablishmentKey)
        {
            var dateStart = GetStartDate(HttpContext.Current.Request.QueryString);
            var dateEnd = GetEndDate(HttpContext.Current.Request.QueryString);
            var IsDay = GetIsDay(HttpContext.Current.Request.QueryString);
            /*
             (
                (r.DtCalendar >= dateStart && r.DtCalendar <= dateEnd) ||
                (r.Start >= dateStart && (r.End ?? r.Start) <= dateEnd)
             )
             */
            PagedResult<GEEventsVM> Results = Mapper.Map<PagedResult<GEEvents>, PagedResult<GEEventsVM>>
            (
                await _IGEEventsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    r => r.EstablishmentKey == pEstablishmentKey &&
                            r.DtCalendar >= dateStart && r.DtCalendar <= dateEnd
                )
            );

            if (Results == null)
                return InvalidRequest(null, _IGEEventsAS.Errors);

            return RequestOK(Results);
        }

        [Route("User/{pId}/all")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllByUser(string pEstablishmentKey, long pId)
        {
            var dateStart = GetStartDate(HttpContext.Current.Request.QueryString);
            var dateEnd = GetEndDate(HttpContext.Current.Request.QueryString);
            /*var Results =
                Mapper.Map<PagedResult<GEUserCalendar>, PagedResult<GEUserCalendarVM>>
                (
                await _IGEUserCalendarAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    r => r.EstablishmentKey == pEstablishmentKey &&
                    r.UserID == pId)
                );*/
            var Results =
                Mapper.Map<PagedResult<GEEvents>, PagedResult<GEEventsVM>>
                (
                await _IGEEventsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    r => r.EstablishmentKey == pEstablishmentKey &&
                    r.GEUserCalendar.Any(s => s.EstablishmentKey == pEstablishmentKey && s.UserID == pId &&
                    ((r.Start >= dateStart && r.Start <= dateEnd) ||
                    (r.End >= dateStart && r.End <= dateEnd)))
                    )
                );

            if (Results == null)
                return InvalidRequest(null, _IGEUserCalendarAS.Errors);

            return RequestOK(Results);
        }

        [Route("Establishments/{pId}/all")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllByEstablishment(string pEstablishmentKey, long pId)
        {
            var dateStart = GetStartDate(HttpContext.Current.Request.QueryString);
            var dateEnd = GetEndDate(HttpContext.Current.Request.QueryString);
            /* var Results =
                Mapper.Map<PagedResult<GEEstabCalendar>, PagedResult<GEEstabCalendarVM>>
                (
                await _IGEEstabCalendarAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    r => r.EstablishmentKey == pEstablishmentKey &&
                    r.EstabID == pId)
                );*/
            var Results =
                Mapper.Map<PagedResult<GEEvents>, PagedResult<GEEventsVM>>
                (
                await _IGEEventsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    r => r.EstablishmentKey == pEstablishmentKey &&
                    r.GEEstabCalendar.Any(s => s.EstablishmentKey == pEstablishmentKey && s.EstabID == pId &&
                    ((r.Start >= dateStart && r.Start <= dateEnd) ||
                    (r.End >= dateStart && r.End <= dateEnd))))
                );

            if (Results == null)
                return InvalidRequest(null, _IGEEstabCalendarAS.Errors);

            return RequestOK(Results);
        }

        [Route("Enterprise/{pId}/all")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllByEnterprise(string pEstablishmentKey, long pId)
        {
            var dateStart = GetStartDate(HttpContext.Current.Request.QueryString);
            var dateEnd = GetEndDate(HttpContext.Current.Request.QueryString);
            /*var Results =
                Mapper.Map<PagedResult<GEEnterpriseEvent>, PagedResult<GEEnterpriseEventVM>>
                (
                await _IGEEnterpriseEventAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    r => r.EstablishmentKey == pEstablishmentKey &&
                    r.EnterpriseID == pId)
                );*/
            var Results =
                Mapper.Map<PagedResult<GEEvents>, PagedResult<GEEventsVM>>
                (
                await _IGEEventsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    r => r.EstablishmentKey == pEstablishmentKey &&
                    r.GEEnterpriseEvent.Any(s => s.EstablishmentKey == pEstablishmentKey && s.EnterpriseID == pId &&
                    ((r.Start >= dateStart && r.Start <= dateEnd) ||
                    (r.End >= dateStart && r.End <= dateEnd))))
                );

            if (Results == null)
                return InvalidRequest(null, _IGEEnterpriseEventAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEEvents>, PagedResult<GEEventsVM>>
                (
                await _IGEEventsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    r => r.Id == pId &&
                    r.EstablishmentKey == pEstablishmentKey,
                    s => s.GEEventsClassification));

            if (Results == null)
                return InvalidRequest(null, _IGEEventsAS.Errors);

            if (!Results.Results.Any())
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, [FromBody] GECreateEventsVM pGEEventsVM)
        {
            _IGEEventsAS.Errors.Clear();
            pGEEventsVM.Id = id;
            pGEEventsVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEEventsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEventsVM, ModelState);

            var Result = await _IGEEventsAS.UpdateEventAsync(pGEEventsVM.EstablishmentKey, pGEEventsVM);

            if (_IGEEventsAS.Errors.Any() || Result == null)
                return InvalidRequest(pGEEventsVM, _IGEEventsAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GECreateEventsVM pGEEventsVM)
        {
            _IGEEventsAS.Errors.Clear();
            ValidateModelState(pGEEventsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEventsVM, ModelState);

            var Result = await _IGEEventsAS.CreateEventAsync(pEstablishmentKey, pGEEventsVM);

            if (_IGEEventsAS.Errors.Any() || Result == null)
                return InvalidRequest(pGEEventsVM, _IGEEventsAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEEventsAS.Errors.Clear();
            var Result = await _IGEEventsAS.RemoveEventIdAsync(pId, pEstablishmentKey, pUniqueKey);

            if (_IGEEventsAS.Errors.Any() || Result == null)
                return InvalidRequest(null, _IGEEventsAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("key/{pUniqueKey}/id/{pId}/complete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> PostCompleteAsync(string pEstablishmentKey, string pUniqueKey, long pId, [FromBody] GEEventsStatusVM pEventStatus)
        {
            _IGEEventsAS.Errors.Clear();
            var Result = await _IGEEventsAS.ChangeEventStatusAsync(pId, pEstablishmentKey, pUniqueKey, pEventStatus);

            if (_IGEEventsAS.Errors.Any() || Result == null)
                return InvalidRequest(null, _IGEEventsAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("key/{pUniqueKey}/id/{pId}/generate")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> PostGenerateAsync(string pEstablishmentKey, string pUniqueKey, long pId, [FromBody] GEEventsChildVM pGEEventsChild)
        {
            _IGEEventsAS.Errors.Clear();
            var Result = await _IGEEventsAS.GenerateEventChildAsync(pId, pEstablishmentKey, pUniqueKey, pGEEventsChild);

            if (_IGEEventsAS.Errors.Any() || Result == null)
                return InvalidRequest(null, _IGEEventsAS.Errors);

            return RequestOK(Result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEUserCalendarAS.Dispose();
                _IGEEstabCalendarAS.Dispose();
                _IGEEnterpriseEventAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}