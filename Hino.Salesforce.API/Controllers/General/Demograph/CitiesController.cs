﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General.Demograph;
using Hino.Salesforce.Application.ViewModels.General.Demograph;
using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General.Demograph
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Demograph/Cities/{pEstablishmentKey}")]
    public class CitiesController : BaseController
    {
        private readonly IGECitiesAS _IGECitiesAS;
        public CitiesController(IGECitiesAS pIGECitiesAS)
        {
            _IGECitiesAS = pIGECitiesAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<GECities>, PagedResult<GECitiesVM>>
                (
                await _IGECitiesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IGECitiesAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        [IgnoreValidateEstablishmentKey]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] GECitiesVM[] syncDataVM)
        {
            _IGECitiesAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);

            var Result = await _IGECitiesAS.SyncData(pEstablishmentKey, Mapper.Map<GECitiesVM[], GECities[]>(syncDataVM));

            if (_IGECitiesAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGECitiesAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var Results =
                Mapper.Map<PagedResult<GECities>, PagedResult<GECitiesVM>>
                (
                await _IGECitiesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGECitiesAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GECities>, PagedResult<GECitiesVM>>
                (
                await _IGECitiesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId));

            if (Results == null)
                return InvalidRequest(null, _IGECitiesAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GECitiesVM pGECitiesVM)
        {
            _IGECitiesAS.Errors.Clear();
            pGECitiesVM.Id = id;
            pGECitiesVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGECitiesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGECitiesVM, ModelState);

            var Result = _IGECitiesAS.Update(Mapper.Map<GECitiesVM, GECities>(pGECitiesVM));

            await _IGECitiesAS.SaveChanges();

            if (_IGECitiesAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGECitiesAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GECitiesVM pGECitiesVM)
        {
            _IGECitiesAS.Errors.Clear();
            ValidateModelState(pGECitiesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGECitiesVM, ModelState);

            var Result = _IGECitiesAS.Add(Mapper.Map<GECitiesVM, GECities>(pGECitiesVM));

            await _IGECitiesAS.SaveChanges();

            if (_IGECitiesAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGECitiesAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGECitiesAS.Errors.Clear();
            var Result = await _IGECitiesAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

            if (Result != null && !_IGECitiesAS.Errors.Any())
                RequestOK(Mapper.Map<GECities, GECitiesVM>(Result));

            return RequestOK(Result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGECitiesAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}