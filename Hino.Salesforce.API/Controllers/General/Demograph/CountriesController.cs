﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General.Demograph;
using Hino.Salesforce.Application.ViewModels.General.Demograph;
using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General.Demograph
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Demograph/Countries/{pEstablishmentKey}")]
    public class CountriesController : BaseController
    {
        private readonly IGECountriesAS _IGECountriesAS;
        public CountriesController(IGECountriesAS pIGECountriesAS)
        {
            _IGECountriesAS = pIGECountriesAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<GECountries>, PagedResult<GECountriesVM>>
                (
                await _IGECountriesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IGECountriesAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        [IgnoreValidateEstablishmentKey]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] GECountriesVM[] syncDataVM)
        {
            _IGECountriesAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);

            var Result = await _IGECountriesAS.SyncData(pEstablishmentKey, Mapper.Map<GECountriesVM[], GECountries[]>(syncDataVM));

            if (_IGECountriesAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGECountriesAS.Errors);

            return RequestOK(Result);
        }

        [Route("load")]
        [HttpPost]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> PostLoadCountries(string pEstablishmentKey, [FromBody] GECountriesLoadVM[] loadCountries)
        {
            _IGECountriesAS.Errors.Clear();

            await _IGECountriesAS.LoadCountriesAsync(loadCountries);

            if (_IGECountriesAS.Errors.Count > 0)
                return InvalidRequest(null, _IGECountriesAS.Errors);

            return RequestOK(new { Success = true });
        }

        [Route("all")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var Results =
                Mapper.Map<PagedResult<GECountries>, PagedResult<GECountriesVM>>
                (
                await _IGECountriesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGECountriesAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GECountries>, PagedResult<GECountriesVM>>
                (
                await _IGECountriesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGECountriesAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GECountriesVM pGECountriesVM)
        {
            _IGECountriesAS.Errors.Clear();
            pGECountriesVM.Id = id;
            pGECountriesVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGECountriesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGECountriesVM, ModelState);

            var Result = _IGECountriesAS.Update(Mapper.Map<GECountriesVM, GECountries>(pGECountriesVM));

            await _IGECountriesAS.SaveChanges();

            if (_IGECountriesAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGECountriesAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GECountriesVM pGECountriesVM)
        {
            _IGECountriesAS.Errors.Clear();
            ValidateModelState(pGECountriesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGECountriesVM, ModelState);

            var Result = _IGECountriesAS.Add(Mapper.Map<GECountriesVM, GECountries>(pGECountriesVM));

            await _IGECountriesAS.SaveChanges();

            if (_IGECountriesAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGECountriesAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGECountriesAS.Errors.Clear();
            var Result = await _IGECountriesAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

            if (Result != null && !_IGECountriesAS.Errors.Any())
                RequestOK(Mapper.Map<GECountries, GECountriesVM>(Result));

            return RequestOK(Result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGECountriesAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}