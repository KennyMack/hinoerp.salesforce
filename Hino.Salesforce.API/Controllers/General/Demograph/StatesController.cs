﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General.Demograph;
using Hino.Salesforce.Application.ViewModels.General.Demograph;
using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General.Demograph
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Demograph/States/{pEstablishmentKey}")]
    public class StatesController : BaseController
    {
        private readonly IGEStatesAS _IGEStatesAS;
        public StatesController(IGEStatesAS pIGEStatesAS)
        {
            _IGEStatesAS = pIGEStatesAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<GEStates>, PagedResult<GEStatesVM>>
                (
                await _IGEStatesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IGEStatesAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        [IgnoreValidateEstablishmentKey]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] GEStatesVM[] syncDataVM)
        {
            _IGEStatesAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);

            var Result = await _IGEStatesAS.SyncData(pEstablishmentKey, Mapper.Map<GEStatesVM[], GEStates[]>(syncDataVM));

            if (_IGEStatesAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEStatesAS.Errors);

            return RequestOK(Result);
        }

        [Route("load")]
        [HttpPost]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> PostLoadStates(string pEstablishmentKey, [FromBody] GEStatesLoadVM[] loadStates)
        {
            _IGEStatesAS.Errors.Clear();

            await _IGEStatesAS.LoadStatesAsync(loadStates);

            if (_IGEStatesAS.Errors.Count > 0)
                return InvalidRequest(null, _IGEStatesAS.Errors);

            return RequestOK(new { Success = true });
        }

        [Route("all")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var Results =
                Mapper.Map<PagedResult<GEStates>, PagedResult<GEStatesVM>>
                (
                await _IGEStatesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEStatesAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEStates>, PagedResult<GEStatesVM>>
                (
                await _IGEStatesAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEStatesAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEStatesVM pGEStatesVM)
        {
            _IGEStatesAS.Errors.Clear();
            pGEStatesVM.Id = id;
            pGEStatesVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEStatesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEStatesVM, ModelState);

            var Result = _IGEStatesAS.Update(Mapper.Map<GEStatesVM, GEStates>(pGEStatesVM));

            await _IGEStatesAS.SaveChanges();

            if (_IGEStatesAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEStatesAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEStatesVM pGEStatesVM)
        {
            _IGEStatesAS.Errors.Clear();
            ValidateModelState(pGEStatesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEStatesVM, ModelState);

            var Result = _IGEStatesAS.Add(Mapper.Map<GEStatesVM, GEStates>(pGEStatesVM));

            await _IGEStatesAS.SaveChanges();

            if (_IGEStatesAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEStatesAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEStatesAS.Errors.Clear();
            var Result = await _IGEStatesAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

            if (Result != null && !_IGEStatesAS.Errors.Any())
                RequestOK(Mapper.Map<GEStates, GEStatesVM>(Result));

            return RequestOK(Result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEStatesAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}