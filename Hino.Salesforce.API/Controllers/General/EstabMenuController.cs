﻿using AutoMapper;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General
{
    [RoutePrefix("api/General/Establishment/Menu/{pEstablishmentKey}")]
    public class EstabMenuController : BaseController
    {
        private readonly IGEEstabMenuAS _IGEEstabMenuAS;
        public EstabMenuController(IGEEstabMenuAS pIGEEstabMenuAS)
        {
            _IGEEstabMenuAS = pIGEEstabMenuAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<GEEstabMenu>, PagedResult<GEEstabMenuVM>>
                (
                await _IGEEstabMenuAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IGEEstabMenuAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] GEEstabMenuVM[] syncDataVM)
        {
            _IGEEstabMenuAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);

            var Result = await _IGEEstabMenuAS.SyncData(pEstablishmentKey, Mapper.Map<GEEstabMenuVM[], GEEstabMenu[]>(syncDataVM));

            if (_IGEEstabMenuAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEEstabMenuAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var Results =
                Mapper.Map<PagedResult<GEEstabMenu>, PagedResult<GEEstabMenuVM>>
                (
                await _IGEEstabMenuAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEEstabMenuAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEEstabMenu>, PagedResult<GEEstabMenuVM>>
                (
                await _IGEEstabMenuAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId));

            if (Results == null)
                return InvalidRequest(null, _IGEEstabMenuAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEEstabMenuVM pGEEstabMenuVM)
        {
            _IGEEstabMenuAS.Errors.Clear();
            pGEEstabMenuVM.Id = id;
            pGEEstabMenuVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEEstabMenuVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEstabMenuVM, ModelState);

            var Result = _IGEEstabMenuAS.Update(Mapper.Map<GEEstabMenuVM, GEEstabMenu>(pGEEstabMenuVM));

            await _IGEEstabMenuAS.SaveChanges();

            if (_IGEEstabMenuAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEEstabMenuAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEEstabMenuVM pGEEstabMenuVM)
        {
            _IGEEstabMenuAS.Errors.Clear();
            ValidateModelState(pGEEstabMenuVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEstabMenuVM, ModelState);

            var Result = _IGEEstabMenuAS.Add(Mapper.Map<GEEstabMenuVM, GEEstabMenu>(pGEEstabMenuVM));

            await _IGEEstabMenuAS.SaveChanges();

            if (_IGEEstabMenuAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEEstabMenuAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEEstabMenuAS.Errors.Clear();
            var Result = await _IGEEstabMenuAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);
            await _IGEEstabMenuAS.SaveChanges();

            if (Result != null && !_IGEEstabMenuAS.Errors.Any())
                RequestOK(Mapper.Map<GEEstabMenu, GEEstabMenuVM>(Result));

            return RequestOK(Result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEstabMenuAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}