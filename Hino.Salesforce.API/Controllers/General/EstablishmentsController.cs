﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Cache.Establishment;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using Hino.Salesforce.Infra.Cross.Utils.Settings;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Establishments/{pEstablishmentKey}")]
    public class EstablishmentsController : BaseController
    {
        private readonly EstablishmentInfoCache _EstablishmentInfoCache;
        private readonly IGEEstablishmentsAS _IGEEstablishmentsAS;
        public EstablishmentsController(IGEEstablishmentsAS pIGEEstablishmentsAS,
            EstablishmentInfoCache pEstablishmentInfoCache)
        {
            _IGEEstablishmentsAS = pIGEEstablishmentsAS;
            _EstablishmentInfoCache = pEstablishmentInfoCache;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<GEEstablishments>, PagedResult<GEEstablishmentsVM>>
                (
                await _IGEEstablishmentsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date,
                s => s.GEEstabDevices));

            if (Results == null)
                return InvalidRequest(null, _IGEEstablishmentsAS.Errors);

            return RequestOK(Results);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var Results =
                Mapper.Map<PagedResult<GEEstablishments>, PagedResult<GEEstablishmentsVM>>
                (
                await _IGEEstablishmentsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey,
                m => m.GEEstabMenu,
                s => s.GEEstabDevices,
                p => p.FSFiscalOper));

            if (Results == null)
                return InvalidRequest(null, _IGEEstablishmentsAS.Errors);

            return RequestOK(Results);
        }

        [Route("my-estab")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey)
        {
            var keyCache = $"{pEstablishmentKey}-my-estab";

            GEEstablishments Results;

            Results = _EstablishmentInfoCache.Get<GEEstablishments>(keyCache);
            if (Results == null)
            {
                Results = await _IGEEstablishmentsAS.GetByEstablishmentKeyAsync(pEstablishmentKey);

                TimeSpan finalExpiration = TimeSpan.FromSeconds(Convert.ToInt32(AppSettings.FinalExpiration));
                _EstablishmentInfoCache.Set(keyCache, JsonConvert.SerializeObject(Results, _JsonSerializerSettings), finalExpiration);
            }

            if (Results == null)
                return InvalidRequest(null, _IGEEstablishmentsAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var keyCache = $"{pEstablishmentKey}-{pId}";

            var Results =  new PagedResult<GEEstablishmentsVM>();
            

            var Estab = _EstablishmentInfoCache.Get<GEEstablishmentsVM>(keyCache);

            if (Estab == null)
            {
                Results =
                    Mapper.Map<PagedResult<GEEstablishments>, PagedResult<GEEstablishmentsVM>>
                    (
                    await _IGEEstablishmentsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    r => r.Id == pId &&
                    r.EstablishmentKey == pEstablishmentKey,
                    s => s.GEEstabDevices,
                    m => m.GEEstabMenu,
                    p => p.FSFiscalOper,
                    pf => pf.PfFiscalGroup,
                    pj => pj.PjFiscalGroup,
                    pr => pr.DefaultProduct));

                TimeSpan finalExpiration = TimeSpan.FromSeconds(Convert.ToInt32(AppSettings.FinalExpiration));
                _EstablishmentInfoCache.Set(keyCache, Results.Results.FirstOrDefault(), finalExpiration);
            }
            else
            {
                Results.PageSize = 10;
                Results.PageCount = 1;
                Results.RowCount = 1;
                Results.Results.Add(Estab);
            }

            if (Results == null)
                return InvalidRequest(null, _IGEEstablishmentsAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEEstablishmentsCreateVM pGEEstablishmentsVM)
        {
            _IGEEstablishmentsAS.Errors.Clear();
            pGEEstablishmentsVM.Id = id;
            pGEEstablishmentsVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEEstablishmentsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEEstablishmentsVM, ModelState);

            var Result = await _IGEEstablishmentsAS.UpdateEstablishmentAsync(pGEEstablishmentsVM);

            await _IGEEstablishmentsAS.SaveChanges();

            if (_IGEEstablishmentsAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEEstablishmentsAS.Errors);


            var keyCache = $"{Result.EstablishmentKey}-{Result.Id}";
            var keyCacheEstab = $"{Result.EstablishmentKey}-my-estab";

            _EstablishmentInfoCache.Remove(keyCache);
            _EstablishmentInfoCache.Remove(keyCacheEstab);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public IHttpActionResult Post(string pEstablishmentKey, [FromBody] GEEstablishmentsVM pGEEstablishmentsVM)
        {
            _IGEEstablishmentsAS.Add(Mapper.Map<GEEstablishmentsVM, GEEstablishments>(pGEEstablishmentsVM));

            var keyCache = $"{pEstablishmentKey}-{pGEEstablishmentsVM.Id}";
            var keyCacheEstab = $"{pEstablishmentKey}-my-estab";

            _EstablishmentInfoCache.Remove(keyCache);
            _EstablishmentInfoCache.Remove(keyCacheEstab);

            return InvalidRequest(null, _IGEEstablishmentsAS.Errors);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEEstablishmentsAS.Errors.Clear();
            var Result = Mapper.Map<GEEstablishments, GEEstablishmentsVM>(
                await _IGEEstablishmentsAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            if (_IGEEstablishmentsAS.Errors.Count > 0)
                return InvalidRequest(null, _IGEEstablishmentsAS.Errors);


            var keyCache = $"{pEstablishmentKey}-{pId}";
            var keyCacheEstab = $"{pEstablishmentKey}-my-estab";

            _EstablishmentInfoCache.Remove(keyCache);
            _EstablishmentInfoCache.Remove(keyCacheEstab);

            return RequestOK(Result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEEstablishmentsAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}