﻿using AutoMapper;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Hino.Salesforce.Infra.Cross.Utils.Files;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General
{
    [RoutePrefix("api/General/Files/Path/{pEstablishmentKey}")]
    public class FilesPathController : BaseController
    {
        private readonly IGEFilesPathAS _IGEFilesPathAS;
        public FilesPathController(IGEFilesPathAS pIGEFilesPathAS)
        {
            _IGEFilesPathAS = pIGEFilesPathAS;
        }

        [HttpPost]
        [Route("key/{pUniqueKey}/download")]
        public async Task<IHttpActionResult> PostDownloadProductFile(string pEstablishmentKey, string pUniqueKey)
        {
            _IGEFilesPathAS.Errors.Clear();

            string Result;
            try
            {
                Result = await _IGEFilesPathAS.DownloadFileAsync(pEstablishmentKey, pUniqueKey);

                if (_IGEFilesPathAS.Errors.Count > 0)
                    return InvalidRequest(Result, _IGEFilesPathAS.Errors);
            }
            catch (Exception ex)
            {
                return InvalidRequest(ex.Message, _IGEFilesPathAS.Errors);
            }

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("enterprise/key/{pUniqueKey}/id/{id}/upload")]
        [Route("product/key/{pUniqueKey}/id/{id}/upload")]
        [Route("orders/key/{pUniqueKey}/id/{id}/upload")]
        [Route("order/item/key/{pUniqueKey}/id/{id}/upload")]
        [Route("proposals/key/{pUniqueKey}/id/{id}/upload")]
        [Route("proposal/item/key/{pUniqueKey}/id/{id}/upload")]
        public async Task<IHttpActionResult> PostCreateFiles(string pEstablishmentKey, string pUniqueKey, long id)
        {
            _IGEFilesPathAS.Errors.Clear();
            // ValidateModelState(pGEProductsVM);
            // 
            // if (!ModelState.IsValid)
            //     return InvalidRequest(pGEProductsVM, ModelState);

            var Result = new GEResultUploadFileVM();
            try
            {
                var httpRequest = System.Web.HttpContext.Current.Request;

                if (httpRequest.Files.Count > 0)
                {
                    var url = httpRequest.Url.AbsolutePath.ToUploadFolder();

                    Result = await _IGEFilesPathAS.UploadFilesAsync(url, httpRequest.Files, new BaseVM()
                    {
                        UniqueKey = pUniqueKey,
                        EstablishmentKey = pEstablishmentKey,
                        Id = id
                    });

                    if (_IGEFilesPathAS.Errors.Count > 0)
                        return InvalidRequest(Result, _IGEFilesPathAS.Errors);
                }
            }
            catch (Exception ex)
            {
                return InvalidRequest(ex.Message, _IGEFilesPathAS.Errors);
            }

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("enterprise/key/{pUniqueKey}/id/{id}/remove")]
        [Route("product/key/{pUniqueKey}/id/{id}/remove")]
        [Route("orders/key/{pUniqueKey}/id/{id}/remove")]
        [Route("order/item/key/{pUniqueKey}/id/{id}/remove")]
        [Route("proposals/key/{pUniqueKey}/id/{id}/remove")]
        [Route("proposal/item/key/{pUniqueKey}/id/{id}/remove")]
        public async Task<IHttpActionResult> PostRemoveFile(string pEstablishmentKey, string pUniqueKey, long id, [FromBody] GEResulUploadItemVM pFile)
        {
            _IGEFilesPathAS.Errors.Clear();
            ValidateModelState(pFile);

            if (!ModelState.IsValid)
                return InvalidRequest(pFile, ModelState);

            GEResulUploadItemVM Result;
            try
            {
                var httpRequest = System.Web.HttpContext.Current.Request;

                var url = httpRequest.Url.AbsolutePath.ToUploadFolder();

                Result = await _IGEFilesPathAS.DeleteFileAsync(url, pEstablishmentKey, pUniqueKey, id, pFile);

                if (_IGEFilesPathAS.Errors.Any())
                    return InvalidRequest(Result, _IGEFilesPathAS.Errors);

            }
            catch (Exception ex)
            {
                return InvalidRequest(ex.Message, _IGEFilesPathAS.Errors);
            }

            return RequestOK(Result);
        }


        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<GEFilesPath>, PagedResult<GEFilesPathVM>>
                (
                await _IGEFilesPathAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IGEFilesPathAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] GEFilesPathVM[] syncDataVM)
        {
            _IGEFilesPathAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);

            var Result = await _IGEFilesPathAS.SyncData(pEstablishmentKey, Mapper.Map<GEFilesPathVM[], GEFilesPath[]>(syncDataVM));

            if (_IGEFilesPathAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEFilesPathAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var Results =
                Mapper.Map<PagedResult<GEFilesPath>, PagedResult<GEFilesPathVM>>
                (
                await _IGEFilesPathAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEFilesPathAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEFilesPath>, PagedResult<GEFilesPathVM>>
                (
                await _IGEFilesPathAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId));

            if (Results == null)
                return InvalidRequest(null, _IGEFilesPathAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEFilesPathVM pGEFilesPathVM)
        {
            _IGEFilesPathAS.Errors.Clear();
            pGEFilesPathVM.Id = id;
            pGEFilesPathVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEFilesPathVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEFilesPathVM, ModelState);

            var Result = _IGEFilesPathAS.Update(Mapper.Map<GEFilesPathVM, GEFilesPath>(pGEFilesPathVM));

            await _IGEFilesPathAS.SaveChanges();

            if (_IGEFilesPathAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEFilesPathAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEFilesPathVM pGEFilesPathVM)
        {
            _IGEFilesPathAS.Errors.Clear();
            ValidateModelState(pGEFilesPathVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEFilesPathVM, ModelState);

            var Result = _IGEFilesPathAS.Add(Mapper.Map<GEFilesPathVM, GEFilesPath>(pGEFilesPathVM));

            await _IGEFilesPathAS.SaveChanges();

            if (_IGEFilesPathAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEFilesPathAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEFilesPathAS.Errors.Clear();
            var Result = await _IGEFilesPathAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);
            await _IGEFilesPathAS.SaveChanges();

            if (Result != null && !_IGEFilesPathAS.Errors.Any())
                RequestOK(Mapper.Map<GEFilesPath, GEFilesPathVM>(Result));

            return RequestOK(Result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEFilesPathAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}