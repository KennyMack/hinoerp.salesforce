﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General.Kanban;
using Hino.Salesforce.Application.ViewModels.General.Kanban;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General.Kanban
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Director)]
    [RoutePrefix("api/General/Kanban/Settings/{pEstablishmentKey}")]
    public class SettingsController : BaseController
    {
        private readonly IGESettingsAS _IGESettingsAS;
        public SettingsController(IGESettingsAS pIGESettingsAS)
        {
            _IGESettingsAS = pIGESettingsAS;
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<GESettings>, PagedResult<GESettingsVM>>
                (
                await _IGESettingsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    GESettingsVM.GetDefaultFilter(pEstablishmentKey, filter)
                ));

            if (Results == null)
                return InvalidRequest(null, _IGESettingsAS.Errors);

            return RequestOK(Results);
        }

        [Route("Establishment/id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetByEstablishmentId(string pEstablishmentKey, long pId)
        {
            var Results = await _IGESettingsAS.GetByEstablishmentIdAsync(pEstablishmentKey, pId);

            if (Results == null)
                return InvalidRequest(null, _IGESettingsAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GESettings>, PagedResult<GESettingsVM>>
                (
                await _IGESettingsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGESettingsAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, [FromBody] GESettingsVM pGESettingsVM)
        {
            _IGESettingsAS.Errors.Clear();
            pGESettingsVM.Id = id;
            pGESettingsVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGESettingsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGESettingsVM, ModelState);

            var Result = await _IGESettingsAS.CreateOrChangeSettingAsync(pGESettingsVM);

            if (_IGESettingsAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGESettingsAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GESettingsVM pGESettingsVM)
        {
            _IGESettingsAS.Errors.Clear();
            pGESettingsVM.EstablishmentKey = pEstablishmentKey;
            ValidateModelState(pGESettingsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGESettingsVM, ModelState);

            var Result = await _IGESettingsAS.CreateOrChangeSettingAsync(pGESettingsVM);

            if (_IGESettingsAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGESettingsAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGESettingsAS.Errors.Clear();
            var Result = Mapper.Map<GESettings, GESettingsVM>(
                await _IGESettingsAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            return InvalidRequest(Result, _IGESettingsAS.Errors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGESettingsAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
