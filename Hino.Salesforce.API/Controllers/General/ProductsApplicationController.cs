﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Products/Application/{pEstablishmentKey}")]
    public class ProductsApplicationController : BaseController
    {
        private readonly IGEProductAplicAS _IGEProductAplicAS;
        public ProductsApplicationController(IGEProductAplicAS pIGEProductAplicAS)
        {
            _IGEProductAplicAS = pIGEProductAplicAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<GEProductAplic>, PagedResult<GEProductAplicVM>>
                (
                await _IGEProductAplicAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IGEProductAplicAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] GEProductAplicVM[] syncDataVM)
        {
            _IGEProductAplicAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);
            _IGEProductAplicAS.DontSendToQueue = true;
            var Result = await _IGEProductAplicAS.SyncData(pEstablishmentKey, Mapper.Map<GEProductAplicVM[], GEProductAplic[]>(syncDataVM));
            _IGEProductAplicAS.DontSendToQueue = false;
            if (_IGEProductAplicAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductAplicAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();

            /*
             r => r.EstablishmentKey == pEstablishmentKey &&
                   (
                     (
                        (r.Description.ToUpper().Contains(filter))
                     ) ||
                     (filter == "")
                   )
                )
            */

            var Results =
                Mapper.Map<PagedResult<GEProductAplic>, PagedResult<GEProductAplicVM>>
                (
                await _IGEProductAplicAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                GEProductAplicVM.GetDefaultFilter(pEstablishmentKey, filter)));

            if (Results != null)
                Results.Results = Results.Results.OrderBy(r => r.Description).ToList();

            if (Results == null)
                return InvalidRequest(null, _IGEProductAplicAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEProductAplic>, PagedResult<GEProductAplicVM>>
                (
                await _IGEProductAplicAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEProductAplicAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEProductAplicVM pGEProductsVM)
        {
            _IGEProductAplicAS.Errors.Clear();
            pGEProductsVM.Id = id;
            pGEProductsVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEProductsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEProductsVM, ModelState);

            var Result = _IGEProductAplicAS.Update(Mapper.Map<GEProductAplicVM, GEProductAplic>(pGEProductsVM));

            await _IGEProductAplicAS.SaveChanges();

            if (_IGEProductAplicAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductAplicAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEProductAplicVM pGEProductsVM)
        {
            _IGEProductAplicAS.Errors.Clear();
            ValidateModelState(pGEProductsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEProductsVM, ModelState);

            var Result = _IGEProductAplicAS.Add(Mapper.Map<GEProductAplicVM, GEProductAplic>(pGEProductsVM));

            await _IGEProductAplicAS.SaveChanges();

            if (_IGEProductAplicAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductAplicAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEProductAplicAS.Errors.Clear();
            var Result = Mapper.Map<GEProductAplic, GEProductAplicVM>(
                await _IGEProductAplicAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            return InvalidRequest(Result, _IGEProductAplicAS.Errors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEProductAplicAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}