﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Products/{pEstablishmentKey}")]
    public class ProductsController : BaseController
    {
        private readonly IGEProductsAS _IGEProductsAS;
        public ProductsController(IGEProductsAS pIGEProductsAS)
        {
            _IGEProductsAS = pIGEProductsAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<GEProducts>, PagedResult<GEProductsVM>>
                (
                await _IGEProductsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IGEProductsAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] GEProductsVM[] syncDataVM)
        {
            _IGEProductsAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);
            _IGEProductsAS.DontSendToQueue = true;
            var Result = await _IGEProductsAS.SyncDataProd(pEstablishmentKey, syncDataVM);
            _IGEProductsAS.DontSendToQueue = false;
            if (_IGEProductsAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductsAS.Errors);

            return RequestOK(Result);
        }

        [HttpPut]
        [Route("stock-balance")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> StockBalance(string pEstablishmentKey, GEProductsVM[] pGEProductsVM)
        {
            _IGEProductsAS.Errors.Clear();

            _IGEProductsAS.DontSendToQueue = true;
            var Result = await _IGEProductsAS.SyncStockBalance(pEstablishmentKey, Mapper.Map<GEProductsVM[], GEProducts[]>(pGEProductsVM));
            _IGEProductsAS.DontSendToQueue = false;

            if (_IGEProductsAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductsAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();
            var column = GetQueryColumn();
            var searchPosition = GetSearchPosition();
            var Results =
                Mapper.Map<PagedResult<GEProducts>, PagedResult<GEProductsVM>>
                (
                await _IGEProductsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                GEProductsVM.GetDefaultFilter(pEstablishmentKey, filter, column, searchPosition),
                s => s.GEProductsFamily,
                t => t.GEProductsType,
                g => g.GEProductsSaleUnit,
                l => l.GEProductsUnit,
                f => f.FSNCM,
                j => j.GEProductAplic
                ));

            if (Results != null)
                Results.Results = Results.Results.OrderBy(r => r.Description).ToList();

            if (Results == null)
                return InvalidRequest(null, _IGEProductsAS.Errors);

            return RequestOK(Results);
        }

        [Route("view")]
        [HttpGet]
        public async Task<IHttpActionResult> GetView(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();
            var column = GetQueryColumn();
            var searchPosition = GetSearchPosition();
            var Results =
                Mapper.Map<PagedResult<GEProducts>, PagedResult<GEMainProductsVM>>
                (
                    await _IGEProductsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    GEProductsVM.GetDefaultFilter(pEstablishmentKey, filter, column, searchPosition),
                    s => s.GEProductsFamily,
                    l => l.GEProductsUnit,
                    t => t.GEProductsType,
                    f => f.FSNCM
                ));

            if (Results != null)
                Results.Results = Results.Results.OrderBy(r => r.ProductKey).ToList();

            if (Results == null)
                return InvalidRequest(null, _IGEProductsAS.Errors);

            return RequestOK(Results);
        }

        [Route("search")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSearch(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();
            var column = GetQueryColumn();
            var searchPosition = GetSearchPosition();
            var Results =
                Mapper.Map<PagedResult<GEProducts>, PagedResult<GEProductsSearchVM>>
                (
                await _IGEProductsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                GEProductsVM.GetDefaultFilter(pEstablishmentKey, filter, column, searchPosition)
                ));

            if (Results != null)
                Results.Results = Results.Results.OrderBy(r => r.Description).ToList();

            if (Results == null)
                return InvalidRequest(null, _IGEProductsAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEProducts>, PagedResult<GEProductsVM>>
                (
                await _IGEProductsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey,
                s => s.GEProductsFamily,
                t => t.GEProductsType,
                g => g.GEProductsSaleUnit,
                l => l.GEProductsUnit,
                f => f.FSNCM,
                p => p.GEFilesPath,
                j => j.GEProductAplic));

            if (Results == null)
                return InvalidRequest(null, _IGEProductsAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEProductsVM pGEProductsVM)
        {
            _IGEProductsAS.Errors.Clear();
            pGEProductsVM.Id = id;
            pGEProductsVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEProductsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEProductsVM, ModelState);

            var Result = _IGEProductsAS.UpdateProduct(pGEProductsVM);

            await _IGEProductsAS.SaveChanges();

            if (_IGEProductsAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductsAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEProductsVM pGEProductsVM)
        {
            _IGEProductsAS.Errors.Clear();
            ValidateModelState(pGEProductsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEProductsVM, ModelState);

            var Result = await _IGEProductsAS.CreateProduct(pGEProductsVM);

            if (_IGEProductsAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductsAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("new/key")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> PostGenerateProductKey(string pEstablishmentKey, [FromBody] GEGenerateProductKeyVM pGEGenerateProductKeyVM)
        {
            _IGEProductsAS.Errors.Clear();
            ValidateModelState(pGEGenerateProductKeyVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEGenerateProductKeyVM, ModelState);

            var Result = await _IGEProductsAS.GenerateProductKeyAsync(pGEGenerateProductKeyVM);

            if (_IGEProductsAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductsAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEProductsAS.Errors.Clear();
            var Result = await _IGEProductsAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

            if (Result != null && !_IGEProductsAS.Errors.Any())
                RequestOK(Mapper.Map<GEProducts, GEProductsVM>(Result));

            return InvalidRequest(Result, _IGEProductsAS.Errors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEProductsAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}