﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Products/Family/{pEstablishmentKey}")]
    public class ProductsFamilyController : BaseController
    {
        private readonly IGEProductsFamilyAS _IGEProductsFamilyAS;
        public ProductsFamilyController(IGEProductsFamilyAS pIGEProductsFamilyAS)
        {
            _IGEProductsFamilyAS = pIGEProductsFamilyAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<GEProductsFamily>, PagedResult<GEProductsFamilyVM>>
                (
                await _IGEProductsFamilyAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IGEProductsFamilyAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] GEProductsFamilyVM[] syncDataVM)
        {
            _IGEProductsFamilyAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);
            _IGEProductsFamilyAS.DontSendToQueue = true;
            var Result = await _IGEProductsFamilyAS.SyncData(pEstablishmentKey, Mapper.Map<GEProductsFamilyVM[], GEProductsFamily[]>(syncDataVM));
            _IGEProductsFamilyAS.DontSendToQueue = false;
            if (_IGEProductsFamilyAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductsFamilyAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();

            /*
             r => r.EstablishmentKey == pEstablishmentKey &&
                   (
                     (
                        (r.Description.ToUpper().Contains(filter)) ||
                        (r.CatDescription.ToUpper().Contains(filter)) ||
                        (r.ClassDescription.ToUpper().Contains(filter)) ||
                        (r.GroupDescription.ToUpper().Contains(filter))
                     ) ||
                     (filter == "")
                   )
             */

            var Results =
                Mapper.Map<PagedResult<GEProductsFamily>, PagedResult<GEProductsFamilyVM>>
                (
                await _IGEProductsFamilyAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    GEProductsFamilyVM.GetDefaultFilter(pEstablishmentKey, filter)
                ));

            if (Results != null)
                Results.Results = Results.Results.OrderBy(r => r.Description).ToList();

            if (Results == null)
                return InvalidRequest(null, _IGEProductsFamilyAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEProductsFamily>, PagedResult<GEProductsFamilyVM>>
                (
                await _IGEProductsFamilyAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEProductsFamilyAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEProductsFamilyVM pGEProductsVM)
        {
            _IGEProductsFamilyAS.Errors.Clear();
            pGEProductsVM.Id = id;
            pGEProductsVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEProductsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEProductsVM, ModelState);

            var Result = _IGEProductsFamilyAS.Update(Mapper.Map<GEProductsFamilyVM, GEProductsFamily>(pGEProductsVM));

            await _IGEProductsFamilyAS.SaveChanges();

            if (_IGEProductsFamilyAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductsFamilyAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEProductsFamilyVM pGEProductsVM)
        {
            _IGEProductsFamilyAS.Errors.Clear();
            ValidateModelState(pGEProductsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEProductsVM, ModelState);

            var Result = _IGEProductsFamilyAS.Add(Mapper.Map<GEProductsFamilyVM, GEProductsFamily>(pGEProductsVM));

            await _IGEProductsFamilyAS.SaveChanges();

            if (_IGEProductsFamilyAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductsFamilyAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEProductsFamilyAS.Errors.Clear();
            var Result = Mapper.Map<GEProductsFamily, GEProductsFamilyVM>(
                await _IGEProductsFamilyAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            return InvalidRequest(Result, _IGEProductsFamilyAS.Errors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEProductsFamilyAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
