﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Products/Type/{pEstablishmentKey}")]
    public class ProductsTypeController : BaseController
    {
        private readonly IGEProductsTypeAS _IGEProductsTypeAS;
        public ProductsTypeController(IGEProductsTypeAS pIGEProductsTypeAS)
        {
            _IGEProductsTypeAS = pIGEProductsTypeAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<GEProductsType>, PagedResult<GEProductsTypeVM>>
                (
                await _IGEProductsTypeAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IGEProductsTypeAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] GEProductsTypeVM[] syncDataVM)
        {
            _IGEProductsTypeAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);
            _IGEProductsTypeAS.DontSendToQueue = true;
            var Result = await _IGEProductsTypeAS.SyncData(pEstablishmentKey, Mapper.Map<GEProductsTypeVM[], GEProductsType[]>(syncDataVM));
            _IGEProductsTypeAS.DontSendToQueue = false;
            if (_IGEProductsTypeAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductsTypeAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();

            /*
             r => r.EstablishmentKey == pEstablishmentKey &&
                   (
                     (
                        (r.Description.ToUpper().Contains(filter))
                     ) ||
                     (filter == "")
                   )
                )
             */

            var Results =
                Mapper.Map<PagedResult<GEProductsType>, PagedResult<GEProductsTypeVM>>
                (
                await _IGEProductsTypeAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    GEProductsTypeVM.GetDefaultFilter(pEstablishmentKey, filter)));

            if (Results != null)
                Results.Results = Results.Results.OrderBy(r => r.Description).ToList();

            if (Results == null)
                return InvalidRequest(null, _IGEProductsTypeAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEProductsType>, PagedResult<GEProductsTypeVM>>
                (
                await _IGEProductsTypeAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEProductsTypeAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEProductsTypeVM pGEProductsVM)
        {
            _IGEProductsTypeAS.Errors.Clear();
            pGEProductsVM.Id = id;
            pGEProductsVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEProductsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEProductsVM, ModelState);

            var Result = _IGEProductsTypeAS.Update(Mapper.Map<GEProductsTypeVM, GEProductsType>(pGEProductsVM));

            await _IGEProductsTypeAS.SaveChanges();

            if (_IGEProductsTypeAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductsTypeAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEProductsTypeVM pGEProductsVM)
        {
            _IGEProductsTypeAS.Errors.Clear();
            ValidateModelState(pGEProductsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEProductsVM, ModelState);

            var Result = _IGEProductsTypeAS.Add(Mapper.Map<GEProductsTypeVM, GEProductsType>(pGEProductsVM));

            await _IGEProductsTypeAS.SaveChanges();

            if (_IGEProductsTypeAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductsTypeAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEProductsTypeAS.Errors.Clear();
            var Result = Mapper.Map<GEProductsType, GEProductsTypeVM>(
                await _IGEProductsTypeAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            return InvalidRequest(Result, _IGEProductsTypeAS.Errors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEProductsTypeAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
