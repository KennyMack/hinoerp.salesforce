﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Products/Unit/{pEstablishmentKey}")]
    public class UnitController : BaseController
    {
        private readonly IGEProductsUnitAS _IGEProductsUnitAS;
        public UnitController(IGEProductsUnitAS pIGEProductsUnitAS)
        {
            _IGEProductsUnitAS = pIGEProductsUnitAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<GEProductsUnit>, PagedResult<GEProductsUnitVM>>
                (
                await _IGEProductsUnitAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IGEProductsUnitAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        [IgnoreValidateEstablishmentKey]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] GEProductsUnitVM[] syncDataVM)
        {
            _IGEProductsUnitAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);
            _IGEProductsUnitAS.DontSendToQueue = true;
            var Result = await _IGEProductsUnitAS.SyncData(pEstablishmentKey, Mapper.Map<GEProductsUnitVM[], GEProductsUnit[]>(syncDataVM));
            _IGEProductsUnitAS.DontSendToQueue = false;
            if (_IGEProductsUnitAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductsUnitAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();

            /*
             r => 
                   (
                     (
                        (r.Description.ToUpper().Contains(filter)) ||
                        (r.Unit.ToUpper().Contains(filter))
                     ) ||
                     (filter == "")
                   )
                )
             */

            var Results =
                Mapper.Map<PagedResult<GEProductsUnit>, PagedResult<GEProductsUnitVM>>(
                    await _IGEProductsUnitAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    GEProductsUnitVM.GetDefaultFilter(pEstablishmentKey, filter))
                );

            if (Results != null)
                Results.Results = Results.Results.OrderBy(r => r.Unit).ToList();

            if (Results == null)
                return InvalidRequest(null, _IGEProductsUnitAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEProductsUnit>, PagedResult<GEProductsUnitVM>>
                (
                await _IGEProductsUnitAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId));

            if (Results == null)
                return InvalidRequest(null, _IGEProductsUnitAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, GEProductsUnitVM pGEProductsUnitVM)
        {
            _IGEProductsUnitAS.Errors.Clear();
            pGEProductsUnitVM.Id = id;
            pGEProductsUnitVM.UniqueKey = pUniqueKey;

            ValidateModelState(pGEProductsUnitVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEProductsUnitVM, ModelState);

            var Result = _IGEProductsUnitAS.Update(Mapper.Map<GEProductsUnitVM, GEProductsUnit>(pGEProductsUnitVM));

            await _IGEProductsUnitAS.SaveChanges();

            if (_IGEProductsUnitAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductsUnitAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEProductsUnitVM pGEProductsUnitVM)
        {
            _IGEProductsUnitAS.Errors.Clear();
            ValidateModelState(pGEProductsUnitVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEProductsUnitVM, ModelState);

            var Result = _IGEProductsUnitAS.Add(Mapper.Map<GEProductsUnitVM, GEProductsUnit>(pGEProductsUnitVM));

            await _IGEProductsUnitAS.SaveChanges();

            if (_IGEProductsUnitAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEProductsUnitAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEProductsUnitAS.Errors.Clear();
            var Result = Mapper.Map<GEProductsUnit, GEProductsUnitVM>(
                await _IGEProductsUnitAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            return InvalidRequest(Result, _IGEProductsUnitAS.Errors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEProductsUnitAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
