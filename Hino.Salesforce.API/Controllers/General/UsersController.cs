﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.General
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/General/Users/{pEstablishmentKey}")]
    public class UsersController : BaseController
    {
        private readonly IGEUsersAS _IGEUsersAS;
        public UsersController(IGEUsersAS pIGEUsersAS)
        {
            _IGEUsersAS = pIGEUsersAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<GEUsers>, PagedResult<GEUsersEditVM>>
                (
                await _IGEUsersAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IGEUsersAS.Errors);

            return RequestOK(Results);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();
            /*
             r => r.EstablishmentKey == pEstablishmentKey
            */
            var Results =
                Mapper.Map<PagedResult<GEUsers>, PagedResult<GEUsersEditVM>>
                (
                await _IGEUsersAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                GEUsersVM.GetDefaultFilter(pEstablishmentKey, filter)));

            if (Results == null)
                return InvalidRequest(null, _IGEUsersAS.Errors);

            return RequestOK(Results);
        }

        [Route("all/Sellers")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSellers(string pEstablishmentKey)
        {
            var Results =
                Mapper.Map<PagedResult<GEUsers>, PagedResult<GEUsersEditVM>>
                (
                await _IGEUsersAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    r => r.EstablishmentKey == pEstablishmentKey &&
                    (r.UserType == Infra.Cross.Utils.EUserType.Representant ||
                     r.UserType == Infra.Cross.Utils.EUserType.Sallesman ||
                     r.UserType == Infra.Cross.Utils.EUserType.Director ||
                     r.UserType == Infra.Cross.Utils.EUserType.Accredited ||
                     r.UserType == Infra.Cross.Utils.EUserType.Master)
                ));

            if (Results == null)
                return InvalidRequest(null, _IGEUsersAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<GEUsers>, PagedResult<GEUsersEditVM>>
                (
                await _IGEUsersAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IGEUsersAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pEstablishmentKey, string pUniqueKey, long id, [FromBody] GEUsersVM pGEUsersVM)
        {
            _IGEUsersAS.Errors.Clear();
            pGEUsersVM.Id = id;
            pGEUsersVM.UniqueKey = pUniqueKey;
            pGEUsersVM.EstablishmentKey = pEstablishmentKey;

            pGEUsersVM.Email = pGEUsersVM.Email.ToLower();
            ValidateModelState(pGEUsersVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEUsersVM, ModelState);

            var Result = _IGEUsersAS.UpdateUser(Mapper.Map<GEUsersVM, GEUsers>(pGEUsersVM));

            await _IGEUsersAS.SaveChanges();

            if (_IGEUsersAS.Errors.Count > 0)
                return InvalidRequest(Result, _IGEUsersAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] GEUsersVM pGEUsersVM)
        {
            _IGEUsersAS.Errors.Clear();
            ValidateModelState(pGEUsersVM);

            pGEUsersVM.Email = pGEUsersVM.Email.ToLower();

            if (!ModelState.IsValid)
                return InvalidRequest(pGEUsersVM, ModelState);

            var User = _IGEUsersAS.Add(Mapper.Map<GEUsersVM, GEUsers>(pGEUsersVM));

            await _IGEUsersAS.SaveChanges();

            if (_IGEUsersAS.Errors.Count > 0)
                return InvalidRequest(User, _IGEUsersAS.Errors);

            return RequestOK(User);
        }

        [HttpPost]
        [Route("mobile/create")]
        [IgnoreAuthJWT]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> CreateUsermobile(string pEstablishmentKey, [FromBody] GEUsersCreateVM pGEUsersVM)
        {
            _IGEUsersAS.Errors.Clear();
            ValidateModelState(pGEUsersVM);

            pGEUsersVM.Email = pGEUsersVM.Email.ToLower();
            if (!ModelState.IsValid)
                return InvalidRequest(pGEUsersVM, ModelState);

            await _IGEUsersAS.CreateUserAsync(pGEUsersVM);

            if (_IGEUsersAS.Errors.Count > 0)
                return InvalidRequest(pGEUsersVM, _IGEUsersAS.Errors);

            return RequestOK(pGEUsersVM);
        }

        [HttpPost]
        [Route("client/create")]
        [IgnoreAuthJWT]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> CreateUserClient(string pEstablishmentKey, [FromBody] GEUsersCreateVM pGEUsersVM)
        {
            _IGEUsersAS.Errors.Clear();
            ValidateModelState(pGEUsersVM);

            pGEUsersVM.Email = pGEUsersVM.Email.ToLower();
            if (!ModelState.IsValid)
                return InvalidRequest(pGEUsersVM, ModelState);

            await _IGEUsersAS.CreateUserAsync(pGEUsersVM);

            if (_IGEUsersAS.Errors.Count > 0)
                return InvalidRequest(pGEUsersVM, _IGEUsersAS.Errors);

            return RequestOK(pGEUsersVM);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IGEUsersAS.Errors.Clear();
            await Task.Delay(1);
            _IGEUsersAS.Errors.Add(new ModelException
            {
                ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                Field = "Id",
                Value = "0",
                Messages = new string[]
                {
                    Infra.Cross.Resources.ErrorMessagesResource.RemoveNotAllowed
                }
            });

            return InvalidRequest(null, _IGEUsersAS.Errors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IGEUsersAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}