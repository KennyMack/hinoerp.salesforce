﻿using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.Route;
using Hino.Salesforce.Application.ViewModels.Route;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Geocoding
{
    [RoutePrefix("api/Geocoding")]
    public class AddressController : BaseController
    {
        private readonly IMapBox _IMapBox;

        public AddressController(IMapBox pIMapBox)
        {
            _IMapBox = pIMapBox;
        }

        [HttpPost]
        [Route("Address")]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Post([FromBody] AddressGeoVM pAddressVM)
        {
            _IMapBox.Errors.Clear();
            ValidateModelState(pAddressVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pAddressVM, ModelState);

            var result = await _IMapBox.GetAddressAsync(pAddressVM);

            if (result == null)
                return InvalidRequest(pAddressVM, _IMapBox.Errors);

            return RequestOK(result);
        }
    }
}
