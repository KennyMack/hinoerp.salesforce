﻿using Hino.Salesforce.Application.Interfaces.Logistics;
using Hino.Salesforce.Application.ViewModels.Route;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Hino.Salesforce.API.Controllers
{
    public class MapController : Controller
    {
        /*private readonly ILORoutesAS _ILORoutesAS;
        public MapController(ILORoutesAS pILORoutesAS)
        {
            _ILORoutesAS = pILORoutesAS;
        }*/

        [HttpPost]
        public ActionResult Post([System.Web.Http.FromBody] OptimizedTripResultVM pOptimized)
        {
            var RouteData = new MapOptimizedVM();

            foreach (var item in pOptimized.waypoints)
                RouteData.points.Add(new MapPointVM
                {
                    type = "Feature",
                    geometry = new MapGeometryVM
                    {
                        type = "Point",
                        coordinates = item.location
                    }
                });

            RouteData.route = new MapRouteGeometryVM
            {
                type = "LineString",
                coordinates = pOptimized.trips[0].geometry.coordinates
            };


            ViewBag.RouteData = RouteData;

            return View();
        }

        // GET: Map
        public async Task<ActionResult> Index(string pEstablishmentKey, long pId)
        {
            var _ILORoutesAS = (ILORoutesAS)Startup.DependencyResolver.GetService(typeof(ILORoutesAS));
            var result = await _ILORoutesAS
                .GetMapJsonAsync(pEstablishmentKey, pId);

            if (result == null)
                return HttpNotFound("Mapa não encontrado.");

            ViewBag.Features = result.features;
            ViewBag.Coordinates = result.coordinates;

            return View();
        }
    }
}