﻿using Hino.Salesforce.API.Reports.Sales;
using System.IO;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Hino.Salesforce.API.Controllers.Reports
{
    public class ReportSalesController : Controller
    {
        public async Task<ActionResult> PrintOrder(string pEstablishmentKey, long pId)
        {
            if (pEstablishmentKey == "f48c8bd0-a048-4ae4-a988-75b791fdd80b" ||
                pEstablishmentKey == "b421e29f-d411-4cf6-a74d-6db9de4fe6c8")
            {
                using (var report = new API.Reports.Sales.Custom.Holec.ReportSaleOrder(pEstablishmentKey, pId))
                {
                    await report.LoadData();
                    using (MemoryStream ms = new MemoryStream())
                    {
                        report.ExportToPdf(ms);
                        return File(ms.ToArray(), "application/pdf", "PedidoVenda.pdf");
                    }
                }
            }
            else
            {
                using (var report = new ReportSaleOrder(pEstablishmentKey, pId))
                {
                    await report.LoadData();
                    using (MemoryStream ms = new MemoryStream())
                    {
                        report.ExportToPdf(ms);
                        return File(ms.ToArray(), "application/pdf", "PedidoVenda.pdf");
                    }
                }
            }
        }
    }
}