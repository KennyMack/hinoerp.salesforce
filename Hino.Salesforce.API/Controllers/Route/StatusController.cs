﻿using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.Route;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Route
{
    [RoutePrefix("api/Router")]
    public class StatusController : BaseController
    {
        private readonly IRouteXL _IRouteXL;

        public StatusController(IRouteXL pIRouteXL)
        {
            _IRouteXL = pIRouteXL;
        }

        [HttpGet]
        [Route("Status")]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Get()
        {
            _IRouteXL.Errors.Clear();
            var result = await _IRouteXL.GetStatus();

            if (!result)
                return InvalidRequest(null, _IRouteXL.Errors);

            return RequestOK(true);
        }
    }
}
