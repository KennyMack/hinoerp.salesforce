﻿using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.Logistics;
using Hino.Salesforce.Application.Interfaces.Route;
using Hino.Salesforce.Application.ViewModels.Route;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Route
{
    [RoutePrefix("api/Router")]
    public class TourController : BaseController
    {
        private readonly IRouteXL _IRouteXL;
        private readonly IMapBox _IMapBox;
        private readonly IHereMaps _IHereMaps;
        private readonly ILOWaypointsAS _ILOWaypointsAS;

        public TourController(IRouteXL pIRouteXL, IMapBox pIMapBox, IHereMaps pIHereMaps,
            ILOWaypointsAS pILOWaypointsAS)
        {
            _IRouteXL = pIRouteXL;
            _IMapBox = pIMapBox;
            _IHereMaps = pIHereMaps;
            _ILOWaypointsAS = pILOWaypointsAS;
        }

        [HttpPost]
        [Route("Tour")]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> Post()
        {
            _IRouteXL.Errors.Clear();
            var result = await _IRouteXL.PostTour();

            if (_IRouteXL.Errors.Count > 0)
                return InvalidRequest(result, _IRouteXL.Errors);

            return RequestOK(result);
        }

        [HttpPost]
        [Route("Optimized")]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> PostOptimized([FromBody] OptimizedVM pLocations)
        {
            _IMapBox.Errors.Clear();
            var result = await _IMapBox.OptimizedLocationsAsync(pLocations);

            if (_IMapBox.Errors.Count > 0)
                return InvalidRequest(result, _IMapBox.Errors);

            return RequestOK(result);
        }

        [HttpPost]
        [Route("Find/Sequence")]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> FindSequence([FromBody] OptimizedVM pLocations)
        {
            _IHereMaps.Errors.Clear();
            var result = await _IHereMaps.FindSequenceAsync(pLocations);

            if (_IHereMaps.Errors.Count > 0)
                return InvalidRequest(result, _IHereMaps.Errors);

            return RequestOK(result);
        }

        [HttpGet]
        [Route("{pEstablishmentKey}/Waypoints/{pId}")]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> FindWaypoints(string pEstablishmentKey, long pId)
        {
            var Results =
                    await _ILOWaypointsAS.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                    r.RouteID == pId);

            if (Results == null)
                return InvalidRequest(null, _ILOWaypointsAS.Errors);

            if (!Results.Any())
                return NotFoundRequest();

            return RequestOK(Results);
        }
    }
}
