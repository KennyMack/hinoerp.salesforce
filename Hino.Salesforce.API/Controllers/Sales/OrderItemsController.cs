﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Sales
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/Sales/Orders/Items/{pEstablishmentKey}")]
    public class OrderItemsController : BaseController
    {
        private readonly IVEOrderItemsAS _IVEOrderItemsAS;
        public OrderItemsController(IVEOrderItemsAS pIVEOrderItemsAS)
        {
            _IVEOrderItemsAS = pIVEOrderItemsAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<VEOrderItems>, PagedResult<VEOrderItemsVM>>
                (
                await _IVEOrderItemsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date,
                p => p.GEProducts,
                o => o.VEOrders));

            if (Results == null)
                return InvalidRequest(null, _IVEOrderItemsAS.Errors);

            return RequestOK(Results);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var Results =
                Mapper.Map<PagedResult<VEOrderItems>, PagedResult<VEOrderItemsVM>>
                (
                await _IVEOrderItemsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey,
                p => p.GEProducts,
                o => o.VEOrders));

            if (Results == null)
                return InvalidRequest(null, _IVEOrderItemsAS.Errors);

            return RequestOK(Results);
        }


        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<VEOrderItems>, PagedResult<VEOrderItemsVM>>
                (
                await _IVEOrderItemsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey,
                p => p.GEProducts,
                o => o.VEOrders));

            if (Results == null)
                return InvalidRequest(null, _IVEOrderItemsAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, VEOrderItemsVM pVEOrderItemsVM)
        {
            _IVEOrderItemsAS.Errors.Clear();
            pVEOrderItemsVM.Id = id;
            pVEOrderItemsVM.UniqueKey = pUniqueKey;

            ValidateModelState(pVEOrderItemsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVEOrderItemsVM, ModelState);

            var Result = _IVEOrderItemsAS.Update(Mapper.Map<VEOrderItemsVM, VEOrderItems>(pVEOrderItemsVM));

            await _IVEOrderItemsAS.SaveChanges();

            if (_IVEOrderItemsAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVEOrderItemsAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] VEOrderItemsVM pVEOrderItemsVM)
        {
            _IVEOrderItemsAS.Errors.Clear();
            ValidateModelState(pVEOrderItemsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVEOrderItemsVM, ModelState);

            var Result = _IVEOrderItemsAS.Add(Mapper.Map<VEOrderItemsVM, VEOrderItems>(pVEOrderItemsVM));

            await _IVEOrderItemsAS.SaveChanges();

            if (_IVEOrderItemsAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVEOrderItemsAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IVEOrderItemsAS.Errors.Clear();
            var OrderItems = Mapper.Map<VEOrderItems, VEOrderItemsVM>(
                await _IVEOrderItemsAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            await _IVEOrderItemsAS.SaveChanges();

            if (_IVEOrderItemsAS.Errors.Count > 0)
                return InvalidRequest(null, _IVEOrderItemsAS.Errors);

            return RequestOK(OrderItems);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IVEOrderItemsAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}