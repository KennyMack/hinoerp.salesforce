﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Sales
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/Sales/Orders/{pEstablishmentKey}")]
    public class OrdersController : BaseController
    {
        private readonly IVEOrdersAS _IVEOrdersAS;
        public OrdersController(IVEOrdersAS pIVEOrdersAS)
        {
            _IVEOrdersAS = pIVEOrdersAS;
        }

        [Route("user/{pUserId}/sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, long pUserId, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<VEOrders>, PagedResult<VEOrdersVM>>
                (
                await _IVEOrdersAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.UserID == pUserId &&
                r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<VEOrders>, PagedResult<VEOrdersVM>>
                (
                await _IVEOrdersAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Results);
        }

        [Route("push/new")]
        [HttpGet]
        public async Task<IHttpActionResult> GetPushNew(string pEstablishmentKey)
        {
            var Results =
                Mapper.Map<PagedResult<VEOrders>, PagedResult<VEOrdersVM>>
                (
                await _IVEOrdersAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.StatusSinc == Infra.Cross.Utils.Enums.EStatusSinc.Sincronized));

            if (Results == null)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] VEOrdersVM[] syncDataVM)
        {
            _IVEOrdersAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);
            _IVEOrdersAS.DontSendToQueue = true;
            var Result = await _IVEOrdersAS.SyncData(pEstablishmentKey, Mapper.Map<VEOrdersVM[], VEOrders[]>(syncDataVM));
            _IVEOrdersAS.DontSendToQueue = false;
            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVEOrdersAS.Errors);

            return RequestOK(Result);
        }

        [Route("sync/erp")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncERPData(string pEstablishmentKey, [FromBody] VEOrdersVM[] syncDataVM)
        {
            _IVEOrdersAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);
            _IVEOrdersAS.DontSendToQueue = true;
            var Result = await _IVEOrdersAS.SyncERPData(pEstablishmentKey, Mapper.Map<VEOrdersVM[], VEOrders[]>(syncDataVM));
            _IVEOrdersAS.DontSendToQueue = false;
            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVEOrdersAS.Errors);

            return RequestOK(Result);
        }
        
        // OK
        [Route("all/proposal")]
        [HttpGet]
        public async Task<IHttpActionResult> GetProposal(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<VEOrders>, PagedResult<VEOrdersVM>>
                (
                await _IVEOrdersAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    VEOrdersVM.GetDefaultFilter(pEstablishmentKey, filter, true)
                ));

            if (Results == null)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Results);
        }

        [Route("view/proposal")]
        [HttpGet]
        public async Task<IHttpActionResult> GetViewProposals(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<VEOrders>, PagedResult<VEMainOrdersVM>>
                (
                await _IVEOrdersAS.QueryViewAsync(GetPageNumber(), GetLimitNumber(),
                    VEOrdersVM.GetDefaultFilter(pEstablishmentKey, filter, true)
                ));

            if (Results == null)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Results);
        }

        //ok
        [Route("view/proposal/user/{pUserId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetProposalByUserId(string pEstablishmentKey, long pUserId)
        {
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<VEOrders>, PagedResult<VEMainOrdersVM>>
                (
                await _IVEOrdersAS.QueryViewAsync(GetPageNumber(), GetLimitNumber(),
                    VEOrdersVM.GetDefaultFilterByUser(pEstablishmentKey, filter, pUserId, true)
                ));

            if (Results == null)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Results);
        }

        //ok
        [Route("all/orders")]
        [HttpGet]
        public async Task<IHttpActionResult> GetOrders(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<VEOrders>, PagedResult<VEOrdersVM>>
                (
                await _IVEOrdersAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    VEOrdersVM.GetDefaultFilter(pEstablishmentKey, filter, false)
                ));

            if (Results == null)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Results);
        }

        [Route("view/orders")]
        [HttpGet]
        public async Task<IHttpActionResult> GetViewOrders(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<VEOrders>, PagedResult<VEMainOrdersVM>>
                (
                await _IVEOrdersAS.QueryViewAsync(GetPageNumber(), GetLimitNumber(),
                    VEOrdersVM.GetDefaultFilter(pEstablishmentKey, filter, false)
                ));

            if (Results == null)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Results);
        }

        //ok
        [Route("view/orders/user/{pUserId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetOrdersByUserId(string pEstablishmentKey, long pUserId)
        {
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<VEOrders>, PagedResult<VEMainOrdersVM>>
                (
                await _IVEOrdersAS.QueryViewAsync(GetPageNumber(), GetLimitNumber(),
                    VEOrdersVM.GetDefaultFilterByUser(pEstablishmentKey, filter, pUserId, false)
                ));

            if (Results == null)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Results);
        }

        //ok
        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<VEOrders>, PagedResult<VEOrdersVM>>
                (
                    await _IVEOrdersAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                        VEOrdersVM.GetDefaultFilterAll(pEstablishmentKey, filter)
                ));

            if (Results == null)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Results);
        }

        // ok
        [Route("enterprise/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetByEnterpriseId(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<VEOrders>, PagedResult<VEOrdersVM>>
                (
                    await _IVEOrdersAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    r => r.EnterpriseID == pId &&
                    r.EstablishmentKey == pEstablishmentKey
                ));

            if (Results == null)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var resp = Mapper.Map<VEOrders, VEOrdersVM>(await _IVEOrdersAS.GetByIdAsync(pId, pEstablishmentKey, ""));

            if (resp == null)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            var Results = new PagedResult<VEOrdersVM>
            {
                CurrentPage = 1,
                PageCount = 1,
                PageSize = 30,
                RowCount = 1
            };
            Results.Results.Add(resp);

            if (!Results.Results.Any())
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, VEOrdersVM pVEOrdersVM)
        {
            _IVEOrdersAS.Errors.Clear();
            pVEOrdersVM.Id = id;
            pVEOrdersVM.UniqueKey = pUniqueKey;

            ValidateModelState(pVEOrdersVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVEOrdersVM, ModelState);

            var Result = await _IVEOrdersAS.UpdateOrder(pVEOrdersVM);

            // await _IVEOrdersAS.SaveChanges();

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVEOrdersAS.Errors);

            return RequestOK(Result);
        }
        //ok
        [Route("id/{id}/convert")]
        [HttpGet]
        public async Task<IHttpActionResult> GetCanConvert(string pEstablishmentKey, long id)
        {
            _IVEOrdersAS.Errors.Clear();

            var Order = await _IVEOrdersAS.CanConvertToOrderAsync(pEstablishmentKey, id);

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(Order, _IVEOrdersAS.Errors);

            return RequestOK(Order);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] VEOrdersVM pVEOrdersVM)
        {
            _IVEOrdersAS.Errors.Clear();
            ValidateModelState(pVEOrdersVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVEOrdersVM, ModelState);

            var Order = await _IVEOrdersAS.CreateOrder(pVEOrdersVM);

            //await _IVEOrdersAS.SaveChanges();

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(Order, _IVEOrdersAS.Errors);

            return RequestOK(Order);
        }

        [HttpPost]
        [Route("key/{pUniqueKey}/id/{id}/new/revision")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> PostNewRevision(string pUniqueKey, long id, [FromBody] VEOrderStatusVM pVEOrdersVM)
        {
            _IVEOrdersAS.Errors.Clear();
            ValidateModelState(pVEOrdersVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVEOrdersVM, ModelState);

            var Order = await _IVEOrdersAS.GenerateRevisionAsync(pVEOrdersVM);

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(Order, _IVEOrdersAS.Errors);

            return RequestOK(Order);
        }

        [HttpPost]
        [Route("key/{pUniqueKey}/id/{id}/new/copy")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> PostNewCopy(string pUniqueKey, long id, [FromBody] VEOrderStatusVM pVEOrdersVM)
        {
            _IVEOrdersAS.Errors.Clear();
            ValidateModelState(pVEOrdersVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVEOrdersVM, ModelState);

            var Order = await _IVEOrdersAS.CopyOrderAsync(pVEOrdersVM);

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(Order, _IVEOrdersAS.Errors);

            return RequestOK(Order);
        }

        [HttpPost]
        [Route("key/{pUniqueKey}/id/{id}/status")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> PostStatus(string pUniqueKey, long id, [FromBody] VEOrderStatusVM pVEOrdersVM)
        {
            _IVEOrdersAS.Errors.Clear();
            ValidateModelState(pVEOrdersVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVEOrdersVM, ModelState);

            var Order = await _IVEOrdersAS.ChangeStatusAsync(pVEOrdersVM);

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(Order, _IVEOrdersAS.Errors);

            return RequestOK(Order);
        }

        //ok
        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IVEOrdersAS.Errors.Clear();
            var Result = Mapper.Map<VEOrders, VEOrdersVM>(
                await _IVEOrdersAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Result);
        }

        [HttpGet]
        [Route("Resume/User/{pUserId}/Pending/Payment")]
        public IHttpActionResult GetResumePedingPayment(string pEstablishmentKey, long pUserId)
        {
            _IVEOrdersAS.Errors.Clear();

            DateTime dateToCheck = DateTime.Now.AddHours(24);

            var Result = _IVEOrdersAS.CountResults(r => r.EstablishmentKey == pEstablishmentKey &&
                                                            r.UserID == pUserId &&
                                                            !r.IsProposal &&
                                                            r.StatusPay != Infra.Cross.Utils.Enums.EStatusPay.Total &&
                                                            !r.GEPaymentType.Boleto &&                                                             
                                                            r.PaymentDueDate <= dateToCheck);
            PaymentPendingVM paymentPendingVM = new PaymentPendingVM
            {
                HasPaymentPending = Result > 0
            };

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(paymentPendingVM);
        }

        [HttpGet]
        [Route("Resume/User/{pUserId}/Pending/Proposals")]
        public IHttpActionResult GetResumePedingProposals(string pEstablishmentKey, long pUserId)
        {
            _IVEOrdersAS.Errors.Clear();

            var Result = _IVEOrdersAS.CountResults(r => r.EstablishmentKey == pEstablishmentKey &&
                                                            r.UserID == pUserId &&
                                                            r.IsProposal &&
                                                            r.Status == "P");

            ProposalsPendingVM proposalsPendingVM = new ProposalsPendingVM
            {
                Quantity = Result
            };

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(proposalsPendingVM);
        }        

        [HttpPost]
        [Route("Resume/Commission/Product")]
        public async Task<IHttpActionResult> GetCommissionByProduct(string pEstablishmentKey, [FromBody] DefaultSalesResumeFilterModel SalesFilterModel)
        {
            _IVEOrdersAS.Errors.Clear();

            ValidateModelState(SalesFilterModel);

            if (!ModelState.IsValid)
                return InvalidRequest(SalesFilterModel, ModelState);

            var filter = GetQueryFilter();

            var Result = await _IVEOrdersAS.GetCommissionByProduct(GetPageNumber(),
                                                                   GetLimitNumber(),
                                                                   pEstablishmentKey,
                                                                   SalesFilterModel.UserId,
                                                                   SalesFilterModel.InitialDate,
                                                                   SalesFilterModel.FinalDate,
                                                                   filter);

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("Resume/Commission/Detailed")]
        public async Task<IHttpActionResult> GetDetailedCommission(string pEstablishmentKey, [FromBody] DefaultSalesResumeFilterModel SalesFilterModel)
        {
            _IVEOrdersAS.Errors.Clear();

            ValidateModelState(SalesFilterModel);

            if (!ModelState.IsValid)
                return InvalidRequest(SalesFilterModel, ModelState);

            var filter = GetQueryFilter();

            var Result = await _IVEOrdersAS.GetDetailedCommission(GetPageNumber(),
                                                                  GetLimitNumber(),
                                                                  pEstablishmentKey,
                                                                  SalesFilterModel.UserId,
                                                                  SalesFilterModel.InitialDate,
                                                                  SalesFilterModel.FinalDate,
                                                                  filter);

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("Resume/Total/Commission")]
        public async Task<IHttpActionResult> GetTotalCommission(string pEstablishmentKey, [FromBody] DefaultSalesResumeFilterModel SalesFilterModel)
        {
            _IVEOrdersAS.Errors.Clear();

            ValidateModelState(SalesFilterModel);

            if (!ModelState.IsValid)
                return InvalidRequest(SalesFilterModel, ModelState);

            var Result = await _IVEOrdersAS.GetTotalCommission(pEstablishmentKey,
                                                               SalesFilterModel.UserId,
                                                               SalesFilterModel.InitialDate,
                                                               SalesFilterModel.FinalDate);

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("Resume/Sales/Product")]
        public async Task<IHttpActionResult> GetSalesByProduct(string pEstablishmentKey, [FromBody] DefaultSalesResumeFilterModel SalesFilterModel)
        {
            _IVEOrdersAS.Errors.Clear();

            ValidateModelState(SalesFilterModel);

            if (!ModelState.IsValid)
                return InvalidRequest(SalesFilterModel, ModelState);

            var filter = GetQueryFilter();

            var Result = await _IVEOrdersAS.GetSalesByProduct(GetPageNumber(),
                                                              GetLimitNumber(),
                                                              pEstablishmentKey,
                                                              SalesFilterModel.UserId,
                                                              SalesFilterModel.InitialDate,
                                                              SalesFilterModel.FinalDate,
                                                              filter);

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("Resume/Sales/Month")]
        public async Task<IHttpActionResult> GetSalesByMonth(string pEstablishmentKey, [FromBody] DefaultSalesResumeFilterModel SalesFilterModel)
        {
            _IVEOrdersAS.Errors.Clear();

            ValidateModelState(SalesFilterModel);

            if (!ModelState.IsValid)
                return InvalidRequest(SalesFilterModel, ModelState);

            var filter = GetQueryFilter();

            var Result = await _IVEOrdersAS.GetSalesByMonth(GetPageNumber(),
                                                            GetLimitNumber(),
                                                            pEstablishmentKey,
                                                            SalesFilterModel.UserId,
                                                            SalesFilterModel.InitialDate,
                                                            SalesFilterModel.FinalDate,
                                                            filter);

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("Resume/Detailed")]
        public async Task<IHttpActionResult> DetailedSales(string pEstablishmentKey, [FromBody] DefaultSalesResumeFilterModel SalesFilterModel)
        {
            _IVEOrdersAS.Errors.Clear();

            ValidateModelState(SalesFilterModel);

            if (!ModelState.IsValid)
                return InvalidRequest(SalesFilterModel, ModelState);

            var filter = GetQueryFilter();

            var Result = await _IVEOrdersAS.GetDetailedSales(GetPageNumber(),
                                                            GetLimitNumber(),
                                                            pEstablishmentKey,
                                                            SalesFilterModel.UserId,
                                                            SalesFilterModel.InitialDate,
                                                            SalesFilterModel.FinalDate,
                                                            filter);

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Result);
        }

        [HttpGet]
        [Route("Resume/User/{pUserId}/Limit")]
        public async Task<IHttpActionResult> DetailedSalesLimit(string pEstablishmentKey, Int32 pUserId)
        {
            _IVEOrdersAS.Errors.Clear();

            var filter = GetQueryFilter();

            var Result = await _IVEOrdersAS.GetSalesLimit(GetPageNumber(),
                                                          GetLimitNumber(),
                                                          pEstablishmentKey,
                                                          pUserId,
                                                          filter);

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("Resume/Total/Sales")]
        public async Task<IHttpActionResult> GetTotalSales(string pEstablishmentKey, [FromBody] DefaultSalesResumeFilterModel SalesFilterModel)
        {
            _IVEOrdersAS.Errors.Clear();

            ValidateModelState(SalesFilterModel);

            if (!ModelState.IsValid)
                return InvalidRequest(SalesFilterModel, ModelState);

            var Result = await _IVEOrdersAS.GetTotalSales(pEstablishmentKey,
                                                          SalesFilterModel.UserId,
                                                          SalesFilterModel.InitialDate,
                                                          SalesFilterModel.FinalDate);

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Result);
        }

        [HttpGet]
        [Route("key/{pUniqueKey}/id/{id}/Resume/Stock/Balance")]
        public async Task<IHttpActionResult> GetOrderStockBalanceAsync(string pEstablishmentKey, string pUniqueKey, long id)
        {
            _IVEOrdersAS.Errors.Clear();

            var Result = await _IVEOrdersAS.GetOrderStockBalanceAsync(pEstablishmentKey, pUniqueKey, id);

            if (_IVEOrdersAS.Errors.Count > 0)
                return InvalidRequest(null, _IVEOrdersAS.Errors);

            return RequestOK(Result);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IVEOrdersAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}