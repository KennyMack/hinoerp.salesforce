﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Sales
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/Sales/Region/{pEstablishmentKey}")]
    public class RegionSaleController : BaseController
    {
        private readonly IVERegionSaleAS _IVERegionSaleAS;
        public RegionSaleController(IVERegionSaleAS pIVERegionSaleAS)
        {
            _IVERegionSaleAS = pIVERegionSaleAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<VERegionSale>, PagedResult<VERegionSaleVM>>
                (
                await _IVERegionSaleAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IVERegionSaleAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] VERegionSaleVM[] syncDataVM)
        {
            _IVERegionSaleAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);
            _IVERegionSaleAS.DontSendToQueue = true;
            var Result = await _IVERegionSaleAS.SyncData(pEstablishmentKey, Mapper.Map<VERegionSaleVM[], VERegionSale[]>(syncDataVM));
            _IVERegionSaleAS.DontSendToQueue = false;
            if (_IVERegionSaleAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVERegionSaleAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var Results =
                Mapper.Map<PagedResult<VERegionSale>, PagedResult<VERegionSaleVM>>
                (
                await _IVERegionSaleAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey,
                s => s.VERegionSaleUF));

            if (Results == null)
                return InvalidRequest(null, _IVERegionSaleAS.Errors);

            return RequestOK(Results);
        }


        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<VERegionSale>, PagedResult<VERegionSaleVM>>
                (
                await _IVERegionSaleAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IVERegionSaleAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, VERegionSaleVM pVERegionSaleVM)
        {
            _IVERegionSaleAS.Errors.Clear();
            pVERegionSaleVM.Id = id;
            pVERegionSaleVM.UniqueKey = pUniqueKey;

            ValidateModelState(pVERegionSaleVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVERegionSaleVM, ModelState);

            var Result = _IVERegionSaleAS.Update(Mapper.Map<VERegionSaleVM, VERegionSale>(pVERegionSaleVM));

            await _IVERegionSaleAS.SaveChanges();

            if (_IVERegionSaleAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVERegionSaleAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] VERegionSaleVM pVERegionSaleVM)
        {
            _IVERegionSaleAS.Errors.Clear();
            ValidateModelState(pVERegionSaleVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVERegionSaleVM, ModelState);

            var Result = _IVERegionSaleAS.Add(Mapper.Map<VERegionSaleVM, VERegionSale>(pVERegionSaleVM));

            await _IVERegionSaleAS.SaveChanges();

            if (_IVERegionSaleAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVERegionSaleAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IVERegionSaleAS.Errors.Clear();
            var RegionSale = Mapper.Map<VERegionSale, VERegionSaleVM>(
                await _IVERegionSaleAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            await _IVERegionSaleAS.SaveChanges();

            if (_IVERegionSaleAS.Errors.Count > 0)
                return InvalidRequest(null, _IVERegionSaleAS.Errors);

            return RequestOK(RegionSale);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IVERegionSaleAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
