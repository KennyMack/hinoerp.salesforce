﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Sales
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/Sales/Region/UF/{pEstablishmentKey}")]
    public class RegionSaleUFController : BaseController
    {
        private readonly IVERegionSaleUFAS _IVERegionSaleUFAS;
        public RegionSaleUFController(IVERegionSaleUFAS pIVERegionSaleUFAS)
        {
            _IVERegionSaleUFAS = pIVERegionSaleUFAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);

            var Results =
                Mapper.Map<PagedResult<VERegionSaleUF>, PagedResult<VERegionSaleUFVM>>
                (
                await _IVERegionSaleUFAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Modified >= date));

            if (Results == null)
                return InvalidRequest(null, _IVERegionSaleUFAS.Errors);

            return RequestOK(Results);
        }

        [Route("sync")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] VERegionSaleUFVM[] syncDataVM)
        {
            _IVERegionSaleUFAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);
            _IVERegionSaleUFAS.DontSendToQueue = true;
            var Result = await _IVERegionSaleUFAS.SyncData(pEstablishmentKey, Mapper.Map<VERegionSaleUFVM[], VERegionSaleUF[]>(syncDataVM));
            _IVERegionSaleUFAS.DontSendToQueue = false;
            if (_IVERegionSaleUFAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVERegionSaleUFAS.Errors);

            return RequestOK(Result);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var Results =
                Mapper.Map<PagedResult<VERegionSaleUF>, PagedResult<VERegionSaleUFVM>>
                (
                await _IVERegionSaleUFAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey,
                s => s.VERegionSale));

            if (Results == null)
                return InvalidRequest(null, _IVERegionSaleUFAS.Errors);

            return RequestOK(Results);
        }


        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<VERegionSaleUF>, PagedResult<VERegionSaleUFVM>>
                (
                await _IVERegionSaleUFAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey,
                s => s.VERegionSale));

            if (Results == null)
                return InvalidRequest(null, _IVERegionSaleUFAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, VERegionSaleUFVM pVERegionSaleUFVM)
        {
            _IVERegionSaleUFAS.Errors.Clear();
            pVERegionSaleUFVM.VERegionSale = null;
            pVERegionSaleUFVM.Id = id;
            pVERegionSaleUFVM.UniqueKey = pUniqueKey;

            ValidateModelState(pVERegionSaleUFVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVERegionSaleUFVM, ModelState);

            var Result = await _IVERegionSaleUFAS.UpdateSalePrice(pVERegionSaleUFVM);

            // await _IVERegionSaleUFAS.SaveChanges();

            if (_IVERegionSaleUFAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVERegionSaleUFAS.Errors);

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] VERegionSaleUFVM pVERegionSaleUFVM)
        {
            _IVERegionSaleUFAS.Errors.Clear();
            pVERegionSaleUFVM.VERegionSale = null;

            ValidateModelState(pVERegionSaleUFVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVERegionSaleUFVM, ModelState);

            var Result = await _IVERegionSaleUFAS.CreateSalePrice(pVERegionSaleUFVM);

            // await _IVERegionSaleUFAS.SaveChanges();

            if (_IVERegionSaleUFAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVERegionSaleUFAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IVERegionSaleUFAS.Errors.Clear();
            var RegionSale = Mapper.Map<VERegionSaleUF, VERegionSaleUFVM>(
                await _IVERegionSaleUFAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            await _IVERegionSaleUFAS.SaveChanges();

            if (_IVERegionSaleUFAS.Errors.Count > 0)
                return InvalidRequest(null, _IVERegionSaleUFAS.Errors);

            return RequestOK(RegionSale);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IVERegionSaleUFAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
