﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Sales
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/Sales/Prices/{pEstablishmentKey}")]
    public class SalePriceController : BaseController
    {
        private readonly IVESalePriceAS _IVESalePriceAS;
        private readonly IGEPaymentConditionAS _IGEPaymentConditionAS;
        public SalePriceController(IVESalePriceAS pIVESalePriceAS, IGEPaymentConditionAS pIGEPaymentConditionAS)
        {
            _IVESalePriceAS = pIVESalePriceAS;
            _IGEPaymentConditionAS = pIGEPaymentConditionAS;
        }

        [Route("sync/date/{pDate}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetSync(string pEstablishmentKey, string pDate)
        {
            var date = ConvertDateUrl(pDate);
            try
            {
                var Results =
                 Mapper.Map<PagedResult<VESalePrice>, PagedResult<VESalePriceVM>>
                 (
                    await _IVESalePriceAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                            r => r.EstablishmentKey == pEstablishmentKey &&
                            r.Modified >= date,
                            s => s.VERegionSale));
                return RequestOK(Results);
            }
            catch (System.Exception e)
            {
                if (_IVESalePriceAS.Errors.Count <= 0)
                    return InvalidRequest(null, e.Message);
            }

            return InvalidRequest(null, _IVESalePriceAS.Errors);
        }

        [Route("sync")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] VESalePriceVM[] syncDataVM)
        {
            _IVESalePriceAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);
            _IVESalePriceAS.DontSendToQueue = true;
            var Result = await _IVESalePriceAS.SyncData(pEstablishmentKey, Mapper.Map<VESalePriceVM[], VESalePrice[]>(syncDataVM));
            _IVESalePriceAS.DontSendToQueue = false;
            if (_IVESalePriceAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVESalePriceAS.Errors);

            return RequestOK(Result);
        }

        [Route("by/erp")]
        [HttpGet]
        public async Task<IHttpActionResult> GetByERP(string pEstablishmentKey)
        {
            var Results =
                Mapper.Map<PagedResult<VESalePrice>, PagedResult<VESalePriceVM>>
                (
                    await _IVESalePriceAS.GetListCodPRVendaAsync(pEstablishmentKey)
                );

            if (Results == null)
                return InvalidRequest(null, _IVESalePriceAS.Errors);

            return RequestOK(Results);
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var filter = GetQueryFilter();

            var Results =
                Mapper.Map<PagedResult<VESalePrice>, PagedResult<VESalePriceVM>>
                (
                await _IVESalePriceAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                VESalePriceVM.GetDefaultFilter(pEstablishmentKey, filter),
                s => s.VERegionSale,
                p => p.GEProducts));

            if (Results == null)
                return InvalidRequest(null, _IVESalePriceAS.Errors);

            return RequestOK(Results);
        }

        [Route("region/{pRegionId}/payCondition/{pPayConditionId}/user/{pUserId}/search")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllRegionPriceSearch(string pEstablishmentKey, long pRegionId, long pPayConditionId, long pUserId)
        {
            var filter = GetQueryFilter();
            var searchPosition = GetSearchPosition();

            PagedResult<VESalePriceVM> Results = null;

            if (!filter.IsEmpty())
                Results = await _IVESalePriceAS.GetAllSalePrice(GetPageNumber(), GetLimitNumber(),
                                                                pEstablishmentKey, pRegionId, 0,
                                                                pPayConditionId, pUserId,
                                                                filter, searchPosition);

            if (Results == null)
                return InvalidRequest(null, _IVESalePriceAS.Errors);

            return RequestOK(Results);
        }

        [Route("list/{pCodPrVenda}/payCondition/{pPayConditionId}/user/{pUserId}/search")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllListaPriceSearch(string pEstablishmentKey, long pCodPrVenda, long pPayConditionId, long pUserId)
        {
            var filter = GetQueryFilter();
            var searchPosition = GetSearchPosition();

            PagedResult<VESalePriceVM> Results = null;

            if (!filter.IsEmpty())
                Results = await _IVESalePriceAS.GetAllSalePrice(GetPageNumber(), GetLimitNumber(),
                                                                pEstablishmentKey, 0, pCodPrVenda, pPayConditionId, pUserId,
                                                                filter, searchPosition);

            if (Results == null)
                return InvalidRequest(null, _IVESalePriceAS.Errors);

            return RequestOK(Results);
        }

        [Route("region/{pRegionId}/payCondition/{pPayConditionId}/user/{pUserId}/all")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllRegionPrice(string pEstablishmentKey, long pRegionId, long pPayConditionId, long pUserId)
        {
            var filter = GetQueryFilter();
            var searchPosition = GetSearchPosition();

            var Results = await _IVESalePriceAS.GetAllSalePrice(GetPageNumber(), GetLimitNumber(),
                                                                pEstablishmentKey, pRegionId, 0, pPayConditionId, pUserId,
                                                                filter, searchPosition);

            if (Results == null)
                return InvalidRequest(null, _IVESalePriceAS.Errors);

            return RequestOK(Results);
        }

        [Route("list/{pCodPrVenda}/payCondition/{pPayConditionId}/user/{pUserId}/all")]
        [HttpGet]
        public async Task<IHttpActionResult> GetAllListPrice(string pEstablishmentKey, long pCodPrVenda, long pPayConditionId, long pUserId)
        {
            var filter = GetQueryFilter();
            var searchPosition = GetSearchPosition();

            var Results = await _IVESalePriceAS.GetAllSalePrice(GetPageNumber(), GetLimitNumber(),
                                                                pEstablishmentKey, 0, pCodPrVenda, pPayConditionId, pUserId,
                                                                filter, searchPosition);

            if (Results == null)
                return InvalidRequest(null, _IVESalePriceAS.Errors);

            return RequestOK(Results);
        }

        [Route("region/{pRegionId}/payCondition/{pPayConditionId}/user/{pUserId}/product/{pProductId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetByRegionAndProductId(string pEstablishmentKey, long pRegionId, long pPayConditionId, long pUserId, long pProductId)
        {
            var Result = await _IVESalePriceAS.GetProductSalePrice(pEstablishmentKey, pProductId, pRegionId, 0, pPayConditionId, pUserId);

            if (Result == null && _IVESalePriceAS.Errors.Any())
                return InvalidRequest(null, _IVESalePriceAS.Errors);

            if (Result == null)
                return NotFoundRequest();

            return RequestOK(Result);
        }

        [Route("list/{pCodPrVenda}/payCondition/{pPayConditionId}/user/{pUserId}/product/{pProductId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetByListAndProductId(string pEstablishmentKey, long pCodPrVenda, long pPayConditionId, long pUserId, long pProductId)
        {
            var Result = await _IVESalePriceAS.GetProductSalePrice(pEstablishmentKey, pProductId, 0, pCodPrVenda, pPayConditionId, pUserId);

            if (Result == null && _IVESalePriceAS.Errors.Any())
                return InvalidRequest(null, _IVESalePriceAS.Errors);

            if (Result == null)
                return NotFoundRequest();

            return RequestOK(Result);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<VESalePrice>, PagedResult<VESalePriceVM>>
                (
                await _IVESalePriceAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey,
                s => s.VERegionSale,
                p => p.GEProducts));

            if (Results == null)
                return InvalidRequest(null, _IVESalePriceAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, VESalePriceVM pVESalePriceVM)
        {
            _IVESalePriceAS.Errors.Clear();
            pVESalePriceVM.GEProducts = null;
            pVESalePriceVM.VERegionSale = null;
            pVESalePriceVM.Id = id;
            pVESalePriceVM.UniqueKey = pUniqueKey;

            ValidateModelState(pVESalePriceVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVESalePriceVM, ModelState);

            var Result = _IVESalePriceAS.UpdateSalePrice(pVESalePriceVM);

            await _IVESalePriceAS.SaveChanges();

            if (_IVESalePriceAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVESalePriceAS.Errors);

            await _IVESalePriceAS.GenerateEntryQueueAsync();

            return RequestOK(Result);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] VESalePriceVM pVESalePriceVM)
        {
            _IVESalePriceAS.Errors.Clear();
            pVESalePriceVM.GEProducts = null;
            pVESalePriceVM.VERegionSale = null;
            ValidateModelState(pVESalePriceVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVESalePriceVM, ModelState);

            var Result = _IVESalePriceAS.CreateSalePrice(pVESalePriceVM);

            await _IVESalePriceAS.SaveChanges();

            if (_IVESalePriceAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVESalePriceAS.Errors);

            await _IVESalePriceAS.GenerateEntryQueueAsync();

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IVESalePriceAS.Errors.Clear();
            var SalePrice = Mapper.Map<VESalePrice, VESalePriceVM>(
                await _IVESalePriceAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            await _IVESalePriceAS.SaveChanges();

            if (_IVESalePriceAS.Errors.Count > 0)
                return InvalidRequest(null, _IVESalePriceAS.Errors);

            await _IVESalePriceAS.GenerateEntryQueueAsync();

            return RequestOK(SalePrice);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IVESalePriceAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
