﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Sales
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/Sales/Work/Region/{pEstablishmentKey}")]
    public class SaleWorkRegionController : BaseController
    {
        private readonly IVESaleWorkRegionAS _IVESaleWorkRegionAS;
        public SaleWorkRegionController(IVESaleWorkRegionAS pIVESaleWorkRegionAS)
        {
            _IVESaleWorkRegionAS = pIVESaleWorkRegionAS;
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();

            /*
             r => r.EstablishmentKey == pEstablishmentKey &&
                   (
                     (
                        (r.Id.ToString().Contains(filter)) ||
                        (r.ZIPCodeEnd.ToUpper().Contains(filter)) ||
                        (r.ZIPCodeStart.ToUpper().Contains(filter))
                     ) ||
                     (filter == "")
                   )
            */

            var Results =
                Mapper.Map<PagedResult<VESaleWorkRegion>, PagedResult<VESaleWorkRegionVM>>
                (
                await _IVESaleWorkRegionAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    VESaleWorkRegionVM.GetDefaultFilter(pEstablishmentKey, filter)
                ));

            if (Results == null)
                return InvalidRequest(null, _IVESaleWorkRegionAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<VESaleWorkRegion>, PagedResult<VESaleWorkRegionVM>>
                (
                await _IVESaleWorkRegionAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IVESaleWorkRegionAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, VESaleWorkRegionVM pVESaleWorkRegionVM)
        {
            _IVESaleWorkRegionAS.Errors.Clear();
            pVESaleWorkRegionVM.Id = id;
            pVESaleWorkRegionVM.UniqueKey = pUniqueKey;

            ValidateModelState(pVESaleWorkRegionVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVESaleWorkRegionVM, ModelState);

            var Enterprise = _IVESaleWorkRegionAS.Update(Mapper.Map<VESaleWorkRegionVM, VESaleWorkRegion>(pVESaleWorkRegionVM));

            if (_IVESaleWorkRegionAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IVESaleWorkRegionAS.Errors);
            else
                await _IVESaleWorkRegionAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] VESaleWorkRegionVM pVESaleWorkRegionVM)
        {
            _IVESaleWorkRegionAS.Errors.Clear();
            ValidateModelState(pVESaleWorkRegionVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVESaleWorkRegionVM, ModelState);

            var Enterprise = _IVESaleWorkRegionAS.Add(Mapper.Map<VESaleWorkRegionVM, VESaleWorkRegion>(pVESaleWorkRegionVM));

            if (_IVESaleWorkRegionAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IVESaleWorkRegionAS.Errors);
            else
                await _IVESaleWorkRegionAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IVESaleWorkRegionAS.Errors.Clear();
            var Result = await _IVESaleWorkRegionAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

            if (Result != null && !_IVESaleWorkRegionAS.Errors.Any())
            {
                await _IVESaleWorkRegionAS.SaveChanges();
                return RequestOK(Mapper.Map<VESaleWorkRegion, VESaleWorkRegionVM>(Result));
            }

            return InvalidRequest(Result, _IVESaleWorkRegionAS.Errors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IVESaleWorkRegionAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
