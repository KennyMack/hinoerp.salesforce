﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.Interfaces.Sales.Transactions;
using Hino.Salesforce.Application.ViewModels.Sales.Transactions;
using Hino.Salesforce.Infra.Cross.Entities.Sales.Transactions;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Sales.Transactions
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/Sales/Transactions/{pEstablishmentKey}")]
    public class TransactionsController : BaseController
    {
        private readonly IVETransactionsAS _IVETransactionsAS;
        private readonly ICapptaApiAS _ICapptaApiAS;
        private readonly IVEOrdersAS _IVEOrderAS;

        public TransactionsController(IVETransactionsAS pIVETransactionsAS,
                                      ICapptaApiAS pICapptaApiAS,
                                      IVEOrdersAS pIVEOrderAS)
        {
            _IVETransactionsAS = pIVETransactionsAS;
            _ICapptaApiAS = pICapptaApiAS;
            _IVEOrderAS = pIVEOrderAS;
        }

        [HttpGet]
        [Route("all")]
        public async Task<IHttpActionResult> GetAllTransactions(string pEstablishmentKey)
        {
            var Results = Mapper.Map<PagedResult<VETransactions>, PagedResult<VETransactionsVM>>
                (
                    await _IVETransactionsAS.QueryPagedAsync(GetPageNumber(),
                                                             GetLimitNumber(),
                                                             r => r.EstablishmentKey == pEstablishmentKey)
                );

            if (Results == null)
                return InvalidRequest(null, _IVETransactionsAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<VETransactions>, PagedResult<VETransactionsVM>>
                (
                await _IVETransactionsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IVETransactionsAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [Route("authCode/{pAuthCode}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetByAuthCode(string pEstablishmentKey, string pAuthCode)
        {
            var Results =
                Mapper.Map<PagedResult<VETransactions>, PagedResult<VETransactionsVM>>
                (
                    await _IVETransactionsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                                                             r => r.AuthCode == pAuthCode &&
                                                             r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IVETransactionsAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [Route("terminal/{pTerminalId}/nsuHost/{pNsuHost}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetByNsuHost(string pEstablishmentKey, string pTerminalId, string pNsuHost)
        {
            var Results =
                Mapper.Map<PagedResult<VETransactions>, PagedResult<VETransactionsVM>>
                (
                    await _IVETransactionsAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                                                             r => r.NSUHost == pNsuHost &&
                                                                  r.TerminalId == pTerminalId &&
                                                                  r.EstablishmentKey == pEstablishmentKey)
                );
            
            /*
            if (Results == null || Results.Results.Count <= 0)
                return InvalidRequest(null, _IVETransactionsAS.Errors);
            
            if (Results.Results.Count <= 0)
                return NotFoundRequest();
            */

            return RequestOK(Results);
        }

        [HttpGet]
        [Route("Order/{pOrderID}/all")]
        public async Task<IHttpActionResult> GetOrderTransactions(string pEstablishmentKey, long pOrderID)
        {
            var Results = Mapper.Map<PagedResult<VETransactions>, PagedResult<VETransactionsVM>>
                (
                    await _IVETransactionsAS.QueryPagedAsync(GetPageNumber(),
                                                             GetLimitNumber(),
                                                             r => r.EstablishmentKey == pEstablishmentKey &&
                                                                  r.OrderID == pOrderID)
                );

            if (Results == null)
                return InvalidRequest(null, _IVETransactionsAS.Errors);

            return RequestOK(Results);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post([FromBody] VETransactionsVM pVETransactionsVM)
        {
            _IVETransactionsAS.Errors.Clear();
            ValidateModelState(pVETransactionsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVETransactionsVM, ModelState);

            var Result = await _IVETransactionsAS.CreateTransaction(pVETransactionsVM);

            if (_IVETransactionsAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVETransactionsAS.Errors);

            return RequestOK(Result);
        }

        [Route("sync/erp")]
        [HttpPost]
        public virtual async Task<IHttpActionResult> PostSyncData(string pEstablishmentKey, [FromBody] VETransactionsVM[] syncDataVM)
        {
            _IVETransactionsAS.Errors.Clear();
            ValidateModelState(syncDataVM);

            if (!ModelState.IsValid)
                return InvalidRequest(syncDataVM, ModelState);
            _IVETransactionsAS.DontSendToQueue = true;
            var Result = await _IVETransactionsAS.SyncData(pEstablishmentKey, Mapper.Map<VETransactionsVM[], VETransactions[]>(syncDataVM));
            _IVETransactionsAS.DontSendToQueue = false;
            if (_IVETransactionsAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVETransactionsAS.Errors);

            return RequestOK(Result);
        }

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{pId}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long pId, [FromBody] VETransactionsVM pVETransactionsVM)
        {
            _IVETransactionsAS.Errors.Clear();
            pVETransactionsVM.Id = pId;
            pVETransactionsVM.UniqueKey = pUniqueKey;

            ValidateModelState(pVETransactionsVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVETransactionsVM, ModelState);

            var Result = await _IVETransactionsAS.UpdateTransaction(pVETransactionsVM);

            if (_IVETransactionsAS.Errors.Count > 0)
                return InvalidRequest(Result, _IVETransactionsAS.Errors);

            return RequestOK(Result);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId/*, [FromBody] VETransactionsVM pVETransactionsVM*/)
        {
            _IVETransactionsAS.Errors.Clear();
            var Result = Mapper.Map<VETransactions, VETransactionsVM>(
                await _IVETransactionsAS.RemoveById(pId, pEstablishmentKey, pUniqueKey));

            if (_IVETransactionsAS.Errors.Count > 0)
                return InvalidRequest(null, _IVETransactionsAS.Errors);

            await _IVETransactionsAS.SaveChanges();
            return RequestOK(Result);
        }

        [HttpPost]
        [Route("order/{pOrderId}/search/{pAuthCode}")]
        //[EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Search(string pEstablishmentKey, long pOrderId, string pAuthCode)
        {
            _ICapptaApiAS.Errors.Clear();

            if ((await _IVETransactionsAS.QueryAsync(r => r.NSU == pAuthCode && r.EstablishmentKey == pEstablishmentKey)).Any())
                return InvalidRequest(null, Infra.Cross.Resources.ErrorMessagesResource.CreateNotAllowed);

            var capptaTransaction = await _ICapptaApiAS.GetTransactionAsync(pAuthCode);

            if (_ICapptaApiAS.Errors.Any())
                return InvalidRequest(pAuthCode, _ICapptaApiAS.Errors);

            if (capptaTransaction != null)
            {
                var order = (await _IVEOrderAS.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                               r.Id == pOrderId)).FirstOrDefault();

                if (capptaTransaction.installments != order.GEPaymentCondition.Installments)
                    return InvalidRequest(pAuthCode, Hino.Salesforce.Infra.Cross.Resources.ValidationMessagesResource.PaymentDiffer);

                VETransactions transaction = new VETransactions()
                {
                    AcquirerName = capptaTransaction.acquirer,
                    AdminCode = capptaTransaction.administrativeCode,
                    Amount = (capptaTransaction.amountInCent / 100),
                    AuthCode = capptaTransaction.acquirerAuthorizationCode,
                    AuthDate = capptaTransaction.date,
                    CardBrand = capptaTransaction.cardBrand,
                    CardLastDigits = capptaTransaction.cardNumber.Substring(capptaTransaction.cardNumber.Length - 4, 4),
                    Description = capptaTransaction.paymentMethod,
                    EstablishmentKey = pEstablishmentKey,
                    Installments = capptaTransaction.installments,
                    NSU = capptaTransaction.uniqueSequentialNumber,
                    OrderID = pOrderId,
                    Reversed = false,
                    Status = Infra.Cross.Utils.Enums.ETransactionStatus.Done,
                    Origin = Infra.Cross.Utils.Enums.ETransactionOrigin.Pos
                };

                if (capptaTransaction.paymentMethod.IndexOf("Débito") >= 0)
                    transaction.Type = Infra.Cross.Utils.Enums.ETransactionType.Debit;
                else if (capptaTransaction.paymentMethod.IndexOf("Crédito") >= 0)
                    transaction.Type = Infra.Cross.Utils.Enums.ETransactionType.Credit;
                else
                    transaction.Type = Infra.Cross.Utils.Enums.ETransactionType.Reversal;

                _IVETransactionsAS.Add(transaction);
                var Result = await _IVETransactionsAS.SaveChanges();

                if (_IVETransactionsAS.Errors.Count > 0)
                    return InvalidRequest(Result, _IVETransactionsAS.Errors);

                return RequestOK(transaction);
            }

            return InvalidRequest(pAuthCode, Infra.Cross.Resources.ValidationMessagesResource.InvalidValue);

        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
                _IVETransactionsAS.Dispose();

            base.Dispose(disposing);

        }
    }
}