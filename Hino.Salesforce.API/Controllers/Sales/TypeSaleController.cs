﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Sales
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/Sales/Type/{pEstablishmentKey}")]
    public class TypeSaleController : BaseController
    {
        private readonly IVETypeSaleAS _IVETypeSaleAS;
        public TypeSaleController(IVETypeSaleAS pIVETypeSaleAS)
        {
            _IVETypeSaleAS = pIVETypeSaleAS;
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var Results =
                Mapper.Map<PagedResult<VETypeSale>, PagedResult<VETypeSaleVM>>
                (
                await _IVETypeSaleAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IVETypeSaleAS.Errors);

            return RequestOK(Results);
        }


        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<VETypeSale>, PagedResult<VETypeSaleVM>>
                (
                await _IVETypeSaleAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                r => r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey));

            if (Results == null)
                return InvalidRequest(null, _IVETypeSaleAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IVETypeSaleAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
