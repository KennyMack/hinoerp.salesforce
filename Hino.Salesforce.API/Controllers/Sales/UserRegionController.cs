﻿using AutoMapper;
using Hino.Salesforce.API.Attributes;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers.Sales
{
    [AuthJWT(Infra.Cross.Utils.Enums.ERoles.Assistant)]
    [RoutePrefix("api/Sales/User/Region/{pEstablishmentKey}")]
    public class UserRegionController : BaseController
    {
        private readonly IVEUserRegionAS _IVEUserRegionAS;
        public UserRegionController(IVEUserRegionAS pIVEUserRegionAS)
        {
            _IVEUserRegionAS = pIVEUserRegionAS;
        }

        [Route("all")]
        [HttpGet]
        public async Task<IHttpActionResult> Get(string pEstablishmentKey)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();

            /*
              r => r.EstablishmentKey == pEstablishmentKey &&
                   (
                     (
                        (r.Id.ToString().Contains(filter)) ||
                        (r.UserId.ToString().Contains(filter)) ||
                        (r.SaleWorkId.ToString().Contains(filter))
                     ) ||
                     (filter == "")
                   )
             */

            var Results =
                Mapper.Map<PagedResult<VEUserRegion>, PagedResult<VEUserRegionVM>>
                (
                await _IVEUserRegionAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    VEUserRegionVM.GetDefaultFilter(pEstablishmentKey, filter),
                s => s.GEUsers,
                t => t.VESaleWorkRegion));

            if (Results == null)
                return InvalidRequest(null, _IVEUserRegionAS.Errors);

            return RequestOK(Results);
        }

        [Route("user/{pUserId}/all")]
        [HttpGet]
        public async Task<IHttpActionResult> GetByUser(string pEstablishmentKey, long pUserId)
        {
            var compareInfo = CultureInfo.InvariantCulture.CompareInfo;
            var filter = GetQueryFilter();

            /*
             r => r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                   (
                     (
                        (r.Id.ToString().Contains(filter)) ||
                        (r.UserId.ToString().Contains(filter)) ||
                        (r.SaleWorkId.ToString().Contains(filter))
                     ) ||
                     (filter == "")
                   )
            */

            var Results =
                Mapper.Map<PagedResult<VEUserRegion>, PagedResult<VEUserRegionVM>>
                (
                await _IVEUserRegionAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    VEUserRegionVM.GetDefaultFilterByUser(pEstablishmentKey, filter, pUserId),
                s => s.GEUsers,
                t => t.VESaleWorkRegion));

            if (Results == null)
                return InvalidRequest(null, _IVEUserRegionAS.Errors);

            return RequestOK(Results);
        }

        [Route("id/{pId}")]
        [HttpGet]
        public async Task<IHttpActionResult> GetById(string pEstablishmentKey, long pId)
        {
            var Results =
                Mapper.Map<PagedResult<VEUserRegion>, PagedResult<VEUserRegionVM>>
                (
                await _IVEUserRegionAS.QueryPagedAsync(GetPageNumber(), GetLimitNumber(),
                    r => r.Id == pId &&
                    r.EstablishmentKey == pEstablishmentKey,
                s => s.GEUsers,
                t => t.VESaleWorkRegion));

            if (Results == null)
                return InvalidRequest(null, _IVEUserRegionAS.Errors);

            if (Results.Results.Count <= 0)
                return NotFoundRequest();

            return RequestOK(Results);
        }

        [Route("user/{pUser}/cep/{pZipCode}")]
        [HttpGet]
        public IHttpActionResult GetExistsZipCode(string pEstablishmentKey, long pUser, string pZipCode) =>
            RequestOK(_IVEUserRegionAS.ZipCodeExists(pEstablishmentKey, pUser, pZipCode));

        [HttpPut]
        [Route("key/{pUniqueKey}/id/{id}/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.UPDATE)]
        public async Task<IHttpActionResult> Put(string pUniqueKey, long id, VEUserRegionVM pVEUserRegionVM)
        {
            _IVEUserRegionAS.Errors.Clear();
            pVEUserRegionVM.Id = id;
            pVEUserRegionVM.UniqueKey = pUniqueKey;

            ValidateModelState(pVEUserRegionVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVEUserRegionVM, ModelState);

            var Enterprise = _IVEUserRegionAS.Update(Mapper.Map<VEUserRegionVM, VEUserRegion>(pVEUserRegionVM));

            if (_IVEUserRegionAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IVEUserRegionAS.Errors);
            else
                await _IVEUserRegionAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpPost]
        [Route("save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> Post(string pEstablishmentKey, [FromBody] VEUserRegionVM pVEUserRegionVM)
        {
            _IVEUserRegionAS.Errors.Clear();
            ValidateModelState(pVEUserRegionVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pVEUserRegionVM, ModelState);

            var Enterprise = _IVEUserRegionAS.Add(Mapper.Map<VEUserRegionVM, VEUserRegion>(pVEUserRegionVM));

            if (_IVEUserRegionAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IVEUserRegionAS.Errors);
            else
                await _IVEUserRegionAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpPost]
        [Route("list/save")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.CREATE)]
        public async Task<IHttpActionResult> PostList(string pEstablishmentKey, [FromBody] VEUserRegionListVM pGEUserEnterprisesVM)
        {
            _IVEUserRegionAS.Errors.Clear();
            ValidateModelState(pGEUserEnterprisesVM);

            if (!ModelState.IsValid)
                return InvalidRequest(pGEUserEnterprisesVM, ModelState);

            var Enterprise = await _IVEUserRegionAS.CreateOrUpdateListAsync(pEstablishmentKey, pGEUserEnterprisesVM.results);

            if (_IVEUserRegionAS.Errors.Count > 0)
                return InvalidRequest(Enterprise, _IVEUserRegionAS.Errors);
            else
                await _IVEUserRegionAS.SaveChanges();

            return RequestOK(Enterprise);
        }

        [HttpDelete]
        [Route("key/{pUniqueKey}/id/{pId}/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> Delete(string pEstablishmentKey, string pUniqueKey, long pId)
        {
            _IVEUserRegionAS.Errors.Clear();
            var Result = await _IVEUserRegionAS.RemoveById(pId, pEstablishmentKey, pUniqueKey);

            if (Result != null && !_IVEUserRegionAS.Errors.Any())
            {
                await _IVEUserRegionAS.SaveChanges();
                return RequestOK(Mapper.Map<VEUserRegion, VEUserRegionVM>(Result));
            }

            return InvalidRequest(Result, _IVEUserRegionAS.Errors);
        }

        [HttpPost]
        [Route("list/delete")]
        [EstablishmentKey(ActionActual = Enums.EActionExec.DELETE)]
        public async Task<IHttpActionResult> PostDelete(string pEstablishmentKey, [FromBody] VEUserRegionListVM pGEUserEnterprisesVM)
        {
            _IVEUserRegionAS.Errors.Clear();
            var Result = await _IVEUserRegionAS.RemoveListAsync(pEstablishmentKey, pGEUserEnterprisesVM.results);

            if (Result != null && !_IVEUserRegionAS.Errors.Any())
            {
                await _IVEUserRegionAS.SaveChanges();
                return RequestOK(Mapper.Map<VEUserRegion[], VEUserRegionVM[]>(Result));
            }

            return InvalidRequest(Result, _IVEUserRegionAS.Errors);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _IVEUserRegionAS.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
