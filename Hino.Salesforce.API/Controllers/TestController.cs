﻿using Hino.Salesforce.API.Filter;
using Hino.Salesforce.Application.Interfaces.General;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers
{
    [RoutePrefix("api/Test")]
    public class TestController : BaseController
    {
        private readonly IGEEstablishmentsAS _IGEEstablishmentsAS;
        public TestController(IGEEstablishmentsAS pIGEEstablishmentsAS)
        {
            _IGEEstablishmentsAS = pIGEEstablishmentsAS;
        }
        [Route("")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public IHttpActionResult Get()
        {
            return RequestOK(
               JsonConvert.DeserializeObject(@"
                   {
                       'Connected': 'Ok'
                   }")
             );
        }
        [Route("conn")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public async Task<IHttpActionResult> GetConn()
        {
            try
            {
                return RequestOK(await _IGEEstablishmentsAS.GetAllAsync());
            }
            catch (Exception e)
            {
                return InvalidRequest(null, e);
            }
        }

        [Route("configuration")]
        [HttpGet]
        [IgnoreValidateEstablishmentKey]
        public IHttpActionResult GetConnConf()
        {
            try
            {
                var ret = new List<string>();
                /*
                foreach (string appsetting in WebConfigurationManager.AppSettings.Keys)
                    ret.Add($"{appsetting}:{ WebConfigurationManager.AppSettings[appsetting]}");
                ret.Add("-------------------------------------");
                foreach (ConnectionStringSettings cs in WebConfigurationManager.ConnectionStrings)
                    ret.Add($"{cs.Name}:{ cs.ConnectionString}");
                ret.Add("-------------------------------------");
                foreach (DictionaryEntry ev in System.Environment.GetEnvironmentVariables())
                    ret.Add($"{ev.Key}:{ev.Value}");
                */

                return RequestOK(ret);
            }
            catch (Exception e)
            {
                return InvalidRequest(null, e);
            }
        }
    }
}
