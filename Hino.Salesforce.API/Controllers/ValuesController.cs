﻿using Newtonsoft.Json;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web.Configuration;
using System.Web.Http;

namespace Hino.Salesforce.API.Controllers
{
    public class ValuesController : BaseController
    {
        // GET api/values
        public IHttpActionResult Get()
        {
            return RequestOK(
              JsonConvert.DeserializeObject(@"
                   {
                       'Conected': 'OK'
                   }")
            );
        }

        // GET api/values/5
        public string[] Get(int id)
        {
            var ret = new List<string>();
            foreach (string appsetting in WebConfigurationManager.AppSettings.Keys)
                ret.Add($"{appsetting}:{ WebConfigurationManager.AppSettings[appsetting]}");
            ret.Add("-------------------------------------");
            foreach (ConnectionStringSettings cs in WebConfigurationManager.ConnectionStrings)
                ret.Add($"{cs.Name}:{ cs.ConnectionString}");
            ret.Add("-------------------------------------");
            foreach (DictionaryEntry ev in System.Environment.GetEnvironmentVariables())
                ret.Add($"{ev.Key}:{ev.Value}");
            return ret.ToArray();
        }

        // POST api/values
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
