﻿namespace Hino.Salesforce.API.Enums
{
    public enum EActionExec
    {
        CREATE = 0,
        UPDATE = 1,
        DELETE = 2
    }
}