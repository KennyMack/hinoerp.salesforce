﻿using Hino.Salesforce.API.Enums;
using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Infra.Cross.Utils;
using System;
using System.Linq;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Hino.Salesforce.API.Filter
{
    /*public class EstablishmentKeyFilter : System.Web.Mvc.ActionFilterAttribute, System.Web.Mvc.IActionFilter
    {
        void System.Web.Mvc.IActionFilter.OnActionExecuting(
            System.Web.Mvc.ActionExecutingContext filterContext)
        {

            if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(EstablishmentKeyAttribute), true).Length > 0)
            {
                var formCollection =
                new FormCollection(filterContext.Controller.ControllerContext.HttpContext.Request.Fo‌​rm);

                formCollection.Set("EstablishmentKey",
                    filterContext.Controller.ControllerContext.HttpContext.Request.Params.Get("pEstablishmentKey"));

                if (!string.IsNullOrEmpty(filterContext.Controller.ControllerContext.HttpContext.Request.Params.Get("pUniqueKey").ToString()))
                {
                    formCollection.Set("UniqueKey",
                        filterContext.Controller.ControllerContext.HttpContext.Request.Params.Get("pUniqueKey").ToString());

                }

                filterContext.ActionParameters["form"] = formCollection;
            }
        }
    }*/

    public class EstablishmentKeyFilter : ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            var attr = actionContext.ActionDescriptor.GetCustomAttributes<EstablishmentKeyAttribute>().FirstOrDefault();

            if (attr != null &&
                attr.ActionActual.In(EActionExec.CREATE, EActionExec.UPDATE))
            {
                try
                {
                    var EstablishmentKey = actionContext.RequestContext.RouteData.Values["pEstablishmentKey"].ToString();

                    if (!string.IsNullOrEmpty(EstablishmentKey))
                    {
                        var Model = actionContext.ActionArguments.Where(r =>
                                r.Value.GetType().IsSubclassOf(typeof(BaseVM)))
                                .FirstOrDefault();

                        if (Model.Value != null)
                            ((BaseVM)Model.Value).EstablishmentKey = EstablishmentKey;

                        if (attr.ActionActual == EActionExec.CREATE && ((BaseVM)Model.Value).UniqueKey.IsEmpty())
                            ((BaseVM)Model.Value).UniqueKey = EstablishmentKey;

                        actionContext.ModelState.Clear();
                    }
                }
                catch (Exception)
                {

                }
            }
        }
    }

    [AttributeUsage(AttributeTargets.Method, Inherited = true)]
    public class EstablishmentKeyAttribute : Attribute
    {
        public EActionExec ActionActual { get; set; }
    }
}