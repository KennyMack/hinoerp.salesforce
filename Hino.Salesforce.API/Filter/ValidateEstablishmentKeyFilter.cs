﻿using Hino.Salesforce.API.HttpUtils;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Infra.Cross.Utils.Settings;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Hino.Salesforce.API.Filter
{
    public class ValidateEstablishmentKeyFilter : ActionFilterAttribute
    {

        public async override Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            if (!actionContext.ActionDescriptor.GetCustomAttributes<IgnoreValidateEstablishmentKeyAttribute>().Any())
            {
                try
                {
                    var HinoId = "NOFUNFOU";
                    var EstablishmentKey = actionContext.RequestContext.RouteData.Values["pEstablishmentKey"].ToString();
                    try
                    {
                        HinoId = AppSettings.Hino_Id; // ConfigurationManager.AppSettings["Hino_Id"].ToString();
                    }
                    catch (Exception)
                    {
                        HinoId = "NOFUNFOU";
                    }
                    if (EstablishmentKey != HinoId)
                    {
                        var _IGEEstablishmentsAS = (IGEEstablishmentsAS)Startup.DependencyResolver.GetService(typeof(IGEEstablishmentsAS));

                        if (!await _IGEEstablishmentsAS.ExistsEstablishmentAsync(EstablishmentKey))
                            throw new Exception();
                    }
                }
                catch (Exception e)
                {
                    Infra.Cross.Utils.Exceptions.Logging.Exception(e);
                    HttpActionFilterResult.InvalidResult(actionContext,
                        new Infra.Cross.Utils.Exceptions.ModelException
                        {
                            ErrorCode = (int)Infra.Cross.Utils.Exceptions.EExceptionErrorCodes.InvalidEstablishmentKey,
                            Field = "pEstablishmentKey",
                            Value = "pEstablishmentKey",
                            Messages = new[] { "Chave do estabelecimento não é válida." }
                        }
                    );
                    //return null;
                }
            }
            //return base.OnActionExecutingAsync(actionContext, cancellationToken);
        }

    }

    [AttributeUsage(AttributeTargets.Method, Inherited = true)]
    public class IgnoreValidateEstablishmentKeyAttribute : Attribute { }
}
