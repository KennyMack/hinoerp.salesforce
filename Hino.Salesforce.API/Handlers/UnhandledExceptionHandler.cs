﻿using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System.Web.Http.ExceptionHandling;

namespace Hino.Salesforce.API.Handlers
{
    public class UnhandledExceptionHandler : ExceptionLogger
    {
        public override void Log(ExceptionLoggerContext context)
        {
            Logging.Exception(context.Exception);
            if (context.Exception.InnerException != null)
                Logging.Exception(context.Exception.InnerException);
        }
    }
}