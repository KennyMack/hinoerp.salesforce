﻿using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http.Controllers;

namespace Hino.Salesforce.API.HttpUtils
{
    public static class HttpActionFilterResult
    {
        public static void InvalidResult(
            HttpActionContext actionContext,
            ModelException pExeption)
        {
            var txtMessage = $"{{ ErrorCode: {pExeption.ErrorCode}, Field : \"{pExeption.Field}\", Value : \"{pExeption.Value}\", Messages : [ \"{ string.Join(" ", pExeption.Messages.Select(r => r?.Replace("\"", "\'")))}\" ] }}";


            var result = new System.Web.Mvc.JsonResult
            {
                Data = new
                {
                    status = HttpStatusCode.BadRequest,
                    success = false,
                    data = new JObject(),
                    error = new JArray()
                    {
                        JObject.Parse(txtMessage)
                    }
                },
                JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet
            };
            var message = new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(result, Formatting.Indented), Encoding.UTF8, "application/json"),
                ReasonPhrase = "Validation error",
                StatusCode = HttpStatusCode.BadRequest
            };

            actionContext.Response = message;
            actionContext.Response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json")
            {
                CharSet = "utf-8"
            };
        }

        public static void UnauthorizedResult(
            HttpActionContext actionContext,
            ModelException pExeption)
        {
            var txtMessage = $"{{ ErrorCode: {pExeption.ErrorCode}, Field : \"{pExeption.Field}\", Value : \"{pExeption.Value}\", Messages : [ \"{ string.Join(" ", pExeption.Messages.Select(r => r?.Replace("\"", "\'")))}\" ] }}";


            var result = new System.Web.Mvc.JsonResult
            {
                Data = new
                {
                    status = HttpStatusCode.Unauthorized,
                    success = false,
                    data = new JObject(),
                    error = new JArray()
                    {
                        JObject.Parse(txtMessage.Replace("\"", "\'"))
                    }
                },
                JsonRequestBehavior = System.Web.Mvc.JsonRequestBehavior.AllowGet
            };
            var message = new HttpResponseMessage()
            {
                Content = new StringContent(JsonConvert.SerializeObject(result, Formatting.Indented), Encoding.UTF8, "application/json"),
                ReasonPhrase = "Unauthorized",
                StatusCode = HttpStatusCode.BadRequest
            };

            actionContext.Response = message;
            actionContext.Response.Content.Headers.ContentType = new MediaTypeHeaderValue("application/json")
            {
                CharSet = "utf-8"
            };
        }
    }
}