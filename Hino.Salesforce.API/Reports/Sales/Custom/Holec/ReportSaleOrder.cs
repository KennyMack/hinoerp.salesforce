﻿using AutoMapper;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.API.Reports.Sales.Custom.Holec
{
    public partial class ReportSaleOrder : DevExpress.XtraReports.UI.XtraReport
    {
        private readonly string _EstablishmentKey;
        private readonly long _Id;
        private VEOrdersVM _VEOrdersVM;

        public ReportSaleOrder(string pEstablishmentKey, long pId)
        {
            InitializeComponent();
            _EstablishmentKey = pEstablishmentKey;
            _Id = pId;

            xrpicLogoHolec.Visible = true;
        }

        public async Task LoadData()
        {
            var _IVEOrdersAS = (IVEOrdersAS)Startup.DependencyResolver.GetService(typeof(IVEOrdersAS));
            var _IGEEnterprisesAS = (IGEEnterprisesAS)Startup.DependencyResolver.GetService(typeof(IGEEnterprisesAS));
            var _GEEstablishment = (IGEEstablishmentsAS)Startup.DependencyResolver.GetService(typeof(IGEEstablishmentsAS));

            var Establishment = (await _GEEstablishment.QueryAsync(r => r.EstablishmentKey == _EstablishmentKey)).FirstOrDefault();

            _VEOrdersVM = Mapper.Map<VEOrders, VEOrdersVM>((await _IVEOrdersAS.QueryAsync(r =>
                r.EstablishmentKey == _EstablishmentKey && r.Id == _Id)).FirstOrDefault());
            if (_VEOrdersVM != null)
            {
                _VEOrdersVM.GEEnterprises = Mapper.Map<GEEnterprises, GEEnterprisesVM>(_IGEEnterprisesAS.GetById(
                    _VEOrdersVM.EnterpriseID, _VEOrdersVM.EstablishmentKey, _VEOrdersVM.GEEnterprises.UniqueKey,
                    r => r.GEEnterpriseCategory,
                    s => s.GEEnterpriseContacts,
                    s => s.GEEnterpriseFiscalGroup,
                    s => s.GEEnterpriseGeo,
                    s => s.GEEnterpriseGroup,
                    s => s.GEPaymentCondition,
                    s => s.GEEnterpriseGroup));

                xtcEmpresa.Text = $"{_VEOrdersVM.GEEnterprises.Id} - {_VEOrdersVM.GEEnterprises.RazaoSocial} / {_VEOrdersVM.GEEnterprises.NomeFantasia}";

                xtcPropostaPed.Text = !_VEOrdersVM.IsProposal ? $"Pedido: {_VEOrdersVM.Id}/{_VEOrdersVM.OrderVersion}" : $"Proposta: {_VEOrdersVM.Id}/{_VEOrdersVM.OrderVersion}";
                xtcAddressLine1.Text = $@"{_VEOrdersVM.GEEnterprises.CommercialAddress.Address}, {_VEOrdersVM.GEEnterprises.CommercialAddress.Num} - {_VEOrdersVM.GEEnterprises.CommercialAddress.District}";
                xtcAddressLine2.Text = $@"{_VEOrdersVM.GEEnterprises.CommercialAddress.ZipCode} - {_VEOrdersVM.GEEnterprises.CommercialAddress.CityName} - {_VEOrdersVM.GEEnterprises.CommercialAddress.UF}";

                _VEOrdersVM.VEOrderItems = _VEOrdersVM.VEOrderItems.OrderBy(r => r.Item).ThenBy(r => r.ItemLevel).ToArray();
            }

            foreach (var item in _VEOrdersVM.VEOrderItems)
            {
                item.GEProducts.Name = $"{item.GEProducts.ProductKey} - {item.GEProducts.AditionalInfo}\r\n{item.GEProducts.Description}";

                if (!item.Note.IsEmpty() && item.ItemLevel == 0)
                    item.GEProducts.Name = $"{item.GEProducts.ProductKey} - {item.GEProducts.AditionalInfo}\r\n{item.GEProducts.Name}\r\n{item.Note}";

                item.GEProducts.Name += $"\r\n\r\nNCM: {item.GEProducts.NCM}";

                if (item.ItemLevel == 0)
                {
                    foreach (var itemLevel in _VEOrdersVM.VEOrderItems.Where(r => r.Item == item.Item && r.ItemLevel > 0))
                    {
                        item.GEProducts.Name += $"\r\n\r\n{itemLevel.Quantity}    {itemLevel.GEProducts.ProductKey} - {itemLevel.GEProducts.AditionalInfo}\r\n{itemLevel.GEProducts.Name}\r\n{itemLevel.Note}";
                    }
                }
            }

            var natOperItem = _VEOrdersVM.VEOrderItems.OrderByDescending(r => r.TotalValueWithDiscount).FirstOrDefault();
            xlNatOper.Text = "";
            if (natOperItem != null)
                xlNatOper.Text = $"Mat. Destinado: {natOperItem.FSFiscalOper?.Description}";
            /*
            if (_VEOrdersVM.GEEnterprises.GEEnterpriseGroup != null)
                xlNatOper.Text = $"{xlNatOper.Text}\r\n{_VEOrdersVM.GEEnterprises.GEEnterpriseGroup.Identifier}-{_VEOrdersVM.GEEnterprises.GEEnterpriseGroup.Description}";
            */

            var IPIs = _VEOrdersVM.VEOrderItems.SelectMany(r => r.VEOrderTaxes).Where(s => s.Type == 3);

            // Total em Reais excluso IPI, ST e DIFAL: 
            // ICMS Diferencial de aliquota:
            // Valor em reais do frete:
            // Total em Reais com todos impostos inclusos:

            _VEOrdersVM.VEOrdersTotals.Add(new VEOrdersTotalsVM
            {
                Description = "Total em Reais excluso IPI, ST e DIFAL:",
                Value = _VEOrdersVM.VEOrderItems.Sum(r => r.TotalValueWithDiscount)
            });

            if (_VEOrdersVM.TotalICMSDiferencial > 0)
            {
                _VEOrdersVM.VEOrdersTotals.Add(new VEOrdersTotalsVM
                {
                    Description = "ICMS Diferencial de aliquota:",
                    Value = (float)_VEOrdersVM.TotalICMSDiferencial
                });
            }

            if (_VEOrdersVM.VEOrderItems.Sum(r => r.TotalICMSST) > 0)
            {
                _VEOrdersVM.VEOrdersTotals.Add(new VEOrdersTotalsVM
                {
                    Description = "ICMS Substituição Tributária:",
                    Value = (float)_VEOrdersVM.VEOrderItems.Sum(r => r.TotalICMSST)
                });
            }

            if (_VEOrdersVM.FreightValue > 0)
            {
                _VEOrdersVM.VEOrdersTotals.Add(new VEOrdersTotalsVM
                {
                    Description = "Valor em reais do frete:",
                    Value = (float)_VEOrdersVM.FreightValue
                });
            }
            _VEOrdersVM.VEOrdersTotals.Add(new VEOrdersTotalsVM
            {
                Description = "Total em Reais com todos impostos inclusos:",
                Value = (float)_VEOrdersVM.TotalValueWithDiscountAndTaxes
            });

            ReportFooter.Visible = _VEOrdersVM.IsProposal;
            xrCondComerciais.Visible = _VEOrdersVM.IsProposal;

            objectDataSource1.DataSource = _VEOrdersVM;
        }

        private void xrTableCell2_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void xrTableCell56_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }

        private void ReportSaleOrder_DataSourceDemanded(object sender, EventArgs e)
        {
            objectDataSource1.Fill();
        }

        private void xrTableCell5_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {

        }
    }
}
