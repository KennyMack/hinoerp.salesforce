﻿using AutoMapper;
using DevExpress.XtraReports.UI;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.API.Reports.Sales
{
    public partial class ReportSaleOrder : DevExpress.XtraReports.UI.XtraReport
    {
        private readonly string _EstablishmentKey;
        private readonly long _Id;
        private VEOrdersVM _VEOrdersVM;

        public ReportSaleOrder(string pEstablishmentKey, long pId)
        {
            InitializeComponent();
            _EstablishmentKey = pEstablishmentKey;
            _Id = pId;

            xrpicLogoHolec.Visible = false;

            xrpicLogoDisemp.Visible = (_EstablishmentKey == "08d08c79-f4a0-4a9e-884a-78bb48bd51fd");
            xrpicLogoHolec.Visible = (_EstablishmentKey == "f48c8bd0-a048-4ae4-a988-75b791fdd80b");

            xrpicLogoDisemp.LocationFloat = new DevExpress.Utils.PointFloat(17.99999F, 28F);
            xrpicLogoHolec.LocationFloat = new DevExpress.Utils.PointFloat(17.99999F, 28F);
        }

        public async Task LoadData()
        {
            var _IVEOrdersAS = (IVEOrdersAS)Startup.DependencyResolver.GetService(typeof(IVEOrdersAS));
            var _IGEEnterprisesAS = (IGEEnterprisesAS)Startup.DependencyResolver.GetService(typeof(IGEEnterprisesAS));
            var _GEEstablishment = (IGEEstablishmentsAS)Startup.DependencyResolver.GetService(typeof(IGEEstablishmentsAS));

            var Establishment = (await _GEEstablishment.QueryAsync(r => r.EstablishmentKey == _EstablishmentKey)).FirstOrDefault();

            _VEOrdersVM = Mapper.Map<VEOrders, VEOrdersVM>((await _IVEOrdersAS.QueryAsync(r =>
                r.EstablishmentKey == _EstablishmentKey && r.Id == _Id)).FirstOrDefault());
            if (_VEOrdersVM != null)
            {
                _VEOrdersVM.GEEnterprises = Mapper.Map<GEEnterprises, GEEnterprisesVM>(_IGEEnterprisesAS.GetById(
                    _VEOrdersVM.EnterpriseID, _VEOrdersVM.EstablishmentKey, _VEOrdersVM.GEEnterprises.UniqueKey,
                    r => r.GEEnterpriseCategory,
                    s => s.GEEnterpriseContacts,
                    s => s.GEEnterpriseFiscalGroup,
                    s => s.GEEnterpriseGeo,
                    s => s.GEEnterpriseGroup,
                    s => s.GEPaymentCondition));

                xtcTitle.Text = !_VEOrdersVM.IsProposal ? "Pedido de venda" : "Proposta";
                xtcPropostaPed.Text = !_VEOrdersVM.IsProposal ? $"Pedido: {_VEOrdersVM.Id}/{_VEOrdersVM.OrderVersion}" : $"Proposta: {_VEOrdersVM.Id}/{_VEOrdersVM.OrderVersion}";
                xtcAddress.Text = $@"{_VEOrdersVM.GEEnterprises.CommercialAddress.Address}, {_VEOrdersVM.GEEnterprises.CommercialAddress.Num} - {_VEOrdersVM.GEEnterprises.CommercialAddress.District}, {_VEOrdersVM.GEEnterprises.CommercialAddress.CityName} - {_VEOrdersVM.GEEnterprises.CommercialAddress.UF}, {_VEOrdersVM.GEEnterprises.CommercialAddress.ZipCode}";

                xtcDTEntrega.Text = (_VEOrdersVM.IsProposal) ? $"{_VEOrdersVM.ShippingDays} dias após aprovação" : _VEOrdersVM.DeliveryDate.ToString("dd/MM/yyyy");

                _VEOrdersVM.VEOrderItems = _VEOrdersVM.VEOrderItems.OrderBy(r => r.Item).ThenBy(r => r.ItemLevel).ToArray();
            }
            if (Establishment != null)
                lblEstablishmentData.Text = $"{Establishment.CNPJCPF} - {Establishment.RazaoSocial} / {Establishment.NomeFantasia}\r\n{Establishment.Phone}\r\n{Establishment.Email}";


            foreach (var item in _VEOrdersVM.VEOrderItems)
            {
                if (!item.Note.IsEmpty())
                    item.GEProducts.Name = $"{item.GEProducts.Name}\r\n{item.Note}";
            }

            objectDataSource1.DataSource = _VEOrdersVM;
        }

        private void ReportSaleOrder_DataSourceDemanded(object sender, EventArgs e)
        {
            objectDataSource1.Fill();

        }

        private void xrTableCell51_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ((XRTableCell)sender).Text = _VEOrdersVM.FreightPaidBy.GetDescription();
        }

        private void xtcDiscountHead_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ((XRTableCell)sender).Text = _VEOrdersVM.PercDiscount.ToString();
        }

        private void xtcClientOrder_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ((XRTableCell)sender).Text = _VEOrdersVM.ClientOrder;
        }

        private void xtcOrderId_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ((XRTableCell)sender).Text = $"{_VEOrdersVM.Id}/{_VEOrdersVM.OrderVersion}";
        }

        private void xtcOrderDate_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
            ((XRTableCell)sender).Text = _VEOrdersVM.Created.ToString("dd/MM/yyyy HH:mm");
        }

        private void xtcTotalWithFrete_BeforePrint(object sender, System.Drawing.Printing.PrintEventArgs e)
        {
        }
    }
}
