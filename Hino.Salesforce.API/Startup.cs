﻿using Hino.Salesforce.API.App_Start;
using Hino.Salesforce.API.Controllers;
using Hino.Salesforce.API.Controllers.Admin;
using Hino.Salesforce.API.Controllers.Auth;
using Hino.Salesforce.API.Controllers.Fiscal;
using Hino.Salesforce.API.Controllers.Fiscal.Taxes;
using Hino.Salesforce.API.Controllers.General;
using Hino.Salesforce.API.Controllers.General.Business;
using Hino.Salesforce.API.Controllers.General.Calendar;
using Hino.Salesforce.API.Controllers.General.Demograph;
using Hino.Salesforce.API.Controllers.General.Kanban;
using Hino.Salesforce.API.Controllers.Geocoding;
using Hino.Salesforce.API.Controllers.Reports;
using Hino.Salesforce.API.Controllers.Route;
using Hino.Salesforce.API.Controllers.Sales;
using Hino.Salesforce.API.Controllers.Sales.Transactions;
using Hino.Salesforce.API.Filter;
using Hino.Salesforce.API.Handlers;
using Hino.Salesforce.Application.Interfaces.Auth;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.Providers;
using Hino.Salesforce.Infra.Cross.Cache.Auth;
using Hino.Salesforce.Infra.Cross.Identity.Configurations;
using Hino.Salesforce.Infra.Cross.IoC;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Settings;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Owin;
using System;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Security;
using System.Web.Http;
using System.Web.Http.Dependencies;
using System.Web.Http.ExceptionHandling;

[assembly: OwinStartup(typeof(Hino.Salesforce.API.Startup))]
namespace Hino.Salesforce.API
{
    public class Startup
    {
        public static IDependencyResolver DependencyResolver { get; private set; }

        public void Configuration(IAppBuilder app)
        {
            var filebase = GetFileContent.BasePath();

            var files = Directory.GetFiles(string.Concat(filebase, "\\Templates"));
            /*
            if (files.Length <= 0)
                throw new NotImplementedException("Templates de e-mail não localizados");
            */


            Ssl.EnableTrustedHosts();

            // Enable cors
            app.UseCors(CorsOptions.AllowAll);

            ConfigurationManager.AppSettings.LoadSettingsAPI();

            // config WebApi
            var config = new HttpConfiguration();
            

            // Web API configuration and services
            config.Services.Replace(typeof(IExceptionLogger), new UnhandledExceptionHandler());

            // config routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApiSalesforce",
                routeTemplate: "api/{controller}/{action}/{pEstablishmentKey}"
            );

            config.Routes.MapHttpRoute(
                name: "DefaultApiAdminSalesforce",
                routeTemplate: "api/admin/{controller}/{action}"
            );

            var services = new ServiceCollection();
            services.RegisterServices();

            services.AddTransient(typeof(ProductsApplicationController));
            services.AddTransient(typeof(EnterprisesController));
            services.AddTransient(typeof(EnterpriseGeoController));
            services.AddTransient(typeof(PaymentConditionController));
            services.AddTransient(typeof(PaymentTypeController));
            services.AddTransient(typeof(CitiesController));
            services.AddTransient(typeof(CountriesController));
            services.AddTransient(typeof(StatesController));
            services.AddTransient(typeof(EstabDevicesController));
            services.AddTransient(typeof(EstablishmentsController));
            services.AddTransient(typeof(ProductsController));
            services.AddTransient(typeof(UsersController));
            services.AddTransient(typeof(OrderItemsController));
            services.AddTransient(typeof(OrdersController));
            services.AddTransient(typeof(AdminEstablishmentsController));
            services.AddTransient(typeof(AdminDevicesController));
            services.AddTransient(typeof(AdminUsersController));
            services.AddTransient(typeof(MordorController));
            services.AddTransient(typeof(RegionSaleController));
            services.AddTransient(typeof(RegionSaleUFController));
            services.AddTransient(typeof(RegionSaleController));
            services.AddTransient(typeof(SalePriceController));
            services.AddTransient(typeof(FiscalTaxesController));
            services.AddTransient(typeof(FiscalOperController));
            services.AddTransient(typeof(StatusController));
            services.AddTransient(typeof(TourController));
            services.AddTransient(typeof(AddressController));
            services.AddTransient(typeof(CepController));
            services.AddTransient(typeof(TestController));
            services.AddTransient(typeof(MapController));
            services.AddTransient(typeof(ProductsFamilyController));
            services.AddTransient(typeof(ProductsTypeController));
            services.AddTransient(typeof(UnitController));
            services.AddTransient(typeof(NCMController));
            services.AddTransient(typeof(EnterpriseFiscalGroupController));
            services.AddTransient(typeof(EnterpriseGroupController));
            services.AddTransient(typeof(EnterpriseCategoryController));
            services.AddTransient(typeof(UserEnterprisesController));
            services.AddTransient(typeof(SaleWorkRegionController));
            services.AddTransient(typeof(UserRegionController));
            services.AddTransient(typeof(ReportSalesController));
            services.AddTransient(typeof(TransactionsController));
            services.AddTransient(typeof(EstabMenuController));
            services.AddTransient(typeof(FilesPathController));
            services.AddTransient(typeof(SettingsController));
            services.AddTransient(typeof(EventsController));
            services.AddTransient(typeof(UserPayTypeController));
            services.AddTransient(typeof(EnterpriseAnnotationController));
            services.AddTransient(typeof(TypeSaleController));

            config.Formatters.JsonFormatter.SerializerSettings.ReferenceLoopHandling
                = Newtonsoft.Json.ReferenceLoopHandling.Ignore;

            var resolver = new DepResolver(services.BuildServiceProvider());
            config.DependencyResolver = resolver;

            config.Filters.Add(new NullModelStateActionFilter());
            config.Filters.Add(new AuthenticationFilter());
            config.Filters.Add(new ExceptionHandleFilter());
            config.Filters.Add(new ValidateEstablishmentKeyFilter());
            config.Filters.Add(new EstablishmentKeyFilter());

            EnableAccessToken(app, (IGEUsersAS)config.DependencyResolver.GetService(typeof(IGEUsersAS)),
                (IGEEstablishmentsAS)config.DependencyResolver.GetService(typeof(IGEEstablishmentsAS)),
                (IAURefreshTokenAS)config.DependencyResolver.GetService(typeof(IAURefreshTokenAS)),
                (SessionTokenCache)config.DependencyResolver.GetService(typeof(SessionTokenCache)),
                (SigningConfigurations)config.DependencyResolver.GetService(typeof(SigningConfigurations)),
                (TokenConfigurations)config.DependencyResolver.GetService(typeof(TokenConfigurations))
                );

            DependencyResolver = config.DependencyResolver;

            // Enable webapi config
            app.UseWebApi(config);
        }

        private void EnableAccessToken(IAppBuilder app, IGEUsersAS pIGEUsersAS,
            IGEEstablishmentsAS pIGEEstablishmentsAS,
            IAURefreshTokenAS pIAURefreshTokenAS,
            SessionTokenCache pSessionTokenCache,
            SigningConfigurations pSigningConfigurations,
            TokenConfigurations pTokenConfigurations)
        {
            var SettingsToken = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/api/Auth/Mordor/login"),
                AccessTokenExpireTimeSpan = TimeSpan.FromSeconds(pTokenConfigurations.FinalExpiration),
                /*AccessTokenFormat = new JWTTokenFormat(
                    pIAURefreshTokenAS, pSessionTokenCache,
                    pSigningConfigurations, pTokenConfigurations),*/
                RefreshTokenProvider = new OAuthBearerRefreshTokenProvider(pIGEUsersAS, pIGEEstablishmentsAS,
                    pIAURefreshTokenAS, pSessionTokenCache,
                    pSigningConfigurations, pTokenConfigurations),
                Provider = new AuthorizationTokenProvider(pIGEUsersAS, pIGEEstablishmentsAS,
                    pIAURefreshTokenAS, pSessionTokenCache,
                    pSigningConfigurations, pTokenConfigurations)
            };

            app.UseOAuthAuthorizationServer(SettingsToken);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions()
            {
                AuthenticationMode = AuthenticationMode.Active,
                AuthenticationType = "Bearer",
                //AccessTokenProvider = new OAuthBearerProvider()
            });
        }
    }

    public static class Ssl
    {
        private static readonly string[] TrustedHosts = new[] {
            "https://heimdall-homolog.cappta.com.br",
            "https://transactions-homolog.cappta.com.br"
        };

        public static void EnableTrustedHosts()
        {
            ServicePointManager.ServerCertificateValidationCallback =
            (sender, certificate, chain, errors) =>
            {
                if (errors == SslPolicyErrors.None)
                {
                    return true;
                }

                var request = sender as HttpWebRequest;
                if (request != null)
                {
                    return TrustedHosts.Contains(request.RequestUri.Host);
                }

                return false;
            };
        }
    }
}
