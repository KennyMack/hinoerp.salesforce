﻿using System.Web.Optimization;

namespace Hino.Salesforce.Admin
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at https://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular").Include(
                "~/Scripts/angular.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-touch").Include(
                            "~/Scripts/angular-touch.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-sanitize").Include(
                            "~/Scripts/angular-sanitize.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-route").Include(
                            "~/Scripts/angular-route.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-resource").Include(
                            "~/Scripts/angular-resource.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-ext").Include(
                            "~/Scripts/angular-parse-ext.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-messages").Include(
                            "~/Scripts/angular-messages.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-format").Include(
                            "~/Scripts/angular-message-format.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-loader").Include(
                            "~/Scripts/angular-loader.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-cookies").Include(
                            "~/Scripts/angular-cookies.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-aria").Include(
                            "~/Scripts/angular-aria.js"));

            bundles.Add(new ScriptBundle("~/bundles/angular-animate").Include(
                            "~/Scripts/angular-animate.js"));

            bundles.Add(new ScriptBundle("~/bundles/app").Include(
                "~/Scripts/App/app.module.js",
                "~/Scripts/static/InputMask.js",
                "~/Scripts/static/URLS.js",
                "~/Scripts/static/UrlQueryString.js",

                "~/Scripts/App/modules/core/core.modules.js",
                "~/Scripts/App/modules/core/factories/localsave.factory.js",
                "~/Scripts/App/modules/core/services/haserror.service.js",
                "~/Scripts/App/modules/core/services/request.service.js",

                "~/Scripts/App/modules/auth/auth.modules.js",
                "~/Scripts/App/modules/auth/factories/authentication.factory.js",
                "~/Scripts/App/modules/auth/factories/authorization.factory.js",
                "~/Scripts/App/modules/auth/controllers/login.controller.js",

                "~/Scripts/App/modules/establishment/establishment.modules.js",
                "~/Scripts/App/modules/establishment/factories/establishment.factory.js",
                "~/Scripts/App/modules/establishment/factories/devices.factory.js",
                "~/Scripts/App/modules/establishment/controllers/establishment.controller.js",


                "~/Scripts/App/modules/main/main.modules.js",
                "~/Scripts/App/modules/main/controllers/main.controller.js",

                "~/Scripts/App/app.js"));



            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/Content/bootstrap.css",
                      "~/Content/site.css"));
        }
    }
}
