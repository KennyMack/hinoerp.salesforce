﻿using System.Web.Mvc;

namespace Hino.Salesforce.Admin.Controllers
{
    //[RoutePrefix("establishment")]
    public class EstablishmentController : Controller
    {
        // GET: Establishment
        public ActionResult Index()
        {
            ViewBag.Message = "Estabelecimentos";
            return View();
        }

        // GET: Establishment Create
        public ActionResult Create()
        {
            ViewBag.Message = "Novo Estabelecimento";

            return View();
        }
        [HttpGet]
        public ActionResult Edit(long id, string pEstablishmentKey, string pKey)
        {
            ViewBag.Message = "Editar Estabelecimento";
            ViewBag.EstablishmentKey = pEstablishmentKey;
            ViewBag.UniqueKey = pKey;
            ViewBag.Id = id;

            return View();
        }
    }
}