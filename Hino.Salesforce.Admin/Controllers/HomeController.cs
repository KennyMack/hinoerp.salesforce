﻿using System.Web.Mvc;

namespace Hino.Salesforce.Admin.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        public ActionResult Devices()
        {
            ViewBag.Message = "Dispositivos";

            return View();
        }

        public ActionResult Establishment()
        {
            ViewBag.Message = "Estabelecimentos";

            return View();
        }
    }
}