﻿using System.Configuration;
using System.Web.Mvc;

namespace Hino.Salesforce.Admin.Controllers
{
    public class MordorController : Controller
    {
        // GET: Mordor
        public ActionResult Login()
        {
            ViewBag.HinoId = ConfigurationManager.AppSettings["Hino_Id"].ToString();
            return View();
        }

        // GET: Register
        public ActionResult Register()
        {
            return View();
        }
    }
}