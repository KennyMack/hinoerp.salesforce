﻿'use strict';
(function (angular, hinoApp) {
    function configApp(
        $httpProvider,
        $routeProvider,
        $locationProvider,
        $windowProvider) {

        $httpProvider.interceptors.push(function ($q) {
            return {
                /*request: function (config) {
                    console.log('intercept request');
                    console.log(config);
                    return config;
                },
                response: function (response) {
                    console.log('intercept response');
                    console.log(response);
                    return $q.reject(response);
                },*/
                responseError: function (response) {
                    
                    console.log('intercept response error');
                    console.log(response);
                   /* if (response.status === 401 &&
                        !window.location.pathname.toUpperCase().match(/(\/MORDOR\/LOGIN)/))
                        window.location.href = '/Mordor/login';*/
                    return $q.reject(response);
                }
            };
        });

        /*$httpProvider.interceptors.push(function ($location, $window) {
            console.log('$windowProvider');
            let wind = $windowProvider.$get();
            return {
                responseError: function (rejection) {
                    console.log('rejection');
                    console.log(rejection);
                    return new Promise(function (resolve, reject) {
                        if (wind.location.pathname.toUpperCase().match(/(\/MORDOR\/LOGIN)/ )&&
                            rejection.status === 401) {
                            wind.location.href = '/Mordor/login';
                            console.log('401');
                        }

                        return reject(rejection);
                    });
                }
            };
        });*/
    }

    function runApp(
        $rootScope,
        authorization,
        $location,
        $interval,
        BASEURLS,
        $window,
        $anchorScroll,
        hasError) {
        $rootScope.__Title = 'Hino';
        $rootScope.__showModal = false;
        $rootScope.__showLoad = false;
        $rootScope.BASEURLS = BASEURLS;
        if (!$rootScope.__pathHistory)
            $rootScope.__pathHistory = [];

        $rootScope.$on('$routeChangeStart', function (event, next) {
            console.log('$routeChangeStart');

            window.scrollTo(0, 0);
            $anchorScroll();
            hasError.clearError();
            $rootScope.__showModal = false;
            $rootScope.__showLoad = true;
            $rootScope.$broadcast('evt__showModal', false);
            $rootScope.$broadcast('evt__showLoad', true);
        });

        $rootScope.$on('$routeChangeSuccess', function (event, next) {
            console.log('$routeChangeSuccess');
            window.scrollTo(0, 0);
            $anchorScroll();
            hasError.clearError();
            $rootScope.__Title = next.title || 'Hino';
            document.title = $rootScope.__Title;

            authorization.authorize(next)
                .then(function (url) {
                    $rootScope.__pathHistory.push($location.$$path);
                    $rootScope.$broadcast('evt_navBarUser_event', '');
                })
                .catch(function (url) {
                    $location.path(url);
                });
        });

        $rootScope.$on('$viewContentLoaded', function () {
            console.log('$viewContentLoaded');

            var i = $interval(function () {
                $rootScope.$broadcast('evt__showModal', false);
                $rootScope.$broadcast('evt__showLoad', false);
                $interval.cancel(i);
            }, 1300);

        });

        $rootScope.$on('evt__showModal', function (ev, value) {
            $rootScope.__showModal = value;
        });

        $rootScope.$on('evt__showLoad', function (ev, value) {
            $rootScope.__showLoad = value;
        });
    }
    configApp.$inject = [
        '$httpProvider',
        '$routeProvider',
        '$locationProvider',
        '$windowProvider'
    ];

    runApp.$inject = [
        '$rootScope',
        hinoApp.modules.auth.factories.authorization,
        '$location',
        '$interval',
        'BASEURLS',
        '$window',
        '$anchorScroll',
        hinoApp.modules.core.services.hasError
    ];

    angular.module(hinoApp.modules.app.name, [
        'ngRoute',
        hinoApp.modules.core.name,
        hinoApp.modules.auth.name,
        hinoApp.modules.main.name,
        hinoApp.modules.establishment.name
    ])
        .constant('BASEURLS', {
             BASE: 'http://localhost:6080',
             BASE_API: 'http://localhost:6090/Hino.Salesforce.API/api',
            //BASE_API: 'http://localhost:6090/api',
            URLS: hinoApp.URLS
        }).constant('LOCALNAME', {
            USER_TOKEN: 'User-Token',
            REFRESH_TOKEN: 'Refresh-Token',
            ESTABLISHMENTKEY: 'EstablishmentKey'
        })
        .config(configApp)
        .run(runApp);
}(angular, hinoApp));