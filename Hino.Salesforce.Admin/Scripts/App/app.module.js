﻿'use strict';
(function (window) {

    var hinoApp = window.hinoApp = window.hinoApp || {};

    hinoApp.modules = {
        app: {
            name: 'hinoApp'
        }
    };

    hinoApp.components = {};
    hinoApp.URLS = {};
}(window));