﻿'use strict';
(function (angular, hinoApp) {
    hinoApp.modules.auth = {
        name: 'hinoApp.auth',
        controllers: {
            login: {
                name: 'LoginController',
                nameas: 'LoginCtrl'
            },
            register: {
                name: 'RegisterController',
                nameas: 'RegisterCtrl'
            },
            password: {
                name: 'PasswordController',
                nameas: 'PasswordCtrl'
            }
        },
        routes: {
            admin: {
                authenticate: hinoApp.URLS.MORDOR.ADMIN.AUTHENTICATION()
            },
            login: hinoApp.URLS.LOGIN(),
            register: hinoApp.URLS.REGISTER(),
            loginNext: hinoApp.URLS.LOGINNEXT(),
            authenticate: hinoApp.URLS.MORDOR.AUTHENTICATION(),
            me: hinoApp.URLS.MORDOR.ME(),
            change_pass: hinoApp.URLS.CHANGE_PASS(),
            change: hinoApp.URLS.MORDOR.CHANGE(),
            forgot: hinoApp.URLS.MORDOR.FORGOT(),
            notAuthorized: hinoApp.URLS.NOTAUTHORIZED(),
            serverError: hinoApp.URLS.SERVERERROR(),
            user: hinoApp.URLS.USER(),
            forgot_pass: hinoApp.URLS.FORGOT_PASS(),
            recover: hinoApp.URLS.MORDOR.RECOVER(),
            recover_pass: hinoApp.URLS.RECOVER_PASS()
        },
        factories: {
            authentication: 'authenticationFactory',
            authorization: 'authorizationFactory'
        },
        services: {
        },
        templates: {
        },
        imports: {
            localSave: hinoApp.modules.core.factories.localSave,
            request: hinoApp.modules.core.services.request,
            //message: hinoApp.modules.core.services.messages,
            hasError: hinoApp.modules.core.services.hasError
        }
    };

    angular.module(hinoApp.modules.auth.name, [
        hinoApp.modules.core.name,
        'ngRoute',
        'ngResource'
    ]);

}(angular, hinoApp));