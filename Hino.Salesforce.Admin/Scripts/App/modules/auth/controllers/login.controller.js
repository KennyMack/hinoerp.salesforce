﻿'use strict';
(function (angular, hinoApp) {
    function LoginCtrl(
        $rootScope,
        $scope,
        $routeParams,
        $location,
        authFactory,
        hasError,
        $window) {
        let vm = this;
        vm.user = {
            user_email: '',
            username: '',
            email: '',
            pass: '',
            establishmentkey: '',
            grant_type: 'password',
            origin_login: 'admin'
        };

        vm.init = function () {
            hasError.clearError();
            authFactory.logOut();
        };

        vm.hasError = function (field) {
            return hasError.hasError(field);
        };

        vm.getErrorMessage = function (field) {
            return hasError.getErrorMessage(field);
        };

        vm.btnCreate = function () {
            $location.path(hinoApp.modules.auth.routes.register);
        };

        vm.btnAuthenticate = function () {

            hasError.clearError();
            $rootScope.$broadcast('evt__showLoad', true);
            let token = '';

            vm.user.establishmentkey = $('#HinoId').val();
            vm.user.username = /@/.test(vm.user.user_email) ? '' : vm.user.user_email;
            vm.user.email = /@/.test(vm.user.user_email) ? vm.user.user_email : '';            

            authFactory.authenticateAdmin(vm.user.username, vm.user.pass,
                vm.user.email, vm.user.establishmentkey, vm.user.grant_type,
                vm.user.origin_login)
                .then(function (data) {
                    token = data.data;
                    return authFactory.setToken(token.access_token.toString());
                })
                .then(function () {
                    return authFactory.setRefreshToken(token.refresh_token.toString());
                })
                .then(function () {
                    return authFactory.setEstablishmentKey(vm.user.establishmentkey);
                })
                .then(function () {
                    $window.location.href = "/Home";
                    
                })
                .catch(function (data) {
                    try {
                        hasError.addError(data.data.error,
                            data.data.error_description);
                        alert(data.data.error_description);
                    } catch {
                        alert('Não foi possível realizar o login, verifique os campos.');
                    }
                    $scope.$apply(function () {
                        $rootScope.$broadcast('evt__showLoad', false);
                    });
                });
        };
    }

    LoginCtrl.$inject = [
        '$rootScope',
        '$scope',
        '$routeParams',
        '$location',
        hinoApp.modules.auth.factories.authentication,
        hinoApp.modules.auth.imports.hasError,
       '$window'
    ];

    angular.module(hinoApp.modules.auth.name)
        .controller(hinoApp.modules.auth.controllers.login.name, LoginCtrl);
}(angular, hinoApp));