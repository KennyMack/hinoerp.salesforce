﻿'use strict';
(function (angular, hinoApp) {
    function authenticationFactory(LOCALNAME, request, localSave, $interval) {
        function authenticate(usr, pass, email, establishmentkey, grant_type, origin_login) {
            let user = {
                email,
                username: usr,
                password: pass,
                grant_type,
                establishmentkey
            };
            return request._post(hinoApp.modules.auth.routes.authenticate, user);
        }

        function authenticateAdmin(usr, pass, email, establishmentkey, grant_type, origin_login) {
            let user = {
                email,
                username: usr,
                password: pass,
                grant_type,
                establishmentkey,
                origin_login
            };
            return request._postFormEncoded(hinoApp.modules.auth.routes.admin.authenticate,
                user);
        }

        function setToken(value) {
            return new Promise(function (resolve, reject) {
                var i = $interval(function () {
                    localSave.setValueLS(LOCALNAME.USER_TOKEN, value);
                    $interval.cancel(i);
                    resolve();
                }, 2000);
            });
        }

        function setRefreshToken(value) {
            return new Promise(function (resolve, reject) {
                var i = $interval(function () {
                    localSave.setValueLS(LOCALNAME.REFRESH_TOKEN, value);
                    $interval.cancel(i);
                    resolve();
                }, 2000);
            });
        }

        function setEstablishmentKey(value) {
            return new Promise(function (resolve, reject) {
                var i = $interval(function () {
                    localSave.setValueLS(LOCALNAME.ESTABLISHMENTKEY, value);
                    $interval.cancel(i);
                    resolve();
                }, 2000);
            });
        }

        function changePass(user) {
            return request._post(hinoApp.modules.auth.routes.change, user);
        }

        function forgotPass(user) {
            return request._post(hinoApp.modules.auth.routes.forgot, user);
        }

        function recoverPass(user) {
            return request._post(hinoApp.modules.auth.routes.recover, user);
        }

        function register(user) {
            return request._post(hinoApp.modules.auth.routes.user, user);
        }

        function credential() {
            return request._get(hinoApp.modules.auth.routes.me);
        }

        function logOut() {
            localSave.removeValueLS(LOCALNAME.USER_TOKEN);
            localSave.removeValueLS(LOCALNAME.REFRESH_TOKEN);
            localSave.removeValueLS(LOCALNAME.ESTABLISHMENTKEY);
        }

        function isAuthenticated() {
            var tok = localSave.getValueLS(LOCALNAME.USER_TOKEN);
            return tok !== null;
        }

        return {
            authenticate: authenticate,
            authenticateAdmin: authenticateAdmin,
            setToken: setToken,
            setRefreshToken: setRefreshToken,
            setEstablishmentKey: setEstablishmentKey,
            credential: credential,
            logOut: logOut,
            isAuthenticated: isAuthenticated,
            register: register,
            changePass: changePass,
            forgotPass: forgotPass,
            recoverPass: recoverPass
        };
    }

    authenticationFactory.$inject = [
        'LOCALNAME',
        hinoApp.modules.auth.imports.request,
        hinoApp.modules.auth.imports.localSave,
        '$interval'
    ];

    angular.module(hinoApp.modules.auth.name)
        .factory(hinoApp.modules.auth.factories.authentication, authenticationFactory);
}(angular, hinoApp));