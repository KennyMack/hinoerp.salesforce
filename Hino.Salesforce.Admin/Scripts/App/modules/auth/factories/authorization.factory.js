﻿'use strict';
(function (angular, hinoApp) {

    function authorizationFactory(authentication) {
        function isLogged() {
            return new Promise(function (resolve, reject) {
                if (!authentication.isAuthenticated()) {
                    authentication.logOut(); 
                    reject('/Mordor/Login');
                }
                else {
                    authentication.credential()
                        .then(function (data) {
                            
                            if (data.status.toString() === '200') {
                                resolve(data);
                            }
                            else if (data.status.toString() === '401') {
                                authentication.logOut();
                                reject('/Mordor/Login');
                            }
                            else {
                                reject('/Mordor/Login');
                            }

                        })
                        .catch(function (err) {
                            reject('/Mordor/Login');
                        });
                }

            });
        }

        function authorize(next) {
            return new Promise(function (resolve, reject) {
                let requiresLogin = false;

                if (next.access !== undefined &&
                    next.access.hasOwnProperty('requiresLogin')) {
                    requiresLogin = next.access.requiresLogin;
                }

                if (requiresLogin) {
                    if (!authentication.isAuthenticated()) {
                        authentication.logOut();
                        reject(hinoApp.modules.auth.routes.login);
                    }
                    else {
                        authentication.credential(function (err, data, success, status) {
                            if (status === 200) {
                                resolve('');
                            }
                            else if (status === 401) {
                                authentication.logOut();
                                reject(hinoApp.modules.auth.routes.login);
                            }
                            else {
                                reject(hinoApp.modules.auth.routes.serverError(status));
                            }

                        });
                    }
                }
                else
                    resolve('');
            });

        }

        return {
            authorize: authorize,
            isLogged: isLogged
        };
    }

    authorizationFactory.$inject = [
        hinoApp.modules.auth.factories.authentication];

    angular.module(hinoApp.modules.auth.name)
        .factory(hinoApp.modules.auth.factories.authorization, authorizationFactory);
}(angular, hinoApp));