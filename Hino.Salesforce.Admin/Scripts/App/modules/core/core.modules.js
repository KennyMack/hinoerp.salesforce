﻿'use strict';

(function (angular, hinoApp) {
    hinoApp.modules.core = {
        name: 'hinoApp.core',
        services: {
            request: 'requestService',
            hasError: 'hasErrorService'
        },
        factories: {
            localSave: 'localSaveFactory'
        },
        components: {
        },
        filters: {
        },
        imports: {

        }
    };

    angular.module(hinoApp.modules.core.name, []);

}(angular, hinoApp));