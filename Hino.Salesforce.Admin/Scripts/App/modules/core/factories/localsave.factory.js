﻿'use strict';

(function (angular, hinoApp) {

	function localSaveFactory($window) {
		return {
			getValueLS: function (key) {
				return $window.localStorage.getItem(key);
			},

			setValueLS: function (key, value) {
				$window.localStorage.setItem(key, value);
			},

			getJSONValueLS: function (key) {
				try {
					return JSON.parse($window.localStorage.getItem(key));
				}
				catch (e) {
					return {};
				}
			},

			setJSONValueLS: function (key, value) {
				$window.localStorage.setItem(key, JSON.stringify(value));
			},

			removeValueLS: function (key) {
				$window.localStorage.removeItem(key);
			}
		};
	}

	localSaveFactory.$inject = ['$window'];

    angular.module(hinoApp.modules.core.name)
        .service(hinoApp.modules.core.factories.localSave, localSaveFactory);
}(angular, hinoApp));
