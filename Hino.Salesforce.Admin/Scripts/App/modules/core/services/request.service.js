﻿'use strict';

(function (angular, hinoApp) {

    function requestFactory(
        BASEURLS,
        LOCALNAME,
        localsave,
        $http,
        $httpParamSerializerJQLike) {

        function getHeader() {
            return {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + localsave.getValueLS(LOCALNAME.USER_TOKEN),
                'refresh_token': localsave.getValueLS(LOCALNAME.REFRESH_TOKEN)
            };
        }

        function getHeaderFormEncoded() {
            return {
                'Content-Type': 'application/x-www-form-urlencoded'
            };
        }

        function getHeaderBinary() {
            return {
                // 'Content-Type': 'application/epub+zip',
                'Authorization': 'Bearer ' + localsave.getValueLS(LOCALNAME.USER_TOKEN),
                'refresh_token': localsave.getValueLS(LOCALNAME.REFRESH_TOKEN)
            };
        }

        function requestFormEncoded(method, url, data) {
            let enc = $httpParamSerializerJQLike(data);
            return $http({
                method: method,
                url: url,
                data: enc,
                headers: getHeaderFormEncoded()
            });
        }

        function request(method, url, data) {
            return $http({
                method: method,
                url: url,
                data: data,
                headers: getHeader()
            });
        }

        function requestBinary(method, url, data) {
            return $http({
                method: method,
                url: url,
                data: data,
                headers: getHeaderBinary()
            });
        }

        function _get(url) {
            return new Promise(function (resolve, reject) {
                let uri = BASEURLS.BASE_API + url;
                request('GET', uri, {})
                    .then(function (data) {
                        resolve(data);
                    })
                    .catch(function (err) {
                        reject(err);
                    });
            });
        }

        function _getBinary(url, data) {
            return new Promise(function (resolve, reject) {
                let uri = url;
                requestBinary('GET', uri, data || {})
                    .then(function (data) {
                        resolve(data);
                    })
                    .catch(function (data) {
                        reject(data);
                    });
            });
        }
        function _postFormEncoded(url, data) {
            return new Promise(function (resolve, reject) {
                let uri = BASEURLS.BASE_API + url;
                requestFormEncoded('POST', uri, data || {})
                    .then(function (data) {
                        resolve(data);
                    })
                    .catch(function (data) {
                        reject(data);
                    });
            });
        }
        function _post(url, data) {
            return new Promise(function (resolve, reject) {
                let uri = BASEURLS.BASE_API + url;
                request('POST', uri, data || {})
                    .then(function (data) {
                        resolve(data);
                    })
                    .catch(function (data) {
                        reject(data);
                    });
            });
        }

        function _put(url, data) {
            return new Promise(function (resolve, reject) {
                let uri = BASEURLS.BASE_API + url;
                request('PUT', uri, data || {})
                    .then(function (data) {
                        resolve(data);
                    })
                    .catch(function (data) {
                        reject(data);
                    });
            });
        }

        function _delete(url, data) {
            return new Promise(function (resolve, reject) {
                let uri = BASEURLS.BASE_API + url;
                request('DELETE', uri, data || {})
                    .then(function (data) {
                        resolve(data);
                    })
                    .catch(function (data) {
                        reject(data);
                    });
            });
        }        

        return {
            _get: _get,
            _getBinary: _getBinary,
            _post: _post,
            _postFormEncoded: _postFormEncoded,
            _put: _put,
            _delete: _delete,
            _getHeader: getHeader
        };
    }

    requestFactory.$inject = [
        'BASEURLS',
        'LOCALNAME',
        hinoApp.modules.core.factories.localSave,
        '$http',
        '$httpParamSerializerJQLike'
    ];

    angular.module(hinoApp.modules.core.name)
        .service(hinoApp.modules.core.services.request, requestFactory);
}(angular, hinoApp));
