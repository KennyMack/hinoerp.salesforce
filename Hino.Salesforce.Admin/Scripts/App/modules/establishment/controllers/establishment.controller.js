﻿'use strict';
(function (angular, hinoApp) {
    function establishmentCtrl(
        $rootScope,
        $location,
        $scope,
        $window,
        establishmentFactory,
        devicesFactory) {
        var vm = this;
        vm.listEstablishments = [];
        vm.listErrors = [];

        vm.establishment = {
            RazaoSocial: '',
            NomeFantasia: '',
            Email: '',
            Phone: '',
            CNPJCPF: '',
            Devices: '',
            PIS: '',
            COFINS: '',
            GEEstabDevices: [
            ],
            Id: '',
            EstablishmentKey: '',
            UniqueKey: '',
            Created: '',
            Modified: '',
            IsActive: true
        };

        vm.listDevices = [];

        vm.initList = function () {
            vm.showModal = false;
            vm.showLoad = false;
            $rootScope.$broadcast('evt__showLoad', true);

            establishmentFactory.listAllAdmin()
                .then(function (data) {
                    $scope.$apply(function () {
                        vm.listEstablishments = data.data.Data.data.Results;
                        $rootScope.$broadcast('evt__showLoad', false);
                    });
                })
                .catch(function (err) {
                    $scope.$apply(function () {
                        $rootScope.$broadcast('evt__showLoad', false);
                    });
                });
        };

        vm.initEdit = function () {
            vm.showModal = false;
            vm.showLoad = false;
            $rootScope.$broadcast('evt__showLoad', true);
            
            let search = hinoApp.UrlQueryString.getUrlValues();
            vm.establishment.EstablishmentKey = search.pEstablishmentKey;
            vm.establishment.UniqueKey = search.pKey;
            vm.establishment.Id = search.pId;

            devicesFactory.getByEstablishmentAdmin(vm.establishment.EstablishmentKey,
                vm.establishment.UniqueKey, vm.establishment.Id)
                .then(function (devices) {
                    vm.listDevices = devices.data.Data.data;

                    return establishmentFactory.getByIdAdmin(vm.establishment.EstablishmentKey,
                        vm.establishment.UniqueKey, vm.establishment.Id);
                })
                .then(function (establishment) {
                    try {
                        vm.establishment = establishment.data.Data.data;
                    } catch {
                        alert('Não foi possível recuperar as informações da empresa');
                    }
                    $scope.$apply(function () {
                        $rootScope.$broadcast('evt__showLoad', false);
                    });
                })
                .catch(function (err) {
                    alert('Não foi possível recuperar as informações da empresa');
                    $scope.$apply(function () {
                        $rootScope.$broadcast('evt__showLoad', false);
                    });
                });
        };

        vm.errorResponseForm = function (errors) {
            errors.forEach(function (r) {
                try {
                    $scope.frmCreateEstablishment[r.Field].MessageServer = r.Messages[0];
                } catch {
                    console.log(r.Messages[0]);
                }
            });
        };

        vm.deleteEstablishment = function (establishmentkey, uniquekey, id) {
            if (confirm('confirma a exclusão do estabelecimento selecionado?')) {
                $rootScope.$broadcast('evt__showLoad', true);
                establishmentFactory.deleteAdmin(id, establishmentkey, uniquekey)
                    .then(function (data) {
                        $window.location.href = '/Establishment/Index';
                    })
                    .catch(function (err) {
                        alert(err.data.Data.error[0].Messages[0]);
                        $scope.$apply(function () {
                            $rootScope.$broadcast('evt__showLoad', false);
                        });
                    });                
            }
        };

        vm.saveEstablishment = function () {
            if ($scope.frmCreateEstablishment.$valid) {
                vm.listErrors = [];
                $rootScope.$broadcast('evt__showLoad', true);
                establishmentFactory.createAdmin(vm.establishment)
                    .then(function (data) {
                        console.log('data')
                        console.log(data);
                        
                        if (!data.data.Data.success) {
                            let err = data.data.Data.error;
                            vm.listErrors = err;
                            vm.errorResponseForm(vm.listErrors);
                        }
                        else {
                            $window.location.href = '/Establishment/Index';
                        }

                        $scope.$apply(function () {
                            $rootScope.$broadcast('evt__showLoad', false);
                        });
                    })
                    .catch(function (err) {
                        console.log('err');
                        console.log(err);
                        let er = err.data.Data.error;
                        vm.listErrors = er;
                        vm.errorResponseForm(vm.listErrors);
                        $scope.$apply(function () {
                            $rootScope.$broadcast('evt__showLoad', false);
                        });
                    });
            }
        };

        vm.updateEstablishment = function () {
            if ($scope.frmCreateEstablishment.$valid) {
                vm.listErrors = [];
                $rootScope.$broadcast('evt__showLoad', true);

                establishmentFactory.updateAdmin(vm.establishment.Id, vm.establishment)
                    .then(function (data) {
                        if (!data.data.Data.success) {
                            let err = data.data.Data.error;
                            vm.listErrors = err;
                            vm.errorResponseForm(vm.listErrors);
                        }
                        else {
                            $window.location.href = '/Establishment/Index';
                        }
                        $scope.$apply(function () {
                            $rootScope.$broadcast('evt__showLoad', false);
                        });
                    })
                    .catch(function (err) {
                        let er = err.data.Data.error;
                        vm.listErrors = er;
                        vm.errorResponseForm(vm.listErrors);
                        $scope.$apply(function () {
                            $rootScope.$broadcast('evt__showLoad', false);
                        });
                    });
            }
        };
    }

    establishmentCtrl.$inject = [
        '$rootScope',
        '$location',
        '$scope',
        '$window',
        hinoApp.modules.establishment.factories.establishment,
        hinoApp.modules.establishment.factories.devices
    ];  

    angular.module(hinoApp.modules.establishment.name)
        .controller(hinoApp.modules.establishment.controllers.establishment.name, establishmentCtrl);
} (angular, hinoApp));