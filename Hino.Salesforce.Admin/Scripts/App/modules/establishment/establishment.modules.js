﻿'use strict';
(function (angular, hinoApp) {
    hinoApp.modules.establishment = {
        name: 'hinoApp.establishment',
        controllers: {
            establishment: {
                name: 'EstablishmentController',
                nameas: 'EstablishmentCtrl'
            }
        },
        routes: {
            admin: {
                establishment: {
                    list: hinoApp.URLS.ADMIN.ESTABLISHMENTS.LIST(),
                    getbyid: hinoApp.URLS.ADMIN.ESTABLISHMENTS.GETBYID(),
                    create: hinoApp.URLS.ADMIN.ESTABLISHMENTS.CREATE(),
                    update: hinoApp.URLS.ADMIN.ESTABLISHMENTS.UPDATE(),
                    delete: hinoApp.URLS.ADMIN.ESTABLISHMENTS.DELETE()
                },
                devices: {
                    list: hinoApp.URLS.ADMIN.DEVICES.LIST(),
                    getbyid: hinoApp.URLS.ADMIN.DEVICES.GETBYID(),
                    getbyEstablishment: hinoApp.URLS.ADMIN.DEVICES.GETBYESTABLISHMENT(),
                    create: hinoApp.URLS.ADMIN.DEVICES.CREATE(),
                    update: hinoApp.URLS.ADMIN.DEVICES.UPDATE(),
                    delete: hinoApp.URLS.ADMIN.DEVICES.DELETE()
                }
            }
        },
        factories: {
            establishment: 'establishmentFactory',
            devices: 'devicesFactory'
        },
        templates: {
        },
        imports: {
            localSave: hinoApp.modules.core.factories.localSave,
            authorization: hinoApp.modules.auth.factories.authorization,
            request: hinoApp.modules.core.services.request
        }
    };    

    angular.module(hinoApp.modules.establishment.name, [
        hinoApp.modules.core.name
    ]);

}(angular, hinoApp));