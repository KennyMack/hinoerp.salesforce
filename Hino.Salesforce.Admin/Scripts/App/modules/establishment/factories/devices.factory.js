﻿'use strict';
(function (angular, hinoApp) {

    function devicesFactory (request) {

        function listAllAdmin() {
            return request._get(hinoApp.modules.establishment.routes.admin.devices.list);
        }

        function listAll() {
            return null;//request._get(hinoApp.modules.establishment.routes.list, user);

        }

        function getByIdAdmin(establishmentkey, uniquekey, id) {
            return request._get(hinoApp.modules.establishment.routes.admin.devices.getbyid(establishmentkey, uniquekey, id));
        }

        function getByEstablishmentAdmin(establishmentkey, uniquekey, id) {
            return request._get(hinoApp.modules.establishment.routes.admin.devices.getbyEstablishment(establishmentkey, uniquekey, id));
        }

        function createAdmin(establishment) {
            return request._post(hinoApp.modules.establishment.routes.admin.devices.create, establishment);

        }

        function updateAdmin(id, establishment) {
            return request._put(hinoApp.modules.establishment.routes.admin.devices.update(id), establishment);

        }

        function update() {
            return null;
        }

        function deleteAdmin(id, establishment, uniquekey) {
            return request._delete(hinoApp.modules.establishment.routes.admin.devices.delete(establishment, uniquekey, id));
        }

        return {
            listAllAdmin,
            listAll,
            getByIdAdmin,
            getByEstablishmentAdmin,
            createAdmin,
            updateAdmin,
            update,
            deleteAdmin
        };
    }

    devicesFactory.$inject = [
        hinoApp.modules.establishment.imports.request
    ];

    angular.module(hinoApp.modules.establishment.name)
        .factory(hinoApp.modules.establishment.factories.devices,
            devicesFactory);
}(angular, hinoApp));
