﻿
'use strict';
(function (angular, hinoApp) {

    function establishmentFactory(request) {

        function listAllAdmin() {
            return request._get(hinoApp.modules.establishment.routes.admin.establishment.list);
        }

        function listAll() {
            return null;//request._get(hinoApp.modules.establishment.routes.list, user);

        }

        function getByIdAdmin(establishmentkey, uniquekey, id) {
            return request._get(hinoApp.modules.establishment.routes.admin.establishment.getbyid(establishmentkey, uniquekey, id));

        }

        function createAdmin(establishment) {
            return request._post(hinoApp.modules.establishment.routes.admin.establishment.create, establishment);

        }

        function updateAdmin(id, establishment) {
            return request._put(hinoApp.modules.establishment.routes.admin.establishment.update(id), establishment);
              
        }

        function update() {
            return null;
        }

        function deleteAdmin(id, establishment, uniquekey) {
            return request._delete(hinoApp.modules.establishment.routes.admin.establishment.delete(establishment, uniquekey, id));
        }

        return {
            listAllAdmin,
            listAll,
            getByIdAdmin,
            createAdmin,
            updateAdmin,
            update,
            deleteAdmin
        };
    }

    establishmentFactory.$inject = [
        hinoApp.modules.establishment.imports.request
    ];

    angular.module(hinoApp.modules.establishment.name)
        .factory(hinoApp.modules.establishment.factories.establishment,
            establishmentFactory);
}(angular, hinoApp));
