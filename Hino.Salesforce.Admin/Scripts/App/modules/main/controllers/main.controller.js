﻿'use strict';
(function (angular, hinoApp) {
    function MainCtrl($rootScope, $window, authorization, authentication) {
        var vm = this;
        vm.showModal = false;
        vm.showLoad = false;

        var appWindow = angular.element($window);

        appWindow.bind('resize', function () {
            vm.showModal = false;
            $rootScope.$broadcast('evt__showModal', false);
            $rootScope.__showModal = false;
        });

        $rootScope.$on('evt__showModal', function (ev, value) {
            vm.showModal = value;
        });

        $rootScope.$on('evt__showLoad', function (ev, value) {
            vm.showLoad = value;
        });

        vm.isAuthenticated = function () {
            return authentication.isAuthenticated();
        };

        vm.LogOut = function () {
            console.log('LogOut ');
            $rootScope.$broadcast('evt__showLoad', true);
            authentication.logOut();
            $window.location.href = '/mordor/login';
            $rootScope.$broadcast('evt__showLoad', false);
        };

        vm.init = function () {
            vm.showModal = false;
            vm.showLoad = false;

            if (!$window.location.pathname.toUpperCase().match(/(\/MORDOR\/LOGIN)/)) {
                authorization.isLogged()
                    .then(function (url) {

                    })
                    .catch(function (err) {
                        $window.location.href = err;
                    });
            }
        };
    }

    MainCtrl.$inject = [
        '$rootScope',
        '$window',
        hinoApp.modules.main.imports.authorization,
        hinoApp.modules.main.imports.authentication
    ];

    angular.module(hinoApp.modules.main.name)
        .controller(hinoApp.modules.main.controllers.main.name, MainCtrl);
}(angular, hinoApp));
