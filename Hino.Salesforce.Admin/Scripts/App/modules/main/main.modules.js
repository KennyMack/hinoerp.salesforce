﻿'use strict';
(function (angular, hinoApp) {
    hinoApp.modules.main = {
        name: 'hinoApp.main',
        controllers: {
            main: {
                name: 'MainController',
                nameas: 'MainCtrl'
            }
        },
        routes: {
        },
        factories: {
        },
        templates: {
        },
        imports: {
            authorization: hinoApp.modules.auth.factories.authorization,
            authentication: hinoApp.modules.auth.factories.authentication
        }
    };

    angular.module(hinoApp.modules.main.name, [
        'ngRoute'
    ]);

}(angular, hinoApp));