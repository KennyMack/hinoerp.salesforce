﻿'use strict';
(function (hinoApp) {
    let obj = {};
    let masc = null;
    hinoApp.InputMask = {

        fMasc: function (objeto, mascara) {
            obj = objeto;
            masc = mascara;
            setTimeout("hinoApp.InputMask.fMascEx()", 1);
        },
        fMascEx: function () {
            obj.value = masc(obj.value);
        },
        mTel: function (tel) {
            tel = tel.replace(/\D/g, "");
            tel = tel.replace(/^(\d)/, "($1");
            tel = tel.replace(/(.{3})(\d)/, "$1) $2");
            if (tel.length === 10) {
                tel = tel.replace(/(.{1})$/, "-$1");
            } else if (tel.length === 11) {
                tel = tel.replace(/(.{2})$/, "-$1");
            } else if (tel.length === 12) {
                tel = tel.replace(/(.{3})$/, "-$1");
            } else if (tel.length === 13) {
                tel = tel.replace(/(.{4})$/, "-$1");
            } else if (tel.length > 13) {
                tel = tel.replace(/(.{4})$/, "-$1");
            }
            return tel;
        },
        mCNPJ: function (cnpj) {
            cnpj = cnpj.replace(/\D/g, "");
            cnpj = cnpj.replace(/^(\d{3})(\d)/, "$1.$2");
            cnpj = cnpj.replace(/^(\d{3})\.(\d{3})(\d)/, "$1.$2.$3");
            cnpj = cnpj.replace(/\.(\d{3})(\d)/, ".$1/$2");
            cnpj = cnpj.replace(/(\d{4})(\d)/, "$1-$2");
            return cnpj;
        },
        mCPF: function (cpf) {
            cpf = cpf.replace(/\D/g, "");
            cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2");
            cpf = cpf.replace(/(\d{3})(\d)/, "$1.$2");
            cpf = cpf.replace(/(\d{3})(\d{1,2})$/, "$1-$2");
            return cpf;
        },
        mCEP: function (cep) {
            cep = cep.replace(/\D/g, "");
            cep = cep.replace(/^(\d{2})(\d)/, "$1.$2");
            cep = cep.replace(/\.(\d{3})(\d)/, ".$1-$2");
            return cep;
        },
        mNum: function (num) {
            num = num.replace(/\D/g, "");
            return num;
        }
    };
}(hinoApp));
