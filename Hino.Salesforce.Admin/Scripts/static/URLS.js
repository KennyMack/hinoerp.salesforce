﻿'use strict';
(function (hinoApp) {
    hinoApp.URLS = {
        HOME: function () {
            return '/';
        },
        USER: function () {
            return '/users';
        },
        USER_DETAIL_PAGE: function () {
            return function (username) {
                return '/user/' + username;
            };
        },
        USER_DETAIL: function () {
            return function (username) {
                return '/user/' + username;
            };
        },
        LOGIN: function () {
            return '/auth/mordor/login';
        },
        CHANGE_PASS: function () {
            return '/auth/mordor/change';
        },
        FORGOT_PASS: function () {
            return '/auth/mordor/forgot';
        },
        RECOVER_PASS: function () {
            return function (param) {
                return '/auth/mordor/recover/' + param;
            };
        },
        REGISTER: function () {
            return '/mordor/register';
        },
        LOGINNEXT: function () {
            return function () {
                return '/mordor/login/:next';
            };

        },
        MORDOR: {
            AUTHENTICATION: function () {
                return '/auth/mordor/login';
            },
            ME: function () {
                return '/auth/mordor/me';
            },
            CHANGE: function () {
                return '/auth/mordor/change';
            },
            FORGOT: function () {
                return '/auth/mordor/forgot';
            },
            RECOVER: function () {
                return '/auth/mordor/recover';
            },
            ADMIN: {
                AUTHENTICATION: function () {
                    return '/auth/mordor/login';
                },
                ME: function () {
                    return '/auth/mordor/me';
                },
                CHANGE: function () {
                    return '/auth/mordor/change';
                },
                FORGOT: function () {
                    return '/auth/mordor/forgot';
                },
                RECOVER: function () {
                    return '/auth/mordor/recover';
                }                
            }
        },
        ADMIN: {
            ESTABLISHMENTS: {
                LIST: function () {
                    return '/admin/establishments/all';
                },
                GETBYID: function () {
                    return function (establishment, uniquekey, id) {
                        return `/admin/establishments/${establishment}/key/${uniquekey}/id/${id}`;
                    };
                },
                CREATE: function () {
                    return '/admin/establishments/save';
                },
                UPDATE: function () {
                    return function (id) {
                        return `/admin/establishments/id/${id}/save`;
                    };
                },
                DELETE: function () {
                    return function (establishment, uniquekey, id) {
                        return `/admin/establishments/${establishment}/key/${uniquekey}/id/${id}/delete`;
                    };
                }
            },
            DEVICES: {
                LIST: function () {
                    return '/admin/devices/all';
                },
                GETBYID: function () {
                    return function (establishment, uniquekey, id) {
                        return `/admin/devices/${establishment}/key/${uniquekey}/id/${id}`;
                    };
                },
                GETBYESTABLISHMENT: function () {
                    return function (establishment, uniquekey, id) {
                        return `/admin/devices/establishment/${establishment}/key/${uniquekey}/id/${id}`;
                    };
                },
                CREATE: function () {
                    return '/admin/devices/save';
                },
                UPDATE: function () {
                    return function (id) {
                        return `/admin/devices/id/${id}/save`;
                    };
                },
                DELETE: function () {
                    return function (establishment, uniquekey, id) {
                        return `/admin/devices/${establishment}/key/${uniquekey}/id/${id}/delete`;
                    };
                }
            }
        },
        NOTAUTHORIZED: function () {
            return '/not-authorized';
        },
        SERVERERROR: function () {
            return function (code) {
                if (code)
                    return '/server-error/' + code;
                else
                    return '/server-error/:errorCode';
            };
        },
        NOTFOUND: function () {
            return '/not-found';
        },
        USERS: function () {
            return function (user) {
                if (user)
                    return '/users/' + user;
                else
                    return '/users';
            };
        }
    };
}(hinoApp));
