﻿'use strict';
(function (hinoApp) {
    hinoApp.UrlQueryString = {
        getUrlValues: function () {
            /*let values = [], hash;
            let hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');
            for (let i = 0; i < hashes.length; i++) {
                hash = hashes[i].slice('=');
                values.push(hash[0]);
                values[hash[0]] = hash[1];
            }*/

            let parameters = {};
            let param = [];
            let query = window.location.search.substring(1);
            let queries = query.split('&');

            for (var i = 0; i < queries.length; i++) {
                try {
                    param = queries[i].split('=');
                    parameters[param[0]] = param[1] || '';
                }
                catch {
                    try {
                        param = queries[i].split('=');
                        parameters[param[0] + '_' + i] = param[1] || '';

                    } catch  {
                        continue;
                    }
                }
            }

            return parameters;
        },
        getUrlValue: function (param) {
            try {
                return this.getUrlValues()[param];

            } catch {
                return null;
            }
        }
    };
}(hinoApp));