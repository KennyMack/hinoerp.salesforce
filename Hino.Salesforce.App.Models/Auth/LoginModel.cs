﻿using Hino.Salesforce.App.Utils.Attributes;

namespace Hino.Salesforce.App.Models.Auth
{
    [EndPoint("Auth/Mordor/login")]
    public class LoginModel : BaseEntity
    {
        [DisplayField]
        public string UserOrEmail { get; set; }
        [DisplayField]
        public string Password { get; set; }
    }
}
