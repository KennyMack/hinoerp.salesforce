﻿using Hino.Salesforce.App.Models.Interfaces;
using Hino.Salesforce.App.Utils.Attributes;
using SQLite;
using System;

namespace Hino.Salesforce.App.Models
{
    public class BaseEntity : IBaseEntity
    {
        public BaseEntity()
        {

        }

        [PrimaryKey]
        [DisplayField]
        public long Id { get; set; }
        [DisplayField]
        public long? IdApi { get; set; }
        [DisplayField]
        public string EstablishmentKey { get; set; }
        [DisplayField]
        public string UniqueKey { get; set; }
        [DisplayField]
        public DateTime Created { get; set; }
        [DisplayField]
        public DateTime Modified { get; set; }
        [DisplayField]
        public bool IsActive { get; set; }

    }
}
