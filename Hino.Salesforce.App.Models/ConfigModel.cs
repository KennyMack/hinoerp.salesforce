﻿using System;

namespace Hino.Salesforce.App.Models
{
    public class ConfigModel : BaseEntity
    {
        public string UserKey { get; set; }
        public bool SyncWIFI { get; set; }
        public bool SyncOnStart { get; set; }
        public bool SyncOnSave { get; set; }
        public int IntervalSync { get; set; }
        public DateTime LastSync { get; set; }
    }
}
