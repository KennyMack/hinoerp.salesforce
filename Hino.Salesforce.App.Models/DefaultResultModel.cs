﻿using Hino.Salesforce.App.Utils.Exceptions;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Hino.Salesforce.App.Models.Auth
{

    public class DefaultResultModel
    {
        public DefaultResultModel()
        {
            error = new List<ModelException>();
        }

        public int status { get; set; }
        public bool success { get; set; }
        public JContainer data { get; set; }
        public List<ModelException> error { get; set; }
    }
}
