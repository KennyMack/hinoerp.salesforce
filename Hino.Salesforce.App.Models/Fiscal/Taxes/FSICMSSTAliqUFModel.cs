﻿using Hino.Salesforce.App.Utils.Attributes;

namespace Hino.Salesforce.App.Models.Fiscal.Taxes
{
    [EndPoint("Fiscal/Aliquota/{pEstablishmentKey}")]
    public class FSICMSSTAliqUF : BaseEntity
    {
        [RequiredField]
        [DisplayField]
        public string UF { get; set; }

        [RequiredField]
        [DisplayField]
        public string NCM { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal ALIQICMSST { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal ALIQICMS { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal MVA { get; set; }
    }
}
