﻿using Hino.Salesforce.App.Models.General.Demograph;
using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.Utils.Attributes;
using SQLite;
using System.Collections.Generic;

namespace Hino.Salesforce.App.Models.General.Business
{
    [EndPoint("General/Business/Enterprises/{pEstablishmentKey}")]
    public class GEEnterprisesModel : BaseEntity
    {
        [DisplayField]
        [RequiredField]
        public string RazaoSocial { get; set; }
        [DisplayField]
        [RequiredField]
        public string NomeFantasia { get; set; }
        [DisplayField]
        [RequiredField]
        public string CNPJCPF { get; set; }
        [DisplayField]
        [RequiredField]
        public int Type { get; set; }
        public string TypeDescription
        {
            get => Type == 0 ? "Pessoa Física" : "Pessoa Jurídica";
        }
        [DisplayField]
        [RequiredField]
        public int Status { get; set; }
        public string StatusDesc
        {
            get
            {
                switch (Status)
                {
                    case 1:
                        return "Ativo";
                    case 2:
                        return "Inativo";
                    case 3:
                        return "Bloqueado";
                    default:
                        return "Novo";
                }
            }
        }

        /// <summary>
        /// 0 - Aguardando sincronizacao
        /// 1 - Sincronizado
        /// 2 - Integrado
        /// </summary>
        [DisplayField]
        [RequiredField]
        public int StatusSinc { get; set; }
        /// <summary>
        /// 0 - Aguardando sincronizacao
        /// 1 - Sincronizado
        /// 2 - Integrado
        /// </summary>
        public string StatusSincDesc
        {
            get
            {
                switch (StatusSinc)
                {
                    case 1:
                        return "Sincronizado";
                    case 2:
                        return "Integrado";
                    default:
                        return "Aguardando sincronização";
                }
            }
        }
        /// <summary>
        /// 0 - Aguardando sincronizacao
        /// 1 - Sincronizado
        /// 2 - Integrado
        /// </summary>
        public string StatusSincColor
        {
            get
            {
                switch (StatusSinc)
                {
                    case 1:
                        return "#05a05a";
                    case 2:
                        return "#89d2ff";
                    default:
                        return "#ffd700";
                }
            }
        }

        [DisplayField]
        public string CellPhone { get; set; }

        [DisplayField]
        public string Phone { get; set; }

        [DisplayField]
        public string Email { get; set; }

        [DisplayField]
        [RequiredField]
        public string Address { get; set; }

        [DisplayField]
        public string Complement { get; set; }

        [DisplayField]
        public string District { get; set; }

        [DisplayField]
        public string Num { get; set; }

        [DisplayField]
        public string ZipCode { get; set; }

        [DisplayField]
        public string Site { get; set; }


        [DisplayField]
        public long? CountryID { get; set; }
        [Ignore]
        public virtual GECountriesModel GECountries { get; set; }


        [DisplayField]
        public long? StateID { get; set; }
        [Ignore]
        public virtual GEStatesModel GEStates { get; set; }


        [DisplayField]
        public long? CityID { get; set; }
        [Ignore]
        public virtual GECitiesModel GECities { get; set; }

        [Ignore]
        public virtual ICollection<VEOrdersModel> VEOrders { get; set; }

        public string DisplayName
        {
            get
            {
                if (Id < 0)
                    return $"(NOVO) - {RazaoSocial}";

                return $"{Id} - {RazaoSocial}";
            }
        }

        public string Initials { get; set; }
        /*{
            get
            {
                var name = RazaoSocial.Trim().Split(' ');
                var clName = string.Join("", name);

                var rand = new Random();
                //var charA = (int)Math.Ceiling((decimal)rand.Next(0, RazaoSocial.Length - 1));
                var charB = (int)Math.Floor((decimal)rand.Next(0, clName.Length));

                return $"{RazaoSocial[0]}{clName[charB]}".ToUpper();
            }
        }*/

        public string CNPJCPFType
        {
            get => $"{CNPJCPF} / {TypeDescription}";

        }
        public bool DisplayPhone { get => !string.IsNullOrEmpty(Phone) || !string.IsNullOrEmpty(CellPhone); }

        [DisplayField]
        //[ForeignKey(typeof(GEPaymentConditionModel))]
        //[OneToOne]
        public long? PayConditionId { get; set; }
        [Ignore]
        public virtual GEPaymentConditionModel GEPaymentCondition { get; set; }

        [RequiredField]
        [DisplayField]
        public string UF { get; set; }

        public long RegionId { get; set; }

        public long UserId { get; set; }
    }
}

