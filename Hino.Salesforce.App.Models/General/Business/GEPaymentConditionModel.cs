﻿using Hino.Salesforce.App.Utils.Attributes;
using SQLite;
using SQLiteNetExtensions.Attributes;

namespace Hino.Salesforce.App.Models.General.Business
{
    [EndPoint("General/Business/Payment/Condition/{pEstablishmentKey}")]
    public class GEPaymentConditionModel : BaseEntity
    {
        public GEPaymentConditionModel()
        {
            //this.VEOrders = new HashSet<VEOrdersModel>();
        }

        [ForeignKey(typeof(GEPaymentTypeModel))]
        public long TypePayID { get; set; }
        [Ignore]
        [OneToOne]
        public virtual GEPaymentTypeModel GEPaymentType { get; set; }

        public string Description { get; set; }
        //[Ignore]
        //public virtual ICollection<VEOrdersModel> VEOrders { get; set; }
    }
}
