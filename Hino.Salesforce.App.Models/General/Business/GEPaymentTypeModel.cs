﻿using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.Utils.Attributes;
using SQLite;
using System.Collections.Generic;

namespace Hino.Salesforce.App.Models.General.Business
{
    [EndPoint("General/Business/Payment/Type/{pEstablishmentKey}")]
    public class GEPaymentTypeModel : BaseEntity
    {
        public GEPaymentTypeModel()
        {
            //this.GEPaymentCondition = new List<GEPaymentConditionModel>();
            this.VEOrders = new List<VEOrdersModel>();
        }

        public string Description { get; set; }
        /*[Ignore]
        [OneToMany]
        public virtual List<GEPaymentConditionModel> GEPaymentCondition { get; set; }*/
        [Ignore]
        public virtual List<VEOrdersModel> VEOrders { get; set; }
    }
}
