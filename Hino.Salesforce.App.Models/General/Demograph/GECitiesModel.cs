﻿using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Utils.Attributes;
using SQLite;
using System.Collections.Generic;

namespace Hino.Salesforce.App.Models.General.Demograph
{
    [EndPoint("General/Demograph/Cities/{pEstablishmentKey}")]
    public class GECitiesModel : BaseEntity
    {
        public GECitiesModel()
        {
            this.GEEnterprises = new HashSet<GEEnterprisesModel>();
        }

        public string Name { get; set; }
        public string IBGE { get; set; }
        public string DDD { get; set; }

        public long StateID { get; set; }
        [Ignore]
        public virtual GEStatesModel GEStates { get; set; }
        [Ignore]
        public virtual ICollection<GEEnterprisesModel> GEEnterprises { get; set; }
    }
}
