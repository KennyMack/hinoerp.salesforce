﻿using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Utils.Attributes;
using SQLite;
using System.Collections.Generic;

namespace Hino.Salesforce.App.Models.General.Demograph
{
    [EndPoint("General/Demograph/Countries/{pEstablishmentKey}")]
    public class GECountriesModel : BaseEntity
    {
        public GECountriesModel()
        {
            this.GEEnterprises = new HashSet<GEEnterprisesModel>();
            this.GEStates = new HashSet<GEStatesModel>();
        }
        public string Initials { get; set; }
        public string Name { get; set; }
        public string BACEN { get; set; }
        public string DDI { get; set; }

        public string Description
        {
            get
            {
                if (string.IsNullOrEmpty(Initials))
                    return $"{Name} ({Initials})";

                return Name;
            }
        }

        [Ignore]
        public virtual ICollection<GEEnterprisesModel> GEEnterprises { get; set; }
        [Ignore]
        public virtual ICollection<GEStatesModel> GEStates { get; set; }
    }
}
