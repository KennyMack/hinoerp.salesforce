﻿using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Utils.Attributes;
using SQLite;
using System.Collections.Generic;

namespace Hino.Salesforce.App.Models.General.Demograph
{
    [EndPoint("General/Demograph/States/{pEstablishmentKey}")]
    public class GEStatesModel : BaseEntity
    {
        public GEStatesModel()
        {
            this.GECities = new HashSet<GECitiesModel>();
            this.GEEnterprises = new HashSet<GEEnterprisesModel>();

        }

        public string Name { get; set; }
        public string Initials { get; set; }
        public string IBGE { get; set; }

        public string Description
        {
            get
            {
                if (string.IsNullOrEmpty(Initials))
                    return $"{Name} ({Initials})";

                return Name;
            }
        }

        public long CountryID { get; set; }
        [Ignore]
        public virtual GECountriesModel GECountries { get; set; }
        [Ignore]
        public virtual ICollection<GECitiesModel> GECities { get; set; }
        [Ignore]
        public virtual ICollection<GEEnterprisesModel> GEEnterprises { get; set; }
    }
}
