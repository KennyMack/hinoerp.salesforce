﻿using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.Utils.Attributes;
using SQLite;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Hino.Salesforce.App.Models.General
{
    [EndPoint("General/Products/{pEstablishmentKey}")]
    public class GEProductsModel : BaseEntity, INotifyPropertyChanged
    {
        public GEProductsModel()
        {
            this.VEOrderItems = new HashSet<VEOrderItemsModel>();
            this.VESalePrice = new HashSet<VESalePriceModel>();

        }

        public string ProductKey { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string Family { get; set; }
        public decimal PercIPI { get; set; }
        public decimal PercMaxDiscount { get; set; }
        public decimal Value { get; set; }
        public decimal StockBalance { get; set; }
        public int Status { get; set; }
        public string NCM { get; set; }
        public string Unit { get; set; }
        public decimal Weight { get; set; }
        public decimal PackageQTD { get; set; }
        public string SaleUnit { get; set; }
        public decimal SaleFactor { get; set; }

        public string DisplayName
        {
            get
            {
                return $"{ProductKey} - {Name}";
            }
        }

        [Ignore]
        public virtual ICollection<VEOrderItemsModel> VEOrderItems { get; set; }
        [Ignore]
        public virtual ICollection<VESalePriceModel> VESalePrice { get; set; }

        decimal _quantitySelected = 0;
        [Ignore]
        public decimal QuantitySelected
        {
            get { return _quantitySelected; }
            set { SetProperty(ref _quantitySelected, value); }
        }

        decimal _quantityConverted = 0;
        [Ignore]
        public decimal QuantityConverted
        {
            get { return _quantityConverted; }
            set { SetProperty(ref _quantityConverted, value); }
        }

        decimal _quantityPackageWeightConverted = 0;
        [Ignore]
        public decimal QuantityPackageWeightConverted
        {
            get { return _quantityPackageWeightConverted; }
            set { SetProperty(ref _quantityPackageWeightConverted, value); }
        }

        #region INotifyPropertyChanged
        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion
    }
}
