﻿using System;

namespace Hino.Salesforce.App.Models.Interfaces
{
    public interface IBaseEntity
    {
        long Id { get; set; }
        long? IdApi { get; set; }
        string EstablishmentKey { get; set; }
        string UniqueKey { get; set; }
        DateTime Created { get; set; }
        DateTime Modified { get; set; }
        bool IsActive { get; set; }
    }
}
