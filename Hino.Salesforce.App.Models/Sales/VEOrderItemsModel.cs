﻿using Hino.Salesforce.App.Models.General;
using Hino.Salesforce.App.Utils.Attributes;
using SQLite;
using SQLiteNetExtensions.Attributes;
using System.ComponentModel;

namespace Hino.Salesforce.App.Models.Sales
{
    [EndPoint("Sales/Orders/Items/{pEstablishmentKey}")]
    public class VEOrderItemsModel : BaseEntity, INotifyPropertyChanged
    {
        [ForeignKey(typeof(VEOrdersModel))]
        public long OrderID { get; set; }
        [Ignore]
        public virtual VEOrdersModel VEOrders { get; set; }

        public long ProductID { get; set; }
        [Ignore]
        public virtual GEProductsModel GEProducts { get; set; }

        public decimal TableValue { get; set; }

        [Ignore]
        public decimal TotalValue
        {
            get => Value * Quantity;
        }

        public decimal PercDiscount { get; set; }
        public string Note { get; set; }

        private decimal _Value;
        public decimal Value
        {
            get => _Value;
            set
            {
                _Value = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Value)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TotalValue)));
            }
        }

        private decimal _Quantity;
        public decimal Quantity
        {
            get => _Quantity;
            set
            {
                _Quantity = value;
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(Quantity)));
                PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(nameof(TotalValue)));
            }
        }


        public event PropertyChangedEventHandler PropertyChanged;
    }
}
