﻿using Hino.Salesforce.App.Utils.Attributes;
using SQLite;
using System.Collections.Generic;

namespace Hino.Salesforce.App.Models.Sales
{
    [EndPoint("Sales/Region/{pEstablishmentKey}")]
    public class VERegionSaleModel : BaseEntity
    {
        public VERegionSaleModel()
        {
            this.VESalePrice = new HashSet<VESalePriceModel>();
        }

        [RequiredField]
        [DisplayField]
        public string Description { get; set; }

        [DisplayField]
        public long IdERP { get; set; }

        [Ignore]
        public virtual ICollection<VESalePriceModel> VESalePrice { get; set; }

    }
}
