﻿using Hino.Salesforce.App.Models.General;
using Hino.Salesforce.App.Utils.Attributes;
using SQLite;

namespace Hino.Salesforce.App.Models.Sales
{
    [EndPoint("Sales/Prices/{pEstablishmentKey}")]
    public class VESalePriceModel : BaseEntity
    {
        public VESalePriceModel()
        {

        }

        [RequiredField]
        [DisplayField]
        public long CodPrVenda { get; set; }

        [RequiredField]
        [DisplayField]
        public long RegionId { get; set; }

        [Ignore]
        public virtual VERegionSaleModel VERegionSale { get; set; }

        [RequiredField]
        [DisplayField]
        public long ProductId { get; set; }
        [Ignore]
        public virtual GEProductsModel GEProducts { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal Value { get; set; }

    }
}
