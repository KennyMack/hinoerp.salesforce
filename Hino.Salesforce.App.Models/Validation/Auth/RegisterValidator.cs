﻿using FluentValidation;

namespace Hino.Salesforce.App.Models.Validation.Auth
{
    public class RegisterValidator : AbstractValidator<RegisterModel>
    {
        public RegisterValidator()
        {
            RuleFor(x => x.EstablishmentKey)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.ValidationMessagesResource.RequiredDefault);
            RuleFor(x => x.UserName)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.ValidationMessagesResource.RequiredUsername);
            RuleFor(x => x.UserKey)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.ValidationMessagesResource.RequiredDefault);
            RuleFor(x => x.Email)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.ValidationMessagesResource.RequiredEmail);
            RuleFor(x => x.Email)
                .NotEmpty()
                .NotNull()
                .EmailAddress()
                .WithMessage(Resources.ValidationMessagesResource.InvalidEmail);
            RuleFor(x => x.Password)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.ValidationMessagesResource.RequiredPassword);
            RuleFor(x => x.PasswordConfirm)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.ValidationMessagesResource.RequiredPassword);
            RuleFor(x => x.PasswordConfirm)
                .Equal(x => x.Password)
                .WithMessage(Resources.ValidationMessagesResource.PasswordNotEqual);
        }

    }
}
