﻿using FluentValidation;

namespace Hino.Salesforce.App.Models.Validation
{
    public class ConfigValidator : AbstractValidator<ConfigModel>
    {
        public ConfigValidator()
        {
            RuleFor(x => x.IntervalSync)
               .NotNull()
               .WithMessage(Resources.ValidationMessagesResource.RequiredDefault);
            RuleFor(x => x.IntervalSync)
                .Must(r => int.TryParse(r.ToString(), out int o))
                .WithMessage(Resources.ValidationMessagesResource.MustBeNumber);
            RuleFor(x => x.IntervalSync)
                .Must(r =>
                {
                    if (int.TryParse(r.ToString(), out int o))
                        return o > 0;
                    return false;
                })
                .WithMessage(Resources.ValidationMessagesResource.GreaterThanZero);
        }
    }
}
