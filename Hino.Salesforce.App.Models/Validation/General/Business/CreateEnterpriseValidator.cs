﻿using FluentValidation;
using Hino.Salesforce.App.Models.General.Business;

namespace Hino.Salesforce.App.Models.Validation.General.Business
{
    public class CreateEnterpriseValidator : AbstractValidator<GEEnterprisesModel>
    {
        public CreateEnterpriseValidator()
        {
            RuleFor(x => x.RazaoSocial)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.ValidationMessagesResource.RequiredDefault);

            RuleFor(x => x.NomeFantasia)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.ValidationMessagesResource.RequiredDefault);

            RuleFor(x => x.CNPJCPF)
                .Must((cnpjcpf) =>
                {
                    if (string.IsNullOrEmpty(cnpjcpf) ||
                        string.IsNullOrWhiteSpace(cnpjcpf))
                        return true;

                    if (Validators.IsCnpj(cnpjcpf))
                        return true;

                    if (Validators.IsCPF(cnpjcpf))
                        return true;

                    return false;
                })
                .WithMessage(Resources.ValidationMessagesResource.InvalidCNPJCPF);

            RuleFor(x => x.CNPJCPF)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.ValidationMessagesResource.RequiredDefault);

            RuleFor(x => x.Type)
                .NotNull()
                .WithMessage(Resources.ValidationMessagesResource.RequiredDefault);

            RuleFor(x => x.Address)
                .NotEmpty()
                .NotNull()
                .WithMessage(Resources.ValidationMessagesResource.RequiredDefault);

            RuleFor(x => x.ZipCode)
                 .Must((zipcode) =>
                 {
                     if (string.IsNullOrEmpty(zipcode) ||
                         string.IsNullOrWhiteSpace(zipcode))
                         return true;

                     if (Validators.ZipCodeValidator(zipcode))
                         return true;

                     return false;
                 })
                .WithMessage(Resources.ValidationMessagesResource.InvalidZipCode);

            RuleFor(x => x.Email)
                 .Must((email) =>
                 {
                     if (string.IsNullOrEmpty(email) ||
                         string.IsNullOrWhiteSpace(email))
                         return true;

                     if (Validators.EmailValidator(email))
                         return true;

                     return false;
                 })
                .WithMessage(Resources.ValidationMessagesResource.InvalidZipCode);
        }
    }
}
