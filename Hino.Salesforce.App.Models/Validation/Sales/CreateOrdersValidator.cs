﻿using FluentValidation;
using Hino.Salesforce.App.Models.Sales;
using System;

namespace Hino.Salesforce.App.Models.Validation.Sales
{
    public class CreateOrdersValidator : AbstractValidator<VEOrdersModel>
    {
        public CreateOrdersValidator()
        {
            RuleFor(x => x.EnterpriseID)
                .NotNull()
                .WithMessage(
                string.Format(
                    Resources.ValidationMessagesResource.RequiredDefault,
                    Resources.FieldsNameResource.RazaoSocial)
                );
            /*RuleFor(x => x.EnterpriseID)
                .Must(r =>
                {

                    if (int.TryParse(r.ToString(), out int o))
                        return o > 0;
                    return false;
                })
                .WithMessage(
                string.Format(
                    Resources.ValidationMessagesResource.RequiredDefault,
                    Resources.FieldsNameResource.RazaoSocial));*/

            RuleFor(x => x.UserID)
                .NotNull()
                .WithMessage(
                string.Format(
                    Resources.ValidationMessagesResource.RequiredDefault,
                    Resources.FieldsNameResource.UserName));

            RuleFor(x => x.UserID)
                .Must(r =>
                {
                    if (int.TryParse(r.ToString(), out int o))
                        return o > 0;
                    return false;
                })
                .WithMessage(
                string.Format(
                    Resources.ValidationMessagesResource.RequiredDefault,
                    Resources.FieldsNameResource.UserName));

            RuleFor(x => x.TypePaymentID)
                .NotNull()
                .WithMessage(
                string.Format(
                    Resources.ValidationMessagesResource.RequiredDefault,
                    Resources.FieldsNameResource.PaymentType));

            RuleFor(x => x.TypePaymentID)
                .Must(r =>
                {
                    if (int.TryParse(r.ToString(), out int o))
                        return o > 0;
                    return false;
                })
                .WithMessage(
                string.Format(
                    Resources.ValidationMessagesResource.RequiredDefault,
                    Resources.FieldsNameResource.PaymentType));

            RuleFor(x => x.PayConditionID)
                .NotNull()
                .WithMessage(
                string.Format(
                    Resources.ValidationMessagesResource.RequiredDefault,
                    Resources.FieldsNameResource.PaymentCondition));

            RuleFor(x => x.PayConditionID)
                .Must(r =>
                {
                    if (int.TryParse(r.ToString(), out int o))
                        return o > 0;
                    return false;
                })
                .WithMessage(
                string.Format(
                    Resources.ValidationMessagesResource.RequiredDefault,
                    Resources.FieldsNameResource.PaymentCondition));

            RuleFor(x => x.DeliveryDate)
                .NotNull()
                .WithMessage(
                string.Format(
                    Resources.ValidationMessagesResource.RequiredDefault,
                    Resources.FieldsNameResource.DeliveryDate));

            RuleFor(x => x.DeliveryDate)
                .Must(r =>
                {
                    if (DateTime.TryParse(r.ToString(), out DateTime o))
                        return o.Date >= DateTime.Now.Date;
                    return false;
                })
                .WithMessage(Resources.ValidationMessagesResource.InvalidDeliveryDate);
        }
    }
}
