﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Hino.Salesforce.App.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class ValidationMessagesResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal ValidationMessagesResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Hino.Salesforce.App.Resources.ValidationMessagesResource", typeof(ValidationMessagesResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Informe o usuário ou e-mail..
        /// </summary>
        public static string AuthRequiredUsernameEmail {
            get {
                return ResourceManager.GetString("AuthRequiredUsernameEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usuário e/ou senha incorreto..
        /// </summary>
        public static string AuthUserAuthFail {
            get {
                return ResourceManager.GetString("AuthUserAuthFail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Usuário não encontrado..
        /// </summary>
        public static string AuthUserNotFound {
            get {
                return ResourceManager.GetString("AuthUserNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Alteração de condição de pagamento não permitida..
        /// </summary>
        public static string CanChangePayment {
            get {
                return ResourceManager.GetString("CanChangePayment", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Celular em formato inválido.
        /// </summary>
        public static string CellphoneInvalid {
            get {
                return ResourceManager.GetString("CellphoneInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Deve ter {1} caracteres..
        /// </summary>
        public static string CharactersCount {
            get {
                return ResourceManager.GetString("CharactersCount", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Cidade não encontrada..
        /// </summary>
        public static string CityNotFound {
            get {
                return ResourceManager.GetString("CityNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Todos os dados serão excluídos, deseja continuar ?.
        /// </summary>
        public static string ConfirmClearDatabase {
            get {
                return ResourceManager.GetString("ConfirmClearDatabase", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confirma a exclusão?.
        /// </summary>
        public static string ConfirmRemove {
            get {
                return ResourceManager.GetString("ConfirmRemove", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to País não encontrado..
        /// </summary>
        public static string CountryNotFound {
            get {
                return ResourceManager.GetString("CountryNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data deve estar entre {1} e {2}..
        /// </summary>
        public static string DateRange {
            get {
                return ResourceManager.GetString("DateRange", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Selecione a empresa..
        /// </summary>
        public static string EnterpriseNotSelected {
            get {
                return ResourceManager.GetString("EnterpriseNotSelected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Buscando informações do usuário.
        /// </summary>
        public static string GetingUserInformation {
            get {
                return ResourceManager.GetString("GetingUserInformation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login para continuar.
        /// </summary>
        public static string GoToLogin {
            get {
                return ResourceManager.GetString("GoToLogin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Deve ser maior que zero..
        /// </summary>
        public static string GreaterThanZero {
            get {
                return ResourceManager.GetString("GreaterThanZero", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CNPJ inválido..
        /// </summary>
        public static string InvalidCNPJ {
            get {
                return ResourceManager.GetString("InvalidCNPJ", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CNPJ/CPF inválido..
        /// </summary>
        public static string InvalidCNPJCPF {
            get {
                return ResourceManager.GetString("InvalidCNPJCPF", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CPF inválido..
        /// </summary>
        public static string InvalidCPF {
            get {
                return ResourceManager.GetString("InvalidCPF", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data de entrega não é válida.
        /// </summary>
        public static string InvalidDeliveryDate {
            get {
                return ResourceManager.GetString("InvalidDeliveryDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to E-mail não é válido..
        /// </summary>
        public static string InvalidEmail {
            get {
                return ResourceManager.GetString("InvalidEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Valor inválido.
        /// </summary>
        public static string InvalidValue {
            get {
                return ResourceManager.GetString("InvalidValue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to CEP Inválido..
        /// </summary>
        public static string InvalidZipCode {
            get {
                return ResourceManager.GetString("InvalidZipCode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Concluído.
        /// </summary>
        public static string LoadComplete {
            get {
                return ResourceManager.GetString("LoadComplete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Processando.
        /// </summary>
        public static string LoadProcessing {
            get {
                return ResourceManager.GetString("LoadProcessing", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Iniciando.
        /// </summary>
        public static string LoadStarting {
            get {
                return ResourceManager.GetString("LoadStarting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Login não permitido..
        /// </summary>
        public static string LoginNotAllowed {
            get {
                return ResourceManager.GetString("LoginNotAllowed", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Logado com sucesso.
        /// </summary>
        public static string LoginSucceeded {
            get {
                return ResourceManager.GetString("LoginSucceeded", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Deve ter no máx. {1} caracteres..
        /// </summary>
        public static string MaxCharacters {
            get {
                return ResourceManager.GetString("MaxCharacters", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data máxima deve ser {1}..
        /// </summary>
        public static string MaxDate {
            get {
                return ResourceManager.GetString("MaxDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Valor máximo deve ser {1}..
        /// </summary>
        public static string MaxValue {
            get {
                return ResourceManager.GetString("MaxValue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Deve ter no min. {1} caracteres..
        /// </summary>
        public static string MinCharacters {
            get {
                return ResourceManager.GetString("MinCharacters", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data mínima deve ser {1}..
        /// </summary>
        public static string MinDate {
            get {
                return ResourceManager.GetString("MinDate", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Valor mínimo deve ser {1}..
        /// </summary>
        public static string MinValue {
            get {
                return ResourceManager.GetString("MinValue", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Deve ser um número..
        /// </summary>
        public static string MustBeNumber {
            get {
                return ResourceManager.GetString("MustBeNumber", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registro não encontrado.
        /// </summary>
        public static string NotFound {
            get {
                return ResourceManager.GetString("NotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Selecione o item..
        /// </summary>
        public static string OrderItemNotSelected {
            get {
                return ResourceManager.GetString("OrderItemNotSelected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Selecione o pedido..
        /// </summary>
        public static string OrderNotSelected {
            get {
                return ResourceManager.GetString("OrderNotSelected", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Senhas devem ser iguais.
        /// </summary>
        public static string PasswordNotEqual {
            get {
                return ResourceManager.GetString("PasswordNotEqual", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Já existe um pagamento com mesmo código de autorização.
        /// </summary>
        public static string PaymentExists {
            get {
                return ResourceManager.GetString("PaymentExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Telefone em formato inválido.
        /// </summary>
        public static string PhoneInvalid {
            get {
                return ResourceManager.GetString("PhoneInvalid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Quantidade inválida..
        /// </summary>
        public static string QuantityIsNotValid {
            get {
                return ResourceManager.GetString("QuantityIsNotValid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Num. caracteres deve estar entre {1} e {2}..
        /// </summary>
        public static string RangeCharacters {
            get {
                return ResourceManager.GetString("RangeCharacters", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Somente números entre {1} e {2}.
        /// </summary>
        public static string RangeOnlyDecimal {
            get {
                return ResourceManager.GetString("RangeOnlyDecimal", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Somente números inteiros entre {1} e {2}.
        /// </summary>
        public static string RangeOnlyInteger {
            get {
                return ResourceManager.GetString("RangeOnlyInteger", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Existe registro vinculado..
        /// </summary>
        public static string RegisterChild {
            get {
                return ResourceManager.GetString("RegisterChild", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registro já existe..
        /// </summary>
        public static string RegisterExists {
            get {
                return ResourceManager.GetString("RegisterExists", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Ocorreu um problema :/.
        /// </summary>
        public static string RegisterFail {
            get {
                return ResourceManager.GetString("RegisterFail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Registrado com sucesso :).
        /// </summary>
        public static string RegisterSucceeded {
            get {
                return ResourceManager.GetString("RegisterSucceeded", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Removido com sucesso..
        /// </summary>
        public static string RemoveSuccess {
            get {
                return ResourceManager.GetString("RemoveSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} é de preenchimento obrigatório..
        /// </summary>
        public static string RequiredDefault {
            get {
                return ResourceManager.GetString("RequiredDefault", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Informe o E-mail..
        /// </summary>
        public static string RequiredEmail {
            get {
                return ResourceManager.GetString("RequiredEmail", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Informe a Senha..
        /// </summary>
        public static string RequiredPassword {
            get {
                return ResourceManager.GetString("RequiredPassword", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Informe a Pessoa..
        /// </summary>
        public static string RequiredPersonId {
            get {
                return ResourceManager.GetString("RequiredPersonId", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Informe o Usuário..
        /// </summary>
        public static string RequiredUsername {
            get {
                return ResourceManager.GetString("RequiredUsername", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Salvo com sucesso..
        /// </summary>
        public static string SaveSuccess {
            get {
                return ResourceManager.GetString("SaveSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sincronizado com sucesso..
        /// </summary>
        public static string SendWithSuccess {
            get {
                return ResourceManager.GetString("SendWithSuccess", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Estado não encontrado..
        /// </summary>
        public static string StateNotFound {
            get {
                return ResourceManager.GetString("StateNotFound", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Validando dados.
        /// </summary>
        public static string ValidatingCredentials {
            get {
                return ResourceManager.GetString("ValidatingCredentials", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Valor inválido..
        /// </summary>
        public static string ValueIsNotValid {
            get {
                return ResourceManager.GetString("ValueIsNotValid", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Valor deve estar entre {1} e {2}..
        /// </summary>
        public static string ValueRange {
            get {
                return ResourceManager.GetString("ValueRange", resourceCulture);
            }
        }
    }
}
