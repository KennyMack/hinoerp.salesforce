﻿using System;
using Xamarin.Forms;

namespace Hino.Salesforce.App.Templates
{
    public class EntryControlBehavior : Behavior<Entry>
    {
        public object NextEntry { get; set; }
        private ReturnType RetType;
        protected override void OnAttachedTo(Entry entry)
        {
            RetType = entry.ReturnType;
            entry.Completed += OnCompleted;
            base.OnAttachedTo(entry);
        }

        protected override void OnDetachingFrom(Entry entry)
        {
            entry.TextChanged -= OnCompleted;
            base.OnDetachingFrom(entry);
        }

        private void OnCompleted(object sender, EventArgs args)
        {
            try
            {
                if (RetType == ReturnType.Next)
                    ((View)((Binding)NextEntry).Source)?.Focus();
                /*else if (RetType == ReturnType.Done || RetType == ReturnType.Go)
                    MessagingCenter.Send<EntryControlBehavior, string>(this, "FormEntryCompleted", "Completed");*/

            }
            catch (Exception)
            {

            }
        }
    }
}
