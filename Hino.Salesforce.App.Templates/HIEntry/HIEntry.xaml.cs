﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Templates.HIEntry
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HIEntry : ContentView
    {
        public HIEntry()
        {
            InitializeComponent();
        }
    }
}