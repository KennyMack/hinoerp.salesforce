﻿using FFImageLoading.Transformations;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Templates.ImageBox
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class HIImageBox : ContentView
    {
        public static readonly BindableProperty ImageSourceProperty =
                   BindableProperty.Create(nameof(ImageSource), typeof(ImageSource),
                               typeof(HIImageBox), defaultBindingMode: BindingMode.TwoWay,
                               propertyChanged: (bindable, oldVal, newVal) =>
                               {
                                   var matEntry = (HIImageBox)bindable;
                                   matEntry.picImageBox.Source = (ImageSource)newVal;
                               });

        public static BindableProperty ImageColorProperty =
              BindableProperty.Create(nameof(ImageColor), typeof(string),
                      typeof(HIImageBox), defaultValue: "6e6e6e",
                      propertyChanged: OnImageColorChanged);

        public ImageSource ImageSource
        {
            get { return (ImageSource)GetValue(ImageSourceProperty); }
            set { SetValue(ImageSourceProperty, value); }
        }

        public string ImageColor
        {
            get { return (string)GetValue(ImageColorProperty); }
            set { SetValue(ImageColorProperty, value); }
        }

        public event EventHandler imageBoxClicked;
        public virtual void imageBoxOn_Clicked(object sender, EventArgs e)
        {
            imageBoxClicked?.Invoke(sender, e);
        }

        public HIImageBox()
        {
            InitializeComponent();
            this.picImageBox.BindingContext = this;

            TapGestureRecognizer imageBoxTap = new TapGestureRecognizer();
            imageBoxTap.Tapped += imageBoxOn_Clicked;
            picImageBox.GestureRecognizers.Add(imageBoxTap);

            picImageBox.Transformations.Add(new TintTransformation
            {
                EnableSolidColor = true,
                HexColor = "6e6e6e"
            });
        }

        #region Convert Color
        private static Color ConvertColor(string color)
        {

            if (color.IndexOf("#") <= -1)
            {
                try
                {
                    return System.Drawing.Color.FromName(color);
                }
                catch (Exception)
                {

                }
            }
            else
            {
                try
                {
                    return Color.FromHex(color);
                }
                catch (Exception)
                {
                }
            }
            return Color.FromHex("#6e6e6e");
        }
        #endregion

        #region Convert To Hex
        private static string ConvertToHex(Color color)
        {
            int r = (int)(color.R * 256);
            int g = (int)(color.G * 256);
            int b = (int)(color.B * 256);
            int a = (int)(color.A * 256);

            return $"{r.ToString("X2")}{g.ToString("X2")}{b.ToString("X2")}";
        }
        #endregion

        private static void OnImageColorChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var control = bindable as HIImageBox;
            control.picImageBox.Transformations.Clear();
            if (newvalue.ToString().IndexOf("#") <= -1)
            {
                var color = ConvertToHex(ConvertColor(newvalue.ToString()));
                try
                {
                    control.picImageBox.Transformations.Add(new TintTransformation
                    {
                        EnableSolidColor = true,
                        HexColor = color
                    });
                }
                catch (Exception)
                {
                    control.picImageBox.Transformations.Add(new TintTransformation
                    {
                        EnableSolidColor = true,
                        HexColor = "6e6e6e"
                    });
                }
            }
            else
            {
                try
                {
                    control.picImageBox.Transformations.Add(new TintTransformation
                    {
                        EnableSolidColor = true,
                        HexColor = newvalue.ToString().Substring(1)
                    });
                }
                catch (Exception)
                {
                    control.picImageBox.Transformations.Add(new TintTransformation
                    {
                        EnableSolidColor = true,
                        HexColor = "6e6e6e"
                    });
                }

            }
        }
    }
}