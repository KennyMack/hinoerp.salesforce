﻿using FFImageLoading.Transformations;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Templates.ImageEntry
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ImageEntry : ContentView
    {

        public static BindableProperty HIBehaviorProperty =
                      BindableProperty.Create(nameof(HIBehavior), typeof(Behavior[]),
                      typeof(ImageEntry), defaultBindingMode: BindingMode.TwoWay,
                      defaultValue: null,
                      propertyChanged: (bindable, oldVal, newval) =>
                      {
                          var matEntry = (ImageEntry)bindable;
                          foreach (var item in (Behavior[])newval)
                              matEntry.txtEntry.Behaviors.Add(item);
                      });

        public static BindableProperty EntriesProperty =
                      BindableProperty.Create(nameof(Entries), typeof(ImageEntry[]),
                      typeof(ImageEntry), defaultBindingMode: BindingMode.TwoWay,
                      defaultValue: null);

        public static BindableProperty ReturnTypeProperty =
                      BindableProperty.Create(nameof(ReturnType), typeof(ReturnType),
                      typeof(ImageEntry), defaultBindingMode: BindingMode.TwoWay,
                      defaultValue: ReturnType.Default);

        public static BindableProperty TextProperty =
                      BindableProperty.Create(nameof(Text), typeof(string),
                      typeof(ImageEntry), defaultBindingMode: BindingMode.TwoWay);

        public static BindableProperty PlaceholderProperty =
              BindableProperty.Create(nameof(Placeholder),
              typeof(string), typeof(ImageEntry),
                      defaultBindingMode: BindingMode.TwoWay,
                      propertyChanged: (bindable, oldVal, newval) =>
                      {
                          var matEntry = (ImageEntry)bindable;
                          matEntry.txtEntry.Placeholder = (string)newval;
                      });

        public static BindableProperty IsPasswordProperty =
                    BindableProperty.Create(nameof(IsPassword), typeof(bool),
              typeof(ImageEntry), defaultValue: false,
                      propertyChanged: (bindable, oldVal, newVal) =>
                      {
                          var matEntry = (ImageEntry)bindable;
                          matEntry.txtEntry.IsPassword = (bool)newVal;
                      });

        public static BindableProperty DisplayLabelProperty =
                    BindableProperty.Create(nameof(DisplayLabel), typeof(bool),
                    typeof(ImageEntry), defaultValue: false,
                      propertyChanged: (bindable, oldVal, newVal) =>
                      {
                          var matEntry = (ImageEntry)bindable;
                          matEntry.label.IsVisible = (bool)newVal;
                      });

        public static BindableProperty LabelTextProperty =
              BindableProperty.Create(nameof(LabelText),
              typeof(string), typeof(ImageEntry),
                      defaultBindingMode: BindingMode.TwoWay,
                      propertyChanged: (bindable, oldVal, newval) =>
                      {
                          var matEntry = (ImageEntry)bindable;
                          matEntry.label.Text = (string)newval;
                      });

        public static BindableProperty KeyboardProperty =
              BindableProperty.Create(nameof(Keyboard), typeof(Keyboard),
                      typeof(ImageEntry), defaultValue: Keyboard.Default,
                      propertyChanged: (bindable, oldVal, newVal) =>
                      {
                          var matEntry = (ImageEntry)bindable;
                          matEntry.txtEntry.Keyboard = (Keyboard)newVal;
                      });
        public static BindableProperty AccentColorProperty =
              BindableProperty.Create(nameof(AccentColor), typeof(Color),
                      typeof(ImageEntry), defaultValue: Color.Accent);

        public static readonly BindableProperty LeftImageSourceProperty =
                   BindableProperty.Create(nameof(LeftImageSource), typeof(ImageSource),
                               typeof(ImageEntry), defaultBindingMode: BindingMode.TwoWay,
                               propertyChanged: (bindable, oldVal, newVal) =>
                               {
                                   var matEntry = (ImageEntry)bindable;
                                   matEntry.imageLeft.Source = (ImageSource)newVal;
                               });
        public static readonly BindableProperty RightImageSourceProperty =
                               BindableProperty.Create(nameof(RightImageSource), typeof(ImageSource),
                               typeof(ImageEntry), defaultBindingMode: BindingMode.TwoWay,
                               propertyChanged: (bindable, oldVal, newVal) =>
                               {
                                   var matEntry = (ImageEntry)bindable;
                                   matEntry.imageRight.Source = (ImageSource)newVal;
                               });
        public static readonly BindableProperty ImagePositionProperty =
                   BindableProperty.Create(nameof(ImagePosition),
                               typeof(ImageEntryPosition), typeof(ImageEntry),
                               defaultValue: ImageEntryPosition.None, propertyChanged: OnImagePositionChanged);

        public static BindableProperty LeftImageColorProperty =
              BindableProperty.Create(nameof(LeftImageColor), typeof(string),
                      typeof(ImageEntry), defaultValue: "6e6e6e", propertyChanged: OnLeftImageColorChanged);

        public static BindableProperty RightImageColorProperty =
              BindableProperty.Create(nameof(RightImageColor), typeof(string),
                      typeof(ImageEntry), defaultValue: "6e6e6e", propertyChanged: OnRightImageColorChanged);

        public Behavior[] HIBehavior
        {
            get { return (Behavior[])GetValue(HIBehaviorProperty); }
            set { SetValue(HIBehaviorProperty, value); }
        }
        public ImageEntry[] Entries
        {
            get { return (ImageEntry[])GetValue(EntriesProperty); }
            set { SetValue(EntriesProperty, value); }
        }
        public ReturnType ReturnType
        {
            get { return (ReturnType)GetValue(ReturnTypeProperty); }
            set { SetValue(ReturnTypeProperty, value); }
        }
        public string LeftImageColor
        {
            get { return (string)GetValue(LeftImageColorProperty); }
            set { SetValue(LeftImageColorProperty, value); }
        }

        public string RightImageColor
        {
            get { return (string)GetValue(RightImageColorProperty); }
            set { SetValue(RightImageColorProperty, value); }
        }

        public ImageSource RightImageSource
        {
            get { return (ImageSource)GetValue(RightImageSourceProperty); }
            set { SetValue(RightImageSourceProperty, value); }
        }
        public ImageSource LeftImageSource
        {
            get { return (ImageSource)GetValue(LeftImageSourceProperty); }
            set { SetValue(LeftImageSourceProperty, value); }
        }
        public Color AccentColor
        {
            get { return (Color)GetValue(AccentColorProperty); }
            set { SetValue(AccentColorProperty, value); }
        }
        public Keyboard Keyboard
        {
            get { return (Keyboard)GetValue(KeyboardProperty); }
            set { SetValue(KeyboardProperty, value); }
        }
        public bool DisplayLabel
        {
            get { return (bool)GetValue(DisplayLabelProperty); }
            set { SetValue(DisplayLabelProperty, value); }
        }
        public bool IsPassword
        {
            get { return (bool)GetValue(IsPasswordProperty); }
            set { SetValue(IsPasswordProperty, value); }
        }
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        public string LabelText
        {
            get { return (string)GetValue(LabelTextProperty); }
            set { SetValue(LabelTextProperty, value); }
        }

        public Entry TextEntry
        {
            get => txtEntry;
        }


        public string Placeholder
        {
            get { return (string)GetValue(PlaceholderProperty); }
            set { SetValue(PlaceholderProperty, value); }
        }
        public ImageEntryPosition ImagePosition
        {
            get => (ImageEntryPosition)GetValue(ImagePositionProperty);
            set => SetValue(ImagePositionProperty, value);
        }
        public event EventHandler LeftImageClicked;

        public virtual void LeftImageOn_Clicked(object sender, EventArgs e)
        {
            LeftImageClicked?.Invoke(sender, e);
        }
        public event EventHandler RightImageClicked;

        public virtual void RightImageOn_Clicked(object sender, EventArgs e)
        {
            RightImageClicked?.Invoke(sender, e);
        }
        public ImageEntry()
        {
            InitializeComponent();
            txtEntry.BindingContext = this;
            txtEntry.Completed += TxtEntry_Completed;
            txtEntry.Focused += ImageEntry_Focused;
            this.Focused += ImageEntry_Focused;
            TapGestureRecognizer imgLeftTap = new TapGestureRecognizer();
            imgLeftTap.Tapped += LeftImageOn_Clicked;
            imageLeft.GestureRecognizers.Add(imgLeftTap);

            TapGestureRecognizer imgRightTap = new TapGestureRecognizer();
            imgRightTap.Tapped += RightImageOn_Clicked;
            imageRight.GestureRecognizers.Add(imgRightTap);

            imageLeft.Transformations.Add(new TintTransformation
            {
                EnableSolidColor = true,
                HexColor = "6e6e6e"
            });

            imageRight.Transformations.Add(new TintTransformation
            {
                EnableSolidColor = true,
                HexColor = "6e6e6e"
            });
        }

        private void ImageEntry_Focused(object sender, FocusEventArgs e)
        {
            txtEntry.Focus();
        }

        public event EventHandler NextEntry;

        private void TxtEntry_Completed(object sender, EventArgs e)
        {
            try
            {
                if (ReturnType == ReturnType.Next)
                    Entries[this.TabIndex + 1].TextEntry.Focus();
                /*else if (ReturnType == ReturnType.Done || ReturnType == ReturnType.Go)
                    MessagingCenter.Send<ImageEntry, string>(this, "FormEntryCompleted", "Completed");*/

            }
            catch
            {

            }

            NextEntry?.Invoke(sender, e);
        }

        #region Convert Color
        private static Color ConvertColor(string color)
        {

            if (color.IndexOf("#") <= -1)
            {
                try
                {
                    return System.Drawing.Color.FromName(color);
                }
                catch (Exception)
                {

                }
            }
            else
            {
                try
                {
                    return Color.FromHex(color);
                }
                catch (Exception)
                {
                }
            }
            return Color.FromHex("#6e6e6e");
        }
        #endregion

        #region Convert To Hex
        private static string ConvertToHex(Color color)
        {
            int r = (int)(color.R * 256);
            int g = (int)(color.G * 256);
            int b = (int)(color.B * 256);
            int a = (int)(color.A * 256);

            return $"{r.ToString("X2")}{g.ToString("X2")}{b.ToString("X2")}";
        }
        #endregion

        private static void OnRightImageColorChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var control = bindable as ImageEntry;
            control.imageRight.Transformations.Clear();
            if (newvalue.ToString().IndexOf("#") <= -1)
            {
                var color = ConvertToHex(ConvertColor(newvalue.ToString()));
                try
                {
                    control.imageRight.Transformations.Add(new TintTransformation
                    {
                        EnableSolidColor = true,
                        HexColor = color
                    });
                }
                catch (Exception)
                {
                    control.imageRight.Transformations.Add(new TintTransformation
                    {
                        EnableSolidColor = true,
                        HexColor = "6e6e6e"
                    });
                }
            }
            else
            {
                try
                {
                    control.imageRight.Transformations.Add(new TintTransformation
                    {
                        EnableSolidColor = true,
                        HexColor = newvalue.ToString().Substring(1)
                    });
                }
                catch (Exception)
                {
                    control.imageRight.Transformations.Add(new TintTransformation
                    {
                        EnableSolidColor = true,
                        HexColor = "6e6e6e"
                    });
                }
            }
        }

        private static void OnLeftImageColorChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var control = bindable as ImageEntry;
            control.imageLeft.Transformations.Clear();
            if (newvalue.ToString().IndexOf("#") <= -1)
            {
                var color = ConvertToHex(ConvertColor(newvalue.ToString()));
                try
                {
                    control.imageLeft.Transformations.Add(new TintTransformation
                    {
                        EnableSolidColor = true,
                        HexColor = color
                    });
                }
                catch (Exception)
                {
                    control.imageLeft.Transformations.Add(new TintTransformation
                    {
                        EnableSolidColor = true,
                        HexColor = "6e6e6e"
                    });
                }
            }
            else
            {
                try
                {
                    control.imageLeft.Transformations.Add(new TintTransformation
                    {
                        EnableSolidColor = true,
                        HexColor = newvalue.ToString().Substring(1)
                    });
                }
                catch (Exception)
                {
                    control.imageLeft.Transformations.Add(new TintTransformation
                    {
                        EnableSolidColor = true,
                        HexColor = "6e6e6e"
                    });
                }
            }
        }

        private static void OnImagePositionChanged(BindableObject bindable, object oldvalue, object newvalue)
        {
            var control = bindable as ImageEntry;
            switch (control.ImagePosition)
            {
                case ImageEntryPosition.None:
                    control.imageLeft.IsVisible = false;
                    control.imageRight.IsVisible = false;
                    break;
                case ImageEntryPosition.Left:
                    control.imageLeft.IsVisible = true;
                    control.imageRight.IsVisible = false;
                    break;
                case ImageEntryPosition.Right:
                    control.imageLeft.IsVisible = false;
                    control.imageRight.IsVisible = true;
                    break;
                case ImageEntryPosition.Both:
                    control.imageLeft.IsVisible = true;
                    control.imageRight.IsVisible = true;
                    break;
            }
        }
    }
}