﻿namespace Hino.Salesforce.App.Templates.ImageEntry
{
    public enum ImageEntryPosition
    {
        None = 0,
        Left = 1,
        Right = 2,
        Both = 3
    }
}
