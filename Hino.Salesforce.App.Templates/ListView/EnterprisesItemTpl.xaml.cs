﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Templates.ListView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EnterprisesItemTpl : ViewCell
    {
        public EnterprisesItemTpl()
        {
            InitializeComponent();
        }

        public event EventHandler EditItemClicked;
        private void MiEdit_Clicked(object sender, EventArgs e)
        {
            EditItemClicked?.Invoke(sender, e);
        }

        public event EventHandler RemoveItemClicked;
        private void MiRemove_Clicked(object sender, EventArgs e)
        {
            RemoveItemClicked?.Invoke(sender, e);
        }
    }
}