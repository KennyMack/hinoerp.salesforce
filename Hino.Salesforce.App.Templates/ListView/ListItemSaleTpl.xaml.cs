﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Templates.ListView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListItemSaleTpl : ViewCell
    {
        public ListItemSaleTpl()
        {
            InitializeComponent();
        }
    }
}