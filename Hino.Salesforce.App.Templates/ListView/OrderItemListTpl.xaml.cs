﻿using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Templates.ListView
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class OrderItemListTpl : ViewCell
    {
        public OrderItemListTpl()
        {
            InitializeComponent();
        }
        public event EventHandler imageBoxClicked;
        public virtual void imageBoxOn_Clicked(object sender, EventArgs e)
        {
            imageBoxClicked?.Invoke(sender, e);
        }

        private void Button_Clicked(object sender, EventArgs e)
        {
            imageBoxOn_Clicked(sender, e);
            // await DisplayAlert("Continuar?", "e.Text", "Sim", "Não");
        }

        public event EventHandler EditItemClicked;
        private void MiEdit_Clicked(object sender, EventArgs e)
        {
            EditItemClicked?.Invoke(sender, e);
        }

        public event EventHandler RemoveItemClicked;
        private void MiRemove_Clicked(object sender, EventArgs e)
        {
            RemoveItemClicked?.Invoke(sender, e);
        }
    }
}