﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Templates.SearchEntry
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchEntry : ContentView
    {
        public static BindableProperty TextProperty =
                      BindableProperty.Create(nameof(Text), typeof(string),
                      typeof(SearchEntry), defaultBindingMode: BindingMode.TwoWay);

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static BindableProperty LabelProperty =
                     BindableProperty.Create(nameof(Label), typeof(string),
                     typeof(SearchEntry), defaultBindingMode: BindingMode.TwoWay,
                     propertyChanged: (bindable, oldVal, newVal) =>
                     {
                         var matEntry = (SearchEntry)bindable;
                         matEntry.lblLabel.Text = newVal.ToString();
                     });

        public string Label
        {
            get { return (string)GetValue(LabelProperty); }
            set { SetValue(LabelProperty, value); }
        }

        public SearchEntry()
        {
            InitializeComponent();
            txtValue.BindingContext = this;

            TapGestureRecognizer icoSearchTap = new TapGestureRecognizer();
            slSearch.GestureRecognizers.Add(icoSearchTap);
            bvLine.GestureRecognizers.Add(icoSearchTap);
            icoSearch.GestureRecognizers.Add(icoSearchTap);
            txtValue.GestureRecognizers.Add(icoSearchTap);
            lblLabel.GestureRecognizers.Add(icoSearchTap);
            icoSearchTap.Tapped += IcoSearchTap_Tapped;
        }

        public event EventHandler SearchEntryClicked;
        public virtual void OnSearchEntry_Clicked(object sender, EventArgs e)
        {
            SearchEntryClicked?.Invoke(sender, e);
        }

        private async void IcoSearchTap_Tapped(object sender, EventArgs e)
        {
            Task TaskOicoSearch = icoSearch.FadeTo(0.3, 300, Easing.Linear);
            Task TaskOtxtValue = txtValue.FadeTo(0.3, 300, Easing.Linear);
            Task TaskOlblLabel = lblLabel.FadeTo(0.3, 300, Easing.Linear);
            Task TaskObvLine = bvLine.FadeTo(0.3, 300, Easing.Linear);

            var allTasks = new List<Task>
            {
                TaskOicoSearch,
                TaskOtxtValue,
                TaskOlblLabel,
                TaskObvLine
            };

            foreach (var item in allTasks)
            {
                await item;
            }
            Task TaskIicoSearch = icoSearch.FadeTo(1, 300, Easing.Linear);
            Task TaskItxtValue = txtValue.FadeTo(1, 300, Easing.Linear);
            Task TaskIlblLabel = lblLabel.FadeTo(1, 300, Easing.Linear);
            Task TaskIbvLine = bvLine.FadeTo(1, 300, Easing.Linear);

            allTasks = new List<Task>
            {
                TaskIicoSearch,
                TaskItxtValue,
                TaskIlblLabel,
                TaskIbvLine
            };
            foreach (var item in allTasks)
            {
                await item;
            }

            OnSearchEntry_Clicked(icoSearch, e);
        }
    }
}