﻿using System;
using System.ComponentModel;
using System.Runtime.CompilerServices;

namespace Hino.Salesforce.App.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class DisplayFieldAttribute : DisplayNameAttribute
    {
        public DisplayFieldAttribute([CallerMemberName] string propertyName = "")
        {
            var a = new System.Resources.ResourceManager(typeof(Resources.FieldsNameResource));

            DisplayNameValue = a.GetString(propertyName);
        }
    }
}
