﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Hino.Salesforce.App.Utils.Attributes
{
    [AttributeUsage(AttributeTargets.Property, Inherited = true)]
    public class RequiredFieldAttribute : RequiredAttribute
    {
        public RequiredFieldAttribute()
        {
            AllowEmptyStrings = false;
            ErrorMessageResourceName = "RequiredDefault";
            ErrorMessageResourceType = typeof(Resources.ValidationMessagesResource);
        }
    }
}
