﻿namespace Hino.Salesforce.App.Utils.Extensions
{
    public static class StringEx
    {
        public static string SubStrTitle(this string pString)
        {
            if (pString.Length > 20)
                return pString.Substring(0, 20) + "...";

            return pString;
        }
    }
}
