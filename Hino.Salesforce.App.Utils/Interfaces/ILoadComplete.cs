﻿namespace Hino.Salesforce.App.Utils.Interfaces
{
    public interface ILoadComplete
    {
        object Sender { get; }
    }
}
