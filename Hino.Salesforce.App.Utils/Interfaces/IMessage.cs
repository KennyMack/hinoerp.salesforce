﻿using System;

namespace Hino.Salesforce.App.Utils.Interfaces
{
    public interface IMessage
    {
        void ShowShortMessage(String text);
    }
}
