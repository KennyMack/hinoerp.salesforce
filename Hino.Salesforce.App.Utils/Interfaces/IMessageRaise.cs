﻿namespace Hino.Salesforce.App.Utils.Interfaces
{
    public interface IMessageRaise
    {
        string Text { get; }
    }
}
