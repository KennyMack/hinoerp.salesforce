﻿namespace Hino.Salesforce.App.Utils.Load.Enums
{
    public enum LoadStatus
    {
        Iniciando = 0,
        Concluido = 1
    }

    public enum LoadType
    {
        ShowLoad = 0,
        SyncStatus = 1,
        RefreshLogin = 2
    }
}
