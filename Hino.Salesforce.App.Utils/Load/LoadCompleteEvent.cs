﻿using Hino.Salesforce.App.Utils.Interfaces;
using System;

namespace Hino.Salesforce.App.Utils.Load
{
    public class LoadCompleteEvent : EventArgs, ILoadComplete
    {
        public object Sender { get; private set; }
        public LoadCompleteEvent(object pSender)
        {
            this.Sender = pSender;
        }
    }
}
