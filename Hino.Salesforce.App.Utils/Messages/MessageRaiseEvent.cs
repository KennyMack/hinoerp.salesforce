﻿using Hino.Salesforce.App.Utils.Interfaces;
using System;

namespace Hino.Salesforce.App.Utils.Messages
{
    public class MessageRaiseEvent : EventArgs, IMessageRaise
    {
        public MessageRaiseEvent(string text)
        {
            Text = text;
        }

        public string Text { get; private set; }
    }
}
