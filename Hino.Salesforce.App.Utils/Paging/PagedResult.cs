﻿using System.Collections.Generic;

namespace Hino.Salesforce.App.Utils.Paging
{
    public class PagedResult<T> : BasePagedResult where T : class
    {
        public IList<T> Results { get; set; }

        public PagedResult()
        {
            Results = new List<T>();
        }
    }
}
