﻿using Hino.Salesforce.App.Services.DB;
using System.IO;

namespace Hino.Salesforce.App.Droid.Database
{
    public class DBSQLite : IDBSQLite
    {
        public string DataBasePath => Path.Combine(
                    System.Environment.GetFolderPath(
                    System.Environment.SpecialFolder.Personal),
                    "HINOERP.db3"
                );
    }
}