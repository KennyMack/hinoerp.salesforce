﻿using Android.Widget;
using Hino.Salesforce.App.Utils.Interfaces;

namespace Hino.Salesforce.App.Droid
{
    public class Message : IMessage
    {
        public void ShowShortMessage(string text)
        {
            Toast.MakeText(Android.App.Application.Context, text, ToastLength.Short).Show();
        }
    }
}
