using Hino.Salesforce.App.Models.General;
using Hino.Salesforce.App.Services.API.Interfaces.Auth;
using Hino.Salesforce.App.Views.Main;
using System;
using System.Globalization;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

[assembly: XamlCompilation(XamlCompilationOptions.Compile)]
namespace Hino.Salesforce.App
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("pt-BR");



            DInjection.Initialize();

            //ThemeManager.LoadTheme();

            // MainPage = new MainPage();
        }

        protected override void OnStart()
        {
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("pt-BR");
            // Handle when your app starts
            if (!IsLogged())
            {
                var establishmentKey = App.ESTABLISHMENTKEY;
                var userEmail = App.UserLogged?.Email;
                App.USER_TOKEN = null;
                App.REFRESH_TOKEN = null;
                App.ESTABLISHMENTKEY = null;
                App.UserLogged = null;
                App.ESTABLISHMENTKEY = establishmentKey;

                var navpage = new NavigationPage(new Views.Auth.LoginPage(userEmail));
                MainPage = navpage;
            }
            else
            {
                MainPage = new MasterMainPageMenu();
            }




            //  var a = App.User;
            //  var r = App.BASEIP;
            //  if (App.BASEIP != null)
            //  {
            //      if (App.User != null)
            //          MainPage = new MasterMainPageMenu();
            //      else

            //  }
            //  else
            //      MainPage = new PathServerPage();

        }

        #region Check Token Status
        private async Task<bool> CheckTokenStatus()
        {
            IAuthAPI authAPI = DInjection.GetIntance<IAuthAPI>();
            var result = await authAPI.Me();

            var us = result.data.ToObject<GEUsersModel>();
            return result.status == 200;
        }
        #endregion

        #region Is Logged
        private bool IsLogged()
        {
            var retval = true;
            if (!Current.Properties.TryGetValue("UniqueKey", out object UniqueKey))
                retval = false;
            if (!Current.Properties.TryGetValue("EstablishmentKey", out object EstablishmentKey))
                retval = false;
            if (!Current.Properties.TryGetValue("UserName", out object UserName))
                retval = false;
            if (!Current.Properties.TryGetValue("Email", out object Email))
                retval = false;
            if (!Current.Properties.TryGetValue("Password", out object Password))
                retval = false;
            if (!Current.Properties.TryGetValue("UserType", out object UserType))
                retval = false;
            if (!Current.Properties.TryGetValue("PercDiscount", out object PercDiscount))
                retval = false;
            if (!Current.Properties.TryGetValue("PercCommission", out object PercCommission))
                retval = false;
            if (!Current.Properties.TryGetValue("UserKey", out object UserKey))
                retval = false;
            if (string.IsNullOrEmpty(App.USER_TOKEN))
                retval = false;

            return retval;
        }
        #endregion

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            CultureInfo.DefaultThreadCurrentCulture = new CultureInfo("pt-BR");
            // Handle when your app resumes
            // if (!IsLogged())
            //     MainPage = new NavigationPage(new Views.Auth.LoginPage());
            /*else
                MainPage = new MasterMainPageMenu();*/
            // if (App.User == null && App.BASEIP != null)
            //     MainPage = new LoginPage();
            // else if (App.BASEIP == null)
            //     MainPage = new PathServerPage();
        }

        /// <summary>
        /// User-Token
        /// </summary>
        public static string USER_TOKEN
        {
            get
            {
                if (Current.Properties.TryGetValue("USER_TOKEN", out object USER_TOKEN))
                    return USER_TOKEN.ToString();

                return null;
            }
            set
            {
                if (value == null)
                    Current.Properties.Clear();
                else
                    Current.Properties["USER_TOKEN"] = value;

                Current.SavePropertiesAsync();
            }
        }

        /// <summary>
        /// Refresh-Token
        /// </summary>
        public static string REFRESH_TOKEN
        {
            get
            {
                if (Current.Properties.TryGetValue("REFRESH_TOKEN", out object REFRESH_TOKEN))
                    return REFRESH_TOKEN.ToString();

                return null;
            }
            set
            {
                if (value == null)
                    Current.Properties.Clear();
                else
                    Current.Properties["REFRESH_TOKEN"] = value;

                Current.SavePropertiesAsync();
            }
        }

        /// <summary>
        /// ESTABLISHMENTKEY
        /// </summary>
        public static string ESTABLISHMENTKEY
        {
            get
            {
                if (Current.Properties.TryGetValue("EstablishmentKey", out object ESTABLISHMENTKEY))
                    return ESTABLISHMENTKEY.ToString();

                return null;
            }
            set
            {
                if (value == null)
                    Current.Properties.Clear();
                else
                    Current.Properties["EstablishmentKey"] = value;

                Current.SavePropertiesAsync();
            }
        }

        public static GEUsersModel UserLogged
        {
            get
            {
                var user = new GEUsersModel();
                if (Current.Properties.TryGetValue("UniqueKey", out object UniqueKey))
                    user.UniqueKey = UniqueKey?.ToString();
                if (Current.Properties.TryGetValue("EstablishmentKey", out object EstablishmentKey))
                    user.EstablishmentKey = EstablishmentKey?.ToString();
                if (Current.Properties.TryGetValue("UserName", out object UserName))
                    user.UserName = UserName?.ToString();
                if (Current.Properties.TryGetValue("Email", out object Email))
                    user.Email = Email?.ToString();
                if (Current.Properties.TryGetValue("Password", out object Password))
                    user.Password = Password?.ToString();
                if (Current.Properties.TryGetValue("UserType", out object UserType))
                    user.UserType = (int)UserType;
                if (Current.Properties.TryGetValue("PercDiscount", out object PercDiscount))
                    user.PercDiscount = (decimal)PercDiscount;
                if (Current.Properties.TryGetValue("PercCommission", out object PercCommission))
                    user.PercCommission = (decimal)PercCommission;
                if (Current.Properties.TryGetValue("UserKey", out object UserKey))
                    user.UserKey = UserKey?.ToString();
                if (Current.Properties.TryGetValue("Id", out object Id))
                    user.Id = (long)Id;

                return user;
            }
            set
            {
                if (value == null)
                    Current.Properties.Clear();
                else
                {
                    Current.Properties["Id"] = value.Id;
                    Current.Properties["UniqueKey"] = value.UniqueKey;
                    Current.Properties["EstablishmentKey"] = value.EstablishmentKey;
                    Current.Properties["UserName"] = value.UserName;
                    Current.Properties["Email"] = value.Email;
                    Current.Properties["Password"] = value.Password;
                    Current.Properties["UserType"] = value.UserType;
                    Current.Properties["PercDiscount"] = value.PercDiscount;
                    Current.Properties["PercCommission"] = value.PercCommission;
                    Current.Properties["UserKey"] = value.UserKey;
                }
                Current.SavePropertiesAsync();
            }
        }

        /// <summary>
        /// FIRS ACCESS
        /// </summary>
        public static bool FIRSTACCESS
        {
            get
            {
                if (Current.Properties.TryGetValue("FIRSTACCESS", out object isFirst))
                    return Convert.ToInt32(isFirst) > 0;

                return true;
            }
            set
            {

                Current.Properties["FIRSTACCESS"] = value ? 1 : 0;

                Current.SavePropertiesAsync();
            }
        }

        /// <summary>
        /// FIRS SYNC OK
        /// </summary>
        public static bool FIRSTSYNCOK
        {
            get
            {
                if (Current.Properties.TryGetValue("FIRSTSYNCOK", out object isFirstOk))
                    return Convert.ToInt32(isFirstOk) > 0;

                return false;
            }
            set
            {

                Current.Properties["FIRSTSYNCOK"] = value ? 1 : 0;

                Current.SavePropertiesAsync();
            }
        }



        public static string BASEURL
        {

            //get => "192.168.145.105:6090/hino.salesforce.api/api";
            // "192.168.2.103:6090/hino.salesforce.api/api";

            get => "3.229.108.251:6081/api";
            //#endif
            //get => "192.168.2.103/hino.salesforce.api/api";
            // get => "192.168.2.103:6090/hino.salesforce.api/api";
            //get => "192.168.145.105:6090/hino.salesforce.api/api";
        }
    }
}
