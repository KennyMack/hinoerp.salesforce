﻿using Autofac;
using Hino.Salesforce.App.Services.API.Auth;
using Hino.Salesforce.App.Services.API.General;
using Hino.Salesforce.App.Services.API.General.Business;
using Hino.Salesforce.App.Services.API.General.Demograph;
using Hino.Salesforce.App.Services.API.Interfaces.Auth;
using Hino.Salesforce.App.Services.API.Interfaces.General;
using Hino.Salesforce.App.Services.API.Interfaces.General.Business;
using Hino.Salesforce.App.Services.API.Interfaces.General.Demograph;
using Hino.Salesforce.App.Services.API.Interfaces.Sales;
using Hino.Salesforce.App.Services.API.Sales;
using Hino.Salesforce.App.Services.DB;
using Hino.Salesforce.App.Services.DB.General;
using Hino.Salesforce.App.Services.DB.General.Business;
using Hino.Salesforce.App.Services.DB.General.Demograph;
using Hino.Salesforce.App.Services.DB.Interfaces;
using Hino.Salesforce.App.Services.DB.Interfaces.General;
using Hino.Salesforce.App.Services.DB.Interfaces.General.Business;
using Hino.Salesforce.App.Services.DB.Interfaces.General.Demograph;
using Hino.Salesforce.App.Services.DB.Interfaces.Sales;
using Hino.Salesforce.App.Services.DB.Sales;

namespace Hino.Salesforce.App
{
    public static class DInjection
    {
        private static IContainer _container;

        //public static IAuthAPI ServiceA { get { return _container.Resolve<IAuthAPI>(); } }

        public static void Initialize()
        {
            if (_container == null)
            {
                var builder = new ContainerBuilder();

                builder.RegisterType<AuthAPI>().As<IAuthAPI>();
                builder.RegisterType<UsersAPI>().As<IUsersAPI>();
                builder.RegisterType<GEProductsAPI>().As<IGEProductsAPI>();

                builder.RegisterType<GEEnterprisesAPI>().As<IGEEnterprisesAPI>();
                builder.RegisterType<GEPaymentConditionAPI>().As<IGEPaymentConditionAPI>();
                builder.RegisterType<GEPaymentTypeAPI>().As<IGEPaymentTypeAPI>();
                builder.RegisterType<GECitiesAPI>().As<IGECitiesAPI>();
                builder.RegisterType<GECountriesAPI>().As<IGECountriesAPI>();
                builder.RegisterType<GEStatesAPI>().As<IGEStatesAPI>();
                builder.RegisterType<VEOrderItemsAPI>().As<IVEOrderItemsAPI>();
                builder.RegisterType<VEOrdersAPI>().As<IVEOrdersAPI>();
                builder.RegisterType<VESalePriceAPI>().As<IVESalePriceAPI>();
                builder.RegisterType<VERegionSaleAPI>().As<IVERegionSaleAPI>();

                builder.RegisterType<ConfigDB>().As<IConfigDB>();
                builder.RegisterType<GEUsersDB>().As<IGEUsersDB>();
                builder.RegisterType<GEEnterprisesDB>().As<IGEEnterprisesDB>();
                builder.RegisterType<GEPaymentConditionDB>().As<IGEPaymentConditionDB>();
                builder.RegisterType<GEPaymentTypeDB>().As<IGEPaymentTypeDB>();
                builder.RegisterType<GECitiesDB>().As<IGECitiesDB>();
                builder.RegisterType<GECountriesDB>().As<IGECountriesDB>();
                builder.RegisterType<GEStatesDB>().As<IGEStatesDB>();
                builder.RegisterType<GEProductsDB>().As<IGEProductsDB>();
                builder.RegisterType<VEOrderItemsDB>().As<IVEOrderItemsDB>();
                builder.RegisterType<VEOrdersDB>().As<IVEOrdersDB>();
                builder.RegisterType<VESalePriceDB>().As<IVESalePriceDB>();
                builder.RegisterType<VERegionSaleDB>().As<IVERegionSaleDB>();

                _container = builder.Build();
            }
        }

        public static T GetIntance<T>() =>
            _container.Resolve<T>();

    }
}
