﻿using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Services.API.Interfaces.General.Business;

namespace Hino.Salesforce.App.Services.API.General.Business
{
    public class GEPaymentTypeAPI : BaseAPI<GEPaymentTypeModel>, IGEPaymentTypeAPI
    {
    }
}
