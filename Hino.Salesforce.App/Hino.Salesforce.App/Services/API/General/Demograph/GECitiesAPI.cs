﻿using Hino.Salesforce.App.Models.General.Demograph;
using Hino.Salesforce.App.Services.API.Interfaces.General.Demograph;

namespace Hino.Salesforce.App.Services.API.General.Demograph
{
    public class GECitiesAPI : BaseAPI<GECitiesModel>, IGECitiesAPI
    {
    }
}
