﻿using Hino.Salesforce.App.Models.General;
using Hino.Salesforce.App.Services.API.Interfaces.General;

namespace Hino.Salesforce.App.Services.API.General
{
    public class GEProductsAPI : BaseAPI<GEProductsModel>, IGEProductsAPI
    {
    }
}
