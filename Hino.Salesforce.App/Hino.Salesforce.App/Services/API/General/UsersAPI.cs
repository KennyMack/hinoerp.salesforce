﻿using Hino.Salesforce.App.Models;
using Hino.Salesforce.App.Models.Auth;
using Hino.Salesforce.App.Models.General;
using Hino.Salesforce.App.Services.API.Interfaces.General;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.API.General
{
    public class UsersAPI : BaseAPI<GEUsersModel>, IUsersAPI
    {
        public async Task<DefaultResultModel> CreateUserAsync(RegisterModel model)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(model),
                Encoding.UTF8, "application/json");

            var url = $"{base.EndPoint.Replace("{pEstablishmentKey}", model.EstablishmentKey)}/mobile/create";
            var ret = await _Request.PostAsync(url, false, content);
            return ret.ToObject<DefaultResultModel>(base._JsonSerializer);
        }
    }
}
