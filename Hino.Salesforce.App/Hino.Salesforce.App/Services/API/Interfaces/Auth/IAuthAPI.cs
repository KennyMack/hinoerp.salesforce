﻿using Hino.Salesforce.App.Models.Auth;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.API.Interfaces.Auth
{
    public interface IAuthAPI : IBaseAPI<LoginModel>
    {
        Task<AuthResultModel> AuthenticateAsync(LoginModel pLogin);
        Task<AuthResultModel> RefreshTokenAsync();
        Task<DefaultResultModel> Me();
    }
}
