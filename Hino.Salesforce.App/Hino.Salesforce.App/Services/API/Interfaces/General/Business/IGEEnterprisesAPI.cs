﻿using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Utils.Paging;
using System;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.API.Interfaces.General.Business
{
    public interface IGEEnterprisesAPI : IBaseAPI<GEEnterprisesModel>
    {
        Task<PagedResult<GEEnterprisesModel>> GetAllSyncByUserIdAsync(DateTime pDate, long pUserId);
    }
}
