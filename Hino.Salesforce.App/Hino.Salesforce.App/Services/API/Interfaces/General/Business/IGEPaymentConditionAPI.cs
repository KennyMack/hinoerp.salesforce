﻿using Hino.Salesforce.App.Models.General.Business;

namespace Hino.Salesforce.App.Services.API.Interfaces.General.Business
{
    public interface IGEPaymentConditionAPI : IBaseAPI<GEPaymentConditionModel>
    {
    }
}
