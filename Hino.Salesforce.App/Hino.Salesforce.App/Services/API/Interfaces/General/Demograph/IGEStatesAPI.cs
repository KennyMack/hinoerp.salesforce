﻿using Hino.Salesforce.App.Models.General.Demograph;

namespace Hino.Salesforce.App.Services.API.Interfaces.General.Demograph
{
    public interface IGEStatesAPI : IBaseAPI<GEStatesModel>
    {
    }
}
