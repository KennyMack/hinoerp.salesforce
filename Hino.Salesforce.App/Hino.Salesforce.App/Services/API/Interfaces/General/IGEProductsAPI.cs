﻿using Hino.Salesforce.App.Models.General;

namespace Hino.Salesforce.App.Services.API.Interfaces.General
{
    public interface IGEProductsAPI : IBaseAPI<GEProductsModel>
    {
    }
}
