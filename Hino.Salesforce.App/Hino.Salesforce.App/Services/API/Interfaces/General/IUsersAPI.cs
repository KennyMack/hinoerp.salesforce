﻿using Hino.Salesforce.App.Models;
using Hino.Salesforce.App.Models.Auth;
using Hino.Salesforce.App.Models.General;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.API.Interfaces.General
{
    public interface IUsersAPI : IBaseAPI<GEUsersModel>
    {
        Task<DefaultResultModel> CreateUserAsync(RegisterModel model);
    }
}
