﻿using Hino.Salesforce.App.Models;
using Hino.Salesforce.App.Utils.Paging;
using System;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.API.Interfaces
{
    public interface IBaseAPI<T> where T : BaseEntity
    {
        string BASEURL { get; }
        Task<PagedResult<T>> GetAllSyncAsync(DateTime pDate);
        Task<PagedResult<T>> GetAllAsync();
        Task<T> GetByIdAsync(string pUniqueKey, long pId);
        Task<T> PostAsync(T model);
        Task<T> PutAsync(T model, string pUniqueKey, long id);
        Task<T> DeleteAsync(string pUniqueKey, long id);
        Task<bool> ConectionTest();
    }
}
