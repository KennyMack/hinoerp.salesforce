﻿using Hino.Salesforce.App.Utils.Exceptions;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.API.Interfaces
{
    public interface IRequest
    {
        List<ModelException> Errors { get; set; }
        Task<JContainer> GetAsync(string path, bool Token, TimeSpan? timeout = null);
        Task<T> GetFullURLAsync<T>(string path, bool Token, TimeSpan? timeout = null);
        Task<JContainer> PostAsync(string path, bool Token, HttpContent content);
        Task<JContainer> PutAsync(string path, bool Token, HttpContent content);
        Task<JContainer> DeleteAsync(string path, bool Token);
    }
}
