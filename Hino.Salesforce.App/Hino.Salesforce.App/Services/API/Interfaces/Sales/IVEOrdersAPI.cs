﻿using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.Utils.Paging;
using System;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.API.Interfaces.Sales
{
    public interface IVEOrdersAPI : IBaseAPI<VEOrdersModel>
    {
        Task<PagedResult<VEOrdersModel>> GetAllSyncUserAsync(long pUserId, DateTime pDate);
    }
}
