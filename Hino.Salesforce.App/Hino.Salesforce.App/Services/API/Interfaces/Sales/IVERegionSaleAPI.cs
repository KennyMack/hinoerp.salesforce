﻿using Hino.Salesforce.App.Models.Sales;

namespace Hino.Salesforce.App.Services.API.Interfaces.Sales
{
    public interface IVERegionSaleAPI : IBaseAPI<VERegionSaleModel>
    {
    }
}
