﻿using Hino.Salesforce.App.Models.Auth;
using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.Services.API.Interfaces.Sales;
using Hino.Salesforce.App.Utils.Paging;
using System;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.API.Sales
{
    public class VEOrdersAPI : BaseAPI<VEOrdersModel>, IVEOrdersAPI
    {
        public async Task<PagedResult<VEOrdersModel>> GetAllSyncUserAsync(long pUserId, DateTime pDate)
        {
            var result =
                await _Request.GetAsync(
                    $"{EndPoint.Replace("{pEstablishmentKey}", User.EstablishmentKey)}/user/{pUserId}/sync/date/{pDate.ToString("ddMMyyyyHHmmss")}", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PagedResult<VEOrdersModel>>(this._JsonSerializer);
        }

    }
}
