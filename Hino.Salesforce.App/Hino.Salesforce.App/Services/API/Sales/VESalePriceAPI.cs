﻿using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.Services.API.Interfaces.Sales;

namespace Hino.Salesforce.App.Services.API.Sales
{
    public class VESalePriceAPI : BaseAPI<VESalePriceModel>, IVESalePriceAPI
    {
    }
}
