﻿using Hino.Salesforce.App.Models.Auth;
using Hino.Salesforce.App.Models.General;
using Hino.Salesforce.App.Services.API.Interfaces.Auth;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.Application.Auth
{
    public class AuthenticationService
    {
        public async Task<AuthResultModel> LoginAsync(LoginModel pLoginModel)
        {
            var _IAuthAPI = DInjection.GetIntance<IAuthAPI>();
            var ret = await _IAuthAPI.AuthenticateAsync(pLoginModel);

            if (!string.IsNullOrEmpty(ret.error))
            {
                return new AuthResultModel
                {
                    success = false,
                    error = ret.error,
                    error_description = ret.error_description
                };
            }

            ret.success = true;

            App.ESTABLISHMENTKEY = pLoginModel.EstablishmentKey;
            App.USER_TOKEN = ret.access_token;
            App.REFRESH_TOKEN = ret.refresh_token;

            return ret;
        }

        public async Task<AuthResultModel> RefreshLoginAsync()
        {
            var _IAuthAPI = DInjection.GetIntance<IAuthAPI>();
            var ret = await _IAuthAPI.RefreshTokenAsync();

            if (!string.IsNullOrEmpty(ret.error))
            {
                return new AuthResultModel
                {
                    success = false,
                    error = ret.error,
                    error_description = ret.error_description
                };
            }

            ret.success = true;

            App.USER_TOKEN = ret.access_token;
            App.REFRESH_TOKEN = ret.refresh_token;

            return ret;
        }

        public async Task<GEUsersModel> GetMe()
        {
            var _IAuthAPI = DInjection.GetIntance<IAuthAPI>();
            var me = await _IAuthAPI.Me();
            if (!me.success)
                return null;

            return me.data.ToObject<GEUsersModel>();
        }

        public async Task<string> SetUserLogged()
        {
            var _IAuthAPI = DInjection.GetIntance<IAuthAPI>();
            var me = await _IAuthAPI.Me();

            if (!me.success)
                return me.error[0].Messages.ToArray()[0];

            App.UserLogged = me.data.ToObject<GEUsersModel>();

            return "";
        }


        #region Check and refresh token
        public async Task<bool> CheckAndRefreshToken()
        {
            var retval = false;
            var me = await GetMe();

            if (me == null)
            {
                var refresh = await RefreshLoginAsync();
                retval = refresh.success;
            }
            else
                retval = true;

            return retval;
        }
        #endregion

        #region Check connection with server
        public async Task<bool> CheckConnectionWithServer()
        {
            var _IAuthAPI = DInjection.GetIntance<IAuthAPI>();
            return await _IAuthAPI.ConectionTest();
        }
        #endregion

    }
}
