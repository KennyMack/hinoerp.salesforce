﻿using Hino.Salesforce.App.Models;
using Hino.Salesforce.App.Services.DB.Interfaces;
using Hino.Salesforce.App.Utils.Extensions;
using Hino.Salesforce.App.Utils.Paging;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Hino.Salesforce.App.Services.DB
{
    public class BaseDataStore<T> : IBaseDataStore<T> where T : BaseEntity, new()
    {
        protected readonly IDBSQLite _IDBSQLite;
        protected readonly SQLiteAsyncConnection _DataBase;

        public BaseDataStore()
        {
            _IDBSQLite = DependencyService.Get<IDBSQLite>();
            _DataBase = new SQLiteAsyncConnection(_IDBSQLite.DataBasePath);
            try
            {
                _DataBase.CreateTableAsync<T>().Wait();
            }
            catch (Exception)
            {

            }
        }

        public async Task<T> AddOrUpdateItemAsync(T item)
        {
            await _DataBase.InsertOrReplaceAsync(item);
            return item;
        }

        public virtual async Task<T> AddItemAsync(T item)
        {
            if (item.Id < 0)
            {
                item.Created = DateTime.Now;
                item.Modified = DateTime.Now;
            }
            await _DataBase.InsertAsync(item);
            return item;
        }

        public virtual async Task<T> UpdateItemAsync(T item)
        {
            item.Modified = DateTime.Now;
            await _DataBase.UpdateAsync(item);

            return item;
        }

        public virtual async Task<bool> DeleteItemAsync(long id)
        {
            T item = await this.GetItemAsync(id);
            if (item == null)
                return false;

            return (await _DataBase.DeleteAsync(item)) > 0;
        }

        public virtual async Task<T> GetItemAsync(long id) =>
            await _DataBase.Table<T>().Where(r => r.Id == id).FirstOrDefaultAsync();

        public virtual async Task AddFromPagedResult(PagedResult<T> result)
        {
            foreach (var item in result.Results)
            {
                var itemDb = await this.GetItemAsync(item.Id);

                if (itemDb == null)
                    await this.AddItemAsync(item);
                else if (itemDb != null && !item.EqualTo(itemDb))
                {
                    itemDb.CopyProperties(item);
                    await this.UpdateItemAsync(item);
                }
            }
        }

        public PagedResult<T> PaginateQuery(IList<T> query, int page)
        {
            var result = new PagedResult<T>
            {
                CurrentPage = page,
                PageSize = 50,
                RowCount = query.Count(),
                Results = query
            };

            var pageCount = (double)result.RowCount / 50;
            result.PageCount = (int)Math.Ceiling(pageCount);

            return result;
        }

        public virtual async Task<PagedResult<T>> GetItemsAsync(int page)
        {
            if (page > 0)
                return PaginateQuery(
                    await _DataBase.Table<T>().OrderByDescending(r => r.Id).Skip((page - 1) * 50).Take(50).ToListAsync(), page);

            return PaginateQuery(
                    await _DataBase.Table<T>().OrderByDescending(r => r.Id).ToListAsync(), page);
        }

        public virtual async Task<PagedResult<T>> Query(Expression<Func<T, bool>> predicate, int page)
        {
            if (page > 0)
                return PaginateQuery(
                    await _DataBase.Table<T>().Where(predicate).OrderByDescending(r => r.Id).Skip((page - 1) * 50).Take(50).ToListAsync(), page);

            return PaginateQuery(
                    await _DataBase.Table<T>().Where(predicate).OrderByDescending(r => r.Id).ToListAsync(), page);
        }

        public async Task<long> NextSequence()
        {
            Random rd = new Random();
            var id = 0;
            while (true)
            {
                id = rd.Next(-999999999, -1);
                if (await GetItemAsync(id) == null)
                    break;
            }

            return id;
        }

        #region Convert Search Data to number
        protected int ConvertSearchToNumber(string pSearch)
        {
            if (int.TryParse(pSearch, out int ret))
                return ret;

            return 0;
        }
        #endregion

        public virtual async Task<PagedResult<T>> Search(string pSearchData, int page)
        {
            if (!string.IsNullOrEmpty(pSearchData))
            {
                var num = ConvertSearchToNumber(pSearchData);
                return await Query(r =>
                    r.Id == num, page);
            }
            else
                return await GetItemsAsync(page);
        }

        public virtual void ClearTable()
        {
            try
            {
                _DataBase.DropTableAsync<T>().Wait();
                _DataBase.CreateTableAsync<T>().Wait();
            }
            catch (Exception)
            {

            }
        }
    }
}
