﻿using Hino.Salesforce.App.Models;
using Hino.Salesforce.App.Services.DB.Interfaces;
using System;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.DB
{
    public class ConfigDB : BaseDataStore<ConfigModel>, IConfigDB
    {
        public async Task<ConfigModel> UpdateLastSync(DateTime pDate)
        {
            var model = await GetItemAsync(1);
            model.LastSync = pDate;
            await UpdateItemAsync(model);

            return model;
        }
    }
}
