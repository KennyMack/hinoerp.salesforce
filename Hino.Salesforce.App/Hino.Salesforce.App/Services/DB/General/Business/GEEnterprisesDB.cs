﻿using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Services.DB.Interfaces.General.Business;
using Hino.Salesforce.App.Utils.Extensions;
using Hino.Salesforce.App.Utils.Paging;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.DB.General.Business
{
    public class GEEnterprisesDB : BaseDataStore<GEEnterprisesModel>, IGEEnterprisesDB
    {
        public override async Task<PagedResult<GEEnterprisesModel>> Query(Expression<Func<GEEnterprisesModel, bool>> predicate, int page)
        {
            if (page > 0)
                return PaginateQuery(
                    await _DataBase.Table<GEEnterprisesModel>().Where(predicate)
                    .OrderBy(r => r.StatusSinc)
                    .ThenByDescending(r => r.Id)
                    .Skip((page - 1) * 50)
                    .Take(50).ToListAsync(), page);

            return PaginateQuery(
                    await _DataBase.Table<GEEnterprisesModel>().Where(predicate)
                    .OrderBy(r => r.StatusSinc)
                    .ThenByDescending(r => r.Id)
                    .ToListAsync(), page);
        }

        public override async Task<PagedResult<GEEnterprisesModel>> GetItemsAsync(int page)
        {
            if (page > 0)
                return PaginateQuery(
                    await _DataBase.Table<GEEnterprisesModel>()
                        .OrderBy(r => r.StatusSinc)
                        .ThenByDescending(r => r.Id)
                        .Skip((page - 1) * 50)
                        .Take(50).ToListAsync(), page);

            return PaginateQuery(
                    await _DataBase.Table<GEEnterprisesModel>()
                        .OrderBy(r => r.StatusSinc)
                        .ThenByDescending(r => r.Id)
                        .ToListAsync(), page);
        }

        string GenerateInitials(GEEnterprisesModel item)
        {
            var name = item.RazaoSocial.Trim().Split(' ');
            var clName = string.Join("", name);

            var rand = new Random();
            var charA = (int)Math.Floor((decimal)rand.Next(0, clName.Length));

            return $"{item.RazaoSocial[0]}{clName[charA]}".ToUpper();

        }

        public override Task<GEEnterprisesModel> AddItemAsync(GEEnterprisesModel item)
        {
            if (string.IsNullOrEmpty(item.Initials) ||
                string.IsNullOrWhiteSpace(item.Initials))
                item.Initials = GenerateInitials(item);

            return base.AddItemAsync(item);
        }

        public override async Task<GEEnterprisesModel> UpdateItemAsync(GEEnterprisesModel item)
        {
            item.Modified = DateTime.Now;

            if (string.IsNullOrEmpty(item.Initials) ||
                string.IsNullOrWhiteSpace(item.Initials))
                item.Initials = GenerateInitials(item);

            await _DataBase.UpdateAsync(item);

            return item;
        }


        public async Task<PagedResult<GEEnterprisesModel>> SearchActives(string pSearchData, int page)
        {
            if (!string.IsNullOrEmpty(pSearchData))
            {
                var num = ConvertSearchToNumber(pSearchData);
                return await Query(r =>
                    ((r.Status == 1) &&
                    (
                    r.RazaoSocial.ToLower().Contains(pSearchData.ToLower()) ||
                    r.Id == num ||
                    r.Initials.ToLower().Contains(pSearchData.ToLower()) ||
                    r.CNPJCPF.ToLower().Contains(pSearchData.ToLower()) ||
                    r.NomeFantasia.ToLower().Contains(pSearchData.ToLower())
                    )), page);
            }
            else
                return await Query(r => r.Status == 1, page);// GetItemsAsync(page);
        }

        public override async Task<PagedResult<GEEnterprisesModel>> Search(string pSearchData, int page)
        {
            if (!string.IsNullOrEmpty(pSearchData))
            {
                var num = ConvertSearchToNumber(pSearchData);
                return await Query(r =>
                    r.RazaoSocial.ToLower().Contains(pSearchData.ToLower()) ||
                    r.Id == num ||
                    r.Initials.ToLower().Contains(pSearchData.ToLower()) ||
                    r.CNPJCPF.ToLower().Contains(pSearchData.ToLower()) ||
                    r.NomeFantasia.ToLower().Contains(pSearchData.ToLower()), page);
            }
            else
                return await GetItemsAsync(page);
        }

        public async Task<PagedResult<GEEnterprisesModel>> GetAllNew()
        {
            return await Query(r => r.Id < 0, -1);
        }

        public async Task<int> UpdateIdEnterprise(long pActualId, long pNewId) =>
            await base._DataBase.ExecuteAsync(@"UPDATE GEEnterprisesModel SET Id = ? WHERE Id = ?", pNewId, pActualId);

        public override async Task AddFromPagedResult(PagedResult<GEEnterprisesModel> result)
        {
            foreach (var item in result.Results)
            {
                item.CityID = null;
                item.CountryID = null;
                item.StateID = null;

                var itemDb = await this.GetItemAsync(item.Id);

                if (itemDb == null)
                    await this.AddItemAsync(item);
                else if (itemDb != null && !item.EqualTo(itemDb))
                {
                    var initials = itemDb.Initials;

                    if (string.IsNullOrEmpty(initials) ||
                        string.IsNullOrWhiteSpace(initials))
                        initials = GenerateInitials(item);

                    itemDb.CopyProperties(item);
                    itemDb.Initials = initials;

                    await this.UpdateItemAsync(item);
                }
            }
        }
    }
}
