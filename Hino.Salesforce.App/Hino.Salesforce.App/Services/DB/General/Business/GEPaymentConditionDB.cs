﻿using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Services.DB.Interfaces.General.Business;
using Hino.Salesforce.App.Utils.Paging;
using SQLite;
using SQLiteNetExtensions.Extensions;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.DB.General.Business
{
    public class GEPaymentConditionDB : BaseDataStore<GEPaymentConditionModel>, IGEPaymentConditionDB
    {
        public override Task<PagedResult<GEPaymentConditionModel>> GetItemsAsync(int page)
        {
            var result = new List<GEPaymentConditionModel>();

            using (var db = new SQLiteConnection(_IDBSQLite.DataBasePath))
            {
                result = db.GetAllWithChildren<GEPaymentConditionModel>().OrderByDescending(r => r.Id)
                    .Skip((page - 1) * 50).Take(50).ToList();
            }
            return Task.FromResult(base.PaginateQuery(result, page));
        }

        public override Task<GEPaymentConditionModel> GetItemAsync(long id)
        {
            GEPaymentConditionModel result = null;

            using (var db = new SQLiteConnection(_IDBSQLite.DataBasePath))
            {
                result = db.GetAllWithChildren<GEPaymentConditionModel>().Where(r => r.Id == id).FirstOrDefault();
            }
            return Task.FromResult(result);
        }

        public override Task<PagedResult<GEPaymentConditionModel>> Query(Expression<System.Func<GEPaymentConditionModel, bool>> predicate, int page)
        {
            var result = new List<GEPaymentConditionModel>();

            using (var db = new SQLiteConnection(_IDBSQLite.DataBasePath))
            {
                result = db.GetAllWithChildren<GEPaymentConditionModel>().Where(predicate.Compile()).OrderByDescending(r => r.Id)
                    .Skip((page - 1) * 50).Take(50).ToList();
            }
            return Task.FromResult(base.PaginateQuery(result, page));
        }

        public async override Task<PagedResult<GEPaymentConditionModel>> Search(string pSearchData, int page)
        {
            if (!string.IsNullOrEmpty(pSearchData))
            {
                var num = ConvertSearchToNumber(pSearchData);
                return await Query(r =>
                    r.Description.ToLower().Contains(pSearchData.ToLower()) ||
                    r.GEPaymentType.Description.ToLower().Contains(pSearchData.ToLower()) ||
                    r.Id == num, page);
            }
            else
                return await GetItemsAsync(page);
        }

    }
}
