﻿using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Services.DB.Interfaces.General.Business;
using Hino.Salesforce.App.Utils.Paging;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.DB.General.Business
{
    public class GEPaymentTypeDB : BaseDataStore<GEPaymentTypeModel>, IGEPaymentTypeDB
    {
        public async override Task<PagedResult<GEPaymentTypeModel>> Search(string pSearchData, int page)
        {
            if (!string.IsNullOrEmpty(pSearchData))
            {
                var num = ConvertSearchToNumber(pSearchData);
                return await Query(r =>
                    r.Description.ToLower().Contains(pSearchData.ToLower()) ||
                    r.Id == num, page);
            }
            else
                return await GetItemsAsync(page);
        }
    }
}
