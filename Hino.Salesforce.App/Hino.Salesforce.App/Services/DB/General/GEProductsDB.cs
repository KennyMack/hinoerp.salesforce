﻿using Hino.Salesforce.App.Models.General;
using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Services.DB.Interfaces.General;
using Hino.Salesforce.App.Utils.Paging;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.DB.General
{
    public class GEProductsDB : BaseDataStore<GEProductsModel>, IGEProductsDB
    {
        public async Task<PagedResult<GEProductsModel>> SearchProdSale(string pSearchData, GEEnterprisesModel pGEEnterprises, int page)
        {
            var sqlQuery = $@"SELECT GEProductsModel.Created, GEProductsModel.Description, GEProductsModel.EstablishmentKey, 
                                     GEProductsModel.Family, GEProductsModel.Id, GEProductsModel.Image, GEProductsModel.IsActive, 
                                     GEProductsModel.Modified, GEProductsModel.Name, GEProductsModel.NCM, GEProductsModel.PercIPI, 
                                     GEProductsModel.PercMaxDiscount, GEProductsModel.ProductKey, GEProductsModel.Status, 
                                     GEProductsModel.StockBalance, GEProductsModel.Type, GEProductsModel.UniqueKey,
                                     VESalePriceModel.Value, GEProductsModel.Weight, GEProductsModel.PackageQTD,
                                     GEProductsModel.Unit, GEProductsModel.SaleUnit, GEProductsModel.SaleFactor
                                FROM GEProductsModel,
                                     VESalePriceModel
                               WHERE GEProductsModel.Id = VESalePriceModel.ProductId
                                 AND VESalePriceModel.RegionId  = {pGEEnterprises.RegionId} ";

            if (!string.IsNullOrEmpty(pSearchData))
                sqlQuery += $@" AND ((lower(GEProductsModel.Name) like '%{pSearchData}%')
                                     OR(lower(GEProductsModel.ProductKey) like '%{pSearchData}%')) ";

            sqlQuery += " ORDER BY GEProductsModel.Name ";

            if (page > 0)
                sqlQuery += $@" LIMIT {(page - 1) * 50},50 ";

            var result = await _DataBase.QueryAsync<GEProductsModel>(sqlQuery);
            return PaginateQuery(result, page);
        }

        public override async Task<PagedResult<GEProductsModel>> Search(string pSearchData, int page)
        {
            if (!string.IsNullOrEmpty(pSearchData))
                return await Query(r => r.Name.ToLower().Contains(pSearchData) ||
                                r.ProductKey.ToLower().Contains(pSearchData) ||
                                r.NCM.ToLower().Contains(pSearchData), page);
            else
                return await GetItemsAsync(page);

        }
    }
}
