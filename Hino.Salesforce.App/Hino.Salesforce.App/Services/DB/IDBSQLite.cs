﻿namespace Hino.Salesforce.App.Services.DB
{
    public interface IDBSQLite
    {
        string DataBasePath { get; }
    }
}
