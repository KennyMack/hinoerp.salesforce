﻿using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Utils.Paging;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.DB.Interfaces.General.Business
{
    public interface IGEEnterprisesDB : IBaseDataStore<GEEnterprisesModel>
    {
        Task<PagedResult<GEEnterprisesModel>> GetAllNew();
        Task<int> UpdateIdEnterprise(long pActualId, long pNewId);
        Task<PagedResult<GEEnterprisesModel>> SearchActives(string pSearchData, int page);

    }
}
