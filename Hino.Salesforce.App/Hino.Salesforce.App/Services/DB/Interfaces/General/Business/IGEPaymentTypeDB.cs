﻿using Hino.Salesforce.App.Models.General.Business;

namespace Hino.Salesforce.App.Services.DB.Interfaces.General.Business
{
    public interface IGEPaymentTypeDB : IBaseDataStore<GEPaymentTypeModel>
    {
    }
}
