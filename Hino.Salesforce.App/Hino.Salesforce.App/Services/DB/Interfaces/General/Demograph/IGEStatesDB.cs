﻿using Hino.Salesforce.App.Models.General.Demograph;

namespace Hino.Salesforce.App.Services.DB.Interfaces.General.Demograph
{
    public interface IGEStatesDB : IBaseDataStore<GEStatesModel>
    {
    }
}
