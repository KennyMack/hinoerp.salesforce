﻿using Hino.Salesforce.App.Models.General;
using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Utils.Paging;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.DB.Interfaces.General
{
    public interface IGEProductsDB : IBaseDataStore<GEProductsModel>
    {
        Task<PagedResult<GEProductsModel>> SearchProdSale(string pSearchData, GEEnterprisesModel pGEEnterprises, int page);
    }
}
