﻿using Hino.Salesforce.App.Models.General;

namespace Hino.Salesforce.App.Services.DB.Interfaces.General
{
    public interface IGEUsersDB : IBaseDataStore<GEUsersModel>
    {
    }
}
