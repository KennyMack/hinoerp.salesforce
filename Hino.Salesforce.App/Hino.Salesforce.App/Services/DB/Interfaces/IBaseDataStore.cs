﻿using Hino.Salesforce.App.Models;
using Hino.Salesforce.App.Utils.Paging;
using System;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.DB.Interfaces
{
    public interface IBaseDataStore<T> where T : BaseEntity, new()
    {

        Task<long> NextSequence();
        Task AddFromPagedResult(PagedResult<T> result);
        Task<T> AddOrUpdateItemAsync(T item);
        Task<T> AddItemAsync(T item);
        Task<T> UpdateItemAsync(T item);
        Task<bool> DeleteItemAsync(long id);
        Task<T> GetItemAsync(long id);
        Task<PagedResult<T>> GetItemsAsync(int page);
        Task<PagedResult<T>> Query(System.Linq.Expressions.Expression<Func<T, bool>> predicate, int page);
        Task<PagedResult<T>> Search(string pSearchData, int page);
        void ClearTable();

    }
}
