﻿using Hino.Salesforce.App.Models;
using System;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.DB.Interfaces
{
    public interface IConfigDB : IBaseDataStore<ConfigModel>
    {
        Task<ConfigModel> UpdateLastSync(DateTime pDate);
    }
}
