﻿using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.Utils.Paging;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.DB.Interfaces.Sales
{
    public interface IVEOrderItemsDB : IBaseDataStore<VEOrderItemsModel>
    {
        Task<bool> DeleteByOrderIdAsync(long id);
        Task<int> UpdateIdOrder(long pActualId, long pNewId);
        Task AddItemsFromOrder(PagedResult<VEOrdersModel> result);
    }
}
