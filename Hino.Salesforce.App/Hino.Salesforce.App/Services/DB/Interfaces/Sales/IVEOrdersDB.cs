﻿using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.Utils.Paging;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.DB.Interfaces.Sales
{
    public interface IVEOrdersDB : IBaseDataStore<VEOrdersModel>
    {
        Task<PagedResult<VEOrdersModel>> GetAllByEnterpriseId(long pId);
        Task<PagedResult<VEOrdersModel>> GetAllNew();
        Task<int> UpdateIdOrder(long pActualId, long pNewId);
    }
}
