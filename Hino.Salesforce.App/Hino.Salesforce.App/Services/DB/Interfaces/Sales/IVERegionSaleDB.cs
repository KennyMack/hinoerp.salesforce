﻿using Hino.Salesforce.App.Models.Sales;

namespace Hino.Salesforce.App.Services.DB.Interfaces.Sales
{
    public interface IVERegionSaleDB : IBaseDataStore<VERegionSaleModel>
    {
    }
}
