﻿using Hino.Salesforce.App.Models.Sales;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.DB.Interfaces.Sales
{
    public interface IVESalePriceDB : IBaseDataStore<VESalePriceModel>
    {
        Task<IList<VESalePriceModel>> GetValuesByProductId(long ProductId);
    }
}
