﻿using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.Services.DB.Interfaces.Sales;
using Hino.Salesforce.App.Utils.Paging;
using System;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.DB.Sales
{
    public class VEOrderItemsDB : BaseDataStore<VEOrderItemsModel>, IVEOrderItemsDB
    {
        public async Task<bool> DeleteByOrderIdAsync(long id)
        {
            try
            {
                var items = await Query(r => r.OrderID == id, -1);

                foreach (var item in items.Results)
                {
                    await DeleteItemAsync(item.Id);
                }
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        public async Task<int> UpdateIdOrder(long pActualId, long pNewId) =>
            await base._DataBase.ExecuteAsync(@"UPDATE VEOrderItemsModel SET OrderID = ? WHERE OrderID = ?", pNewId, pActualId);

        public async Task AddItemsFromOrder(PagedResult<VEOrdersModel> result)
        {
            foreach (var item in result.Results)
            {
                foreach (var itemSale in item.VEOrderItems)
                {
                    await this.AddOrUpdateItemAsync(itemSale);
                    /*
                    var itemDb = await this.GetItemAsync(itemSale.Id);

                    if (itemDb == null)
                        await this.AddItemAsync(itemSale);
                    else if (itemDb != null && !item.EqualTo(itemDb))
                    {
                        itemDb.CopyProperties(item);
                        await this.UpdateItemAsync(item);
                    }*/
                }


            }
        }
    }
}
