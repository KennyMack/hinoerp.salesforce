﻿using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.Services.DB.Interfaces.Sales;
using Hino.Salesforce.App.Utils.Paging;
using SQLite;
using SQLiteNetExtensions.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.DB.Sales
{
    public class VEOrdersDB : BaseDataStore<VEOrdersModel>, IVEOrdersDB
    {
        public override Task<PagedResult<VEOrdersModel>> GetItemsAsync(int page)
        {
            var result = new List<VEOrdersModel>();

            using (var db = new SQLiteConnection(_IDBSQLite.DataBasePath))
            {
                if (page > 0)
                {
                    result = db.GetAllWithChildren<VEOrdersModel>().OrderBy(r => r.StatusSinc).ThenByDescending(r => r.Id)
                         .Skip((page - 1) * 50).Take(50).ToList();
                }
                else
                {
                    result = db.GetAllWithChildren<VEOrdersModel>().OrderBy(r => r.StatusSinc).ThenByDescending(r => r.Id)
                        .ToList();
                }
            }
            return Task.FromResult<PagedResult<VEOrdersModel>>(base.PaginateQuery(result, page));
        }

        public override Task<PagedResult<VEOrdersModel>> Query(Expression<System.Func<VEOrdersModel, bool>> predicate, int page)
        {
            var result = new List<VEOrdersModel>();
            try
            {
                using (var db = new SQLiteConnection(_IDBSQLite.DataBasePath))
                {
                    if (page > 0)
                    {
                        result = db.GetAllWithChildren<VEOrdersModel>().Where(predicate.Compile()).OrderByDescending(r => r.Id)
                            .Skip((page - 1) * 50).Take(50).ToList();
                    }
                    else
                    {
                        result = db.GetAllWithChildren<VEOrdersModel>().Where(predicate.Compile()).OrderByDescending(r => r.Id)
                            .ToList();
                    }
                }
            }
            catch (Exception)
            {
            }

            return Task.FromResult<PagedResult<VEOrdersModel>>(base.PaginateQuery(result, page));
        }

        public async Task<PagedResult<VEOrdersModel>> GetAllNew()
        {
            return await Query(r => r.Id < 0 && r.IsProposal == false, -1);
        }

        public async Task<PagedResult<VEOrdersModel>> GetAllByEnterpriseId(long pId) =>
            await Query(r => r.EnterpriseID == pId, -1);

        public async Task<int> UpdateIdOrder(long pActualId, long pNewId) =>
            await base._DataBase.ExecuteAsync(@"UPDATE VEOrdersModel SET Id = ? WHERE Id = ?", pNewId, pActualId);
    }
}
