﻿using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.Services.DB.Interfaces.Sales;

namespace Hino.Salesforce.App.Services.DB.Sales
{
    public class VERegionSaleDB : BaseDataStore<VERegionSaleModel>, IVERegionSaleDB
    {
    }
}
