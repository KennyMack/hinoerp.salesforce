﻿using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.Services.DB.Interfaces.Sales;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.DB.Sales
{
    public class VESalePriceDB : BaseDataStore<VESalePriceModel>, IVESalePriceDB
    {
        public async Task<IList<VESalePriceModel>> GetValuesByProductId(long ProductId)
        {
            var Results = await Query(r => r.ProductId == ProductId, -1);

            return Results.Results;
        }
    }
}
