﻿using Hino.Salesforce.App.Models.Interfaces;
using Hino.Salesforce.App.Services.DB.Interfaces.General.Business;
using Hino.Salesforce.App.Services.Search.Interfaces;
using Hino.Salesforce.App.Utils.Extensions;
using Hino.Salesforce.App.Utils.Paging;
using Hino.Salesforce.App.ViewModels;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.Search.General.Business
{
    public class GEPaymentConditionSearch : ISearchDataResult
    {
        private IGEPaymentConditionDB _IGEPaymentTypeDB;
        public GEPaymentConditionSearch()
        {
            _IGEPaymentTypeDB = DInjection.GetIntance<IGEPaymentConditionDB>();
        }

        public string KeyName { get; set; }
        public string Description { get; set; }
        public string Details { get; set; }
        public string Title { get; set; }

        public async Task<PagedResult<SearchItemList>> GetDataFromSourceAsync(string pSearchData, int pPage)
        {
            var result = await _IGEPaymentTypeDB.Search(pSearchData, pPage);

            var lstSearchResult = new List<SearchItemList>();
            foreach (IBaseEntity item in result.Results)
            {
                var ret = item.GetProperties();

                var sil = new SearchItemList();

                if (!string.IsNullOrEmpty(KeyName))
                    sil.Id = Convert.ToString(ret[KeyName]);
                if (!string.IsNullOrEmpty(Description))
                    sil.Description = Convert.ToString(ret[Description]);
                if (!string.IsNullOrEmpty(Details))
                    sil.Details = Convert.ToString(ret[Details]);
                sil.Title = Title;
                lstSearchResult.Add(sil);
            }

            return new PagedResult<SearchItemList>
            {
                CurrentPage = result.CurrentPage,
                PageCount = result.PageCount,
                PageSize = result.PageSize,
                RowCount = result.RowCount,
                Results = lstSearchResult
            };

        }
    }
}
