﻿using Hino.Salesforce.App.Utils.Paging;
using Hino.Salesforce.App.ViewModels;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.Services.Search.Interfaces
{
    public interface ISearchDataResult
    {
        string KeyName { get; set; }
        string Description { get; set; }
        string Details { get; set; }
        string Title { get; set; }
        Task<PagedResult<SearchItemList>> GetDataFromSourceAsync(string pSearchData, int pPage);
    }
}
