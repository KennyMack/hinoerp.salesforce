﻿using Hino.Salesforce.App.Services.DB.Interfaces;
using Hino.Salesforce.App.Services.DB.Interfaces.General;
using Hino.Salesforce.App.Services.DB.Interfaces.General.Business;
using Hino.Salesforce.App.Services.DB.Interfaces.General.Demograph;
using Hino.Salesforce.App.Services.DB.Interfaces.Sales;

namespace Hino.Salesforce.App.Services.Utils
{
    public class DataBaseCleaner
    {
        private readonly IConfigDB _IConfigDB;
        private readonly IGEProductsDB _IGEProductsDB;
        private readonly IGEEnterprisesDB _IGEEnterprisesDB;
        private readonly IGEPaymentConditionDB _IGEPaymentConditionDB;
        private readonly IGEPaymentTypeDB _IGEPaymentTypeDB;
        private readonly IGECitiesDB _IGECitiesDB;
        private readonly IGECountriesDB _IGECountriesDB;
        private readonly IGEStatesDB _IGEStatesDB;
        private readonly IVEOrderItemsDB _IVEOrderItemsDB;
        private readonly IVEOrdersDB _IVEOrdersDB;
        private readonly IVESalePriceDB _IVESalePriceDB;
        private readonly IVERegionSaleDB _IVERegionSaleDB;

        public DataBaseCleaner()
        {
            _IConfigDB = DInjection.GetIntance<IConfigDB>();
            _IGEProductsDB = DInjection.GetIntance<IGEProductsDB>();
            _IGEEnterprisesDB = DInjection.GetIntance<IGEEnterprisesDB>();
            _IGEPaymentConditionDB = DInjection.GetIntance<IGEPaymentConditionDB>();
            _IGEPaymentTypeDB = DInjection.GetIntance<IGEPaymentTypeDB>();
            _IGECitiesDB = DInjection.GetIntance<IGECitiesDB>();
            _IGECountriesDB = DInjection.GetIntance<IGECountriesDB>();
            _IGEStatesDB = DInjection.GetIntance<IGEStatesDB>();
            _IVEOrderItemsDB = DInjection.GetIntance<IVEOrderItemsDB>();
            _IVEOrdersDB = DInjection.GetIntance<IVEOrdersDB>();
            _IVESalePriceDB = DInjection.GetIntance<IVESalePriceDB>();
            _IVERegionSaleDB = DInjection.GetIntance<IVERegionSaleDB>();
        }

        #region Clear DataBase
        public void ClearDataBase()
        {
            _IGEProductsDB.ClearTable();
            _IGEEnterprisesDB.ClearTable();
            _IGEPaymentConditionDB.ClearTable();
            _IGEPaymentTypeDB.ClearTable();
            _IGECitiesDB.ClearTable();
            _IGECountriesDB.ClearTable();
            _IGEStatesDB.ClearTable();
            _IVEOrderItemsDB.ClearTable();
            _IVEOrdersDB.ClearTable();
            _IConfigDB.ClearTable();
            _IVESalePriceDB.ClearTable();
            _IVERegionSaleDB.ClearTable();
        }
        #endregion

    }
}
