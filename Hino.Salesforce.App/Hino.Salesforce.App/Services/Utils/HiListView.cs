﻿using System.Collections;
using System.Windows.Input;
using Xamarin.Forms;

namespace Hino.Salesforce.App.Services.Utils
{
    public class HiListView : ListView
    {
        public static readonly BindableProperty LoadMoreCommandProperty =
            BindableProperty.Create("LoadMoreCommand", typeof(string), typeof(ICommand), null);

        public ICommand LoadMoreCommand
        {
            get { return (ICommand)GetValue(LoadMoreCommandProperty); }
            set { SetValue(LoadMoreCommandProperty, value); }
        }

        public HiListView()
        {
            ItemAppearing += InfiniteListView_ItemAppearing;
        }

        void InfiniteListView_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            var items = ItemsSource as IList;

            if (items != null && e.Item == items[items.Count - 1])
            {
                if (LoadMoreCommand != null && LoadMoreCommand.CanExecute(null))
                    LoadMoreCommand.Execute(null);
            }
        }
    }
}
