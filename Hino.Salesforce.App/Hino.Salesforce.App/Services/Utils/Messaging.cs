﻿using Hino.Salesforce.App.Utils.Load.Enums;
using Hino.Salesforce.App.Views.Auth;
using Hino.Salesforce.App.Views.Main;
using Rg.Plugins.Popup.Services;
using System;
using Xamarin.Forms;

namespace Hino.Salesforce.App.Services.Utils
{
    public class Messaging : IDisposable
    {
        private readonly string Message;
        public bool IsLoading;
        public Messaging(string pMessage)
        {
            Message = pMessage;
            IsLoading = false;
            if (Message == LoadType.ShowLoad.ToString())
            {
                ModalLoadingPage modal = null;
                MessagingCenter.Subscribe<Messaging, string>(this, Message, async (obj, txt) =>
                {
                    if (txt == Resources.ValidationMessagesResource.LoadStarting && !IsLoading)
                    {
                        IsLoading = true;
                        if (modal == null)
                        {
                            modal = new ModalLoadingPage(Message);
                            modal.ChangeStatusText(txt);
                            await PopupNavigation.Instance.PushAsync(modal);
                        }
                    }

                    modal?.ChangeStatusText(txt);
                    modal?.ChangeStatusErrorText("");

                    if (txt == Resources.ValidationMessagesResource.LoadComplete && IsLoading)
                    {
                        IsLoading = false;
                        modal = null;
                        await PopupNavigation.Instance.PopAsync();
                    }
                });
            }
            else if (Message == LoadType.RefreshLogin.ToString())
            {
                RefreshLoginPage modal = null;
                MessagingCenter.Subscribe<Messaging, string>(this, Message, async (obj, txt) =>
                {
                    if (txt == Resources.ErrorMessagesResource.SessionInvalidOrExpired && !IsLoading)
                    {
                        IsLoading = true;
                        if (modal == null)
                        {
                            modal = new RefreshLoginPage();
                            await PopupNavigation.Instance.PushAsync(modal);
                        }
                    }
                });
            }
        }

        #region Send Message
        public void SendMessage(string text)
        {
            MessagingCenter.Send(this, Message, text);
        }
        #endregion

        #region Unsubscribe
        public void Unsubscribe()
        {
            MessagingCenter.Unsubscribe<Messaging, string>(this, Message);
        }
        #endregion

        public void Dispose()
        {
            MessagingCenter.Unsubscribe<Messaging, string>(this, Message);
        }
    }
}
