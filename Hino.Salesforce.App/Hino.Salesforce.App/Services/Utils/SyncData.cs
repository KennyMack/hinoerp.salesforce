﻿using Hino.Salesforce.App.Models;
using Hino.Salesforce.App.Services.API.Interfaces.General;
using Hino.Salesforce.App.Services.API.Interfaces.General.Business;
using Hino.Salesforce.App.Services.API.Interfaces.General.Demograph;
using Hino.Salesforce.App.Services.API.Interfaces.Sales;
using Hino.Salesforce.App.Services.Application.Auth;
using Hino.Salesforce.App.Services.DB.Interfaces;
using Hino.Salesforce.App.Services.DB.Interfaces.General;
using Hino.Salesforce.App.Services.DB.Interfaces.General.Business;
using Hino.Salesforce.App.Services.DB.Interfaces.General.Demograph;
using Hino.Salesforce.App.Services.DB.Interfaces.Sales;
using Hino.Salesforce.App.Services.Utils;
using Hino.Salesforce.App.Utils.Interfaces;
using Hino.Salesforce.App.Utils.Load.Enums;
using Hino.Salesforce.App.Views.Main;
using Plugin.Connectivity;
using Plugin.Connectivity.Abstractions;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Hino.Salesforce.App.Services.SyncData
{
    public class SyncData : IDisposable
    {
        private readonly IMessage _MessageShow;
        private readonly Messaging _MessagePassword;
        public bool Synchonizing { get; private set; }
        private readonly IGEEnterprisesAPI _IGEEnterprisesAPI;
        private readonly IGEPaymentConditionAPI _IGEPaymentConditionAPI;
        private readonly IGEPaymentTypeAPI _IGEPaymentTypeAPI;
        private readonly IGEProductsAPI _IGEProductsAPI;
        private readonly IGECitiesAPI _IGECitiesAPI;
        private readonly IGECountriesAPI _IGECountriesAPI;
        private readonly IGEStatesAPI _IGEStatesAPI;
        private readonly IVEOrderItemsAPI _IVEOrderItemsAPI;
        private readonly IVEOrdersAPI _IVEOrdersAPI;
        private readonly IVESalePriceAPI _IVESalePriceAPI;
        private readonly IVERegionSaleAPI _IVERegionSaleAPI;

        private readonly IConfigDB _IConfigDB;
        private readonly IGEProductsDB _IGEProductsDB;
        private readonly IGEEnterprisesDB _IGEEnterprisesDB;
        private readonly IGEPaymentConditionDB _IGEPaymentConditionDB;
        private readonly IGEPaymentTypeDB _IGEPaymentTypeDB;
        private readonly IGECitiesDB _IGECitiesDB;
        private readonly IGECountriesDB _IGECountriesDB;
        private readonly IGEStatesDB _IGEStatesDB;
        private readonly IVEOrderItemsDB _IVEOrderItemsDB;
        private readonly IVEOrdersDB _IVEOrdersDB;
        private readonly IVESalePriceDB _IVESalePriceDB;
        private readonly IVERegionSaleDB _IVERegionSaleDB;

        private readonly AuthenticationService _AuthenticationService;

        public SyncData()
        {
            _IGEEnterprisesAPI = DInjection.GetIntance<IGEEnterprisesAPI>();
            _IGEPaymentConditionAPI = DInjection.GetIntance<IGEPaymentConditionAPI>();
            _IGEPaymentTypeAPI = DInjection.GetIntance<IGEPaymentTypeAPI>();
            _IGEProductsAPI = DInjection.GetIntance<IGEProductsAPI>();
            _IGECitiesAPI = DInjection.GetIntance<IGECitiesAPI>();
            _IGECountriesAPI = DInjection.GetIntance<IGECountriesAPI>();
            _IGEStatesAPI = DInjection.GetIntance<IGEStatesAPI>();
            _IVEOrderItemsAPI = DInjection.GetIntance<IVEOrderItemsAPI>();
            _IVEOrdersAPI = DInjection.GetIntance<IVEOrdersAPI>();
            _IVESalePriceAPI = DInjection.GetIntance<IVESalePriceAPI>();
            _IVERegionSaleAPI = DInjection.GetIntance<IVERegionSaleAPI>();

            _IConfigDB = DInjection.GetIntance<IConfigDB>();
            _IGEProductsDB = DInjection.GetIntance<IGEProductsDB>();
            _IGEEnterprisesDB = DInjection.GetIntance<IGEEnterprisesDB>();
            _IGEPaymentConditionDB = DInjection.GetIntance<IGEPaymentConditionDB>();
            _IGEPaymentTypeDB = DInjection.GetIntance<IGEPaymentTypeDB>();
            _IGECitiesDB = DInjection.GetIntance<IGECitiesDB>();
            _IGECountriesDB = DInjection.GetIntance<IGECountriesDB>();
            _IGEStatesDB = DInjection.GetIntance<IGEStatesDB>();
            _IVEOrderItemsDB = DInjection.GetIntance<IVEOrderItemsDB>();
            _IVEOrdersDB = DInjection.GetIntance<IVEOrdersDB>();
            _IVESalePriceDB = DInjection.GetIntance<IVESalePriceDB>();
            _IVERegionSaleDB = DInjection.GetIntance<IVERegionSaleDB>();

            _AuthenticationService = new AuthenticationService();
            _MessagePassword = new Messaging(LoadType.RefreshLogin.ToString());
            _MessageShow = DependencyService.Get<IMessage>();

        }

        public async Task<bool> RefreshTokenUser() =>
            await _AuthenticationService.CheckAndRefreshToken();

        public bool IsConnected()
        {
            if (!CrossConnectivity.IsSupported)
                return true;
            else
                return CrossConnectivity.Current.IsConnected;
        }

        public async Task<bool> SyncOnStart()
        {
            var Config = await _IConfigDB.GetItemAsync(1);

            return Config.SyncOnStart;
        }

        public async Task<bool> ConnectionTypeOK()
        {
            var Config = await _IConfigDB.GetItemAsync(1);

            var hasInternet = false;
            if (!CrossConnectivity.IsSupported)
                hasInternet = true;
            else
                hasInternet = CrossConnectivity.Current.IsConnected;

            if (hasInternet)
            {
                if (Config.SyncWIFI)
                {
                    var conType = CrossConnectivity.Current.ConnectionTypes;
                    hasInternet = conType.Where(r => r == ConnectionType.WiFi).Count() > 0 ?
                        true : false;

                    return hasInternet;
                }

                return hasInternet;
            }

            return false;
        }

        public bool HasToSync(ConfigModel pConfig)
        {
            if (Synchonizing)
                return false;

            var hasInternet = false;
            if (pConfig.LastSync.AddMinutes(pConfig.IntervalSync) > DateTime.Now)
                hasInternet = false;

            return hasInternet &&
               !Synchonizing;
        }

        public void ShowInvalidOrExpired()
        {
            _MessageShow.ShowShortMessage(Resources.ErrorMessagesResource.InvalidRefreshToken);
            _MessagePassword.SendMessage(Resources.ErrorMessagesResource.SessionInvalidOrExpired);
        }

        public async Task SendChanges()
        {
            if (Synchonizing)
                return;

            Synchonizing = true;
            await Task.Delay(200);

            if (!await RefreshTokenUser())
            {
                ShowInvalidOrExpired();
                return;
            }

            /*if (!await _AuthenticationService.CheckConnectionWithServer())
            {
                _MessageShow.ShowShortMessage(Resources.ErrorMessagesResource.ComunicationFail);
                return;
            }*/

            ModalLoadingPage modal = null;

            await PopupNavigation.Instance.PushAsync(modal = new ModalLoadingPage(LoadType.SyncStatus.ToString()));
            modal.ChangeStatusText(Resources.ValidationMessagesResource.LoadStarting);

            modal.ChangeStatusText(Resources.ValidationMessagesResource.LoadProcessing);

            await SendEnterprises();
            await SendOrders();

            modal.ChangeStatusText(Resources.ValidationMessagesResource.LoadComplete);
            await PopupNavigation.Instance.PopAsync();

            App.FIRSTSYNCOK = true;
            _MessageShow.ShowShortMessage(Resources.ValidationMessagesResource.SendWithSuccess);

            Synchonizing = false;
        }

        public async Task Syncronize(bool sendChanges = false)
        {
            if (Synchonizing)
                return;

            Synchonizing = true;
            await Task.Delay(200);

            if (!await RefreshTokenUser())
            {
                ShowInvalidOrExpired();
                return;
            }

            /*if (!await _AuthenticationService.CheckConnectionWithServer())
            {
                _MessageShow.ShowShortMessage(Resources.ErrorMessagesResource.ComunicationFail);
                return;
            }*/

            ModalLoadingPage modal = null;
            await PopupNavigation.Instance.PushAsync(modal = new ModalLoadingPage(LoadType.SyncStatus.ToString()));
            modal.ChangeStatusText(Resources.ValidationMessagesResource.LoadStarting);
            var config = await _IConfigDB.GetItemAsync(1);

            // Task TaskSyncCountries = SyncCountries(config.LastSync);
            // Task TaskSyncStates = SyncStates(config.LastSync);
            // Task TaskSyncCities = SyncCities(config.LastSync);
            Task TaskSyncPaymentType = SyncPaymentType(config.LastSync);
            Task TaskSyncPaymentCondition = SyncPaymentCondition(config.LastSync);
            Task TaskSyncProducts = SyncProducts(config.LastSync);
            Task TaskSyncEnterprises = SyncEnterprises(config.LastSync);
            Task TaskSyncOrders = SyncOrders(config.LastSync);
            Task TaskSyncRegions = SyncRegions(config.LastSync);
            Task TaskSyncSalesPrices = SyncSalesPrices(config.LastSync);

            var allTasks = new List<Task>
            {
                // TaskSyncCountries,
                // TaskSyncStates,
                // TaskSyncCities,
                TaskSyncPaymentType,
                TaskSyncPaymentCondition,
                TaskSyncRegions,
                TaskSyncProducts,
                TaskSyncSalesPrices,
                TaskSyncEnterprises,
                TaskSyncOrders
            };

            modal.ChangeStatusText(Resources.ValidationMessagesResource.LoadProcessing);
            foreach (var item in allTasks)
            {
                await item;
                await Task.Delay(100);
            }
            modal.ChangeStatusText(Resources.ValidationMessagesResource.LoadComplete);
            await PopupNavigation.Instance.PopAsync();

            if (sendChanges)
            {
                Synchonizing = false;
                await SendChanges();
                await _IConfigDB.UpdateLastSync(DateTime.Now);
            }


            Synchonizing = false;
        }

        public async Task SyncronizeProducts()
        {
            if (Synchonizing)
                return;

            Synchonizing = true;
            await Task.Delay(200);


            if (!await RefreshTokenUser())
            {
                ShowInvalidOrExpired();
                return;
            }

            /*if (!await _AuthenticationService.CheckConnectionWithServer())
            {
                _MessageShow.ShowShortMessage(Resources.ErrorMessagesResource.ComunicationFail);
                return;
            }*/

            ModalLoadingPage modal = null;
            await PopupNavigation.Instance.PushAsync(modal = new ModalLoadingPage(LoadType.SyncStatus.ToString()));
            modal.ChangeStatusText(Resources.ValidationMessagesResource.LoadStarting);
            var config = await _IConfigDB.GetItemAsync(1);

            Task TaskSyncRegions = SyncRegions(new DateTime(2001, 01, 01, 0, 0, 0));
            Task TaskSyncSalesPrices = SyncSalesPrices(new DateTime(2001, 01, 01, 0, 0, 0));
            Task TaskSyncProducts = SyncProducts(new DateTime(2001, 01, 01, 0, 0, 0));


            var allTasks = new List<Task>
            {
                TaskSyncRegions,
                TaskSyncSalesPrices,
                TaskSyncProducts
            };

            modal.ChangeStatusText(Resources.ValidationMessagesResource.LoadProcessing);
            foreach (var item in allTasks)
            {
                await item;
                await Task.Delay(100);
            }

            await SendEnterprises();
            await SendOrders();

            modal.ChangeStatusText(Resources.ValidationMessagesResource.LoadComplete);
            await PopupNavigation.Instance.PopAsync();

            Synchonizing = false;
        }

        public async Task SyncronizeOrders()
        {
            if (Synchonizing)
                return;

            Synchonizing = true;
            await Task.Delay(200);


            if (!await RefreshTokenUser())
            {
                ShowInvalidOrExpired();
                return;
            }

            /*if (!await _AuthenticationService.CheckConnectionWithServer())
            {
                _MessageShow.ShowShortMessage(Resources.ErrorMessagesResource.ComunicationFail);
                return;
            }*/

            ModalLoadingPage modal = null;
            await PopupNavigation.Instance.PushAsync(modal = new ModalLoadingPage(LoadType.SyncStatus.ToString()));
            modal.ChangeStatusText(Resources.ValidationMessagesResource.LoadStarting);
            var config = await _IConfigDB.GetItemAsync(1);

            //Task TaskSyncPaymentType = SyncPaymentType(new DateTime(2001, 01, 01, 0, 0, 0));
            //Task TaskSyncPaymentCondition = SyncPaymentCondition(new DateTime(2001, 01, 01, 0, 0, 0));
            //Task TaskSyncProducts = SyncProducts(new DateTime(2001, 01, 01, 0, 0, 0));
            // Task TaskSyncEnterprises = SyncEnterprises(new DateTime(2001, 01, 01, 0, 0, 0));
            Task TaskSyncOrders = SyncOrders(new DateTime(2001, 01, 01, 0, 0, 0));


            var allTasks = new List<Task>
            {
                //TaskSyncPaymentType,
               // TaskSyncPaymentCondition,
                //TaskSyncProducts,
                //TaskSyncEnterprises,
                TaskSyncOrders
            };

            modal.ChangeStatusText(Resources.ValidationMessagesResource.LoadProcessing);
            foreach (var item in allTasks)
            {
                await item;
                await Task.Delay(100);
            }

            //await SendEnterprises();
            await SendOrders();

            modal.ChangeStatusText(Resources.ValidationMessagesResource.LoadComplete);
            await PopupNavigation.Instance.PopAsync();

            Synchonizing = false;
        }

        #region Sync Countries
        private async Task SyncCountries(DateTime pLastSync)
        {
            try
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "S-Sincronizando Países");
                var results = await _IGECountriesAPI.GetAllSyncAsync(pLastSync);

                await _IGECountriesDB.AddFromPagedResult(results);
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Sincronizando Países com sucesso");
            }
            catch (Exception)
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Problema ao sincronizar Países");
            }

        }
        #endregion

        #region Sync States
        private async Task SyncStates(DateTime pLastSync)
        {
            try
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "S-Sincronizando Estados");
                var results = await _IGEStatesAPI.GetAllSyncAsync(pLastSync);


                await _IGEStatesDB.AddFromPagedResult(results);
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Sincronizando Estados com sucesso");
            }
            catch (Exception)
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Problema ao sincronizar Estados");
            }
        }
        #endregion

        #region Sync Cities
        private async Task SyncCities(DateTime pLastSync)
        {
            try
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "S-Sincronizando Cidades");
                var results = await _IGECitiesAPI.GetAllSyncAsync(pLastSync);


                await _IGECitiesDB.AddFromPagedResult(results);
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Sincronizando Cidades com sucesso");
            }
            catch (Exception)
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Problema ao sincronizar Cidades");
            }
        }
        #endregion

        #region Sync Payment Type
        private async Task SyncPaymentType(DateTime pLastSync)
        {
            try
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "S-Sincronizando Forma pgto.");
                var results = await _IGEPaymentTypeAPI.GetAllSyncAsync(pLastSync);


                await _IGEPaymentTypeDB.AddFromPagedResult(results);
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Sincronizando Forma pgto. com sucesso");
            }
            catch (Exception)
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Problema ao sincronizar Forma pgto.");
            }
        }
        #endregion

        #region Sync Payment Condition
        private async Task SyncPaymentCondition(DateTime pLastSync)
        {
            try
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "S-Sincronizando Cond. pgto.");
                var results = await _IGEPaymentConditionAPI.GetAllSyncAsync(pLastSync);


                await _IGEPaymentConditionDB.AddFromPagedResult(results);
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Sincronizando Cond. pgto. com sucesso");
            }
            catch (Exception)
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Problema ao sincronizar Cond. pgto.");
            }
        }
        #endregion

        #region Sync Products
        private async Task SyncProducts(DateTime pLastSync)
        {
            try
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "S-Sincronizando Produtos");
                var results = await _IGEProductsAPI.GetAllSyncAsync(pLastSync);


                await _IGEProductsDB.AddFromPagedResult(results);
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Sincronizando Produtos com sucesso");
            }
            catch (Exception)
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Problema ao sincronizar Produtos");
            }
        }
        #endregion

        #region Sync Regions
        private async Task SyncRegions(DateTime pLastSync)
        {
            try
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "S-Sincronizando Regiões");
                var results = await _IVERegionSaleAPI.GetAllSyncAsync(new DateTime(2000, 01, 01, 0, 0, 0));

                await _IVERegionSaleDB.AddFromPagedResult(results);

                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Sincronizando Regiões com sucesso");
            }
            catch (Exception)
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Problema ao sincronizar Regiões");
            }
        }
        #endregion

        #region Sync Sales Prices
        private async Task SyncSalesPrices(DateTime pLastSync)
        {
            try
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "S-Sincronizando Preços de venda");
                var results = await _IVESalePriceAPI.GetAllSyncAsync(pLastSync);

                await _IVESalePriceDB.AddFromPagedResult(results);
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Sincronizando Preços de venda com sucesso");
            }
            catch (Exception)
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Problema ao sincronizar preços de venda");
            }
        }
        #endregion

        #region Sync Enterprises
        private async Task SyncEnterprises(DateTime pLastSync)
        {
            try
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "S-Sincronizando Clientes");
                var results = await _IGEEnterprisesAPI.GetAllSyncByUserIdAsync(pLastSync, App.UserLogged.Id);


                await _IGEEnterprisesDB.AddFromPagedResult(results);
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Sincronizando Clientes com sucesso");
            }
            catch (Exception)
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Problema ao sincronizar Clientes");
            }
        }
        #endregion

        #region Sync Orders
        private async Task SyncOrders(DateTime pLastSync)
        {
            try
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "S-Sincronizando Pedidos");
                var results = await _IVEOrdersAPI.GetAllSyncUserAsync(App.UserLogged.Id, pLastSync);


                await _IVEOrdersDB.AddFromPagedResult(results);

                /*var itemsSale = results.Results.Select(s => s.VEOrderItems);*/

                /*var resultsItems = await _IVEOrderItemsAPI.GetAllSyncAsync(pLastSync);*/


                await _IVEOrderItemsDB.AddItemsFromOrder(results);
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Sincronizando Pedidos com sucesso");
            }
            catch (Exception)
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Problema ao sincronizar Pedidos");
            }
        }
        #endregion

        #region Send Enterprises
        private async Task SendEnterprises()
        {
            try
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "S-Sincronizando Clientes");

                var results = (await _IGEEnterprisesDB.GetAllNew()).Results;

                foreach (var enterprise in results)
                {
                    enterprise.IdApi = enterprise.Id;
                    enterprise.Id = 0;
                    enterprise.StatusSinc = 1;

                    var newEnterprise = await _IGEEnterprisesAPI.PostAsync(enterprise);

                    await _IGEEnterprisesDB.UpdateIdEnterprise((long)enterprise.IdApi, newEnterprise.Id);

                    enterprise.Id = newEnterprise.Id;
                    enterprise.Status = newEnterprise.Status;
                    enterprise.StatusSinc = 1;
                    enterprise.EstablishmentKey = newEnterprise.EstablishmentKey;
                    enterprise.UniqueKey = newEnterprise.UniqueKey;

                    await _IGEEnterprisesDB.UpdateItemAsync(enterprise);

                    var Orders = (await _IVEOrdersDB.GetAllByEnterpriseId((long)enterprise.IdApi)).Results;

                    foreach (var order in Orders)
                    {
                        order.EnterpriseID = enterprise.Id;

                        await _IVEOrdersDB.UpdateItemAsync(order);
                    }
                }

                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Sincronizando Clientes com sucesso");
            }
            catch (Exception)
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Problema ao sincronizar Clientes");
            }
        }
        #endregion

        #region Send Orders
        private async Task SendOrders()
        {
            try
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "S-Sincronizando Pedidos");

                var results = (await _IVEOrdersDB.GetAllNew()).Results;

                foreach (var order in results)
                {
                    order.IdApi = order.Id;
                    order.Id = 0;
                    order.StatusSinc = 1;

                    var newOrder = await _IVEOrdersAPI.PostAsync(order);

                    await _IVEOrdersDB.UpdateIdOrder((long)order.IdApi, newOrder.Id);

                    order.Id = newOrder.Id;
                    order.Status = newOrder.Status;
                    order.StatusSinc = 1;
                    order.EstablishmentKey = newOrder.EstablishmentKey;
                    order.UniqueKey = newOrder.UniqueKey;

                    await _IVEOrdersDB.UpdateItemAsync(order);

                    await _IVEOrderItemsDB.UpdateIdOrder((long)order.IdApi, newOrder.Id);
                }

                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Sincronizando Pedidos com sucesso");
            }
            catch (Exception)
            {
                MessagingCenter.Send(this, LoadType.SyncStatus.ToString(), "E-Problema ao sincronizar Pedidos");
            }
        }
        #endregion

        public void Dispose() =>
            _MessagePassword.Dispose();
    }
}

