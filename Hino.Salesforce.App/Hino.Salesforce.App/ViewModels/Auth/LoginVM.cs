﻿using Hino.Salesforce.App.Models.Auth;
using Hino.Salesforce.App.Services.API.Interfaces.Auth;
using Hino.Salesforce.App.Services.Application.Auth;
using Hino.Salesforce.App.Views.Auth;
using Hino.Salesforce.App.Views.Main;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Hino.Salesforce.App.ViewModels.Auth
{
    public class LoginVM : BaseViewModel
    {
        private LoginModel _LoginModel;
        public IAuthAPI _IAuthAPI;
        public ICommand CmdLoginClick { protected set; get; }
        public ICommand CmdRegisterClick { protected set; get; }

        public string LBLESTABLISHMENTKEY { get => Hino.Salesforce.App.Resources.FieldsNameResource.EstablishmentKey; }
        public string LBLPASSWORD { get => Hino.Salesforce.App.Resources.FieldsNameResource.Password; }
        public string LBLUSEROREMAIL { get => Hino.Salesforce.App.Resources.FieldsNameResource.UserName; }

        public bool NOTHASESTABLISHMENT { get => string.IsNullOrEmpty(App.ESTABLISHMENTKEY); }

        private string _UserOrEmail;
        private string _Password;
        private string _EstablishmentKey;

        public string ESTABLISHMENTKEY
        {
            get
            {
                return _EstablishmentKey;
            }
            set
            {
                SetProperty(ref _EstablishmentKey, value);
            }
        }

        public string PASSWORD
        {
            get
            {
                return _Password;
            }
            set
            {
                SetProperty(ref _Password, value);
            }
        }

        public string USEROREMAIL
        {
            get
            {
                return _UserOrEmail;
            }
            set
            {
                SetProperty(ref _UserOrEmail, value);
            }
        }

        #region Initialize
        private void Initialize()
        {
            _LoginModel = new LoginModel();
            CmdLoginClick = new Command(OnLoginClick);
            CmdRegisterClick = new Command(OnRegisterClick);
            _IAuthAPI = DInjection.GetIntance<IAuthAPI>();
            if (!string.IsNullOrEmpty(App.ESTABLISHMENTKEY))
                ESTABLISHMENTKEY = App.ESTABLISHMENTKEY;
        }
        #endregion

        public LoginVM(string pEmail)
        {
            USEROREMAIL = pEmail;
            Initialize();
        }

        public LoginVM()
        {
            Initialize();
        }

        public async void OnRegisterClick()
        {
            if (IsBusy)
                return;

            this.ClearLoadMessage();
            await ChangePage(new RegisterPage());
        }

        public async void OnLoginClick()
        {
            if (IsBusy)
                return;
            RegisterLoading<LoginVM>();
            _LoginModel.UserOrEmail = _UserOrEmail;
            _LoginModel.Password = _Password;
            _LoginModel.EstablishmentKey = _EstablishmentKey;

            if (string.IsNullOrEmpty(_LoginModel.UserOrEmail))
                ShowShortMessage("Informe o usuário");
            else if (string.IsNullOrEmpty(_LoginModel.Password))
                ShowShortMessage("Informe a senha");
            else if (string.IsNullOrEmpty(_LoginModel.EstablishmentKey))
                ShowShortMessage("Informe a chave do estabelecimento");
            else
            {
                IsBusy = true;
                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadStarting);
                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadProcessing);
                await Task.Delay(200);
                try
                {
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.ValidatingCredentials);


                    var AuthService = new AuthenticationService();
                    var ret = await AuthService.LoginAsync(_LoginModel);

                    if (!ret.success)
                    {
                        IsBusy = false;
                        ShowShortMessage(ret.error_description);
                        return;
                    }

                    base.SendLoadingMessage(Resources.ValidationMessagesResource.GetingUserInformation);

                    var message = await AuthService.SetUserLogged();
                    if (message != "")
                    {
                        IsBusy = false;
                        ShowShortMessage(message);
                        return;
                    }
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoginSucceeded);

                    await Task.Delay(200);
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);

                    App.Current.MainPage = new MasterMainPageMenu();
                }
                catch (Exception)
                {
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);
                    ShowShortMessage(Resources.ErrorMessagesResource.ComunicationFail);
                }
                finally
                {
                    IsBusy = false;
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);
                }
            }
            this.ClearLoadMessage();
        }
    }
}
