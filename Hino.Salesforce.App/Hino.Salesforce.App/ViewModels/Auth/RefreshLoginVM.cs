﻿using Hino.Salesforce.App.Models.Auth;
using Hino.Salesforce.App.Services.Application.Auth;
using Rg.Plugins.Popup.Services;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Hino.Salesforce.App.ViewModels.Auth
{
    public class RefreshLoginVM : BaseViewModel
    {
        private readonly AuthenticationService _AuthenticationService;
        public ICommand CmdLoginClick { protected set; get; }
        public string LBLPASSWORD { get => Resources.FieldsNameResource.Password; }
        public string LBLTEXT { get => Resources.ErrorMessagesResource.InvalidRefreshToken; }

        private string _Password;
        public string PASSWORD
        {
            get
            {
                return _Password;
            }
            set
            {
                SetProperty(ref _Password, value);
            }
        }

        public RefreshLoginVM()
        {
            CmdLoginClick = new Command(OnLoginClick);
            Title = "Senha";
            _AuthenticationService = new AuthenticationService();
        }


        public async void OnLoginClick()
        {
            if (IsBusy)
                return;

            var _LoginModel = new LoginModel
            {
                UserOrEmail = App.UserLogged.Email,
                Password = _Password,
                EstablishmentKey = App.ESTABLISHMENTKEY
            };

            if (string.IsNullOrEmpty(_LoginModel.Password))
                ShowShortMessage("Informe a senha");
            else
            {
                IsBusy = true;
                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadStarting);
                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadProcessing);
                await Task.Delay(200);
                try
                {
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.ValidatingCredentials);

                    var ret = await _AuthenticationService.LoginAsync(_LoginModel);

                    if (!ret.success)
                    {
                        IsBusy = false;
                        ShowShortMessage(ret.error_description);
                        return;
                    }

                    base.SendLoadingMessage(Resources.ValidationMessagesResource.GetingUserInformation);

                    var message = await _AuthenticationService.SetUserLogged();
                    if (message != "")
                    {
                        IsBusy = false;
                        ShowShortMessage(message);
                        return;
                    }
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoginSucceeded);

                    await Task.Delay(200);
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);

                    await PopupNavigation.Instance.PopAsync();
                }
                catch (Exception)
                {
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);
                    ShowShortMessage(Resources.ErrorMessagesResource.ComunicationFail);
                }
                finally
                {
                    IsBusy = false;
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);
                }
            }
        }

        public async Task OnBackClick()
        {
            await PopupNavigation.Instance.PopAsync();
        }
    }
}
