﻿using Hino.Salesforce.App.Models;
using Hino.Salesforce.App.Models.Validation.Auth;
using Hino.Salesforce.App.Services.API.Interfaces.Auth;
using Hino.Salesforce.App.Services.API.Interfaces.General;
using Hino.Salesforce.App.Utils.Messages;
using System;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Hino.Salesforce.App.ViewModels.Auth
{
    public class RegisterVM : BaseViewModel
    {
        public ICommand CmdRegisterClick { protected set; get; }
        public IAuthAPI _IAuthAPI;
        public IUsersAPI _IUsersAPI;
        private RegisterModel _RegisterModel;

        public string LBLESTABLISHMENTKEY { get => Hino.Salesforce.App.Resources.FieldsNameResource.EstablishmentKey; }
        public string LBLUSERKEY { get => Hino.Salesforce.App.Resources.FieldsNameResource.UserKey; }
        public string LBLUSERNAME { get => Hino.Salesforce.App.Resources.FieldsNameResource.UserName; }
        public string LBLEMAIL { get => Hino.Salesforce.App.Resources.FieldsNameResource.Email; }
        public string LBLPASSWORD { get => Hino.Salesforce.App.Resources.FieldsNameResource.Password; }
        public string LBLPASSWORDCONFIRM { get => Hino.Salesforce.App.Resources.FieldsNameResource.PasswordConfirm; }

        private string _EstablishmentKey;
        private string _UserKey;
        private string _UserName;
        private string _Email;
        private string _Password;
        private string _PasswordConfirm;

        public string ESTABLISHMENTKEY { get => _EstablishmentKey; set => SetProperty(ref _EstablishmentKey, value); }

        public string USERKEY { get => _UserKey; set => SetProperty(ref _UserKey, value); }

        public string USERNAME { get => _UserName; set => SetProperty(ref _UserName, value); }

        public string EMAIL { get => _Email; set => SetProperty(ref _Email, value); }

        public string PASSWORD { get => _Password; set => SetProperty(ref _Password, value); }

        public string PASSWORDCONFIRM { get => _PasswordConfirm; set => SetProperty(ref _PasswordConfirm, value); }

        public RegisterVM()
        {
            _RegisterModel = new RegisterModel();
            CmdRegisterClick = new Command(OnRegisterClick);
            _Validator = new RegisterValidator();

            _IAuthAPI = DInjection.GetIntance<IAuthAPI>();
            _IUsersAPI = DInjection.GetIntance<IUsersAPI>();
        }

        public async Task GoToLogin() =>
            await App.Current.MainPage.Navigation.PopToRootAsync();

        public async void OnRegisterClick()
        {
            if (IsBusy)
                return;
            this.RegisterLoading<RegisterVM>();

            _RegisterModel.EstablishmentKey = _EstablishmentKey;
            _RegisterModel.UserKey = _UserKey;
            _RegisterModel.UserName = _UserName;
            _RegisterModel.Email = _Email;
            _RegisterModel.Password = _Password;
            _RegisterModel.PasswordConfirm = _PasswordConfirm;
            _RegisterModel.OriginCreate = "MOBILE";

            if (await ValidateModelAsync(_RegisterModel))
            {
                IsBusy = true;
                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadStarting);
                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadProcessing);

                try
                {
                    var ret = await _IUsersAPI.CreateUserAsync(_RegisterModel);

                    if (!ret.success)
                    {
                        await Task.Delay(200);
                        base.SendLoadingMessage(Resources.ValidationMessagesResource.RegisterFail);
                        IsBusy = false;
                        ShowShortMessage(ret.error[0].Messages.ToArray()[0]);
                        return;
                    }

                    App.ESTABLISHMENTKEY = _RegisterModel.EstablishmentKey;
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.RegisterSucceeded);
                    await Task.Delay(200);
                    ShowShortMessage(Resources.ValidationMessagesResource.GoToLogin);
                    await Task.Delay(200);
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);

                    OnMessageRaise(new MessageRaiseEvent(
                        string.Concat(Resources.ValidationMessagesResource.RegisterSucceeded,
                        "   ", Resources.ValidationMessagesResource.GoToLogin)));

                }
                catch (Exception)
                {
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);
                    ShowShortMessage(Resources.ErrorMessagesResource.ComunicationFail);
                }
                finally
                {
                    IsBusy = false;
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);
                }
            }
            else
            {
                DisplayValidationModelErrors(true);
            }
            ClearLoadMessage();
        }
    }
}
