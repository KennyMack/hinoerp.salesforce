﻿using FluentValidation;
using FluentValidation.Results;
using Hino.Salesforce.App.Models;
using Hino.Salesforce.App.Services.DB;
using Hino.Salesforce.App.Services.Utils;
using Hino.Salesforce.App.Templates.FloatingButton;
using Hino.Salesforce.App.Utils.Interfaces;
using Hino.Salesforce.App.Utils.Load.Enums;
using Hino.Salesforce.App.Utils.Paging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.MultiSelectListView;

namespace Hino.Salesforce.App.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        public ICommand RefreshDataCommand { get; }
        public ICommand LoadMoreCommand { get; }
        public IValidator _Validator;
        public ValidationResult _ValidationResult;
        private Messaging _LoadMessaging;

        public void ShowShortMessage(string message)
        {
            var instance = DependencyService.Get<IMessage>();
            instance.ShowShortMessage(message);
        }

        public IDBSQLite GetDBInstance() => DependencyService.Get<IDBSQLite>();

        public IDataStore<Item> DataStore => DependencyService.Get<IDataStore<Item>>() ?? new MockDataStore();

        public BaseViewModel()
        {
            RefreshDataCommand = new Command(RefreshData);
            LoadMoreCommand = new Command(async () => await LoadMoreData());
            _LoadMessaging = null;
        }

        #region Send Loading Message
        public void SendLoadingMessage(string message)
        {
            _LoadMessaging?.SendMessage(message);
        }
        #endregion

        #region Register Loading
        public void RegisterLoading<T>() where T : class
        {
            if (_LoadMessaging == null)
                _LoadMessaging = new Messaging(LoadType.ShowLoad.ToString());
        }
        #endregion

        private bool _ShowSearch = false;
        public bool ShowSearch
        {
            get => _ShowSearch;
            set
            {
                SetProperty(ref _ShowSearch, value);
            }
        }

        int _pageNum = 1;
        public int PageNum
        {
            get { return _pageNum; }
            set
            {
                SetProperty(ref _pageNum, value);
            }
        }

        bool _isBusy = false;
        public bool IsBusy
        {
            get { return _isBusy; }
            set
            {
                SetProperty(ref _isBusy, value);
                IsNotBusy = !value;
            }
        }

        bool _isNotBusy = true;
        public bool IsNotBusy
        {
            get { return _isNotBusy; }
            set { SetProperty(ref _isNotBusy, value); }
        }

        string _title = string.Empty;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        protected bool SetProperty<T>(ref T backingStore, T value,
            [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return false;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
            return true;
        }

        #region INotifyPropertyChanged
        public event PropertyChangedEventHandler PropertyChanged;
        protected void OnPropertyChanged([CallerMemberName] string propertyName = "")
        {
            var changed = PropertyChanged;
            if (changed == null)
                return;

            changed.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        protected virtual void RefreshData()
        {
            if (IsBusy)
                return;
            this.PageNum = 1;

        }

        protected virtual Task LoadMoreData()
        {
            return Task.FromResult(1);
        }

        public virtual int NextPageNum() =>
            PageNum++;

        #region Message Information
        public event EventHandler<IMessageRaise> MessageRaise;
        protected void OnMessageRaise(IMessageRaise e) =>
            MessageRaise?.Invoke(this, e);
        #endregion

        #region Message Confirmation
        public event EventHandler<IMessageRaise> MessageConfirmRaise;
        protected void OnMessageConfirmRaise(IMessageRaise e) =>
            MessageConfirmRaise?.Invoke(this, e);
        #endregion

        #region Load Complete
        public event EventHandler<ILoadComplete> OnLoadComplete;
        protected void OnLoadCompleteRaise(ILoadComplete e)
        {
            this.IsBusy = false;
            OnLoadComplete?.Invoke(this, e);
        }
        #endregion

        #region Display Validation Model Errors
        public void DisplayValidationModelErrors(bool first = false)
        {
            if (_ValidationResult != null)
            {
                foreach (var error in _ValidationResult.Errors)
                {
                    ShowShortMessage(error.ErrorMessage);
                    if (first)
                        break;
                }
            }
        }
        #endregion

        #region Validate Model
        public async Task<bool> ValidateModelAsync<T>(T model)
        {
            try
            {
                _ValidationResult = await _Validator?.ValidateAsync(model);

                return _ValidationResult.IsValid;
            }
            catch (Exception)
            {
                throw new NotImplementedException("Tá sem a instância Bahiano!!!!");
            }
        }
        #endregion

        #region Change Page
        public async Task ChangePage(Page page) =>
            await App.Current.MainPage.Navigation.PushAsync(page);
        #endregion

        #region Clear LoadMessage
        public void ClearLoadMessage()
        {
            _LoadMessaging?.Unsubscribe();
            _LoadMessaging = null;
        }
        #endregion

        #region Infinity List View
        public void InfiniteListView(ListView pList)
        {
            pList.ItemAppearing -= null;
            pList.ItemAppearing += InfiniteListView_ItemAppearing;
        }
        void InfiniteListView_ItemAppearing(object sender, ItemVisibilityEventArgs e)
        {
            var lv = ((ListView)sender);
            var items = lv.ItemsSource as IList;

            if (IsBusy || items.Count == 0)
                return;

            if (items != null && e.Item == items[items.Count - 1])
            {
                if (LoadMoreCommand != null && LoadMoreCommand.CanExecute(null))
                {
                    NextPageNum();
                    LoadMoreCommand.Execute(null);
                }
            }
        }
        #endregion

        #region SearchData

        protected string _SearchData;
        public string SEARCHDATA
        {
            get
            {
                return _SearchData;
            }
            set
            {
                SetProperty(ref _SearchData, value);
            }
        }

        public void SearchEvent(SearchBar sbText)
        {
            sbText.TextChanged -= null;
            sbText.TextChanged += SbText_TextChanged;
            sbText.SearchButtonPressed -= null;
            sbText.SearchButtonPressed += SbText_SearchButtonPressed;
        }

        private async void SbText_SearchButtonPressed(object sender, EventArgs e)
        {
            await SearchData_Changed();
        }

        private async void SbText_TextChanged(object sender, TextChangedEventArgs e)
        {
            await SearchData_Changed();
        }

        public virtual Task SearchData_Changed()
        {
            return Task.FromResult<object>(null);
        }
        #endregion


        #region FAB Button
        public void FABButton(HIFloatingButton pfabBtn)
        {
            pfabBtn.Clicked += OnFABButtonTapped;
        }

        public virtual void OnFABButtonTapped(object sender, EventArgs e)
        {

        }
        #endregion
    }

    public static class ObservableCollectionEx
    {
        #region Copy From List
        public static void CopyFromList<T>(this ObservableCollection<T> to, IEnumerable<T> from, bool pClearList = true)
        {
            if (pClearList)
                to.Clear();

            foreach (var item in from)
            {
                to.Add(item);
            }
        }
        #endregion

        #region Copy From Paged Result
        public static void CopyFromPagedResult<T>(this ObservableCollection<T> to, PagedResult<T> from, bool pClearList = true) where T : class
        {
            if (pClearList)
                to.Clear();

            foreach (var item in from.Results)
            {
                to.Add(item);
            }
        }
        #endregion

        #region Copy From Paged Result
        public static void CopyFromPagedResult<T>(this MultiSelectObservableCollection<T> to, PagedResult<T> from, bool pClearList = true) where T : class
        {
            if (pClearList)
                to.Clear();

            foreach (var item in from.Results)
            {
                to.Add(item);
            }
        }
        #endregion
    }


}
