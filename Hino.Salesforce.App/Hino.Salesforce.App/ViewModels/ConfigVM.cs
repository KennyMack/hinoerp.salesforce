﻿using Hino.Salesforce.App.Models;
using Hino.Salesforce.App.Models.Validation;
using Hino.Salesforce.App.Services.DB.Interfaces;
using Hino.Salesforce.App.Services.SyncData;
using Hino.Salesforce.App.Services.Utils;
using Hino.Salesforce.App.Utils.Messages;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace Hino.Salesforce.App.ViewModels
{
    public class ConfigVM : BaseViewModel
    {
        private ConfigModel _ConfigModel;
        private readonly IConfigDB _IConfigDB;
        public ICommand CmdSaveClick { protected set; get; }
        public ICommand CmdClearDataClick { protected set; get; }
        public ICommand CmdSincAllClick { protected set; get; }

        public string LBLSYNCWIFI { get => Resources.FieldsNameResource.SyncWIFI; }
        public string LBLUSERKEY { get => Resources.FieldsNameResource.UserKey; }
        public string LBLESTABLISHMENTKEY { get => Resources.FieldsNameResource.EstablishmentKey; }
        public string LBLUSERLOGGED { get => Resources.FieldsNameResource.UserLogged; }
        public string LBLSYNCONSTART { get => Resources.FieldsNameResource.SyncOnStart; }
        public string LBLSYNCONSAVE { get => Resources.FieldsNameResource.SyncOnSave; }
        public string LBLINTERVALSYNC { get => Resources.FieldsNameResource.IntervalSync; }
        public string LBLLASTSYNC { get => Resources.FieldsNameResource.LastSync; }

        string _EstablishmentKey;
        string _UserKey;
        bool _SyncWIFI;
        bool _SyncOnStart;
        bool _SyncOnSave;
        string _IntervalSync;
        string _LastSync;

        #region Properties
        public string ESTABLISHMENTKEY
        {
            get => _EstablishmentKey;
            set
            {
                SetProperty(ref _EstablishmentKey, value);
            }
        }
        public string USERKEY
        {
            get => _UserKey;
            set
            {
                SetProperty(ref _UserKey, value);
            }
        }
        public bool SYNCWIFI
        {
            get => _SyncWIFI;
            set
            {
                SetProperty(ref _SyncWIFI, value);
            }
        }
        public bool SYNCONSTART
        {
            get => _SyncOnStart;
            set
            {
                SetProperty(ref _SyncOnStart, value);
            }
        }
        public bool SYNCONSAVE
        {
            get => _SyncOnSave;
            set
            {
                SetProperty(ref _SyncOnSave, value);
            }
        }
        public string INTERVALSYNC
        {
            get => _IntervalSync;
            set
            {
                SetProperty(ref _IntervalSync, value);
            }
        }
        public string LASTSYNC
        {
            get => _LastSync;
            set
            {
                SetProperty(ref _LastSync, value);
            }
        }
        #endregion

        public ConfigVM()
        {
            Title = "Configurações";
            _IConfigDB = DInjection.GetIntance<IConfigDB>();
            CmdSincAllClick = new Command(OnSincAllClick);
            CmdSaveClick = new Command(OnSaveClick);
            CmdClearDataClick = new Command(OnClearDataClick);
            _Validator = new ConfigValidator();
        }

        public async void OnLoad()
        {
            _ConfigModel = await _IConfigDB.GetItemAsync(1);
            if (_ConfigModel == null)
            {
                _ConfigModel = new ConfigModel
                {
                    Id = 1,
                    UniqueKey = Guid.NewGuid().ToString(),
                    LastSync = DateTime.Now,
                    EstablishmentKey = App.ESTABLISHMENTKEY,
                    UserKey = App.UserLogged.UserKey,
                    SyncOnSave = true,
                    SyncOnStart = true,
                    SyncWIFI = false,
                    IntervalSync = 30
                };
                await _IConfigDB.AddItemAsync(_ConfigModel);
            }
            else
            {
                ESTABLISHMENTKEY = _ConfigModel.EstablishmentKey;
                USERKEY = _ConfigModel.UserKey;
                SYNCWIFI = _ConfigModel.SyncWIFI;
                SYNCONSTART = _ConfigModel.SyncOnStart;
                SYNCONSAVE = _ConfigModel.SyncOnSave;
                INTERVALSYNC = _ConfigModel.IntervalSync.ToString();
                LASTSYNC = _ConfigModel.LastSync.ToString("dd/MM/yyyy HH:mm");
            }
        }

        #region Save Clicked
        private async void OnSaveClick()
        {
            if (IsBusy)
                return;

            _ConfigModel.Id = 1;
            _ConfigModel.UniqueKey = Guid.NewGuid().ToString();
            _ConfigModel.EstablishmentKey = App.ESTABLISHMENTKEY;
            _ConfigModel.UserKey = App.UserLogged.UserKey;
            int.TryParse(_IntervalSync, out int t);
            _ConfigModel.IntervalSync = t;
            _ConfigModel.SyncWIFI = _SyncWIFI;
            _ConfigModel.SyncOnStart = _SyncOnStart;
            _ConfigModel.SyncOnSave = _SyncOnSave;

            if (await ValidateModelAsync(_ConfigModel))
            {
                await _IConfigDB.UpdateItemAsync(_ConfigModel);
                ShowShortMessage(Resources.ValidationMessagesResource.SaveSuccess);
            }
            else
                DisplayValidationModelErrors(true);
        }
        #endregion

        private async void OnSincAllClick()
        {
            if (IsBusy)
                return;
            try
            {
                IsBusy = true;
                using (SyncData sync = new SyncData())
                {
                    if (sync.IsConnected() &&
                        await sync.ConnectionTypeOK())
                    {
                        var Config = await _IConfigDB.GetItemAsync(1);
                        Config.LastSync = new DateTime(1900, 1, 1, 1, 1, 1);
                        await _IConfigDB.UpdateItemAsync(Config);

                        await sync.Syncronize();
                        await sync.SendChanges();
                        IsBusy = false;
                    }
                    else
                        ShowShortMessage("Sem conexão");
                }
            }
            catch (Exception)
            {
            }
        }

        #region Clear Data Clicked
        private void OnClearDataClick()
        {
            if (IsBusy)
                return;

            OnMessageConfirmRaise(new MessageRaiseEvent(Resources.ValidationMessagesResource.ConfirmClearDatabase));
        }
        #endregion

        public async void ClearAllData()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            try
            {
                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadStarting);
                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadProcessing);



                var Config = await _IConfigDB.GetItemAsync(1);
                Config.LastSync = new DateTime(1900, 1, 1, 1, 1, 1);
                await _IConfigDB.UpdateItemAsync(Config);
                new DataBaseCleaner().ClearDataBase();
                App.USER_TOKEN = null;
                App.REFRESH_TOKEN = null;
                App.ESTABLISHMENTKEY = null;
                App.UserLogged = null;
                var navpage = new NavigationPage(new Views.Auth.LoginPage());
                App.Current.MainPage = navpage;
                IsBusy = false;

                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);
            }
            catch (Exception)
            {
                IsBusy = false;
                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);
            }
            finally
            {
                IsBusy = false;
                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);
            }

        }
    }
}
