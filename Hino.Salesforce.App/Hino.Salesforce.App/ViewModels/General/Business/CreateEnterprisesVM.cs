﻿using Hino.Salesforce.App.Models;
using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Models.General.Demograph;
using Hino.Salesforce.App.Models.Validation.General.Business;
using Hino.Salesforce.App.Services.API;
using Hino.Salesforce.App.Services.DB.Interfaces;
using Hino.Salesforce.App.Services.DB.Interfaces.General.Business;
using Hino.Salesforce.App.Services.SyncData;
using Hino.Salesforce.App.Utils.Extensions;
using Hino.Salesforce.App.Views.General.Business.Enterprises;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Hino.Salesforce.App.ViewModels.General.Business
{
    public class CreateEnterprisesVM : BaseViewModel
    {
        public ICommand CmdCreateClick { protected set; get; }

        private bool _EditLoading = false;
        private GEEnterprisesModel _GEEnterprisesModelEdit;
        private readonly GEEnterprisesModel _GEEnterprisesModel;
        private IGEEnterprisesDB _IGEEnterprisesDB;
        // VOLTAR NO FUTURO
        // private IGECountriesDB _IGECountriesDB;
        // private IGEStatesDB _IGEStatesDB;
        // private IGECitiesDB _IGECitiesDB;
        private IConfigDB _IConfigDB;

        public string LBLRAZAOSOCIAL { get => Resources.FieldsNameResource.RazaoSocial; }
        public string LBLNOMEFANTASIA { get => Resources.FieldsNameResource.NomeFantasia; }
        public string LBLCNPJCPF { get => Resources.FieldsNameResource.CNPJCPF; }
        public string LBLTYPE { get => Resources.FieldsNameResource.TypeEnterprise; }
        public string LBLCELLPHONE { get => Resources.FieldsNameResource.CellPhone; }
        public string LBLPHONE { get => Resources.FieldsNameResource.Phone; }
        public string LBLEMAIL { get => Resources.FieldsNameResource.Email; }
        public string LBLADDRESS { get => Resources.FieldsNameResource.Address; }
        public string LBLCOMPLEMENT { get => Resources.FieldsNameResource.Complement; }
        public string LBLDISTRICT { get => Resources.FieldsNameResource.District; }
        public string LBLNUM { get => Resources.FieldsNameResource.Num; }
        public string LBLZIPCODE { get => Resources.FieldsNameResource.ZipCode; }
        public string LBLSITE { get => Resources.FieldsNameResource.Site; }
        public string LBLUF { get => Resources.FieldsNameResource.UF; }
        public string LBLCOUNTRYID { get => Resources.FieldsNameResource.NameCountry; }
        public string LBLSTATEID { get => Resources.FieldsNameResource.NameState; }
        public string LBLCITYID { get => Resources.FieldsNameResource.NameCity; }

        List<EnterpriseType> _TypeEnterprise;
        public List<EnterpriseType> TYPEENTERPRISE
        {
            get { return _TypeEnterprise; }
            set
            {
                SetProperty(ref _TypeEnterprise, value);
            }
        }

        /* VOLTAR NO FUTURO
        ObservableCollection<GECountriesModel> _lstCountries;
        public ObservableCollection<GECountriesModel> LSTCOUNTRIES
        {
            get { return _lstCountries; }
            set
            {
                SetProperty(ref _lstCountries, value);
            }
        }
        ObservableCollection<GEStatesModel> _lstStates;
        public ObservableCollection<GEStatesModel> LSTSTATES
        {
            get { return _lstStates; }
            set
            {
                SetProperty(ref _lstStates, value);
            }
        }
        ObservableCollection<GECitiesModel> _lstCities;
        public ObservableCollection<GECitiesModel> LSTCITIES
        {
            get { return _lstCities; }
            set
            {
                SetProperty(ref _lstCities, value);
            }
        }*/

        #region Properties
        /* VOLTAR NO FUTURO
        GECountriesModel _SelectedCountry;
        public GECountriesModel SELECTEDCOUNTRY
        {
            get
            {
                return _SelectedCountry;
            }
            set
            {
                SetProperty(ref _SelectedCountry, value);
                if (!_EditLoading)
                {
                    COUNTRYID = (_SelectedCountry?.Id ?? null).ToString();
                    SELECTEDSTATE = null;
                    STATEID = null;
                    SELECTEDCITY = null;
                    CITYID = null;
                }
            }
        }

        GEStatesModel _SelectedState;
        public GEStatesModel SELECTEDSTATE
        {
            get
            {
                return _SelectedState;
            }
            set
            {
                SetProperty(ref _SelectedState, value);
                if (!_EditLoading)
                {
                    STATEID = (_SelectedState?.Id ?? null).ToString();
                    SELECTEDCITY = null;
                    CITYID = null;
                }
            }
        }

        GECitiesModel _SelectedCity;
        public GECitiesModel SELECTEDCITY
        {
            get
            {
                return _SelectedCity;
            }
            set
            {
                SetProperty(ref _SelectedCity, value);
                if (!_EditLoading)
                    CITYID = (_SelectedCity?.Id ?? null).ToString();
            }
        }
        */

        EnterpriseType _SelectedEnterpriseType;
        public EnterpriseType SELECTEDENTERPRISETYPE
        {
            get
            {
                return _SelectedEnterpriseType;
            }
            set
            {
                SetProperty(ref _SelectedEnterpriseType, value);
                if (!_EditLoading)
                {
                    TYPE = _SelectedEnterpriseType?.Index ?? "";
                    CNPJCPF = "";
                }
            }
        }

        private string _RazaoSocial;
        private string _NomeFantasia;
        private string _CNPJCPF;
        private string _Type;
        private string _CellPhone;
        private string _Phone;
        private string _Email;
        private string _Address;
        private string _Complement;
        private string _District;
        private string _Num;
        private string _ZipCode;
        private string _Site;
        private string _CountryID;
        private string _StateID;
        private string _CityID;
        private string _Uf;
        public string RAZAOSOCIAL
        {
            get
            {
                return _RazaoSocial;
            }
            set
            {
                SetProperty(ref _RazaoSocial, value);
            }
        }

        public string NOMEFANTASIA
        {
            get
            {
                return _NomeFantasia;
            }
            set
            {
                SetProperty(ref _NomeFantasia, value);
            }
        }

        public string CNPJCPF
        {
            get
            {
                return _CNPJCPF;
            }
            set
            {
                SetProperty(ref _CNPJCPF, value);
            }
        }

        public string TYPE
        {
            get
            {
                return _Type;
            }
            set
            {
                SetProperty(ref _Type, value);
            }
        }

        public string CELLPHONE
        {
            get
            {
                return _CellPhone;
            }
            set
            {
                SetProperty(ref _CellPhone, value);
            }
        }

        public string PHONE
        {
            get
            {
                return _Phone;
            }
            set
            {
                SetProperty(ref _Phone, value);
            }
        }

        public string EMAIL
        {
            get
            {
                return _Email;
            }
            set
            {
                SetProperty(ref _Email, value);
            }
        }

        public string ADDRESS
        {
            get
            {
                return _Address;
            }
            set
            {
                SetProperty(ref _Address, value);
            }
        }

        public string COMPLEMENT
        {
            get
            {
                return _Complement;
            }
            set
            {
                SetProperty(ref _Complement, value);
            }
        }

        public string DISTRICT
        {
            get
            {
                return _District;
            }
            set
            {
                SetProperty(ref _District, value);
            }
        }

        public string NUM
        {
            get
            {
                return _Num;
            }
            set
            {
                SetProperty(ref _Num, value);
            }
        }

        public string ZIPCODE
        {
            get
            {
                return _ZipCode;
            }
            set
            {
                SetProperty(ref _ZipCode, value);
            }
        }

        public string SITE
        {
            get
            {
                return _Site;
            }
            set
            {
                SetProperty(ref _Site, value);
            }
        }

        public string COUNTRYID
        {
            get
            {
                return _CountryID;
            }
            set
            {
                SetProperty(ref _CountryID, value);
            }
        }

        public string STATEID
        {
            get
            {
                return _StateID;
            }
            set
            {
                SetProperty(ref _StateID, value);
            }
        }

        public string CITYID
        {
            get
            {
                return _CityID;
            }
            set
            {
                SetProperty(ref _CityID, value);
            }
        }

        public string UF
        {
            get
            {
                return _Uf;
            }
            set
            {
                SetProperty(ref _Uf, value);
            }
        }

        #endregion

        #region Initialize
        private void Initialize()
        {
            _GEEnterprisesModelEdit = null;
            _IConfigDB = DInjection.GetIntance<IConfigDB>();
            _IGEEnterprisesDB = DInjection.GetIntance<IGEEnterprisesDB>();
            // VOLTAR NO FUTURO
            // _IGECountriesDB = DInjection.GetIntance<IGECountriesDB>();
            // _IGEStatesDB = DInjection.GetIntance<IGEStatesDB>();
            // _IGECitiesDB = DInjection.GetIntance<IGECitiesDB>();
            _Validator = new CreateEnterpriseValidator();
            CmdCreateClick = new Command(OnCreateClick);

            // LSTCOUNTRIES = new ObservableCollection<GECountriesModel>();
            // LSTSTATES = new ObservableCollection<GEStatesModel>();
            // LSTCITIES = new ObservableCollection<GECitiesModel>();


            TYPEENTERPRISE = new List<EnterpriseType>();
            TYPEENTERPRISE.Add(new EnterpriseType("Pessoa Física", "0"));
            TYPEENTERPRISE.Add(new EnterpriseType("Pessoa Jurídica", "1"));

            MessagingCenter.Subscribe<MainEnterprisesPage, GEEnterprisesModel>(this, "LoadRegister", (obj, model) =>
            {
                if (IsBusy)
                    return;

                IsBusy = true;
                _EditLoading = true;

                _GEEnterprisesModelEdit = model;
                RAZAOSOCIAL = model.RazaoSocial;
                NOMEFANTASIA = model.NomeFantasia;
                SELECTEDENTERPRISETYPE = TYPE == "0" ? TYPEENTERPRISE[0] : TYPEENTERPRISE[1];
                TYPE = model.Type.ToString();
                CNPJCPF = model.CNPJCPF;
                CELLPHONE = model.CellPhone;
                PHONE = model.Phone;
                EMAIL = model.Email;
                ADDRESS = model.Address;
                COMPLEMENT = model.Complement;
                DISTRICT = model.District;
                NUM = model.Num;
                ZIPCODE = model.ZipCode;
                SITE = model.Site;
                UF = model.UF;
                IsBusy = false;
                /*
                await LoadData();

                var country = LSTCOUNTRIES.Where(r => r.Id == model.CountryID).FirstOrDefault();
                if (country != null)
                {
                    SELECTEDCOUNTRY = country;
                    COUNTRYID = country.Id.ToString();

                }

                await LoadStates();

                var state = LSTSTATES.Where(r => r.Id == model.StateID).FirstOrDefault();
                if (state != null)
                {
                    SELECTEDSTATE = state;
                    STATEID = state.Id.ToString();
                }

                await LoadCity();

                var city = LSTCITIES.Where(r => r.Id == model.CityID).FirstOrDefault();
                if (city != null)
                {
                    SELECTEDCITY = city;
                    CITYID = city.Id.ToString();
                }*/
                _EditLoading = false;
            });
        }

        public CreateEnterprisesVM(GEEnterprisesModel pGEEnterprisesModel)
        {
            Title = "Alterar Cliente";
            _GEEnterprisesModel = pGEEnterprisesModel;
            Initialize();
        }

        public CreateEnterprisesVM()
        {
            Title = "Novo Cliente";
            _GEEnterprisesModel = new GEEnterprisesModel();
            Initialize();
        }
        #endregion

        #region Clear Instance
        public void ClearIntance()
        {
            MessagingCenter.Unsubscribe<MainEnterprisesPage, GECountriesModel>(this, "LoadRegister");
        }
        #endregion

        /* VOLTAR NO FUTURO
        #region Load Data
        public async Task LoadData()
        {
            if (IsBusy)
                return;
            IsBusy = true;
            var countries = (await _IGECountriesDB.GetItemsAsync(-1)).Results.OrderBy(r => r.Name);

            foreach (var item in countries)
                LSTCOUNTRIES.Add(item);

            var brasil = LSTCOUNTRIES.Where(r => r.Name.Trim().ToUpper() == "BRASIL").FirstOrDefault();

            if (brasil != null)
            {
                SELECTEDCOUNTRY = brasil;
                COUNTRYID = brasil.Id.ToString();
            }

            IsBusy = false;
        }
        #endregion
        
        #region Load States
        public async Task LoadStates()
        {
            SELECTEDSTATE = null;
            STATEID = null;

            SELECTEDCITY = null;
            CITYID = null;

            LSTSTATES.Clear();

            if (SELECTEDCOUNTRY != null && SELECTEDCOUNTRY.Id > 0)
            {
                
                IsBusy = true;
                var states = (await _IGEStatesDB.Query(r => r.CountryID == SELECTEDCOUNTRY.Id, -1)).Results.OrderBy(r => r.Name);

                foreach (var item in states)
                    LSTSTATES.Add(item);

                IsBusy = false;
            }
        }
        #endregion

        #region Load City
        public async Task LoadCity()
        {

            SELECTEDCITY = null;
            CITYID = null;

            LSTCITIES.Clear();

            if (SELECTEDSTATE != null && SELECTEDSTATE.Id > 0)
            {
                IsBusy = true;
                var cities = (await _IGECitiesDB.Query(r => r.StateID == SELECTEDSTATE.Id, -1)).Results.OrderBy(r => r.Name);

                foreach (var item in cities)
                    LSTCITIES.Add(item);

                IsBusy = false;
            }
        }
        #endregion
        */

        #region Get CEP info
        public async Task<bool> GetCEPInfo()
        {
            if (IsBusy)
                return false;

            if (string.IsNullOrEmpty(ZIPCODE) ||
                string.IsNullOrWhiteSpace(ZIPCODE))
                return false;

            IsBusy = true;
            base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadStarting);
            base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadProcessing);

            ADDRESS = "";
            DISTRICT = "";
            COMPLEMENT = "";
            UF = "";

            try
            {
                var regex = new Regex(@"\d+");
                var zipCode = "";

                var results = regex.Matches(ZIPCODE);
                foreach (Match item in results)
                    zipCode += item.Value;

                var url = $"http://viacep.com.br/ws/{zipCode}/json/";
                var _Request = new Request();
                var resultCep = await _Request.GetFullURLAsync<CEPModel>(url, false);


                if (resultCep != null)
                {
                    ADDRESS = resultCep.logradouro;
                    DISTRICT = resultCep.bairro;
                    COMPLEMENT = resultCep.complemento;
                    UF = resultCep.uf;
                }
                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);
                IsBusy = false;

                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);
                ShowShortMessage(Resources.ValidationMessagesResource.LoadComplete);

            }
            catch (Exception)
            {
                IsBusy = false;
                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);
                ShowShortMessage(Resources.ErrorMessagesResource.CEPNotFound);
                return false;
            }

            return true;
        }
        #endregion

        #region On Create Click
        public async void OnCreateClick()
        {
            if (IsBusy)
                return;

            _GEEnterprisesModel.CopyProperties(this);

            if (await ValidateModelAsync(_GEEnterprisesModel))
            {
                IsBusy = true;
                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadStarting);
                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadProcessing);

                try
                {
                    _GEEnterprisesModel.EstablishmentKey = App.ESTABLISHMENTKEY;
                    _GEEnterprisesModel.UniqueKey = _GEEnterprisesModelEdit != null ? _GEEnterprisesModelEdit.UniqueKey : Guid.NewGuid().ToString();
                    _GEEnterprisesModel.Id = _GEEnterprisesModelEdit != null ? _GEEnterprisesModelEdit.Id : await _IGEEnterprisesDB.NextSequence();
                    _GEEnterprisesModel.Initials = _GEEnterprisesModelEdit != null ? _GEEnterprisesModelEdit.Initials : "";
                    _GEEnterprisesModel.Status = 0;
                    _GEEnterprisesModel.StatusSinc = 0;
                    _GEEnterprisesModel.IsActive = true;
                    _GEEnterprisesModel.Created = DateTime.Now;
                    var EmpBase = (await _IGEEnterprisesDB.Query(r => r.Id > 0, 1)).Results.FirstOrDefault();
                    _GEEnterprisesModel.RegionId = EmpBase.RegionId;
                    _GEEnterprisesModel.UserId = App.UserLogged.Id;
                    _GEEnterprisesModel.Modified = DateTime.Now;
                    if (_GEEnterprisesModelEdit == null)
                        await _IGEEnterprisesDB.AddItemAsync(_GEEnterprisesModel);
                    else
                        await _IGEEnterprisesDB.UpdateItemAsync(_GEEnterprisesModel);

                    var Config = await _IConfigDB.GetItemAsync(1);

                    if (Config.SyncOnSave)
                    {
                        using (SyncData sync = new SyncData())
                        {
                            if (sync.IsConnected() &&
                                await sync.ConnectionTypeOK())
                                await sync.SendChanges();
                        }
                    }

                    IsBusy = false;

                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);

                    ShowShortMessage(Resources.ValidationMessagesResource.SaveSuccess);

                    MessagingCenter.Send(this, "ChangePage", 0);
                }
                catch (Exception)
                {
                    IsBusy = false;
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);
                    ShowShortMessage(Resources.ErrorMessagesResource.ComunicationFail);
                }
                finally
                {
                    IsBusy = false;
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);
                }
            }
            else
            {
                DisplayValidationModelErrors(true);
            }
        }
        #endregion
    }
}
