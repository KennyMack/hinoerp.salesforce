﻿using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Services.API.Interfaces.General.Business;
using Hino.Salesforce.App.Services.DB.Interfaces.General.Business;
using Hino.Salesforce.App.Utils.Messages;
using Hino.Salesforce.App.Utils.Paging;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Hino.Salesforce.App.ViewModels.General.Business
{
    public class EnterprisesVM : BaseViewModel
    {
        private readonly IGEEnterprisesDB _IGEEnterprisesDB;
        private readonly IGEEnterprisesAPI _IGEEnterprisesAPI;
        public GEEnterprisesModel SelectedItem;
        public ObservableCollection<GEEnterprisesModel> Items { get; set; }

        public EnterprisesVM()
        {
            Title = "Clientes";
            Items = new ObservableCollection<GEEnterprisesModel>();

            _IGEEnterprisesDB = DInjection.GetIntance<IGEEnterprisesDB>();
            _IGEEnterprisesAPI = DInjection.GetIntance<IGEEnterprisesAPI>();
            base.PageNum = 1;
        }

        #region Convert Search Data to number
        private int ConvertSearchToNumber(string pSearch)
        {
            if (int.TryParse(pSearch, out int ret))
                return ret;

            return 0;
        }
        #endregion

        #region On Create Click
        public override void OnFABButtonTapped(object sender, EventArgs e) =>
            MessagingCenter.Send(this, "ChangePage", 1);
        #endregion

        #region Get Data From Source
        private async Task<PagedResult<GEEnterprisesModel>> GetDataFromSourceAsync()
        {
            return await _IGEEnterprisesDB.Search(_SearchData, base.PageNum);
            /*if (!string.IsNullOrEmpty(_SearchData))
            {
                var num = ConvertSearchToNumber(_SearchData);
                return await _IGEEnterprisesDB.Query(r =>
                    r.RazaoSocial.ToLower().Contains(_SearchData.ToLower()) ||
                    r.Id == num ||
                    r.CNPJCPF.ToLower().Contains(_SearchData.ToLower()) ||
                    r.Email.ToLower().Contains(_SearchData.ToLower()) ||
                    r.NomeFantasia.ToLower().Contains(_SearchData.ToLower()), base.PageNum);
            }
            else
                return await _IGEEnterprisesDB.GetItemsAsync(base.PageNum);*/
        }
        #endregion

        #region Search Data
        public async override Task SearchData_Changed()
        {
            base.PageNum = 1;

            Items.CopyFromPagedResult(await GetDataFromSourceAsync());
        }
        #endregion

        protected async override Task LoadMoreData()
        {
            IsBusy = true;
            var Title = base.Title;
            base.Title = "Carregando";

            try
            {
                Items.CopyFromPagedResult(await GetDataFromSourceAsync(), false);
                IsBusy = false;
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);
                base.Title = Title;

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected async override void RefreshData()
        {
            base.RefreshData();

            IsBusy = true;

            base.PageNum = 1;
            Items.Clear();

            await LoadMoreData();
            IsBusy = false;
        }

        #region Delete Click
        public void DeleteClick()
        {
            if (SelectedItem != null)
            {
                if (SelectedItem.StatusSinc == 0)
                {
                    OnMessageConfirmRaise(new MessageRaiseEvent(Resources.ValidationMessagesResource.ConfirmRemove));
                }
                else
                    ShowShortMessage(Resources.ErrorMessagesResource.EnterpriseWasSyncronized);
            }
            else
                ShowShortMessage(Resources.ValidationMessagesResource.EnterpriseNotSelected);
        }
        #endregion

        #region Delete Enterprise
        public async Task DeleteEnterprise()
        {
            if (await _IGEEnterprisesDB.DeleteItemAsync(SelectedItem.Id))
            {
                Items.Remove(SelectedItem);
                ShowShortMessage(Resources.ValidationMessagesResource.RemoveSuccess);
            }
        }
        #endregion

        #region On Edit Click
        public void EditEnterprise(GEEnterprisesModel pModel)
        {
            if (pModel.StatusSinc == 0)
                MessagingCenter.Send(this, "ChangePage", pModel);
            else
                ShowShortMessage(Resources.ErrorMessagesResource.EnterpriseWasSyncronized);
        }
        #endregion
    }
}
