﻿using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Services.DB.Interfaces.General.Business;
using System;
using System.Collections.ObjectModel;

namespace Hino.Salesforce.App.ViewModels.General.Business
{
    public class PaymentConditionVM : BaseViewModel
    {
        private readonly IGEPaymentConditionDB _IGEPaymentConditionDB;
        public ObservableCollection<GEPaymentConditionModel> Items { get; set; }

        public PaymentConditionVM()
        {
            Items = new ObservableCollection<GEPaymentConditionModel>();
            _IGEPaymentConditionDB = DInjection.GetIntance<IGEPaymentConditionDB>();
        }

        protected async override void RefreshData()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.CopyFromPagedResult(await _IGEPaymentConditionDB.GetItemsAsync(1));
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);
            }
            finally
            {
                IsBusy = false;
            }

        }
    }
}
