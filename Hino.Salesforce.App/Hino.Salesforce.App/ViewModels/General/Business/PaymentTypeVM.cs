﻿using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Services.DB.Interfaces.General.Business;
using System;
using System.Collections.ObjectModel;

namespace Hino.Salesforce.App.ViewModels.General.Business
{
    public class PaymentTypeVM : BaseViewModel
    {
        private readonly IGEPaymentTypeDB _IGEPaymentTypeDB;
        public ObservableCollection<GEPaymentTypeModel> Items { get; set; }

        public PaymentTypeVM()
        {
            Items = new ObservableCollection<GEPaymentTypeModel>();
            _IGEPaymentTypeDB = DInjection.GetIntance<IGEPaymentTypeDB>();
        }

        protected async override void RefreshData()
        {
            if (IsBusy)
                return;

            IsBusy = true;

            try
            {
                Items.CopyFromPagedResult(await _IGEPaymentTypeDB.GetItemsAsync(1));
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);
            }
            finally
            {
                IsBusy = false;
            }

        }
    }
}
