﻿using Hino.Salesforce.App.Models.General;
using Hino.Salesforce.App.Services.DB.Interfaces.General;
using Hino.Salesforce.App.Services.DB.Interfaces.Sales;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.ViewModels.General
{
    public class EditProductsVM : BaseViewModel
    {
        private readonly IGEProductsDB _IGEProductsDB;
        private readonly IVESalePriceDB _IVESalePriceDB;

        #region Labels
        public string LBLNAME { get => Resources.FieldsNameResource.Name; }
        public string LBLDESCRIPTION { get => Resources.FieldsNameResource.DescriptionProd; }
        public string LBLPRODUCTKEY { get => Resources.FieldsNameResource.ProductKey; }
        public string LBLTYPE { get => Resources.FieldsNameResource.Type; }
        public string LBLFAMILY { get => Resources.FieldsNameResource.Family; }
        public string LBLNCM { get => Resources.FieldsNameResource.NCM; }
        #endregion

        #region Properties
        private readonly GEProductsModel _GEProductsModel;
        public bool DISPLAYIMAGE
        {
            get => !string.IsNullOrEmpty(_GEProductsModel.Image);
        }
        public string IMAGE
        {
            get => _GEProductsModel.Image;
        }
        public string DISPLAYNAME
        {
            get => _GEProductsModel.DisplayName;
        }
        public string TYPE
        {
            get => _GEProductsModel.Type;
        }
        public string FAMILY
        {
            get => _GEProductsModel.Family;
        }
        public decimal PERCMAXDISCOUNT
        {
            get => _GEProductsModel.PercMaxDiscount;
        }
        public decimal PERCIPI
        {
            get => _GEProductsModel.PercIPI;
        }
        public decimal STOCKBALANCE
        {
            get => _GEProductsModel.StockBalance;
        }
        public string DESCRIPTION
        {
            get => _GEProductsModel.Description;
        }
        public string NCM
        {
            get => _GEProductsModel.NCM;
        }
        private string _value;
        public string VALUE
        {
            get => _value;
            set => SetProperty(ref _value, value);
        }
        #endregion

        public EditProductsVM(GEProductsModel pModel)
        {
            _GEProductsModel = pModel;
            Title = pModel.ProductKey;
            _IGEProductsDB = DInjection.GetIntance<IGEProductsDB>();
            _IVESalePriceDB = DInjection.GetIntance<IVESalePriceDB>();
        }

        public async Task LoadValues()
        {
            var values = await _IVESalePriceDB.GetValuesByProductId(_GEProductsModel.Id);

            if (values.Count > 0)
            {
                StringBuilder sbValues = new StringBuilder();

                var strVal = "";
                foreach (var item in values)
                {
                    strVal = item.Value.ToString("n2");
                    sbValues.Append($"{strVal}");
                }
                VALUE = sbValues.ToString();
            }
            else
                VALUE = "0";
        }
    }
}
