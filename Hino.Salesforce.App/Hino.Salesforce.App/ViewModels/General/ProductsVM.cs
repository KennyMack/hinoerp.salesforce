﻿using Hino.Salesforce.App.Models.General;
using Hino.Salesforce.App.Services.API.Interfaces.General;
using Hino.Salesforce.App.Services.DB.Interfaces.General;
using Hino.Salesforce.App.Services.DB.Interfaces.Sales;
using Hino.Salesforce.App.Utils.Paging;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace Hino.Salesforce.App.ViewModels.General
{
    public class ProductsVM : BaseViewModel
    {
        private readonly IGEProductsDB _IGEProductsDB;
        private readonly IVESalePriceDB _IVESalePriceDB;
        private readonly IVERegionSaleDB _IVERegionSaleDB;
        private readonly IGEProductsAPI _IGEProductsAPI;
        public ObservableCollection<GEProductsModel> Items { get; set; }

        public ProductsVM()
        {
            Title = "Produtos";
            Items = new ObservableCollection<GEProductsModel>();

            _IVESalePriceDB = DInjection.GetIntance<IVESalePriceDB>();
            _IGEProductsDB = DInjection.GetIntance<IGEProductsDB>();
            _IVERegionSaleDB = DInjection.GetIntance<IVERegionSaleDB>();
            _IGEProductsAPI = DInjection.GetIntance<IGEProductsAPI>();

            base.PageNum = 1;
        }

        #region Get Data From Source
        private async Task<PagedResult<GEProductsModel>> GetDataFromSourceAsync()
        {
            return await _IGEProductsDB.Search(_SearchData, base.PageNum);
            /*if (!string.IsNullOrEmpty(_SearchData))
                return await _IGEProductsDB.Query(r => r.Name.ToLower().Contains(_SearchData) ||
                                r.ProductKey.ToLower().Contains(_SearchData), base.PageNum);
            else
                return await _IGEProductsDB.GetItemsAsync(base.PageNum);*/
        }
        #endregion

        #region Search Data
        public async override Task SearchData_Changed()
        {
            base.PageNum = 1;

            Items.CopyFromPagedResult(await GetDataFromSourceAsync());
        }
        #endregion

        protected async override Task LoadMoreData()
        {
            IsBusy = true;
            var Title = base.Title;
            base.Title = "Carregando";

            try
            {
                Items.CopyFromPagedResult(await GetDataFromSourceAsync(), false);
                IsBusy = false;
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);
                base.Title = Title;

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected async override void RefreshData()
        {
            base.RefreshData();

            IsBusy = true;

            Items.Clear();

            await LoadMoreData();
        }

    }
}
