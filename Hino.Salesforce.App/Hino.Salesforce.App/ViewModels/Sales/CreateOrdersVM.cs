﻿using Hino.Salesforce.App.Models.General;
using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.Models.Validation.Sales;
using Hino.Salesforce.App.Services.DB.Interfaces;
using Hino.Salesforce.App.Services.DB.Interfaces.General;
using Hino.Salesforce.App.Services.DB.Interfaces.General.Business;
using Hino.Salesforce.App.Services.DB.Interfaces.Sales;
using Hino.Salesforce.App.Services.Search.General.Business;
using Hino.Salesforce.App.Services.SyncData;
using Hino.Salesforce.App.Utils.Extensions;
using Hino.Salesforce.App.Utils.Messages;
using Hino.Salesforce.App.Views.Main;
using Hino.Salesforce.App.Views.Sales;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Hino.Salesforce.App.ViewModels.Sales
{
    public class CreateOrdersVM : BaseViewModel
    {
        private bool IsModal = false;
        private VEOrdersModel _VEOrdersModelEdit;
        public VEOrderItemsModel SelectedItem;

        private GEPaymentConditionModel _GEPaymentConditionModel;
        private GEPaymentConditionSearch _GEPaymentConditionSearch;

        private GEEnterprisesModel _GEEnterprisesModel;
        private GEEnterprisesSearch _GEEnterprisesSearch;

        private GEPaymentTypeModel _GEPaymentTypeModel;
        private GEPaymentTypeSearch _GEPaymentTypeSearch;

        public bool CanChangePayment { get; private set; }
        public IGEProductsDB _IGEProductsDB;
        public IGEEnterprisesDB _IGEEnterprisesDB;
        public IGEPaymentConditionDB _IGEPaymentConditionDB;
        public IGEPaymentTypeDB _IGEPaymentTypeDB;
        private IConfigDB _IConfigDB;
        public ObservableCollection<VEOrderItemsModel> Items { get; set; }
        public ICommand CmdCreateClick { protected set; get; }

        public ICommand CmdAddClick { protected set; get; }
        public ICommand CmdRemoveClick { protected set; get; }
        public ICommand CmdClearItemsClick { protected set; get; }

        public string LBLENTERPRISEID { get => Resources.FieldsNameResource.RazaoSocial; }
        public string LBLTYPEPAYMENTID { get => Resources.FieldsNameResource.PaymentType; }
        public string LBLPAYCONDITIONID { get => Resources.FieldsNameResource.PaymentCondition; }
        public string LBLDELIVERYDATE { get => Resources.FieldsNameResource.DeliveryDate; }
        public string LBLNOTE { get => Resources.FieldsNameResource.Note; }
        public string LBLISPROPOSAL { get => Resources.FieldsNameResource.IsProposal; }
        public string LBLORDERITEMS { get => Resources.FieldsNameResource.OrderItems; }

        #region Properties
        public string _Enterprise;
        public string _TypePayment;
        public string _PayCondition;

        public string _EnterpriseID;
        public string _UserID;
        public string _TypePaymentID;
        public string _PayConditionID;
        public string _CodPedVenda;
        public string _NumPedMob;
        public DateTime _DeliveryDate;
        public string _Note;
        public string _Status;
        public string _StatusSinc;
        public string _IsProposal;

        public decimal TOTALVALUE
        {
            get
            {
                if (Items.Count > 0)
                {
                    var retval = Items.Sum(r => r.Quantity * r.Value) +
                        Items.Sum(r => ((r.Quantity * r.Value) * (r.GEProducts.PercIPI / 100)));
                    return retval;
                }
                return 0;
            }
        }
        public decimal TOTALDISCOUNT
        {
            get
            {
                var ItemsDiscount = Items.Where(r => r.TableValue != r.Value);
                if (ItemsDiscount.Count() > 0)
                    return ItemsDiscount.Sum(r => (r.Quantity * (r.TableValue - r.Value)));
                return 0;
            }
        }
        public decimal TOTALIPI
        {
            get
            {
                if (Items.Count > 0)
                    return Items.Sum(r => ((r.Quantity * r.Value) * (r.GEProducts.PercIPI / 100)));
                return 0;
            }
        }

        public int _ListHeight;
        public int LISTHEIGHT
        {
            get
            {
                return _ListHeight;
            }
            set
            {
                SetProperty(ref _ListHeight, value);
            }
        }

        public string PAYCONDITION
        {
            get
            {
                return _PayCondition;
            }
            set
            {
                SetProperty(ref _PayCondition, value);
            }
        }

        public string TYPEPAYMENT
        {
            get
            {
                return _TypePayment;
            }
            set
            {
                SetProperty(ref _TypePayment, value);
            }
        }

        public string ENTERPRISE
        {
            get
            {
                return _Enterprise;
            }
            set
            {
                SetProperty(ref _Enterprise, value);
            }
        }

        public string ENTERPRISEID
        {
            get
            {
                return _EnterpriseID;
            }
            set
            {
                SetProperty(ref _EnterpriseID, value);
            }
        }
        public string USERID
        {
            get
            {
                return _UserID;
            }
            set
            {
                SetProperty(ref _UserID, value);
            }
        }
        public string TYPEPAYMENTID
        {
            get
            {
                return _TypePaymentID;
            }
            set
            {
                SetProperty(ref _TypePaymentID, value);
            }
        }
        public string PAYCONDITIONID
        {
            get
            {
                return _PayConditionID;
            }
            set
            {
                SetProperty(ref _PayConditionID, value);
            }
        }
        public string CODPEDVENDA
        {
            get
            {
                return _CodPedVenda;
            }
            set
            {
                SetProperty(ref _CodPedVenda, value);
            }
        }
        public string NUMPEDMOB
        {
            get
            {
                return _NumPedMob;
            }
            set
            {
                SetProperty(ref _NumPedMob, value);
            }
        }
        public DateTime DELIVERYDATE
        {
            get
            {
                return _DeliveryDate;
            }
            set
            {
                /*if (DateTime.TryParseExact(value, "MM/dd/yyyy HH:mm:ss", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dt))
                    SetProperty(ref _DeliveryDate, dt.ToShortDateString());
                else*/
                SetProperty(ref _DeliveryDate, value);
            }
        }
        public string NOTE
        {
            get
            {
                return _Note;
            }
            set
            {
                SetProperty(ref _Note, value);
            }
        }
        public string STATUS
        {
            get
            {
                return _Status;
            }
            set
            {
                SetProperty(ref _Status, value);
            }
        }
        public string STATUSSINC
        {
            get
            {
                return _StatusSinc;
            }
            set
            {
                SetProperty(ref _StatusSinc, value);
            }
        }
        public string ISPROPOSAL
        {
            get
            {
                return _IsProposal;
            }
            set
            {
                SetProperty(ref _IsProposal, value);
            }
        }

        #endregion

        private VEOrdersModel _VEOrdersModel;
        private IVEOrdersDB _IVEOrdersDB;
        private IVEOrderItemsDB _IVEOrderItemsDB;

        #region Initialize
        private void Initialize()
        {
            _GEEnterprisesSearch = new GEEnterprisesSearch
            {
                KeyName = "Id",
                Description = "RazaoSocial",
                Details = "NomeFantasia",
                Title = "Empresas"
            };

            _GEPaymentTypeSearch = new GEPaymentTypeSearch
            {
                KeyName = "Id",
                Description = "Description",
                Details = "",
                Title = "Forma de pagamento"
            };

            _GEPaymentConditionSearch = new GEPaymentConditionSearch
            {
                KeyName = "Id",
                Description = "Description",
                Details = "",
                Title = "Cond. Pagto."
            };
            DELIVERYDATE = DateTime.Now;
            ISPROPOSAL = "1";

            // MessagingCenter.Send(this, "ChangeDeliveryDate", DateTime.Now);
            MessagingCenter.Send(this, "ChangeCheckBox", true);


            _IConfigDB = DInjection.GetIntance<IConfigDB>();
            _IGEProductsDB = DInjection.GetIntance<IGEProductsDB>();
            _IGEPaymentConditionDB = DInjection.GetIntance<IGEPaymentConditionDB>();
            _IGEPaymentTypeDB = DInjection.GetIntance<IGEPaymentTypeDB>();
            _IGEEnterprisesDB = DInjection.GetIntance<IGEEnterprisesDB>();
            _IVEOrdersDB = DInjection.GetIntance<IVEOrdersDB>();
            _IVEOrderItemsDB = DInjection.GetIntance<IVEOrderItemsDB>();
            _Validator = new CreateOrdersValidator();
            CmdCreateClick = new Command(OnCreateClick);
            CmdClearItemsClick = new Command(OnClearItemsClick);

            CmdAddClick = new Command(OnAddClick);
            CmdRemoveClick = new Command(OnRemoveClick);

            Items = new ObservableCollection<VEOrderItemsModel>();
            Items.CollectionChanged += Items_CollectionChanged;

            CanChangePayment = true;

            LISTHEIGHT = 0;
            MessagingCenter.Subscribe<SearchSinglePageVM, string>(this, "ModalClosed", (obj, item) =>
            {
                IsModal = false;
            });

            MessagingCenter.Subscribe<SearchSinglePageVM, SearchItemList>(this, "ItemSelected", async (obj, item) =>
            {
                if (item.Title == "Empresas")
                {
                    var ss = await _IGEEnterprisesDB.GetItemsAsync(-1);

                    _GEEnterprisesModel = await _IGEEnterprisesDB.GetItemAsync(Convert.ToInt64(item.Id));

                    ENTERPRISE = $"{_GEEnterprisesModel.Id} - {_GEEnterprisesModel.RazaoSocial.SubStrTitle()}";

                    if (_GEEnterprisesModel.PayConditionId != null && _GEEnterprisesModel.PayConditionId > 0)
                    {
                        _GEPaymentConditionModel = await _IGEPaymentConditionDB.GetItemAsync(
                            Convert.ToInt64(_GEEnterprisesModel.PayConditionId));

                        _GEPaymentTypeModel = await _IGEPaymentTypeDB.GetItemAsync(
                            _GEPaymentConditionModel.GEPaymentType.Id);

                        TYPEPAYMENT = $"{_GEPaymentTypeModel.Id} - {_GEPaymentTypeModel.Description.SubStrTitle()}";

                        PAYCONDITION = $"{_GEPaymentConditionModel.Id} - {_GEPaymentConditionModel.Description.SubStrTitle()}";

                        CanChangePayment = false;
                    }

                    IsModal = false;
                }

                if (item.Title == "Forma de pagamento")
                {
                    _GEPaymentTypeModel = await _IGEPaymentTypeDB.GetItemAsync(Convert.ToInt64(item.Id));

                    TYPEPAYMENT = $"{_GEPaymentTypeModel.Id} - {_GEPaymentTypeModel.Description.SubStrTitle()}";

                    PAYCONDITION = "";
                    IsModal = false;
                }

                if (item.Title == "Cond. Pagto.")
                {
                    _GEPaymentConditionModel = await _IGEPaymentConditionDB.GetItemAsync(Convert.ToInt64(item.Id));

                    _GEPaymentTypeModel = _GEPaymentConditionModel.GEPaymentType; // await _IGEPaymentTypeDB.GetItemAsync(_GEPaymentConditionModel.GEPaymentType.Id);

                    TYPEPAYMENT = $"{_GEPaymentTypeModel.Id} - {_GEPaymentTypeModel.Description.SubStrTitle()}";

                    PAYCONDITION = $"{_GEPaymentConditionModel.Id} - {_GEPaymentConditionModel.Description.SubStrTitle()}";
                    IsModal = false;
                }
            });

            MessagingCenter.Subscribe<ListItemsSalesVM, string>(this, "ItemsClosed", (obj, items) =>
            {
                OnPropertyChanged(nameof(TOTALVALUE));
                OnPropertyChanged(nameof(TOTALDISCOUNT));
                OnPropertyChanged(nameof(TOTALIPI));
                IsModal = false;
            });

            MessagingCenter.Subscribe<ListItemsSalesVM, List<GEProductsModel>>(this, "ItemsSalesSelected", (obj, items) =>
            {
                foreach (var item in items.Distinct())
                {
                    if (Items.Where(r => r.GEProducts.ProductKey == item.ProductKey).Count() > 0)
                        continue;
                    item.Name = item.Name.SubStrTitle();
                    Items.Add(new VEOrderItemsModel
                    {
                        GEProducts = item,
                        ProductID = item.Id,
                        PercDiscount = 0,
                        OrderID = 0,
                        TableValue = item.Value,
                        Value = item.Value,
                        Quantity = Convert.ToDecimal(item.QuantitySelected),
                        Id = 0
                    });
                }
                OnPropertyChanged(nameof(TOTALVALUE));
                OnPropertyChanged(nameof(TOTALDISCOUNT));
                OnPropertyChanged(nameof(TOTALIPI));
                IsModal = false;
            });

            MessagingCenter.Subscribe<EditOrderItemVM, string>(this, "EditItemCancel", (obj, items) =>
            {
                OnPropertyChanged(nameof(TOTALVALUE));
                OnPropertyChanged(nameof(TOTALDISCOUNT));
                OnPropertyChanged(nameof(TOTALIPI));
                IsModal = false;
            });

            MessagingCenter.Subscribe<EditOrderItemVM, VEOrderItemsModel>(this, "EditItemSaved", (obj, orderItem) =>
            {
                IsModal = false;
                var ProdItem = Items.Where(r => r.GEProducts.ProductKey == orderItem.GEProducts.ProductKey)
                .First();
                ProdItem.Quantity = orderItem.Quantity;
                ProdItem.Value = orderItem.Value;
                OnPropertyChanged(nameof(TOTALVALUE));
                OnPropertyChanged(nameof(TOTALDISCOUNT));
                OnPropertyChanged(nameof(TOTALIPI));
            });


        }

        private void Items_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            LISTHEIGHT = (Items.Count) * 82;

            OnPropertyChanged(nameof(TOTALVALUE));
            OnPropertyChanged(nameof(TOTALDISCOUNT));
            OnPropertyChanged(nameof(TOTALIPI));
        }

        public CreateOrdersVM(VEOrdersModel pVEOrdersModel)
        {
            Title = "Alterar Pedido";
            _VEOrdersModel = pVEOrdersModel;
            Initialize();
        }

        public CreateOrdersVM()
        {
            Title = "Novo Pedido";
            _VEOrdersModel = new VEOrdersModel();
            Initialize();
        }
        #endregion

        #region Clear Instance
        public void ClearInstance()
        {
            MessagingCenter.Unsubscribe<SearchSinglePageVM, string>(this, "ModalClosed");
            MessagingCenter.Unsubscribe<SearchSinglePageVM, SearchItemList>(this, "ItemSelected");
            MessagingCenter.Unsubscribe<ListItemsSalesVM, string>(this, "ItemsClosed");
            MessagingCenter.Unsubscribe<ListItemsSalesVM, List<GEProductsModel>>(this, "ItemsSalesSelected");
            MessagingCenter.Unsubscribe<EditOrderItemVM, string>(this, "EditItemCancel");
            MessagingCenter.Unsubscribe<EditOrderItemVM, VEOrderItemsModel>(this, "EditItemSaved");
            MessagingCenter.Unsubscribe<MainSalesPage, VEOrdersModel>(this, "LoadRegister");
        }
        #endregion

        #region Remove item
        public void RemoveItem()
        {
            Items.Remove(SelectedItem);
            OnPropertyChanged(nameof(TOTALVALUE));
            OnPropertyChanged(nameof(TOTALDISCOUNT));
            OnPropertyChanged(nameof(TOTALIPI));
        }
        #endregion

        #region On Remove Click
        private void OnRemoveClick()
        {
            if (SelectedItem != null)
            {
                OnMessageConfirmRaise(new MessageRaiseEvent(Resources.ValidationMessagesResource.ConfirmRemove));
            }
            else
                ShowShortMessage(Resources.ValidationMessagesResource.OrderItemNotSelected);
        }
        #endregion

        #region On Add Click
        private async void OnAddClick()
        {
            if (IsModal)
                return;

            if (ENTERPRISE == null)
            {
                ShowShortMessage(Resources.ErrorMessagesResource.EnterpriseNotInformed);
                return;
            }
            IsModal = true;
            await PopupNavigation.Instance.PushAsync(
                new ListItemsSalesPage(_GEEnterprisesModel)
            );
        }
        #endregion

        #region On Create Click
        public async void OnCreateClick()
        {
            if (IsBusy)
                return;


            if (Items.Count <= 0)
            {
                ShowShortMessage(Resources.ErrorMessagesResource.OrderHasNoItems);
                return;
            }

            try
            {
                _VEOrdersModel.CopyProperties(this);
                _VEOrdersModel.EnterpriseID = _GEEnterprisesModel?.Id ?? 0;
                _VEOrdersModel.PayConditionID = _GEPaymentConditionModel?.Id ?? 0;
                _VEOrdersModel.TypePaymentID = _GEPaymentTypeModel?.Id ?? 0;
                _VEOrdersModel.UserID = App.UserLogged.Id;

            }
            catch (Exception)
            {

            }

            if (await ValidateEnterprise() && await ValidateModelAsync(_VEOrdersModel))
            {
                IsBusy = true;
                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadStarting);
                base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadProcessing);

                try
                {
                    _VEOrdersModel.EstablishmentKey = App.ESTABLISHMENTKEY;
                    _VEOrdersModel.UniqueKey = _VEOrdersModelEdit != null ? _VEOrdersModelEdit.UniqueKey : Guid.NewGuid().ToString();
                    _VEOrdersModel.Id = _VEOrdersModelEdit != null ? _VEOrdersModelEdit.Id : await _IVEOrdersDB.NextSequence();
                    _VEOrdersModel.Status = "P";
                    _VEOrdersModel.StatusSinc = 0;
                    _VEOrdersModel.IsActive = true;
                    _VEOrdersModel.Created = DateTime.Now;
                    _VEOrdersModel.Modified = DateTime.Now;
                    if (_VEOrdersModelEdit == null)
                        await _IVEOrdersDB.AddItemAsync(_VEOrdersModel);
                    else
                        await _IVEOrdersDB.UpdateItemAsync(_VEOrdersModel);

                    await _IVEOrderItemsDB.DeleteByOrderIdAsync(_VEOrdersModel.Id);

                    foreach (var item in Items)
                    {
                        item.EstablishmentKey = _VEOrdersModel.EstablishmentKey;
                        item.UniqueKey = Guid.NewGuid().ToString();
                        item.Id = await _IVEOrderItemsDB.NextSequence();
                        item.OrderID = _VEOrdersModel.Id;
                        item.IsActive = true;
                        item.Created = DateTime.Now;
                        item.Modified = DateTime.Now;
                        await _IVEOrderItemsDB.AddItemAsync(item);
                    }

                    if (!_VEOrdersModel.IsProposal)
                    {
                        var Config = await _IConfigDB.GetItemAsync(1);

                        if (Config.SyncOnSave)
                        {
                            using (SyncData _SyncData = new SyncData())
                            {
                                if (_SyncData.IsConnected() &&
                                   await _SyncData.ConnectionTypeOK())
                                {
                                    if (await _SyncData.RefreshTokenUser())
                                        await _SyncData.SendChanges();
                                }
                            }
                        }
                    }

                    IsBusy = false;
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);

                    ShowShortMessage(Resources.ValidationMessagesResource.SaveSuccess);


                    MessagingCenter.Send(this, "ChangePage", 0);
                }
                catch (Exception)
                {
                    IsBusy = false;
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);
                    ShowShortMessage(Resources.ErrorMessagesResource.ComunicationFail);
                }
                finally
                {
                    IsBusy = false;
                    base.SendLoadingMessage(Resources.ValidationMessagesResource.LoadComplete);
                }
            }
            else
            {
                DisplayValidationModelErrors(true);
            }
        }
        #endregion

        #region Show Search Form Enterprise
        public async Task ShowSearchFormEnterpriseIDAsync()
        {
            if (IsModal)
                return;

            if (Items.Count > 0)
            {
                ShowShortMessage(Resources.ErrorMessagesResource.OrderHasItems);
                return;
            }

            IsModal = true;
            await PopupNavigation.Instance.PushAsync(
                new SearchSinglePage(_GEEnterprisesSearch)
            );

        }
        #endregion

        #region Show Search Form TYPEPAYMENTID
        public async Task ShowSearchFormTYPEPAYMENTIDAsync()
        {
            if (IsModal)
                return;

            IsModal = true;
            await PopupNavigation.Instance.PushAsync(
                new SearchSinglePage(_GEPaymentTypeSearch)
            );
        }
        #endregion

        #region Show Search Form PAYCONDITIONID
        public async Task ShowSearchFormPAYCONDITIONIDAsync()
        {
            if (IsModal)
                return;

            IsModal = true;
            await PopupNavigation.Instance.PushAsync(
                new SearchSinglePage(_GEPaymentConditionSearch)
            );
        }
        #endregion

        #region Show Edit Item
        public async Task ShowEditItemAsync()
        {
            if (IsModal)
                return;

            if (SelectedItem == null)
                ShowShortMessage(Resources.ValidationMessagesResource.OrderItemNotSelected);

            IsModal = true;
            await PopupNavigation.Instance.PushAsync(
                new EditOrderItemPage(SelectedItem)
            );
        }
        #endregion

        #region Validate Enterprise
        async Task<bool> ValidateEnterprise()
        {
            if (_GEEnterprisesModel != null &&
                await _IGEEnterprisesDB.GetItemAsync(_GEEnterprisesModel.Id) != null)
                return true;
            else
            {
                _ValidationResult = new FluentValidation.Results.ValidationResult();
                _ValidationResult.Errors.Add(new FluentValidation.Results.ValidationFailure
                  ("EnterpriseId", string.Format(
                  Resources.ValidationMessagesResource.RequiredDefault,
                  Resources.FieldsNameResource.RazaoSocial)));
            }

            return false;
        }
        #endregion

        #region Clear All Items
        private void OnClearItemsClick()
        {
            Items.Clear();
        }
        #endregion

        public async Task LoadRegister(VEOrdersModel model)
        {
            if (IsBusy)
                return;

            IsBusy = true;
            _VEOrdersModelEdit = model;

            _GEEnterprisesModel = model.GEEnterprises;
            ENTERPRISE = $"{_GEEnterprisesModel.Id} - {_GEEnterprisesModel.RazaoSocial.SubStrTitle()}";

            _GEPaymentTypeModel = model.GEPaymentType;
            TYPEPAYMENT = $"{_GEPaymentTypeModel.Id} - {_GEPaymentTypeModel.Description.SubStrTitle()}";

            _GEPaymentConditionModel = model.GEPaymentCondition;
            PAYCONDITION = $"{_GEPaymentConditionModel.Id} - {_GEPaymentConditionModel.Description.SubStrTitle()}";

            CanChangePayment = true;

            if (_GEEnterprisesModel.PayConditionId != null)
                CanChangePayment = false;
            CODPEDVENDA = model.CodPedVenda.ToString();
            NUMPEDMOB = model.NumPedMob.ToString();
            DELIVERYDATE = model.DeliveryDate;
            NOTE = model.Note;
            STATUS = model.Status;
            STATUSSINC = model.StatusSinc.ToString();
            ISPROPOSAL = model.IsProposal ? "1" : "0";
            MessagingCenter.Send(this, "ChangeCheckBox", model.IsProposal);

            USERID = model.UserID.ToString();
            Items.Clear();
            foreach (var item in model.VEOrderItems)
            {
                var product = await _IGEProductsDB.GetItemAsync(item.ProductID);

                product.Name = product.Name.SubStrTitle();
                Items.Add(new VEOrderItemsModel
                {
                    GEProducts = product,
                    ProductID = item.ProductID,
                    PercDiscount = item.PercDiscount,
                    OrderID = item.OrderID,
                    TableValue = item.TableValue,
                    Value = item.Value,
                    Quantity = item.Quantity,
                    Id = item.Id,
                    IdApi = item.IdApi,
                    EstablishmentKey = item.EstablishmentKey,
                    UniqueKey = item.UniqueKey,
                    Note = item.Note,
                    IsActive = item.IsActive,
                    Created = item.Created,
                    Modified = item.Modified
                });
            }



            IsBusy = false;
        }
    }

}
