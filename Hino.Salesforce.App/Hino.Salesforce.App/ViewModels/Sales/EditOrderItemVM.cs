﻿using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.Utils.Extensions;
using Rg.Plugins.Popup.Services;
using System;
using System.Windows.Input;
using Xamarin.Forms;

namespace Hino.Salesforce.App.ViewModels.Sales
{
    public class EditOrderItemVM : BaseViewModel
    {
        public ICommand CmdSaveClick { protected set; get; }
        public ICommand CmdCancelClick { protected set; get; }

        private readonly VEOrderItemsModel _VEOrderItemsModel;


        public string LBLPRODUCT { get => Resources.FieldsNameResource.DescriptionProd; }
        public string LBLQUANTITY { get => Resources.FieldsNameResource.Quantity; }
        public string LBLVALUE { get => Resources.FieldsNameResource.Value; }

        public string _Quantity;
        public string QUANTITY
        {
            get
            {
                return _Quantity;
            }
            set
            {
                SetProperty(ref _Quantity, value);
            }
        }

        public string _Value;
        public string VALUE
        {
            get
            {
                return _Value;
            }
            set
            {
                SetProperty(ref _Value, value);
            }
        }

        public string _Product;
        public string PRODUCT
        {
            get
            {
                return _Product;
            }
            set
            {
                SetProperty(ref _Product, value);
            }
        }

        public string _Unit;
        public string UNIT
        {
            get
            {
                return _Unit;
            }
            set
            {
                SetProperty(ref _Unit, value);
            }
        }

        public string _Weight;
        public string WEIGHT
        {
            get
            {
                return _Weight;
            }
            set
            {
                SetProperty(ref _Weight, value);
            }
        }

        public string _PackageQTD;
        public string PACKAGEQTD
        {
            get
            {
                return _PackageQTD;
            }
            set
            {
                SetProperty(ref _PackageQTD, value);
            }
        }

        public string _QuantityConverted;
        public string QUANTITYCONVERTED
        {
            get
            {
                return _QuantityConverted;
            }
            set
            {
                SetProperty(ref _QuantityConverted, value);
            }
        }


        public EditOrderItemVM(VEOrderItemsModel pOrderItemModel)
        {
            _VEOrderItemsModel = pOrderItemModel;

            CmdSaveClick = new Command(OnSaveClick);
            CmdCancelClick = new Command(OnCancelClick);
        }

        private async void OnCancelClick(object obj)
        {
            MessagingCenter.Send(this, "EditItemCancel", "a");
            await PopupNavigation.Instance.PopAsync();

        }

        #region Validate Quantity
        private bool ValidateQuantity()
        {
            if (!decimal.TryParse(_Quantity, out decimal qtd))
                return false;
            if (qtd <= 0)
                return false;

            return true;
        }
        #endregion

        #region Validate Value
        private bool ValidateValue()
        {
            if (!decimal.TryParse(_Value, out decimal value))
                return false;
            if (value <= 0)
                return false;

            return true;
        }
        #endregion

        private async void OnSaveClick(object obj)
        {
            if (!ValidateQuantity())
            {
                ShowShortMessage(Resources.ValidationMessagesResource.QuantityIsNotValid);
                return;
            }
            if (!ValidateValue())
            {
                ShowShortMessage(Resources.ValidationMessagesResource.ValueIsNotValid);
                return;
            }

            _VEOrderItemsModel.Quantity = Convert.ToDecimal(_Quantity);
            _VEOrderItemsModel.Value = Convert.ToDecimal(_Value);

            MessagingCenter.Send(this, "EditItemSaved", _VEOrderItemsModel);
            await PopupNavigation.Instance.PopAsync();
        }

        #region Load Data
        public void LoadData()
        {
            _VEOrderItemsModel.GEProducts.Name = _VEOrderItemsModel.GEProducts.Name.SubStrTitle();
            PRODUCT = _VEOrderItemsModel.GEProducts.DisplayName;
            QUANTITY = _VEOrderItemsModel.Quantity.ToString();
            VALUE = _VEOrderItemsModel.Value.ToString();
            UNIT = _VEOrderItemsModel.GEProducts.Unit;
            WEIGHT = _VEOrderItemsModel.GEProducts.Weight.ToString();
            PACKAGEQTD = _VEOrderItemsModel.GEProducts.PackageQTD.ToString();
            QUANTITYCONVERTED = _VEOrderItemsModel.GEProducts.QuantityConverted.ToString();
            UpdateQuantityTotalSelected();
        }
        #endregion

        #region Add Quantity
        public void AddQuantity()
        {
            try
            {
                QUANTITY = (Convert.ToDecimal(QUANTITY) + 1).ToString();
            }
            catch (Exception)
            {
                QUANTITY = "1";
            }
            UpdateQuantityTotalSelected();
        }
        #endregion

        #region Remove Quantity
        public void RemoveQuantity()
        {
            try
            {
                QUANTITY = (Convert.ToDecimal(QUANTITY) - 1).ToString();
            }
            catch (Exception)
            {
                QUANTITY = "1";
            }
            UpdateQuantityTotalSelected();
        }
        #endregion

        #region Update Quantity Total Selected
        public void UpdateQuantityTotalSelected()
        {
            try
            {
                QUANTITYCONVERTED = "1";
                _VEOrderItemsModel.GEProducts.QuantityConverted = 1;

                var PackageQTD = 1;
                _VEOrderItemsModel.Quantity = Convert.ToDecimal(QUANTITY);
                var QuantitySelected = _VEOrderItemsModel.Quantity;
                if (QuantitySelected > 0 && _VEOrderItemsModel.GEProducts.PackageQTD > 0)
                {
                    PackageQTD = Convert.ToInt32(Math.Ceiling(QuantitySelected /
                        _VEOrderItemsModel.GEProducts.PackageQTD));
                }

                _VEOrderItemsModel.GEProducts.QuantityConverted = PackageQTD;
                QUANTITYCONVERTED = _VEOrderItemsModel.GEProducts.QuantityConverted.ToString();
            }
            catch (Exception)
            {
            }
        }
        #endregion
    }
}
