﻿using Hino.Salesforce.App.Models.General;
using Rg.Plugins.Popup.Services;
using System;
using System.Linq;
using System.Windows.Input;
using Xamarin.Forms;

namespace Hino.Salesforce.App.ViewModels.Sales
{
    public class ItemSaleQtdConvertVM : BaseViewModel
    {
        public ICommand CmdSaveClick { protected set; get; }
        public ICommand CmdCancelClick { protected set; get; }
        public ICommand CmdMinusClick { protected set; get; }
        public ICommand CmdPlusClick { protected set; get; }

        private readonly GEProductsModel _GEProductsModel;
        public event EventHandler ProductConfimClick;

        public string LBLTYPE { get => Resources.FieldsNameResource.Type; }

        public string _Type;
        public string TYPE
        {
            get
            {
                return _Type;
            }
            set
            {
                SetProperty(ref _Type, value);
            }
        }

        public string DisplayName
        {
            get
            {
                return _GEProductsModel.DisplayName;
            }
        }

        public string Unit
        {
            get
            {
                return _GEProductsModel.Unit;
            }
        }

        public decimal Weight
        {
            get
            {
                return _GEProductsModel.Weight;
            }
        }

        public decimal PackageQTD
        {
            get
            {
                return _GEProductsModel.PackageQTD;
            }
            set
            {
                _GEProductsModel.PackageQTD = value;

                OnPropertyChanged();
            }
        }

        public string SaleUnit
        {
            get
            {
                return _GEProductsModel.SaleUnit;
            }
        }

        public decimal Value
        {
            get
            {
                return _GEProductsModel.Value;
            }
        }

        public decimal StockBalance
        {
            get
            {
                return _GEProductsModel.StockBalance;
            }
        }

        public decimal QuantitySelected
        {
            get
            {
                return _GEProductsModel.QuantitySelected;
            }
            set
            {
                _GEProductsModel.QuantitySelected = value;
                OnPropertyChanged();
            }
        }

        decimal _QtdTotalKG = 0;
        public decimal QTDTOTALKG
        {
            get
            {
                return _QtdTotalKG;
            }
            set
            {
                SetProperty(ref _QtdTotalKG, value);
            }
        }
        decimal _QtdTotalPCS = 0;
        public decimal QTDTOTALPCS
        {
            get
            {
                return _QtdTotalPCS;
            }
            set
            {
                SetProperty(ref _QtdTotalPCS, value);
            }
        }
        decimal _QtdTotalPackages = 0;
        public decimal QTDTOTALPACKAGES
        {
            get
            {
                return _QtdTotalPackages;
            }
            set
            {
                SetProperty(ref _QtdTotalPackages, value);
            }
        }

        public ItemSaleQtdConvertVM(GEProductsModel pGEProductsModel)
        {
            _GEProductsModel = pGEProductsModel;
            TYPE = "KG";
            CmdSaveClick = new Command(OnSaveClick);
            CmdCancelClick = new Command(OnCancelClick);
            CmdMinusClick = new Command(OnMinusClick);
            CmdPlusClick = new Command(OnPlusClick);
        }

        private async void OnCancelClick(object obj) =>
            await PopupNavigation.Instance.PopAsync();


        #region Validate Quantity
        private bool ValidateQuantity()
        {
            if (!decimal.TryParse(_GEProductsModel.QuantitySelected.ToString(), out decimal qtd))
                return false;
            if (qtd <= 0)
                return false;

            return true;
        }
        #endregion

        private async void OnSaveClick(object obj)
        {
            if (!ValidateQuantity())
            {
                ShowShortMessage(Resources.ValidationMessagesResource.QuantityIsNotValid);
                return;
            }

            var UMLenght = new string[] { "PC", "UN", "PÇ", "PEÇA", "PECA", "UNIDADE", "BR", "FL", "FOLHA", "CB" };
            var UMMult = new string[] { "DUZ", "DZ", "DUZIA", "CX", "CAIXA", "PR",
                                        "PAR", "PARES", "JG", "JOGO", "KIT", "KT",
                                        "CE", "CT", "CENTO", "MC", "MAÇO", "SC",
                                        "SACO", "MI", "MILHEIRO", "1000UN", "CJ" };
            var UMWeight = new string[] { "KG", "KILO", "GR", "G", "GRAMA", "TON",
                                              "TN", "T", "TONELADA" };


            if (UMLenght.Any(r => r == _GEProductsModel.Unit))
                _GEProductsModel.QuantitySelected = QTDTOTALPCS;
            else if (UMMult.Any(r => r == _GEProductsModel.Unit))
                _GEProductsModel.QuantitySelected = QTDTOTALPACKAGES;
            else if (UMWeight.Any(r => r == _GEProductsModel.Unit))
                _GEProductsModel.QuantitySelected = QTDTOTALKG;

            ProductConfimClick?.Invoke(_GEProductsModel, new EventArgs());
            await PopupNavigation.Instance.PopAsync();
        }


        public void OnMinusClick(object obj)
        {
            if (Convert.ToDecimal(QuantitySelected) >= 1)
                QuantitySelected = (QuantitySelected - 1);
            else
                QuantitySelected = 0;

            UpdateQuantityTotalSelected();
        }

        public void OnPlusClick(object obj)
        {
            QuantitySelected = (QuantitySelected + 1);

            UpdateQuantityTotalSelected();
        }

        public void QuantityComplete()
        {
            UpdateQuantityTotalSelected();
        }

        #region Update Quantity Total Selected
        public void UpdateQuantityTotalSelected()
        {

            try
            {
                if (TYPE == "KG")
                    CalculateKG();
                else if (TYPE == "PEÇAS")
                    CalculatePC();
                else if (TYPE == "EMBALAGENS")
                    CalculatePackage();
            }
            catch (Exception)
            {
                ClearProperties();
            }
        }
        #endregion

        #region Clear Properties
        public void ClearProperties()
        {
            _GEProductsModel.QuantityConverted = 0;
            this.QuantitySelected = 0;
            this.QTDTOTALKG = 0;
            this.QTDTOTALPACKAGES = 0;
            this.QTDTOTALPCS = 0;
        }
        #endregion

        #region Get Factor Unit
        private decimal GetFactorUnit(string pUnit)
        {
            switch (pUnit)
            {
                case "DUZ":
                case "DZ":
                case "DUZIA":
                    return 12;
                case "CX":
                case "CAIXA":
                    return 1;
                case "PR":
                case "PAR":
                case "PARES":
                    return 2;
                case "JG":
                case "JOGO":
                    return 1;
                case "KIT":
                case "KT":
                    return 1;
                case "CE":
                case "CT":
                case "CENTO":
                    return 100;
                case "MC":
                case "MAÇO":
                case "SC":
                case "SACO":
                    return 1;
                case "MI":
                case "MILHEIRO":
                case "1000UN":
                    return 1000;
                case "CJ":
                    return 1;
            }

            return 1;
        }
        #endregion

        #region Calculate KG
        private void CalculateKG()
        {
            QTDTOTALKG = QuantitySelected;
            QTDTOTALPCS = Math.Round(QuantitySelected / _GEProductsModel.Weight, 4);
            QTDTOTALPACKAGES = Math.Ceiling((QuantitySelected / _GEProductsModel.Weight) / _GEProductsModel.PackageQTD);
        }
        #endregion

        #region Calculate PC
        private void CalculatePC()
        {
            QTDTOTALKG = Math.Round(QuantitySelected * _GEProductsModel.Weight, 4);
            QTDTOTALPCS = QuantitySelected;
            QTDTOTALPACKAGES = Math.Ceiling(QuantitySelected / _GEProductsModel.PackageQTD);
        }
        #endregion

        #region Calculate Package
        private void CalculatePackage()
        {
            QTDTOTALKG = Math.Round(QuantitySelected * _GEProductsModel.PackageQTD * _GEProductsModel.Weight, 4);
            QTDTOTALPCS = Math.Round(QuantitySelected * _GEProductsModel.PackageQTD, 4);
            QTDTOTALPACKAGES = QuantitySelected;
        }
        #endregion
    }
}
