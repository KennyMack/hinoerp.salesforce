﻿using Hino.Salesforce.App.Models.General;
using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.Services.DB.Interfaces.General;
using Hino.Salesforce.App.Services.DB.Interfaces.Sales;
using Hino.Salesforce.App.Utils.Paging;
using Hino.Salesforce.App.Views.Sales;
using Rg.Plugins.Popup.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.MultiSelectListView;

namespace Hino.Salesforce.App.ViewModels.Sales
{
    public class ListItemsSalesVM : BaseViewModel
    {
        private readonly GEEnterprisesModel _GEEnterprisesModel;
        private readonly IGEProductsDB _IGEProductsDB;
        private readonly IVESalePriceDB _IVESalePriceDB;

        public ICommand CmdOkClick { protected set; get; }
        public ICommand CmdBackClick { protected set; get; }
        public ICommand CmdMinusClick { protected set; get; }
        public ICommand CmdPlusClick { protected set; get; }
        public ICommand CmdConvertClick { protected set; get; }

        private List<GEProductsModel> _ItemsSelected = new List<GEProductsModel>();

        private MultiSelectObservableCollection<GEProductsModel> _Items;
        public MultiSelectObservableCollection<GEProductsModel> Items
        {
            get => _Items;
            set
            {
                SetProperty(ref _Items, value);
            }
        }

        public ListItemsSalesVM(GEEnterprisesModel pModel)
        {
            _GEEnterprisesModel = pModel;
            CmdOkClick = new Command(OnOkClick);
            CmdBackClick = new Command(OnBackClick);
            CmdMinusClick = new Command(OnMinusClick);
            CmdPlusClick = new Command(OnPlusClick);
            CmdConvertClick = new Command(OnConvertClick);
            _Items = new MultiSelectObservableCollection<GEProductsModel>();
            _IGEProductsDB = DInjection.GetIntance<IGEProductsDB>();
            _IVESalePriceDB = DInjection.GetIntance<IVESalePriceDB>();

            base.PageNum = 1;
        }

        public async void OnConvertClick(object obj)
        {
            if (IsBusy)
                return;

            if (!(obj is GEProductsModel item))
                return;

            var ItemPage = new ItemSaleQtdConvertPage(item);
            ItemPage.ProductConfimClick += ItemPage_ProductConfimClick;

            await PopupNavigation.Instance.PushAsync(ItemPage);
        }

        private void ItemPage_ProductConfimClick(object sender, EventArgs e)
        {
            if (IsBusy)
                return;

            QuantityComplete(sender);
        }

        public void OnMinusClick(object obj)
        {
            if (!(obj is GEProductsModel item))
                return;
            if (Convert.ToDecimal(item.QuantitySelected) >= 1)
                item.QuantitySelected = (item.QuantitySelected - 1);
            else
                item.QuantitySelected = 0;

            if (Convert.ToDecimal(item.QuantitySelected) <= 0 &&
                ExistsItemSelected(item))
            {
                _ItemsSelected.Remove(_ItemsSelected.Where(r => r.Id == item.Id).First());
                Items.Where(r => r.Data.Id == item.Id).First().IsSelected = false;
            }
            UpdateQuantityTotalSelected(item);
        }

        public void OnPlusClick(object obj)
        {
            if (!(obj is GEProductsModel item))
                return;
            item.QuantitySelected = (item.QuantitySelected + 1);
            if (item.QuantitySelected > 0)
            {
                if (!ExistsItemSelected(item))
                    _ItemsSelected.Add(item);
                Items.Where(r => r.Data.Id == item.Id).First().IsSelected = true;
            }
            UpdateQuantityTotalSelected(item);
        }

        public void QuantityComplete(object obj)
        {
            if (!(obj is GEProductsModel item))
                return;
            if (item.QuantitySelected > 0)
            {
                if (!ExistsItemSelected(item))
                    _ItemsSelected.Add(item);
                Items.Where(r => r.Data.Id == item.Id).First().IsSelected = true;
            }
            else if (item.QuantitySelected <= 0 &&
                ExistsItemSelected(item))
            {
                _ItemsSelected.Remove(_ItemsSelected.Where(r => r.Id == item.Id).First());
                var itemSel = Items.Where(r => r.Data.Id == item.Id).First();
                itemSel.IsSelected = false;
                itemSel.Data.QuantityConverted = 0;
            }

            UpdateQuantityTotalSelected(item);
        }

        #region Update Quantity Total Selected
        public void UpdateQuantityTotalSelected(GEProductsModel item)
        {
            try
            {
                _ItemsSelected.Where(r => r.Id == item.Id).First().QuantitySelected = item.QuantitySelected;
            }
            catch (Exception)
            {

            }

            try
            {
                var itemSelected = _ItemsSelected.Where(r => r.Id == item.Id).First();

                var Unit = itemSelected.Unit;

                itemSelected.QuantityConverted = 1;

                var PackageQTD = 1;
                var QuantitySelected = Convert.ToDecimal(itemSelected.QuantitySelected);
                itemSelected.QuantityPackageWeightConverted = Math.Round(QuantitySelected / itemSelected.Weight, 4);

                var UMLenght = new string[] { "PC", "UN", "PÇ", "PEÇA", "PECA", "UNIDADE", "BR", "FL", "FOLHA", "CB" };
                var UMMult = new string[] { "DUZ", "DZ", "DUZIA", "CX", "CAIXA", "PR",
                                            "PAR", "PARES", "JG", "JOGO", "KIT", "KT",
                                            "CE", "CT", "CENTO", "MC", "MAÇO", "SC",
                                            "SACO", "MI", "MILHEIRO", "1000UN", "CJ" };
                var UMWeight = new string[] { "KG", "KILO", "GR", "G", "GRAMA", "TON",
                                              "TN", "T", "TONELADA" };
                if (QuantitySelected > 0 && itemSelected.PackageQTD > 0)
                {
                    if (UMLenght.Any(r => r == Unit))
                        PackageQTD = Convert.ToInt32(Math.Ceiling(QuantitySelected / itemSelected.PackageQTD));
                    else if (UMMult.Any(r => r == Unit))
                        PackageQTD = Convert.ToInt32(Math.Ceiling(Math.Ceiling((QuantitySelected * GetFactorUnit(Unit))) / itemSelected.PackageQTD));
                    else if (UMWeight.Any(r => r == Unit))
                        PackageQTD = Convert.ToInt32(Math.Ceiling(Math.Ceiling((QuantitySelected / itemSelected.Weight)) / itemSelected.PackageQTD));
                    else
                        PackageQTD = Convert.ToInt32(Math.Ceiling(QuantitySelected / itemSelected.PackageQTD));
                }



                /*
                if (QuantitySelected > 0 && itemSelected.PackageQTD > 0)
                {
                    PackageQTD = Convert.ToInt32(Math.Ceiling(QuantitySelected / itemSelected.PackageQTD));
                }*/


                itemSelected.QuantityConverted = PackageQTD;
            }
            catch (Exception)
            {
            }
        }
        #endregion

        #region Get Factor Unit
        private decimal GetFactorUnit(string pUnit)
        {
            switch (pUnit)
            {
                case "DUZ":
                case "DZ":
                case "DUZIA":
                    return 12;
                case "CX":
                case "CAIXA":
                    return 1;
                case "PR":
                case "PAR":
                case "PARES":
                    return 2;
                case "JG":
                case "JOGO":
                    return 1;
                case "KIT":
                case "KT":
                    return 1;
                case "CE":
                case "CT":
                case "CENTO":
                    return 100;
                case "MC":
                case "MAÇO":
                case "SC":
                case "SACO":
                    return 1;
                case "MI":
                case "MILHEIRO":
                case "1000UN":
                    return 1000;
                case "CJ":
                    return 1;
            }

            return 1;
        }
        #endregion

        #region Copy Selected Items
        private void CopySelectedItems()
        {
            Items.Where(r => Convert.ToDecimal(r.Data.QuantitySelected) > 0).ToList()
                   .ForEach(r =>
                   {
                       if (!ExistsItemSelected(r.Data))
                           _ItemsSelected.Add(r.Data);
                       UpdateQuantityTotalSelected(r.Data);
                   });
        }
        #endregion

        #region Get Data From Source
        private async Task<PagedResult<GEProductsModel>> GetDataFromSourceAsync()
        {
            //CopySelectedItems();

            return await _IGEProductsDB.SearchProdSale(_SearchData, _GEEnterprisesModel, base.PageNum);
        }
        #endregion

        #region SelectItems
        private void SelectItems()
        {
            Items.ToList().
                ForEach(r =>
                {
                    r.IsSelected = _ItemsSelected.Where(s => s.Id == r.Data.Id).Count() > 0;
                    r.Data.QuantitySelected = _ItemsSelected.Where(s => s.Id == r.Data.Id)
                        .Sum(g => g.QuantitySelected);
                });
        }
        #endregion

        #region Search Data
        public async override Task SearchData_Changed()
        {
            base.PageNum = 1;

            Items.CopyFromPagedResult(await GetDataFromSourceAsync());
            SelectItems();
        }
        #endregion

        protected async override Task LoadMoreData()
        {
            IsBusy = true;

            try
            {
                Items.CopyFromPagedResult(await GetDataFromSourceAsync(), false);
                SelectItems();
                IsBusy = false;
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected async override void RefreshData()
        {
            base.RefreshData();

            IsBusy = true;

            base.PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }

        private async void OnBackClick(object obj)
        {
            MessagingCenter.Send(this, "ItemsClosed", "a");
            await PopupNavigation.Instance.PopAsync();
        }

        private async void OnOkClick(object obj)
        {
            //CopySelectedItems();
            /*if (_SelectedItem == null)
            {
                ShowShortMessage("Selecione o registro");
                return;
            }*/

            MessagingCenter.Send(this, "ItemsSalesSelected", _ItemsSelected);
            await PopupNavigation.Instance.PopAsync();
        }


        #region ExistsItemSelected
        private bool ExistsItemSelected(GEProductsModel pItem) =>
            _ItemsSelected.Exists(r => r.Id == pItem.Id);
        #endregion

        #region Selection Changed
        public void SelectionChanged(GEProductsModel pItem)
        {
            if (!ExistsItemSelected(pItem))
            {
                if (pItem.QuantitySelected == 0)
                    pItem.QuantitySelected = 1;
                _ItemsSelected.Add(pItem);
            }
        }
        #endregion
    }
}
