﻿using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.Services.DB.Interfaces.Sales;
using Hino.Salesforce.App.Utils.Messages;
using Hino.Salesforce.App.Utils.Paging;
using System;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace Hino.Salesforce.App.ViewModels.Sales
{
    public class OrdersVM : BaseViewModel
    {
        public ICommand CmdDeleteClick { protected set; get; }
        private readonly IVEOrdersDB _IVEOrdersDB;
        private readonly IVEOrderItemsDB _IVEOrderItemsDB;
        public ObservableCollection<VEOrdersModel> Items { get; set; }
        public VEOrdersModel SelectedItem;

        public OrdersVM()
        {
            Title = "Clientes";
            Items = new ObservableCollection<VEOrdersModel>();
            CmdDeleteClick = new Command(OnDeleteClick);

            _IVEOrderItemsDB = DInjection.GetIntance<IVEOrderItemsDB>();
            _IVEOrdersDB = DInjection.GetIntance<IVEOrdersDB>();
            base.PageNum = 1;
        }

        #region Convert Search Data to number
        private int ConvertSearchToNumber(string pSearch)
        {
            if (int.TryParse(pSearch, out int ret))
                return ret;

            return 0;
        }
        #endregion

        #region On Create Click
        public override void OnFABButtonTapped(object sender, EventArgs e) =>
            MessagingCenter.Send(this, "ChangePage", 1);
        #endregion

        #region On Edit Click
        public void EditOrder(VEOrdersModel pModel)
        {
            if (pModel.StatusSinc == 0)
                MessagingCenter.Send(this, "ChangePage", pModel);
            else
                ShowShortMessage(Resources.ErrorMessagesResource.OrderWasSincronized);
        }
        #endregion

        #region Delete Orders
        public async Task DeleteOrders()
        {
            if (await _IVEOrderItemsDB.DeleteByOrderIdAsync(SelectedItem.Id) &&
                await _IVEOrdersDB.DeleteItemAsync(SelectedItem.Id))
            {
                Items.Remove(SelectedItem);
                ShowShortMessage(Resources.ValidationMessagesResource.RemoveSuccess);
            }
        }
        #endregion

        #region On Delete Click
        private void OnDeleteClick()
        {
            if (SelectedItem != null)
            {
                if (SelectedItem.StatusSinc == 0)
                {
                    OnMessageConfirmRaise(new MessageRaiseEvent(Resources.ValidationMessagesResource.ConfirmRemove));
                }
                else
                    ShowShortMessage(Resources.ErrorMessagesResource.OrderWasSincronized);
            }
            else
                ShowShortMessage(Resources.ValidationMessagesResource.OrderNotSelected);
        }
        #endregion

        #region Get Data From Source
        private async Task<PagedResult<VEOrdersModel>> GetDataFromSourceAsync()
        {
            if (!string.IsNullOrEmpty(_SearchData))
            {
                var num = ConvertSearchToNumber(_SearchData);
                return await _IVEOrdersDB.Query(r =>
                    r.GEEnterprises.RazaoSocial.Normalize(NormalizationForm.FormD).ToLower().Contains(_SearchData.Normalize(NormalizationForm.FormD).ToLower()) ||
                    r.Id == num ||
                    r.GEEnterprises.CNPJCPF.ToLower().Contains(_SearchData.ToLower()) ||
                    //r.GEEnterprises.Email.ToLower().Contains(_SearchData.ToLower()) ||
                    r.GEEnterprises.NomeFantasia.Normalize(NormalizationForm.FormD).ToLower().Contains(_SearchData.Normalize(NormalizationForm.FormD).ToLower())
                    , base.PageNum);
            }
            else
                return await _IVEOrdersDB.GetItemsAsync(base.PageNum);
        }
        #endregion

        #region Search Data
        public async override Task SearchData_Changed()
        {
            base.PageNum = 1;

            Items.CopyFromPagedResult(await GetDataFromSourceAsync());
        }
        #endregion

        protected async override Task LoadMoreData()
        {
            IsBusy = true;
            var Title = base.Title;
            base.Title = "Carregando";

            try
            {
                Items.CopyFromPagedResult(await GetDataFromSourceAsync(), false);
                IsBusy = false;
            }
            catch (Exception e)
            {
                ShowShortMessage(e.Message);
                base.Title = Title;

                IsBusy = false;
            }
            finally
            {
                IsBusy = false;
            }
        }

        protected async override void RefreshData()
        {
            base.RefreshData();

            IsBusy = true;

            base.PageNum = 1;
            Items.Clear();

            await LoadMoreData();
        }
    }
}
