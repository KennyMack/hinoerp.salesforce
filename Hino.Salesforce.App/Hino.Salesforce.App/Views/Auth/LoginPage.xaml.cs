﻿
using Hino.Salesforce.App.Templates.ImageEntry;
using Hino.Salesforce.App.ViewModels.Auth;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Auth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginVM loginVM;

        #region Initialize
        private void Initialize(string pEmail = "")
        {
            if (string.IsNullOrEmpty(pEmail) ||
                string.IsNullOrWhiteSpace(pEmail))
                this.BindingContext = loginVM = new LoginVM();
            else
                this.BindingContext = loginVM = new LoginVM(pEmail);

            var entries = new ImageEntry[]
            {
                txtEstablishmentKey,
                txtUserOrEmail,
                txtPassword
            };
            txtEstablishmentKey.Entries = entries;
            txtUserOrEmail.Entries = entries;
            txtPassword.Entries = entries;
        }

        public LoginPage(string pEmail)
        {
            InitializeComponent();
            Initialize(pEmail);
        }

        public LoginPage()
        {
            InitializeComponent();
            Initialize();
        }
        #endregion

        protected override void OnAppearing()
        {
            base.OnAppearing();
            // loginVM.RegisterLoading<LoginVM>();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            // loginVM.ClearLoadMessage();
        }
    }
}