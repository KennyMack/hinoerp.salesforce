﻿using Hino.Salesforce.App.ViewModels.Auth;
using System;
using System.Threading.Tasks;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Auth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RefreshLoginPage : Rg.Plugins.Popup.Pages.PopupPage
    {
        private RefreshLoginVM _RefreshLoginVM;
        public RefreshLoginPage()
        {
            InitializeComponent();
            BindingContext = _RefreshLoginVM = new RefreshLoginVM();

        }

        private async void IcoBack_imageBoxClicked(object sender, EventArgs e)
        {
            await _RefreshLoginVM.OnBackClick();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // ### Methods for supporting animations in your popup page ###

        // Invoked before an animation appearing
        protected override void OnAppearingAnimationBegin()
        {
            base.OnAppearingAnimationBegin();
        }

        // Invoked after an animation appearing
        protected override void OnAppearingAnimationEnd()
        {
            base.OnAppearingAnimationEnd();
        }

        // Invoked before an animation disappearing
        protected override void OnDisappearingAnimationBegin()
        {
            base.OnDisappearingAnimationBegin();
        }

        // Invoked after an animation disappearing
        protected override void OnDisappearingAnimationEnd()
        {
            base.OnDisappearingAnimationEnd();
        }

        protected override Task OnAppearingAnimationBeginAsync()
        {
            return base.OnAppearingAnimationBeginAsync();
        }

        protected override Task OnAppearingAnimationEndAsync()
        {
            return base.OnAppearingAnimationEndAsync();
        }

        protected override Task OnDisappearingAnimationBeginAsync()
        {
            return base.OnDisappearingAnimationBeginAsync();
        }

        protected override Task OnDisappearingAnimationEndAsync()
        {
            return base.OnDisappearingAnimationEndAsync();
        }

        protected override bool OnBackButtonPressed()
        {
            //_SearchSinglePageVM.CmdBackClick.Execute(null);
            // Return true if you don't want to close this popup page when a back button is pressed
            return base.OnBackButtonPressed();
        }

        protected override bool OnBackgroundClicked()
        {
            //_SearchSinglePageVM.CmdBackClick.Execute(null);
            // Return false if you don't want to close this popup page when a background of the popup page is clicked
            return base.OnBackgroundClicked();
        }
    }
}