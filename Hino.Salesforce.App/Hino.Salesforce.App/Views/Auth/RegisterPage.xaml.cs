﻿using Hino.Salesforce.App.Templates.ImageEntry;
using Hino.Salesforce.App.ViewModels.Auth;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Auth
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        public RegisterVM registerVM;
        public RegisterPage()
        {
            InitializeComponent();
            BindingContext = registerVM = new RegisterVM();

            var entries = new ImageEntry[]
            {
                txtESTABLISHMENTKEY,
                txtUSERKEY,
                txtUSERNAME,
                txtEMAIL,
                txtPASSWORD,
                txtPASSWORDCONFIRM
            };
            txtESTABLISHMENTKEY.Entries = entries;
            txtUSERKEY.Entries = entries;
            txtUSERNAME.Entries = entries;
            txtEMAIL.Entries = entries;
            txtPASSWORD.Entries = entries;
            txtPASSWORDCONFIRM.Entries = entries;

            registerVM.MessageRaise += RegisterVM_MessageRaise;
        }

        private async void RegisterVM_MessageRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
            await DisplayAlert("Registro", e.Text, "OK");
            await registerVM.GoToLogin();
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            //registerVM.ClearLoadMessage();
        }
    }
}