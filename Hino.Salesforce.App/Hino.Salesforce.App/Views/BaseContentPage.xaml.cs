﻿using Hino.Salesforce.App.Utils.Events;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BaseContentPage : ContentPage
    {
        #region Eventos
        public event EventHandler<AppearingEvent> AfterAppearing;
        protected void AfterAppearingRaise(AppearingEvent e) =>
            AfterAppearing?.Invoke(this, e);
        #endregion

        public BaseContentPage()
        {
            InitializeComponent();
        }
    }
}