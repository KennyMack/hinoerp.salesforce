﻿using System.Collections.Generic;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BaseTabbedPage : TabbedPage
    {
        protected Stack<Page> TabStack { get; private set; } = new Stack<Page>();

        public BaseTabbedPage()
        {
            InitializeComponent();
        }
        protected override void OnCurrentPageChanged()
        {
            var page = CurrentPage;
            if (page != null && TabStack.Where(r => r.Title == page.Title).Count() <= 0)
                TabStack.Push(page);

            base.OnCurrentPageChanged();
        }

        protected override bool OnBackButtonPressed()
        {
            if (TabStack.Any())
                TabStack.Pop();

            if (TabStack.Any())
            {
                CurrentPage = TabStack.Pop();

                return true;
            }

            return base.OnBackButtonPressed();
        }
    }
}