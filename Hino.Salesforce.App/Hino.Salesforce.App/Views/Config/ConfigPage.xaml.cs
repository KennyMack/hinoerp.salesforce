﻿using Hino.Salesforce.App.ViewModels;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Config
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ConfigPage : ContentPage
    {
        private ConfigVM _ConfigVM;

        public ConfigPage()
        {
            InitializeComponent();
            BindingContext = _ConfigVM = new ConfigVM();
            _ConfigVM.MessageConfirmRaise += ConfigVM_MessageConfirmRaise;
        }

        private async void ConfigVM_MessageConfirmRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
            if (await DisplayAlert("Continuar?", e.Text, "Sim", "Não"))
            {
                _ConfigVM.ClearAllData();
            }
        }

        protected override void OnAppearing()
        {
            _ConfigVM.OnLoad();
        }

        private void SWISPROPOSAL_Toggled(object sender, ToggledEventArgs e)
        {

        }

        private void SWSYNCONSTART_Toggled(object sender, ToggledEventArgs e)
        {

        }

        private void SWSYNCONSAVE_Toggled(object sender, ToggledEventArgs e)
        {

        }

        private void SWINTERVALSYNC_Toggled(object sender, ToggledEventArgs e)
        {

        }
    }
}