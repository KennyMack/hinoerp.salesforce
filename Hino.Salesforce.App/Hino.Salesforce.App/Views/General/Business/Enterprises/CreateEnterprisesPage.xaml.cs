﻿using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.ViewModels.General.Business;
using System;
using System.Linq;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Business.Enterprises
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateEnterprisesPage : BaseContentPage
    {
        private CreateEnterprisesVM _CreateEnterprisesVM;

        #region Initialize
        private void Initialize(GEEnterprisesModel pModel = null)
        {
            if (pModel == null)
                _CreateEnterprisesVM = new CreateEnterprisesVM();
            else
                _CreateEnterprisesVM = new CreateEnterprisesVM(pModel);

            this.BindingContext = _CreateEnterprisesVM;
        }

        public CreateEnterprisesPage(GEEnterprisesModel pModel)
        {
            InitializeComponent();
            Initialize(pModel);
        }

        public CreateEnterprisesPage()
        {
            InitializeComponent();
            Initialize();
        }
        #endregion

        protected override void OnAppearing()
        {
            base.OnAppearing();
            _CreateEnterprisesVM.RegisterLoading<GEEnterprisesModel>();
            // VOLTAR NO FUTURO
            // await _CreateEnterprisesVM.LoadData();

            txtZIPCODE.HIBehavior = new Behavior[] {
                new Templates.EntryMaskedBehavior
                {
                    Mask = "XXXXX-XXX"
                }
            };
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            _CreateEnterprisesVM.ClearLoadMessage();
            _CreateEnterprisesVM.ClearIntance();
            Initialize();
        }

        private void CbeType_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                var behavior = txtCNPJCPF.Behaviors.Where(r => r.ToString() == "Hino.Salesforce.App.Templates.EntryMaskedBehavior").FirstOrDefault();
                if (behavior != null)
                    txtCNPJCPF.Behaviors.Remove(behavior);

                txtCNPJCPF.Behaviors.Add(new Templates.EntryMaskedBehavior
                {
                    Mask = ((Picker)sender).SelectedIndex == 0 ? "XXX.XXX.XXX-XX" : "XX.XXX.XXX/XXXX-XX"
                });
            }
            catch (Exception)
            {

            }
        }

        /* VOLTAR NO FUTURO
        private async void CbeCountry_SelectedIndexChanged(object sender, EventArgs e)
        {
            await _CreateEnterprisesVM.LoadStates();
        }

        private async void CbeStates_SelectedIndexChanged(object sender, EventArgs e)
        {
            await _CreateEnterprisesVM.LoadCity();

        }

        private void CbeCities_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        */

        private async void TxtZIPCODE_RightImageClicked(object sender, EventArgs e)
        {
            if (await _CreateEnterprisesVM.GetCEPInfo())
                txtNUM.Focus();
        }

        private void SeUF_SearchEntryClicked(object sender, EventArgs e)
        {

        }
    }
}