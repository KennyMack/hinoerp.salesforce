﻿using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.ViewModels.General.Business;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Business.Enterprises
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListEnterprisesPage : BaseContentPage
    {
        private readonly EnterprisesVM _EnterprisesVM;
        public ListEnterprisesPage()
        {
            InitializeComponent();

            BindingContext = _EnterprisesVM = new EnterprisesVM();
            _EnterprisesVM.InfiniteListView(lvListEnterprises);
            _EnterprisesVM.SearchEvent(txtEnterprisesSearchBar);
            _EnterprisesVM.FABButton(fabBtn);
            _EnterprisesVM.MessageConfirmRaise += EnterprisesVM_MessageConfirmRaise;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_EnterprisesVM.Items.Count == 0)
                _EnterprisesVM.RefreshDataCommand.Execute(null);
        }

        private async void EnterprisesVM_MessageConfirmRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
            if (await DisplayAlert("Continuar?", e.Text, "Sim", "Não"))
            {
                await _EnterprisesVM.DeleteEnterprise();
            }
            lvListEnterprises.SelectedItem = null;
            _EnterprisesVM.SelectedItem = null;
        }

        private void SearchItem_Clicked(object sender, EventArgs e)
        {
            _EnterprisesVM.ShowSearch = !_EnterprisesVM.ShowSearch;

            if (_EnterprisesVM.ShowSearch)
                txtEnterprisesSearchBar.Focus();
        }

        private void Edit_Clicked(object sender, EventArgs e)
        {
            if (!(sender is MenuItem item))
                return;

            if (!(item.CommandParameter is GEEnterprisesModel itemSelected))
                return;

            _EnterprisesVM.EditEnterprise(itemSelected);
        }

        private void Remove_Clicked(object sender, EventArgs e)
        {
            if (!(sender is MenuItem item))
                return;

            if (!(item.CommandParameter is GEEnterprisesModel itemSelected))
                return;

            _EnterprisesVM.SelectedItem = itemSelected;
            _EnterprisesVM.DeleteClick();
        }

        private void LvListEnterprises_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (!(e.SelectedItem is GEEnterprisesModel item))
                return;

            _EnterprisesVM.SelectedItem = item;
        }
    }
}