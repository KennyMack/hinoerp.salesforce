﻿using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.ViewModels.General.Business;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Business.Enterprises
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainEnterprisesPage : BaseTabbedPage
    {
        public MainEnterprisesPage()
        {
            InitializeComponent();
            var CreateEnterprisesPage = new CreateEnterprisesPage
            {
                Title = "Novo Cliente"
            };

            var ListEnterprisesPage = new ListEnterprisesPage
            {
                Title = "CLientes"
            };

            ListEnterprisesPage.AfterAppearing += (sender, evt) =>
            {
                ListEnterprisesPage.Title = $@"{evt.Title} ({evt.Quantidade})";
            };

            this.Children.Add(ListEnterprisesPage);
            this.Children.Add(CreateEnterprisesPage);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<CreateEnterprisesVM, int>(this, "ChangePage", (obj, page) =>
            {
                this.CurrentPage = Children[page];
            });

            MessagingCenter.Subscribe<EnterprisesVM, int>(this, "ChangePage", (obj, page) =>
            {
                this.CurrentPage = Children[page];
            });

            MessagingCenter.Subscribe<EnterprisesVM, GEEnterprisesModel>(this, "ChangePage", (obj, model) =>
            {
                MessagingCenter.Send(this, "LoadRegister", model);
                this.CurrentPage = Children[1];
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<CreateEnterprisesVM, string>(this, "ChangePage");
            MessagingCenter.Unsubscribe<EnterprisesVM, string>(this, "ChangePage");
            MessagingCenter.Unsubscribe<EnterprisesVM, GEEnterprisesModel>(this, "ChangePage");
        }


    }
}