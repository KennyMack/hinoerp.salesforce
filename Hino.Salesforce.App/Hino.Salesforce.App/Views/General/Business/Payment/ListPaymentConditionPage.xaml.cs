﻿using Hino.Salesforce.App.ViewModels.General.Business;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Business.Payment
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListPaymentConditionPage : BaseContentPage
    {
        private readonly PaymentConditionVM _PaymentConditionVM;
        public ListPaymentConditionPage()
        {
            InitializeComponent();
            BindingContext = _PaymentConditionVM = new PaymentConditionVM();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_PaymentConditionVM.Items.Count == 0)
                _PaymentConditionVM.RefreshDataCommand.Execute(null);
        }
    }
}