﻿using Hino.Salesforce.App.ViewModels.General.Business;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Business.Payment
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListPaymentTypePage : BaseContentPage
    {
        private readonly PaymentTypeVM _PaymentTypeVM;
        public ListPaymentTypePage()
        {
            InitializeComponent();
            BindingContext = _PaymentTypeVM = new PaymentTypeVM();
        }
        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_PaymentTypeVM.Items.Count == 0)
                _PaymentTypeVM.RefreshDataCommand.Execute(null);
        }
    }
}