﻿using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Business.Payment
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainPayment : BaseTabbedPage
    {
        public MainPayment()
        {
            InitializeComponent();

            var ListPaymentTypePage = new ListPaymentTypePage
            {
                Title = "Forma Pagto."
            };

            var ListPaymentConditionPage = new ListPaymentConditionPage
            {
                Title = "Cond. Pagto."
            };

            ListPaymentTypePage.AfterAppearing += (sender, evt) =>
            {
                ListPaymentTypePage.Title = $@"{evt.Title} ({evt.Quantidade})";
            };
            ListPaymentConditionPage.AfterAppearing += (sender, evt) =>
            {
                ListPaymentConditionPage.Title = $@"{evt.Title} ({evt.Quantidade})";
            };

            this.Children.Add(ListPaymentTypePage);
            this.Children.Add(ListPaymentConditionPage);
        }
    }
}