﻿using Hino.Salesforce.App.Models.General;
using Hino.Salesforce.App.ViewModels.General;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Products
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditProductsPage : ContentPage
    {
        private readonly EditProductsVM _EditProductsVM;
        public EditProductsPage(GEProductsModel pModel)
        {
            InitializeComponent();

            this.BindingContext = _EditProductsVM = new EditProductsVM(pModel);
        }

        protected async override void OnAppearing()
        {
            await _EditProductsVM.LoadValues();
            base.OnAppearing();
        }
    }
}