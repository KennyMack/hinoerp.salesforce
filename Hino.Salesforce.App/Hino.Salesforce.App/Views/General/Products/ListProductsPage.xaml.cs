﻿using Hino.Salesforce.App.Models.General;
using Hino.Salesforce.App.Services.SyncData;
using Hino.Salesforce.App.ViewModels.General;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.General.Products
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListProductsPage : ContentPage
    {
        private SyncData _SyncData;
        private readonly ProductsVM _ProductsVM;
        public ListProductsPage()
        {
            InitializeComponent();
            BindingContext = _ProductsVM = new ProductsVM();
            _ProductsVM.InfiniteListView(lvListProducts);
            _ProductsVM.SearchEvent(txtProductsSearchBar);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_ProductsVM.Items.Count == 0)
                _ProductsVM.RefreshDataCommand.Execute(null);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (_SyncData != null)
                _SyncData.Dispose();
        }

        private void FAB_Clicked(object sender, EventArgs e)
        {
            _ProductsVM.ShowShortMessage("clicou");
        }

        private void SearchItem_Clicked(object sender, EventArgs e)
        {
            _ProductsVM.ShowSearch = !_ProductsVM.ShowSearch;

            if (_ProductsVM.ShowSearch)
                txtProductsSearchBar.Focus();
        }

        private async void LvListProducts_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as GEProductsModel;
            if (item == null)
                return;

            await Navigation.PushAsync(new EditProductsPage(item));

            // Manually deselect item.
            lvListProducts.SelectedItem = null;
        }

        private async void SyncProducts_Clicked(object sender, EventArgs e)
        {
            if (_SyncData == null)
            {
                _SyncData = new SyncData();

                if (_SyncData.IsConnected() &&
                    await _SyncData.ConnectionTypeOK())
                    await _SyncData.SyncronizeProducts();
                _SyncData.Dispose();
                _SyncData = null;
            }
        }
    }
}