﻿using Hino.Salesforce.App.Utils.Interfaces;
using Hino.Salesforce.App.Views.Sales;
using Plugin.Permissions;
using Plugin.Permissions.Abstractions;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Main
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterMainPageMenu : MasterDetailPage
    {
        public MasterMainPageMenu()
        {
            InitializeComponent();
            MasterPage.ListView.ItemSelected += ListView_ItemSelected;
            this.IsPresentedChanged += MasterMainPageMenu_IsPresentedChanged;
        }
        private void MasterMainPageMenu_IsPresentedChanged(object sender, EventArgs e)
        {
            MasterPage.UpdateConnectionStatus();
        }

        private async void ListView_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            var item = e.SelectedItem as MasterMainPageMenuMenuItem;
            if (item == null)
                return;

            if (item.TargetType == typeof(bool))
            {
                var establishmentKey = App.ESTABLISHMENTKEY;
                App.USER_TOKEN = null;
                App.REFRESH_TOKEN = null;
                App.ESTABLISHMENTKEY = null;
                App.UserLogged = null;
                App.ESTABLISHMENTKEY = establishmentKey;

                var navpage = new NavigationPage(new Views.Auth.LoginPage());
                App.Current.MainPage = navpage;

                return;
            }
            var page = (Page)Activator.CreateInstance(item.TargetType);
            page.Title = item.Title;

            if (item.TargetType == typeof(MainSalesPage))
            {
                //App.Current.MainPage = page;
                await Detail.Navigation.PopToRootAsync();
            }
            else
                await Detail.Navigation.PushAsync(page);

            IsPresented = false;

            MasterPage.ListView.SelectedItem = null;
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();

            try
            {
                var status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Camera);
                if (status != PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Camera))
                    {
                        DependencyService.Get<IMessage>().ShowShortMessage("Permissão para a câmera é obrigatória.");
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Camera);
                    //Best practice to always check that the key exists
                    if (results.ContainsKey(Permission.Camera))
                        status = results[Permission.Camera];
                }

                status = await CrossPermissions.Current.CheckPermissionStatusAsync(Permission.Storage);
                if (status != PermissionStatus.Granted)
                {
                    if (await CrossPermissions.Current.ShouldShowRequestPermissionRationaleAsync(Permission.Storage))
                    {
                        DependencyService.Get<IMessage>().ShowShortMessage("Permissão para acesso ao armazenamento é obrigatória.");
                    }

                    var results = await CrossPermissions.Current.RequestPermissionsAsync(Permission.Storage);
                    //Best practice to always check that the key exists
                    if (results.ContainsKey(Permission.Storage))
                        status = results[Permission.Storage];
                }
            }
            catch (Exception)
            {

                //throw;
            }
        }
    }
}