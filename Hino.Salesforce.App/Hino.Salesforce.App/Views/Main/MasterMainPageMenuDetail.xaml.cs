﻿
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Main
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterMainPageMenuDetail : ContentPage
    {
        public MasterMainPageMenuDetail()
        {
            InitializeComponent();
        }
    }
}