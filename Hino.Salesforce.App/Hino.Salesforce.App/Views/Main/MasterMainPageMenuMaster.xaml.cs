﻿using Hino.Salesforce.App.Models;
using Hino.Salesforce.App.Services.DB.Interfaces;
using Hino.Salesforce.App.Services.SyncData;
using Hino.Salesforce.App.ViewModels;
using Hino.Salesforce.App.Views.Config;
using Hino.Salesforce.App.Views.General.Business.Enterprises;
using Hino.Salesforce.App.Views.General.Products;
using Hino.Salesforce.App.Views.Sales;
using Plugin.Connectivity;
using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Main
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MasterMainPageMenuMaster : ContentPage
    {
        private bool _Startup;
        public bool UpdateConnectionStatus()
        {
            var ret = _MasterMainPageMenuMasterViewModel.UpdateConnectionStatus();

            lblSyncronize.IsEnabled = ret;

            if (lblSyncronize.IsEnabled)
            {
                imgStatusConnOK.IsVisible = true;
                imgStatusConnNO.IsVisible = false;
            }
            else
            {
                imgStatusConnOK.IsVisible = false;
                imgStatusConnNO.IsVisible = true;
            }

            return ret;
        }

        public ListView ListView;
        private MasterMainPageMenuMasterViewModel _MasterMainPageMenuMasterViewModel;
        public MasterMainPageMenuMaster()
        {
            InitializeComponent();
            _Startup = true;
            TapGestureRecognizer tapSyncronizer = new TapGestureRecognizer();
            tapSyncronizer.Tapped += Syncronize_Clicked;
            lblSyncronize.GestureRecognizers.Add(tapSyncronizer);
            // imgStatusConnOK.IsVisible = false;
            // imgStatusConnNO.IsVisible = false;

            // ModalLoadingPage modal = null;

            BindingContext = _MasterMainPageMenuMasterViewModel = new MasterMainPageMenuMasterViewModel();
            ListView = MenuItemsListView;

            _MasterMainPageMenuMasterViewModel.PropertyChanged += _MasterMainPageMenuMasterViewModel_PropertyChanged;

            /*MessagingCenter.Subscribe<SyncData, string>(this, "SyncStatus", async (obj, item) =>
            {
                if (item == Hino.Salesforce.App.Resources.ValidationMessagesResource.LoadStarting && !IsSync)
                {
                    IsSync = true;
                    if (modal == null)
                    {
                        modal = new ModalLoadingPage(LoadType.SyncStatus.ToString());
                        await PopupNavigation.Instance.PushAsync(modal);
                    }
                }                    

                if (item == Hino.Salesforce.App.Resources.ValidationMessagesResource.LoadComplete && IsSync)
                {
                    IsSync = false;
                    modal = null;
                    await PopupNavigation.Instance.PopAsync();
                }                    
            });*/
        }

        private void _MasterMainPageMenuMasterViewModel_PropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            if (e.PropertyName == "TextConnectionStatus")
            {
                if (_MasterMainPageMenuMasterViewModel.TextConnectionStatus == "Conectado")
                {
                    imgStatusConnOK.IsVisible = true;
                    imgStatusConnNO.IsVisible = false;
                }
                else
                {
                    imgStatusConnOK.IsVisible = false;
                    imgStatusConnNO.IsVisible = true;
                }

            }
        }

        protected async override void OnAppearing()
        {
            base.OnAppearing();
            await _MasterMainPageMenuMasterViewModel.LoadConfig();
            if (_Startup)
            {
                if (App.FIRSTACCESS && !App.FIRSTSYNCOK)
                    await DisplayAlert(Salesforce.App.Resources.ErrorMessagesResource.WelcomeTitle,
                        Salesforce.App.Resources.ErrorMessagesResource.WelcomeMessage, "OK");
                else if (!App.FIRSTACCESS && !App.FIRSTSYNCOK)
                    await DisplayAlert(Salesforce.App.Resources.ErrorMessagesResource.FirstSyncTitle,
                        Salesforce.App.Resources.ErrorMessagesResource.FirstSyncMessage, "OK");

                await _MasterMainPageMenuMasterViewModel.SyncData(_Startup);
                _Startup = false;
            }
        }

        class MasterMainPageMenuMasterViewModel : BaseViewModel
        {
            private SyncData _SyncData;
            private ConfigModel _ConfigModel;
            private IConfigDB _IConfigDB;
            public ObservableCollection<MasterMainPageMenuMenuItem> MenuItems { get; set; }
            string _iconConnectionStatus;
            public string IconConnectionStatus
            {
                get { return _iconConnectionStatus; }
                set
                {
                    SetProperty(ref _iconConnectionStatus, value);
                }
            }
            string _textConnectionStatus;
            public string TextConnectionStatus
            {
                get { return _textConnectionStatus; }
                set
                {
                    SetProperty(ref _textConnectionStatus, value);
                }
            }
            string _iconColorStatus;
            public string IconColorStatus
            {
                get { return _iconColorStatus; }
                set
                {
                    SetProperty(ref _iconColorStatus, value);
                }
            }
            public bool UpdateConnectionStatus()
            {
                var hasInternet = false;
                if (!CrossConnectivity.IsSupported)
                    hasInternet = true;
                else
                    hasInternet = CrossConnectivity.Current.IsConnected;

                if (hasInternet)
                {
                    IconConnectionStatus = "\uf111";
                    TextConnectionStatus = "Conectado";
                    IconColorStatus = "#05a05a";
                    return true;
                }
                else
                {
                    IconConnectionStatus = "\uf06a";
                    TextConnectionStatus = "Desconectado";
                    IconColorStatus = "#B80000";
                    return false;
                }
            }

            public MasterMainPageMenuMasterViewModel()
            {
                UpdateConnectionStatus();
                _IConfigDB = DInjection.GetIntance<IConfigDB>();

                MenuItems = new ObservableCollection<MasterMainPageMenuMenuItem>(new[]
                {
                    new MasterMainPageMenuMenuItem {
                        Id = 0,
                        Title = "Meus Pedidos",
                        Icon = "chart-area",
                        IconCode = Application.Current.Resources["imgSales"].ToString(),
                        //IconCode = "\uf1fe",
                        TargetType = typeof(MainSalesPage)

                    },
                    new MasterMainPageMenuMenuItem {
                        Id = 0,
                        Title = "Produtos",
                        Icon = "archive",
                        IconCode = Application.Current.Resources["imgProducts"].ToString(),
                        //IconCode = "\uf187",
                        TargetType = typeof(ListProductsPage)

                    },
                    new MasterMainPageMenuMenuItem {
                        Id = 0,
                        Title = "Empresas",
                        Icon = "table",
                        IconCode = Application.Current.Resources["imgEnterprises"].ToString(),
                        //IconCode = "\uf0ce",
                        TargetType = typeof(MainEnterprisesPage)

                    },
                    /*new MasterMainPageMenuMenuItem {
                        Id = 0,
                        Title = "Forma/Cond. Pagto.",
                        Icon = "tag",
                        IconCode = Application.Current.Resources["imgTag"].ToString(),
                        //IconCode = "\uf02b",
                        TargetType = typeof(MainPayment)

                    },*/
                    new MasterMainPageMenuMenuItem {
                        Id = 0,
                        Title = "Configurações",
                        Icon = "cogs",
                        IconCode = Application.Current.Resources["imgConfig"].ToString(),
                        //IconCode = "\uf085",
                        TargetType = typeof(ConfigPage)

                    },
                    new MasterMainPageMenuMenuItem {
                        Id = 4,
                        Title = "Sair",
                        Icon = "times",
                        IconCode = Application.Current.Resources["imgClose"].ToString(),
                        //IconCode = "\uf00d",
                        TargetType = typeof(bool) },
                });
            }

            #region Load Config
            public async Task LoadConfig()
            {
                _ConfigModel = await _IConfigDB.GetItemAsync(1);
                if (_ConfigModel == null)
                {
                    try
                    {
                        _ConfigModel = new ConfigModel
                        {
                            Id = 1,
                            UniqueKey = Guid.NewGuid().ToString(),
                            EstablishmentKey = App.ESTABLISHMENTKEY,
                            UserKey = App.UserLogged.UserKey,
                            SyncOnSave = true,
                            SyncOnStart = true,
                            SyncWIFI = false,
                            IntervalSync = 30,
                            LastSync = new DateTime(1900, 1, 1, 1, 1, 1)
                        };

                        await _IConfigDB.AddItemAsync(_ConfigModel);

                    }
                    catch (Exception)
                    {

                    }
                }
            }
            #endregion

            #region Sync Data
            public async Task SyncData(bool onStart, bool force = false)
            {
                if ((_SyncData == null || force) && App.UserLogged != null)
                {
                    _SyncData = new SyncData();

                    if (_SyncData.IsConnected() &&
                        await _SyncData.ConnectionTypeOK())
                    {
                        if (await _SyncData.RefreshTokenUser())
                        {
                            var syncronize = (await _SyncData.SyncOnStart() && onStart) || force;

                            if (syncronize)
                            {
                                if (_SyncData.HasToSync(_ConfigModel) || force || App.FIRSTACCESS || !App.FIRSTSYNCOK)
                                {
                                    await _SyncData.Syncronize(true);
                                    // await _SyncData.SendChanges();
                                }
                            }
                        }
                        else if (force)
                            _SyncData.ShowInvalidOrExpired();
                        // ShowShortMessage(Salesforce.App.Resources.ErrorMessagesResource.InvalidRefreshToken);
                    }
                    App.FIRSTACCESS = false;
                    _SyncData.Dispose();
                    _SyncData = null;
                }
            }
            #endregion
        }

        private async void Syncronize_Clicked(object sender, EventArgs e)
        {
            await lblSyncronize.FadeTo(0.3, 300, Easing.Linear);
            await lblSyncronize.FadeTo(1, 300, Easing.Linear);
            await _MasterMainPageMenuMasterViewModel.SyncData(false, true);
        }

    }
}