﻿using System;

namespace Hino.Salesforce.App.Views.Main
{

    public class MasterMainPageMenuMenuItem
    {
        public MasterMainPageMenuMenuItem()
        {
            TargetType = typeof(MasterMainPageMenuDetail);
        }
        public int Id { get; set; }
        public string Title { get; set; }
        public string Icon { get; set; }
        public string IconCode { get; set; }

        public Type TargetType { get; set; }
    }
}