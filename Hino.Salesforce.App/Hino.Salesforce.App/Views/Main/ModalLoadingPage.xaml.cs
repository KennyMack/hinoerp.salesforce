﻿using Hino.Salesforce.App.Services.SyncData;
using Hino.Salesforce.App.Utils.Load.Enums;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Main
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ModalLoadingPage : Rg.Plugins.Popup.Pages.PopupPage
    {
        public ModalLoadingPage(string pMessage)
        {
            InitializeComponent();
            txtStatusError.Text = "";
            txtStatus.Text = "";
            if (pMessage == LoadType.SyncStatus.ToString())
            {
                MessagingCenter.Subscribe<SyncData, string>(this, "SyncStatus", (obj, item) =>
                {
                    var type = item.Substring(0, 2);
                    if (type == "S-")
                        txtStatus.Text = item.Substring(2);
                    else if (type == "E-")
                        txtStatusError.Text = item.Substring(2);
                    else
                    {
                        txtStatus.Text = item;
                        txtStatusError.Text = "";
                    }
                });
            }
        }

        public void ChangeStatusErrorText(string pText)
        {
            txtStatusError.Text = pText;
        }

        public void ChangeStatusText(string pText)
        {
            txtStatus.Text = pText;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        // ### Methods for supporting animations in your popup page ###

        // Invoked before an animation appearing
        protected override void OnAppearingAnimationBegin()
        {
            base.OnAppearingAnimationBegin();
        }

        // Invoked after an animation appearing
        protected override void OnAppearingAnimationEnd()
        {
            base.OnAppearingAnimationEnd();
        }

        // Invoked before an animation disappearing
        protected override void OnDisappearingAnimationBegin()
        {
            base.OnDisappearingAnimationBegin();
        }

        // Invoked after an animation disappearing
        protected override void OnDisappearingAnimationEnd()
        {
            base.OnDisappearingAnimationEnd();
        }

        protected override Task OnAppearingAnimationBeginAsync()
        {
            return base.OnAppearingAnimationBeginAsync();
        }

        protected override Task OnAppearingAnimationEndAsync()
        {
            return base.OnAppearingAnimationEndAsync();
        }

        protected override Task OnDisappearingAnimationBeginAsync()
        {
            return base.OnDisappearingAnimationBeginAsync();
        }

        protected override Task OnDisappearingAnimationEndAsync()
        {
            return base.OnDisappearingAnimationEndAsync();
        }

        protected override bool OnBackButtonPressed()
        {
            // Return true if you don't want to close this popup page when a back button is pressed
            return true;
        }

        protected override bool OnBackgroundClicked()
        {
            // Return false if you don't want to close this popup page when a background of the popup page is clicked
            return false;
        }
    }
}