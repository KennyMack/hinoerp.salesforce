﻿using Hino.Salesforce.App.Services.Search.Interfaces;
using Hino.Salesforce.App.ViewModels;
using System;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Main
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class SearchSinglePage : Rg.Plugins.Popup.Pages.PopupPage
    {
        private SearchSinglePageVM _SearchSinglePageVM;

        public SearchSinglePage(ISearchDataResult pIRepositorySearch)
        {
            InitializeComponent();
            BindingContext = _SearchSinglePageVM = new SearchSinglePageVM(pIRepositorySearch);
            _SearchSinglePageVM.Title = pIRepositorySearch.Title;
            _SearchSinglePageVM.InfiniteListView(lvResultSearch);
            _SearchSinglePageVM.SearchEvent(txtSearchBar);

            TapGestureRecognizer icoBackTap = new TapGestureRecognizer();
            icoBack.GestureRecognizers.Add(icoBackTap);
            icoBackTap.Tapped += IcoBackTap_Tapped; ;
        }

        private void IcoBackTap_Tapped(object sender, EventArgs e)
        {
            _SearchSinglePageVM.CmdBackClick.Execute(null);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_SearchSinglePageVM.Items.Count == 0)
                _SearchSinglePageVM.RefreshDataCommand.Execute(null);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            var item = args.SelectedItem as SearchItemList;
            if (item == null)
                return;

            _SearchSinglePageVM.CmdOkClick.Execute(null);

            lvResultSearch.SelectedItem = null;
        }


        // ### Methods for supporting animations in your popup page ###

        // Invoked before an animation appearing
        protected override void OnAppearingAnimationBegin()
        {
            base.OnAppearingAnimationBegin();
        }

        // Invoked after an animation appearing
        protected override void OnAppearingAnimationEnd()
        {
            base.OnAppearingAnimationEnd();
        }

        // Invoked before an animation disappearing
        protected override void OnDisappearingAnimationBegin()
        {
            base.OnDisappearingAnimationBegin();
        }

        // Invoked after an animation disappearing
        protected override void OnDisappearingAnimationEnd()
        {
            base.OnDisappearingAnimationEnd();
        }

        protected override Task OnAppearingAnimationBeginAsync()
        {
            return base.OnAppearingAnimationBeginAsync();
        }

        protected override Task OnAppearingAnimationEndAsync()
        {
            return base.OnAppearingAnimationEndAsync();
        }

        protected override Task OnDisappearingAnimationBeginAsync()
        {
            return base.OnDisappearingAnimationBeginAsync();
        }

        protected override Task OnDisappearingAnimationEndAsync()
        {
            return base.OnDisappearingAnimationEndAsync();
        }

        protected override bool OnBackButtonPressed()
        {
            _SearchSinglePageVM.CmdBackClick.Execute(null);
            // Return true if you don't want to close this popup page when a back button is pressed
            return base.OnBackButtonPressed();
        }

        protected override bool OnBackgroundClicked()
        {
            _SearchSinglePageVM.CmdBackClick.Execute(null);
            // Return false if you don't want to close this popup page when a background of the popup page is clicked
            return base.OnBackgroundClicked();
        }

        private void IcoBack_imageBoxClicked(object sender, EventArgs e)
        {
            _SearchSinglePageVM.CmdBackClick.Execute(null);
        }
    }
}