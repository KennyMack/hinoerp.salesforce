﻿using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.ViewModels.Sales;
using System;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Sales
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CreateSalesPage : BaseContentPage
    {
        private CreateOrdersVM _CreateOrdersVM;

        #region Initialize
        private void Initialize(VEOrdersModel pModel = null)
        {
            if (pModel == null)
                _CreateOrdersVM = new CreateOrdersVM();
            else
                _CreateOrdersVM = new CreateOrdersVM(pModel);

            this.BindingContext = _CreateOrdersVM;
            _CreateOrdersVM.MessageConfirmRaise += OrdersVM_MessageConfirmRaise;

            /*MessagingCenter.Subscribe<CreateOrdersVM, DateTime>(this, "ChangeDeliveryDate", (obj, date) =>
            {
                dpDeliverDate.Date = date;
            });*/
        }

        public async Task LoadRegister(VEOrdersModel model) =>
            await _CreateOrdersVM.LoadRegister(model);


        private async void SearchEnterpriseClicked_Tapped(object sender, System.EventArgs e)
        {
            await _CreateOrdersVM.ShowSearchFormEnterpriseIDAsync();
        }

        private async void SearchPayConditionClicked_Tapped(object sender, System.EventArgs e)
        {
            if (_CreateOrdersVM.CanChangePayment)
                await _CreateOrdersVM.ShowSearchFormPAYCONDITIONIDAsync();
            else
                _CreateOrdersVM.ShowShortMessage(Hino.Salesforce.App.Resources.ValidationMessagesResource.CanChangePayment);
        }

        public CreateSalesPage(VEOrdersModel pModel)
        {
            InitializeComponent();
            Initialize(pModel);
        }

        public CreateSalesPage()
        {
            InitializeComponent();
            Initialize();
        }
        #endregion

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<CreateOrdersVM, bool>(this, "ChangeCheckBox", (obj, item) =>
            {
                swISPROPOSAL.IsToggled = item;
            });
            Initialize();
            _CreateOrdersVM.RegisterLoading<VEOrdersModel>();
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            _CreateOrdersVM.ClearLoadMessage();
            _CreateOrdersVM.ClearInstance();
            MessagingCenter.Unsubscribe<CreateOrdersVM, bool>(this, "ChangeCheckBox");
        }

        private void SwISPROPOSAL_Toggled(object sender, ToggledEventArgs e)
        {
            _CreateOrdersVM.ISPROPOSAL = e.Value ? "1" : "0";
        }

        private async void OrdersVM_MessageConfirmRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
            if (await DisplayAlert("Continuar?", e.Text, "Sim", "Não"))
            {
                _CreateOrdersVM.RemoveItem();
            }
            lvListOrders.SelectedItem = null;
            _CreateOrdersVM.SelectedItem = null;
        }

        private void LvListOrders_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (!(e.SelectedItem is VEOrderItemsModel item))
                return;

            _CreateOrdersVM.SelectedItem = item;
        }

        private async void Edit_Clicked(object sender, EventArgs e)
        {
            if (!(sender is MenuItem item))
                return;

            if (!(item.CommandParameter is VEOrderItemsModel itemSelected))
                return;

            _CreateOrdersVM.SelectedItem = itemSelected;
            await _CreateOrdersVM.ShowEditItemAsync();
        }

        private void Remove_Clicked(object sender, EventArgs e)
        {
            if (!(sender is MenuItem item))
                return;

            if (!(item.CommandParameter is VEOrderItemsModel itemSelected))
                return;

            _CreateOrdersVM.SelectedItem = itemSelected;
            _CreateOrdersVM.CmdRemoveClick.Execute(null);
        }

        private async void BtnAddItens_imageBoxClicked(object sender, EventArgs e)
        {
            await btnAddItens.FadeTo(0.3, 300, Easing.Linear);
            await btnAddItens.FadeTo(1, 300, Easing.Linear);
            _CreateOrdersVM.CmdAddClick.Execute(null);
        }
    }
}