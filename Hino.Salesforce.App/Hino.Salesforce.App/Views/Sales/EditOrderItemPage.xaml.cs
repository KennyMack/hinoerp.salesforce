﻿using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.ViewModels.Sales;
using System;
using System.Threading.Tasks;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Sales
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class EditOrderItemPage : Rg.Plugins.Popup.Pages.PopupPage
    {
        private EditOrderItemVM _EditOrderItemVM;
        public EditOrderItemPage(VEOrderItemsModel pOrderItem)
        {
            InitializeComponent();
            BindingContext = _EditOrderItemVM = new EditOrderItemVM(pOrderItem);

            //TapGestureRecognizer icoBackTap = new TapGestureRecognizer();
            //icoBack.GestureRecognizers.Add(icoBackTap);
            //icoBackTap.Tapped += IcoBackTap_Tapped;
        }
        private void IcoBackTap_Tapped(object sender, EventArgs e)
        {
            _EditOrderItemVM.CmdCancelClick.Execute(null);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            _EditOrderItemVM.LoadData();
        }

        // ### Methods for supporting animations in your popup page ###

        // Invoked before an animation appearing
        protected override void OnAppearingAnimationBegin()
        {
            base.OnAppearingAnimationBegin();
        }

        // Invoked after an animation appearing
        protected override void OnAppearingAnimationEnd()
        {
            base.OnAppearingAnimationEnd();
        }

        // Invoked before an animation disappearing
        protected override void OnDisappearingAnimationBegin()
        {
            base.OnDisappearingAnimationBegin();
        }

        // Invoked after an animation disappearing
        protected override void OnDisappearingAnimationEnd()
        {
            base.OnDisappearingAnimationEnd();
        }

        protected override Task OnAppearingAnimationBeginAsync()
        {
            return base.OnAppearingAnimationBeginAsync();
        }

        protected override Task OnAppearingAnimationEndAsync()
        {
            return base.OnAppearingAnimationEndAsync();
        }

        protected override Task OnDisappearingAnimationBeginAsync()
        {
            return base.OnDisappearingAnimationBeginAsync();
        }

        protected override Task OnDisappearingAnimationEndAsync()
        {
            return base.OnDisappearingAnimationEndAsync();
        }

        protected override bool OnBackButtonPressed()
        {
            // Return true if you don't want to close this popup page when a back button is pressed
            return false;//base.OnBackButtonPressed();
        }

        protected override bool OnBackgroundClicked()
        {
            // Return false if you don't want to close this popup page when a background of the popup page is clicked
            return false; //base.OnBackgroundClicked();
        }

        private void IcoBack_imageBoxClicked(object sender, EventArgs e)
        {
            // _ListItemsSalesVM.CmdBackClick.Execute(null);
        }

        private void ImageEntry_RightImageClicked(object sender, EventArgs e)
        {

        }


        private void TxtQuantity_RightImageClicked(object sender, EventArgs e)
        {
            _EditOrderItemVM.AddQuantity();
            //txtQuantity.Text = (Convert.ToDecimal(txtQuantity.Text) + 1).ToString();
        }

        private void TxtQuantity_LeftImageClicked(object sender, EventArgs e)
        {
            _EditOrderItemVM.RemoveQuantity();
            //txtQuantity.Text = (Convert.ToDecimal(txtQuantity.Text) - 1).ToString();
        }
    }
}