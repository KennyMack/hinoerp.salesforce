﻿using Hino.Salesforce.App.Models.General;
using Hino.Salesforce.App.ViewModels.Sales;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Sales
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ItemSaleQtdConvertPage : Rg.Plugins.Popup.Pages.PopupPage
    {
        public event EventHandler ProductConfimClick;

        private readonly ItemSaleQtdConvertVM _ItemSaleQtdConvertVM;
        public ItemSaleQtdConvertPage(GEProductsModel pProduct)
        {
            InitializeComponent();
            BindingContext = _ItemSaleQtdConvertVM = new ItemSaleQtdConvertVM(pProduct);
            _ItemSaleQtdConvertVM.ProductConfimClick += _ItemSaleQtdConvertVM_ProductConfimClick;
        }

        private void _ItemSaleQtdConvertVM_ProductConfimClick(object sender, EventArgs e) =>
            ProductConfimClick.Invoke(sender, e);

        private void TxtQuantity_Completed(object sender, EventArgs e)
        {
            _ItemSaleQtdConvertVM.QuantityComplete();
        }

        private void TxtQuantity_Unfocused(object sender, FocusEventArgs e)
        {
            _ItemSaleQtdConvertVM.QuantityComplete();
        }

        private void PckTYPE_SelectedIndexChanged(object sender, EventArgs e) =>
            _ItemSaleQtdConvertVM.ClearProperties();
    }
}
