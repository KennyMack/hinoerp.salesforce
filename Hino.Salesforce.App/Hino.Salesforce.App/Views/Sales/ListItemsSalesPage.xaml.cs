﻿using Hino.Salesforce.App.Models.General;
using Hino.Salesforce.App.Models.General.Business;
using Hino.Salesforce.App.ViewModels.Sales;
using System;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.MultiSelectListView;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Sales
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListItemsSalesPage : Rg.Plugins.Popup.Pages.PopupPage
    {
        ListItemsSalesVM _ListItemsSalesVM;

        public ListItemsSalesPage(GEEnterprisesModel pModel)
        {
            InitializeComponent();
            BindingContext = _ListItemsSalesVM = new ListItemsSalesVM(pModel);
            _ListItemsSalesVM.InfiniteListView(lvResultSearch);
            _ListItemsSalesVM.SearchEvent(txtSearchBar);

            TapGestureRecognizer icoBackTap = new TapGestureRecognizer();
            icoBack.GestureRecognizers.Add(icoBackTap);
            icoBackTap.Tapped += IcoBackTap_Tapped;
        }

        private void IcoBackTap_Tapped(object sender, EventArgs e)
        {
            _ListItemsSalesVM.CmdBackClick.Execute(null);
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();

            if (_ListItemsSalesVM.Items.Count == 0)
                _ListItemsSalesVM.RefreshDataCommand.Execute(null);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
        }

        void OnItemSelected(object sender, SelectedItemChangedEventArgs args)
        {
            if (!(args.SelectedItem is SelectableItem item))
                return;

            _ListItemsSalesVM.SelectionChanged((GEProductsModel)item.Data);

            lvResultSearch.SelectedItem = null;
        }

        // ### Methods for supporting animations in your popup page ###

        // Invoked before an animation appearing
        protected override void OnAppearingAnimationBegin()
        {
            base.OnAppearingAnimationBegin();
        }

        // Invoked after an animation appearing
        protected override void OnAppearingAnimationEnd()
        {
            base.OnAppearingAnimationEnd();
        }

        // Invoked before an animation disappearing
        protected override void OnDisappearingAnimationBegin()
        {
            base.OnDisappearingAnimationBegin();
        }

        // Invoked after an animation disappearing
        protected override void OnDisappearingAnimationEnd()
        {
            base.OnDisappearingAnimationEnd();
        }

        protected override Task OnAppearingAnimationBeginAsync()
        {
            return base.OnAppearingAnimationBeginAsync();
        }

        protected override Task OnAppearingAnimationEndAsync()
        {
            return base.OnAppearingAnimationEndAsync();
        }

        protected override Task OnDisappearingAnimationBeginAsync()
        {
            return base.OnDisappearingAnimationBeginAsync();
        }

        protected override Task OnDisappearingAnimationEndAsync()
        {
            return base.OnDisappearingAnimationEndAsync();
        }

        protected override bool OnBackButtonPressed()
        {
            // Return true if you don't want to close this popup page when a back button is pressed
            return false;//base.OnBackButtonPressed();
        }

        protected override bool OnBackgroundClicked()
        {
            // Return false if you don't want to close this popup page when a background of the popup page is clicked
            return false; //base.OnBackgroundClicked();
        }

        private void IcoBack_imageBoxClicked(object sender, EventArgs e)
        {
            _ListItemsSalesVM.CmdBackClick.Execute(null);
        }

        private void TxtQuantity_LeftImageClicked(object sender, EventArgs e)
        {
        }

        private void TxtQuantity_RightImageClicked(object sender, EventArgs e)
        {

        }

        private void TxtQuantity_Completed(object sender, EventArgs e)
        {
            _ListItemsSalesVM.QuantityComplete(((Entry)sender).ReturnCommandParameter);
        }

        private void TxtQuantity_Unfocused(object sender, FocusEventArgs e)
        {
            _ListItemsSalesVM.QuantityComplete(((Entry)sender).ReturnCommandParameter);
        }
    }
}