﻿using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.Services.SyncData;
using Hino.Salesforce.App.ViewModels.Sales;
using System;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Sales
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ListSalesPage : BaseContentPage
    {
        private SyncData _SyncData;
        private readonly OrdersVM _OrdersVM;
        public ListSalesPage()
        {
            InitializeComponent();

            BindingContext = _OrdersVM = new OrdersVM();
            _SyncData = null;

            _OrdersVM.InfiniteListView(lvListOrders);
            _OrdersVM.SearchEvent(txtOrdersSearchBar);
            _OrdersVM.FABButton(fabBtn);
            _OrdersVM.MessageConfirmRaise += OrdersVM_MessageConfirmRaise;

        }

        private async void OrdersVM_MessageConfirmRaise(object sender, Utils.Interfaces.IMessageRaise e)
        {
            if (await DisplayAlert("Continuar?", e.Text, "Sim", "Não"))
            {
                await _OrdersVM.DeleteOrders();
            }
            lvListOrders.SelectedItem = null;
            _OrdersVM.SelectedItem = null;
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<MainSalesPage, string>(this, "Reload", (obj, page) =>
            {
                _OrdersVM.RefreshDataCommand.Execute(null);
            });

            if (_OrdersVM.Items.Count == 0)
                _OrdersVM.RefreshDataCommand.Execute(null);
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            if (_SyncData != null)
                _SyncData.Dispose();
            MessagingCenter.Unsubscribe<MainSalesPage, string>(this, "Reload");
        }

        private void LvListOrders_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (!(e.SelectedItem is VEOrdersModel item))
                return;

            _OrdersVM.SelectedItem = item;
        }

        private void SearchItem_Clicked(object sender, EventArgs e)
        {
            _OrdersVM.ShowSearch = !_OrdersVM.ShowSearch;

            if (_OrdersVM.ShowSearch)
                txtOrdersSearchBar.Focus();
        }

        private void Edit_Clicked(object sender, EventArgs e)
        {
            if (!(sender is MenuItem item))
                return;

            if (!(item.CommandParameter is VEOrdersModel itemSelected))
                return;

            _OrdersVM.EditOrder(itemSelected);
        }

        private void Remove_Clicked(object sender, EventArgs e)
        {
            if (!(sender is MenuItem item))
                return;

            if (!(item.CommandParameter is VEOrdersModel itemSelected))
                return;

            _OrdersVM.SelectedItem = itemSelected;
            _OrdersVM.CmdDeleteClick.Execute(null);
        }

        private async void SyncOrders_Clicked(object sender, EventArgs e)
        {
            if (_SyncData == null)
            {
                _SyncData = new SyncData();
                if (_SyncData.IsConnected() &&
                    await _SyncData.ConnectionTypeOK())
                    await _SyncData.SyncronizeOrders();
                _SyncData.Dispose();
                _SyncData = null;
            }
        }
    }
}