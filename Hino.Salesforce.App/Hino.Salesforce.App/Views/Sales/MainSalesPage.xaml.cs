﻿using Hino.Salesforce.App.Models.Sales;
using Hino.Salesforce.App.ViewModels.Sales;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hino.Salesforce.App.Views.Sales
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class MainSalesPage : BaseTabbedPage
    {
        public MainSalesPage()
        {
            InitializeComponent();

            var CreateSalesPage = new CreateSalesPage
            {
                Title = "Novo Pedido"
            };

            var ListSalesPage = new ListSalesPage
            {
                Title = "Pedidos"
            };

            ListSalesPage.AfterAppearing += (sender, evt) =>
            {
                ListSalesPage.Title = $@"{evt.Title} ({evt.Quantidade})";
            };

            this.Children.Add(ListSalesPage);
            this.Children.Add(CreateSalesPage);

        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            MessagingCenter.Subscribe<CreateOrdersVM, int>(this, "ChangePage", (obj, page) =>
            {
                this.CurrentPage = Children[page];
                MessagingCenter.Send(this, "Reload", "Reload");
            });

            MessagingCenter.Subscribe<OrdersVM, int>(this, "ChangePage", (obj, page) =>
            {
                this.CurrentPage = Children[page];
            });

            MessagingCenter.Subscribe<OrdersVM, VEOrdersModel>(this, "ChangePage", async (obj, model) =>
            {
                this.CurrentPage = Children[1];
                await ((CreateSalesPage)this.CurrentPage).LoadRegister(model);
                //MessagingCenter.Send(this, "LoadRegister", model);
            });
        }

        protected override void OnDisappearing()
        {
            base.OnDisappearing();
            MessagingCenter.Unsubscribe<CreateOrdersVM, int>(this, "ChangePage");
            MessagingCenter.Unsubscribe<OrdersVM, int>(this, "ChangePage");
            MessagingCenter.Unsubscribe<OrdersVM, VEOrdersModel>(this, "ChangePage");
            MessagingCenter.Unsubscribe<MainSalesPage, VEOrdersModel>(this, "LoadRegister");
        }
    }
}