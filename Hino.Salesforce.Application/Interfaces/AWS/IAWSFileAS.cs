﻿using Amazon;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.AWS
{
    public interface IAWSFileAS
    {
        List<ModelException> Errors { get; set; }
        RegionEndpoint ConvertRegion(string pRegion);
        Task<bool> UploadFileAsync(string pFileName, string pAWSKey);
        Task<bool> DeleteFileAsync(string pAWSKey);
        string DownloadFileAsync(string pAWSKey);
    }
}
