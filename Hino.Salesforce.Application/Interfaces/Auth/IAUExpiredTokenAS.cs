using Hino.Salesforce.Infra.Cross.Entities.Auth;

namespace Hino.Salesforce.Application.Interfaces.Auth
{
    public interface IAUExpiredTokenAS : IBaseAppService<AUExpiredToken>
    {
    }
}
