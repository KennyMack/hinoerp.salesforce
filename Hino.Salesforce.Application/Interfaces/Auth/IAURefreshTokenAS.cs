using Hino.Salesforce.Infra.Cross.Entities.Auth;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.Auth
{
    public interface IAURefreshTokenAS : IBaseAppService<AURefreshToken>
    {
        Task<AURefreshToken> GetByRefreshTokenId(string pRefreshTokenId, string pEstablishmentKey);
        Task<AURefreshToken> GetByRefreshTokenId(string pRefreshTokenId);
        Task RemoveByRefreshTokenId(string pRefreshTokenId);
    }
}
