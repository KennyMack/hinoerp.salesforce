﻿using Hino.Salesforce.Infra.Cross.Entities.General;

namespace Hino.Salesforce.Application.Interfaces.Email.General
{
    public interface IEstablishmentEmailAS : IBaseEmailAS<GEEstablishments>
    {

    }
}
