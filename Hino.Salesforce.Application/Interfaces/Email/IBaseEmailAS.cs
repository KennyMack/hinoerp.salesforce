﻿using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.Email
{
    public interface IBaseEmailAS<T> where T : class
    {
        Task<bool> SendCreate(T model, string pFrom, string pTo);
        Task<bool> SendRemove(T model, string pFrom, string pTo);
        Task<bool> SendUpdate(T model, string pFrom, string pTo);
    }
}
