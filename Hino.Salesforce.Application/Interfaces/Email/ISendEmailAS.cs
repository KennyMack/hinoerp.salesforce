﻿using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.Email
{
    public interface ISendEmailAS
    {
        string LocalEmail { get; set; }
        MailAddress From { get; set; }
        MailAddressCollection To { get; set; }
        List<Dictionary<string, string>> ReplaceTags { get; set; }

        Task<bool> Send(string pEstablishmentKey, MailMessage pMailMessage);
    }
}
