using Hino.Salesforce.Infra.Cross.Entities.Fiscal;

namespace Hino.Salesforce.Application.Interfaces.Fiscal
{
    public interface IFSFiscalOperAS : IBaseAppService<FSFiscalOper>
    {
    }
}
