using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;

namespace Hino.Salesforce.Application.Interfaces.Fiscal.Taxes
{
    public interface IFSICMSSTAliqUFAS : IBaseAppService<FSICMSSTAliqUF>
    {
    }
}
