using Hino.Salesforce.Application.ViewModels.Fiscal.Taxes;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.Fiscal.Taxes
{
    public interface IFSNCMAS : IBaseAppService<FSNCM>
    {
        Task LoadNCMsAsync(FSNCMLoadVM[] pLoad);
    }
}
