﻿using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.General.Business
{
    public interface IGEEnterpriseAnnotationAS : IBaseAppService<GEEnterpriseAnnot>
    {
    }
}
