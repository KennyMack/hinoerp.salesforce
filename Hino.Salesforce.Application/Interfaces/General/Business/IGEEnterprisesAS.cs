using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.General.Business
{
    public interface IGEEnterprisesAS : IBaseAppService<GEEnterprises>
    {
        Task<IEnumerable<GEEnterprises>> SyncDataERP(string pEstablishmentKey, GEEnterprises[] pRegisters);
        Task<GEEnterprisesVM> CreateEnterprise(GEEnterprisesVM model);
        Task<GEEnterprisesVM> UpdateEnterprise(GEEnterprisesVM model);
        Task<GEEnterprisesVM> UpdateToClient(string pUniqueKey, long pId, GEEnterprisesVM pGEEnterprisesVM);
        Task<GEEnterprisesVM> SearchEnterpriseAsync(string pEstablishmentKey, string pCNPJ);
        Task<PagedResult<GEEnterprisesVM>> GetEnterprise(string pEstablishmentKey, long pId);
        Task<bool> EnterpriseExists(string pEstablishmentKey, string pCNPJ, EEnterpriseClassification classification, long idActual);
    }
}
