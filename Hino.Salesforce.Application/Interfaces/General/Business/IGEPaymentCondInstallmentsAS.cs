using Hino.Salesforce.Infra.Cross.Entities.General.Business;

namespace Hino.Salesforce.Application.Interfaces.General.Business
{
    public interface IGEPaymentCondInstallmentsAS : IBaseAppService<GEPaymentCondInstallments>
    {
    }
}
