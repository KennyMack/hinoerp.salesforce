using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.General.Business
{
    public interface IGEPaymentConditionAS : IBaseAppService<GEPaymentCondition>
    {
        Task<GEPaymentCondition> ChangeAsync(GEPaymentCondition model);
    }
}
