using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.General.Business
{
    public interface IGEUserEnterprisesAS : IBaseAppService<GEUserEnterprises>
    {
        Task<GEUserEnterprises> CreateOrUpdateAsync(GEUserEnterprisesVM enterprise);
        Task<GEUserEnterprises[]> CreateOrUpdateListAsync(string pEstablishmentKey, GEUserEnterprisesVM[] enterprise);
        Task<GEUserEnterprises[]> RemoveListAsync(string pEstablishmentKey, GEUserEnterprisesVM[] enterprise);
        Task<PagedResult<GEUserEnterprises>> QuerySearchPagedAsync(int page, int pageSize, Expression<Func<GEUserEnterprises, bool>> predicate, params Expression<Func<GEUserEnterprises, object>>[] includeProperties);
    }
}
