using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Application.Interfaces;
using System.Threading.Tasks;
using Hino.Salesforce.Application.ViewModels.General.Business;

namespace Hino.Salesforce.Application.Interfaces.General.Business
{
    public interface IGEUserPayTypeAS : IBaseAppService<GEUserPayType>
    {
        Task<GEUserPayType> CreateOrUpdateAsync(GEUserPayTypeVM paymentType);
        Task<GEUserPayType[]> CreateOrUpdateListAsync(string pEstablishmentKey, GEUserPayTypeVM[] paymentType);
        Task<GEUserPayType[]> RemoveListAsync(string pEstablishmentKey, GEUserPayTypeVM[] paymentType);
    }
}
