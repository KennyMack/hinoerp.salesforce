using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;

namespace Hino.Salesforce.Application.Interfaces.General.Demograph
{
    public interface IGECitiesAS : IBaseAppService<GECities>
    {
    }
}
