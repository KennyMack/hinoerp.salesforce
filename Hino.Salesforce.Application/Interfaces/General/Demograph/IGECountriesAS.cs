﻿using Hino.Salesforce.Application.ViewModels.General.Demograph;
using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.General.Demograph
{
    public interface IGECountriesAS : IBaseAppService<GECountries>
    {
        Task LoadCountriesAsync(GECountriesLoadVM[] pLoad);
    }
}
