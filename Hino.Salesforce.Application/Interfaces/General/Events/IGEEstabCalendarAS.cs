using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Application.Interfaces;

namespace Hino.Salesforce.Application.Interfaces.General.Events
{
    public interface IGEEstabCalendarAS : IBaseAppService<GEEstabCalendar>
    {
    }
}
