using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Application.Interfaces;
using System.Threading.Tasks;
using Hino.Salesforce.Application.ViewModels.General.Events;

namespace Hino.Salesforce.Application.Interfaces.General.Events
{
    public interface IGEEventsAS : IBaseAppService<GEEvents>
    {
        Task<GEEventsVM> CreateEventAsync(string pEstablishmentKey, GECreateEventsVM pGEEventsVM);
        Task<GEEventsVM> UpdateEventAsync(string pEstablishmentKey, GECreateEventsVM pGEEventsVM);
        Task<GEEventsVM> RemoveEventIdAsync(long pId, string pEstablishmentKey, string pUniqueKey);
        Task<GEEventsVM> ChangeEventStatusAsync(long pId, string pEstablishmentKey, string pUniqueKey, GEEventsStatusVM pEventStatus);
        Task<GEEventsVM> GenerateEventChildAsync(long pId, string pEstablishmentKey, string pUniqueKey, GEEventsChildVM pEventStatus);
    }
}
