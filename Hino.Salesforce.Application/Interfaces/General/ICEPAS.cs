﻿using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.General
{
    public interface ICEPAS
    {
        List<ModelException> Errors { get; set; }
        Task<CEPVM> GetCEPAsync(string pCEP);
        Task<CEPVM> GetCEPAddressAsync(string pCEP, string pNumber, string pCountry, bool pAllInfo);
        Task<CEPVM> GetCEPAddressEnterpriseAsync(ReceitaWSEnterpriseVM pEnterprise);
    }
}
