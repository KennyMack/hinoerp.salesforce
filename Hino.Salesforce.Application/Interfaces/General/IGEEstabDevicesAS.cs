using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.General;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.General
{
    public interface IGEEstabDevicesAS : IBaseAppService<GEEstabDevices>
    {
        Task<GEEstabDevicesVM> CreateDeviceAsync(GEEstabDevicesVM model);
    }
}
