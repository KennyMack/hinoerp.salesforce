using Hino.Salesforce.Infra.Cross.Entities.General;

namespace Hino.Salesforce.Application.Interfaces.General
{
    public interface IGEEstabMenuAS : IBaseAppService<GEEstabMenu>
    {
    }
}
