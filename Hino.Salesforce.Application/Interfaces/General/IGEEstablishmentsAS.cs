using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.General;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.General
{
    public interface IGEEstablishmentsAS : IBaseAppService<GEEstablishments>
    {
        Task<GEEstablishments> CreateEstablishmentAsync(GEEstablishmentsCreateVM pEstab);
        Task<GEEstablishments> UpdateEstablishmentAsync(GEEstablishmentsCreateVM pEstab);
        Task<bool> HasEstablishmentDependencyAsync(string pEstablishmentKey);
        Task<bool> ExistsEstablishmentAsync(string pEstablishmentKey);
        Task<IEnumerable<GEEstablishments>> GetByIdAndEstablishmentKeyAsync(long pId, string pEstablishmentKey);
        Task<GEEstablishments> GetByEstablishmentKeyAsync(string pEstablishmentKey);
        Task<IEnumerable<GEEstabDevices>> GetDevicesEstablishment(string pEstablishmentKey, string pUniqueKey, long pId);
    }
}
