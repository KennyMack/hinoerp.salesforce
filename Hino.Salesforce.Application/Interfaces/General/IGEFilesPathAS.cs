using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Files;
using System.Threading.Tasks;
using System.Web;

namespace Hino.Salesforce.Application.Interfaces.General
{
    public interface IGEFilesPathAS : IBaseAppService<GEFilesPath>
    {
        Task<GEResultUploadFileVM> UploadFilesAsync(UploadDestinationFolder pFilePath, HttpFileCollection pFiles, BaseVM pModel);
        Task<GEResultUploadFileVM> UploadFilesToProductAsync(HttpFileCollection pFiles, GEProductsVM pProduct);
        Task<GEResulUploadItemVM> DeleteFileAsync(UploadDestinationFolder pFilePath, string pEstablishmentKey, string pUniqueKey, long id, GEResulUploadItemVM pFileToRemove);
        Task<GEResulUploadItemVM> DeleteFileOfProductAsync(long id, string pEstablishmentKey, string pUniqueKey, GEResulUploadItemVM pFileToRemove);
        Task<string> DownloadFileAsync(string pEstablishmentKey, string pFilePath);
    }
}
