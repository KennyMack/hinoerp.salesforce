using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.General;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.General
{
    public interface IGEProductsAS : IBaseAppService<GEProducts>
    {
        Task<IEnumerable<GEProductsVM>> SyncDataProd(string pEstablishmentKey, GEProductsVM[] pRegisters);
        Task<IEnumerable<GEProducts>> SyncStockBalance(string pEstablishmentKey, GEProducts[] pRegisters);
        Task<bool> ExistsProductKey(string pEstablishmentKey, string pProductKey);
        Task<GEProductsVM> CreateProduct(GEProductsVM pProduct);
        Task<GEProductsVM> UpdateProduct(GEProductsVM pProduct);
        Task<GEGenerateProductKeyVM> GenerateProductKeyAsync(GEGenerateProductKeyVM pProduct);
    }
}
