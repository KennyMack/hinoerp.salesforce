using Hino.Salesforce.Application.ViewModels.Auth;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.General;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.General
{
    public interface IGEUsersAS : IBaseAppService<GEUsers>
    {
        Task<GEUsers> GetByUsernameAsync(string username, string pEstablishmentKey, params Expression<Func<GEUsers, object>>[] includeProperties);
        Task<GEUsers> GetByEmailAsync(string email, string pEstablishmentKey, params Expression<Func<GEUsers, object>>[] includeProperties);
        Task<GEUsersVM> GetUserById(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<GEUsers, object>>[] includeProperties);
        Task<GEUsers> CreateAdminUserAsync(string pHinoId, GEUsersVM pGEUsersVM);
        Task<GEUsersVM> CreateUserAsync(GEUsersCreateVM pGEUsersVM);
        Task<GEUsersVM> ValidateAuthenticationAsync(LoginVM model, bool pHinoLogin);
        Task<GEUsersVM> ValidateRefreshAsync(LoginVM model, bool pHinoLogin);
        GEUsersVM Me(string pSessionID);
        Task<GEUsers> UpdateUser(GEUsers model);
        Task<GEUsersPasswordVM> ChangePassword(GEUsersPasswordVM model);
    }
}
