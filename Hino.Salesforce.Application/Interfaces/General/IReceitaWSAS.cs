﻿using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.General
{
    public interface IReceitaWSAS
    {
        List<ModelException> Errors { get; set; }
        Task<ReceitaWSEnterpriseVM> GetEnterpriseAsync(string pCNPJ, string pTokenCNPJ);
    }
}
