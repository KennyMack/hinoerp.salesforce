using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;

namespace Hino.Salesforce.Application.Interfaces.General.Kanban
{
    public interface IGEBoardUsersAS : IBaseAppService<GEBoardUsers>
    {
    }
}
