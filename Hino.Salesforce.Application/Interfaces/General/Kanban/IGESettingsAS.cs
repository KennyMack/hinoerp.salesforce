using Hino.Salesforce.Application.ViewModels.General.Kanban;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.General.Kanban
{
    public interface IGESettingsAS : IBaseAppService<GESettings>
    {
        Task<GESettingsVM> CreateOrChangeSettingAsync(GESettingsVM pSettings);
        Task<GESettingsVM> GetByEstablishmentIdAsync(string pEstablishmentKey, long pId);
    }
}
