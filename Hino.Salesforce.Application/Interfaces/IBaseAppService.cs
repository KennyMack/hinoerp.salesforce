﻿using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces
{
    public interface IBaseAppService<T> where T : class
    {
        bool DontSendToQueue { get; set; }
        List<ModelException> Errors { get; set; }
        Task<IEnumerable<T>> SyncData(string pEstablishmentKey, T[] pRegisters);
        T Add(T model);
        int CountResults(Expression<Func<T, bool>> predicate);
        Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties);
        T GetById(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties);
        T GetByIdToUpdate(long id, string pEstablishmentKey, string pUniqueKey);
        Task<T> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        T Update(T model);
        void Remove(T model);
        Task<T> RemoveById(long id, string pEstablishmentKey, string pUniqueKey);
        long NextSequence();
        Task<long> NextSequenceAsync();
        Task<int> SaveChanges();
        void RollBackChanges();
        void Dispose();

    }
}
