using Hino.Salesforce.Application.ViewModels.Route;
using Hino.Salesforce.Infra.Cross.Entities.Logistics;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.Logistics
{
    public interface ILORoutesAS : IBaseAppService<LORoutes>
    {
        Task<MapVM> GetMapJsonAsync(string pEstablishmentKey, long pId);
    }
}
