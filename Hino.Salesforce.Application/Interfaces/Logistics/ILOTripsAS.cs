using Hino.Salesforce.Infra.Cross.Entities.Logistics;

namespace Hino.Salesforce.Application.Interfaces.Logistics
{
    public interface ILOTripsAS : IBaseAppService<LOTrips>
    {
    }
}
