using Hino.Salesforce.Infra.Cross.Entities.Logistics;

namespace Hino.Salesforce.Application.Interfaces.Logistics
{
    public interface ILOWaypointsAS : IBaseAppService<LOWaypoints>
    {
    }
}
