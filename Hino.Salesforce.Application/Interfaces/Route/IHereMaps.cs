﻿using Hino.Salesforce.Application.ViewModels.Route;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.Route
{
    public interface IHereMaps
    {
        List<ModelException> Errors { get; set; }
        Task<HereMapsResultVM> GetAddressAsync(AddressGeoVM pAddress);
        Task<HereFindSequenceResultVM> FindSequenceAsync(OptimizedVM pLocations);
    }
}
