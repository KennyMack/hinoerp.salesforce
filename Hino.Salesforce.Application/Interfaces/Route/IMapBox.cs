﻿using Hino.Salesforce.Application.ViewModels.Route;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.Route
{
    public interface IMapBox
    {
        List<ModelException> Errors { get; set; }
        Task<MapBoxResultVM> GetAddressAsync(AddressGeoVM pAddress);
        Task<OptimizedTripResultVM> OptimizedLocationsAsync(OptimizedVM pLocations);

    }
}
