﻿using Hino.Salesforce.Application.ViewModels.Route;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.Route
{
    public interface IRouteXL
    {
        List<ModelException> Errors { get; set; }
        string BaseRoute();
        string GetAuthentication(string pUser, string pPassword);
        Task<bool> GetStatus();
        Task<TourVM> PostTour();
    }
}
