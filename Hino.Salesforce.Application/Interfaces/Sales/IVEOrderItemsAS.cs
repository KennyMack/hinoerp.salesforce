using Hino.Salesforce.Infra.Cross.Entities.Sales;
using System.Collections.Generic;

namespace Hino.Salesforce.Application.Interfaces.Sales
{
    public interface IVEOrderItemsAS : IBaseAppService<VEOrderItems>
    {
        IEnumerable<VEOrderItems> GetItemsByOrderId(long OrderId, string pEstablishmentKey);
    }
}
