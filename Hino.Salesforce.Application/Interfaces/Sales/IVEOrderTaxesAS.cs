using Hino.Salesforce.Infra.Cross.Entities.Sales;

namespace Hino.Salesforce.Application.Interfaces.Sales
{
    public interface IVEOrderTaxesAS : IBaseAppService<VEOrderTaxes>
    {
    }
}
