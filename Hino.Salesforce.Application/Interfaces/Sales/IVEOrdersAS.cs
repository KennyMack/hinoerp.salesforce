using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.Sales
{
    public interface IVEOrdersAS : IBaseAppService<VEOrders>
    {
        Task<VEOrdersVM> CreateOrder(VEOrdersVM pOrder);
        Task<VEOrdersVM> UpdateOrder(VEOrdersVM pOrder);
        Task<PagedResult<VEOrdersVM>> CanConvertToOrderAsync(string pEstablishmentKey, long id);
        Task<VEOrdersVM> GenerateRevisionAsync(VEOrderStatusVM pOrder);
        Task<VEOrdersVM> CopyOrderAsync(VEOrderStatusVM pOrder);
        Task<IEnumerable<VEOrders>> GetOrdersLinkedAsync(string pEstablishmentKey, long id);
        Task<IEnumerable<VEOrders>> GetOrderDirectLinkedAsync(string pEstablishmentKey, long id);
        Task<VEOrdersVM> ChangeStatusAsync(VEOrderStatusVM pOrder);
        Task<IEnumerable<VEOrders>> SyncERPData(string pEstablishmentKey, VEOrders[] pVEOrders);
        Task<PagedResult<CommissionByProductVM>> GetCommissionByProduct(int pageNumber, int limitNumber, string pEstablishmentKey, long userId, DateTime initialDate, DateTime finalDate, String filter);
        Task<TotalCommissionVM> GetTotalCommission(string pEstablishmentKey, long userId, DateTime initialDate, DateTime finalDate);
        Task<PagedResult<SalesByProductVM>> GetSalesByProduct(int pageNumber, int limitNumber, string pEstablishmentKey, long userId, DateTime initialDate, DateTime finalDate, String filter);
        Task<ResumePagedResult<DetailedSalesVM>> GetDetailedSales(int pageNumber, int limitNumber, string pEstablishmentKey, long userId, DateTime initialDate, DateTime finalDate, String filter);
        Task<ResumePagedResult<DetailedSalesVM>> GetDetailedCommission(int pageNumber, int limitNumber, string pEstablishmentKey, long userId, DateTime initialDate, DateTime finalDate, String filter);
        Task<ResumePagedResult<VEOrdersVM>> GetSalesLimit(int pageNumber, int limitNumber, string pEstablishmentKey, long pUserId, String filter);
        Task<PagedResult<SalesByMonthVM>> GetSalesByMonth(int pageNumber, int limitNumber, string pEstablishmentKey, long userId, DateTime initialDate, DateTime finalDate, String filter);
        Task<TotalSalesVM> GetTotalSales(string pEstablishmentKey, long userId, DateTime initialDate, DateTime finalDate);
        Task<IEnumerable<VEOrderStockBalanceVM>> GetOrderStockBalanceAsync(string pEstablishmentKey, string pUniqueKey, long id);
        Task<IEnumerable<VEOrders>> GetOrdersLinkedTreeAsync(string pEstablishmentKey, long id);
        Task<PagedResult<VEOrders>> QueryViewAsync(int page, int pageSize, Expression<Func<VEOrders, bool>> predicate);
    }
}
