﻿using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.Sales
{
    public interface IVERegionSaleAS : IBaseAppService<VERegionSale>
    {
    }
}
