using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.Sales
{
    public interface IVERegionSaleUFAS : IBaseAppService<VERegionSaleUF>
    {
        Task UpdateEnterpriseRegionId(string pEstablishmentKey, string pUF, long pRegionId);
        Task<bool> ExistsUFRegion(string pEstablishmentKey, string pUF);
        Task<VERegionSaleUFVM> CreateSalePrice(VERegionSaleUFVM pRegionSale);
        Task<VERegionSaleUFVM> UpdateSalePrice(VERegionSaleUFVM pRegionSale);
    }
}
