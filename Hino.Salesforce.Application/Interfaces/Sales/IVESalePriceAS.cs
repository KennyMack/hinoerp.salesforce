﻿using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Collections;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.Sales
{
    public interface IVESalePriceAS : IBaseAppService<VESalePrice>
    {
        Task<VESalePriceVM> CreateSalePrice(VESalePriceVM pSalePrice);
        Task<VESalePriceVM> UpdateSalePrice(VESalePriceVM pSalePrice);
        Task<PagedResult<VESalePriceVM>> GetAllSalePrice(int pageSize, int limit, string pEstablishmentKey, long pRegionId, long pCodPrVenda, long pPaymentConditionId, long pUserId, string filter, string searchPosition);
        Task<PagedResult<VESalePriceVM>> GetProductSalePrice(string pEstablishmentKey, long pProductId, long pRegionId, long pCodPrVenda, long pPaymentConditionId, long pUserId);
        Task<PagedResult<VESalePrice>> GetListCodPRVendaAsync(string pEstablishmentKey);
        Task GenerateEntryQueueAsync();
    }
}
