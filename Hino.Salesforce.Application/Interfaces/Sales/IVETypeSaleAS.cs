using Hino.Salesforce.Infra.Cross.Entities.Sales;

namespace Hino.Salesforce.Application.Interfaces.Sales
{
    public interface IVETypeSaleAS : IBaseAppService<VETypeSale>
    {
    }
}
