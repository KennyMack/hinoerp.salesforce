using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.Sales
{
    public interface IVEUserRegionAS : IBaseAppService<VEUserRegion>
    {
        Task<VEUserRegion> CreateOrUpdateAsync(VEUserRegionVM userRegion);
        Task<VEUserRegion[]> CreateOrUpdateListAsync(string pEstablishmentKey, VEUserRegionVM[] userRegion);
        Task<VEUserRegion[]> RemoveListAsync(string pEstablishmentKey, VEUserRegionVM[] userRegion);
        bool ZipCodeExists(string pEstablishmentKey, long pUserId, string pZipCode);
    }
}
