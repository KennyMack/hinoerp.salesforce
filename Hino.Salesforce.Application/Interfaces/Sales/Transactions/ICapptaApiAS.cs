﻿using Hino.Salesforce.Application.ViewModels.Sales.Transactions;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.Sales.Transactions
{
    public interface ICapptaApiAS
    {
        List<ModelException> Errors { get; set; }
        Task<CapptaApiVM> GetTransactionAsync(String authCode);
        Task<Boolean> Authenticate();
    }
}
