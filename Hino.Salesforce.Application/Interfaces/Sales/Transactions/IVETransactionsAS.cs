using Hino.Salesforce.Application.ViewModels.Sales.Transactions;
using Hino.Salesforce.Infra.Cross.Entities.Sales.Transactions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Interfaces.Sales.Transactions
{
    public interface IVETransactionsAS : IBaseAppService<VETransactions>
    {
        Task<VETransactionsVM> CreateTransaction(VETransactionsVM pTransaction);
        Task<VETransactionsVM> UpdateTransaction(VETransactionsVM pTransaction);
        Task<VETransactionsVM> SearchTransationByNSUAsync(string pEstablishmentKey, string NSU);
    }
}
