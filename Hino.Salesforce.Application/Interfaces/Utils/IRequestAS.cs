﻿using RestSharp;

namespace Hino.Salesforce.Application.Interfaces.Utils
{
    public interface IRequestAS
    {
        RestClient CreateClient();
        RestRequest CreateRequest(string pUri, Method pMethod);
    }
}
