﻿using Hino.Salesforce.Application.Interfaces.Auth;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Cache.Auth;
using Hino.Salesforce.Infra.Cross.Identity.Configurations;
using Hino.Salesforce.Infra.Cross.Identity.Token;
using Hino.Salesforce.Infra.Cross.Utils.Settings;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Providers
{
    public class AuthorizationTokenProvider : OAuthAuthorizationServerProvider
    {
        private string EstablishmentKey { get; set; }
        private string UserEmail { get; set; }
        private string UserName { get; set; }
        private string UserPass { get; set; }
        private string OriginLogin { get; set; }
        private string GrantType { get; set; }
        private string RefreshTokenId { get; set; }
        private GEUsersVM _GEUsers;
        private bool HinoLogin { get; set; }

        private readonly IGEUsersAS _IGEUsersAS;
        private readonly IGEEstablishmentsAS _IGEEstablishmentsAS;
        private readonly IAURefreshTokenAS _IAURefreshTokenAS;
        private readonly SessionTokenCache _SessionTokenCache;
        private readonly SigningConfigurations _SigningConfigurations;
        private readonly TokenConfigurations _TokenConfigurations;

        public AuthorizationTokenProvider(IGEUsersAS pIGEUsersAS,
            IGEEstablishmentsAS pIGEEstablishmentsAS,
            IAURefreshTokenAS pIAURefreshTokenAS,
            SessionTokenCache pSessionTokenCache,
            SigningConfigurations pSigningConfigurations,
            TokenConfigurations pTokenConfigurations
            )
        {
            _IGEUsersAS = pIGEUsersAS;
            _IGEEstablishmentsAS = pIGEEstablishmentsAS;
            _SessionTokenCache = pSessionTokenCache;
            _IAURefreshTokenAS = pIAURefreshTokenAS;
            _SigningConfigurations = pSigningConfigurations;
            _TokenConfigurations = pTokenConfigurations;
        }

        public override Task MatchEndpoint(OAuthMatchEndpointContext context)
        {
            string[] domains;
            if (context.Response.Headers.TryGetValue("Access-Control-Allow-Origin", out domains))
                context.Response.Headers.SetValues("Access-Control-Allow-Origin", new string[] { "*" });
            else
                context.Response.Headers.Add("Access-Control-Allow-Origin", new string[] { "*" });

            if (context.Response.Headers.TryGetValue("Access-Control-Allow-Headers", out domains))
                context.Response.Headers.SetValues("Access-Control-Allow-Headers",
                    new string[] { "Authorization", "Content-Type", "Refresh_Token", "EstablishmentKey" });
            else
                context.Response.Headers.Add("Access-Control-Allow-Headers",
                    new string[] { "Authorization", "Content-Type", "Refresh_Token", "EstablishmentKey" });

            if (context.Response.Headers.TryGetValue("Access-Control-Allow-Methods", out domains))
                context.Response.Headers.SetValues("Access-Control-Allow-Methods",
                    new string[] { "OPTIONS", "POST", "GET", "PUT", "DELETE" });
            else
                context.Response.Headers.Add("Access-Control-Allow-Methods",
                    new string[] { "OPTIONS", "POST", "GET", "PUT", "DELETE" });


            return base.MatchEndpoint(context);
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {

            foreach (KeyValuePair<string, string> property in context.Properties.Dictionary)
            {
                context.AdditionalResponseParameters.Add(property.Key, property.Value);
            }

            return Task.FromResult<object>(null);
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            _IGEUsersAS.Errors.Clear();
            try
            {
                EstablishmentKey = context.Parameters.Get("establishmentkey");
                if (string.IsNullOrEmpty(EstablishmentKey) | string.IsNullOrWhiteSpace(EstablishmentKey))
                    throw new Exception();

                var HinoId = "NOFUNFOU";
                try
                {
                    HinoId = AppSettings.Hino_Id;// ConfigurationManager.AppSettings["Hino_Id"].ToString();
                }
                catch (Exception)
                {
                    HinoId = "NOFUNFOU";
                }

                HinoLogin = false;

                if (EstablishmentKey != HinoId)
                {

                    var teste = await _IGEEstablishmentsAS.QueryAsync(r => r.EstablishmentKey == EstablishmentKey);

                    if (!teste.Any())
                        throw new Exception("Login não permitido, Estabelecimento não encontrado.");
                }
                else
                {
                    HinoLogin = true;
                    EstablishmentKey = HinoId;
                }
            }
            catch (Exception e)
            {
                Hino.Salesforce.Infra.Cross.Utils.Exceptions.Logging.Exception(e);
                context.Rejected();
                context.SetError("Invalid_EstablishmentKey", (AppSettings.IsDebug) ? e.Message :
                    Infra.Cross.Resources.ErrorMessagesResource.InvalidEstablishmentKeyOrNull);

                return;
            }


            try
            {
                OriginLogin = context.Parameters.Get("origin_login");
                if (string.IsNullOrEmpty(OriginLogin) | string.IsNullOrWhiteSpace(OriginLogin))
                    throw new Exception("Origem do login não informada.");
            }
            catch (Exception e)
            {
                Hino.Salesforce.Infra.Cross.Utils.Exceptions.Logging.Exception(e);
                context.Rejected();
                context.SetError("Origin_Login", (AppSettings.IsDebug) ? e.Message : Infra.Cross.Resources.ErrorMessagesResource.OriginLoginInvalidOrNull);

                return;
            }

            try
            {
                GrantType = context.Parameters.Get("grant_type");
                if (string.IsNullOrEmpty(GrantType) | string.IsNullOrWhiteSpace(GrantType))
                    throw new Exception("Tipo da liberação não informado");
            }
            catch (Exception e)
            {
                Hino.Salesforce.Infra.Cross.Utils.Exceptions.Logging.Exception(e);
                context.Rejected();
                context.SetError("grant_type", (AppSettings.IsDebug) ? e.Message : Infra.Cross.Resources.ErrorMessagesResource.InvalidGrantType);

                return;
            }

            if (GrantType == "password")
            {

                try
                {
                    UserEmail = context.Parameters.Get("email");
                }
                catch (Exception)
                {

                }

                try
                {
                    UserName = context.Parameters.Get("username");
                }
                catch (Exception)
                {

                }

                try
                {
                    UserPass = context.Parameters.Get("password");
                }
                catch (Exception)
                {

                }

                var LoginUser = new ViewModels.Auth.LoginVM
                {
                    UserName = UserName,
                    Email = UserEmail,
                    Password = UserPass,
                    EstablishmentKey = EstablishmentKey,
                    OriginLogin = OriginLogin
                };

                _GEUsers = await _IGEUsersAS.ValidateAuthenticationAsync(LoginUser, HinoLogin);

                if (_GEUsers == null)
                {
                    context.Rejected();
                    StringBuilder st = new StringBuilder();
                    _IGEUsersAS.Errors.ForEach(r =>
                    {
                        st.Append(string.Join(", ", r.Messages));
                    });

                    context.SetError("invalid_credentials", st.ToString());

                    return;
                }

                context.Validated();
            }
            else if (GrantType == "refresh_token")
            {
                try
                {
                    RefreshTokenId = context.Parameters.Get("refresh_token");
                    if (string.IsNullOrEmpty(RefreshTokenId) | string.IsNullOrWhiteSpace(RefreshTokenId))
                        throw new Exception("Refresh token não informado");
                }
                catch (Exception e)
                {
                    Hino.Salesforce.Infra.Cross.Utils.Exceptions.Logging.Exception(e);
                    context.Rejected();
                    context.SetError("refresh_token", (AppSettings.IsDebug) ? e.Message : Infra.Cross.Resources.ErrorMessagesResource.InvalidRefreshToken);

                    return;
                }

                var LoginUser = new ViewModels.Auth.LoginVM
                {
                    RefreshToken = RefreshTokenId,
                    EstablishmentKey = EstablishmentKey,
                    OriginLogin = OriginLogin
                };

                _GEUsers = await _IGEUsersAS.ValidateRefreshAsync(LoginUser, HinoLogin);

                if (_GEUsers == null)
                {
                    context.Rejected();
                    StringBuilder st = new StringBuilder();
                    _IGEUsersAS.Errors.ForEach(r =>
                    {
                        st.Append(string.Join(", ", r.Messages));
                    });

                    context.SetError("invalid_credentials", st.ToString());

                    return;
                }

                context.Validated();
            }
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            if (_GEUsers != null)
            {
                var PID = Process.GetCurrentProcess().Id.ToString();
                var SessionId = string.Concat(_GEUsers.EstablishmentKey, "$", Guid.NewGuid().ToString("N"), "$", _GEUsers.UserKey);

                var claims = new List<Claim>()
                {
                    new Claim(ClaimTypes.Sid, Guid.NewGuid().ToString("N")),
                    new Claim(TokenClaim.PID, PID),
                    new Claim(TokenClaim.SessionId, SessionId),
                    new Claim(ClaimTypes.Name, _GEUsers.UserName),
                    new Claim(ClaimTypes.Email, _GEUsers.Email),
                    new Claim("UserType", ((int)_GEUsers.UserType).ToString()),
                    new Claim("UserId", _GEUsers.Id.ToString()),
                };

                var Roles = new String[] { "Admin", "User" };
                foreach (var role in Roles)
                    claims.Add(new Claim(ClaimTypes.Role, role));

                var data = new Dictionary<string, string>
                {
                    { "EstablishmentKey", _GEUsers.EstablishmentKey },
                    { "UserID", _GEUsers.Id.ToString() },
                    { "UserKey", _GEUsers.UserKey },
                    { "UniqueKey", _GEUsers.UniqueKey },
                    { TokenClaim.SessionId, SessionId },
                    { "roles", string.Join(",", Roles)}
                };

                var properties = new AuthenticationProperties(data);

                ClaimsIdentity oAuthIdentity = new ClaimsIdentity(claims, "Bearer");

                var ticket = new AuthenticationTicket(oAuthIdentity, properties);

                TimeSpan finalExpiration = TimeSpan.FromSeconds(_TokenConfigurations.FinalExpiration);
                _GEUsers.LastLogin = DateTime.Now;

                _SessionTokenCache.Set(SessionId,
                    JsonConvert.SerializeObject(
                        new
                        {
                            _GEUsers.EstablishmentKey,
                            _GEUsers.UserName,
                            _GEUsers.Email,
                            _GEUsers.LastLogin,
                            _GEUsers.UserType,
                            _GEUsers.Name,
                            _GEUsers.IsActive,
                            _GEUsers.UniqueKey,
                            _GEUsers.UserKey,
                            _GEUsers.Id,
                            _GEUsers.PercCommission,
                            _GEUsers.PercDiscount,
                            _GEUsers.PercDiscountPrice,
                            _GEUsers.PercIncreasePrice,
                            _GEUsers.StoreId,
                            _GEUsers.TerminalId,
                            _GEUsers.IsBlockedByPay,
                            _GEUsers.GEEstablishments
                        }), finalExpiration);

                context.Validated(ticket);
            }
            else
            {
                context.Rejected();
                context.SetError("invalid_credentials", Infra.Cross.Resources.ValidationMessagesResource.AuthUserAuthFail);
                return;
            }

            await Task.FromResult<object>(null);
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            var newIdentity = new ClaimsIdentity(context.Ticket.Identity);

            var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);

            context.Validated(newTicket);

            return Task.FromResult<object>(null);
        }
    }
}
