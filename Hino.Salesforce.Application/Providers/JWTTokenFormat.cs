﻿using Hino.Salesforce.Application.Interfaces.Auth;
using Hino.Salesforce.Infra.Cross.Cache.Auth;
using Hino.Salesforce.Infra.Cross.Identity.Configurations;
using Microsoft.Owin.Security;
using System;

namespace Hino.Salesforce.Application.Providers
{
    public class JWTTokenFormat : ISecureDataFormat<AuthenticationTicket>
    {
        private readonly IAURefreshTokenAS _IAURefreshTokenAS;
        private readonly SessionTokenCache _SessionTokenCache;
        private readonly SigningConfigurations _SigningConfigurations;
        private readonly TokenConfigurations _TokenConfigurations;

        public JWTTokenFormat(
            IAURefreshTokenAS pIAURefreshTokenAS,
            SessionTokenCache pSessionTokenCache,
            SigningConfigurations pSigningConfigurations,
            TokenConfigurations pTokenConfigurations
            )
        {
            _SessionTokenCache = pSessionTokenCache;
            _IAURefreshTokenAS = pIAURefreshTokenAS;
            _SigningConfigurations = pSigningConfigurations;
            _TokenConfigurations = pTokenConfigurations;
        }

        public string Protect(AuthenticationTicket data)
        {
            try
            {

                data.Properties.Dictionary.TryGetValue("tokenvalue", out string token);
                data.Properties.Dictionary.Remove("tokenvalue");

                return token;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public AuthenticationTicket Unprotect(string protectedText)
        {
            throw new NotImplementedException();
        }
    }
}
