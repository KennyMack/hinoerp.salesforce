﻿using Hino.Salesforce.Application.Interfaces.Auth;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Infra.Cross.Cache.Auth;
using Hino.Salesforce.Infra.Cross.Identity.Configurations;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Infrastructure;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Providers
{
    public class OAuthBearerRefreshTokenProvider : IAuthenticationTokenProvider
    {

        private readonly IGEUsersAS _IGEUsersAS;
        private readonly IGEEstablishmentsAS _IGEEstablishmentsAS;
        private readonly IAURefreshTokenAS _IAURefreshTokenAS;
        private readonly SessionTokenCache _SessionTokenCache;
        private readonly SigningConfigurations _SigningConfigurations;
        private readonly TokenConfigurations _TokenConfigurations;

        public OAuthBearerRefreshTokenProvider(IGEUsersAS pIGEUsersAS,
            IGEEstablishmentsAS pIGEEstablishmentsAS,
            IAURefreshTokenAS pIAURefreshTokenAS,
            SessionTokenCache pSessionTokenCache,
            SigningConfigurations pSigningConfigurations,
            TokenConfigurations pTokenConfigurations
            )
        {
            _IGEUsersAS = pIGEUsersAS;
            _IGEEstablishmentsAS = pIGEEstablishmentsAS;
            _SessionTokenCache = pSessionTokenCache;
            _IAURefreshTokenAS = pIAURefreshTokenAS;
            _SigningConfigurations = pSigningConfigurations;
            _TokenConfigurations = pTokenConfigurations;
        }

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            var refreshToken = Guid.NewGuid().ToString("N");

            var refreshTokenProperties = new AuthenticationProperties(context.Ticket.Properties.Dictionary)
            {
                IssuedUtc = context.Ticket.Properties.IssuedUtc,
                ExpiresUtc = DateTime.UtcNow.AddDays(30)
            };
            var refreshTokenTicket = new AuthenticationTicket(context.Ticket.Identity, refreshTokenProperties);

            var serializedTicket = context.SerializeTicket();
            var SessionId = context.Ticket.Properties.Dictionary["SessionId"];

            _IAURefreshTokenAS.Add(
                new Infra.Cross.Entities.Auth.AURefreshToken
                {
                    EstablishmentKey = context.Ticket.Properties.Dictionary["EstablishmentKey"],
                    UserID = Convert.ToInt32(context.Ticket.Properties.Dictionary["UserID"]),
                    UserKey = context.Ticket.Properties.Dictionary["UserKey"],
                    SessionID = SessionId,
                    RefreshTokenID = refreshToken,
                    ProtectedTicket = serializedTicket
                }
            );

            await _IAURefreshTokenAS.SaveChanges();

            try
            {
                var data = _SessionTokenCache.Get<string>(SessionId);

                if (string.IsNullOrEmpty(data))
                {
                    TimeSpan finalExpiration = TimeSpan.FromSeconds(_TokenConfigurations.FinalExpiration);

                    var geUsers = await _IGEUsersAS.GetUserById(Convert.ToInt32(context.Ticket.Properties.Dictionary["UserID"]),
                        context.Ticket.Properties.Dictionary["EstablishmentKey"],
                        context.Ticket.Properties.Dictionary["UniqueKey"]);

                    _SessionTokenCache.Set(SessionId,
                        JsonConvert.SerializeObject(
                            new
                            {
                                geUsers.EstablishmentKey,
                                geUsers.UserName,
                                geUsers.Email,
                                geUsers.LastLogin,
                                geUsers.UserType,
                                geUsers.IsActive,
                                geUsers.Name,
                                geUsers.UniqueKey,
                                geUsers.UserKey,
                                geUsers.Id,
                                geUsers.PercCommission,
                                geUsers.PercDiscount,
                                geUsers.PercDiscountPrice,
                                geUsers.PercIncreasePrice,
                                geUsers.StoreId,
                                geUsers.TerminalId,
                                geUsers.IsBlockedByPay,
                                geUsers.GEEstablishments
                            }), finalExpiration);
                }
            }
            catch (Exception e)
            {
                Hino.Salesforce.Infra.Cross.Utils.Exceptions.Logging.Exception(e);
            }

            context.SetToken(refreshToken);
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            this.CreateAsync(context).Wait();
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            try
            {
                var RefreshToken = await _IAURefreshTokenAS.GetByRefreshTokenId(context.Token);

                if (RefreshToken != null)
                {
                    context.DeserializeTicket(RefreshToken.ProtectedTicket);

                    await _IAURefreshTokenAS.RemoveByRefreshTokenId(context.Token);
                }
            }
            catch (Exception e)
            {
                Hino.Salesforce.Infra.Cross.Utils.Exceptions.Logging.Exception(e);
            }
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            this.ReceiveAsync(context).Wait();
        }
    }
}
