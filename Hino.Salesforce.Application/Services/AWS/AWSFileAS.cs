﻿using Amazon;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Hino.Salesforce.Application.Interfaces.AWS;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Hino.Salesforce.Infra.Cross.Utils.Settings;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.AWS
{
    public class AWSFileAS : IAWSFileAS
    {
        public List<ModelException> Errors { get; set; }
        public AWSFileAS()
        {
            Errors = new List<ModelException>();
        }

        #region Convert Region
        public RegionEndpoint ConvertRegion(string pRegion)
        {
            switch (pRegion)
            {
                case "USEast1":
                    return RegionEndpoint.USEast1;
                case "AFSouth1":
                    return RegionEndpoint.AFSouth1;
                case "MESouth1":
                    return RegionEndpoint.MESouth1;
                case "CACentral1":
                    return RegionEndpoint.CACentral1;
                case "CNNorthWest1":
                    return RegionEndpoint.CNNorthWest1;
                case "CNNorth1":
                    return RegionEndpoint.CNNorth1;
                case "USGovCloudWest1":
                    return RegionEndpoint.USGovCloudWest1;
                case "SAEast1":
                    return RegionEndpoint.SAEast1;
                case "APSoutheast2":
                    return RegionEndpoint.APSoutheast2;
                case "APSoutheast1":
                    return RegionEndpoint.APSoutheast1;
                case "APSouth1":
                    return RegionEndpoint.APSouth1;
                case "APNortheast3":
                    return RegionEndpoint.APNortheast3;
                case "USGovCloudEast1":
                    return RegionEndpoint.USGovCloudEast1;
                case "APNortheast1":
                    return RegionEndpoint.APNortheast1;
                case "APNortheast2":
                    return RegionEndpoint.APNortheast2;
                case "USWest1":
                    return RegionEndpoint.USWest1;
                case "USWest2":
                    return RegionEndpoint.USWest2;
                case "EUNorth1":
                    return RegionEndpoint.EUNorth1;
                case "EUWest1":
                    return RegionEndpoint.EUWest1;
                case "USEast2":
                    return RegionEndpoint.USEast2;
                case "EUWest3":
                    return RegionEndpoint.EUWest3;
                case "EUCentral1":
                    return RegionEndpoint.EUCentral1;
                case "EUSouth1":
                    return RegionEndpoint.EUSouth1;
                case "APEast1":
                    return RegionEndpoint.APEast1;
                case "EUWest2":
                    return RegionEndpoint.EUWest2;
            }

            return RegionEndpoint.SAEast1;
        }
        #endregion

        public async Task<bool> UploadFileAsync(string pFileName, string pAWSKey)
        {
            try
            {
                using (AmazonS3Client client = new AmazonS3Client(
                    AppSettings.AWS_AccessKey,
                    AppSettings.AWS_SecretAccess,
                    ConvertRegion(AppSettings.AWS_Region)
                ))
                {
                    // Realizando o upload
                    using (TransferUtility fileTransferUtility = new TransferUtility(client))
                        await fileTransferUtility.UploadAsync(pFileName, AppSettings.AWS_Bucket, pAWSKey);
                }
            }
            catch (AmazonS3Exception awsExc)
            {
                Logging.Exception(awsExc);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.UnhandledException,
                    Field = "AWSUpload",
                    Messages = new[] { awsExc.Message },
                    Value = pAWSKey
                });

                return false;
            }
            catch (Exception exc)
            {
                Logging.Exception(exc);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.UnhandledException,
                    Field = "AWSUpload",
                    Messages = new[] { exc.Message },
                    Value = pAWSKey
                });

                return false;
            }

            return true;
        }

        public async Task<bool> DeleteFileAsync(string pAWSKey)
        {
            try
            {
                using (AmazonS3Client client = new AmazonS3Client(
                    AppSettings.AWS_AccessKey,
                    AppSettings.AWS_SecretAccess,
                    ConvertRegion(AppSettings.AWS_Region)
                ))
                {
                    // Realizando a exclusão
                    var deleteObjectRequest = new DeleteObjectRequest
                    {
                        BucketName = AppSettings.AWS_Bucket,
                        Key = pAWSKey
                    };

                    await client.DeleteObjectAsync(deleteObjectRequest);
                }
            }
            catch (AmazonS3Exception awsExc)
            {
                Logging.Exception(awsExc);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.UnhandledException,
                    Field = "AWSUpload",
                    Messages = new[] { awsExc.Message },
                    Value = pAWSKey
                });

                return false;
            }
            catch (Exception exc)
            {
                Logging.Exception(exc);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.UnhandledException,
                    Field = "AWSUpload",
                    Messages = new[] { exc.Message },
                    Value = pAWSKey
                });

                return false;
            }

            return true;
        }

        public string DownloadFileAsync(string pAWSKey)
        {
            try
            {
                using (AmazonS3Client client = new AmazonS3Client(
                    AppSettings.AWS_AccessKey,
                    AppSettings.AWS_SecretAccess,
                    ConvertRegion(AppSettings.AWS_Region)
                ))
                {
                    return client.GetPreSignedURL(new GetPreSignedUrlRequest
                    {
                        BucketName = AppSettings.AWS_Bucket,
                        Expires = DateTime.Now.AddHours(1),
                        Key = pAWSKey

                    });
                }
            }
            catch (AmazonS3Exception awsExc)
            {
                Logging.Exception(awsExc);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.UnhandledException,
                    Field = "AWSUpload",
                    Messages = new[] { awsExc.Message },
                    Value = pAWSKey
                });

                return "";
            }
            catch (Exception exc)
            {
                Logging.Exception(exc);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.UnhandledException,
                    Field = "AWSUpload",
                    Messages = new[] { exc.Message },
                    Value = pAWSKey
                });

                return "";
            }
        }
    }
}
