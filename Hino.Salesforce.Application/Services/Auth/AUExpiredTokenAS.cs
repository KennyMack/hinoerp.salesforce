using Hino.Salesforce.Application.Interfaces.Auth;
using Hino.Salesforce.Domain.Auth.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Auth;

namespace Hino.Salesforce.Application.Services.Auth
{
    public class AUExpiredTokenAS : BaseAppService<AUExpiredToken>, IAUExpiredTokenAS
    {
        private readonly IAUExpiredTokenService _IAUExpiredTokenService;

        public AUExpiredTokenAS(IAUExpiredTokenService pIAUExpiredTokenService) :
             base(pIAUExpiredTokenService)
        {
            _IAUExpiredTokenService = pIAUExpiredTokenService;
        }
    }
}
