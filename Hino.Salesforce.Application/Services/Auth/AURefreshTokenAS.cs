using Hino.Salesforce.Application.Interfaces.Auth;
using Hino.Salesforce.Domain.Auth.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Auth;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Auth
{
    public class AURefreshTokenAS : BaseAppService<AURefreshToken>, IAURefreshTokenAS
    {
        private readonly IAURefreshTokenService _IAURefreshTokenService;

        public AURefreshTokenAS(IAURefreshTokenService pIAURefreshTokenService) :
             base(pIAURefreshTokenService)
        {
            _IAURefreshTokenService = pIAURefreshTokenService;
        }

        public async Task<AURefreshToken> GetByRefreshTokenId(string pRefreshTokenId, string pEstablishmentKey) =>
            await _IAURefreshTokenService.GetByRefreshTokenId(pRefreshTokenId, pEstablishmentKey);

        public async Task<AURefreshToken> GetByRefreshTokenId(string pRefreshTokenId) =>
            await _IAURefreshTokenService.GetByRefreshTokenId(pRefreshTokenId);

        public async Task RemoveByRefreshTokenId(string pRefreshTokenId) =>
            await _IAURefreshTokenService.RemoveByRefreshTokenId(pRefreshTokenId);
    }
}
