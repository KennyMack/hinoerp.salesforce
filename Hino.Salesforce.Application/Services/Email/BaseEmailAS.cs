﻿using Hino.Salesforce.Application.Interfaces.Email;
using Hino.Salesforce.Domain.Base.Interfaces.Email;
using System;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Email
{
    public class BaseEmailAS<T> : IBaseEmailAS<T>, IDisposable where T : class
    {
        private readonly IBaseEmail<T> _BaseEmail;
        public BaseEmailAS(IBaseEmail<T> pBaseEmail)
        {
            _BaseEmail = pBaseEmail;
        }

        public void Dispose()
        {
            throw new NotImplementedException();
        }

        public virtual async Task<bool> SendCreate(T model, string pFrom, string pTo) =>
            await _BaseEmail.SendCreate(model, pFrom, pTo);


        public virtual async Task<bool> SendRemove(T model, string pFrom, string pTo) =>
            await _BaseEmail.SendCreate(model, pFrom, pTo);

        public virtual async Task<bool> SendUpdate(T model, string pFrom, string pTo) =>
            await _BaseEmail.SendCreate(model, pFrom, pTo);
    }
}
