﻿using Hino.Salesforce.Application.Interfaces.Email.General;
using Hino.Salesforce.Infra.Cross.Email.General;
using Hino.Salesforce.Infra.Cross.Entities.General;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Email.General
{
    public class EstablishmentEmailAS : BaseEmailAS<GEEstablishments>, IEstablishmentEmailAS
    {
        private readonly IEstablishmentEmail _IEstablishmentEmail;
        public EstablishmentEmailAS(IEstablishmentEmail pIEstablishmentEmail) :
            base(pIEstablishmentEmail)
        {
            _IEstablishmentEmail = pIEstablishmentEmail;
        }

        public override async Task<bool> SendCreate(GEEstablishments pGEEstablishments, string pFrom, string pTo) =>
            await _IEstablishmentEmail.SendCreate(pGEEstablishments, pFrom, pTo);

        public override async Task<bool> SendRemove(GEEstablishments pGEEstablishments, string pFrom, string pTo) =>
            await _IEstablishmentEmail.SendRemove(pGEEstablishments, pFrom, pTo);

        public override async Task<bool> SendUpdate(GEEstablishments pGEEstablishments, string pFrom, string pTo) =>
            await _IEstablishmentEmail.SendUpdate(pGEEstablishments, pFrom, pTo);
    }
}
