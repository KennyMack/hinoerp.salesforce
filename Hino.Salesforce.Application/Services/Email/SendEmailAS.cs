﻿using Hino.Salesforce.Application.Interfaces.Email;
using Hino.Salesforce.Domain.Base.Interfaces.Email;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Email
{
    public class SendEmailAS : ISendEmailAS
    {
        private readonly ISendEmail _ISendEmail;
        public SendEmailAS(ISendEmail pISendEmail)
        {
            _ISendEmail = pISendEmail;
        }

        public MailAddress From { get => _ISendEmail.From; set => _ISendEmail.From = value; }
        public MailAddressCollection To { get => _ISendEmail.To; set => _ISendEmail.To = value; }
        public List<Dictionary<string, string>> ReplaceTags { get => _ISendEmail.ReplaceTags; set => _ISendEmail.ReplaceTags = value; }
        public string LocalEmail { get => _ISendEmail.LocalEmail; set => _ISendEmail.LocalEmail = value; }

        public async Task<bool> Send(string pEstablishmentKey, MailMessage pMailMessage) =>
            await _ISendEmail.Send(pEstablishmentKey, pMailMessage);
    }
}
