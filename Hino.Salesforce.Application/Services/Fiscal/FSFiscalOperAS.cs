using Hino.Salesforce.Application.Interfaces.Fiscal;
using Hino.Salesforce.Domain.Fiscal.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal;

namespace Hino.Salesforce.Application.Services.Fiscal
{
    public class FSFiscalOperAS : BaseAppService<FSFiscalOper>, IFSFiscalOperAS
    {
        private readonly IFSFiscalOperService _IFSFiscalOperService;

        public FSFiscalOperAS(IFSFiscalOperService pIFSFiscalOperService) :
             base(pIFSFiscalOperService)
        {
            _IFSFiscalOperService = pIFSFiscalOperService;
        }
    }
}
