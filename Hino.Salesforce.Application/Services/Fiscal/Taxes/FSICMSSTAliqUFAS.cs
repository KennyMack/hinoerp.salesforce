using Hino.Salesforce.Application.Interfaces.Fiscal.Taxes;
using Hino.Salesforce.Domain.Fiscal.Interfaces.Services.Taxes;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Fiscal.Taxes
{
    public class FSICMSSTAliqUFAS : BaseAppService<FSICMSSTAliqUF>, IFSICMSSTAliqUFAS
    {
        private readonly IFSICMSSTAliqUFService _IFSICMSSTAliqUFService;

        public FSICMSSTAliqUFAS(IFSICMSSTAliqUFService pIFSICMSSTAliqUFService) :
             base(pIFSICMSSTAliqUFService)
        {
            _IFSICMSSTAliqUFService = pIFSICMSSTAliqUFService;
        }

        public override Task<FSICMSSTAliqUF> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(new Infra.Cross.Utils.Exceptions.ModelException
            {
                ErrorCode = (int)Infra.Cross.Utils.Exceptions.EExceptionErrorCodes.InvalidRequest,
                Field = "Id",
                Value = id.ToString(),
                Messages = new string[]
                {
                    Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("RemoveNotAllowed")
                }
            });

            return Task.FromResult(new FSICMSSTAliqUF());
        }
    }
}
