using Hino.Salesforce.Application.Interfaces.Fiscal.Taxes;
using Hino.Salesforce.Application.ViewModels.Fiscal.Taxes;
using Hino.Salesforce.Domain.Fiscal.Interfaces.Services.Taxes;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Fiscal.Taxes
{
    public class FSNCMAS : BaseAppService<FSNCM>, IFSNCMAS
    {
        private readonly IFSNCMService _IFSNCMService;

        public FSNCMAS(IFSNCMService pIFSNCMService) :
             base(pIFSNCMService)
        {
            _IFSNCMService = pIFSNCMService;
        }

        public override Task<FSNCM> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(new Infra.Cross.Utils.Exceptions.ModelException
            {
                ErrorCode = (int)Infra.Cross.Utils.Exceptions.EExceptionErrorCodes.InvalidRequest,
                Field = "Id",
                Value = id.ToString(),
                Messages = new string[]
                {
                    Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("RemoveNotAllowed")
                }
            });

            return Task.FromResult(new FSNCM());
        }

        public async Task LoadNCMsAsync(FSNCMLoadVM[] pLoad)
        {
            foreach (var item in pLoad)
            {
                var ncm = (await _IFSNCMService.QueryAsync(r => r.NCM == item.NCM)).FirstOrDefault();
                if (ncm == null)
                {
                    _IFSNCMService.Add(new FSNCM
                    {
                        EstablishmentKey = "85db3229-73a2-4989-b2f3-9caa542443fe",
                        NCM = item.NCM,
                        Description = item.Description,
                        IPI = item.IPI == "NT" || string.IsNullOrWhiteSpace(item.IPI) ||
                        string.IsNullOrEmpty(item.IPI) || item.IPI == "-" ? -1 : Convert.ToDecimal(item.IPI)
                    });
                }
                else
                {
                    _IFSNCMService.Update(new FSNCM
                    {
                        Id = ncm.Id,
                        UniqueKey = ncm.UniqueKey,
                        EstablishmentKey = "85db3229-73a2-4989-b2f3-9caa542443fe",
                        NCM = ncm.NCM,
                        Description = item.Description,
                        IPI = item.IPI == "NT" || string.IsNullOrWhiteSpace(item.IPI) ||
                        string.IsNullOrEmpty(item.IPI) || item.IPI == "-" ? -1 : Convert.ToDecimal(item.IPI)
                    });
                }
            }

            if (!_IFSNCMService.Errors.Any())
                await _IFSNCMService.SaveChanges();
        }
    }
}
