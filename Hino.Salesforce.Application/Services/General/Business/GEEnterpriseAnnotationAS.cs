﻿using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General.Business
{
    public class GEEnterpriseAnnotationAS : BaseAppService<GEEnterpriseAnnot>, IGEEnterpriseAnnotationAS
    {
        private readonly IGEEnterpriseAnnotationService _IGEEnterpriseAnnotationService;

        public GEEnterpriseAnnotationAS(IGEEnterpriseAnnotationService pIGEEnterpriseAnnotationService) :
             base(pIGEEnterpriseAnnotationService)
        {
            _IGEEnterpriseAnnotationService = pIGEEnterpriseAnnotationService;
        }
    }
}
