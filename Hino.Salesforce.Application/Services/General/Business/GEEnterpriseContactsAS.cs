using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;

namespace Hino.Salesforce.Application.Services.General.Business
{
    public class GEEnterpriseContactsAS : BaseAppService<GEEnterpriseContacts>, IGEEnterpriseContactsAS
    {
        private readonly IGEEnterpriseContactsService _IGEEnterpriseContactsService;

        public GEEnterpriseContactsAS(IGEEnterpriseContactsService pIGEEnterpriseContactsService) :
             base(pIGEEnterpriseContactsService)
        {
            _IGEEnterpriseContactsService = pIGEEnterpriseContactsService;
        }
    }
}
