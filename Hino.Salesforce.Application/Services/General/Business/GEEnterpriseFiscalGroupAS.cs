using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General.Business
{
    public class GEEnterpriseFiscalGroupAS : BaseAppService<GEEnterpriseFiscalGroup>, IGEEnterpriseFiscalGroupAS
    {
        private readonly IGEEnterpriseFiscalGroupService _IGEEnterpriseFiscalGroupService;

        public GEEnterpriseFiscalGroupAS(IGEEnterpriseFiscalGroupService pIGEEnterpriseFiscalGroupService) :
             base(pIGEEnterpriseFiscalGroupService)
        {
            _IGEEnterpriseFiscalGroupService = pIGEEnterpriseFiscalGroupService;
        }

        public override Task<GEEnterpriseFiscalGroup> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(new Infra.Cross.Utils.Exceptions.ModelException
            {
                ErrorCode = (int)Infra.Cross.Utils.Exceptions.EExceptionErrorCodes.InvalidRequest,
                Field = "Id",
                Value = id.ToString(),
                Messages = new string[]
                {
                    Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("RemoveNotAllowed")
                }
            });

            return Task.FromResult(new GEEnterpriseFiscalGroup());
        }
    }
}
