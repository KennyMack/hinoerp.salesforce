using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;

namespace Hino.Salesforce.Application.Services.General.Business
{
    public class GEEnterpriseGeoAS : BaseAppService<GEEnterpriseGeo>, IGEEnterpriseGeoAS
    {
        private readonly IGEEnterpriseGeoService _IGEEnterpriseGeoService;

        public GEEnterpriseGeoAS(IGEEnterpriseGeoService pIGEEnterpriseGeoService) :
             base(pIGEEnterpriseGeoService)
        {
            _IGEEnterpriseGeoService = pIGEEnterpriseGeoService;
        }
    }
}
