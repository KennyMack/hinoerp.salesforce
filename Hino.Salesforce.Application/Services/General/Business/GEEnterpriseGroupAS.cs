using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Linq;

namespace Hino.Salesforce.Application.Services.General.Business
{
    public class GEEnterpriseGroupAS : BaseAppService<GEEnterpriseGroup>, IGEEnterpriseGroupAS
    {
        private readonly IGEEnterpriseGroupService _IGEEnterpriseGroupService;

        public GEEnterpriseGroupAS(IGEEnterpriseGroupService pIGEEnterpriseGroupService) :
             base(pIGEEnterpriseGroupService)
        {
            _IGEEnterpriseGroupService = pIGEEnterpriseGroupService;
        }

        public override async Task<IEnumerable<GEEnterpriseGroup>> SyncData(string pEstablishmentKey, GEEnterpriseGroup[] pRegisters)
        {
            List<ModelException> SyncErrors = new List<ModelException>();

            for (int i = 0, length = pRegisters.Length; i < length; i++)
            {
                Errors.Clear();

                var IdERP = pRegisters[i].IdERP;
                var Id = pRegisters[i].Id;
                GEEnterpriseGroup result;

                result = Id > 0 ?
                      (await QueryAsync(r => r.Id == Id &&
                                             r.EstablishmentKey == pEstablishmentKey)).FirstOrDefault()
                    : (await QueryAsync(r => r.IdERP == IdERP &&
                                             r.EstablishmentKey == pEstablishmentKey)).FirstOrDefault();

                if (result == null)
                {
                    pRegisters[i].Id = 0;
                    Add(pRegisters[i]);
                }
                else
                {
                    var Db = GetByIdToUpdate(result.Id, result.EstablishmentKey, result.UniqueKey);
                    Db.Description = pRegisters[i].Description;
                    Db.Identifier = pRegisters[i].Identifier;
                    Db.IdERP = pRegisters[i].IdERP;

                    Update(Db);
                }

                if (Errors.Count > 0)
                {
                    pRegisters[i].Id = 0;
                    SyncErrors.Add(Errors[0]);
                    RollBackChanges();
                }
                else
                {
                    pRegisters[i] = result;
                    await SaveChanges();
                }
            }

            await SaveChanges();

            return pRegisters;
        }
    }
}
