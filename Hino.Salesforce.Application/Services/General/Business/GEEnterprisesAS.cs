using AutoMapper;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Demograph;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General.Business
{
    public class GEEnterprisesAS : BaseAppService<GEEnterprises>, IGEEnterprisesAS
    {
        private readonly IGEEnterpriseContactsService _IGEEnterpriseContactsService;
        private readonly IGEEnterpriseGeoService _IGEEnterpriseGeoService;
        private readonly IGEEnterprisesService _IGEEnterprisesService;
        private readonly IGECountriesService _IGECountriesService;
        private readonly IGEUserEnterprisesService _IGEUserEnterprisesService;
        private readonly IGEUsersService _IGEUsersService;
        private readonly IGEEstablishmentsService _IGEEstablishmentsService;
        private readonly IVERegionSaleUFService _IVERegionSaleUFService;
        private readonly IVESalePriceService _IVESalePriceService;
        private readonly IVEEntSalePriceService _IVEEntSalePriceService;
        private readonly IGEEnterpriseGroupService _IGEEnterpriseGroupService;
        private readonly IGEEnterpriseFiscalGroupService _IGEEnterpriseFiscalGroupService;
        private readonly IGEEnterpriseCategoryService _IGEEnterpriseCategoryService;
        private readonly IGEPaymentConditionService _IGEPaymentConditionService;
        private readonly IGEEnterpriseAnnotationService _IGEEnterpriseAnnotationService;
        private readonly IReceitaWSAS _IReceitaWSAS;

        public GEEnterprisesAS(IGEEnterprisesService pIGEEnterprisesService,
                               IGECountriesService pIGECountriesService,
                               IGEEnterpriseGeoService pIGEEnterpriseGeoService,
                               IGEEnterpriseContactsService pIGEEnterpriseContactsService,
                               IGEEstablishmentsService pIGEEstablishmentsService,
                               IReceitaWSAS pIReceitaWSAS,
                               IGEUserEnterprisesService pIGEUserEnterprisesService,
                               IGEUsersService pIGEUsersService,
                               IVERegionSaleUFService pIVERegionSaleUFService,
                               IGEEnterpriseGroupService pIGEEnterpriseGroupService,
                               IGEEnterpriseFiscalGroupService pIGEEnterpriseFiscalGroupService,
                               IGEEnterpriseCategoryService pIGEEnterpriseCategoryService,
                               IGEPaymentConditionService pIGEPaymentConditionService,
                               IGEEnterpriseAnnotationService pIGEEnterpriseAnnotationService,
                               IVESalePriceService pIVESalePriceService,
                               IVEEntSalePriceService pIVEEntSalePriceService) :
             base(pIGEEnterprisesService)
        {
            _IGEPaymentConditionService = pIGEPaymentConditionService;
            _IGEEnterpriseGroupService = pIGEEnterpriseGroupService;
            _IGEEnterpriseFiscalGroupService = pIGEEnterpriseFiscalGroupService;
            _IGEEnterpriseCategoryService = pIGEEnterpriseCategoryService;
            _IVESalePriceService = pIVESalePriceService;
            _IVEEntSalePriceService = pIVEEntSalePriceService;

            _IVERegionSaleUFService = pIVERegionSaleUFService;
            _IReceitaWSAS = pIReceitaWSAS;
            _IGEUsersService = pIGEUsersService;
            _IGEUserEnterprisesService = pIGEUserEnterprisesService;
            _IGEEstablishmentsService = pIGEEstablishmentsService;
            _IGEEnterpriseContactsService = pIGEEnterpriseContactsService;
            _IGEEnterprisesService = pIGEEnterprisesService;
            _IGECountriesService = pIGECountriesService;
            _IGEEnterpriseGeoService = pIGEEnterpriseGeoService;
            _IGEEnterpriseAnnotationService = pIGEEnterpriseAnnotationService;
        }

        async Task<bool> ClearContacts(string pEstablishmentKey, long pEnterpriseId)
        {
            var contacts = await _IGEEnterpriseContactsService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey && r.EnterpriseId == pEnterpriseId);

            foreach (var item in contacts)
                await _IGEEnterpriseContactsService.RemoveById(item.Id, pEstablishmentKey, item.UniqueKey);

            await _IGEEnterpriseContactsService.SaveChanges();

            return true;
        }

        async Task<bool> ClearGeo(string pEstablishmentKey, long pEnterpriseId)
        {
            var geo = await _IGEEnterpriseGeoService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey && r.Enterpriseid == pEnterpriseId);

            foreach (var item in geo)
                await _IGEEnterpriseGeoService.RemoveById(item.Id, pEstablishmentKey, item.UniqueKey);

            await _IGEEnterpriseGeoService.SaveChanges();

            return true;
        }

        async Task<bool> ClearAnnotation(string pEstablishmentKey, long pEnterpriseId)
        {
            var annot = await _IGEEnterpriseAnnotationService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey && r.EnterpriseId == pEnterpriseId);

            foreach (var item in annot)
                await _IGEEnterpriseAnnotationService.RemoveById(item.Id, pEstablishmentKey, item.UniqueKey);

            await _IGEEnterpriseAnnotationService.SaveChanges();

            return true;
        }

        string FormatIE(string pIE, string pUF)
        {
            try
            {
                if (!pIE.IsEmpty() && pIE != "ISENTO" && pIE != "N/INFO" &&
                    pUF.ValidarUF())
                    return pIE.CleanSpecialChar().FormatarIE(pUF);
            }
            catch (Exception)
            {
            }
            return pIE;
        }

        public async Task<IEnumerable<GEEnterprises>> SyncDataERP(string pEstablishmentKey, GEEnterprises[] pRegisters)
        {
            for (int i = 0, length = pRegisters.Length; i < length; i++)
            {
                GEEnterpriseGroup EntGroup;
                GEEnterpriseFiscalGroup FiscalGroup;
                GEEnterpriseCategory EntCategory;
                GEPaymentCondition PayCondition;

                var EstablishmentKey = pRegisters[i].EstablishmentKey;
                var EnterpriseId = pRegisters[i].Id;
                pRegisters[i].StatusSinc = Infra.Cross.Utils.Enums.EStatusSinc.Integrated;

                if (pRegisters[i].PayConditionId > 0)
                {
                    var payCond = pRegisters[i].PayConditionId;
                    PayCondition = (await _IGEPaymentConditionService.QueryAsync(r => r.EstablishmentKey == EstablishmentKey &&
                        r.IdERP == payCond)).FirstOrDefault();
                    if (PayCondition != null)
                        pRegisters[i].PayConditionId = PayCondition.Id;
                }

                if (pRegisters[i].CategoryId > 0)
                {
                    var catId = pRegisters[i].CategoryId;
                    EntCategory = (await _IGEEnterpriseCategoryService.QueryAsync(r => r.EstablishmentKey == EstablishmentKey &&
                        r.IdERP == catId)).FirstOrDefault();
                    if (EntCategory == null && pRegisters[i].GEEnterpriseCategory != null)
                    {
                        EntCategory = new GEEnterpriseCategory
                        {
                            Id = 0,
                            UniqueKey = Guid.NewGuid().ToString(),
                            EstablishmentKey = EstablishmentKey,
                            Description = pRegisters[i].GEEnterpriseCategory.Description,
                            IdERP = pRegisters[i].GEEnterpriseCategory.IdERP
                        };
                        _IGEEnterpriseCategoryService.Add(EntCategory);
                        await _IGEEnterpriseCategoryService.SaveChanges();
                    }
                    pRegisters[i].CategoryId = EntCategory.Id;
                }

                if (pRegisters[i].GroupId > 0)
                {
                    var groupId = pRegisters[i].GroupId;
                    EntGroup = (await _IGEEnterpriseGroupService.QueryAsync(r => r.EstablishmentKey == EstablishmentKey &&
                        r.IdERP == groupId)).FirstOrDefault();
                    if (EntGroup == null && pRegisters[i].GEEnterpriseGroup != null)
                    {
                        EntGroup = new GEEnterpriseGroup
                        {
                            Id = 0,
                            UniqueKey = Guid.NewGuid().ToString(),
                            EstablishmentKey = EstablishmentKey,
                            Description = pRegisters[i].GEEnterpriseGroup.Description,
                            IdERP = pRegisters[i].GEEnterpriseGroup.IdERP
                        };
                        _IGEEnterpriseGroupService.Add(EntGroup);
                        await _IGEEnterpriseGroupService.SaveChanges();
                    }
                    pRegisters[i].GroupId = EntGroup.Id;
                }

                if (pRegisters[i].FiscalGroupId > 0)
                {
                    var fisId = pRegisters[i].FiscalGroupId;
                    FiscalGroup = (await _IGEEnterpriseFiscalGroupService.QueryAsync(r => r.EstablishmentKey == EstablishmentKey &&
                        r.IdERP == fisId)).FirstOrDefault();
                    if (FiscalGroup == null && pRegisters[i].GEEnterpriseFiscalGroup != null)
                    {
                        FiscalGroup = new GEEnterpriseFiscalGroup
                        {
                            Id = 0,
                            UniqueKey = Guid.NewGuid().ToString(),
                            EstablishmentKey = EstablishmentKey,
                            Description = pRegisters[i].GEEnterpriseFiscalGroup.Description,
                            IdERP = pRegisters[i].GEEnterpriseFiscalGroup.IdERP,
                            Type = pRegisters[i].GEEnterpriseFiscalGroup.Type
                        };
                        _IGEEnterpriseFiscalGroupService.Add(FiscalGroup);
                        await _IGEEnterpriseFiscalGroupService.SaveChanges();
                    }
                    pRegisters[i].FiscalGroupId = FiscalGroup.Id;
                }

                var Contacts = await _IGEEnterpriseContactsService.QueryAsync(
                    r => r.EstablishmentKey == EstablishmentKey &&
                    r.EnterpriseId == EnterpriseId
                );

                foreach (var Contact in Contacts)
                    pRegisters[i].GEEnterpriseContacts.Add(Contact);

                pRegisters[i].GEPaymentCondition = null;
                pRegisters[i].GEEnterpriseCategory = null;
                pRegisters[i].GEEnterpriseGroup = null;
                pRegisters[i].GEEnterpriseFiscalGroup = null;
            }


            await SyncData(pEstablishmentKey, pRegisters);

            return pRegisters;
        }

        public override async Task<IEnumerable<GEEnterprises>> SyncData(string pEstablishmentKey, GEEnterprises[] pRegisters)
        {
            List<ModelException> SyncErrors = new List<ModelException>();

            for (int i = 0, length = pRegisters.Length; i < length; i++)
            {
                foreach (var item in pRegisters[i].GEEnterpriseGeo)
                {
                    var Country = (await _IGECountriesService.QueryAsync(r => r.BACEN == item.CodBacen)).FirstOrDefault();

                    if (Country != null)
                    {
                        item.CountryIni = Country.CIOC;
                        item.CountryCode = Country.DDI;
                        item.CountryName = Country.Name;
                    }
                }
                var CNPJCPF = pRegisters[i].CNPJCPF.FormataCPFCNPJ();
                var Type = pRegisters[i].Type;
                var IdErp = pRegisters[i].IdERP;

                if (pRegisters[i].Classification == EEnterpriseClassification.Prospect)
                {
                    var EnterpriseOld = (await QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                               r.Type == Type &&
                                                               r.IdERP == IdErp)).FirstOrDefault();

                    if (EnterpriseOld != null)
                    {
                        pRegisters[i].Created = EnterpriseOld.Created;
                        pRegisters[i].Id = EnterpriseOld.Id;
                        pRegisters[i].UniqueKey = EnterpriseOld.UniqueKey;
                    }
                }
                else
                {
                    var EnterpriseOld = (await QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                               r.Type == Type &&
                                                               r.CNPJCPF == CNPJCPF)).FirstOrDefault();

                    if (EnterpriseOld != null)
                    {
                        pRegisters[i].Created = EnterpriseOld.Created;
                        pRegisters[i].Id = EnterpriseOld.Id;
                        pRegisters[i].UniqueKey = EnterpriseOld.UniqueKey;
                    }
                }

                GEEnterprisesVM result;
                if (pRegisters[i].Id <= 0)
                    result = await CreateEnterprise(Mapper.Map<GEEnterprises, GEEnterprisesVM>(pRegisters[i]));
                else
                    result = await UpdateEnterprise(Mapper.Map<GEEnterprises, GEEnterprisesVM>(pRegisters[i]));

                if (Errors.Count > 0)
                {
                    pRegisters[i].Id = 0;
                    SyncErrors.Add(Errors[0]);
                }
                else
                    pRegisters[i] = Mapper.Map<GEEnterprisesVM, GEEnterprises>(result);
            }

            return pRegisters;
        }

        public async Task<VERegionSaleUF> GetEnterpriseSaleRegionAsync(string pEstablishmentKey, string pUF) =>
            (await _IVERegionSaleUFService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey && r.UF.ToUpper() == pUF)).FirstOrDefault();

        public async Task<VERegionSaleUF> GetEnterpriseSaleRegionWithoutUFAsync(string pEstablishmentKey) =>
            (await _IVERegionSaleUFService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey)).FirstOrDefault();

        private async Task<VESalePrice> GetSalePriceAsync(string pEstablishmentKey, long pId) =>
            (await _IVESalePriceService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey && r.Id == pId)).FirstOrDefault();

        private async Task<IEnumerable<VEEntSalePrice>> GetSalePriceByEnterpriseAsync(string pEstablishmentKey, long pEntepriseId) =>
            (await _IVEEntSalePriceService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey && r.EnterpriseID == pEntepriseId));

        public async Task<bool> EnterpriseExists(string pEstablishmentKey, string pCNPJCPF, EEnterpriseClassification pClassification, long idActual)
        {
            var EnterpriseDuplicated = await QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                             r.CNPJCPF == pCNPJCPF &&
                                                             r.Classification == pClassification &&
                                                             r.Id != idActual);
            return EnterpriseDuplicated.Any();
        }

        public async Task<PagedResult<GEEnterprisesVM>> GetEnterprise(string pEstablishmentKey, long pId)
        {
            var Result = new PagedResult<GEEnterprisesVM>();
            var Enterprise = Mapper.Map<GEEnterprises, GEEnterprisesVM>(
                await _IGEEnterprisesService.GetEnterprise(pEstablishmentKey, pId));

            if (Enterprise != null && Enterprise.CodPrVenda != null)
            {
                var List = await _IVESalePriceService.GetListCodPRVendaAsync(pEstablishmentKey);

                Enterprise.SalePrice = Mapper.Map<VESalePrice, VESalePriceVM>(
                    List.FirstOrDefault(r => r.CodPrVenda == Enterprise.CodPrVenda));
            }

            if (Enterprise != null)
            {
                Result.Results.Add(Enterprise);
                Result.CurrentPage = 1;
                Result.PageCount = 1;
                Result.PageSize = 1;
                Result.RowCount = 1;
            }

            return Result;
        }

        public async Task<GEEnterprisesVM> CreateEnterprise(GEEnterprisesVM model)
        {
            if (model.Classification == EEnterpriseClassification.Prospect && model.GEEnterpriseGeo.FirstOrDefault().ZipCode.IsEmpty())
            {
                model.GEEnterpriseGeo.Clear();
                model.GEEnterpriseGeo.Add(GEEnterpriseGeoVM.GetDefaultProspectGeo());
            }

            var CreateEnterprise = Mapper.Map<GEEnterprisesVM, GEEnterprises>(model);

            CreateEnterprise.RazaoSocial = CreateEnterprise.RazaoSocial.Trim();
            CreateEnterprise.NomeFantasia = CreateEnterprise.NomeFantasia.Trim();

            VERegionSaleUF RegionSale;

            if (model.CommercialAddress != null &&
                model?.CommercialAddress?.UF != null)
                RegionSale = await GetEnterpriseSaleRegionAsync(model.EstablishmentKey, (model?.CommercialAddress.UF?.ToUpper()) ?? "");
            else
                RegionSale = await GetEnterpriseSaleRegionWithoutUFAsync(model.EstablishmentKey);

            if (RegionSale == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "GEEnterprises",
                    Messages = new[] { string.Format(Infra.Cross.Resources.ErrorMessagesResource.UFNotFoundInRegionSale, (model?.CommercialAddress.UF?.ToUpper()) ?? "-") },
                    Value = CreateEnterprise.RazaoSocial
                });
                return model;
            }

            if (CreateEnterprise.CNPJCPF != "" && await EnterpriseExists(CreateEnterprise.EstablishmentKey, CreateEnterprise.CNPJCPF, CreateEnterprise.Classification, CreateEnterprise.Id))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "GEEnterprises",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.CnpjCpfDuplicated },
                    Value = CreateEnterprise.RazaoSocial
                });
                return model;
            }

            CreateEnterprise.CreditUsed = 0;
            CreateEnterprise.CreditLimit = 0;
            CreateEnterprise.RegionId = RegionSale.RegionId;
            CreateEnterprise.BirthDate = CreateEnterprise.BirthDate?.Date;

            if (model.UserId > 0)
                CreateEnterprise.CreatedById = model.UserId;

            if (CreateEnterprise.Type == EEnterpriseType.PessoaFisica)
                CreateEnterprise.NomeFantasia = CreateEnterprise.RazaoSocial;

            foreach (var item in CreateEnterprise.GEEnterpriseGeo)
            {
                item.EstablishmentKey = CreateEnterprise.EstablishmentKey;
                item.IE = FormatIE(model.IE, item.UF);
                if (item.Type == Infra.Cross.Utils.Enums.EAddressType.Commercial)
                    CreateEnterprise.IE = item.IE;
            }

            foreach (var item in CreateEnterprise.GEEnterpriseContacts)
                item.EstablishmentKey = CreateEnterprise.EstablishmentKey;

            foreach (var item in CreateEnterprise.GEEnterpriseAnnotation)
            {
                item.EstablishmentKey = CreateEnterprise.EstablishmentKey;
                item.UniqueKey = "";
                item.Id = 0;
            }

            foreach (var geo in CreateEnterprise.GEEnterpriseGeo)
                geo.Address = geo.Address.Trim();

            var retmodel = _IGEEnterprisesService.CreateEnterprise(CreateEnterprise);

            if (Errors.Count <= 0)
                await _IGEEnterprisesService.SaveChanges();

            if (model.UserId > 0 && Errors.Count <= 0)
            {
                var Users = await _IGEUsersService.QueryAsync(r => r.EstablishmentKey == CreateEnterprise.EstablishmentKey &&
                    r.Id == model.UserId
                );

                if (Users.Count() > 0 && Users.FirstOrDefault() != null &&
                   Users.FirstOrDefault().UserType == EUserType.Representant)
                {
                    var User = Users.FirstOrDefault();
                    _IGEUserEnterprisesService.Add(new GEUserEnterprises
                    {
                        EstablishmentKey = CreateEnterprise.EstablishmentKey,
                        UserId = User.Id,
                        EnterpriseId = retmodel.Id
                    });

                    await _IGEUserEnterprisesService.SaveChanges();
                }
            }

            /* if (Errors.Count <= 0 && model.GEEnterpriseGeo != null && model.GEEnterpriseGeo.Any())
             {
                 var geo = model.GEEnterpriseGeo.First();
                 geo.EnterpriseID = retmodel.Id;
                 geo.EstablishmentKey = retmodel.EstablishmentKey;

                 _IGEEnterpriseGeoService.Add(Mapper.Map<GEEnterpriseGeoVM, GEEnterpriseGeo>(geo));
                 Errors = _IGEEnterpriseGeoService.Errors;
             }*/

            return Mapper.Map<GEEnterprises, GEEnterprisesVM>(retmodel);
        }

        public async Task<GEEnterprisesVM> UpdateEnterprise(GEEnterprisesVM model)
        {
            var UpdateEnterprise = Mapper.Map<GEEnterprisesVM, GEEnterprises>(model);

            VERegionSaleUF RegionSale;

            if (model.CommercialAddress != null &&
                model?.CommercialAddress?.UF != null)
                RegionSale = await GetEnterpriseSaleRegionAsync(model.EstablishmentKey, (model?.CommercialAddress.UF?.ToUpper()) ?? "");
            else
                RegionSale = await GetEnterpriseSaleRegionWithoutUFAsync(model.EstablishmentKey);

            if (RegionSale == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "GEEnterprises",
                    Messages = new[] { string.Format(Infra.Cross.Resources.ErrorMessagesResource.UFNotFoundInRegionSale, (model?.CommercialAddress.UF?.ToUpper()) ?? "-") },
                    Value = UpdateEnterprise.RazaoSocial
                });
                return model;
            }

            UpdateEnterprise.RegionId = RegionSale.RegionId;
            UpdateEnterprise.BirthDate = UpdateEnterprise.BirthDate?.Date;

            if (UpdateEnterprise.Type == EEnterpriseType.PessoaFisica)
                UpdateEnterprise.NomeFantasia = UpdateEnterprise.RazaoSocial;

            await ClearGeo(UpdateEnterprise.EstablishmentKey, UpdateEnterprise.Id);
            foreach (var item in UpdateEnterprise.GEEnterpriseGeo)
            {
                item.Enterpriseid = UpdateEnterprise.Id;
                item.EstablishmentKey = UpdateEnterprise.EstablishmentKey;

                item.IE = FormatIE(model.IE, item.UF);
                if (item.Type == Infra.Cross.Utils.Enums.EAddressType.Commercial)
                    UpdateEnterprise.IE = item.IE;

                _IGEEnterpriseGeoService.Add(new GEEnterpriseGeo
                {
                    Enterpriseid = item.Enterpriseid,
                    Type = item.Type,
                    CNPJCPF = item.CNPJCPF,
                    IE = item.IE,
                    RG = item.RG,
                    CellPhone = item.CellPhone,
                    Phone = item.Phone,
                    Email = item.Email,
                    EmailNFE = item.EmailNFE,
                    Address = item.Address,
                    Complement = item.Complement,
                    District = item.District,
                    Num = item.Num,
                    ZipCode = item.ZipCode,
                    Site = item.Site,
                    CountryIni = item.CountryIni,
                    CountryCode = item.CountryCode,
                    CountryName = item.CountryName,
                    UF = item.UF,
                    StateName = item.StateName,
                    IBGE = item.IBGE,
                    CityName = item.CityName,
                    DisplayLat = item.DisplayLat,
                    DisplayLng = item.DisplayLng,
                    NavLat = item.NavLat,
                    NavLng = item.NavLng,
                    EstablishmentKey = item.EstablishmentKey,
                    UniqueKey = item.UniqueKey,
                    Created = item.Created,
                    Modified = item.Modified,
                    Id = item.Id
                });
            }
            await _IGEEnterpriseGeoService.SaveChanges();

            await ClearContacts(UpdateEnterprise.EstablishmentKey, UpdateEnterprise.Id);

            foreach (var item in UpdateEnterprise.GEEnterpriseContacts)
            {
                item.EstablishmentKey = UpdateEnterprise.EstablishmentKey;
                item.EnterpriseId = UpdateEnterprise.Id;

                _IGEEnterpriseContactsService.Add(new GEEnterpriseContacts
                {
                    EnterpriseId = item.EnterpriseId,
                    ReceptivityIndex = item.ReceptivityIndex,
                    Sector = item.Sector,
                    Email = item.Email,
                    Contact = item.Contact,
                    Ramal = item.Ramal,
                    Phone = item.Phone,
                    Note = item.Note,
                    EstablishmentKey = item.EstablishmentKey,
                    UniqueKey = item.UniqueKey,
                    Created = item.Created,
                    Modified = item.Modified,
                    Id = item.Id
                });
            }

            await _IGEEnterpriseContactsService.SaveChanges();

            await ClearAnnotation(UpdateEnterprise.EstablishmentKey, UpdateEnterprise.Id);

            foreach (var item in UpdateEnterprise.GEEnterpriseAnnotation)
            {
                item.EstablishmentKey = UpdateEnterprise.EstablishmentKey;
                item.EnterpriseId = UpdateEnterprise.Id;

                _IGEEnterpriseAnnotationService.Add(new GEEnterpriseAnnot
                {
                    EnterpriseId = item.EnterpriseId,
                    Annotation = item.Annotation,
                    UserID = item.UserID,
                    IsActive = true,
                    EstablishmentKey = item.EstablishmentKey,
                    UniqueKey = item.UniqueKey,
                    Created = item.Created,
                    Modified = item.Modified,
                    Id = item.Id
                });
            }

            await _IGEEnterpriseAnnotationService.SaveChanges();

            UpdateEnterprise.GEEnterpriseContacts.Clear();
            UpdateEnterprise.GEEnterpriseGeo.Clear();
            UpdateEnterprise.GEEnterpriseAnnotation.Clear();

            var EntUpdate = GetByIdToUpdate(UpdateEnterprise.Id, UpdateEnterprise.EstablishmentKey, UpdateEnterprise.UniqueKey);
            EntUpdate.RazaoSocial = UpdateEnterprise.RazaoSocial;
            EntUpdate.NomeFantasia = UpdateEnterprise.NomeFantasia;
            EntUpdate.CNPJCPF = UpdateEnterprise.CNPJCPF;
            EntUpdate.IE = UpdateEnterprise.IE;
            EntUpdate.Type = UpdateEnterprise.Type;
            EntUpdate.Status = UpdateEnterprise.Status;
            EntUpdate.StatusSinc = UpdateEnterprise.StatusSinc;
            EntUpdate.IdERP = UpdateEnterprise.IdERP;
            EntUpdate.TypeSaleId = UpdateEnterprise.TypeSaleId;
            EntUpdate.RegionId = UpdateEnterprise.RegionId;
            EntUpdate.BirthDate = UpdateEnterprise.BirthDate;
            EntUpdate.Classification = UpdateEnterprise.Classification;
            EntUpdate.ClassifEmpresa = UpdateEnterprise.ClassifEmpresa;
            EntUpdate.FiscalGroupId = UpdateEnterprise.FiscalGroupId;
            EntUpdate.PayConditionId = UpdateEnterprise.PayConditionId;
            EntUpdate.CategoryId = UpdateEnterprise.CategoryId;
            EntUpdate.GroupId = UpdateEnterprise.GroupId;
            EntUpdate.SellerChangePrice = UpdateEnterprise.SellerChangePrice;
            EntUpdate.SellerPayCondition = UpdateEnterprise.SellerPayCondition;
            EntUpdate.SellerBonification = UpdateEnterprise.SellerBonification;
            EntUpdate.SellerSample = UpdateEnterprise.SellerSample;
            EntUpdate.DiscountNFBoleto = UpdateEnterprise.DiscountNFBoleto;
            EntUpdate.PercDiscount = UpdateEnterprise.PercDiscount;
            EntUpdate.CodPrVenda = UpdateEnterprise.CodPrVenda;

            var retModel = _IGEEnterprisesService.Update(EntUpdate);

            if (Errors.Count <= 0)
                await _IGEEnterprisesService.SaveChanges();

            return Mapper.Map<GEEnterprises, GEEnterprisesVM>(retModel);
        }

        public override async Task<GEEnterprises> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            var Enterprise = await _IGEEnterprisesService.GetByIdAsync(id, pEstablishmentKey, pUniqueKey,
                r => r.GEEnterpriseGeo,
                s => s.GEEnterpriseContacts);

            if (Enterprise == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "GEEnterprises",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("NotFound") },
                    Value = id.ToString()
                });
                return null;
            }

            if (Enterprise.StatusSinc != Infra.Cross.Utils.Enums.EStatusSinc.Waiting)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "GEEnterprises",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("StatusSincNotAllowRemove") },
                    Value = id.ToString()
                });
                return null;
            }

            if (Enterprise.VEOrders.Count > 0)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterChild,
                    Field = "VEOrders",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("RegisterChild") },
                    Value = id.ToString()
                });
                return null;
            }

            foreach (var item in Enterprise.GEEnterpriseGeo)
                await _IGEEnterpriseGeoService.RemoveById(item.Id, item.EstablishmentKey, item.UniqueKey);

            foreach (var item in Enterprise.GEEnterpriseContacts)
                await _IGEEnterpriseContactsService.RemoveById(item.Id, item.EstablishmentKey, item.UniqueKey);

            var UserEnterprise = await _IGEUserEnterprisesService.QueryAsync(r =>
                r.EstablishmentKey == Enterprise.EstablishmentKey &&
                r.EnterpriseId == Enterprise.Id);

            foreach (var item in UserEnterprise)
                await _IGEUserEnterprisesService.RemoveById(item.Id, item.EstablishmentKey, item.UniqueKey);

            var SalePriceEnterprise = await GetSalePriceByEnterpriseAsync(Enterprise.EstablishmentKey, Enterprise.Id);

            foreach (var item in SalePriceEnterprise)
                await _IVEEntSalePriceService.RemoveById(item.Id, item.EstablishmentKey, item.UniqueKey);

            Enterprise.GEEnterpriseGeo.Clear();
            Enterprise.GEEnterpriseContacts.Clear();

            await _IGEEnterprisesService.RemoveById(Enterprise.Id, Enterprise.EstablishmentKey, Enterprise.UniqueKey);
            await _IGEUserEnterprisesService.SaveChanges();
            await _IGEEnterpriseContactsService.SaveChanges();
            await _IGEEnterpriseGeoService.SaveChanges();
            await _IGEEnterprisesService.SaveChanges();
            await _IVEEntSalePriceService.SaveChanges();
            return Enterprise;
        }

        public async Task<GEEnterprisesVM> UpdateToClient(string pUniqueKey, long pId, GEEnterprisesVM pGEEnterprisesVM)
        {
            var UpdateEnterprise = Mapper.Map<GEEnterprisesVM, GEEnterprises>(pGEEnterprisesVM);
            
            if (UpdateEnterprise == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "EnterpriseId",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.EnterpriseNotFound },
                    Value = pId.ToString()
                });
                return null;
            }

            var commercialAddress = UpdateEnterprise.GEEnterpriseGeo.FirstOrDefault(r => r.Type == EAddressType.Commercial);
            if (commercialAddress.ZipCode.IsEmpty() ||
                commercialAddress.Address == "PROSPECT")
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Address",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.InvalidAddress },
                    Value = pId.ToString()
                });
                return null;
            }

            if (UpdateEnterprise.CNPJCPF.IsEmpty())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "CNPJCPF",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.InvalidCNPJCPF },
                    Value = pId.ToString()
                });
                return null;
            }

            var estab = await _IGEEstablishmentsService.GetByEstablishmentKeyAsync(UpdateEnterprise.EstablishmentKey);

            UpdateEnterprise.Classification = EEnterpriseClassification.Client;
            if (UpdateEnterprise.Type == EEnterpriseType.PessoaFisica)
            {
                UpdateEnterprise.ClassifEmpresa = estab.PfClassifClient ?? "C3";
                UpdateEnterprise.FiscalGroupId = estab.PfFiscalGroupId;
            }
            else
            {
                UpdateEnterprise.ClassifEmpresa = estab.PjClassifClient ?? "C1";
                UpdateEnterprise.FiscalGroupId = estab.PjFiscalGroupId;
            }

            if (UpdateEnterprise.FiscalGroupId == null)
            {
                var FiscalGroup = (await _IGEEnterpriseFiscalGroupService.QueryAsync(r => r.EstablishmentKey == UpdateEnterprise.EstablishmentKey &&
                                                                                          (r.Type == "C" || r.Type == "X")
                                                                                    )).FirstOrDefault();
                UpdateEnterprise.FiscalGroupId = FiscalGroup.Id;
            }

            var retModel = _IGEEnterprisesService.Update(UpdateEnterprise);

            if (Errors.Count <= 0)
                await _IGEEnterprisesService.SaveChanges();

            return Mapper.Map<GEEnterprises, GEEnterprisesVM>(retModel);
        }

        public async Task<GEEnterprisesVM> SearchEnterpriseAsync(string pEstablishmentKey, string pCNPJ)
        {
            if (pCNPJ.IsEmpty())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "CNPJ",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.InvalidCNPJ },
                    Value = pEstablishmentKey
                });
                return null;
            }

            var Establishment = (await _IGEEstablishmentsService.GetByEstablishmentKeyAsync(pEstablishmentKey));

            if (Establishment == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "GEEstablishments",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.InvalidEstablishmentKeyOrNull },
                    Value = pEstablishmentKey
                });
                return null;
            }

            var result = await _IReceitaWSAS.GetEnterpriseAsync(pCNPJ, Establishment.TokenCNPJ);

            if (_IReceitaWSAS.Errors.Any())
            {
                Errors = _IReceitaWSAS.Errors;
                return null;
            }

            var EnterpriseVM = new GEEnterprisesVM
            {
                Id = 0,
                NomeFantasia = result.nomefantasia.ToLower().ToTitleCase(),
                RazaoSocial = result.nome.ToLower().ToTitleCase(),
                CNPJCPF = result.cnpj,
                IE = "N/INFO",
                Type = Infra.Cross.Utils.Enums.EEnterpriseType.PessoaJuridica,
                Status = Infra.Cross.Utils.Enums.EStatusEnterprise.New,
                StatusSinc = Infra.Cross.Utils.Enums.EStatusSinc.Waiting,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true,
                EstablishmentKey = pEstablishmentKey
            };

            var Address = result.Address.CEPAddress;

            if (Address.PostalCode != null)
            {
                EnterpriseVM.GEEnterpriseGeo.Add(new GEEnterpriseGeoVM
                {
                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    IsActive = true,
                    EstablishmentKey = pEstablishmentKey,
                    Type = Infra.Cross.Utils.Enums.EAddressType.Commercial,
                    EnterpriseID = 0,
                    IE = EnterpriseVM.IE,
                    CNPJCPF = EnterpriseVM.CNPJCPF,
                    Phone = result.primeirotelefone,
                    Email = result.email,
                    Address = Address.Street,
                    Complement = Address.Complement,
                    District = Address.District,
                    Num = result.numero,
                    ZipCode = Address.PostalCode,
                    CountryIni = result.Address.Country.CIOC,
                    CountryCode = result.Address.Country.NumericCode,
                    CountryName = result.Address.Country.Name,
                    UF = result.Address.GEStates.Initials,
                    StateName = result.Address.GEStates.Name,
                    IBGE = result.Address.ibge,
                    DisplayLat = 0,
                    DisplayLng = 0,
                    NavLat = 0,
                    NavLng = 0,
                    CityName = result.Address.GECities.Name
                });
            }

            return EnterpriseVM;
        }

    }
}

