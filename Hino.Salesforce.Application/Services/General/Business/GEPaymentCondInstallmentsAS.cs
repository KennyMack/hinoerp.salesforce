using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;

namespace Hino.Salesforce.Application.Services.General.Business
{
    public class GEPaymentCondInstallmentsAS : BaseAppService<GEPaymentCondInstallments>, IGEPaymentCondInstallmentsAS
    {
        private readonly IGEPaymentCondInstallmentsService _IGEPaymentCondInstallmentsService;

        public GEPaymentCondInstallmentsAS(IGEPaymentCondInstallmentsService pIGEPaymentCondInstallmentsService) :
             base(pIGEPaymentCondInstallmentsService)
        {
            _IGEPaymentCondInstallmentsService = pIGEPaymentCondInstallmentsService;
        }
    }
}
