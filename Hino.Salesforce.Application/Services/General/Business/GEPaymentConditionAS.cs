using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General.Business
{
    public class GEPaymentConditionAS : BaseAppService<GEPaymentCondition>, IGEPaymentConditionAS
    {
        private readonly IGEPaymentConditionService _IGEPaymentConditionService;
        private readonly IGEPaymentCondInstallmentsService _IGEPaymentCondInstallmentsService;

        public GEPaymentConditionAS(IGEPaymentConditionService pIGEPaymentConditionService,
            IGEPaymentCondInstallmentsService pIGEPaymentCondInstallmentsService) :
             base(pIGEPaymentConditionService)
        {
            _IGEPaymentConditionService = pIGEPaymentConditionService;
            _IGEPaymentCondInstallmentsService = pIGEPaymentCondInstallmentsService;
        }

        bool ValidateInstallments(GEPaymentCondition model)
        {
            if (model.GEPaymentCondInstallments.Sum(r => r.Percent) != 100)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "GEPaymentCondInstallments",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.InvalidPercentualInstallments },
                    Value = null
                });
                return false;
            }

            if (model.GEPaymentCondInstallments.Count != model.Installments)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "GEPaymentCondInstallments",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.InvalidCountInstallments },
                    Value = null
                });
                return false;
            }

            return true;
        }

        async Task<bool> ClearInstallments(string pEstablishmentKey, long pCondPayId)
        {
            var installments = await _IGEPaymentCondInstallmentsService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey && r.CondPayId == pCondPayId);

            foreach (var item in installments)
                await _IGEPaymentCondInstallmentsService.RemoveById(item.Id, pEstablishmentKey, item.UniqueKey);

            await _IGEPaymentCondInstallmentsService.SaveChanges();

            return true;
        }

        public override GEPaymentCondition Add(GEPaymentCondition model)
        {
            foreach (var item in model.GEPaymentCondInstallments)
                item.EstablishmentKey = model.EstablishmentKey;

            if (!ValidateInstallments(model))
                return null;

            return base.Add(model);
        }

        public async Task<GEPaymentCondition> ChangeAsync(GEPaymentCondition model)
        {
            if (!ValidateInstallments(model))
                return null;

            var dbModel = await _IGEPaymentConditionService.GetByIdAsync(model.Id, model.EstablishmentKey, model.UniqueKey);

            await ClearInstallments(model.EstablishmentKey, model.Id);
            dbModel.GEPaymentCondInstallments.Clear();

            foreach (var item in model.GEPaymentCondInstallments)
            {
                item.EstablishmentKey = model.EstablishmentKey;
                item.CondPayId = model.Id;

                _IGEPaymentCondInstallmentsService.Add(new GEPaymentCondInstallments
                {
                    Id = 0,
                    Days = item.Days,
                    CondPayId = model.Id,
                    EstablishmentKey = model.EstablishmentKey,
                    Percent = item.Percent
                });
            }

            await _IGEPaymentCondInstallmentsService.SaveChanges();

            dbModel.Description = model.Description;
            dbModel.Installments = model.Installments;
            dbModel.TypePayID = model.TypePayID;
            dbModel.IdERP = model.IdERP;
            dbModel.GEPaymentType = null;
            dbModel.PercDiscount = model.PercDiscount;
            dbModel.PercIncrease = model.PercIncrease;


            return base.Update(dbModel);
        }

        public override async Task<GEPaymentCondition> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            var PaymentCondition = await _IGEPaymentConditionService.GetByIdAsync(id, pEstablishmentKey, pUniqueKey);

            if (PaymentCondition == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "GEPaymentCondition",
                    Messages = new[] { "Nenhum registro encontrado" },
                    Value = id.ToString()
                });
                return null;
            }

            if (PaymentCondition.VEOrders.Count > 0)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterChild,
                    Field = "VEOrders",
                    Messages = new[] { "Existem pedidos vinculados a essa condi��o de pagamento" },
                    Value = id.ToString()
                });
                return null;
            }

            await ClearInstallments(pEstablishmentKey, id);
            _IGEPaymentConditionService.Remove(PaymentCondition);
            await _IGEPaymentConditionService.SaveChanges();
            return PaymentCondition;
        }
    }
}
