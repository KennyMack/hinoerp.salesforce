using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General.Business
{
    public class GEPaymentTypeAS : BaseAppService<GEPaymentType>, IGEPaymentTypeAS
    {
        private readonly IGEPaymentTypeService _IGEPaymentTypeService;

        public GEPaymentTypeAS(IGEPaymentTypeService pIGEPaymentTypeService) :
             base(pIGEPaymentTypeService)
        {
            _IGEPaymentTypeService = pIGEPaymentTypeService;
        }

        public override async Task<GEPaymentType> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            var PaymentType = await _IGEPaymentTypeService.GetByIdAsync(id, pEstablishmentKey, pUniqueKey);

            if (PaymentType == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "GEPaymentType",
                    Messages = new[] { "Nenhum registro encontrado" },
                    Value = id.ToString()
                });
                return null;
            }

            if (PaymentType.VEOrders.Count > 0 || PaymentType.GEPaymentCondition.Count > 0)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterChild,
                    Field = "VEOrders",
                    Messages = new[] { "Existem pedidos e/ou condi��es de pagamento vinculados a essa forma de pagamento" },
                    Value = id.ToString()
                });
                return null;
            }

            _IGEPaymentTypeService.Remove(PaymentType);
            await _IGEPaymentTypeService.SaveChanges();
            return PaymentType;
        }
    }
}
