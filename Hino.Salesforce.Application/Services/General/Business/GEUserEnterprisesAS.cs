using AutoMapper;
using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General.Business
{
    public class GEUserEnterprisesAS : BaseAppService<GEUserEnterprises>, IGEUserEnterprisesAS
    {
        private readonly IGEUserEnterprisesService _IGEUserEnterprisesService;

        public GEUserEnterprisesAS(IGEUserEnterprisesService pIGEUserEnterprisesService) :
             base(pIGEUserEnterprisesService)
        {
            _IGEUserEnterprisesService = pIGEUserEnterprisesService;
        }

        public async Task<PagedResult<GEUserEnterprises>> QuerySearchPagedAsync(int page, int pageSize,
            Expression<Func<GEUserEnterprises, bool>> predicate,
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
            await _IGEUserEnterprisesService.QuerySearchPagedAsync(page, pageSize, predicate, includeProperties);

        public async Task<GEUserEnterprises> CreateOrUpdateAsync(GEUserEnterprisesVM enterprise)
        {
            var ent = await this.QueryAsync(r =>
                r.EstablishmentKey == enterprise.EstablishmentKey &&
                r.UserId == enterprise.UserId &&
                r.EnterpriseId == enterprise.EnterpriseId);

            if (!ent.Any())
            {
                var entNew = new GEUserEnterprises
                {
                    EstablishmentKey = enterprise.EstablishmentKey,
                    UserId = enterprise.UserId,
                    EnterpriseId = enterprise.EnterpriseId
                };

                this.Add(entNew);

                await SaveChanges();

                return entNew;
            }

            return Mapper.Map<GEUserEnterprisesVM, GEUserEnterprises>(enterprise);
        }

        public async Task<GEUserEnterprises[]> CreateOrUpdateListAsync(string pEstablishmentKey, GEUserEnterprisesVM[] enterprise)
        {

            for (int i = 0, length = enterprise.Length; i < length; i++)
            {
                if (enterprise[i].Id < 0)
                    enterprise[i].Id = 0;

                enterprise[i].EstablishmentKey = pEstablishmentKey;
                enterprise[i].GEEnterprises = null;
                enterprise[i].GEUsers = null;

                enterprise[i] = Mapper.Map<GEUserEnterprises, GEUserEnterprisesVM>(await CreateOrUpdateAsync(enterprise[i]));
            }

            return Mapper.Map<GEUserEnterprisesVM[], GEUserEnterprises[]>(enterprise);
        }

        public async Task<GEUserEnterprises[]> RemoveListAsync(string pEstablishmentKey, GEUserEnterprisesVM[] enterprise)
        {
            for (int i = 0, length = enterprise.Length; i < length; i++)
            {
                if (enterprise[i].Id < 0)
                    enterprise[i].Id = 0;

                enterprise[i] = Mapper.Map<GEUserEnterprises, GEUserEnterprisesVM>(await RemoveById(
                    enterprise[i].Id,
                    pEstablishmentKey,
                    enterprise[i].UniqueKey
                    ));
            }

            return Mapper.Map<GEUserEnterprisesVM[], GEUserEnterprises[]>(enterprise);
        }
    }
}
