using Hino.Salesforce.Application.Interfaces;
using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Application.Services;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using System.Threading.Tasks;
using System.Linq;
using AutoMapper;

namespace Hino.Salesforce.Application.Services.General.Business
{
    public class GEUserPayTypeAS : BaseAppService<GEUserPayType>, IGEUserPayTypeAS
    {
        private readonly IGEUserPayTypeService _IGEUserPayTypeService;

        public GEUserPayTypeAS(IGEUserPayTypeService pIGEUserPayTypeService) : 
             base(pIGEUserPayTypeService)
        {
            _IGEUserPayTypeService = pIGEUserPayTypeService;
        }

        public async Task<GEUserPayType> CreateOrUpdateAsync(GEUserPayTypeVM paymentType)
        {
            var ent = await this.QueryAsync(r =>
                r.EstablishmentKey == paymentType.EstablishmentKey &&
                r.UserId == paymentType.UserId &&
                r.PaymentTypeId == paymentType.PaymentTypeId);

            if (!ent.Any())
            {
                var entNew = new GEUserPayType
                {
                    EstablishmentKey = paymentType.EstablishmentKey,
                    UserId = paymentType.UserId,
                    PaymentTypeId = paymentType.PaymentTypeId
                };

                this.Add(entNew);

                await SaveChanges();

                return entNew;
            }

            return Mapper.Map<GEUserPayTypeVM, GEUserPayType>(paymentType);
        }

        public async Task<GEUserPayType[]> CreateOrUpdateListAsync(string pEstablishmentKey, GEUserPayTypeVM[] paymentType)
        {

            for (int i = 0, length = paymentType.Length; i < length; i++)
            {
                if (paymentType[i].Id < 0)
                    paymentType[i].Id = 0;

                paymentType[i].EstablishmentKey = pEstablishmentKey;
                paymentType[i].GEPaymentType = null;
                paymentType[i].GEUsers = null;

                paymentType[i] = Mapper.Map<GEUserPayType, GEUserPayTypeVM>(await CreateOrUpdateAsync(paymentType[i]));
            }

            return Mapper.Map<GEUserPayTypeVM[], GEUserPayType[]>(paymentType);
        }

        public async Task<GEUserPayType[]> RemoveListAsync(string pEstablishmentKey, GEUserPayTypeVM[] paymentType)
        {
            for (int i = 0, length = paymentType.Length; i < length; i++)
            {
                if (paymentType[i].Id < 0)
                    paymentType[i].Id = 0;

                paymentType[i] = Mapper.Map<GEUserPayType, GEUserPayTypeVM>(await RemoveById(
                    paymentType[i].Id,
                    pEstablishmentKey,
                    paymentType[i].UniqueKey
                ));
            }

            return Mapper.Map<GEUserPayTypeVM[], GEUserPayType[]>(paymentType);
        }
    }
}
