﻿using AutoMapper;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.Interfaces.General.Demograph;
using Hino.Salesforce.Application.Interfaces.Route;
using Hino.Salesforce.Application.Utils.CEP;
using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Application.ViewModels.General.Demograph;
using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General
{
    public class CEPAS : ICEPAS
    {
        public List<ModelException> Errors { get; set; }
        private Request _Request;
        private readonly IGECountriesAS _IGECountriesAS;
        private readonly IHereMaps _IHereMaps;

        public CEPAS(
            IHereMaps pIHereMaps,
            IGECountriesAS pIGECountriesAS)
        {
            _IHereMaps = pIHereMaps;
            _IGECountriesAS = pIGECountriesAS;
            _Request = new Request();
            Errors = new List<ModelException>();
        }

        public async Task<CEPVM> GetCEPAsync(string pCEP)
        {
            var resultCEP = new CEPVM();

            if (pCEP.IsEmpty())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "CEP",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.InvalidCEP },
                    Value = ""
                });
                return resultCEP;
            }

            try
            {
                var client = _Request.CreateClient();
                var request = _Request.CreateRequest($"{pCEP.OnlyNumbers()}/json", Method.GET);

                var Iresponse = await client.ExecuteAsync(request);

                resultCEP = JsonConvert.DeserializeObject<CEPVM>(Iresponse.Content, new JsonSerializerSettings
                {
                    Culture = new System.Globalization.CultureInfo("pt-BR"),
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    DateFormatString = "dd/MM/yyyy HH:mm:ss"
                });

                resultCEP.Country = Mapper.Map<GECountries, GECountriesVM>((await _IGECountriesAS.QueryAsync(r => r.Initials == "BR")).FirstOrDefault());
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "CEP",
                    Messages = new[] { string.Format(Infra.Cross.Resources.RouterMessagesResource.StatusNoOK, e.Message) },
                    Value = ""
                });
                return resultCEP;
            }

            return resultCEP;
        }

        async Task<CEPVM> GetHereMapInfos(CEPVM pCep, string pCountry, string pNumber)
        {
            var GeoAddress = await _IHereMaps.GetAddressAsync(new Application.ViewModels.Route.AddressGeoVM
            {
                City = pCep.localidade,
                Country = pCountry,
                District = pCep.bairro,
                Number = pNumber == "0" ? "" : pNumber,
                State = pCep.uf,
                Street = pCep.logradouro,
                ZipCode = pCep.cep
            });

            if (GeoAddress != null)
                pCep.HereAddress = GeoAddress.bestresult;

            Errors = _IHereMaps.Errors;

            return pCep;
        }

        public async Task<CEPVM> GetCEPAddressAsync(string pCEP, string pNumber, string pCountry, bool pAllInfo)
        {
            var CepAddress = await GetCEPAsync(pCEP);

            if (Errors.Any())
                return CepAddress;

            CepAddress.Country = Mapper.Map<GECountries, GECountriesVM>((await _IGECountriesAS.QueryAsync(r => r.Initials == pCountry)).FirstOrDefault());

            if (pAllInfo)
                CepAddress = await GetHereMapInfos(CepAddress, pCountry, pNumber);

            return CepAddress;
        }

        public async Task<CEPVM> GetCEPAddressEnterpriseAsync(ReceitaWSEnterpriseVM pEnterprise)
        {
            var CepAddress = await GetCEPAsync(pEnterprise.cep);

            if (Errors.Any())
                return CepAddress;

            if (CepAddress.logradouro.IsEmpty())
                CepAddress.logradouro = pEnterprise.logradouro;

            if (CepAddress.bairro.IsEmpty())
                CepAddress.bairro = pEnterprise.bairro;

            if (CepAddress.localidade.IsEmpty())
                CepAddress.localidade = pEnterprise.municipio;

            if (CepAddress.uf.IsEmpty())
                CepAddress.uf = pEnterprise.uf;

            if (CepAddress.complemento.IsEmpty())
                CepAddress.complemento = pEnterprise.complemento;

            CepAddress = await GetHereMapInfos(CepAddress, "BR", pEnterprise.numero);

            return CepAddress;

        }
    }
}
