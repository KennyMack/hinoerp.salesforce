using Hino.Salesforce.Application.Interfaces.General.Demograph;
using Hino.Salesforce.Domain.General.Interfaces.Services.Demograph;
using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General.Demograph
{
    public class GECitiesAS : BaseAppService<GECities>, IGECitiesAS
    {
        private readonly IGECitiesService _IGECitiesService;

        public GECitiesAS(IGECitiesService pIGECitiesService) :
             base(pIGECitiesService)
        {
            _IGECitiesService = pIGECitiesService;
        }

        public override Task<GECities> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(new Infra.Cross.Utils.Exceptions.ModelException
            {
                ErrorCode = (int)Infra.Cross.Utils.Exceptions.EExceptionErrorCodes.InvalidRequest,
                Field = "Id",
                Value = id.ToString(),
                Messages = new string[]
                {
                    Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("RemoveNotAllowed")
                }
            });

            return Task.FromResult(new GECities());

            /*var City = await _IGECitiesService.GetByIdAsync(id, pEstablishmentKey, pUniqueKey);

            if (City == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "GECities",
                    Messages = new[] { "Nenhum registro encontrado" },
                    Value = id.ToString()
                });
                return null;
            }

            _IGECitiesService.Remove(City);
            await _IGECitiesService.SaveChanges();
            return City;*/
        }
    }
}
