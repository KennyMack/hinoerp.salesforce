﻿using Hino.Salesforce.Application.Interfaces.General.Demograph;
using Hino.Salesforce.Application.ViewModels.General.Demograph;
using Hino.Salesforce.Domain.General.Interfaces.Services.Demograph;
using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General.Demograph
{
    public class GECountriesAS : BaseAppService<GECountries>, IGECountriesAS
    {
        private readonly List<CountryBacen> lstCountries;
        private readonly IGECountriesService _IGECountriesService;

        public GECountriesAS(IGECountriesService pIGECountriesService) : base(pIGECountriesService)
        {
            _IGECountriesService = pIGECountriesService;
            lstCountries = new List<CountryBacen>();

            lstCountries.Add(new CountryBacen
            {
                bacen = "0132",
                description = "Afeganistão"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7560",
                description = "África do Sul"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0175",
                description = "Albânia, República da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0230",
                description = "Alemanha"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0370",
                description = "Andorra"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0400",
                description = "Angola"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0418",
                description = "Anguilla"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0434",
                description = "Antigua e Barbuda"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0477",
                description = "Antilhas Holandesas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0531",
                description = "Arábia Saudita"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0590",
                description = "Argélia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0639",
                description = "Argentina"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0647",
                description = "Armênia, República da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0655",
                description = "Aruba"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0698",
                description = "Austrália"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0728",
                description = "Áustria"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0736",
                description = "Azerbaijão, República do"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0779",
                description = "Bahamas, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0809",
                description = "Bahrein, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0817",
                description = "Bangladesh"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0833",
                description = "Barbados"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0850",
                description = "Belarus"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0876",
                description = "Bélgica"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0884",
                description = "Belize"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2291",
                description = "Benin"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0906",
                description = "Bermudas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0973",
                description = "Bolívia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0981",
                description = "Bósnia-Herzegovina"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1015",
                description = "Botsuana"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1058",
                description = "Brasil"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1082",
                description = "Brunei"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1112",
                description = "Bulgária, República da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0310",
                description = "Burkina Faso"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1155",
                description = "Burundi"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1198",
                description = "Butão"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1279",
                description = "Cabo Verde, República de"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1457",
                description = "Camarões"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1414",
                description = "Camboja"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1490",
                description = "Canadá"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1504",
                description = "Canal, Ilhas do (Jersey e Guernsey)"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1511",
                description = "Canárias, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1546",
                description = "Catar "
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1376",
                description = "Cayman, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1538",
                description = "Cazaquistão, República do"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7889",
                description = "Chade"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1589",
                description = "Chile"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1600",
                description = "China, República Popular da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1635",
                description = "Chipre"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5118",
                description = "Christmas, Ilha (Navidad)"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7412",
                description = "Cingapura"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1651",
                description = "Cocos (Keeling), Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1694",
                description = "Colômbia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1732",
                description = "Comores, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8885",
                description = "Congo, República Democrática do"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1775",
                description = "Congo, República do"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1830",
                description = "Cook, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1872",
                description = "Coréia, Rep. Pop. Democrática da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1902",
                description = "Coréia, República da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1937",
                description = "Costa do Marfim"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1961",
                description = "Costa Rica"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1988",
                description = "Coveite"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1953",
                description = "Croácia, República da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1996",
                description = "Cuba"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2321",
                description = "Dinamarca"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7838",
                description = "Djibuti"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2356",
                description = "Dominica, Ilha"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "402",
                description = "Egito"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6874",
                description = "El Salvador"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2445",
                description = "Emirados Árabes Unidos"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2399",
                description = "Equador"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2437",
                description = "Eritréia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6289",
                description = "Escócia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2470",
                description = "Eslovaca, República"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2461",
                description = "Eslovênia, República da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2453",
                description = "Espanha"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2496",
                description = "Estados Unidos"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2518",
                description = "Estônia, República da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2534",
                description = "Etiópia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2550",
                description = "Falkland (Ilhas Malvinas)"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2593",
                description = "Feroe, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8702",
                description = "Fiji"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2674",
                description = "Filipinas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2712",
                description = "Finlândia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "1619",
                description = "Formosa (Taiwan)"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2755",
                description = "França "
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2810",
                description = "Gabão"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6289",
                description = "Gales, País de"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2852",
                description = "Gâmbia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2895",
                description = "Gana"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2917",
                description = "Geórgia, República da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2933",
                description = "Gibraltar"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6289",
                description = "Grã-Bretanha"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "2976",
                description = "Granada"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3018",
                description = "Grécia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3050",
                description = "Groenlândia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3093",
                description = "Guadalupe"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3131",
                description = "Guam"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3174",
                description = "Guatemala"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3379",
                description = "Guiana"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3255",
                description = "Guiana Francesa"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3298",
                description = "Guiné"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3344",
                description = "Guiné-Bissau"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3310",
                description = "Guiné-Equatorial"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3417",
                description = "Haiti"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5738",
                description = "Holanda (Países Baixos)"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3450",
                description = "Honduras"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3514",
                description = "Hong Kong, Região Adm. Especial"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3557",
                description = "Hungria, República da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3573",
                description = "Iêmen"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3611",
                description = "Índia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3654",
                description = "Indonésia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6289",
                description = "Inglaterra"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3727",
                description = "Irã, República Islâmica do"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3697",
                description = "Iraque"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3751",
                description = "Irlanda"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6289",
                description = "Irlanda do Norte"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3794",
                description = "Islândia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3832",
                description = "Israel"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3867",
                description = "Itália"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3883",
                description = "Iugoslávia, República Fed. da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3913",
                description = "Jamaica"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3999",
                description = "Japão"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3964",
                description = "Johnston, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4030",
                description = "Jordânia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4111",
                description = "Kiribati"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4200",
                description = "Laos, Rep. Pop. Democrática do"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4235",
                description = "Lebuan"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4260",
                description = "Lesoto "
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4278",
                description = "Letônia, República da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4316",
                description = "Líbano"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4340",
                description = "Libéria"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4383",
                description = "Líbia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4405",
                description = "Liechtenstein"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4421",
                description = "Lituânia, República da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4456",
                description = "Luxemburgo"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4472",
                description = "Macau"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4499",
                description = "Macedônia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4502",
                description = "Madagascar"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4525",
                description = "Madeira, Ilha da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4553",
                description = "Malásia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4588",
                description = "Malavi"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4618",
                description = "Maldivas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4642",
                description = "Máli"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4677",
                description = "Malta"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "3595",
                description = "Man, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4723",
                description = "Marianas do Norte"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4740",
                description = "Marrocos"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4766",
                description = "Marshall, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4774",
                description = "Martinica"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4855",
                description = "Maurício"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4880",
                description = "Mauritânia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4936",
                description = "México"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "0930",
                description = "Mianmar (Birmânia)"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4995",
                description = "Micronésia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4901",
                description = "Midway, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5053",
                description = "Moçambique"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4944",
                description = "Moldávia, República da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4952",
                description = "Mônaco"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "4979",
                description = "Mongólia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5010",
                description = "Montserrat, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5070",
                description = "Namíbia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5088",
                description = "Nauru"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5177",
                description = "Nepal"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5215",
                description = "Nicarágua"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5258",
                description = "Niger"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5282",
                description = "Nigéria"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5312",
                description = "Niue, Ilha"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5355",
                description = "Norfolk, Ilha"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5380",
                description = "Noruega"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5428",
                description = "Nova Caledônia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5487",
                description = "Nova Zelândia "
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5568",
                description = "Omã"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5738",
                description = "Países Baixos (Holanda)"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5754",
                description = "Palau"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5800",
                description = "Panamá"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5452",
                description = "Papua Nova Guiné"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5762",
                description = "Paquistão"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5860",
                description = "Paraguai"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5894",
                description = "Peru"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5932",
                description = "Pitcairn, Ilha"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5991",
                description = "Polinésia Francesa"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6033",
                description = "Polônia, República da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6114",
                description = "Porto Rico"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6076",
                description = "Portugal"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6238",
                description = "Quênia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6254",
                description = "Quirguiz, República"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6289",
                description = "Reino Unido"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6408",
                description = "República Centro-Africana"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6475",
                description = "República Dominicana"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6602",
                description = "Reunião, Ilha"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6700",
                description = "Romênia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6750",
                description = "Ruanda"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6769",
                description = "Rússia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6858",
                description = "Saara Ocidental"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6777",
                description = "Salomão, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6904",
                description = "Samoa"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6912",
                description = "Samoa Americana"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6971",
                description = "San Marino"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7102",
                description = "Santa Helena"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7153",
                description = "Santa Lúcia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6955",
                description = "São Cristóvão e Neves"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7005",
                description = "São Pedro e Miquelon"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7200",
                description = "São Tomé e Príncipe, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7056",
                description = "São Vicente e Granadinas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7285",
                description = "Senegal"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7358",
                description = "Serra Leoa"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7315",
                description = "Seychelles"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7447",
                description = "Síria, República Árabe da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7480",
                description = "Somália"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7501",
                description = "Sri Lanka"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7544",
                description = "Suazilândia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7595",
                description = "Sudão"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7641",
                description = "Suécia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7676",
                description = "Suíça "
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7706",
                description = "Suriname"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7722",
                description = "Tadjiquistão"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7765",
                description = "Tailândia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7803",
                description = "Tanzânia, República Unida da"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7919",
                description = "Tcheca, República"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7820",
                description = "Território Britânico Oc. Índico"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "7951",
                description = "Timor Leste"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8001",
                description = "Togo"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8109",
                description = "Tonga"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8052",
                description = "Toquelau, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8150",
                description = "Trinidad e Tobago"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8206",
                description = "Tunísia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8230",
                description = "Turcas e Caicos, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8249",
                description = "Turcomenistão, República do"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8273",
                description = "Turquia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8281",
                description = "Tuvalu"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8311",
                description = "Ucrânia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8338",
                description = "Uganda"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8451",
                description = "Uruguai"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8478",
                description = "Uzbequistão, República do"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "5517",
                description = "Vanuatu"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8486",
                description = "Vaticano, Estado da Cidade do"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8508",
                description = "Venezuela"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8583",
                description = "Vietnã"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8630",
                description = "Virgens, Ilhas (Britânicas)"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8664",
                description = "Virgens, Ilhas (E.U.A.)"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8737",
                description = "Wake, Ilha"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8753",
                description = "Wallis e Futuna, Ilhas"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8907",
                description = "Zâmbia"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "6653",
                description = "Zimbábue"
            });
            lstCountries.Add(new CountryBacen
            {
                bacen = "8958",
                description = "Zona do Canal do Panamá"
            });
        }

        public async Task LoadCountriesAsync(GECountriesLoadVM[] pLoad)
        {
            var LocalName = "";
            var Lat = 0M;
            var Lng = 0M;
            var DDI = "";

            foreach (var item in pLoad)
            {
                Lat = 0M;
                Lng = 0M;

                if (item.translations != null)
                    LocalName = (item.translations?.br ?? item.translations?.pt) ?? item.name;

                var bacen = lstCountries.FirstOrDefault(r => r.description.Contains(LocalName))?.bacen ?? "";

                try
                {

                    Lat = item.latlng[0];
                    Lng = item.latlng[1];
                }
                catch
                {
                    Lat = 0M;
                    Lng = 0M;
                }

                try
                {
                    DDI = item.callingCodes.First();
                }
                catch
                {
                    DDI = "";
                }


                _IGECountriesService.Add(new GECountries
                {
                    EstablishmentKey = "85db3229-73a2-4989-b2f3-9caa542443fe",
                    CIOC = item.alpha3Code,
                    Initials = item.alpha2Code,
                    Name = LocalName,
                    LocalName = item.name,
                    Lat = Lat,
                    Lng = Lng,
                    NumericCode = item.numericCode,
                    DDI = DDI,
                    BACEN = bacen.PadLeft(5, '0'),
                    flag = item.flag
                });

            }

            if (!_IGECountriesService.Errors.Any())
                await _IGECountriesService.SaveChanges();
        }

        public override Task<GECountries> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(new Infra.Cross.Utils.Exceptions.ModelException
            {
                ErrorCode = (int)Infra.Cross.Utils.Exceptions.EExceptionErrorCodes.InvalidRequest,
                Field = "Id",
                Value = id.ToString(),
                Messages = new string[]
                   {
                    Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("RemoveNotAllowed")
                   }
            });

            return Task.FromResult(new GECountries());

            /*var Country = await _IGECountriesService.GetByIdAsync(id, pEstablishmentKey, pUniqueKey);

            if (Country == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "GECountries",
                    Messages = new[] { "Nenhum registro encontrado" },
                    Value = id.ToString()
                });
                return null;
            }

            if (Country.GEStates.Count > 0)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterChild,
                    Field = "GEStates",
                    Messages = new[] { "Existem estados vinculados a este País" },
                    Value = id.ToString()
                });
                return null;
            }

            _IGECountriesService.Remove(Country);
            await _IGECountriesService.SaveChanges();
            return Country;*/
        }
    }
}
