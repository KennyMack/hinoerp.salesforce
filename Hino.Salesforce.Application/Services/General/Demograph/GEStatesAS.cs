using Hino.Salesforce.Application.Interfaces.General.Demograph;
using Hino.Salesforce.Application.ViewModels.General.Demograph;
using Hino.Salesforce.Domain.General.Interfaces.Services.Demograph;
using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General.Demograph
{
    public class GEStatesAS : BaseAppService<GEStates>, IGEStatesAS
    {
        private readonly IGEStatesService _IGEStatesService;
        private readonly IGECountriesService _IGECountriesService;

        public GEStatesAS(IGEStatesService pIGEStatesService, IGECountriesService pIGECountriesService) :
             base(pIGEStatesService)
        {
            _IGEStatesService = pIGEStatesService;
            _IGECountriesService = pIGECountriesService;
        }

        public async Task LoadStatesAsync(GEStatesLoadVM[] pLoad)
        {
            foreach (var content in pLoad)
            {
                foreach (var item in content.body.data)
                {
                    var country = (await _IGECountriesService.QueryAsync(r => r.Initials == item.countryCode)).First();

                    _IGEStatesService.Add(new GEStates
                    {
                        EstablishmentKey = "85db3229-73a2-4989-b2f3-9caa542443fe",
                        CountryID = country.Id,
                        CodeFIPS = item.fipsCode,
                        Name = item.name,
                        Initials = item.isoCode
                    });
                }
            }

            if (!_IGEStatesService.Errors.Any())
                await _IGEStatesService.SaveChanges();
        }

        public override Task<GEStates> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(new Infra.Cross.Utils.Exceptions.ModelException
            {
                ErrorCode = (int)Infra.Cross.Utils.Exceptions.EExceptionErrorCodes.InvalidRequest,
                Field = "Id",
                Value = id.ToString(),
                Messages = new string[]
                {
                    Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("RemoveNotAllowed")
                }
            });

            return Task.FromResult(new GEStates());
            /*
            var State = await _IGEStatesService.GetByIdAsync(id, pEstablishmentKey, pUniqueKey);

            if (State == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "GEStates",
                    Messages = new[] { "Nenhum registro encontrado" },
                    Value = id.ToString()
                });
                return null;
            }

            if (State.GECities.Count > 0)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterChild,
                    Field = "GECities",
                    Messages = new[] { "Existem cidades vinculadas a este estado" },
                    Value = id.ToString()
                });
                return null;
            }

            _IGEStatesService.Remove(State);
            await _IGEStatesService.SaveChanges();
            return State;*/
        }
    }
}
