using Hino.Salesforce.Application.Interfaces.General.Events;
using Hino.Salesforce.Domain.General.Events.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Events;

namespace Hino.Salesforce.Application.Services.General.Events
{
    public class GEEnterpriseEventAS : BaseAppService<GEEnterpriseEvent>, IGEEnterpriseEventAS
    {
        private readonly IGEEnterpriseEventService _IGEEnterpriseEventService;

        public GEEnterpriseEventAS(IGEEnterpriseEventService pIGEEnterpriseEventService) : 
             base(pIGEEnterpriseEventService)
        {
            _IGEEnterpriseEventService = pIGEEnterpriseEventService;
        }
    }
}
