using Hino.Salesforce.Application.Interfaces.General.Events;
using Hino.Salesforce.Domain.General.Events.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Events;

namespace Hino.Salesforce.Application.Services.General.Events
{
    public class GEEstabCalendarAS : BaseAppService<GEEstabCalendar>, IGEEstabCalendarAS
    {
        private readonly IGEEstabCalendarService _IGEEstabCalendarService;

        public GEEstabCalendarAS(IGEEstabCalendarService pIGEEstabCalendarService) : 
             base(pIGEEstabCalendarService)
        {
            _IGEEstabCalendarService = pIGEEstabCalendarService;
        }
    }
}
