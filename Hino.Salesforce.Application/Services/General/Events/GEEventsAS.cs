using AutoMapper;
using Hino.Salesforce.Application.Interfaces.General.Events;
using Hino.Salesforce.Application.ViewModels.General.Events;
using Hino.Salesforce.Domain.General.Events.Interfaces.Services;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General.Events
{
    public class GEEventsAS : BaseAppService<GEEvents>, IGEEventsAS
    {
        private readonly IGEEventsClassificationService _IGEEventsClassificationService;
        private readonly IGEUserCalendarService _IGEUserCalendarService;
        private readonly IGEEstabCalendarService _IGEEstabCalendarService;
        private readonly IGEEnterpriseEventService _IGEEnterpriseEventService;
        private readonly IGEEventsService _IGEEventsService;
        private readonly IGEUsersService _IGEUsersService;
        private readonly IGEEstablishmentsService _IGEEstablishmentsService;
        private readonly IGEEnterprisesService _IGEEnterprisesService;

        public GEEventsAS(IGEEventsService pIGEEventsService,
            IGEUsersService pIGEUsersService,
            IGEEstablishmentsService pIGEEstablishmentsService,
            IGEEnterprisesService pIGEEnterprisesService,
            IGEUserCalendarService pIGEUserCalendarService,
            IGEEstabCalendarService pIGEEstabCalendarService,
            IGEEnterpriseEventService pIGEEnterpriseEventService,
            IGEEventsClassificationService pIGEEventsClassificationService
            ) : 
             base(pIGEEventsService)
        {
            _IGEEstablishmentsService = pIGEEstablishmentsService;
            _IGEEnterprisesService = pIGEEnterprisesService;
            _IGEEventsService = pIGEEventsService;
            _IGEUsersService = pIGEUsersService;
            _IGEUserCalendarService = pIGEUserCalendarService;
            _IGEEstabCalendarService = pIGEEstabCalendarService;
            _IGEEnterpriseEventService = pIGEEnterpriseEventService;
            _IGEEventsClassificationService = pIGEEventsClassificationService;
        }

        async Task<bool> ValidadeAsync(GECreateEventsVM pGEEventsVM)
        {
            if (pGEEventsVM.UserID != null)
            {
                var user = await _IGEUsersService.QueryAsync(r =>
                    r.EstablishmentKey == pGEEventsVM.EstablishmentKey &&
                    r.Id == pGEEventsVM.UserID
                );
                if (!user.Any())
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                        Field = "UserID",
                        Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.UserNotFound },
                        Value = pGEEventsVM.UserID.ToString()
                    });
                    return false;
                }
            }

            if (pGEEventsVM.EstabID != null)
            {
                var estab = await _IGEEstablishmentsService.QueryAsync(r =>
                    r.EstablishmentKey == pGEEventsVM.EstablishmentKey &&
                    r.Id == pGEEventsVM.EstabID
                );
                if (!estab.Any())
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                        Field = "EstabID",
                        Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.InvalidInternalEstabID },
                        Value = pGEEventsVM.EstabID.ToString()
                    });
                    return false;
                }
            }

            if (pGEEventsVM.EnterpriseID != null)
            {
                var enterprise = await _IGEEnterprisesService.QueryAsync(r =>
                    r.EstablishmentKey == pGEEventsVM.EstablishmentKey &&
                    r.Id == pGEEventsVM.EnterpriseID
                );
                if (!enterprise.Any())
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                        Field = "EnterpriseID",
                        Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.EnterpriseIdNotFound },
                        Value = pGEEventsVM.EnterpriseID.ToString()
                    });
                    return false;
                }
            }

            if (pGEEventsVM.UserID == null &&
                pGEEventsVM.EstabID == null &&
                pGEEventsVM.EnterpriseID == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Body",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.EventReferenceNotInformed },
                    Value = ""
                });
                return false;
            }

            if (pGEEventsVM.ClassificationID != null)
            {
                var classification = await _IGEEventsClassificationService.QueryAsync(r =>
                    r.EstablishmentKey == pGEEventsVM.EstablishmentKey &&
                    r.Id == pGEEventsVM.ClassificationID
                );
                if (!classification.Any())
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                        Field = "ClassificationID",
                        Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.EventClassifNotFound },
                        Value = pGEEventsVM.ClassificationID.ToString()
                    });
                    return false;
                }
            }

            return true;
        }

        public async Task<GEEventsVM> SaveReferenceAsync(string pEstablishmentKey, GEEvents Event, GECreateEventsVM pGEEventsVM)
        {
            if (pGEEventsVM.UserID != null)
            {
                _IGEUserCalendarService.Add(new GEUserCalendar
                {
                    UserID = (long)pGEEventsVM.UserID,
                    EventID = Event.Id,
                    EstablishmentKey = pEstablishmentKey
                });

                Errors = _IGEUserCalendarService.Errors;
                await _IGEUserCalendarService.SaveChanges();

                if (Errors.Any())
                    return null;
            }

            if (pGEEventsVM.EstabID != null)
            {
                _IGEEstabCalendarService.Add(new GEEstabCalendar
                {
                    EstabID = (long)pGEEventsVM.EstabID,
                    EventID = Event.Id,
                    EstablishmentKey = pEstablishmentKey
                });

                Errors = _IGEEstabCalendarService.Errors;
                await _IGEEstabCalendarService.SaveChanges();

                if (Errors.Any())
                    return null;
            }

            if (pGEEventsVM.EnterpriseID != null)
            {
                _IGEEnterpriseEventService.Add(new GEEnterpriseEvent
                {
                    EnterpriseID = (long)pGEEventsVM.EnterpriseID,
                    EventID = Event.Id,
                    EstablishmentKey = pEstablishmentKey
                });

                Errors = _IGEEnterpriseEventService.Errors;
                await _IGEEnterpriseEventService.SaveChanges();

                if (Errors.Any())
                    return null;
            }
            return pGEEventsVM;
        }

        private async Task RemoveReferencesAsync(GEEvents Event)
        {
            foreach (var User in Event.GEUserCalendar)
            {
                await _IGEUserCalendarService.RemoveById(User.Id, User.EstablishmentKey, User.UniqueKey);
                await _IGEUserCalendarService.SaveChanges();
                Errors = _IGEUserCalendarService.Errors;
            }

            foreach (var Estab in Event.GEEstabCalendar)
            {
                await _IGEEstabCalendarService.RemoveById(Estab.Id, Estab.EstablishmentKey, Estab.UniqueKey);
                await _IGEEstabCalendarService.SaveChanges();
                Errors = _IGEEstabCalendarService.Errors;
            }

            foreach (var Enterprise in Event.GEEnterpriseEvent)
            {
                await _IGEEnterpriseEventService.RemoveById(Enterprise.Id, Enterprise.EstablishmentKey, Enterprise.UniqueKey);
                await _IGEEnterpriseEventService.SaveChanges();
                Errors = _IGEEnterpriseEventService.Errors;
            }
        }

        public async Task<GEEventsVM> UpdateEventAsync(string pEstablishmentKey, GECreateEventsVM pGEEventsVM)
        {
            Errors.Clear();
            pGEEventsVM.EstablishmentKey = pEstablishmentKey;

            await ValidadeAsync(pGEEventsVM);

            if (Errors.Any())
                return null;
            var Event = Mapper.Map<GECreateEventsVM, GEEvents>(pGEEventsVM);

            var EventDB = GetByIdToUpdate(Event.Id, Event.EstablishmentKey, Event.UniqueKey);
            EventDB.Id = Event.Id;
            EventDB.Type = Event.Type;
            EventDB.Title = Event.Title;
            EventDB.Description = Event.Description;
            EventDB.DtCalendar = Event.DtCalendar;
            EventDB.Start = Event.Start ?? Event.DtCalendar;
            EventDB.End = Event.End;
            EventDB.ZipCode = Event.ZipCode;
            EventDB.Address = Event.Address;
            EventDB.District = Event.District;
            EventDB.Num = Event.Num;
            EventDB.Complement = Event.Complement;
            EventDB.DisplayLat = Event.DisplayLat;
            EventDB.DisplayLng = Event.DisplayLng;
            EventDB.NavLat = Event.NavLat;
            EventDB.NavLng = Event.NavLng;
            EventDB.CityName = Event.CityName;
            EventDB.StateName = Event.StateName;
            EventDB.UF = Event.UF;
            EventDB.IBGE = Event.IBGE;
            EventDB.Email = Event.Email;
            EventDB.Phone = Event.Phone;
            EventDB.ClassificationID = Event.ClassificationID;
            EventDB.Priority = Event.Priority;
            EventDB.MainEventID = Event.MainEventID;
            EventDB.OriginEventID = Event.OriginEventID;
            EventDB.IsComplete = Event.IsComplete;
            EventDB.IsSuccess = Event.IsSuccess;
            EventDB.EstablishmentKey = Event.EstablishmentKey;
            EventDB.UniqueKey = Event.UniqueKey;
            EventDB.Created = Event.Created;
            EventDB.Modified = Event.Modified;
            EventDB.IsActive = Event.IsActive;
            EventDB.Contact = Event.Contact;
            EventDB.Sector = Event.Sector;


            EventDB.GEEnterpriseEvent = null;
            EventDB.GEEstabCalendar = null;
            EventDB.GEEventsClassification = null;
            EventDB.GEEstabCalendar = null;
            Event = _IGEEventsService.Update(EventDB);

            Errors = _IGEEventsService.Errors;

            if (Errors.Any())
                return null;

            await _IGEEventsService.SaveChanges();

            Errors = _IGEEventsService.Errors;

            if (Errors.Any())
                return null;

            EventDB = await GetByIdAsync(Event.Id, Event.EstablishmentKey, Event.UniqueKey);

            await RemoveReferencesAsync(EventDB);

            if (Errors.Any())
                return null;

            await SaveReferenceAsync(pEstablishmentKey, Event, pGEEventsVM);

            if (Errors.Any())
                return null;

            return Mapper.Map<GEEventsVM>(
                await GetByIdAsync(Event.Id, Event.EstablishmentKey, Event.UniqueKey, r => r.GEEventsClassification));
        }

        public async Task<GEEventsVM> CreateEventAsync(string pEstablishmentKey, GECreateEventsVM pGEEventsVM)
        {
            Errors.Clear();
            pGEEventsVM.EstablishmentKey = pEstablishmentKey;

            await ValidadeAsync(pGEEventsVM);

            if (Errors.Any())
                return null;

            var Event = Mapper.Map<GECreateEventsVM, GEEvents>(pGEEventsVM);

            Event.Start = Event.Start ?? Event.DtCalendar;
            Event.UniqueKey = Guid.NewGuid().ToString();
            Event.Id = await _IGEEventsService.NextSequenceAsync();

            if (Event.MainEventID == null)
                Event.MainEventID = Event.Id;

            Event = _IGEEventsService.Add(Event);

            Errors = _IGEEventsService.Errors;

            if (Errors.Any())
                return null;

            await _IGEEventsService.SaveChanges();

            Errors = _IGEEventsService.Errors;

            if (Errors.Any())
                return null;

            await SaveReferenceAsync(pEstablishmentKey, Event, pGEEventsVM);

            if (Errors.Any())
                return null;

            return Mapper.Map<GEEventsVM>(
                await GetByIdAsync(Event.Id, Event.EstablishmentKey, Event.UniqueKey, r => r.GEEventsClassification));
        }

        async Task<IEnumerable<GEEvents>> GetEventDirectLinkedAsync(string pEstablishmentKey, long id) =>
           await _IGEEventsService.QueryAsync(r => (r.MainEventID == id || r.OriginEventID == id) &&
               r.EstablishmentKey == pEstablishmentKey);

        public async Task<GEEventsVM> RemoveEventIdAsync(long pId, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Clear();

            var Event = await GetByIdAsync(pId, pEstablishmentKey, pUniqueKey);

            if (Event.IsComplete || Event.IsSuccess)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Id",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.CompletedEventCantBeDeleted },
                    Value = pId.ToString()
                });
                return null;
            }

            var EventLinked = await GetEventDirectLinkedAsync(pEstablishmentKey, Event.Id);

            if (EventLinked.Any(r => r.Id != Event.Id))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Id",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.EventHasChild },
                    Value = pId.ToString()
                });
                return null;
            }

            await RemoveReferencesAsync(Event);

            if (Errors.Any())
                return null;

            await _IGEEventsService.RemoveById(pId, pEstablishmentKey, pUniqueKey);

            if (_IGEEventsService.Errors.Any())
                return null;

            await _IGEEventsService.SaveChanges();

            if (_IGEEventsService.Errors.Any())
                return null;

            return Mapper.Map<GEEventsVM>(Event);
        }

        public async Task<GEEventsVM> ChangeEventStatusAsync(long pId, string pEstablishmentKey, string pUniqueKey, GEEventsStatusVM pEventStatus)
        {
            Errors.Clear();

            var Event = GetByIdToUpdate(pId, pEstablishmentKey, pUniqueKey);

            Event.IsComplete = pEventStatus.IsComplete;
            Event.IsSuccess = pEventStatus.IsSuccess;

            Event = _IGEEventsService.Update(Event);

            Errors = _IGEEventsService.Errors;

            if (Errors.Any())
                return null;

            await _IGEEventsService.SaveChanges();

            Errors = _IGEEventsService.Errors;

            if (Errors.Any())
                return null;

            return Mapper.Map<GEEventsVM>(
                await GetByIdAsync(Event.Id, Event.EstablishmentKey, Event.UniqueKey, r => r.GEEventsClassification));
        }

        public async Task<GEEventsVM> GenerateEventChildAsync(long pId, string pEstablishmentKey, string pUniqueKey, GEEventsChildVM pEventsChild)
        {
            Errors.Clear();

            var Event = GetById(pId, pEstablishmentKey, pUniqueKey);

            long MainID = Event.MainEventID ?? Event.Id;

            var EventsLinked = await GetEventDirectLinkedAsync(pEstablishmentKey, MainID);

            var EventDestiny = new GEEvents();

            EventDestiny.Id = 0;
            EventDestiny.Type = pEventsChild.Event.Type;
            EventDestiny.Title = pEventsChild.Event.Title;
            EventDestiny.Description = pEventsChild.Event.Description;
            EventDestiny.DtCalendar = pEventsChild.Event.DtCalendar;
            EventDestiny.Start = pEventsChild.Event.Start ?? pEventsChild.Event.DtCalendar;
            EventDestiny.End = pEventsChild.Event.End;
            EventDestiny.ZipCode = pEventsChild.Event.ZipCode;
            EventDestiny.Address = pEventsChild.Event.Address;
            EventDestiny.District = pEventsChild.Event.District;
            EventDestiny.Num = pEventsChild.Event.Num;
            EventDestiny.Complement = pEventsChild.Event.Complement;
            EventDestiny.DisplayLat = pEventsChild.Event.DisplayLat;
            EventDestiny.DisplayLng = pEventsChild.Event.DisplayLng;
            EventDestiny.NavLat = pEventsChild.Event.NavLat;
            EventDestiny.NavLng = pEventsChild.Event.NavLng;
            EventDestiny.CityName = pEventsChild.Event.CityName;
            EventDestiny.StateName = pEventsChild.Event.StateName;
            EventDestiny.UF = pEventsChild.Event.UF;
            EventDestiny.IBGE = pEventsChild.Event.IBGE;
            EventDestiny.Email = pEventsChild.Event.Email;
            EventDestiny.Phone = pEventsChild.Event.Phone;
            EventDestiny.ClassificationID = pEventsChild.Event.ClassificationID;
            EventDestiny.Priority = pEventsChild.Event.Priority;

            EventDestiny.Id = 0;
            EventDestiny.IsComplete = false;
            EventDestiny.IsSuccess = false;
            EventDestiny.Created = DateTime.Now;
            EventDestiny.Modified = DateTime.Now;
            EventDestiny.IsActive = true;
            EventDestiny.EstablishmentKey = pEstablishmentKey;
            EventDestiny.UniqueKey = Guid.NewGuid().ToString();
            EventDestiny.OriginEventID = Event.Id;
            EventDestiny.MainEventID = MainID;

            EventDestiny.GEEnterpriseEvent = null;
            EventDestiny.GEEstabCalendar = null;
            EventDestiny.GEEventsClassification = null;
            EventDestiny.GEEstabCalendar = null;

            EventDestiny = _IGEEventsService.Add(EventDestiny);

            Errors = _IGEEventsService.Errors;

            if (Errors.Any())
                return null;

            await _IGEEventsService.SaveChanges();

            Errors = _IGEEventsService.Errors;

            if (Errors.Any())
                return null;

            foreach (var User in Event.GEUserCalendar)
            {
                _IGEUserCalendarService.Add(new GEUserCalendar
                {
                    UserID = User.UserID,
                    EventID = EventDestiny.Id,
                    EstablishmentKey = pEstablishmentKey
                });

                Errors = _IGEUserCalendarService.Errors;
                await _IGEUserCalendarService.SaveChanges();

                if (Errors.Any())
                    return null;
            }

            foreach (var Estab in Event.GEEstabCalendar)
            {
                _IGEEstabCalendarService.Add(new GEEstabCalendar
                {
                    EstabID = Estab.EstabID,
                    EventID = EventDestiny.Id,
                    EstablishmentKey = pEstablishmentKey
                });

                Errors = _IGEEstabCalendarService.Errors;
                await _IGEEstabCalendarService.SaveChanges();

                if (Errors.Any())
                    return null;
            }

            foreach (var Enterprise in Event.GEEnterpriseEvent)
            {
                _IGEEnterpriseEventService.Add(new GEEnterpriseEvent
                {
                    EnterpriseID = Enterprise.EnterpriseID,
                    EventID = EventDestiny.Id,
                    EstablishmentKey = pEstablishmentKey
                });

                Errors = _IGEEnterpriseEventService.Errors;
                await _IGEEnterpriseEventService.SaveChanges();

                if (Errors.Any())
                    return null;
            }

            var OriginDB = GetByIdToUpdate(pId, pEstablishmentKey, pUniqueKey);
            OriginDB.IsComplete = pEventsChild.IsComplete;
            OriginDB.IsSuccess = pEventsChild.IsSuccess;
            OriginDB = _IGEEventsService.Update(OriginDB);

            Errors = _IGEEventsService.Errors;

            if (Errors.Any())
                return null;

            await _IGEEventsService.SaveChanges();

            Errors = _IGEEventsService.Errors;

            if (Errors.Any())
                return null;

            return Mapper.Map<GEEventsVM>(
                await GetByIdAsync(EventDestiny.Id, EventDestiny.EstablishmentKey, EventDestiny.UniqueKey, r => r.GEEventsClassification));
        }
    }
}
