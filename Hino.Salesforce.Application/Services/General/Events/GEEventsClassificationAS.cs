using Hino.Salesforce.Application.Interfaces.General.Events;
using Hino.Salesforce.Domain.General.Events.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Events;

namespace Hino.Salesforce.Application.Services.General.Events
{
    public class GEEventsClassificationAS : BaseAppService<GEEventsClassification>, IGEEventsClassificationAS
    {
        private readonly IGEEventsClassificationService _IGEEventsClassificationService;

        public GEEventsClassificationAS(IGEEventsClassificationService pIGEEventsClassificationService) : 
             base(pIGEEventsClassificationService)
        {
            _IGEEventsClassificationService = pIGEEventsClassificationService;
        }
    }
}
