using Hino.Salesforce.Application.Interfaces.General.Events;
using Hino.Salesforce.Domain.General.Events.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Events;

namespace Hino.Salesforce.Application.Services.General.Events
{
    public class GEUserCalendarAS : BaseAppService<GEUserCalendar>, IGEUserCalendarAS
    {
        private readonly IGEUserCalendarService _IGEUserCalendarService;

        public GEUserCalendarAS(IGEUserCalendarService pIGEUserCalendarService) : 
             base(pIGEUserCalendarService)
        {
            _IGEUserCalendarService = pIGEUserCalendarService;
        }
    }
}
