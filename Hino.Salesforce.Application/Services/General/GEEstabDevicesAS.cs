using AutoMapper;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General
{
    public class GEEstabDevicesAS : BaseAppService<GEEstabDevices>, IGEEstabDevicesAS
    {
        private readonly IGEEstabDevicesService _IGEEstabDevicesService;

        public GEEstabDevicesAS(IGEEstabDevicesService pIGEEstabDevicesService) :
             base(pIGEEstabDevicesService)
        {
            _IGEEstabDevicesService = pIGEEstabDevicesService;
        }

        public async Task<GEEstabDevicesVM> CreateDeviceAsync(GEEstabDevicesVM model)
        {
            var result = await _IGEEstabDevicesService.CreateDeviceAsync(
                Mapper.Map<GEEstabDevicesVM, GEEstabDevices>(model));

            return Mapper.Map<GEEstabDevices, GEEstabDevicesVM>(result);
        }

        public override async Task<GEEstabDevices> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            var Devices = await _IGEEstabDevicesService.GetByIdAsync(id, pEstablishmentKey, pUniqueKey);

            if (Devices == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "GEEstabDevices",
                    Messages = new[] { "Nenhum registro encontrado" },
                    Value = id.ToString()
                });
                return null;
            }

            _IGEEstabDevicesService.Remove(Devices);
            await _IGEEstabDevicesService.SaveChanges();
            return Devices;
        }
    }
}
