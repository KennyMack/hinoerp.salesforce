using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;

namespace Hino.Salesforce.Application.Services.General
{
    public class GEEstabMenuAS : BaseAppService<GEEstabMenu>, IGEEstabMenuAS
    {
        private readonly IGEEstabMenuService _IGEEstabMenuService;

        public GEEstabMenuAS(IGEEstabMenuService pIGEEstabMenuService) :
             base(pIGEEstabMenuService)
        {
            _IGEEstabMenuService = pIGEEstabMenuService;
        }
    }
}
