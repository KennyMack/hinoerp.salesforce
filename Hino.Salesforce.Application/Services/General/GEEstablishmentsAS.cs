using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General
{
    public class GEEstablishmentsAS : BaseAppService<GEEstablishments>, IGEEstablishmentsAS
    {
        private readonly IGEEstablishmentsService _IGEEstablishmentsService;
        private readonly IGEEstabDevicesService _IGEEstabDevicesService;
        private readonly IGEEstabMenuService _IGEEstabMenuService;

        public GEEstablishmentsAS(IGEEstablishmentsService pIGEEstablishmentsService,
            IGEEstabDevicesService pIGEEstabDevicesService,
            IGEEstabMenuService pIGEEstabMenuService) :
             base(pIGEEstablishmentsService)
        {
            _IGEEstabDevicesService = pIGEEstabDevicesService;
            _IGEEstablishmentsService = pIGEEstablishmentsService;
            _IGEEstabMenuService = pIGEEstabMenuService;
        }

        public override GEEstablishments Add(GEEstablishments model)
        {
            Errors.Add(new ModelException
            {
                ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                Field = "Id",
                Value = "0",
                Messages = new string[]
                {
                    Infra.Cross.Resources.ErrorMessagesResource.CreateNotAllowed
                }
            });

            return model;
        }

        public async Task<GEEstablishments> CreateEstablishmentAsync(GEEstablishmentsCreateVM pEstab)
        {
            var Estab = new GEEstablishments
            {
                RazaoSocial = pEstab.RazaoSocial,
                NomeFantasia = pEstab.NomeFantasia,
                Email = pEstab.Email,
                Phone = pEstab.Phone,
                CNPJCPF = pEstab.CNPJCPF,
                Devices = pEstab.Devices,
                EstablishmentKey = Guid.NewGuid().ToString(),
                PIS = pEstab.PIS,
                COFINS = pEstab.COFINS,
                DaysPayment = 0,
                SearchPosition = 0,
                OnlyOnDate = false,
                OnlyWithStock = false,
                AditionalInfo = "Dado Adicional",
                FatorR = false,
            };

            _IGEEstablishmentsService.Add(Estab);
            await _IGEEstablishmentsService.SaveChanges();

            return Estab;
        }

        public async Task<GEEstablishments> UpdateEstablishmentAsync(GEEstablishmentsCreateVM pEstab)
        {
            _IGEEstablishmentsService.Errors.Clear();
            _IGEEstabMenuService.Errors.Clear();

            var Estab = new GEEstablishments
            {
                RazaoSocial = pEstab.RazaoSocial,
                NomeFantasia = pEstab.NomeFantasia,
                Email = pEstab.Email,
                Phone = pEstab.Phone,
                CNPJCPF = pEstab.CNPJCPF,
                Devices = pEstab.Devices,
                EstablishmentKey = pEstab.EstablishmentKey,
                Id = pEstab.Id,
                UniqueKey = pEstab.UniqueKey,
                IsActive = pEstab.IsActive,
                PIS = pEstab.PIS,
                COFINS = pEstab.COFINS,
                DefaultFiscalOperID = pEstab.DefaultFiscalOperID,
                DefaultNoteOrder = pEstab.DefaultNoteOrder,
                AditionalInfo = pEstab.AditionalInfo,
                CapptaKey = pEstab.CapptaKey,
                SitefIp = pEstab.SitefIp,
                FatorR = pEstab.FatorR,
                TokenCNPJ = pEstab.TokenCNPJ,
                AllowChangePrice = pEstab.AllowChangePrice,
                AllowDiscount = pEstab.AllowDiscount,
                AllowEnterprise = pEstab.AllowEnterprise,
                AllowPayment = pEstab.AllowPayment,
                OnlyWithStock = pEstab.OnlyWithStock,
                OnlyOnDate = pEstab.OnlyOnDate,
                PfFiscalGroupId = pEstab.PfFiscalGroupId,
                PjFiscalGroupId = pEstab.PjFiscalGroupId,
                PfClassifClient = pEstab.PfClassifClient,
                PjClassifClient = pEstab.PjClassifClient,
                DaysPayment = pEstab.DaysPayment,
                SearchPosition = pEstab.SearchPosition,
                DefaultProductId = pEstab.DefaultProductId,
                UseChangePrice = pEstab.UseChangePrice,
                UsePayCondition = pEstab.UsePayCondition,
                UseBonification = pEstab.UseBonification,
                UseSample = pEstab.UseSample
            };

            if (_IGEEstabDevicesService.DevicesCount(Estab.Id, Estab.EstablishmentKey) > Estab.Devices)
            {
                var resource = new System.Resources.ResourceManager(typeof(Infra.Cross.Resources.ErrorMessagesResource));

                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterChild,
                    Field = "Devices",
                    Messages = new[] { resource.GetString("TotalDeviceIsGreater") },
                    Value = Estab.Devices.ToString()
                });
                return Estab;
            }

            var result = await _IGEEstablishmentsService.UpdateEstablishmentAsync(Estab);

            if (!_IGEEstablishmentsService.Errors.Any())
            {
                foreach (var item in pEstab.GEEstabMenu)
                {
                    var estabMenu = new GEEstabMenu();
                    if (item.Id > 0)
                        estabMenu = _IGEEstabMenuService.GetByIdToUpdate(item.Id, item.EstablishmentKey, item.UniqueKey);

                    estabMenu.GEEstabID = result.Id;
                    estabMenu.EstablishmentKey = Estab.EstablishmentKey;
                    estabMenu.Name = item.Name;
                    estabMenu.Menu = item.Menu;
                    estabMenu.Description = item.Description;
                    estabMenu.Visible = item.Visible;

                    if (item.Id <= 0)
                        _IGEEstabMenuService.Add(estabMenu);
                    else
                    {
                        estabMenu.UniqueKey = item.UniqueKey;

                        _IGEEstabMenuService.Update(estabMenu);
                    }
                }

                await _IGEEstabMenuService.SaveChanges();
            }

            return _IGEEstablishmentsService.GetById(result.Id, result.EstablishmentKey, result.UniqueKey,
                s => s.GEEstabDevices,
                m => m.GEEstabMenu,
                p => p.FSFiscalOper);
        }

        public async Task<IEnumerable<GEEstablishments>> GetByIdAndEstablishmentKeyAsync(long pId, string pEstablishmentKey) =>
            await _IGEEstablishmentsService.GetByIdAndEstablishmentKeyAsync(pId, pEstablishmentKey);

        public async Task<bool> HasEstablishmentDependencyAsync(string pEstablishmentKey) =>
            await _IGEEstablishmentsService.HasEstablishmentDependencyAsync(pEstablishmentKey);

        public override async Task<GEEstablishments> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            var result = await HasEstablishmentDependencyAsync(pEstablishmentKey);

            if (result)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterChild,
                    Field = "GEEstablishments",
                    Messages = new[] { "Existem registros vinculados a esse estabelecimento" },
                    Value = id.ToString()
                });
                return null;
            }

            var estab = await _IGEEstablishmentsService.RemoveById(id, pEstablishmentKey, pUniqueKey);
            await _IGEEstablishmentsService.SaveChanges();
            return estab;
        }

        public async Task<bool> ExistsEstablishmentAsync(string pEstablishmentKey) =>
            await _IGEEstablishmentsService.ExistsEstablishmentAsync(pEstablishmentKey);

        public async Task<IEnumerable<GEEstabDevices>> GetDevicesEstablishment(string pEstablishmentKey, string pUniqueKey, long pId) =>
            await _IGEEstablishmentsService.GetDevicesEstablishment(pEstablishmentKey, pUniqueKey, pId);

        public async Task<GEEstablishments> GetByEstablishmentKeyAsync(string pEstablishmentKey) =>
            await _IGEEstablishmentsService.GetByEstablishmentKeyAsync(pEstablishmentKey);
    }
}
