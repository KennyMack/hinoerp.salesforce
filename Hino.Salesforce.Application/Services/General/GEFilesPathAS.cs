using Hino.Salesforce.Application.Interfaces.AWS;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Hino.Salesforce.Infra.Cross.Utils.Files;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace Hino.Salesforce.Application.Services.General
{
    public class GEFilesPathAS : BaseAppService<GEFilesPath>, IGEFilesPathAS
    {
        private readonly IGEFilesPathService _IGEFilesPathService;
        private readonly IGEProductsService _IGEProductsService;
        private readonly IAWSFileAS _IAWSFileAS;

        public GEFilesPathAS(IGEFilesPathService pIGEFilesPathService,
            IGEProductsService pIGEProductsService,
            IAWSFileAS pIAWSFileAS) :
             base(pIGEFilesPathService)
        {
            _IGEFilesPathService = pIGEFilesPathService;
            _IGEProductsService = pIGEProductsService;
            _IAWSFileAS = pIAWSFileAS;
        }

        #region Generate UploadFolder
        private string GenerateUploadFolder(UploadDestinationFolder pFolder, string pUploadFolder = "")
        {
            var destination = UploadFolder.GenerateDestinationFolder(pFolder);

            if (destination.IsEmpty())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "GEFilesPath",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.UploadFolderIsNotCreated },
                    Value = ""
                });

                return null;
            }

            if (!pUploadFolder.IsEmpty())
            {
                var UploadDir = Path.Combine(destination, pUploadFolder);

                if (!Directory.Exists(UploadDir))
                    Directory.CreateDirectory(UploadDir);

                destination = UploadDir;
            }
            return destination;
        }
        #endregion

        public async Task<GEResultUploadFileVM> UploadFilesAsync(UploadDestinationFolder pFolder, HttpFileCollection pFiles, BaseVM pModel)
        {
            if (pFolder == UploadDestinationFolder.NotIdentified)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "GEFilesPath",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.FileNotFound },
                    Value = pModel.Id.ToString()
                });
                return null;
            }

            var destination = GenerateUploadFolder(pFolder, Guid.NewGuid().ToString("N"));

            if (Errors.Any())
                return null;

            var result = new GEResultUploadFileVM();

            foreach (string file in pFiles)
            {
                var item = new GEResulUploadItemVM();
                HttpPostedFile postedFile = null;
                string currpath = "";
                string AwsKey = "";
                try
                {
                    postedFile = pFiles[file];
                    currpath = Path.Combine(destination, postedFile.FileName);
                    item.FileName = postedFile.FileName;
                    item.Identifier = Guid.NewGuid().ToString();

                    postedFile.SaveAs(currpath);
                    FileInfo fi = new FileInfo(currpath);
                    item.Extension = fi.Extension;
                    item.FileSize = fi.Length;
                    item.Success = true;
                    item.MimeType = MimeMapping.GetMimeMapping(currpath);
                    var date = DateTime.Now.ToString("ddMMyyyyHHmm");
                    AwsKey = $@"{pModel.EstablishmentKey}{pFolder}{item.Identifier}{date}";
                }
                catch (System.Exception ex)
                {
                    Logging.Exception(ex);
                    item.Success = false;
                    item.ErrorMessage = ex.Message;
                }

                if (item.Success)
                {
                    _IAWSFileAS.Errors.Clear();
                    if (!await _IAWSFileAS.UploadFileAsync(currpath, AwsKey))
                    {
                        item.Success = false;
                        Errors.Add(_IAWSFileAS.Errors.FirstOrDefault());
                    }
                }

                if (item.Success)
                {
                    var FilesPath = new GEFilesPath
                    {
                        EstablishmentKey = pModel.EstablishmentKey,
                        Description = "",
                        Name = postedFile.FileName,
                        Identifier = item.Identifier,
                        FileSize = item.FileSize,
                        MimeType = item.MimeType,
                        Path = AwsKey,
                        Extension = item.Extension
                    };

                    switch (pFolder)
                    {
                        case UploadDestinationFolder.Users:
                            FilesPath.UserID = pModel.Id;
                            break;
                        case UploadDestinationFolder.Products:
                            FilesPath.ProductID = pModel.Id;
                            break;
                        case UploadDestinationFolder.Enterprises:
                            FilesPath.EnterpriseID = pModel.Id;
                            break;
                        case UploadDestinationFolder.Establishments:
                            FilesPath.GEEstabID = pModel.Id;
                            break;
                        case UploadDestinationFolder.Order:
                            FilesPath.OrderID = pModel.Id;
                            break;
                        case UploadDestinationFolder.OrderItem:
                            FilesPath.OrderItemID = pModel.Id;
                            break;
                    }

                    _IGEFilesPathService.Add(FilesPath);

                    await _IGEFilesPathService.SaveChanges();
                }

                result.Files.Add(item);
            }

            try
            {
                Directory.Delete(destination, true);
            }
            catch (Exception)
            {

            }

            return result;
        }

        public async Task<GEResulUploadItemVM> DeleteFileAsync(UploadDestinationFolder pFolder, string pEstablishmentKey, string pUniqueKey, long id, GEResulUploadItemVM pFileToRemove)
        {
            if (pFolder == UploadDestinationFolder.NotIdentified)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "GEFilesPath",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.FileNotFound },
                    Value = id.ToString()
                });
                return null;
            }

            var destination = GenerateUploadFolder(pFolder);

            if (Errors.Any())
                return null;

            IEnumerable<GEFilesPath> files = null;

            switch (pFolder)
            {
                case UploadDestinationFolder.Users:
                    files = await _IGEFilesPathService.QueryAsync(r =>
                        r.UserID == id &&
                        r.EstablishmentKey == pEstablishmentKey &&
                        r.Path == pFileToRemove.Path &&
                        r.Identifier == pFileToRemove.Identifier);
                    break;
                case UploadDestinationFolder.Products:
                    files = await _IGEFilesPathService.QueryAsync(r =>
                        r.ProductID == id &&
                        r.EstablishmentKey == pEstablishmentKey &&
                        r.Path == pFileToRemove.Path &&
                        r.Identifier == pFileToRemove.Identifier);
                    break;
                case UploadDestinationFolder.Enterprises:
                    files = await _IGEFilesPathService.QueryAsync(r =>
                        r.EnterpriseID == id &&
                        r.EstablishmentKey == pEstablishmentKey &&
                        r.Path == pFileToRemove.Path &&
                        r.Identifier == pFileToRemove.Identifier);
                    break;
                case UploadDestinationFolder.Establishments:
                    files = await _IGEFilesPathService.QueryAsync(r =>
                        r.GEEstabID == id &&
                        r.EstablishmentKey == pEstablishmentKey &&
                        r.Path == pFileToRemove.Path &&
                        r.Identifier == pFileToRemove.Identifier);
                    break;
                case UploadDestinationFolder.Order:
                    files = await _IGEFilesPathService.QueryAsync(r =>
                        r.OrderID == id &&
                        r.EstablishmentKey == pEstablishmentKey &&
                        r.Path == pFileToRemove.Path &&
                        r.Identifier == pFileToRemove.Identifier);
                    break;
                case UploadDestinationFolder.OrderItem:
                    files = await _IGEFilesPathService.QueryAsync(r =>
                        r.OrderItemID == id &&
                        r.EstablishmentKey == pEstablishmentKey &&
                        r.Path == pFileToRemove.Path &&
                        r.Identifier == pFileToRemove.Identifier);
                    break;
            }

            if (files == null || !files.Any())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "GEFilesPath",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.FileNotFound },
                    Value = id.ToString()
                });

                return null;
            }

            try
            {
                foreach (var item in files)
                {
                    var currpath = Path.Combine(destination, item.Name);
                    UploadFolder.RemoveFileByPath(currpath);

                    if (!await _IAWSFileAS.DeleteFileAsync(item.Path))
                    {
                        Errors.Add(_IAWSFileAS.Errors.FirstOrDefault());
                    }

                    if (!Errors.Any())
                        await _IGEFilesPathService.RemoveById(item.Id, item.EstablishmentKey, item.UniqueKey);
                }
            }
            catch (Exception ex)
            {
                Logging.Exception(ex);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "GEFilesPath",
                    Messages = new[] { ex.Message },
                    Value = pFileToRemove.Path
                });

                return null;
            }

            if (!Errors.Any())
                await _IGEFilesPathService.SaveChanges();

            pFileToRemove.Success = !Errors.Any();

            return pFileToRemove;
        }

        [Obsolete("Usar o metodo UploadFilesToServerAsync no lugar desse", true)]
        public async Task<GEResultUploadFileVM> UploadFilesToProductAsync(HttpFileCollection pFiles, GEProductsVM pProduct)
        {
            var destination = GenerateUploadFolder(UploadDestinationFolder.Products, Guid.NewGuid().ToString("N"));

            if (Errors.Any())
                return null;

            var result = new GEResultUploadFileVM();

            foreach (string file in pFiles)
            {
                var item = new GEResulUploadItemVM();
                HttpPostedFile postedFile = null;
                string currpath = "";
                string AwsKey = "";
                try
                {
                    postedFile = pFiles[file];
                    currpath = Path.Combine(destination, postedFile.FileName);
                    item.FileName = postedFile.FileName;
                    item.Identifier = Guid.NewGuid().ToString();

                    postedFile.SaveAs(currpath);
                    FileInfo fi = new FileInfo(currpath);
                    item.Extension = fi.Extension;
                    item.FileSize = fi.Length;
                    item.Success = true;
                    item.MimeType = MimeMapping.GetMimeMapping(currpath);
                    var date = DateTime.Now.ToString("ddMMyyyyHHmm");
                    AwsKey = $@"{pProduct.EstablishmentKey}{UploadDestinationFolder.Products}{item.Identifier}{date}";
                }
                catch (System.Exception ex)
                {
                    Logging.Exception(ex);
                    item.Success = false;
                    item.ErrorMessage = ex.Message;
                }

                if (item.Success)
                {
                    _IAWSFileAS.Errors.Clear();
                    if (!await _IAWSFileAS.UploadFileAsync(currpath, AwsKey))
                    {
                        item.Success = false;
                        Errors.Add(_IAWSFileAS.Errors.FirstOrDefault());
                    }
                }

                if (item.Success)
                {
                    _IGEFilesPathService.Add(new GEFilesPath
                    {
                        EstablishmentKey = pProduct.EstablishmentKey,
                        Description = "",
                        Name = postedFile.FileName,
                        Identifier = item.Identifier,
                        FileSize = item.FileSize,
                        ProductID = pProduct.Id,
                        MimeType = item.MimeType,
                        Path = AwsKey,
                        Extension = item.Extension
                    });

                    await _IGEFilesPathService.SaveChanges();
                }

                result.Files.Add(item);
            }

            try
            {
                Directory.Delete(destination, true);
            }
            catch (Exception)
            {

            }

            return result;
        }

        [Obsolete("Usar o metodo DeleteFileAsync no lugar desse", true)]
        public async Task<GEResulUploadItemVM> DeleteFileOfProductAsync(long id, string pEstablishmentKey, string pUniqueKey, GEResulUploadItemVM pFileToRemove)
        {
            var destination = GenerateUploadFolder(UploadDestinationFolder.Products);

            if (Errors.Any())
                return null;

            var product = await _IGEProductsService.GetByIdAsync(id, pEstablishmentKey, pUniqueKey, r => r.GEFilesPath);

            if (product == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "GEFilesPath",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.ProductIdNotFound },
                    Value = id.ToString()
                });

                return null;
            }

            var files = product.GEFilesPath.Where(r => r.Identifier == pFileToRemove.Identifier &&
                r.Path == pFileToRemove.Path &&
                r.EstablishmentKey == pEstablishmentKey);

            if (!files.Any())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "GEFilesPath",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.FileNotFound },
                    Value = pFileToRemove.Path
                });

                return null;
            }

            try
            {
                foreach (var item in files)
                {
                    var currpath = Path.Combine(destination, item.Name);
                    UploadFolder.RemoveFileByPath(currpath);

                    if (!await _IAWSFileAS.DeleteFileAsync(item.Path))
                    {
                        Errors.Add(_IAWSFileAS.Errors.FirstOrDefault());
                    }

                    if (!Errors.Any())
                        await _IGEFilesPathService.RemoveById(item.Id, item.EstablishmentKey, item.UniqueKey);
                }
            }
            catch (Exception ex)
            {
                Logging.Exception(ex);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "GEFilesPath",
                    Messages = new[] { ex.Message },
                    Value = pFileToRemove.Path
                });

                return null;
            }

            if (!Errors.Any())
                await _IGEFilesPathService.SaveChanges();

            if (!Errors.Any())
                pFileToRemove.Success = true;

            return pFileToRemove;
        }

        public async Task<string> DownloadFileAsync(string pEstablishmentKey, string pFilePath)
        {
            var file = (await _IGEFilesPathService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                r.Path == pFilePath)).FirstOrDefault();

            if (file == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Path",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.FileNotFound },
                    Value = pFilePath
                });

                return null;
            }

            var url = _IAWSFileAS.DownloadFileAsync(pFilePath);

            if (url.IsEmpty())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Path",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.FileNotFound },
                    Value = pFilePath
                });

                return null;
            }

            return url;// $"data:{file.MimeType},{url}";
        }
    }
}
