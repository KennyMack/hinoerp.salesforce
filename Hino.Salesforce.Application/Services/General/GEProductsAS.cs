using AutoMapper;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Domain.Fiscal.Interfaces.Services.Taxes;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General
{
    public class GEProductsAS : BaseAppService<GEProducts>, IGEProductsAS
    {
        private readonly IGEProductKeyService _IGEProductKeyService;
        private readonly IGEProductsService _IGEProductsService;
        private readonly IGEProductsUnitService _IGEProductsUnitService;
        private readonly IGEProductsTypeService _IGEProductsTypeService;
        private readonly IGEProductsFamilyService _IGEProductsFamilyService;
        private readonly IVESalePriceService _IVESalePriceService;
        private readonly IFSNCMService _IFSNCMService;
        private readonly IGEEstablishmentsService _IGEEstablishmentsService;

        public GEProductsAS(IGEProductsService pIGEProductsService,
                            IGEProductsUnitService pIGEProductsUnitService,
                            IGEProductsTypeService pIGEProductsTypeService,
                            IGEProductsFamilyService pIGEProductsFamilyService,
                            IFSNCMService pIFSNCMService,
                            IGEProductKeyService pIGEProductKeyService,
                            IVESalePriceService pIVESalePriceService,
                            IGEEstablishmentsService pIGEEstablishmentsService
            ) :
             base(pIGEProductsService)
        {
            _IVESalePriceService = pIVESalePriceService;
            _IGEProductKeyService = pIGEProductKeyService;
            _IFSNCMService = pIFSNCMService;
            _IGEProductsService = pIGEProductsService;
            _IGEProductsUnitService = pIGEProductsUnitService;
            _IGEProductsTypeService = pIGEProductsTypeService;
            _IGEProductsFamilyService = pIGEProductsFamilyService;
            _IGEEstablishmentsService = pIGEEstablishmentsService;
        }

        public async Task<IEnumerable<GEProductsVM>> SyncDataProd(string pEstablishmentKey, GEProductsVM[] pRegisters)
        {
            List<ModelException> SyncErrors = new List<ModelException>();

            for (int i = 0, length = pRegisters.Length; i < length; i++)
            {
                GEProductsVM result;
                GEProductsUnit SaleUnit;
                GEProductsUnit Unit;
                GEProductsFamily Family;
                GEProductsType Type;
                FSNCM NCM;
                var EstablishmentKey = pRegisters[i].EstablishmentKey;
                var ProductKey = pRegisters[i].ProductKey;

                if (!pRegisters[i].Unit.IsEmpty())
                {
                    var unit = pRegisters[i].Unit.Trim();
                    Unit = (await _IGEProductsUnitService.QueryAsync(r =>
                        r.EstablishmentKey == EstablishmentKey &&
                        r.Unit == unit
                    )).FirstOrDefault();
                    pRegisters[i].UnitId = Unit?.Id;
                }

                if (!pRegisters[i].SaleUnit.IsEmpty())
                {
                    var unit = pRegisters[i].SaleUnit.Trim();
                    SaleUnit = (await _IGEProductsUnitService.QueryAsync(r =>
                        r.EstablishmentKey == EstablishmentKey &&
                        r.Unit == unit
                    )).FirstOrDefault();
                    pRegisters[i].UnitId = SaleUnit?.Id;
                }

                if (!pRegisters[i].TypeProd.IsEmpty())
                {
                    var typeProd = pRegisters[i].TypeProd.Trim();
                    Type = (await _IGEProductsTypeService.QueryAsync(r =>
                        r.EstablishmentKey == EstablishmentKey &&
                        r.TypeProd == typeProd
                    )).FirstOrDefault();
                    if (Type != null)
                        pRegisters[i].TypeId = Type.Id;
                }

                if (!pRegisters[i].Family.IsEmpty())
                {
                    var fam = pRegisters[i].Family.Trim();
                    Family = (await _IGEProductsFamilyService.QueryAsync(r =>
                        r.EstablishmentKey == EstablishmentKey &&
                        r.Family == fam
                    )).FirstOrDefault();
                    if (Family != null)
                        pRegisters[i].FamilyId = Family.Id;
                }

                if (!pRegisters[i].NCM.IsEmpty())
                {
                    var ncm = pRegisters[i].NCM.Trim();
                    NCM = (await _IFSNCMService.QueryAsync(r =>
                        r.NCM == ncm
                    )).FirstOrDefault();
                    if (NCM != null)
                        pRegisters[i].NCMId = NCM.Id;
                }
                pRegisters[i].FSNCM = null;
                pRegisters[i].GEProductAplic = null;
                pRegisters[i].GEProductsFamily = null;
                pRegisters[i].GEProductsSaleUnit = null;
                pRegisters[i].GEProductsType = null;
                pRegisters[i].GEProductsUnit = null;

                if (pRegisters[i].UniqueKey == pRegisters[i].EstablishmentKey)
                    pRegisters[i].UniqueKey = Guid.NewGuid().ToString();

                var ProductOld = (await _IGEProductsService.QueryAsync(r => r.EstablishmentKey == EstablishmentKey &&
                    r.ProductKey == ProductKey)).FirstOrDefault();

                if (ProductOld != null)
                {
                    pRegisters[i].Id = ProductOld.Id;
                    pRegisters[i].UniqueKey = ProductOld.UniqueKey;
                    pRegisters[i].Created = ProductOld.Created;
                }

                if (pRegisters[i].Id <= 0)
                {
                    pRegisters[i].Id = await _IGEProductsService.NextSequenceAsync();
                    result = await CreateProduct(pRegisters[i]);
                }
                else
                    result = await UpdateProduct(pRegisters[i]);

                pRegisters[i].UniqueKey = result.UniqueKey;
                pRegisters[i].Id = result.Id;

                if (Errors.Count > 0)
                    SyncErrors.Add(Errors[0]);
            }

            if (Errors.Count() <= 0)
                await SaveChanges();

            return pRegisters;
        }

        public override Task<GEProducts> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(new ModelException
            {
                ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                Field = "Id",
                Value = id.ToString(),
                Messages = new string[]
                {
                    Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("RemoveNotAllowed")
                }
            });

            return Task.FromResult(new GEProducts());
        }

        public async Task<IEnumerable<GEProducts>> SyncStockBalance(string pEstablishmentKey, GEProducts[] pRegisters)
        {
            List<ModelException> SyncErrors = new List<ModelException>();

            for (int i = 0, length = pRegisters.Length; i < length; i++)
            {
                var Product = await _IGEProductsService.GetByIdAsync(
                    pRegisters[i].Id, pRegisters[i].EstablishmentKey, pRegisters[i].UniqueKey);

                if (Product != null)
                {
                    Product.StockBalance = pRegisters[i].StockBalance;
                    Product.IsActive = true;
                    try
                    {
                        _IGEProductsService.Update(Product);
                        if (Errors.Count > 0)
                            pRegisters[i].Id = 0;
                    }
                    catch (System.Exception)
                    {
                        SyncErrors.Add(Errors[0]);
                    }
                }
            }

            try
            {
                await SaveChanges();
            }
            catch (System.Exception)
            {
                SyncErrors.Add(Errors[0]);
            }

            return pRegisters;
        }

        public async Task<bool> ExistsProductKey(string pEstablishmentKey, string pProductKey)
        {
            var Result = await _IGEProductsService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey && r.ProductKey == pProductKey);
            return Result.Any();
        }

        public async Task<GEProductsVM> CreateProduct(GEProductsVM pProduct)
        {
            if (await ExistsProductKey(pProduct.EstablishmentKey, pProduct.ProductKey))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Id",
                    Value = pProduct.ProductKey,
                    Messages = new string[]
                    {
                        Infra.Cross.Resources.ValidationMessagesResource.ProductExists
                    }
                });
                return null;
            }

            if (pProduct.EstablishmentKey.In("f48c8bd0-a048-4ae4-a988-75b791fdd80b", "b421e29f-d411-4cf6-a74d-6db9de4fe6c8"))
                pProduct.AditionalInfo = $"BS-{pProduct.ProductKey}";

            var ResultProd = Add(Mapper.Map<GEProductsVM, GEProducts>(pProduct));

            await SaveChanges();

            if (ResultProd != null && !Errors.Any() && pProduct.ProductKeyGenerated)
                await _IGEProductKeyService.NextSequenceByReferenceAsync(pProduct.EstablishmentKey, pProduct.Reference);

            if (!Errors.Any() &&
                pProduct.EstablishmentKey.In("f48c8bd0-a048-4ae4-a988-75b791fdd80b", "b421e29f-d411-4cf6-a74d-6db9de4fe6c8"))
            {
                try
                {
                    var Family = (await _IGEProductsFamilyService.QueryAsync(r => r.EstablishmentKey == pProduct.EstablishmentKey &&
                        r.Id == pProduct.FamilyId)).FirstOrDefault();

                    if (Family != null && Family.Family == "1023")
                        await _IVESalePriceService.CreateProductToAllRegionAsync(pProduct.EstablishmentKey, ResultProd);
                }
                catch (Exception)
                {
                }
            }

            return Mapper.Map<GEProducts, GEProductsVM>(ResultProd);
        }

        public async Task<GEProductsVM> UpdateProduct(GEProductsVM pProduct)
        {
            var ProductOld = GetByIdToUpdate(pProduct.Id, pProduct.EstablishmentKey, pProduct.UniqueKey);

            if (ProductOld.ProductKey != pProduct.ProductKey && await ExistsProductKey(pProduct.EstablishmentKey, pProduct.ProductKey))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Id",
                    Value = pProduct.ProductKey,
                    Messages = new string[]
                    {
                        Infra.Cross.Resources.ValidationMessagesResource.ProductExists
                    }
                });
                return null;
            }
            ProductOld.Image = pProduct.Image;
            ProductOld.Name = pProduct.Name;
            ProductOld.Description = pProduct.Description;
            ProductOld.TypeId = pProduct.TypeId;
            ProductOld.FamilyId = pProduct.FamilyId;
            ProductOld.PercIPI = Convert.ToDecimal(pProduct.PercIPI);
            ProductOld.PercMaxDiscount = Convert.ToDecimal(pProduct.PercMaxDiscount);
            ProductOld.Value = Convert.ToDecimal(pProduct.Value);
            ProductOld.Status = pProduct.Status;
            ProductOld.NCM = pProduct.NCM;
            ProductOld.NCMId = pProduct.NCMId;
            ProductOld.UnitId = pProduct.UnitId;
            ProductOld.Weight = pProduct.Weight;
            ProductOld.PackageQTD = pProduct.PackageQTD;
            ProductOld.SaleUnitId = pProduct.SaleUnitId;
            ProductOld.SaleFactor = pProduct.SaleFactor;
            ProductOld.AplicationId = pProduct.AplicationId;
            ProductOld.CodOrigMerc = pProduct.CodOrigMerc;

            return Mapper.Map<GEProducts, GEProductsVM>(Update(ProductOld));
        }

        public async Task<GEGenerateProductKeyVM> GenerateProductKeyAsync(GEGenerateProductKeyVM pProduct)
        {
            pProduct.Reference = $"{pProduct.Type}.{pProduct.Family}";

            var Sequence = await _IGEProductKeyService.GetSequenceByReferenceAsync(pProduct.EstablishmentKey, pProduct.Reference);

            pProduct.ProductKey = $"{pProduct.Reference}.{Sequence.ToString().PadLeft(5, '0')}";

            return pProduct;

        }
    }
}
