using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General
{
    public class GEProductsFamilyAS : BaseAppService<GEProductsFamily>, IGEProductsFamilyAS
    {
        private readonly IGEProductsFamilyService _IGEProductsFamilyService;

        public GEProductsFamilyAS(IGEProductsFamilyService pIGEProductsFamilyService) :
             base(pIGEProductsFamilyService)
        {
            _IGEProductsFamilyService = pIGEProductsFamilyService;
        }

        public override Task<GEProductsFamily> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(new Infra.Cross.Utils.Exceptions.ModelException
            {
                ErrorCode = (int)Infra.Cross.Utils.Exceptions.EExceptionErrorCodes.InvalidRequest,
                Field = "Id",
                Value = id.ToString(),
                Messages = new string[]
                {
                    Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("RemoveNotAllowed")
                }
            });

            return null;
        }
    }
}
