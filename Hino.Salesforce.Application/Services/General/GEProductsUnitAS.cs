using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General
{
    public class GEProductsUnitAS : BaseAppService<GEProductsUnit>, IGEProductsUnitAS
    {
        private readonly IGEProductsUnitService _IGEProductsUnitService;

        public GEProductsUnitAS(IGEProductsUnitService pIGEProductsUnitService) :
             base(pIGEProductsUnitService)
        {
            _IGEProductsUnitService = pIGEProductsUnitService;
        }

        public override Task<GEProductsUnit> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(new Infra.Cross.Utils.Exceptions.ModelException
            {
                ErrorCode = (int)Infra.Cross.Utils.Exceptions.EExceptionErrorCodes.InvalidRequest,
                Field = "Id",
                Value = id.ToString(),
                Messages = new string[]
                {
                    Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("RemoveNotAllowed")
                }
            });

            return null;
        }
    }
}
