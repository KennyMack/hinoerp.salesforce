using AutoMapper;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.ViewModels.Auth;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Domain.Auth.Interfaces.Services;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Cache.Auth;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Identity;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General
{
    public class GEUsersAS : BaseAppService<GEUsers>, IGEUsersAS
    {
        private readonly IGEUsersService _IGEUsersService;
        private readonly IGEEstablishmentsAS _IGEEstablishmentsAS;
        private readonly IGEEstabDevicesAS _IGEEstabDevicesAS;
        private readonly IAURefreshTokenService _IAURefreshTokenService;
        private readonly SessionTokenCache _SessionTokenCache;

        public GEUsersAS(IGEUsersService pIGEUsersService,
            IGEEstablishmentsAS pIGEEstablishmentsAS,
            IGEEstabDevicesAS pIGEEstabDevicesAS,
            IAURefreshTokenService pIAURefreshTokenService,
            SessionTokenCache pSessionTokenCache) :
             base(pIGEUsersService)
        {
            _IGEEstabDevicesAS = pIGEEstabDevicesAS;
            _IGEUsersService = pIGEUsersService;
            _IGEEstablishmentsAS = pIGEEstablishmentsAS;
            _SessionTokenCache = pSessionTokenCache;
            _IAURefreshTokenService = pIAURefreshTokenService;
        }

        public async Task<GEUsers> GetByEmailAsync(string email, string pEstablishmentKey, params Expression<Func<GEUsers, object>>[] includeProperties) =>
            await _IGEUsersService.GetByEmailAsync(email, pEstablishmentKey, includeProperties);

        public async Task<GEUsers> GetByUsernameAsync(string username, string pEstablishmentKey, params Expression<Func<GEUsers, object>>[] includeProperties) =>
            await _IGEUsersService.GetByUsernameAsync(username, pEstablishmentKey, includeProperties);

        public async Task<GEUsers> CreateAdminUserAsync(string pHinoId, GEUsersVM pGEUsersVM)
        {
            return await _IGEUsersService.CreateAdminUserAsync(pHinoId, Mapper.Map<GEUsersVM, GEUsers>(pGEUsersVM));
        }

        public async Task<GEUsersVM> CreateUserAsync(GEUsersCreateVM pGEUsersVM)
        {
            var establishment = await _IGEEstablishmentsAS.GetByEstablishmentKeyAsync(pGEUsersVM.EstablishmentKey);

            if (establishment == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.ErrorMessagesResource.InvalidEstablishmentKeyOrNull }
                });

                return Mapper.Map<GEUsersCreateVM, GEUsersVM>(pGEUsersVM);
            }

            if (!establishment.IsActive)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.ErrorMessagesResource.EstablishmentInactive }
                });
                return null;
            }

            if (pGEUsersVM.OriginCreate.ToUpper() != "CLIENT")
            {
                var result = (await _IGEEstabDevicesAS.QueryAsync(r =>
                    r.IsActive &&
                    r.UserKey == pGEUsersVM.UserKey)).Count() > 0;

                if (!result)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                        Field = "UserKey",
                        Value = "",
                        Messages = new string[] { Infra.Cross.Resources.ValidationMessagesResource.InvalidUserKey }
                    });

                    return Mapper.Map<GEUsersCreateVM, GEUsersVM>(pGEUsersVM);
                }
            }
            else if (pGEUsersVM.OriginCreate.ToUpper() == "CLIENT")
            {
                if (establishment.Email != pGEUsersVM.Email)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                        Field = "Body",
                        Value = "",
                        Messages = new string[] { Infra.Cross.Resources.ErrorMessagesResource.EstablishmentEmailInvalid }
                    });
                    return Mapper.Map<GEUsersCreateVM, GEUsersVM>(pGEUsersVM);
                }
            }

            return Mapper.Map<GEUsers, GEUsersVM>(await _IGEUsersService.CreateUserAsync(
                Mapper.Map<GEUsersCreateVM, GEUsers>(pGEUsersVM), pGEUsersVM.OriginCreate.ToUpper()));
        }

        public override Task<GEUsers> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            Errors.Add(new ModelException
            {
                ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                Field = "Id",
                Value = id.ToString(),
                Messages = new string[]
                {
                    Infra.Cross.Resources.ErrorMessagesResource.RemoveNotAllowed
                }
            });
            return null;
        }

        public async Task<GEUsers> UpdateUser(GEUsers model)
        {
            return await _IGEUsersService.UpdateUser(model);
        }

        private async Task<GEUsersVM> ValidateLoginAsync(GEUsersVM pGEUser, string pOriginLogin, string pEstablishmentKey, bool pHinoLogin)
        {
            if (!pGEUser.IsActive)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Password",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.ValidationMessagesResource.AuthUserAuthFail }
                });
                return null;
            }

            if (pHinoLogin && pGEUser.UserType != Infra.Cross.Utils.EUserType.Hino)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Body",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.ValidationMessagesResource.LoginNotAllowed }
                });
                return null;
            }

            GEEstablishments Establishment = null;

            if (pGEUser.UserType != Infra.Cross.Utils.EUserType.Hino)
            {
                if (pGEUser.EstablishmentKey != pEstablishmentKey)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                        Field = "Body",
                        Value = "",
                        Messages = new string[] { Infra.Cross.Resources.ValidationMessagesResource.AuthUserAuthFail }
                    });
                    return null;
                }

                Establishment = await _IGEEstablishmentsAS.GetByEstablishmentKeyAsync(pGEUser.EstablishmentKey);

                if (!Establishment.IsActive)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                        Field = "Body",
                        Value = "",
                        Messages = new string[] { Infra.Cross.Resources.ErrorMessagesResource.EstablishmentInactive }
                    });
                    return null;
                }
            }

            if (pGEUser.UserType != Infra.Cross.Utils.EUserType.Hino)
            {
                switch (pOriginLogin.ToUpper())
                {
                    case "ADMIN":
                        if (pGEUser.UserType != Infra.Cross.Utils.EUserType.Hino)
                        {
                            Errors.Add(new ModelException
                            {
                                ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                                Field = "Body",
                                Value = "",
                                Messages = new string[] { Infra.Cross.Resources.ErrorMessagesResource.LoginNotAllowed }
                            });
                            return null;
                        }
                        break;
                    case "MOBILE":
                        if ((int)pGEUser.UserType > 2)
                        {
                            Errors.Add(new ModelException
                            {
                                ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                                Field = "Body",
                                Value = "",
                                Messages = new string[] { Infra.Cross.Resources.ErrorMessagesResource.LoginNotAllowed }
                            });
                            return null;

                        }
                        break;
                    case "CLIENT":
                        if ((int)pGEUser.UserType <= -1)
                        {
                            Errors.Add(new ModelException
                            {
                                ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                                Field = "Body",
                                Value = "",
                                Messages = new string[] { Infra.Cross.Resources.ErrorMessagesResource.LoginNotAllowed }
                            });
                            return null;
                        }
                        break;
                    default:
                        Errors.Add(new ModelException
                        {
                            ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                            Field = "Body",
                            Value = "",
                            Messages = new string[] { Infra.Cross.Resources.ErrorMessagesResource.LoginNotAllowed }
                        });
                        return null;
                }
            }

            if (Establishment != null)
                pGEUser.GEEstablishments = Mapper.Map<GEEstablishments, GEEstablishmentsVM>(Establishment);

            return pGEUser;
        }

        public async Task<GEUsersVM> ValidateRefreshAsync(LoginVM model, bool pHinoLogin)
        {
            Errors.Clear();
            GEUsersVM dbUser = null;

            if (string.IsNullOrEmpty(model.RefreshToken))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Username",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.ErrorMessagesResource.InvalidRefreshToken }
                });
                return dbUser;
            }

            var refreshToken = await _IAURefreshTokenService.GetByRefreshTokenId(model.RefreshToken, model.EstablishmentKey);

            if (refreshToken == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "RefreshToken",
                    Value = model.RefreshToken,
                    Messages = new string[] { Infra.Cross.Resources.ErrorMessagesResource.InvalidRefreshToken }
                });
                return dbUser;
            }

            if (refreshToken.GEUsers == null || string.IsNullOrEmpty(refreshToken.GEUsers.Password))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "UserName",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.ValidationMessagesResource.AuthUserNotFound }
                });
                return dbUser;
            }

            dbUser = Mapper.Map<GEUsers, GEUsersVM>(refreshToken.GEUsers);

            return await ValidateLoginAsync(dbUser, model.OriginLogin, model.EstablishmentKey, pHinoLogin);
        }

        public async Task<GEUsersVM> ValidateAuthenticationAsync(LoginVM model, bool pHinoLogin)
        {
            /* var Token = new JWTTokenValue()
             {
                 IsAuthenticated = false
             };*/
            Errors.Clear();
            GEUsersVM dbUser = null;

            if (string.IsNullOrEmpty(model.UserName) &&
                string.IsNullOrEmpty(model.Email))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Username",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.ErrorMessagesResource.UserNameOrEmailNotInformed }
                });
                return dbUser;
            }

            try
            {
                GEUsers user = null;
                if (!string.IsNullOrEmpty(model.UserName))
                    user = await _IGEUsersService.GetByUsernameAsync(model.UserName, model.EstablishmentKey);
                else if (!string.IsNullOrEmpty(model.Email))
                    user = await _IGEUsersService.GetByEmailAsync(model.Email, model.EstablishmentKey);

                dbUser = Mapper.Map<GEUsers, GEUsersVM>(user);
            }
            catch (Exception ex)
            {
                Logging.Exception(ex);
                dbUser = null;
            }

            if (dbUser == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "UserName",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.ValidationMessagesResource.AuthUserNotFound }
                });
                return dbUser;
            }

            if (!Metis.ValidatePass(model.Password, dbUser.Password))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Password",
                    Value = "",
                    Messages = new string[] { Infra.Cross.Resources.ValidationMessagesResource.AuthUserAuthFail }
                });
                return null;
            }

            return await ValidateLoginAsync(dbUser, model.OriginLogin, model.EstablishmentKey, pHinoLogin);
        }

        public GEUsersVM Me(string pSessionID)
        {
            GEUsersVM geUsers = null;
            try
            {
                var UserCache = JsonConvert.DeserializeObject<GEUsersVM>(_SessionTokenCache.Get<string>(pSessionID));
                /*long id = Convert.ToInt64(UserCache.Id);
                string EstablishmentKey = UserCache.EstablishmentKey.ToString();
                string UniqueKey = UserCache.UniqueKey.ToString();
                string UserKey = UserCache.UserKey.ToString();*/

                //geUsers = _IGEUsersService.GetById(id, EstablishmentKey, UniqueKey);
                //geUsers.Password = null;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                geUsers = null;
            }


            return geUsers;
        }

        public async Task<GEUsersVM> GetUserById(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<GEUsers, object>>[] includeProperties)
        {
            GEUsersVM dbUser = null;
            var user = _IGEUsersService.GetById(id, pEstablishmentKey, pUniqueKey, includeProperties);
            if (user != null)
            {
                dbUser = Mapper.Map<GEUsers, GEUsersVM>(user);
                var Establishment = await _IGEEstablishmentsAS.GetByEstablishmentKeyAsync(user.EstablishmentKey);

                dbUser.GEEstablishments = Mapper.Map<GEEstablishments, GEEstablishmentsVM>(Establishment);
            }

            return dbUser;
        }

        public async Task<GEUsersPasswordVM> ChangePassword(GEUsersPasswordVM model)
        {
            var User = await _IGEUsersService.ChangePassword(Mapper.Map<GEUsers>(model));

            return Mapper.Map<GEUsersPasswordVM>(User);
        }
    }
}
