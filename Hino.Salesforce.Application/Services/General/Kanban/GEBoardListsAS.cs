using Hino.Salesforce.Application.Interfaces.General.Kanban;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;

namespace Hino.Salesforce.Application.Services.General.Kanban
{
    public class GEBoardListsAS : BaseAppService<GEBoardLists>, IGEBoardListsAS
    {
        private readonly IGEBoardListsService _IGEBoardListsService;

        public GEBoardListsAS(IGEBoardListsService pIGEBoardListsService) :
             base(pIGEBoardListsService)
        {
            _IGEBoardListsService = pIGEBoardListsService;
        }
    }
}
