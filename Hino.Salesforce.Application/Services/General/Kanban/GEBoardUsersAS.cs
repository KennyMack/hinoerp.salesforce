using Hino.Salesforce.Application.Interfaces.General.Kanban;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;

namespace Hino.Salesforce.Application.Services.General.Kanban
{
    public class GEBoardUsersAS : BaseAppService<GEBoardUsers>, IGEBoardUsersAS
    {
        private readonly IGEBoardUsersService _IGEBoardUsersService;

        public GEBoardUsersAS(IGEBoardUsersService pIGEBoardUsersService) :
             base(pIGEBoardUsersService)
        {
            _IGEBoardUsersService = pIGEBoardUsersService;
        }
    }
}
