using Hino.Salesforce.Application.Interfaces.General.Kanban;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;

namespace Hino.Salesforce.Application.Services.General.Kanban
{
    public class GEBoardsAS : BaseAppService<GEBoards>, IGEBoardsAS
    {
        private readonly IGEBoardsService _IGEBoardsService;

        public GEBoardsAS(IGEBoardsService pIGEBoardsService) :
             base(pIGEBoardsService)
        {
            _IGEBoardsService = pIGEBoardsService;
        }
    }
}
