using Hino.Salesforce.Application.Interfaces.General.Kanban;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;

namespace Hino.Salesforce.Application.Services.General.Kanban
{
    public class GECardFieldsAS : BaseAppService<GECardFields>, IGECardFieldsAS
    {
        private readonly IGECardFieldsService _IGECardFieldsService;

        public GECardFieldsAS(IGECardFieldsService pIGECardFieldsService) :
             base(pIGECardFieldsService)
        {
            _IGECardFieldsService = pIGECardFieldsService;
        }
    }
}
