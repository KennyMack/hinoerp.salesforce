using Hino.Salesforce.Application.Interfaces.General.Kanban;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;

namespace Hino.Salesforce.Application.Services.General.Kanban
{
    public class GEFieldUsersAS : BaseAppService<GEFieldUsers>, IGEFieldUsersAS
    {
        private readonly IGEFieldUsersService _IGEFieldUsersService;

        public GEFieldUsersAS(IGEFieldUsersService pIGEFieldUsersService) :
             base(pIGEFieldUsersService)
        {
            _IGEFieldUsersService = pIGEFieldUsersService;
        }
    }
}
