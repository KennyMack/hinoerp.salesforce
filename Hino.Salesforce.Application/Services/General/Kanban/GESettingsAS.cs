using AutoMapper;
using Hino.Salesforce.Application.Interfaces.General.Kanban;
using Hino.Salesforce.Application.ViewModels.General.Kanban;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General.Kanban
{
    public class GESettingsAS : BaseAppService<GESettings>, IGESettingsAS
    {
        private readonly IGESettingsService _IGESettingsService;
        private readonly IGEBoardsService _IGEBoardsService;
        private readonly IGEBoardUsersService _IGEBoardUsersService;
        private readonly IGEBoardListsService _IGEBoardListsService;
        private readonly IGESettingsTagsService _IGESettingsTagsService;
        private readonly IGECardFieldsService _IGECardFieldsService;
        private readonly IGEFieldUsersService _IGEFieldUsersService;
        private readonly IGEEstablishmentsService _IGEEstablishmentsService;

        public GESettingsAS(IGESettingsService pIGESettingsService,
            IGEBoardsService pIGEBoardsService,
            IGEBoardUsersService pIGEBoardUsersService,
            IGEBoardListsService pIGEBoardListsService,
            IGESettingsTagsService pIGESettingsTagsService,
            IGECardFieldsService pIGECardFieldsService,
            IGEFieldUsersService pIGEFieldUsersService,
            IGEEstablishmentsService pIGEEstablishmentService) :
             base(pIGESettingsService)
        {
            _IGESettingsService = pIGESettingsService;
            _IGEBoardsService = pIGEBoardsService;
            _IGEBoardUsersService = pIGEBoardUsersService;
            _IGEBoardListsService = pIGEBoardListsService;
            _IGESettingsTagsService = pIGESettingsTagsService;
            _IGECardFieldsService = pIGECardFieldsService;
            _IGEFieldUsersService = pIGEFieldUsersService;
            _IGEEstablishmentsService = pIGEEstablishmentService;
        }

        bool ValidateSetting(GESettingsVM pSettings)
        {
            /*
            if (pSettings.GEBoards.Any() && !pSettings.GEBoards.Any())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Id",
                    Value = "0",
                    Messages = new string[]
                    {
                        Infra.Cross.Resources.ValidationMessagesResource.CardSettingNotInformed
                    }
                });
                return false;
            }

            if (pSettings.GESettingsCard.Any() &&
                pSettings.GESettingsCard.Where(r => !r.GECardFields.Any()).Any())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "Id",
                    Value = "0",
                    Messages = new string[]
                    {
                        Infra.Cross.Resources.ValidationMessagesResource.CardSettingNotInformed
                    }
                });
                return false;
            }
            */

            return true;
        }


        #region Set Default Values
        private async Task SetDefaultValues(long Id, GESettings pSettings)
        {

            foreach (var item in pSettings.GESettingsTags)
            {
                item.EstablishmentKey = pSettings.EstablishmentKey;
                item.UniqueKey = item.Id > 0 ? item.UniqueKey : Guid.NewGuid().ToString();
                item.UniqueKey = item.UniqueKey == item.EstablishmentKey ? Guid.NewGuid().ToString() : item.UniqueKey;
                item.Id = item.Id > 0 ? item.Id : await _IGESettingsTagsService.NextSequenceAsync();
                item.SettingsID = Id;
                item.IsActive = true;
                item.Modified = DateTime.Now;
            }

            foreach (var item in pSettings.GEBoards)
            {
                item.EstablishmentKey = pSettings.EstablishmentKey;
                item.UniqueKey = item.Id > 0 ? item.UniqueKey : Guid.NewGuid().ToString();
                item.UniqueKey = item.UniqueKey == item.EstablishmentKey ? Guid.NewGuid().ToString() : item.UniqueKey;
                item.Id = item.Id > 0 ? item.Id : await _IGEBoardsService.NextSequenceAsync();

                item.SettingsID = Id;
                item.IsActive = true;
                item.Modified = DateTime.Now;
                item.GESettings = null;

                foreach (var user in item.GEBoardUsers)
                {
                    user.EstablishmentKey = pSettings.EstablishmentKey;
                    user.UniqueKey = user.Id > 0 ? user.UniqueKey : Guid.NewGuid().ToString();
                    user.UniqueKey = user.UniqueKey == user.EstablishmentKey ? Guid.NewGuid().ToString() : user.UniqueKey;
                    user.Id = user.Id > 0 ? user.Id : await _IGEBoardUsersService.NextSequenceAsync();

                    user.BoardID = item.Id;
                    user.IsActive = true;
                    user.Modified = DateTime.Now;
                    user.GEBoards = null;
                    user.GEUsers = null;
                }

                foreach (var list in item.GEBoardLists)
                {
                    list.EstablishmentKey = pSettings.EstablishmentKey;
                    list.UniqueKey = list.Id > 0 ? list.UniqueKey : Guid.NewGuid().ToString();
                    list.UniqueKey = list.UniqueKey == list.EstablishmentKey ? Guid.NewGuid().ToString() : list.UniqueKey;
                    list.Id = list.Id > 0 ? list.Id : await _IGEBoardListsService.NextSequenceAsync();

                    list.BoardID = item.Id;
                    list.Position = list.Position;
                    list.IsActive = true;
                    list.Modified = DateTime.Now;
                    list.GEBoards = null;
                }


                foreach (var field in item.GECardFields)
                {
                    field.EstablishmentKey = pSettings.EstablishmentKey;
                    field.UniqueKey = field.Id > 0 ? field.UniqueKey : Guid.NewGuid().ToString();
                    field.UniqueKey = field.UniqueKey == field.EstablishmentKey ? Guid.NewGuid().ToString() : field.UniqueKey;
                    field.Id = field.Id > 0 ? field.Id : await _IGECardFieldsService.NextSequenceAsync();

                    field.BoardID = item.Id;
                    field.IsActive = true;
                    field.Modified = DateTime.Now;
                    field.GEBoards = null;

                    foreach (var user in field.GEFieldUsers)
                    {
                        user.EstablishmentKey = pSettings.EstablishmentKey;
                        user.UniqueKey = user.Id > 0 ? user.UniqueKey : Guid.NewGuid().ToString();
                        user.UniqueKey = user.UniqueKey == user.EstablishmentKey ? Guid.NewGuid().ToString() : user.UniqueKey;
                        user.Id = user.Id > 0 ? user.Id : await _IGEFieldUsersService.NextSequenceAsync();

                        user.FieldID = field.Id;
                        user.IsActive = true;
                        user.Modified = DateTime.Now;
                        user.GECardFields = null;
                        user.GEUsers = null;
                    }
                }
            }
        }
        #endregion

        public async Task<GESettingsVM> CreateSettingAsync(GESettingsVM pSettings)
        {
            var SettingDB = Mapper.Map<GESettingsVM, GESettings>(pSettings);
            SettingDB.Id = await _IGESettingsService.NextSequenceAsync();

            await SetDefaultValues(SettingDB.Id, SettingDB);

            _IGESettingsService.Add(SettingDB);
            await _IGESettingsService.SaveChanges();

            return Mapper.Map<GESettings, GESettingsVM>(SettingDB);
        }

        public async Task<GESettingsVM> UpdateSettingAsync(
            GESettings pSettingsDB,
            GESettingsVM pSettings)
        {
            await _IGESettingsService.RemoveById(pSettingsDB.Id, pSettingsDB.EstablishmentKey, pSettingsDB.UniqueKey);
            await _IGESettingsService.SaveChanges();

            var SettingDB = Mapper.Map<GESettingsVM, GESettings>(pSettings);
            SettingDB.Id = pSettings.Id;
            SettingDB.EstabID = pSettings.EstabID;
            SettingDB.UniqueKey = pSettings.UniqueKey;
            SettingDB.Created = pSettings.Created;
            SettingDB.Modified = DateTime.Now;

            await SetDefaultValues(SettingDB.Id, SettingDB);

            _IGESettingsService.Add(SettingDB);
            await _IGESettingsService.SaveChanges();

            return Mapper.Map<GESettings, GESettingsVM>(SettingDB);
        }

        public async Task<GESettingsVM> CreateOrChangeSettingAsync(GESettingsVM pSettings)
        {
            if (!ValidateSetting(pSettings))
                return null;

            var Estab = await _IGEEstablishmentsService.QueryAsync(r =>
                r.EstablishmentKey == pSettings.EstablishmentKey &&
                r.Id == pSettings.EstabID);

            if (!Estab.Any())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "EstabId",
                    Value = pSettings.EstabID.ToString(),
                    Messages = new string[]
                    {
                        Infra.Cross.Resources.ErrorMessagesResource.InvalidEstablishmentKeyOrNull
                    }
                });
                return null;
            }

            var SettingDB = _IGESettingsService.GetById(pSettings.Id, pSettings.EstablishmentKey, pSettings.UniqueKey);

            if (SettingDB == null)
                pSettings = await CreateSettingAsync(pSettings);
            else
                pSettings = await UpdateSettingAsync(SettingDB, pSettings);

            return pSettings;
        }

        public async Task<GESettingsVM> GetByEstablishmentIdAsync(string pEstablishmentKey, long pId)
        {
            var Result = (await _IGESettingsService.QueryAsync(
                    r => r.EstabID == pId &&
                    r.EstablishmentKey == pEstablishmentKey)).FirstOrDefault();

            foreach (var item in Result.GEBoards)
                item.GEBoardLists = item.GEBoardLists.OrderBy(r => r.Position).ToHashSet();

            return Mapper.Map<GESettings, GESettingsVM>(Result ?? new GESettings());
        }
    }
}
