using Hino.Salesforce.Application.Interfaces.General.Kanban;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;

namespace Hino.Salesforce.Application.Services.General.Kanban
{
    public class GESettingsTagsAS : BaseAppService<GESettingsTags>, IGESettingsTagsAS
    {
        private readonly IGESettingsTagsService _IGESettingsTagsService;

        public GESettingsTagsAS(IGESettingsTagsService pIGESettingsTagsService) :
             base(pIGESettingsTagsService)
        {
            _IGESettingsTagsService = pIGESettingsTagsService;
        }
    }
}
