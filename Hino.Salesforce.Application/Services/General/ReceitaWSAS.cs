﻿using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.Utils.ReceitaWS;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.General
{
    public class ReceitaWSAS : IReceitaWSAS
    {
        private readonly ICEPAS _ICEPAS;
        public List<ModelException> Errors { get; set; }
        private Request _Request;

        public ReceitaWSAS(ICEPAS pICEPAS)
        {
            _ICEPAS = pICEPAS;
            Errors = new List<ModelException>();
            _Request = new Request();
        }

        public async Task<ReceitaWSEnterpriseVM> GetEnterpriseAsync(string pCNPJ, string pTokenCNPJ)
        {
            var resultEnterprise = new ReceitaWSEnterpriseVM();

            if (!pCNPJ.IsCNPJ())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "CNPJ",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.InvalidCNPJ },
                    Value = ""
                });
                return resultEnterprise;
            }

            try
            {
                var client = _Request.CreateClient();
                var request = _Request.CreateRequest($"{pCNPJ.OnlyNumbers()}", Method.GET);

                request.AddHeader("Authorization", $"Bearer {pTokenCNPJ}");

                var Iresponse = await client.ExecuteAsync(request);

                resultEnterprise = JsonConvert.DeserializeObject<ReceitaWSEnterpriseVM>(Iresponse.Content, new JsonSerializerSettings
                {
                    Culture = new System.Globalization.CultureInfo("pt-BR"),
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                    DateFormatString = "dd/MM/yyyy HH:mm:ss"
                });
                resultEnterprise.Address = await _ICEPAS.GetCEPAddressEnterpriseAsync(resultEnterprise);
                //resultEnterprise.cepLimpo, resultEnterprise.numero, "BR", true);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "CNPJ",
                    Messages = new[] { string.Format(Infra.Cross.Resources.RouterMessagesResource.StatusNoOK, e.Message) },
                    Value = ""
                });
                return resultEnterprise;
            }


            return resultEnterprise;
        }
    }
}
