using Hino.Salesforce.Application.Interfaces.Logistics;
using Hino.Salesforce.Application.ViewModels.Route;
using Hino.Salesforce.Domain.Logistics.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Logistics;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Logistics
{
    public class LORoutesAS : BaseAppService<LORoutes>, ILORoutesAS
    {
        private readonly ILORoutesService _ILORoutesService;

        public LORoutesAS(ILORoutesService pILORoutesService) :
             base(pILORoutesService)
        {
            _ILORoutesService = pILORoutesService;
        }

        public async Task<MapVM> GetMapJsonAsync(string pEstablishmentKey, long pId)
        {
            var maps = (await _ILORoutesService.QueryAsync(r => r.Id == pId && r.EstablishmentKey == pEstablishmentKey,
                s => s.LOTrips,
                s => s.LOWaypoints)).FirstOrDefault();

            if (maps == null)
                return null;

            var lstWayPoints = new List<MapPointVM>();
            var lstGeomertyTrips = new List<decimal[]>();

            foreach (var item in maps.LOWaypoints.OrderBy(r => r.WayPointIndex))
            {
                var geo = new MapGeometryVM();
                geo.type = "Point";
                geo.coordinates.Add(item.Lng);
                geo.coordinates.Add(item.Lat);

                lstWayPoints.Add(new MapPointVM
                {
                    type = "Feature",
                    geometry = geo,
                    properties = new MapPointProperyVM
                    {
                        title = item.Name,
                        address = item.Address,
                        icon = item.WayPointIndex == 0 ? "town-hall" : "information"
                    }
                });
            }

            foreach (var item in maps.LOTrips)
            {
                lstGeomertyTrips.Add(new decimal[] { item.Lng, item.Lat });
            }

            return new MapVM
            {
                coordinates = JsonConvert.SerializeObject(lstGeomertyTrips),
                features = JsonConvert.SerializeObject(lstWayPoints)
            };
        }
    }
}
