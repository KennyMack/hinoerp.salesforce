using Hino.Salesforce.Application.Interfaces.Logistics;
using Hino.Salesforce.Domain.Logistics.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Logistics;

namespace Hino.Salesforce.Application.Services.Logistics
{
    public class LOTripsAS : BaseAppService<LOTrips>, ILOTripsAS
    {
        private readonly ILOTripsService _ILOTripsService;

        public LOTripsAS(ILOTripsService pILOTripsService) :
             base(pILOTripsService)
        {
            _ILOTripsService = pILOTripsService;
        }
    }
}
