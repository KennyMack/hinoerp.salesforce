using Hino.Salesforce.Application.Interfaces.Logistics;
using Hino.Salesforce.Domain.Logistics.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Logistics;

namespace Hino.Salesforce.Application.Services.Logistics
{
    public class LOWaypointsAS : BaseAppService<LOWaypoints>, ILOWaypointsAS
    {
        private readonly ILOWaypointsService _ILOWaypointsService;

        public LOWaypointsAS(ILOWaypointsService pILOWaypointsService) :
             base(pILOWaypointsService)
        {
            _ILOWaypointsService = pILOWaypointsService;
        }
    }
}
