﻿using Hino.Salesforce.Application.Interfaces.Logistics;
using Hino.Salesforce.Application.Interfaces.Route;
using Hino.Salesforce.Application.Utils.Here;
using Hino.Salesforce.Application.ViewModels.Route;
using Hino.Salesforce.Infra.Cross.Entities.Logistics;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Route
{
    public class HereMaps : IHereMaps
    {
        public List<ModelException> Errors { get; set; }
        private Request _Request;
        private readonly ILORoutesAS _ILORoutesAS;
        private readonly ILOTripsAS _ILOTripsAS;
        private readonly ILOWaypointsAS _ILOWaypointsAS;

        public HereMaps(ILORoutesAS pILORoutesAS,
            ILOTripsAS pILOTripsAS,
            ILOWaypointsAS pILOWaypointsAS)
        {
            _ILORoutesAS = pILORoutesAS;
            _ILOTripsAS = pILOTripsAS;
            _ILOWaypointsAS = pILOWaypointsAS;
            _Request = new Request();
            Errors = new List<ModelException>();
        }

        public async Task<HereMapsResultVM> GetAddressAsync(AddressGeoVM pAddress)
        {
            try
            {
                var client = _Request.CreateClient();
                // geocode.json?apiKey=mpEMYsnNE-GZpFcDph6mUuQVRJKGuZoMnAh20sGOBV4&searchtext=[ADDR]&locationattributes=all
                var request = _Request.CreateRequest($"geocode.json?", Method.GET);

                var address = pAddress.ToAddress();
                request.AddParameter("apiKey", "mpEMYsnNE-GZpFcDph6mUuQVRJKGuZoMnAh20sGOBV4", ParameterType.GetOrPost);
                request.AddParameter("searchtext", address, ParameterType.GetOrPost);
                request.AddParameter("locationattributes", "all", ParameterType.GetOrPost);

                var Iresponse = await client.ExecuteAsync(request);

                var result = JsonConvert.DeserializeObject<HereMapsResultVM>(Iresponse.Content);

                // result.SetData(pAddress);

                return result;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "Status",
                    Messages = new[] { string.Format(Infra.Cross.Resources.RouterMessagesResource.StatusNoOK, e.Message) },
                    Value = ""
                });
                return null;
            }

        }

        public async Task<HereFindSequenceResultVM> FindSequenceAsync(OptimizedVM pLocations)
        {
            try
            {
                var client = _Request.CreateClientFind();
                var request = _Request.CreateRequest($"findsequence.json?", Method.GET);

                var origin = pLocations.location.First().ToStart();
                var end = pLocations.location.First().ToEnd();

                var destinations = pLocations.GetDestinations();

                request.AddParameter("apiKey", "mpEMYsnNE-GZpFcDph6mUuQVRJKGuZoMnAh20sGOBV4", ParameterType.GetOrPost);

                request.AddParameter("start", origin, ParameterType.GetOrPost);

                for (int i = 0, length = destinations.Count; i < length; i++)
                    request.AddParameter($"destination{i + 1}", destinations[i].ToDestination(), ParameterType.GetOrPost);

                request.AddParameter("end", end, ParameterType.GetOrPost);
                request.AddParameter("mode", "fastest;truck", ParameterType.GetOrPost);
                request.AddParameter("hasTrailer", true, ParameterType.GetOrPost);
                request.AddParameter("requestId", Guid.NewGuid().ToString(), ParameterType.GetOrPost);

                var Iresponse = await client.ExecuteAsync(request);

                var result = JsonConvert.DeserializeObject<HereFindSequenceResultVM>(Iresponse.Content);

                if (result.errors.Count > 0)
                    throw new RouteException(result.errors.First());

                if (result.results.Count <= 0)
                    throw new RouteException(Infra.Cross.Resources.RouterMessagesResource.NoRoutesFinded);

                var trip = result.results.First();

                var route = new LORoutes();

                route.Id = await _ILORoutesAS.NextSequenceAsync();
                route.EstablishmentKey = pLocations.EstablishmentKey;
                route.Weigth = 0;
                route.Duration = trip.time;
                route.Distance = trip.distance;
                route.Note = trip.description;

                foreach (var item in trip.waypoints)
                {
                    LocationVM locationItem = null;
                    if (item.id == "Origem" ||
                        item.id == "Destino")
                        locationItem = pLocations.location.First();
                    else
                        locationItem = pLocations.location.FirstOrDefault(r => r.name == item.id);

                    var interconection = trip.interconnections.FirstOrDefault(r => r.toWaypoint == item.id);

                    if (locationItem != null)
                    {
                        var WayPoint = new LOWaypoints
                        {
                            Id = await _ILOWaypointsAS.NextSequenceAsync(),
                            EstablishmentKey = pLocations.EstablishmentKey,
                            RouteID = route.Id,
                            Distance = interconection?.distance ?? 0,
                            Name = locationItem.name,
                            Address = locationItem.address,
                            WayPointIndex = item.sequence,
                            TripIndex = 0,
                            Lat = item.lat,
                            Lng = item.lng,
                            Created = DateTime.Now,
                            Modified = DateTime.Now,
                            IsActive = true,
                            UniqueKey = Guid.NewGuid().ToString()
                        };

                        route.LOWaypoints.Add(WayPoint);
                    }
                }

                _ILORoutesAS.Add(route);
                await _ILORoutesAS.SaveChanges();

                result.Id = route.Id;
                result.EstablishmentKey = route.EstablishmentKey;
                result.UniqueKey = route.UniqueKey;

                return result;
            }
            catch (RouteException e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "Status",
                    Messages = new[] { e.Message },
                    Value = ""
                });
                return null;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "Status",
                    Messages = new[] { string.Format(Infra.Cross.Resources.RouterMessagesResource.StatusNoOK, e.Message) },
                    Value = ""
                });
                return null;
            }
        }
    }
}
