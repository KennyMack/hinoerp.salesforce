﻿using Hino.Salesforce.Application.Interfaces.Logistics;
using Hino.Salesforce.Application.Interfaces.Route;
using Hino.Salesforce.Application.ViewModels.Route;
using Hino.Salesforce.Infra.Cross.Entities.Logistics;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Route
{
    public class MapBox : IMapBox
    {
        private readonly ILORoutesAS _ILORoutesAS;
        private readonly ILOTripsAS _ILOTripsAS;
        private readonly ILOWaypointsAS _ILOWaypointsAS;

        public MapBox(ILORoutesAS pILORoutesAS,
            ILOTripsAS pILOTripsAS,
            ILOWaypointsAS pILOWaypointsAS)
        {
            _ILORoutesAS = pILORoutesAS;
            _ILOTripsAS = pILOTripsAS;
            _ILOWaypointsAS = pILOWaypointsAS;
            Errors = new List<ModelException>();
        }

        const string BaseRoute = "https://api.mapbox.com/";
        public List<ModelException> Errors { get; set; }

        private RestClient CreateClient() =>
            new RestClient(BaseRoute);

        private RestRequest CreateRequest(string pUri, Method pMethod) =>
            new RestRequest(pUri, pMethod);

        private RestRequest AddHeaders(RestRequest pRequest)
        {
            pRequest.AddHeader("Accept", @"application\json");
            pRequest.AddHeader("Content-Type", "application/x-www-form-urlencoded");

            return pRequest;
        }

        public async Task<MapBoxResultVM> GetAddressAsync(AddressGeoVM pAddress)
        {
            try
            {
                var client = CreateClient();
                var request = CreateRequest($"geocoding/v5/mapbox.places/{pAddress.ToString()}.json?", Method.GET);
                request = AddHeaders(request);

                request.AddParameter("types", "address", ParameterType.GetOrPost);
                request.AddParameter("country", pAddress.Country, ParameterType.GetOrPost);
                request.AddParameter("routing", true, ParameterType.GetOrPost);
                request.AddParameter("autocomplete", false, ParameterType.GetOrPost);
                request.AddParameter("fuzzyMatch", false, ParameterType.GetOrPost);
                request.AddParameter("access_token", "pk.eyJ1Ijoia2VubnltYWNrIiwiYSI6ImNrYXZieG94eDB3aTIyc2xlMWNlY3U5YjEifQ.ZB9WPuhtoeboNIJ6IMOh9g", ParameterType.GetOrPost);

                var Iresponse = await client.ExecuteAsync(request);

                var result = JsonConvert.DeserializeObject<MapBoxResultVM>(Iresponse.Content);

                result.SetData(pAddress);

                return result;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "Status",
                    Messages = new[] { string.Format(Infra.Cross.Resources.RouterMessagesResource.StatusNoOK, e.Message) },
                    Value = ""
                });
                return null;
            }
        }

        public async Task<OptimizedTripResultVM> OptimizedLocationsAsync(OptimizedVM pLocations)
        {
            try
            {
                var client = CreateClient();
                var request = CreateRequest($"optimized-trips/v1/mapbox/driving/{pLocations.ToString()}.json?", Method.GET);
                request = AddHeaders(request);

                request.AddParameter("geometries", "geojson", ParameterType.GetOrPost);

                var radiuses = pLocations.GetRadiuses();

                request.AddParameter("radiuses", radiuses, ParameterType.GetOrPost);
                request.AddParameter("access_token", "pk.eyJ1Ijoia2VubnltYWNrIiwiYSI6ImNrYXZieG94eDB3aTIyc2xlMWNlY3U5YjEifQ.ZB9WPuhtoeboNIJ6IMOh9g", ParameterType.GetOrPost);

                var Iresponse = await client.ExecuteAsync(request);

                var result = JsonConvert.DeserializeObject<OptimizedTripResultVM>(Iresponse.Content);
                var route = new LORoutes();

                var trip = result.trips.First();
                route.Id = await _ILORoutesAS.NextSequenceAsync();
                route.EstablishmentKey = pLocations.EstablishmentKey;
                route.Weigth = trip.weight;
                route.Duration = trip.duration;
                route.Distance = trip.distance;
                route.Note = "";

                var coordinates = trip.geometry.coordinates;
                foreach (var item in coordinates)
                {
                    route.LOTrips.Add(new LOTrips
                    {
                        Id = await _ILOTripsAS.NextSequenceAsync(),
                        EstablishmentKey = pLocations.EstablishmentKey,
                        RouteID = route.Id,
                        Lat = item[1],
                        Lng = item[0],
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        IsActive = true,
                        UniqueKey = Guid.NewGuid().ToString()
                    });
                }

                foreach (var item in result.waypoints)
                {
                    var WayPoint = new LOWaypoints
                    {
                        Id = await _ILOWaypointsAS.NextSequenceAsync(),
                        EstablishmentKey = pLocations.EstablishmentKey,
                        RouteID = route.Id,
                        Distance = item.distance,
                        Name = item.name,
                        WayPointIndex = item.waypoint_index,
                        TripIndex = item.trips_index,
                        Lat = item.location[1],
                        Lng = item.location[0],
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        IsActive = true,
                        UniqueKey = Guid.NewGuid().ToString()
                    };

                    var location = pLocations.location.Where(r =>
                        r.lat == WayPoint.Lat &&
                        r.lng == WayPoint.Lng &&
                        !string.IsNullOrEmpty(r.address))
                        .FirstOrDefault();

                    if (location != null)
                        WayPoint.Name = location.address;

                    route.LOWaypoints.Add(WayPoint);
                }

                _ILORoutesAS.Add(route);
                await _ILORoutesAS.SaveChanges();

                result.Id = route.Id;
                result.EstablishmentKey = pLocations.EstablishmentKey;
                result.UniqueKey = pLocations.ToString();

                return result;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "Status",
                    Messages = new[] { string.Format(Infra.Cross.Resources.RouterMessagesResource.StatusNoOK, e.Message) },
                    Value = ""
                });
                return null;
            }
        }
    }
}
