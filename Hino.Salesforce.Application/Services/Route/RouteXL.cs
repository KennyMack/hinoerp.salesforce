﻿using Hino.Salesforce.Application.Interfaces.Route;
using Hino.Salesforce.Application.ViewModels.Route;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Route
{
    public class RouteXL : IRouteXL
    {
        public List<ModelException> Errors { get; set; }

        public RouteXL()
        {
            Errors = new List<ModelException>();
        }

        private RestClient CreateClient() =>
            new RestClient(BaseRoute());

        private RestRequest CreateRequest(string pUri, Method pMethod) =>
            new RestRequest(pUri, pMethod);

        private RestRequest AddHeaders(RestRequest pRequest)
        {
            pRequest.AddHeader("Authorization", $"Basic {GetAuthentication("kennymack", "J0n4th4n")}");
            pRequest.AddHeader("Accept", @"application\json");
            pRequest.AddHeader("Content-Type", "application/x-www-form-urlencoded");

            return pRequest;
        }

        public string BaseRoute() =>
            "https://api.routexl.com/";

        public string GetAuthentication(string pUser, string pPassword)
        {
            byte[] bytes = Encoding.GetEncoding(28591).GetBytes($"{pUser}:{pPassword}");

            string toReturn = Convert.ToBase64String(bytes);

            return toReturn;
        }

        public async Task<bool> GetStatus()
        {
            try
            {
                var client = CreateClient();
                var request = CreateRequest("status/MandaSalve", Method.GET);
                request = AddHeaders(request);

                var Iresponse = await client.ExecuteAsync(request);

                var StatusResult = JsonConvert.DeserializeObject<StatusVM>(Iresponse.Content);

                return StatusResult.echo == "MandaSalve";
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "Status",
                    Messages = new[] { string.Format(Infra.Cross.Resources.RouterMessagesResource.StatusNoOK, e.Message) },
                    Value = ""
                });
                return false;
            }
        }

        public async Task<TourVM> PostTour()
        {
            if (await GetStatus())
            {
                try
                {
                    var client = CreateClient();
                    var request = CreateRequest("tour", Method.POST);
                    request = AddHeaders(request);
                    var array = new List<AddressesVM>();
                    array.Add(new AddressesVM
                    {
                        address = "R. Dr. Pedro Grota, 2-210 - Jardim Santa Eulália, Limeira - SP, Brazil",
                        lat = -22.598124M,
                        lng = -47.404973M
                    });
                    array.Add(new AddressesVM
                    {
                        address = "R. Humaitá, 566 - Centro, Limeira - SP, Brazil",
                        lat = -22.565508M,
                        lng = -47.407371M
                    });
                    array.Add(new AddressesVM
                    {
                        address = "R. Jaír Formigari, 119 - Jardim Santa Fe, Limeira - SP, Brazil",
                        lat = -22.566770M,
                        lng = -47.423608M
                    });
                    array.Add(new AddressesVM
                    {
                        address = "R. Túlio Taques de Lemos, 94 - Parque das Nacoes, Limeira - SP, Brazil",
                        lat = -22.592200M,
                        lng = -47.406670M
                    });
                    array.Add(new AddressesVM
                    {
                        address = "R. Profa. Júlia Lange Adrien Barros, 511 - Jardim Sen. Vergueiro, Limeira - SP, Brazil",
                        lat = -22.569008M,
                        lng = -47.424997M
                    });
                    array.Add(new AddressesVM
                    {
                        address = "R. Prof. Otaviano José Rodrigues, 1335 - Vila Santa Lina, Limeira - SP, Brazil",
                        lat = -22.585103M,
                        lng = -47.404549M
                    });
                    array.Add(new AddressesVM
                    {
                        address = "R. Santa Cruz, 1040 - Centro, Limeira - SP, Brazil",
                        lat = -22.565783M,
                        lng = -47.406032M
                    });


                    var json = JsonConvert.SerializeObject(array);

                    request.AddParameter("locations", json, ParameterType.GetOrPost);

                    var Iresponse = await client.ExecuteAsync(request);

                    var StatusResult = JsonConvert.DeserializeObject<TourVM>(Iresponse.Content);

                    return StatusResult;
                }
                catch (Exception e)
                {
                    Logging.Exception(e);
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                        Field = "Tour",
                        Messages = new[] { string.Format(Infra.Cross.Resources.RouterMessagesResource.StatusNoOK, e.Message) },
                        Value = ""
                    });
                    return null;
                }
            }
            return null;
        }
    }
}
