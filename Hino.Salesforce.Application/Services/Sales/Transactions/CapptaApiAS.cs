﻿using Hino.Salesforce.Application.Interfaces.Sales.Transactions;
using Hino.Salesforce.Application.Utils.CapptaApi;
using Hino.Salesforce.Application.ViewModels.Sales.Transactions;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Sales.Transactions
{
    public class CapptaApiAS : ICapptaApiAS
    {
        String Token { get; set; }
        public List<ModelException> Errors { get; set; }

        private Request _Request;

        public CapptaApiAS()
        {
            Errors = new List<ModelException>();
            _Request = new Request();
        }

        public async Task<CapptaApiVM> GetTransactionAsync(string authCode)
        {
            var resultTransaction = new CapptaApiVM();
            var body = new CapptaApiSearchVM()
            {
                cnpjs = new List<string>() { "34555898000186" },
                initialdate = new DateTime(2019, 06, 01),
                finalDate = new DateTime(2019, 07, 13),
                uniqueSequentialNumber = authCode
            };

            if (String.IsNullOrEmpty(authCode))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "AUTHCODE",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.InvalidValue },
                    Value = ""
                });

                return resultTransaction;
            }

            if (!await Authenticate())
                return resultTransaction;

            try
            {
                var client = _Request.CreateClient();
                var request = _Request.CreateRequest("transaction", RestSharp.Method.POST);

                request.AddHeader("Authorization", $"Bearer {Token}");
                request.AddParameter("application/json", JsonConvert.SerializeObject(body), RestSharp.ParameterType.RequestBody);

                var response = await client.ExecuteAsync(request);

                if (response.IsSuccessful)
                {
                    var capptaApiResponse = JsonConvert.DeserializeObject<CapptaApiResponseVM>(response.Content, new JsonSerializerSettings
                    {
                        Culture = new System.Globalization.CultureInfo("pt-BR"),
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                        DateFormatString = "dd/MM/yyyy HH:mm:ss"
                    });

                    if (capptaApiResponse?.results != null)
                        resultTransaction = capptaApiResponse.results.FirstOrDefault();
                    else
                        Errors.Add(new ModelException
                        {
                            ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                            Field = "AUTHCODE",
                            Messages = new[] { string.Format(Infra.Cross.Resources.ValidationMessagesResource.NotFound, response.ErrorMessage) },
                            Value = ""
                        });
                }
                else
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                        Field = "AUTHCODE",
                        Messages = new[] { string.Format(Infra.Cross.Resources.RouterMessagesResource.StatusNoOK, response.ErrorMessage) },
                        Value = ""
                    });
                }
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "AUTHCODE",
                    Messages = new[] { string.Format(Infra.Cross.Resources.RouterMessagesResource.StatusNoOK, e.Message) },
                    Value = ""
                });
                return resultTransaction;
            }

            return resultTransaction;
        }

        public async Task<Boolean> Authenticate()
        {
            Boolean authenticated = false;
            var client = _Request.CreateAuthClient();
            var request = _Request.CreateRequest("", RestSharp.Method.POST);

            request.AddParameter("username", "hino_crm", RestSharp.ParameterType.GetOrPost);
            request.AddParameter("password", "*d_nC7C*'KBN3dbY", RestSharp.ParameterType.GetOrPost);
            request.AddParameter("client_id", "front_end", RestSharp.ParameterType.GetOrPost);
            request.AddParameter("grant_type", "password", RestSharp.ParameterType.GetOrPost);

            var result = await client.ExecuteAsync(request);

            if (result.IsSuccessful)
            {
                var apiResponse = new { access_token = "" };
                Token = JsonConvert.DeserializeAnonymousType(result.Content, apiResponse).access_token;
                authenticated = true;
            }
            else
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.UnauthorizedRequest,
                    Field = "AUTHCODE",
                    Messages = new[] { string.Format(Infra.Cross.Resources.RouterMessagesResource.StatusNoOK, result.ErrorMessage) },
                    Value = ""
                });
            }

            return authenticated;
        }
    }
}
