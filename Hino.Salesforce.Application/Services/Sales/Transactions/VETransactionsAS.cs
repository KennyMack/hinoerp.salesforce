using AutoMapper;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.Interfaces.Sales.Transactions;
using Hino.Salesforce.Application.ViewModels.Sales.Transactions;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Domain.Sales.Interfaces.Services.Transaction;
using Hino.Salesforce.Infra.Cross.Entities.Sales.Transactions;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Sales.Transactions
{
    public class VETransactionsAS : BaseAppService<VETransactions>, IVETransactionsAS
    {
        private readonly IVETransactionsService _IVETransactionsService;
        private readonly IVEOrdersService _IVEOrdersService;

        public VETransactionsAS(IVETransactionsService pIVETransactionsService, IVEOrdersService pIVEORdersService) :
             base(pIVETransactionsService)
        {
            _IVETransactionsService = pIVETransactionsService;
            _IVEOrdersService = pIVEORdersService;
        }

        public async Task<VETransactionsVM> CreateTransaction(VETransactionsVM pTransaction)
        {
            var retTransaction = _IVETransactionsService.Add(Mapper.Map<VETransactionsVM, VETransactions>(pTransaction));

            if (!_IVETransactionsService.Errors.Any())
            {
                var order = _IVEOrdersService.GetByIdToUpdate(retTransaction.OrderID, retTransaction.EstablishmentKey, retTransaction.UniqueKey);
                order.StatusPay = EStatusPay.Processing;
                var orderUpdated = _IVEOrdersService.Update(order);
            }

            if (_IVEOrdersService.Errors.Any())
                _IVETransactionsService.Errors.AddRange(_IVEOrdersService.Errors);

            if (!_IVETransactionsService.Errors.Any())
            {
                await _IVETransactionsService.SaveChanges();
                await _IVEOrdersService.SaveChanges();
            }

            if (!_IVETransactionsService.Errors.Any() && !pTransaction.AuthCode.IsEmpty())
                await _IVETransactionsService.GenerateEntryQueueAsync();
            
            return Mapper.Map<VETransactions, VETransactionsVM>(retTransaction);
        }

        public async Task<VETransactionsVM> UpdateTransaction(VETransactionsVM pTransaction)
        {
            var Result = _IVETransactionsService.Update(Mapper.Map<VETransactionsVM, VETransactions>(pTransaction));

            if (!_IVETransactionsService.Errors.Any())
                await _IVETransactionsService.SaveChanges();

            if (!_IVETransactionsService.Errors.Any() && !pTransaction.AuthCode.IsEmpty())
                await _IVETransactionsService.GenerateEntryQueueAsync();

            return Mapper.Map<VETransactions, VETransactionsVM>(Result);
        }

        public override async Task<VETransactions> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            var transaction = await _IVETransactionsService.GetByIdAsync(id, pEstablishmentKey, pUniqueKey);

            if (transaction == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "VETransactions",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.NotFound },
                    Value = id.ToString()
                });
                return null;
            }

            if (transaction?.Status == ETransactionStatus.Done)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.ValidationError,
                    Field = "VETransactions",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("RemoveNotAllowed") },
                    Value = id.ToString()
                });
                return null;
            }

            await _IVETransactionsService.RemoveById(id, pEstablishmentKey, pUniqueKey);

            if (!_IVETransactionsService.Errors.Any())
            {
                var order = _IVEOrdersService.GetByIdToUpdate(transaction.OrderID, transaction.EstablishmentKey, transaction.UniqueKey);
                var transactions = await _IVETransactionsService.QueryAsync(r => r.OrderID == transaction.OrderID && r.Id != transaction.Id);
                if (!transactions.Any())
                {
                    order.StatusPay = EStatusPay.Pending;
                }
                else 
                {
                    var amountPaid = (float)transactions.Where(r => r.Status == ETransactionStatus.Done).Sum(r => r.Amount);
                    if (amountPaid == 0)
                        order.StatusPay = EStatusPay.Processing;
                    else if (amountPaid < order.TotalValue)
                        order.StatusPay = EStatusPay.Parcial;
                    else
                        order.StatusPay = EStatusPay.Total;
                }

                var updatedOrder = _IVEOrdersService.Update(order);
            }

            if (!_IVETransactionsService.Errors.Any())
            {
                await _IVETransactionsService.SaveChanges();
                await _IVEOrdersService.SaveChanges();
            }

            if (!_IVETransactionsService.Errors.Any())
                await _IVETransactionsService.GenerateEntryQueueAsync();

            return transaction;
        }

        public async Task<VETransactionsVM> SearchTransationByNSUAsync(string pEstablishmentKey, string NSU)
        {
            var transaction = (await _IVETransactionsService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                             r.NSU == NSU)).FirstOrDefault();

            return Mapper.Map<VETransactions, VETransactionsVM>(transaction);
        }

        public override async Task<IEnumerable<VETransactions>> SyncData(string pEstablishmentKey, VETransactions[] pRegisters)
        {
            List<ModelException> SyncErrors = new List<ModelException>();

            for (int i = 0, length = pRegisters.Length; i < length; i++)
            {
                VETransactionsVM result;

                if (pRegisters[i].Id <= 0)
                    result = await CreateTransaction(Mapper.Map<VETransactions, VETransactionsVM>(pRegisters[i]));
                else
                    result = await UpdateTransaction(Mapper.Map<VETransactions, VETransactionsVM>(pRegisters[i]));

                if (Errors.Any())
                {
                    pRegisters[i].Id = 0;
                    SyncErrors.Add(Errors[0]);
                }
                else
                    pRegisters[i] = Mapper.Map<VETransactionsVM, VETransactions>(result);
            }

            if (!Errors.Any())
                await SaveChanges();

            return pRegisters;
        }
    }
}
