using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;

namespace Hino.Salesforce.Application.Services.Sales
{
    public class VEEntSalePriceAS : BaseAppService<VEEntSalePrice>, IVEEntSalePriceAS
    {
        private readonly IVEEntSalePriceService _IVEEntSalePriceService;

        public VEEntSalePriceAS(IVEEntSalePriceService pIVEEntSalePriceService) : 
             base(pIVEEntSalePriceService)
        {
            _IVEEntSalePriceService = pIVEEntSalePriceService;
        }
    }
}
