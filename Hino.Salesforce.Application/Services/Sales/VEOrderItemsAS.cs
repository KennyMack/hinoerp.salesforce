using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using System.Collections.Generic;

namespace Hino.Salesforce.Application.Services.Sales
{
    public class VEOrderItemsAS : BaseAppService<VEOrderItems>, IVEOrderItemsAS
    {
        private readonly IVEOrderItemsService _IVEOrderItemsService;

        public VEOrderItemsAS(IVEOrderItemsService pIVEOrderItemsService) :
             base(pIVEOrderItemsService)
        {
            _IVEOrderItemsService = pIVEOrderItemsService;
        }

        public IEnumerable<VEOrderItems> GetItemsByOrderId(long OrderId, string pEstablishmentKey) =>
            _IVEOrderItemsService.GetItemsByOrderId(OrderId, pEstablishmentKey);
    }
}
