using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;

namespace Hino.Salesforce.Application.Services.Sales
{
    public class VEOrderTaxesAS : BaseAppService<VEOrderTaxes>, IVEOrderTaxesAS
    {
        private readonly IVEOrderTaxesService _IVEOrderTaxesService;

        public VEOrderTaxesAS(IVEOrderTaxesService pIVEOrderTaxesService) :
             base(pIVEOrderTaxesService)
        {
            _IVEOrderTaxesService = pIVEOrderTaxesService;
        }
    }
}
