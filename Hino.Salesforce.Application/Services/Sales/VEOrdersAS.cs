using AutoMapper;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Domain.Base.Interfaces.UoW;
using Hino.Salesforce.Domain.Fiscal.Interfaces.Services;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Sales
{
    public class VEOrdersAS : BaseAppService<VEOrders>, IVEOrdersAS
    {
        private readonly IVEOrdersService _IVEOrdersService;
        private readonly IVEOrderItemsService _IVEOrderItemsService;
        private readonly IVEOrderTaxesService _IVEOrderTaxesService;
        private readonly IVESalePriceService _IVESalePriceService;
        private readonly IGEEnterprisesService _IGEEnterprisesService;
        private readonly IGEEstablishmentsService _IGEEstablishmentsService;
        private readonly IGEPaymentTypeService _IGEPaymentTypeService;
        private readonly IGEProductsService _IGEProductsService;
        private readonly IGEPaymentConditionService _IGEPaymentConditionService;
        private readonly IGEUsersService _IGEUsersService;
        private readonly IGEUserEnterprisesService _IGEUserEnterprisesService;
        private readonly IFSFiscalOperService _IFSFiscalOperService;
        private readonly IUnitOfWork _IUnitOfWork;

        public VEOrdersAS(IUnitOfWork pIUnitOfWork,
                          IVEOrdersService pIVEOrdersService,
                          IVEOrderItemsService pIVEOrderItemsService,
                          IGEEstablishmentsService pIGEEstablishmentsService,
                          IGEEnterprisesService pIGEEnterprisesService,
                          IGEPaymentTypeService pIGEPaymentTypeService,
                          IGEProductsService pIGEProductsService,
                          IGEPaymentConditionService pIGEPaymentConditionService,
                          IGEUsersService pIGEUsersService,
                          IGEUserEnterprisesService pIGEUserEnterprisesService,
                          IVEOrderTaxesService pIVEOrderTaxesService,
                          IFSFiscalOperService pIFSFiscalOperService,
                          IVESalePriceService pIVESalePriceService
                          ) :
             base(pIVEOrdersService)
        {
            _IFSFiscalOperService = pIFSFiscalOperService;
            _IVEOrderTaxesService = pIVEOrderTaxesService;
            _IGEUsersService = pIGEUsersService;
            _IUnitOfWork = pIUnitOfWork;
            _IGEEnterprisesService = pIGEEnterprisesService;
            _IGEPaymentTypeService = pIGEPaymentTypeService;
            _IGEProductsService = pIGEProductsService;
            _IGEPaymentConditionService = pIGEPaymentConditionService;
            _IVEOrdersService = pIVEOrdersService;
            _IVEOrderItemsService = pIVEOrderItemsService;
            _IGEEstablishmentsService = pIGEEstablishmentsService;
            _IGEUserEnterprisesService = pIGEUserEnterprisesService;
            _IVESalePriceService = pIVESalePriceService;
        }

        private async Task<bool> ValidateAsync(VEOrdersVM pOrder, bool isCreate)
        {
            var exists = (await _IGEEnterprisesService.QueryAsync(r => r.EstablishmentKey == pOrder.EstablishmentKey &&
            r.Id == pOrder.EnterpriseID)).Any();

            GEPaymentType paymentType = null;

            if (!exists)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "EnterpriseID",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.EnterpriseIdNotFound },
                    Value = pOrder.EnterpriseID.ToString()
                });
                return false;
            }

            if (pOrder.CarrierID != null)
            {
                exists = (await _IGEEnterprisesService.QueryAsync(r => r.EstablishmentKey == pOrder.EstablishmentKey &&
                r.Id == pOrder.CarrierID)).Any();

                if (!exists)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                        Field = "CarrierID",
                        Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.CarrierIdNotFound },
                        Value = pOrder.CarrierID.ToString()
                    });
                    return false;
                }
            }

            if (pOrder.RedispatchID != null)
            {
                exists = (await _IGEEnterprisesService.QueryAsync(r => r.EstablishmentKey == pOrder.EstablishmentKey &&
                r.Id == pOrder.RedispatchID)).Any();

                if (!exists)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                        Field = "RedispatchID",
                        Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.RedispatchIdNotFound },
                        Value = pOrder.RedispatchID.ToString()
                    });
                    return false;
                }
            }

            if (pOrder.TypePaymentID > 0)
            {
                paymentType = (await _IGEPaymentTypeService.QueryAsync(r => r.EstablishmentKey == pOrder.EstablishmentKey && r.Id == pOrder.TypePaymentID)).FirstOrDefault();
                var enterprise = (await _IGEEnterprisesService.QueryAsync(r => r.EstablishmentKey == pOrder.EstablishmentKey && r.Id == pOrder.EnterpriseID)).FirstOrDefault();

                if (paymentType != null && paymentType.Boleto && enterprise.Type == EEnterpriseType.PessoaFisica)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                        Field = "TypePaymentID",
                        Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.BoletoNotAllowed },
                        Value = pOrder.TypePaymentID.ToString()
                    });

                    return false;
                }
            }

            if (pOrder.InPerson && !pOrder.Note.HasLicensePlate())
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "LicensePlate",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.LicensePlateRequired },
                    Value = pOrder.Note ?? ""
                });
                return false;
            }

            exists = (await _IGEPaymentConditionService.QueryAsync(r => r.EstablishmentKey == pOrder.EstablishmentKey &&
                        r.Id == pOrder.PayConditionID)).Any();

            if (!exists)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "PayConditionID",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.PayConditionIdNotFound },
                    Value = pOrder.EnterpriseID.ToString()
                });
                return false;
            }
            /*
            exists = (await _IGEPaymentTypeService.QueryAsync(r => r.EstablishmentKey == pOrder.EstablishmentKey &&
                        r.Id == pOrder.TypePaymentID)).Any();
            */
            if (paymentType == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "TypePaymentID",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.TypePaymentIdNotFound },
                    Value = pOrder.EnterpriseID.ToString()
                });
                return false;
            }

            var condition = (await _IGEPaymentConditionService.QueryAsync(r => r.EstablishmentKey == pOrder.EstablishmentKey &&
                        r.Id == pOrder.PayConditionID)).FirstOrDefault();

            if (condition != null &&
                condition.TypePayID != pOrder.TypePaymentID)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "PayConditionID",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.ConditionIdIsNotChildTypePayId },
                    Value = pOrder.PayConditionID.ToString()
                });
                return false;
            }

            if (pOrder.FreightPaidBy != EFreightPaidBy.SemFrete &&
                pOrder.CarrierID == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "CarrierID",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.CarrierIdNotSupplied },
                    Value = pOrder.CarrierID.ToString()
                });
                return false;
            }

            if (pOrder.FreightPaidBy == EFreightPaidBy.SemFrete &&
                pOrder.FreightValue > 0)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "FreightValue",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.FreightValueNotAllowed },
                    Value = pOrder.FreightValue.ToString()
                });
                return false;
            }

            if (pOrder.TotalValue < pOrder.AdvanceAmount)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "AdvanceAmount",
                    Messages = new[] { String.Format(Infra.Cross.Resources.ValidationMessagesResource.MaxValue, pOrder.TotalValue.ToString("n2")) },
                    Value = pOrder.AdvanceAmount.ToString()
                });
                return false;
            }

            if (pOrder.UserCreatedID > 0)
            {
                var User = (await _IGEUsersService.QueryAsync(r => r.EstablishmentKey == pOrder.EstablishmentKey &&
                            r.Id == pOrder.UserCreatedID)).FirstOrDefault();

                if (User == null)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                        Field = "UserId",
                        Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.UserNotFound },
                        Value = pOrder.UserID.ToString()
                    });
                    return false;
                }

                if (isCreate && User.IsBlockedByPay)
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                        Field = "UserId",
                        Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.UserBlockedByPayment },
                        Value = pOrder.UserID.ToString()
                    });
                    return false;
                }

                if (isCreate && User.BoletoLimit > 0 && paymentType.Boleto)
                {
                    float limiteUsed = (await _IVEOrdersService.QueryAsync(r => r.EstablishmentKey == pOrder.EstablishmentKey &&
                                                                                r.UserID == pOrder.UserID &&
                                                                                r.GEPaymentType.Boleto &&
                                                                                r.StatusCRM != "R" &&
                                                                                r.Status != "R" &&
                                                                                !r.IsProposal &&
                                                                                r.Id != pOrder.Id &&
                                                                                r.StatusPay != EStatusPay.Total)).Sum(s => s.TotalValue - s.PaidAmount);

                    if ((User.BoletoLimit - limiteUsed) < pOrder.TotalValue)
                    {
                        Errors.Add(new ModelException
                        {
                            ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                            Field = "TotalValue",
                            Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.BoletoLimitInsufficient },
                            Value = pOrder.TotalValue.ToString()
                        });
                        return false;
                    }
                }

                if (User.UserType == EUserType.Representant && isCreate)
                {
                    var UserEnterprises = await _IGEUserEnterprisesService.QueryAsync(r => r.EstablishmentKey == pOrder.EstablishmentKey &&
                    r.UserId == User.Id &&
                    r.EnterpriseId == pOrder.EnterpriseID);

                    if (!UserEnterprises.Any())
                    {
                        Errors.Add(new ModelException
                        {
                            ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                            Field = "EnterpriseID",
                            Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.EnterpriseDoesNotBelongToSeller },
                            Value = pOrder.EnterpriseID.ToString()
                        });
                        return false;
                    }
                }
            }

            var Establishment = await _IGEEstablishmentsService.GetByEstablishmentKeyAsync(pOrder.EstablishmentKey);
            if (Establishment.OnlyWithStock)
            {
                foreach (var item in pOrder.VEOrderItems)
                {
                    decimal quantityValidate = 0;

                    if (isCreate)
                        quantityValidate = Convert.ToDecimal(item.Quantity);
                    else
                    {
                        var OldItem = _IVEOrderItemsService.GetById(item.Id, item.EstablishmentKey, item.UniqueKey);

                        if (OldItem != null)
                            quantityValidate = Convert.ToDecimal(Math.Max(item.Quantity - OldItem.Quantity, 0));
                    }

                    if (quantityValidate > 0)
                    {
                        var Product = (await _IGEProductsService.QueryAsync(r => r.Id == item.ProductID &&
                                                                                 r.EstablishmentKey == item.EstablishmentKey)).FirstOrDefault();

                        decimal StockAvailable = Product.StockBalance;

                        if (StockAvailable < quantityValidate)
                        {
                            Errors.Add(new ModelException
                            {
                                ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                                Field = "Quantity",
                                Messages = new[] { String.Format(Infra.Cross.Resources.ErrorMessagesResource.InsufficientStock, Product.Name) },
                                Value = item.Quantity.ToString()
                            });
                            return false;
                        }
                    }
                }
            }

            return true;
        }

        public async Task<PagedResult<VEOrdersVM>> CanConvertToOrderAsync(string pEstablishmentKey, long id)
        {
            var lstResult = (await _IVEOrdersService.QueryPagedAsync(1, 100,
                r => r.EstablishmentKey == pEstablishmentKey &&
                r.Id == id));

            var OrderDB = lstResult.Results.FirstOrDefault();

            if (OrderDB == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "VEOrders",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("NotFound") },
                    Value = id.ToString()
                });
                return null;
            }

            if (OrderDB.GEEnterprises.Classification == EEnterpriseClassification.Prospect)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "VEOrders",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.EnterpriseIsProspect },
                    Value = id.ToString()
                });
                return null;
            }

            if (OrderDB.Status != "P")
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "VEOrders",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.StatusNotAllowChanges },
                    Value = id.ToString()
                });
                return null;
            }

            return Mapper.Map<PagedResult<VEOrders>, PagedResult<VEOrdersVM>>(lstResult);
        }

        private void CalculateDeliveryDate(VEOrders pOrder, List<VEOrderItemsVM> pItems)
        {
            var Days = pItems.Max(r => r.ShippingDays);

            pOrder.DeliveryDate = DateTime.Now.AddWorkdays(Days);
            foreach (var item in pItems)
                item.DeliveryDate = DateTime.Now.AddWorkdays(item.ShippingDays);
        }

        public async Task<VEOrdersVM> CopyOrderAsync(VEOrderStatusVM pOrder)
        {
            var OrderDB = await _IVEOrdersService.GetByIdAsync(pOrder.OrderID, pOrder.EstablishmentKey, pOrder.UniqueKey);

            if (OrderDB == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "VEOrders",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("NotFound") },
                    Value = pOrder.OrderID.ToString()
                });
                return null;
            }

            /*
            if (OrderDB.Status != "P")
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "VEOrders",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.StatusNotAllowChanges },
                    Value = pOrder.OrderID.ToString()
                });
                return null;
            }
            */

            var establishment = await _IGEEstablishmentsService.GetByEstablishmentKeyAsync(pOrder.EstablishmentKey);

            var OrderDestiny = new VEOrders();

            OrderDestiny.CopyProperties(OrderDB);

            OrderDB.RevisionReason = "";
            OrderDestiny.DigitizerID = pOrder.DigitizerID;
            OrderDestiny.RevisionReason = "";
            OrderDestiny.Id = 0;
            OrderDestiny.IdERP = 0;
            OrderDestiny.CodPedVenda = 0;
            OrderDestiny.OriginOrderID = null;
            OrderDestiny.MainOrderID = null;
            OrderDestiny.OrderVersion = 0;
            OrderDestiny.StatusSinc = EStatusSinc.Integrated;
            OrderDestiny.StatusCRM = "P";
            OrderDestiny.Status = "P";
            OrderDestiny.IsProposal = true;
            OrderDestiny.Created = DateTime.Now;
            OrderDestiny.Modified = DateTime.Now;
            OrderDestiny.PaymentDueDate = DateTime.Now.AddWorkdays(establishment.DaysPayment);

            OrderDestiny.IsActive = true;
            OrderDestiny.EstablishmentKey = pOrder.EstablishmentKey;
            OrderDestiny.UniqueKey = Guid.NewGuid().ToString();

            OrderDestiny.GEEnterprises = null;
            OrderDestiny.GEPaymentCondition = null;
            OrderDestiny.GEPaymentType = null;
            OrderDestiny.GERedispatch = null;
            OrderDestiny.GECarriers = null;
            OrderDestiny.GEEnterprises = null;
            OrderDestiny.GEUsers = null;
            OrderDestiny.VEOrderItems = null;
            OrderDestiny.GEUserDigitizer = null;
            OrderDestiny.MainFiscalOper = null;
            OrderDestiny.VETypeSale = null;
            OrderDestiny.VEOrderItems = new List<VEOrderItems>();

            long RegionId = 0;

            if (pOrder.CanChangeValue && OrderDestiny.IsProposal)
            {
                var Enterprise = await _IGEEnterprisesService.QueryAsync(r => r.EstablishmentKey == pOrder.EstablishmentKey &&
                    r.Id == OrderDB.EnterpriseID);

                if (Enterprise.Any())
                    RegionId = Enterprise.First().RegionId;
            }

            foreach (var item in OrderDB.VEOrderItems)
            {
                var itemDB = new VEOrderItems();

                itemDB.CopyProperties(item);

                itemDB.Id = 0;
                itemDB.IdERP = 0;
                itemDB.Created = DateTime.Now;
                itemDB.Modified = DateTime.Now;
                itemDB.IsActive = true;
                itemDB.UniqueKey = Guid.NewGuid().ToString();
                itemDB.EstablishmentKey = pOrder.EstablishmentKey;

                if (pOrder.CanChangeValue && RegionId > 0 && OrderDestiny.IsProposal)
                {
                    var SalePrice = await _IVESalePriceService.QueryAsync(r => r.EstablishmentKey == OrderDB.EstablishmentKey &&
                        r.RegionId == RegionId &&
                        r.ProductId == item.ProductID);
                    if (SalePrice.Any())
                    {
                        var Price = SalePrice.First();
                        itemDB.Value = (float)Price.Value;
                        itemDB.TableValue = Price.Value;
                        itemDB.PercDiscount = 0;
                    }
                }

                itemDB.VEOrders = null;
                itemDB.GEProducts = null;
                itemDB.FSFiscalOper = null;
                itemDB.VEOrderTaxes = null;

                OrderDestiny.VEOrderItems.Add(itemDB);
            }

            var OrderDBVM = Mapper.Map<VEOrders, VEOrdersVM>(OrderDestiny);
            OrderDBVM.UserCreatedID = OrderDestiny.DigitizerID;

            var Created = await this.CreateOrder(OrderDBVM);

            if (this.Errors.Any())
                return null;
            /*
            if (!OrderDB.IsProposal)
                OrderDB.Status = "R";
            
            await this.ChangeStatusAsync(new VEOrderStatusVM
            {
                EstablishmentKey = OrderDB.EstablishmentKey,
                OrderID = OrderDB.Id,
                Status = OrderDB.Status,
                UniqueKey = OrderDB.UniqueKey,
                RevisionReason = pOrder.RevisionReason
            });
            */
            return Created;
        }

        public async Task<VEOrdersVM> GenerateRevisionAsync(VEOrderStatusVM pOrder)
        {
            var OrderDB = await _IVEOrdersService.GetByIdAsync(pOrder.OrderID, pOrder.EstablishmentKey, pOrder.UniqueKey);

            if (OrderDB == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "VEOrders",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("NotFound") },
                    Value = pOrder.OrderID.ToString()
                });
                return null;
            }

            if (OrderDB.Status != "P")
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "VEOrders",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.StatusNotAllowChanges },
                    Value = pOrder.OrderID.ToString()
                });
                return null;
            }

            int LastOrderVersion = 0;
            var MainOrderId = OrderDB.MainOrderID ?? OrderDB.Id;

            var OrdersLinked = await GetOrdersLinkedAsync(pOrder.EstablishmentKey, MainOrderId);
            var establishment = await _IGEEstablishmentsService.GetByEstablishmentKeyAsync(pOrder.EstablishmentKey);

            LastOrderVersion = (OrdersLinked.Any() ? OrdersLinked.Max(r => r.OrderVersion) : LastOrderVersion) + 1;

            var OrderDestiny = new VEOrders();

            OrderDestiny.CopyProperties(OrderDB);

            OrderDB.RevisionReason = pOrder.RevisionReason;

            OrderDestiny.DigitizerID = pOrder.DigitizerID;
            OrderDestiny.RevisionReason = "";
            OrderDestiny.Id = 0;
            OrderDestiny.IdERP = 0;
            OrderDestiny.CodPedVenda = 0;
            OrderDestiny.OriginOrderID = OrderDB.Id;
            OrderDestiny.MainOrderID = OrderDB.MainOrderID ?? OrderDB.Id;
            OrderDestiny.OrderVersion = LastOrderVersion;
            OrderDestiny.StatusSinc = EStatusSinc.Sincronized;
            OrderDestiny.StatusCRM = "P";
            OrderDestiny.Status = "P";
            OrderDestiny.Created = DateTime.Now;
            OrderDestiny.Modified = DateTime.Now;
            OrderDestiny.PaymentDueDate = DateTime.Now.AddWorkdays(establishment.DaysPayment);
            
            OrderDestiny.IsActive = true;
            OrderDestiny.EstablishmentKey = pOrder.EstablishmentKey;
            OrderDestiny.UniqueKey = Guid.NewGuid().ToString();

            OrderDestiny.GEEnterprises = null;
            OrderDestiny.GEPaymentCondition = null;
            OrderDestiny.GEPaymentType = null;
            OrderDestiny.GERedispatch = null;
            OrderDestiny.GECarriers = null;
            OrderDestiny.GEEnterprises = null;
            OrderDestiny.GEUsers = null;
            OrderDestiny.VEOrderItems = null;
            OrderDestiny.GEUserDigitizer = null;
            OrderDestiny.MainFiscalOper = null;
            OrderDestiny.VETypeSale = null;
            OrderDestiny.VEOrderItems = new List<VEOrderItems>();

            long RegionId = 0;

            if (pOrder.CanChangeValue && OrderDestiny.IsProposal)
            {
                var Enterprise = await _IGEEnterprisesService.QueryAsync(r => r.EstablishmentKey == pOrder.EstablishmentKey &&
                    r.Id == OrderDB.EnterpriseID);

                if (Enterprise.Any())
                    RegionId = Enterprise.First().RegionId;
            }

            foreach (var item in OrderDB.VEOrderItems)
            {
                var itemDB = new VEOrderItems();

                itemDB.CopyProperties(item);

                itemDB.Id = 0;
                itemDB.IdERP = 0;
                itemDB.Created = DateTime.Now;
                itemDB.Modified = DateTime.Now;
                itemDB.IsActive = true;
                itemDB.UniqueKey = Guid.NewGuid().ToString();
                itemDB.EstablishmentKey = pOrder.EstablishmentKey;

                if (pOrder.CanChangeValue && RegionId > 0 && OrderDestiny.IsProposal)
                {
                    var SalePrice = await _IVESalePriceService.QueryAsync(r => r.EstablishmentKey == OrderDB.EstablishmentKey &&
                        r.RegionId == RegionId &&
                        r.ProductId == item.ProductID);
                    if (SalePrice.Any())
                    {
                        var Price = SalePrice.First();
                        itemDB.Value = (float)Price.Value;
                        itemDB.TableValue = Price.Value;
                        itemDB.PercDiscount = 0;
                    }
                }

                itemDB.VEOrders = null;
                itemDB.GEProducts = null;
                itemDB.FSFiscalOper = null;
                itemDB.VEOrderTaxes = null;

                OrderDestiny.VEOrderItems.Add(itemDB);
            }

            var OrderDBVM = Mapper.Map<VEOrders, VEOrdersVM>(OrderDestiny);
            OrderDBVM.UserCreatedID = OrderDestiny.DigitizerID;

            var Created = await this.CreateOrder(OrderDBVM);

            if (this.Errors.Any())
                return null;

            if (!OrderDB.IsProposal)
                OrderDB.Status = "R";

            await this.ChangeStatusAsync(new VEOrderStatusVM
            {
                EstablishmentKey = OrderDB.EstablishmentKey,
                OrderID = OrderDB.Id,
                Status = OrderDB.Status,
                UniqueKey = OrderDB.UniqueKey,
                RevisionReason = pOrder.RevisionReason
            });

            return Created;
        }

        public async Task<VEOrdersVM> UpdateOrder(VEOrdersVM pOrder)
        {
            pOrder.GEEnterprises = null;
            pOrder.GEPaymentCondition = null;
            pOrder.GEPaymentType = null;
            pOrder.VETypeSale = null;

            if (await ValidateAsync(pOrder, false))
            {
                List<VEOrderItemsVM> items = new List<VEOrderItemsVM>();
                List<VEOrderTaxesVM> taxes = new List<VEOrderTaxesVM>();
                if (pOrder.VEOrderItems != null)
                {
                    items = pOrder.VEOrderItems.ToList();
                    pOrder.VEOrderItems = null;
                }

                var OrderDB = _IVEOrdersService.GetByIdToUpdate(pOrder.Id, pOrder.EstablishmentKey, pOrder.UniqueKey);

                OrderDB.EnterpriseID = pOrder.EnterpriseID;
                OrderDB.GEEnterprises = null;

                OrderDB.CarrierID = pOrder.CarrierID;
                OrderDB.GECarriers = null;

                OrderDB.RedispatchID = pOrder.RedispatchID;
                OrderDB.GERedispatch = null;

                OrderDB.UserID = pOrder.UserID;
                OrderDB.GEUsers = null;
                OrderDB.DigitizerID = pOrder.DigitizerID;
                OrderDB.GEUserDigitizer = null;

                OrderDB.TypePaymentID = pOrder.TypePaymentID;
                OrderDB.GEPaymentType = null;

                OrderDB.PayConditionID = pOrder.PayConditionID;
                OrderDB.GEPaymentCondition = null;

                OrderDB.MainFiscalOperID = pOrder.MainFiscalOperID;
                OrderDB.MainFiscalOper = null;

                OrderDB.FinancialTaxes = pOrder.FinancialTaxes;
                OrderDB.OnlyOnDate = pOrder.OnlyOnDate;
                OrderDB.AllowPartial = pOrder.AllowPartial;
                OrderDB.RevisionReason = pOrder.RevisionReason;
                OrderDB.CodPedVenda = pOrder.CodPedVenda;
                OrderDB.NumPedMob = pOrder.NumPedMob;
                OrderDB.DeliveryDate = pOrder.DeliveryDate.Date;
                OrderDB.Note = pOrder.Note;
                OrderDB.InnerNote = pOrder.InnerNote;
                OrderDB.DetailedNote = pOrder.DetailedNote;
                OrderDB.StatusCRM = pOrder.StatusCRM;
                OrderDB.Status = pOrder.Status;
                if ((pOrder.Status != "P" && pOrder.StatusCRM == "P") || pOrder.Status == "O")
                    OrderDB.StatusCRM = OrderDB.Status;

                OrderDB.StatusSinc = _IVEOrdersService.ConvertStatusSinc(pOrder.StatusSinc);
                OrderDB.IsProposal = pOrder.IsProposal;
                OrderDB.FreightPaidBy = pOrder.FreightPaidBy;
                OrderDB.RedispatchPaidBy = pOrder.RedispatchPaidBy;
                OrderDB.FreightValue = pOrder.FreightValue;
                OrderDB.ClientOrder = pOrder.ClientOrder;
                OrderDB.IdERP = pOrder.IdERP;
                OrderDB.OriginOrderID = pOrder.OriginOrderID;
                OrderDB.OrderVersion = pOrder.OrderVersion;
                OrderDB.PercCommission = pOrder.PercCommission;
                OrderDB.PercDiscount = pOrder.PercDiscount;
                OrderDB.ContactPhone = pOrder.ContactPhone;
                OrderDB.ContactEmail = pOrder.ContactEmail;
                OrderDB.Contact = pOrder.Contact;
                OrderDB.Sector = pOrder.Sector;
                OrderDB.InPerson = pOrder.InPerson;
                OrderDB.PaymentDueDate = pOrder.PaymentDueDate;
                OrderDB.StatusPay = (EStatusPay)pOrder.StatusPay;
                OrderDB.PaidAmount = pOrder.PaidAmount;
                OrderDB.AdvanceAmount = pOrder.AdvanceAmount;
                OrderDB.TypeSaleId = pOrder.TypeSaleId;
                OrderDB.VETypeSale = null;

                if (OrderDB.IsProposal || pOrder.Converted)
                    CalculateDeliveryDate(OrderDB, items);

                if (pOrder.Converted)
                {
                    //OrderDB.Note = OrderDB.DetailedNote;
                    OrderDB.Created = DateTime.Now;

                    var establishment = await _IGEEstablishmentsService.GetByEstablishmentKeyAsync(pOrder.EstablishmentKey);
                    OrderDB.PaymentDueDate = DateTime.Now.AddWorkdays(establishment.DaysPayment);
                }

                OrderDB.VEOrderItems = null;
                var itemsDB = _IVEOrderItemsService.GetItemsByOrderId(pOrder.Id, pOrder.EstablishmentKey);

                var retOrder = _IVEOrdersService.Update(OrderDB);

                await _IVEOrderTaxesService.ClearOrderTaxesAsync(pOrder.EstablishmentKey, pOrder.Id);
                await _IVEOrderTaxesService.SaveChanges();

                if (!_IVEOrdersService.Errors.Any())
                {
                    DistributeCommission(OrderDB.PercCommission, items);
                    DistributeDiscount(OrderDB.PercDiscount, items);

                    foreach (var item in items)
                    {
                        item.OrderID = retOrder.Id;
                        item.DeliveryDate = item.DeliveryDate.Date;
                        taxes = item.VEOrderTaxes.ToList();

                        item.GEProducts = null;
                        item.FSFiscalOper = null;
                        item.VEOrderTaxes = null;

                        if (item.Id == 0)
                        {
                            var itemSaved = _IVEOrderItemsService.Add(Mapper.Map<VEOrderItemsVM, VEOrderItems>(item));
                            item.Id = itemSaved.Id;
                        }
                        else
                        {
                            var itemDB = _IVEOrderItemsService.GetByIdToUpdate(item.Id, item.EstablishmentKey, item.UniqueKey);

                            if (itemDB == null)
                            {
                                itemDB = new VEOrderItems
                                {
                                    FiscalOperID = item.FiscalOperID,
                                    TableValue = item.TableValue,
                                    Value = item.Value,
                                    Quantity = item.Quantity,
                                    QuantityReference = item.QuantityReference,
                                    PercDiscount = item.PercDiscount,
                                    Note = item.Note,
                                    Item = item.Item,
                                    ItemLevel = item.ItemLevel,
                                    IdERP = item.IdERP,
                                    ClientOrder = item.ClientOrder,
                                    ClientItem = item.ClientItem,
                                    DeliveryDate = item.DeliveryDate,
                                    PercDiscountHead = item.PercDiscountHead,
                                    PercCommissionHead = item.PercCommissionHead,
                                    PercCommission = item.PercCommission,
                                    EstablishmentKey = item.EstablishmentKey,
                                    UniqueKey = item.UniqueKey,
                                    ProductID = item.ProductID,
                                    ShippingDays = item.ShippingDays,
                                    Id = 0,
                                    OrderID = item.OrderID,
                                    FSFiscalOper = null,
                                    GEProducts = null,
                                    VEOrders = null,
                                    VEOrderTaxes = null,
                                    AltDescription = item.AltDescription,
                                    QuantityReturned = item.QuantityReturned
                                };

                                var itemSaved = _IVEOrderItemsService.Add(itemDB);
                                item.Id = itemSaved.Id;
                            }
                            else
                            {
                                itemDB.GEProducts = null;
                                itemDB.VEOrders = null;
                                itemDB.FSFiscalOper = null;
                                itemDB.VEOrderTaxes = null;
                                itemDB.FiscalOperID = item.FiscalOperID;
                                itemDB.TableValue = item.TableValue;
                                itemDB.Value = item.Value;
                                itemDB.Quantity = item.Quantity;
                                itemDB.QuantityReference = item.QuantityReference;
                                itemDB.ShippingDays = item.ShippingDays;
                                itemDB.PercDiscount = item.PercDiscount;
                                itemDB.Note = item.Note;
                                itemDB.Item = item.Item;
                                itemDB.ItemLevel = item.ItemLevel;
                                itemDB.IdERP = item.IdERP;
                                itemDB.ClientOrder = item.ClientOrder;
                                itemDB.ClientItem = item.ClientItem;
                                itemDB.DeliveryDate = item.DeliveryDate;
                                itemDB.PercDiscountHead = item.PercDiscountHead;
                                itemDB.PercCommissionHead = item.PercCommissionHead;
                                itemDB.PercCommission = item.PercCommission;
                                itemDB.AltDescription = item.AltDescription;
                                itemDB.QuantityReturned = item.QuantityReturned;

                                _IVEOrderItemsService.Update(itemDB);
                            }
                        }
                        if (_IVEOrderItemsService.Errors.Any())
                        {

                        }

                        foreach (var tax in taxes)
                        {
                            if (item.Id > 0)
                            {
                                tax.OrderItemID = item.Id;
                                _IVEOrderTaxesService.Add(Mapper.Map<VEOrderTaxesVM, VEOrderTaxes>(tax));
                            }
                        }
                    }

                    foreach (var item in itemsDB)
                    {
                        if (!items.Any(r => r.Id == item.Id))
                            await _IVEOrderItemsService.RemoveById(item.Id, item.EstablishmentKey, item.UniqueKey);
                    }
                }

                if (!_IVEOrderItemsService.Errors.Any() &&
                    !_IVEOrdersService.Errors.Any() &&
                    !_IVEOrderTaxesService.Errors.Any())
                {
                    await SaveChanges();
                    await _IVEOrderItemsService.SaveChanges();
                    await _IVEOrderTaxesService.SaveChanges();
                }

                if (_IVEOrderItemsService.Errors.Any() && !_IVEOrdersService.Errors.Any())
                    _IVEOrdersService.Errors = _IVEOrderItemsService.Errors;

                if (_IVEOrderTaxesService.Errors.Any() && !_IVEOrderItemsService.Errors.Any())
                    Errors = _IVEOrderTaxesService.Errors;

                if (!Errors.Any())
                    await _IVEOrdersService.GenerateEntryQueueAsync();

                return Mapper.Map<VEOrders, VEOrdersVM>(retOrder);
            }


            return pOrder;
        }

        public void DistributeCommission(float pPercCommission, List<VEOrderItemsVM> pItems)
        {
            if (pPercCommission > 0)
            {
                // var VlrTotal = pItems.Sum(r => r.Quantity * (r.Value - (r.Value * (r.PercDiscount / 100))));
                // var PercentItem = 0M;
                foreach (var item in pItems)
                {
                    // PercentItem = (item.Quantity * (item.Value - (item.Value * (item.PercDiscount / 100)))) / VlrTotal;

                    item.PercCommissionHead = pPercCommission; // Math.Round((pPercCommission * PercentItem), 10) * 10;
                }
            }
        }

        public void DistributeDiscount(float pPercDiscount, List<VEOrderItemsVM> pItems)
        {
            if (pPercDiscount > 0)
            {
                // var VlrTotal = pItems.Sum(r => r.Quantity * (r.Value - (r.Value * (r.PercDiscount / 100))));
                // var PercentItem = 0M;
                foreach (var item in pItems)
                {
                    // PercentItem = (item.Quantity * (item.Value - (item.Value * (item.PercDiscount / 100)))) / VlrTotal;

                    item.PercDiscountHead = pPercDiscount; // Math.Round((pPercDiscount * PercentItem), 10);
                }
            }
        }

        public async Task<VEOrdersVM> CreateOrder(VEOrdersVM pOrder)
        {
            pOrder.GEEnterprises = null;
            pOrder.GEPaymentCondition = null;
            pOrder.GEPaymentType = null;
            pOrder.GEUserDigitizer = null;
            pOrder.VETypeSale = null;

            if (await ValidateAsync(pOrder, true))
            {
                List<VEOrderItemsVM> items = new List<VEOrderItemsVM>();
                if (pOrder.VEOrderItems != null)
                {
                    items = pOrder.VEOrderItems.ToList();
                    pOrder.VEOrderItems = null;
                }

                if (!pOrder.IsProposal)
                {
                    var estab = await _IGEEstablishmentsService.GetByEstablishmentKeyAsync(pOrder.EstablishmentKey);
                    pOrder.PaymentDueDate = DateTime.Now.AddWorkdays(estab.DaysPayment);
                }
                else
                    pOrder.PaymentDueDate = DateTime.Now;

                pOrder.DeliveryDate = pOrder.DeliveryDate.Date;
                pOrder.DigitizerID = pOrder.DigitizerID <= 0 ? pOrder.UserCreatedID : pOrder.DigitizerID;

                var newOrder = Mapper.Map<VEOrdersVM, VEOrders>(pOrder);

                if (newOrder.IsProposal)
                    CalculateDeliveryDate(newOrder, items);

                var client = (await _IGEEnterprisesService.QueryAsync(r => r.EstablishmentKey == pOrder.EstablishmentKey &&
                                                                          r.Id == pOrder.EnterpriseID &&
                                                                          r.Classification == EEnterpriseClassification.Client,
                                                                      a => a.GEEnterpriseAnnotation)).FirstOrDefault();

                if (client != null && client.GEEnterpriseAnnotation?.Count() > 0)
                    newOrder.Note += String.Join(", ", client.GEEnterpriseAnnotation.Select(r => r.Annotation));

                var retOrder = _IVEOrdersService.CreateOrder(newOrder);

                DistributeCommission(retOrder.PercCommission, items);
                DistributeDiscount(retOrder.PercDiscount, items);
                foreach (var item in items)
                {
                    item.Id = 0;
                    item.OrderID = retOrder.Id;
                    item.DeliveryDate = item.DeliveryDate.Date;

                    _IVEOrderItemsService.Add(Mapper.Map<VEOrderItemsVM, VEOrderItems>(item));
                }

                if (!Errors.Any())
                {
                    await SaveChanges();
                    await _IVEOrderItemsService.SaveChanges();
                }
                else
                    _IUnitOfWork.Rollback();

                if (_IVEOrderItemsService.Errors.Any() && !_IVEOrdersService.Errors.Any())
                    _IVEOrdersService.Errors = _IVEOrderItemsService.Errors;

                if (!Errors.Any())
                    await _IVEOrdersService.GenerateEntryQueueAsync();

                return Mapper.Map<VEOrders, VEOrdersVM>(retOrder);
            }

            return pOrder;
        }

        public async override Task<VEOrders> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            var Order = await _IVEOrdersService.GetByIdAsync(id, pEstablishmentKey, pUniqueKey);

            if (Order == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "VEOrders",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("NotFound") },
                    Value = id.ToString()
                });
                return null;
            }

            if (Order.StatusSinc == EStatusSinc.Integrated)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "VEOrders",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.StatusSincNotAllowRemove },
                    Value = id.ToString()
                });
                return null;
            }

            if (Order.Status != "P")
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "VEOrders",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.OrderStatusNotAllowRemove },
                    Value = id.ToString()
                });
                return null;
            }

            var OrdersLinked = await GetOrderDirectLinkedAsync(Order.EstablishmentKey, Order.Id);
            if (OrdersLinked.Any(r => r.Id != Order.Id))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "VEOrders",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.OrderIsLinked },
                    Value = id.ToString()
                });
                return null;
            }

            using (_IUnitOfWork)
            {
                await _IVEOrderItemsService.RemoveAllItemsAsync(Order.Id, Order.EstablishmentKey);

                if (_IVEOrderItemsService.Errors.Any())
                {
                    _IUnitOfWork.Rollback();
                    return null;
                }
                await _IVEOrdersService.RemoveById(Order.Id, Order.EstablishmentKey, Order.UniqueKey);

                if (_IVEOrdersService.Errors.Any())
                {
                    _IUnitOfWork.Rollback();
                    return null;
                }

                await _IVEOrderItemsService.SaveChanges();
                await _IVEOrdersService.SaveChanges();
                if (_IVEOrderItemsService.Errors.Any() ||
                    _IVEOrdersService.Errors.Any())
                {
                    _IUnitOfWork.Rollback();
                    return null;
                }

                if (!Errors.Any())
                    await _IVEOrdersService.GenerateEntryQueueAsync();
            }

            return Order;
        }

        public async Task<IEnumerable<VEOrders>> SyncERPData(string pEstablishmentKey, VEOrders[] pRegisters)
        {
            for (int i = 0, length = pRegisters.Length; i < length; i++)
            {
                var EstablishmentKey = pRegisters[i].EstablishmentKey;
                foreach (var item in pRegisters[i].VEOrderItems)
                {
                    if (item.FiscalOperID > 0)
                    {
                        var fiscalOperID = item.FiscalOperID;
                        var FiscalOper = (await _IFSFiscalOperService.QueryAsync(r => r.EstablishmentKey == EstablishmentKey &&
                            r.IdERP == fiscalOperID)).FirstOrDefault();
                        if (FiscalOper == null && item.FSFiscalOper != null)
                        {
                            FiscalOper = new FSFiscalOper
                            {
                                Id = 0,
                                UniqueKey = Guid.NewGuid().ToString(),
                                EstablishmentKey = EstablishmentKey,
                                Description = item.FSFiscalOper.Description,
                                IsBonification = item.FSFiscalOper.IsBonification,
                                IsSample = item.FSFiscalOper.IsSample,
                                IdERP = item.FSFiscalOper.IdERP
                            };

                            _IFSFiscalOperService.Add(FiscalOper);
                            await _IFSFiscalOperService.SaveChanges();
                        }
                        item.FiscalOperID = FiscalOper.Id;
                        item.FSFiscalOper = null;
                    }
                }
            }

            await SyncData(pEstablishmentKey, pRegisters);

            return pRegisters;
        }

        public override async Task<IEnumerable<VEOrders>> SyncData(string pEstablishmentKey, VEOrders[] pRegisters)
        {
            List<ModelException> SyncErrors = new List<ModelException>();
            for (int i = 0, length = pRegisters.Length; i < length; i++)
            {
                var result = await UpdateOrder(Mapper.Map<VEOrders, VEOrdersVM>(pRegisters[i]));
                pRegisters[i] = Mapper.Map<VEOrdersVM, VEOrders>(result);
                if (Errors.Count > 0)
                {
                    pRegisters[i].Id = 0;
                    SyncErrors.Add(Errors[0]);
                }
            }
            return pRegisters;
        }

        public async Task<VEOrdersVM> ChangeStatusAsync(VEOrderStatusVM pOrder)
        {
            if (!pOrder.Status.In("A", "E", "M", "P", "R", "C", "O", "G"))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "VEOrders",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.InvalidOrderStatus },
                    Value = pOrder.OrderID.ToString()
                });
                return null;
            }

            var OrderDB = _IVEOrdersService.GetById(pOrder.OrderID, pOrder.EstablishmentKey, pOrder.UniqueKey);

            if (OrderDB == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "VEOrders",
                    Messages = new[] { Infra.Cross.Resources.ErrorMessagesResource.ResourceManager.GetString("NotFound") },
                    Value = pOrder.OrderID.ToString()
                });
                return null;
            }

            if (OrderDB.Status != "P")
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "VEOrders",
                    Messages = new[] { Infra.Cross.Resources.ValidationMessagesResource.StatusNotAllowChanges },
                    Value = pOrder.OrderID.ToString()
                });
                return null;
            }

            OrderDB.RevisionReason = pOrder.RevisionReason;
            OrderDB.StatusCRM = pOrder.Status;
            if (OrderDB.Status == "P" && pOrder.Status.In("R", "C"))
                OrderDB.Status = pOrder.Status;

            OrderDB.GEEnterprises = null;
            OrderDB.GECarriers = null;
            OrderDB.GERedispatch = null;
            OrderDB.GEEnterprises = null;
            OrderDB.GEPaymentCondition = null;
            OrderDB.GEPaymentType = null;
            OrderDB.GEUsers = null;
            OrderDB.MainFiscalOper = null;
            OrderDB.GEUserDigitizer = null;
            OrderDB.VEOrderItems = null;
            OrderDB.VETypeSale = null;
            _IVEOrdersService.Update(OrderDB);

            await SaveChanges();

            if (pOrder.Status == "A")
            {
                var OrdersLinked = await GetOrdersLinkedTreeAsync(OrderDB.EstablishmentKey, OrderDB.MainOrderID ?? OrderDB.Id);

                foreach (var item in OrdersLinked)
                {
                    if (item.Id != OrderDB.Id)
                    {
                        var orderToReprove = GetByIdToUpdate(item.Id, item.EstablishmentKey, item.UniqueKey);

                        orderToReprove.GEEnterprises = null;
                        orderToReprove.GECarriers = null;
                        orderToReprove.GERedispatch = null;
                        orderToReprove.GEEnterprises = null;
                        orderToReprove.GEPaymentCondition = null;
                        orderToReprove.GEPaymentType = null;
                        orderToReprove.GEUsers = null;
                        orderToReprove.MainFiscalOper = null;
                        orderToReprove.GEUserDigitizer = null;
                        orderToReprove.VEOrderItems = null;
                        orderToReprove.StatusCRM = "R";
                        _IVEOrdersService.Update(orderToReprove);

                        await SaveChanges();
                    }
                }
            }

            if (!Errors.Any())
                await _IVEOrdersService.GenerateEntryQueueAsync();

            return Mapper.Map<VEOrders, VEOrdersVM>(OrderDB);
        }

        public async Task<IEnumerable<VEOrders>> GetOrdersLinkedAsync(string pEstablishmentKey, long id) =>
            await _IVEOrdersService.QueryAsync(r => r.MainOrderID == id &&
                r.EstablishmentKey == pEstablishmentKey);

        public async Task<IEnumerable<VEOrders>> GetOrderDirectLinkedAsync(string pEstablishmentKey, long id) =>
            await _IVEOrdersService.QueryAsync(r => (r.MainOrderID == id || r.OriginOrderID == id) &&
                r.EstablishmentKey == pEstablishmentKey);

        public async Task<IEnumerable<VEOrders>> GetOrdersLinkedTreeAsync(string pEstablishmentKey, long id) =>
            await _IVEOrdersService.QueryAsync(r => (r.MainOrderID ?? r.Id) == id &&
                r.EstablishmentKey == pEstablishmentKey);

        public async Task<PagedResult<CommissionByProductVM>> GetCommissionByProduct(int pageNumber, int limitNumber, string pEstablishmentKey, long userId, DateTime initialDate, DateTime finalDate, String filter)
        {
            IEnumerable<VEOrderItems> Result;

            DateTime initial = initialDate.FirstHour();
            DateTime final = finalDate.LastHour();

            if (filter.IsEmpty())
            {
                Result = await _IVEOrderItemsService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                     r.VEOrders.UserID == userId &&
                                                                     !r.VEOrders.IsProposal &&
                                                                     r.VEOrders.Status == "O" &&
                                                                     r.VEOrders.Created >= initial &&
                                                                     r.VEOrders.Created <= final,
                                                                p => p.GEProducts);
            }
            else
            {
                Result = await _IVEOrderItemsService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                     r.VEOrders.UserID == userId &&
                                                                     !r.VEOrders.IsProposal &&
                                                                     r.VEOrders.Status == "O" &&
                                                                     r.VEOrders.Created >= initial &&
                                                                     r.VEOrders.Created <= final &&
                                                                     (r.GEProducts.Name.Contains(filter.Trim()) || r.GEProducts.ProductKey.Contains(filter)),
                                                               p => p.GEProducts);
            }

            var CommissionByProducts = Result.GroupBy(g => new { g.GEProducts.ProductKey, g.GEProducts.Name })
                                             .Select(r => new CommissionByProductVM()
                                             {
                                                 ProductKey = r.Key.ProductKey,
                                                 Name = r.Key.Name,
                                                 TotalCommission = Convert.ToDecimal(r.Sum(s => s.BaseCommission * (s.PercCommission / 100)))
                                             })
                                             .Where(r => r.TotalCommission > 0)
                                             .OrderByDescending(o => o.TotalCommission).AsQueryable();

            var PagedResult = new PagedResult<CommissionByProductVM>
            {
                CurrentPage = Math.Max(pageNumber, 1),
                PageCount = (int)Math.Ceiling((double)CommissionByProducts.Count() / limitNumber),
                PageSize = limitNumber,
                RowCount = CommissionByProducts.Count()
            };

            foreach (var item in CommissionByProducts.Skip((pageNumber - 1) * limitNumber).Take(limitNumber))
                PagedResult.Results.Add(item);

            return PagedResult;
        }

        public async Task<TotalCommissionVM> GetTotalCommission(string pEstablishmentKey, long userId, DateTime initialDate, DateTime finalDate)
        {
            DateTime initial = initialDate.FirstHour();
            DateTime final = finalDate.LastHour();

            var Query = await _IVEOrderItemsService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                    r.VEOrders.UserID == userId &&
                                                                    !r.VEOrders.IsProposal &&
                                                                    r.VEOrders.Status == "O" &&
                                                                    r.VEOrders.Created >= initial &&
                                                                    r.VEOrders.Created <= final);

            var Commission = Query.Sum(r => r.BaseCommission * (r.PercCommission / 100));

            var TotalCommissionResult = new TotalCommissionVM()
            {
                TotalCommission = (decimal)Commission
            };

            return TotalCommissionResult;
        }

        public async Task<PagedResult<SalesByProductVM>> GetSalesByProduct(int pageNumber, int limitNumber, string pEstablishmentKey, long userId, DateTime initialDate, DateTime finalDate, String filter)
        {
            IEnumerable<VEOrderItems> Result;

            DateTime initial = initialDate.FirstHour();
            DateTime final = finalDate.LastHour();

            if (filter.IsEmpty())
            {
                Result = await _IVEOrderItemsService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                     r.VEOrders.UserID == userId &&
                                                                     !r.VEOrders.IsProposal &&
                                                                     r.VEOrders.Status != "R" &&
                                                                     r.VEOrders.StatusCRM != "R" &&
                                                                     r.VEOrders.Created >= initial &&
                                                                     r.VEOrders.Created <= final,
                                                                p => p.GEProducts);
            }
            else
            {
                Result = await _IVEOrderItemsService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                     r.VEOrders.UserID == userId &&
                                                                     !r.VEOrders.IsProposal &&
                                                                     r.VEOrders.Status != "R" &&
                                                                     r.VEOrders.StatusCRM != "R" &&
                                                                     r.VEOrders.Created >= initial &&
                                                                     r.VEOrders.Created <= final &&
                                                                     (r.GEProducts.Name.Contains(filter.Trim()) || r.GEProducts.ProductKey.Contains(filter)),
                                                                p => p.GEProducts);
            }

            var SalesByProducts = Result.GroupBy(g => new { g.GEProducts.ProductKey, g.GEProducts.Name })
                                                     .Select(r => new SalesByProductVM()
                                                     {
                                                         ProductKey = r.Key.ProductKey,
                                                         Name = r.Key.Name,
                                                         TotalValue = (decimal)r.Sum(s => (s.Value * s.Quantity)),
                                                         TotalQuantity = (decimal)r.Sum(s => s.Quantity)
                                                     })
                                                     .Where(r => r.TotalValue > 0)
                                                     .OrderByDescending(o => o.TotalValue).AsQueryable();

            var PagedResult = new PagedResult<SalesByProductVM>
            {
                CurrentPage = Math.Max(pageNumber, 1),
                PageCount = (int)Math.Ceiling((double)SalesByProducts.Count() / limitNumber),
                PageSize = limitNumber,
                RowCount = SalesByProducts.Count()
            };

            foreach (var item in SalesByProducts.Skip((pageNumber - 1) * limitNumber).Take(limitNumber))
                PagedResult.Results.Add(item);

            return PagedResult;
        }

        public async Task<ResumePagedResult<DetailedSalesVM>> GetDetailedSales(int pageNumber, int limitNumber, string pEstablishmentKey, long userId, DateTime initialDate, DateTime finalDate, String filter)
        {
            IEnumerable<VEOrderItems> Result;

            DateTime initial = initialDate.FirstHour();
            DateTime final = finalDate.LastHour();

            if (filter.IsEmpty())
            {
                Result = await _IVEOrderItemsService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                     r.VEOrders.UserID == userId &&
                                                                     !r.VEOrders.IsProposal &&
                                                                     r.VEOrders.Status != "R" &&
                                                                     r.VEOrders.StatusCRM != "R" &&
                                                                     r.VEOrders.Created >= initial &&
                                                                     r.VEOrders.Created <= final,
                                                                p => p.GEProducts,
                                                                a => a.VEOrders,
                                                                e => e.VEOrders.GEEnterprises);
            }
            else
            {
                Result = await _IVEOrderItemsService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                     r.VEOrders.UserID == userId &&
                                                                     !r.VEOrders.IsProposal &&
                                                                     r.VEOrders.Status != "R" &&
                                                                     r.VEOrders.StatusCRM != "R" &&
                                                                     r.VEOrders.Created >= initial &&
                                                                     r.VEOrders.Created <= final &&
                                                                     (r.GEProducts.Name.Contains(filter.Trim()) || r.GEProducts.ProductKey.Contains(filter)),
                                                                p => p.GEProducts,
                                                                a => a.VEOrders,
                                                                e => e.VEOrders.GEEnterprises);
            }

            float totalValue = Result.Sum(r => r.TotalValueWithDiscount);
            float totalQuantity = Result.Sum(r => r.Quantity);

            var SalesByProducts = Result.Select(r => new DetailedSalesVM()
            {
                VEOrderItems = Mapper.Map<VEOrderItems, VEOrderItemsVM>(r),
                GEEnterprises = new GEEnterprisesVM()
                {
                    Id = r.VEOrders.GEEnterprises.Id,
                    RazaoSocial = r.VEOrders.GEEnterprises.RazaoSocial,
                    NomeFantasia = r.VEOrders.GEEnterprises.NomeFantasia,
                    CNPJCPF = r.VEOrders.GEEnterprises.CNPJCPF
                },
                Created = r.VEOrders.Created
            });

            var PagedResult = new ResumePagedResult<DetailedSalesVM>
            {
                CurrentPage = Math.Max(pageNumber, 1),
                PageCount = (int)Math.Ceiling((double)SalesByProducts.Count() / limitNumber),
                PageSize = limitNumber,
                RowCount = SalesByProducts.Count(),
                TotalValue = totalValue,
                TotalQuantity = totalQuantity
            };

            foreach (var item in SalesByProducts.Skip((pageNumber - 1) * limitNumber).Take(limitNumber))
                PagedResult.Results.Add(item);

            return PagedResult;
        }

        public async Task<ResumePagedResult<DetailedSalesVM>> GetDetailedCommission(int pageNumber, int limitNumber, string pEstablishmentKey, long userId, DateTime initialDate, DateTime finalDate, String filter)
        {
            IEnumerable<VEOrderItems> Result;

            DateTime initial = initialDate.FirstHour();
            DateTime final = finalDate.LastHour();

            if (filter.IsEmpty())
            {
                Result = await _IVEOrderItemsService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                     r.VEOrders.UserID == userId &&
                                                                     !r.VEOrders.IsProposal &&
                                                                     r.VEOrders.Status == "O" &&
                                                                     r.VEOrders.Created >= initial &&
                                                                     r.VEOrders.Created <= final,
                                                                p => p.GEProducts,
                                                                a => a.VEOrders,
                                                                e => e.VEOrders.GEEnterprises);
            }
            else
            {
                Result = await _IVEOrderItemsService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                     r.VEOrders.UserID == userId &&
                                                                     !r.VEOrders.IsProposal &&
                                                                     r.VEOrders.Status == "O" &&
                                                                     r.VEOrders.Created >= initial &&
                                                                     r.VEOrders.Created <= final &&
                                                                     (r.GEProducts.Name.Contains(filter.Trim()) || r.GEProducts.ProductKey.Contains(filter)),
                                                                p => p.GEProducts,
                                                                a => a.VEOrders,
                                                                e => e.VEOrders.GEEnterprises);
            }

            float totalValue = (float)Math.Round(Result.Sum(r => r.BaseCommission * (r.PercCommission / 100)), 2);
            float totalQuantity = Result.Sum(r => r.Quantity - r.QuantityReturned);

            var SalesByProducts = Result.Select(r => new DetailedSalesVM()
            {
                VEOrderItems = Mapper.Map<VEOrderItems, VEOrderItemsVM>(r),
                GEEnterprises = new GEEnterprisesVM()
                {
                    Id = r.VEOrders.GEEnterprises.Id,
                    RazaoSocial = r.VEOrders.GEEnterprises.RazaoSocial,
                    NomeFantasia = r.VEOrders.GEEnterprises.NomeFantasia,
                    CNPJCPF = r.VEOrders.GEEnterprises.CNPJCPF
                },
                Created = r.VEOrders.Created
            });

            var PagedResult = new ResumePagedResult<DetailedSalesVM>
            {
                CurrentPage = Math.Max(pageNumber, 1),
                PageCount = (int)Math.Ceiling((double)SalesByProducts.Count() / limitNumber),
                PageSize = limitNumber,
                RowCount = SalesByProducts.Count(),
                TotalValue = totalValue,
                TotalQuantity = totalQuantity
            };

            foreach (var item in SalesByProducts.Skip((pageNumber - 1) * limitNumber).Take(limitNumber))
                PagedResult.Results.Add(item);

            return PagedResult;
        }

        public async Task<ResumePagedResult<VEOrdersVM>> GetSalesLimit(int pageNumber, int limitNumber, string pEstablishmentKey, long pUserId, String filter)
        {
            IEnumerable<VEOrders> Result;

            if (filter.IsEmpty())
            {
                Result = await _IVEOrdersService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                 r.UserID == pUserId &&
                                                                 r.GEPaymentType.Boleto &&
                                                                 r.Status != "R" &&
                                                                 r.StatusCRM != "R" &&
                                                                 !r.IsProposal &&
                                                                 r.StatusPay != EStatusPay.Total,
                                                            e => e.GEEnterprises);
            }
            else
            {
                Result = await _IVEOrdersService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                 r.UserID == pUserId &&
                                                                 r.GEPaymentType.Boleto &&
                                                                 r.Status != "R" &&
                                                                 r.StatusCRM != "R" &&
                                                                 r.StatusPay != EStatusPay.Total &&
                                                                 !r.IsProposal &&
                                                                 r.GEEnterprises.RazaoSocial.Contains(filter.Trim()),
                                                            e => e.GEEnterprises);
            }

            var user = (await _IGEUsersService.QueryAsync(r => r.Id == pUserId)).FirstOrDefault();
            float limitUsed = Result.Sum(r => r.TotalValue - r.PaidAmount);
            float userLimit = user?.BoletoLimit ?? 0;

            var PagedResult = new ResumePagedResult<VEOrdersVM>
            {
                CurrentPage = Math.Max(pageNumber, 1),
                PageCount = (int)Math.Ceiling((double)Result.Count() / limitNumber),
                PageSize = limitNumber,
                RowCount = Result.Count(),
                UserLimit = userLimit,
                LimitUsed = limitUsed
            };

            foreach (var item in Result.Skip((pageNumber - 1) * limitNumber).Take(limitNumber))
                PagedResult.Results.Add(Mapper.Map<VEOrders, VEOrdersVM>(item));

            return PagedResult;
        }
        public async Task<PagedResult<SalesByMonthVM>> GetSalesByMonth(int pageNumber, int limitNumber, string pEstablishmentKey, 
            long userId, DateTime initialDate, DateTime finalDate, String filter)
        {
            IEnumerable<VEOrderItems> Result;

            DateTime initial = initialDate.FirstHour();
            DateTime final = finalDate.LastHour();

            if (filter.IsEmpty())
            {
                Result = await _IVEOrderItemsService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                     r.VEOrders.UserID == userId &&
                                                                     !r.VEOrders.IsProposal &&
                                                                     r.VEOrders.Created >= initial &&
                                                                     r.VEOrders.Created <= final,
                                                                o => o.VEOrders);
            }
            else
            {
                Result = await _IVEOrderItemsService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                     r.VEOrders.UserID == userId &&
                                                                     !r.VEOrders.IsProposal &&
                                                                     r.VEOrders.Created >= initial &&
                                                                     r.VEOrders.Created <= final &&
                                                                     (r.GEProducts.Name.Contains(filter.Trim()) || r.GEProducts.ProductKey.Contains(filter)),
                                                                o => o.VEOrders);
            }

            var SalesByProducts = Result.GroupBy(g => new { date = new DateTime(g.VEOrders.Created.Year, g.VEOrders.Created.Month, 1) })
                                                     .Select(r => new SalesByMonthVM()
                                                     {
                                                         Date = r.Key.date,
                                                         TotalValue = (decimal)r.Sum(s => (s.Value * s.Quantity)),
                                                         TotalQuantity = (decimal)r.Sum(s => s.Quantity)
                                                     })
                                                     .Where(r => r.TotalValue > 0)
                                                     .OrderBy(o => o.Date).AsQueryable();

            var PagedResult = new PagedResult<SalesByMonthVM>
            {
                CurrentPage = Math.Max(pageNumber, 1),
                PageCount = (int)Math.Ceiling((double)Result.Count() / limitNumber),
                PageSize = limitNumber,
                RowCount = SalesByProducts.Count()
            };

            foreach (var item in SalesByProducts.Skip((pageNumber - 1) * limitNumber).Take(limitNumber))
                PagedResult.Results.Add(item);

            return PagedResult;
        }

        public async Task<TotalSalesVM> GetTotalSales(string pEstablishmentKey, long userId, DateTime initialDate, DateTime finalDate)
        {
            DateTime initial = initialDate.FirstHour();
            DateTime final = finalDate.LastHour();

            var Query = await _IVEOrderItemsService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                    r.VEOrders.UserID == userId &&
                                                                    !r.VEOrders.IsProposal &&
                                                                    r.VEOrders.Created >= initial &&
                                                                    r.VEOrders.Created <= final);

            var TotalValue = Query.Sum(r => r.TotalValue);
            var TotalQuantity = Query.Count();

            var TotalSales = new TotalSalesVM()
            {
                TotalQuantity = TotalQuantity,
                TotalValue = (decimal)TotalValue
            };

            return TotalSales;
        }

        public async Task<IEnumerable<VEOrderStockBalanceVM>> GetOrderStockBalanceAsync(string pEstablishmentKey, string pUniqueKey, long id)
        {
            var lstOrderBalance = new List<VEOrderStockBalanceVM>();
            var Order = await _IVEOrdersService.GetByIdAsync(id, pEstablishmentKey, pUniqueKey);

            var ItemsUnique = Order.VEOrderItems
                .Select(s => new
                {
                    s.ProductID,
                    s.GEProducts
                }).Distinct()
                  .Select(s => new
                {
                    s.ProductID,
                    s.GEProducts,
                    Quantity = Order.VEOrderItems.Where(r => r.ProductID == s.ProductID).Sum(t => t.Quantity)
                });

            foreach (var item in ItemsUnique)
            {
                var ItemBalance = new VEOrderStockBalanceVM
                {
                    ProductID = item.ProductID,
                    GEProducts = item.GEProducts,
                    Quantity = (decimal)item.Quantity,
                    QtdBudgeted = 0,
                    QtdOrders = 0,
                    StockBalance = item.GEProducts.StockBalance
                };

                var Qtd = await _IVEOrdersService.QueryAsync(r => 
                    r.EstablishmentKey == pEstablishmentKey &&
                    r.VEOrderItems.Any(o => o.EstablishmentKey == r.EstablishmentKey &&
                    o.ProductID == item.ProductID) &&
                    r.Id != id &&
                    ((r.Status == "P" ||
                      r.Status == "A" ||
                      r.Status == "M") || 
                     (r.StatusCRM == "P" ||
                      r.StatusCRM == "A" ||
                      r.StatusCRM == "M"
                    ))
                );

                ItemBalance.QtdBudgeted = (decimal)Qtd.Where(r => r.IsProposal)
                    .Select(s => s.VEOrderItems.Where(g => g.EstablishmentKey == pEstablishmentKey &&
                                                           g.ProductID == item.ProductID).Sum(t => t.Quantity)).Sum();
                ItemBalance.QtdOrders = (decimal)Qtd.Where(r => !r.IsProposal)
                    .Select(s => s.VEOrderItems.Where(g => g.EstablishmentKey == pEstablishmentKey &&
                                                           g.ProductID == item.ProductID).Sum(t => t.Quantity)).Sum();

                lstOrderBalance.Add(ItemBalance);
            }

            return lstOrderBalance;
        }

        public async Task<PagedResult<VEOrders>> QueryViewAsync(int page, int pageSize, Expression<Func<VEOrders, bool>> predicate) =>
            await _IVEOrdersService.QueryViewAsync(page, pageSize, predicate);
    }
}
