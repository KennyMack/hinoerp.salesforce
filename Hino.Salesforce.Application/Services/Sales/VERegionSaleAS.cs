﻿using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Sales
{
    public class VERegionSaleAS : BaseAppService<VERegionSale>, IVERegionSaleAS
    {
        private readonly IVERegionSaleService _IVERegionSaleService;
        public VERegionSaleAS(IVERegionSaleService pIVERegionSaleService) : base(pIVERegionSaleService)
        {
            _IVERegionSaleService = pIVERegionSaleService;
        }
    }
}
