using AutoMapper;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Sales
{
    public class VERegionSaleUFAS : BaseAppService<VERegionSaleUF>, IVERegionSaleUFAS
    {
        private readonly IVERegionSaleUFService _IVERegionSaleUFService;
        private readonly IGEEnterprisesService _IGEEnterprisesService;

        public VERegionSaleUFAS(IVERegionSaleUFService pIVERegionSaleUFService,
            IGEEnterprisesService pIGEEnterprisesService) :
             base(pIVERegionSaleUFService)
        {
            _IGEEnterprisesService = pIGEEnterprisesService;
            _IVERegionSaleUFService = pIVERegionSaleUFService;
        }

        public async Task<bool> ExistsUFRegion(string pEstablishmentKey, string pUF)
        {
            var Result = await _IVERegionSaleUFService.QueryAsync(r =>
                r.EstablishmentKey == pEstablishmentKey &&
                r.UF == pUF);
            return Result.Any();
        }

        public async Task UpdateEnterpriseRegionId(string pEstablishmentKey, string pUF, long pRegionId) =>
            await _IGEEnterprisesService.UpdateEnterpriseRegionId(pEstablishmentKey, pUF, pRegionId);

        public async Task<VERegionSaleUFVM> CreateSalePrice(VERegionSaleUFVM pRegionSale)
        {
            if (await ExistsUFRegion(
                pRegionSale.EstablishmentKey,
                pRegionSale.UF))
            {
                Errors.Add(new Infra.Cross.Utils.Exceptions.ModelException
                {
                    ErrorCode = (int)Infra.Cross.Utils.Exceptions.EExceptionErrorCodes.InvalidRequest,
                    Field = "UF",
                    Value = pRegionSale.UF.ToString(),
                    Messages = new string[]
                    {
                        Infra.Cross.Resources.ValidationMessagesResource.UFRegionExists
                    }
                });
                return null;
            }

            var Region = Mapper.Map<VERegionSaleUF, VERegionSaleUFVM>(Add(Mapper.Map<VERegionSaleUFVM, VERegionSaleUF>(pRegionSale)));

            if (!Errors.Any())
                await SaveChanges();

            if (!Errors.Any())
                await UpdateEnterpriseRegionId(pRegionSale.EstablishmentKey, pRegionSale.UF, pRegionSale.RegionId);

            if (Errors.Any())
                return null;

            return Region;
        }

        public async Task<VERegionSaleUFVM> UpdateSalePrice(VERegionSaleUFVM pRegionSale)
        {
            var Region = Mapper.Map<VERegionSaleUF, VERegionSaleUFVM>(Update(Mapper.Map<VERegionSaleUFVM, VERegionSaleUF>(pRegionSale)));

            if (!Errors.Any())
                await SaveChanges();

            if (!Errors.Any())
                await UpdateEnterpriseRegionId(pRegionSale.EstablishmentKey, pRegionSale.UF, pRegionSale.RegionId);

            if (Errors.Any())
                return null;

            return Region;
        }
    }
}
