﻿using AutoMapper;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Sales
{
    public class VESalePriceAS : BaseAppService<VESalePrice>, IVESalePriceAS
    {
        private readonly IVESalePriceService _IVESalePriceService;
        private readonly IGEPaymentConditionService _IGEPaymentConditionService;
        private readonly IGEUsersService _IGEUsersService;
        private readonly IGEEstablishmentsService _IGEEstablishmentsService;

        public VESalePriceAS(IVESalePriceService pIVESalePriceService, 
            IGEPaymentConditionService pIGEPaymentConditionService, 
            IGEUsersService pIGEUsersService,
            IGEEstablishmentsService pGEEstablishmentsService) : base(pIVESalePriceService)
        {
            _IVESalePriceService = pIVESalePriceService;
            _IGEPaymentConditionService = pIGEPaymentConditionService;
            _IGEUsersService = pIGEUsersService;
            _IGEEstablishmentsService = pGEEstablishmentsService;
        }

        public async Task<VESalePriceVM> CreateSalePrice(VESalePriceVM pSalePrice)
        {
            var salePrice = Mapper.Map<VESalePrice>(pSalePrice);

            return Mapper.Map<VESalePrice, VESalePriceVM>(
                await _IVESalePriceService.CreateSalePrice(salePrice));
        }

        public async Task<VESalePriceVM> UpdateSalePrice(VESalePriceVM pSalePrice)
        {
            var salePrice = Mapper.Map<VESalePrice>(pSalePrice);

            return Mapper.Map<VESalePrice, VESalePriceVM>(
                await _IVESalePriceService.UpdateSalePrice(salePrice));
        }

        public async Task<PagedResult<VESalePriceVM>> GetAllSalePrice(
            int pageNumber, int limit, string pEstablishmentKey, 
            long pRegionId, long pCodPrVenda, long pPaymentConditionId, 
            long pUserId, string filter, string searchPosition)
        {
            var establishment = await _IGEEstablishmentsService.GetByEstablishmentKeyAsync(pEstablishmentKey);

            PagedResult<VESalePrice> salesPrices;

            if (establishment.OnlyWithStock)
                salesPrices = await _IVESalePriceService.GetAllPagedSearchProductWithStock(
                    pageNumber, limit, pEstablishmentKey, 
                    pRegionId, pCodPrVenda, 
                    filter, searchPosition);
            else
                salesPrices = await _IVESalePriceService.GetAllPagedSearchProduct(
                    pageNumber, limit, pEstablishmentKey, 
                    pRegionId, pCodPrVenda, 
                    filter, searchPosition);

            if (salesPrices != null)
            {
                var user = (await _IGEUsersService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                    r.Id == pUserId)).FirstOrDefault();

                if (user.PercDiscountPrice > 0)
                {
                    decimal percDiscountPrice = user.PercDiscountPrice / 100;

                    foreach (var salePrice in salesPrices.Results)
                        salePrice.Value -= Math.Round((salePrice.Value * percDiscountPrice), 2);
                }
                else if (user.PercIncreasePrice > 0)
                {
                    decimal percIncreasePrice = user.PercIncreasePrice / 100;

                    foreach (var salePrice in salesPrices.Results)
                        salePrice.Value += Math.Round((salePrice.Value * percIncreasePrice), 2);
                }

                var paymentCondition = (await _IGEPaymentConditionService.QueryAsync(r => r.Id == pPaymentConditionId &&
                                                                                            r.EstablishmentKey == pEstablishmentKey)).FirstOrDefault();

                decimal conditionDiscount = Math.Max(paymentCondition?.PercDiscount ?? 0, 0) / 100;
                decimal conditionIncrease = Math.Max(paymentCondition?.PercIncrease ?? 0, 0) / 100;

                if (conditionDiscount > 0)
                {
                    foreach (var salePrice in salesPrices.Results)
                        salePrice.Value -= (salePrice.Value * conditionDiscount);
                }
                else if (conditionIncrease > 0)
                {
                    foreach (var salePrice in salesPrices.Results)
                        salePrice.Value += (salePrice.Value * conditionIncrease);
                }
            }

            return Mapper.Map<PagedResult<VESalePrice>, PagedResult<VESalePriceVM>>(salesPrices);
        }

        public async Task<PagedResult<VESalePriceVM>> GetProductSalePrice(
            string pEstablishmentKey, long pProdutctId, 
            long pRegionId, long pCodPrVenda, long pPaymentConditionId, 
            long pUserId)
        {
            var salesPrices = (await QueryPagedAsync(1, 100,
                                                    r => r.EstablishmentKey == pEstablishmentKey &&
                                                    r.RegionId == pRegionId &&
                                                    r.ProductId == pProdutctId,
                                                    s => s.VERegionSale));

            if (salesPrices != null)
            {
                var user = (await _IGEUsersService.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                                                                   r.Id == pUserId)).FirstOrDefault();

                if (user.PercDiscountPrice > 0)
                {
                    decimal percDiscountPrice = user.PercDiscountPrice / 100;

                    foreach (var salePrice in salesPrices.Results)
                        salePrice.Value -= (salePrice.Value * percDiscountPrice);
                }
                else if (user.PercIncreasePrice > 0)
                {
                    decimal percIncreasePrice = user.PercIncreasePrice / 100;

                    foreach (var salePrice in salesPrices.Results)
                        salePrice.Value += (salePrice.Value * percIncreasePrice);
                }

                var paymentCondition = (await _IGEPaymentConditionService.QueryAsync(r => r.Id == pPaymentConditionId &&
                                                                                          r.EstablishmentKey == pEstablishmentKey)).FirstOrDefault();

                decimal conditionDiscount = Math.Max(paymentCondition?.PercDiscount ?? 0, 0) / 100;
                decimal conditionIncrease = Math.Max(paymentCondition?.PercIncrease ?? 0, 0) / 100;

                if (conditionDiscount > 0)
                {
                    foreach (var salePrice in salesPrices.Results)
                        salePrice.Value -= Math.Round((salePrice.Value * conditionDiscount), 2);
                }
                else if (conditionIncrease > 0)
                {
                    foreach (var salePrice in salesPrices.Results)
                        salePrice.Value += Math.Round((salePrice.Value * conditionIncrease), 2);
                }
            }

            return Mapper.Map<PagedResult<VESalePrice>, PagedResult<VESalePriceVM>>(salesPrices);
        }

        public override async Task<IEnumerable<VESalePrice>> SyncData(string pEstablishmentKey, VESalePrice[] pRegisters)
        {
            List<ModelException> SyncErrors = new List<ModelException>();

            for (int i = 0, length = pRegisters.Length; i < length; i++)
            {
                VESalePriceVM result;
                long pProductId = pRegisters[i].ProductId;
                long pRegionId = pRegisters[i].RegionId;

                var OldProductRegion = await _IVESalePriceService.QueryAsync(r =>
                    r.EstablishmentKey == pEstablishmentKey &&
                    r.ProductId == pProductId &&
                    r.RegionId == pRegionId);

                if (OldProductRegion.Any())
                {
                    var Prod = OldProductRegion.First();
                    pRegisters[i].Id = Prod.Id;
                    pRegisters[i].UniqueKey = Prod.UniqueKey;
                    pRegisters[i].Created = Prod.Created;
                }

                if (pRegisters[i].Id <= 0)
                    result = await CreateSalePrice(Mapper.Map<VESalePrice, VESalePriceVM>(pRegisters[i]));
                else
                    result = await UpdateSalePrice(Mapper.Map<VESalePrice, VESalePriceVM>(pRegisters[i]));

                if (Errors.Any())
                {
                    pRegisters[i].Id = 0;
                    SyncErrors.Add(Errors[0]);
                }
                else
                    pRegisters[i] = Mapper.Map<VESalePriceVM, VESalePrice>(result);
            }

            if (!Errors.Any())
                await SaveChanges();

            return pRegisters;
        }


        public async Task<PagedResult<VESalePrice>> GetListCodPRVendaAsync(string pEstablishmentKey)
        {
            var Table = await _IVESalePriceService.GetListCodPRVendaAsync(pEstablishmentKey);

            var retorno = new PagedResult<VESalePrice>
            {
                CurrentPage = 1,
                PageCount=1,
                PageSize = Table.Count(),
                RowCount = Table.Count(),
                Results = new List<VESalePrice>(
                    Table
                )
            };
            return retorno;
        }
            

        public Task GenerateEntryQueueAsync() =>
            _IVESalePriceService.GenerateEntryQueueAsync();
    }
}
