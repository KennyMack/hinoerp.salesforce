using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;

namespace Hino.Salesforce.Application.Services.Sales
{
    public class VESaleWorkRegionAS : BaseAppService<VESaleWorkRegion>, IVESaleWorkRegionAS
    {
        private readonly IVESaleWorkRegionService _IVESaleWorkRegionService;

        public VESaleWorkRegionAS(IVESaleWorkRegionService pIVESaleWorkRegionService) :
             base(pIVESaleWorkRegionService)
        {
            _IVESaleWorkRegionService = pIVESaleWorkRegionService;
        }
    }
}
