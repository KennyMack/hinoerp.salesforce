using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;

namespace Hino.Salesforce.Application.Services.Sales
{
    public class VETypeSaleAS : BaseAppService<VETypeSale>, IVETypeSaleAS
    {
        private readonly IVETypeSaleService _IVETypeSaleService;

        public VETypeSaleAS(IVETypeSaleService pIVETypeSaleService) : 
             base(pIVETypeSaleService)
        {
            _IVETypeSaleService = pIVETypeSaleService;
        }
    }
}
