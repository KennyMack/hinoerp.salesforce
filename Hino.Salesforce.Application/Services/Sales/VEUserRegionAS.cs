using AutoMapper;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.Services.Sales
{
    public class VEUserRegionAS : BaseAppService<VEUserRegion>, IVEUserRegionAS
    {
        private readonly IVEUserRegionService _IVEUserRegionService;

        public VEUserRegionAS(IVEUserRegionService pIVEUserRegionService) :
             base(pIVEUserRegionService)
        {
            _IVEUserRegionService = pIVEUserRegionService;
        }

        public bool ZipCodeExists(string pEstablishmentKey, long pUserId, string pZipCode) =>
            _IVEUserRegionService.ZipCodeExists(pEstablishmentKey, pUserId, pZipCode);

        public async Task<VEUserRegion> CreateOrUpdateAsync(VEUserRegionVM userRegion)
        {
            var ent = await this.QueryAsync(r =>
                r.EstablishmentKey == userRegion.EstablishmentKey &&
                r.UserId == userRegion.UserId &&
                r.SaleWorkId == userRegion.SaleWorkId);

            if (!ent.Any())
            {
                var entNew = new VEUserRegion
                {
                    EstablishmentKey = userRegion.EstablishmentKey,
                    UserId = userRegion.UserId,
                    SaleWorkId = userRegion.SaleWorkId
                };

                this.Add(entNew);

                await SaveChanges();

                return entNew;
            }

            return Mapper.Map<VEUserRegionVM, VEUserRegion>(userRegion);
        }

        public async Task<VEUserRegion[]> CreateOrUpdateListAsync(string pEstablishmentKey, VEUserRegionVM[] userRegion)
        {
            for (int i = 0, length = userRegion.Length; i < length; i++)
            {
                if (userRegion[i].Id < 0)
                    userRegion[i].Id = 0;

                userRegion[i].EstablishmentKey = pEstablishmentKey;
                userRegion[i].VESaleWorkRegion = null;
                userRegion[i].GEUsers = null;

                userRegion[i] = Mapper.Map<VEUserRegion, VEUserRegionVM>(await CreateOrUpdateAsync(userRegion[i]));
            }

            return Mapper.Map<VEUserRegionVM[], VEUserRegion[]>(userRegion);
        }

        public async Task<VEUserRegion[]> RemoveListAsync(string pEstablishmentKey, VEUserRegionVM[] userRegion)
        {
            for (int i = 0, length = userRegion.Length; i < length; i++)
            {
                if (userRegion[i].Id < 0)
                    userRegion[i].Id = 0;

                userRegion[i] = Mapper.Map<VEUserRegion, VEUserRegionVM>(await RemoveById(
                    userRegion[i].Id,
                    pEstablishmentKey,
                    userRegion[i].UniqueKey
                    ));
            }

            return Mapper.Map<VEUserRegionVM[], VEUserRegion[]>(userRegion);
        }
    }
}
