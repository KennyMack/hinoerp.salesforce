﻿using RestSharp;
using System;

namespace Hino.Salesforce.Application.Utils.CEP
{
    public class Request : RequestAS
    {
        const string BaseRoute = "http://viacep.com.br/ws/";
        // const string BaseRoute = "https://cep.awesomeapi.com.br/json/";

        public override RestClient CreateClient() =>
            new RestClient(BaseRoute) { UserAgent = "PostmanRuntime/7.29.0" };

        public override RestClient CreateClientFind() =>
            throw new NotImplementedException();

        public override RestRequest CreateRequest(string pUri, Method pMethod) =>
            new RestRequest(pUri, pMethod);
    }
}
