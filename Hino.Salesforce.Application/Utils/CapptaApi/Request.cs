﻿using RestSharp;
using System;

namespace Hino.Salesforce.Application.Utils.CapptaApi
{
    public class Request : RequestAS
    {
        // Homologação
        const string BaseRoute = "https://transactions-homolog.cappta.com.br/api/v2/";
        const String AuthRoute = "https://heimdall-homolog.cappta.com.br/connect/token";
        //Produção
        //const string BaseRoute = "https://transactions.cappta.com.br/api/v2/transaction";
        //const String AuthRoute = ""

        public override RestClient CreateClient() =>
            new RestClient(BaseRoute);

        public RestClient CreateAuthClient() =>
            new RestClient(AuthRoute);

        public override RestClient CreateClientFind() =>
            throw new NotImplementedException();

        public override RestRequest CreateRequest(string pUri, Method pMethod) =>
            new RestRequest(pUri, pMethod);
    }
}
