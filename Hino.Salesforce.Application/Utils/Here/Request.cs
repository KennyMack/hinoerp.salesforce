﻿using RestSharp;

namespace Hino.Salesforce.Application.Utils.Here
{
    public class Request : RequestAS
    {
        const string BaseRoute = "https://geocoder.ls.hereapi.com/6.2/";
        const string BaseRouteFindSequence = "https://wse.ls.hereapi.com/2/";

        public override RestClient CreateClient() =>
            new RestClient(BaseRoute);

        public override RestClient CreateClientFind() =>
            new RestClient(BaseRouteFindSequence);

        public override RestRequest CreateRequest(string pUri, Method pMethod) =>
            new RestRequest(pUri, pMethod);
    }
}
