﻿using RestSharp;
using System;

namespace Hino.Salesforce.Application.Utils.ReceitaWS
{
    public class Request : RequestAS
    {
        const string BaseRoute = "https://www.receitaws.com.br/v1/cnpj/";

        public override RestClient CreateClient() =>
            new RestClient(BaseRoute);

        public override RestClient CreateClientFind() =>
            throw new NotImplementedException();

        public override RestRequest CreateRequest(string pUri, Method pMethod) =>
            new RestRequest(pUri, pMethod);
    }
}
