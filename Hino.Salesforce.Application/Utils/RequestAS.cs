﻿using Hino.Salesforce.Application.Interfaces.Utils;
using RestSharp;

namespace Hino.Salesforce.Application.Utils
{
    public abstract class RequestAS : IRequestAS
    {
        public abstract RestClient CreateClient();
        public abstract RestClient CreateClientFind();
        public abstract RestRequest CreateRequest(string pUri, Method pMethod);
    }
}
