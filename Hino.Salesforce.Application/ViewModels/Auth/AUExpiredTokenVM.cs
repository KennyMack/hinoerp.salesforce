﻿using Hino.Salesforce.Infra.Cross.Entities.Auth;
using System;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.Auth
{
    public class AUExpiredTokenVM : BaseVM
    {
        public string TokenValue { get; set; }

        public static Expression<Func<AUExpiredToken, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
