﻿using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.Auth;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.Auth
{
    public class AURefreshTokenVM : BaseVM
    {
        [ForeignKey("GEUsers")]
        public int UserID { get; set; }
        public virtual GEUsersVM GEUsers { get; set; }

        public string UserKey { get; set; }
        public string SessionID { get; set; }
        public string RefreshTokenID { get; set; }
        public string ProtectedTicket { get; set; }

        public static Expression<Func<AURefreshToken, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
