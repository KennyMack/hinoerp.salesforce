﻿using System;

namespace Hino.Salesforce.Application.ViewModels.Auth
{
    public class AuthResultModel
    {
        public string error { get; set; }
        public string error_description { get; set; }
        public string access_token { get; set; }
        public string token_type { get; set; }
        public string establishmentkey { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
        public bool success { get; set; }
        public DateTime createdat { get; set; }
        public DateTime expireat { get; set; }


    }
}
