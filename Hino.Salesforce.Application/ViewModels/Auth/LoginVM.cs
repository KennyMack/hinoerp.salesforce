﻿using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System.ComponentModel.DataAnnotations;

namespace Hino.Salesforce.Application.ViewModels.Auth
{
    [EndPoint("Auth/Mordor/login")]
    public class LoginVM : BaseVM
    {
        [Max40LengthField]
        [Min8LengthField]
        [DisplayField]
        public string UserName { get; set; }

        [Max40LengthField]
        [Min8LengthField]
        [DisplayField]
        public string Email { get; set; }

        [RequiredField]
        [DataType(DataType.Password)]
        [Min8LengthField]
        [DisplayField]
        public string Password { get; set; }

        [RequiredField]
        [DisplayField]
        public string OriginLogin { get; set; }

        public string RefreshToken { get; set; }
    }
}
