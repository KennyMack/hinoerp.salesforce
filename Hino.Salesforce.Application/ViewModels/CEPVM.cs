﻿using Hino.Salesforce.Application.ViewModels.General.Demograph;
using Hino.Salesforce.Application.ViewModels.Route;
using Newtonsoft.Json;

namespace Hino.Salesforce.Application.ViewModels
{
    public class CEPVM
    {
        public string cep { get; set; }
        public string logradouro { get; set; }
        public string complemento { get; set; }
        public string bairro { get; set; }
        public string localidade { get; set; }
        public string uf { get; set; }
        public string unidade { get; set; }
        public string ibge { get; set; }
        public string gia { get; set; }

        public GECEPAddress CEPAddress
        {
            get
            {
                if (HereAddress != null)
                    return new GECEPAddress
                    {
                        Label = HereAddress.Address.Label,
                        Country = this.Country != null ? this.Country.CIOC : HereAddress.Address.Country,
                        StateName = HereAddress.Address.StateName ?? uf,
                        State = HereAddress.Address.State ?? uf,
                        City = HereAddress.Address.City ?? localidade,
                        District = HereAddress.Address.District ?? bairro,
                        Street = HereAddress.Address.Street ?? logradouro,
                        HouseNumber = HereAddress.Address.HouseNumber,
                        PostalCode = HereAddress.Address.PostalCode ?? cep,
                        Complement = complemento
                    };

                return new GECEPAddress
                {
                    Label = $"{logradouro}, {bairro}, {localidade} - {uf}, {cep}, Brasil",
                    Country = "BRA",
                    StateName = uf,
                    State = uf,
                    City = localidade,
                    District = bairro,
                    Street = logradouro,
                    HouseNumber = "",
                    PostalCode = cep,
                    Complement = complemento
                };
            }
        }

        public GEStatesVM GEStates
        {
            get
            {
                if (HereAddress != null)
                    return new GEStatesVM
                    {
                        Initials = HereAddress.Address.State.ToUpper(),
                        Name = HereAddress.Address.StateName
                    };

                return new GEStatesVM
                {
                    Name = null,
                    Initials = (uf ?? "").ToUpper()
                };
            }
        }
        public GECitiesVM GECities
        {
            get
            {
                if (HereAddress != null)
                    return new GECitiesVM
                    {
                        Name = HereAddress.Address.City,
                        IBGE = ibge
                    };

                return new GECitiesVM
                {
                    IBGE = ibge,
                    Name = localidade
                };
            }
        }

        public GECountriesVM Country { get; set; }
        public MapBoxAddressVM GeoAddress { get; set; }
        public HereLocationVM HereAddress { get; set; }
    }


    public class CEPDeserializeVM
    {
        [JsonProperty("cep")]
        public string cep { get; set; }
        [JsonProperty("address")]
        public string logradouro { get; set; }
        public string complemento { get; set; }
        [JsonProperty("district")]
        public string bairro { get; set; }
        [JsonProperty("city")]
        public string localidade { get; set; }
        [JsonProperty("state")]
        public string uf { get; set; }
        public string unidade { get; set; }
        [JsonProperty("city_ibge")]
        public string ibge { get; set; }
        public string gia { get; set; }

        public GECEPAddress CEPAddress
        {
            get
            {
                if (HereAddress != null)
                    return new GECEPAddress
                    {
                        Label = HereAddress.Address.Label,
                        Country = this.Country != null ? this.Country.CIOC : HereAddress.Address.Country,
                        StateName = HereAddress.Address.StateName ?? uf,
                        State = HereAddress.Address.State ?? uf,
                        City = HereAddress.Address.City ?? localidade,
                        District = HereAddress.Address.District ?? bairro,
                        Street = HereAddress.Address.Street ?? logradouro,
                        HouseNumber = HereAddress.Address.HouseNumber,
                        PostalCode = HereAddress.Address.PostalCode ?? cep,
                        Complement = complemento
                    };

                return new GECEPAddress
                {
                    Label = $"{logradouro}, {bairro}, {localidade} - {uf}, {cep}, Brasil",
                    Country = "BRA",
                    StateName = uf,
                    State = uf,
                    City = localidade,
                    District = bairro,
                    Street = logradouro,
                    HouseNumber = "",
                    PostalCode = cep,
                    Complement = complemento
                };
            }
        }

        public GEStatesVM GEStates
        {
            get
            {
                if (HereAddress != null)
                    return new GEStatesVM
                    {
                        Initials = HereAddress.Address.State.ToUpper(),
                        Name = HereAddress.Address.StateName
                    };

                return new GEStatesVM
                {
                    Name = null,
                    Initials = (uf ?? "").ToUpper()
                };
            }
        }
        public GECitiesVM GECities
        {
            get
            {
                if (HereAddress != null)
                    return new GECitiesVM
                    {
                        Name = HereAddress.Address.City,
                        IBGE = ibge
                    };

                return new GECitiesVM
                {
                    IBGE = ibge,
                    Name = localidade
                };
            }
        }

        public GECountriesVM Country { get; set; }
        public MapBoxAddressVM GeoAddress { get; set; }
        public HereLocationVM HereAddress { get; set; }
    }

    public class GECEPAddress
    {
        public string Label { get; set; }
        public string Country { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string District { get; set; }
        public string Street { get; set; }
        public string HouseNumber { get; set; }
        public string StateName { get; set; }
        public string PostalCode { get; set; }
        public string Complement { get; set; }
    }

}
