﻿using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Hino.Salesforce.Application.ViewModels
{
    public class DefaultResultModel
    {
        public DefaultResultModel()
        {
            error = new List<ModelException>();
        }

        public int status { get; set; }
        public bool success { get; set; }
        public JContainer data { get; set; }
        public List<ModelException> error { get; set; }
    }
}
