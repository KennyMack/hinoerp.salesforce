﻿using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal;
using Hino.Salesforce.Infra.Cross.Utils;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.Fiscal
{
    public class FSFiscalOperVM : BaseVM
    {
        public FSFiscalOperVM()
        {
            this.VEOrderItems = new HashSet<VEOrderItemsVM>();
        }

        public string Description { get; set; }
        public string GetDescription
        {
            get => $"{Id} - {Description}";
        }
        public long IdERP { get; set; }
        public bool IsBonification { get; set; }
        public bool IsSample { get; set; }

        public virtual ICollection<VEOrderItemsVM> VEOrderItems { get; set; }

        public static Expression<Func<FSFiscalOper, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => r.EstablishmentKey == pEstablishmentKey &&
                    r.Description.ToUpper().Contains(pFilter.Trim());

            return r => r.EstablishmentKey == pEstablishmentKey;
        }
    }
}
