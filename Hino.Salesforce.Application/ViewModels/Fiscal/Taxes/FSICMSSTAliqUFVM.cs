﻿using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.Fiscal.Taxes
{
    [EndPoint("Fiscal/Aliquota/{pEstablishmentKey}")]
    public class FSICMSSTAliqUFVM : BaseVM
    {
        [RequiredField]
        [DisplayField]
        public string UF { get; set; }

        [RequiredField]
        [DisplayField]
        public string NCM { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal ALIQICMSST { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal ALIQICMS { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal MVA { get; set; }

        public static Expression<Func<FSICMSSTAliqUF, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
