﻿using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.Fiscal.Taxes
{
    [EndPoint("Fiscal/NCM/{pEstablishmentKey}")]
    public class FSNCMVM : BaseVM
    {
        [RequiredField]
        [DisplayField("CategoryNCM")]
        [Max255LengthField]
        public string Category { get; set; }

        [RequiredField]
        [DisplayField]
        [Max8LengthField]
        public string NCM { get; set; }

        [RequiredField]
        [DisplayField]
        [Max255LengthField]
        public string Description { get; set; }

        [RequiredField]
        public decimal IPI { get; set; }

        [RequiredField]
        public decimal AliquotaII { get; set; }

        public static Expression<Func<FSNCM, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => 
                (
                    r.EstablishmentKey == pEstablishmentKey &&
                    (
                        r.Description.ToUpper().Contains(pFilter.Trim()) ||
                        r.NCM.Contains(pFilter.Trim())
                    )
                );

            return r => r.EstablishmentKey == pEstablishmentKey;
        }
    }

    public class FSNCMLoadVM : BaseVM
    {
        [RequiredField]
        [DisplayField]
        [Max255LengthField]
        public string Category { get; set; }

        [RequiredField]
        [DisplayField]
        [Max8LengthField]
        public string NCM { get; set; }

        [RequiredField]
        [DisplayField]
        [Max255LengthField]
        public string Description { get; set; }

        [RequiredField]
        public string IPI { get; set; }

        [RequiredField]
        public decimal AliquotaII { get; set; }
    }

    public class FSSearchNCM
    {
        [Key]
        [DisplayField]
        public long Id { get; set; }

        [RequiredField]
        [DisplayField("CategoryNCM")]
        [Max255LengthField]
        public string Category { get; set; }

        [RequiredField]
        [DisplayField]
        [Max8LengthField]
        public string NCM { get; set; }

        [RequiredField]
        [DisplayField]
        [Max255LengthField]
        public string Description { get; set; }

        [RequiredField]
        public decimal IPI { get; set; }

        [RequiredField]
        public decimal AliquotaII { get; set; }
    }
}
