﻿using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.ViewModels.General.Business
{
    public class GEEnterpriseAnnotationVM : BaseVM
    {
        [RequiredField]
        [DisplayField]
        [ForeignKey("GEEnterprises")]
        public long EnterpriseId { get; set; }
        [RequiredField]
        [DisplayField]
        public long UserID { get; set; }
        [RequiredField]
        [DisplayField]
        public string Annotation { get; set; }

    }
}
