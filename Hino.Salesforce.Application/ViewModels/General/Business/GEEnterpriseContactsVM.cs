﻿using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General.Business
{
    [EndPoint("General/Business/Enterprises/Contacts/{pEstablishmentKey}")]
    public class GEEnterpriseContactsVM : BaseVM
    {
        [RequiredField]
        [DisplayField]
        [ForeignKey("GEEnterprises")]
        public long EnterpriseId { get; set; }
        public virtual GEEnterprisesVM GEEnterprises { get; set; }

        /// <summary>
        /// Indice de receptividade 
        /// -1 - Não informado
        /// 0 - Não informado
        /// 1 - Muito Baixa
        /// 2 - Baixa
        /// 3 - Média
        /// 4 - Alta
        /// 5 - Muito alta
        /// </summary>
        [DisplayField]
        [RequiredField]
        [IntegerRangeField(-1, 5)]
        public EContactReceptivity ReceptivityIndex { get; set; }

        /// <summary>
        /// Setor da empresa
        /// 0 - Não informado
        /// 1 - Compras
        /// 2 - Comercial
        /// 3 - Contábil
        /// 4 - Contas
        /// 5 - Departamento técnico
        /// 6 - Engenharia
        /// 7 - Financeiro
        /// 8 - Fiscal
        /// 9 - Jurídico
        /// 10 - Manutenção
        /// 11 - Produtivo
        /// 12 - Qualidade
        /// 13 - Sac
        /// 14 - Suporte
        /// 15 - TI
        /// 16 - Vendas
        /// 17 - NFE
        /// 18 - Faturamento
        /// 19 - Cobrança
        /// 20 - Entrega
        /// 999 - Outros
        /// </summary>
        [DisplayField("ContactSector")]
        [IntegerRangeField(-1, 999)]
        [RequiredField]
        public EContactSectors Sector { get; set; }
        [DisplayField]
        public string Email { get; set; }
        [DisplayField]
        public string Contact { get; set; }
        [DisplayField]
        public string Ramal { get; set; }
        [DisplayField]
        public string Phone { get; set; }
        [DisplayField]
        public string Note { get; set; }

        public static Expression<Func<GEEnterpriseContacts, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                (
                    (
                        r.Email.ToUpper().Contains(pFilter.Trim()) ||
                        r.Contact.Contains(pFilter.Trim()) ||
                        r.Email.Contains(pFilter.Trim()) ||
                        r.Ramal.Contains(pFilter.Trim()) ||
                        r.Phone.Contains(pFilter.Trim())
                    )
                );

            return r => r.EstablishmentKey == pEstablishmentKey;
        }
    }
}
