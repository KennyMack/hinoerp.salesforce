﻿using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General.Business
{
    [EndPoint("General/Business/Enterprises/Geo/{pEstablishmentKey}")]
    public class GEEnterpriseGeoVM : BaseVM
    {
        [RequiredField]
        [DisplayField]
        [ForeignKey("GEEnterprises")]
        // [LongGreaterThan(0)]
        public long EnterpriseID { get; set; }
        public virtual GEEnterprisesVM GEEnterprises { get; set; }
        /// <summary>
        /// 0-Comercial 1-Entrega 2-Cobranca 3-Residencial
        /// </summary>
        [DisplayField("TypeAddress")]
        [IntegerRangeField(0, 3)]
        public EAddressType Type { get; set; }
        [DisplayField]
        //[RequiredField]
        [Max20LengthField]
        public string CNPJCPF { get; set; }
        [DisplayField]
        [Max20LengthField]
        [DefaultValue("ISENTO")]
        public string IE { get; set; }
        [DisplayField]
        [Max20LengthField]
        public string RG { get; set; }
        [DisplayField]
        [Max20LengthField]
        [CellPhoneField]
        public string CellPhone { get; set; }
        [DisplayField]
        [Max20LengthField]
        [PhoneField]
        public string Phone { get; set; }
        [DisplayField]
        [Max255LengthField]
        [EmailField]
        public string Email { get; set; }
        [DisplayField]
        [Max255LengthField]
        [EmailField]
        public string EmailNFE { get; set; }
        [DisplayField]
        [Max255LengthField]
        public string Address { get; set; }
        [DisplayField]
        public string Complement { get; set; }
        [DisplayField]
        [Max255LengthField]
        public string District { get; set; }
        [DisplayField]
        [Max10LengthField]
        public string Num { get; set; }
        [DisplayField]
        [Max10LengthField]
        public string ZipCode { get; set; }
        [DisplayField]
        [Max120LengthField]
        public string Site { get; set; }
        [DisplayField]
        [Max10LengthField]
        [RequiredGroupField(
        properties: new[]{
            "CountryIni",
            "CountryCode",
            "CountryName"
        })]
        public string CountryIni { get; set; }
        [DisplayField]
        [Max10LengthField]
        [RequiredGroupField(
        properties: new[]{
            "CountryIni",
            "CountryCode",
            "CountryName"
        })]
        public string CountryCode { get; set; }
        [DisplayField]
        [Max255LengthField]
        [RequiredGroupField(
        properties: new[]{
            "CountryIni",
            "CountryCode",
            "CountryName"
        })]
        public string CountryName { get; set; }
        public string CodBacen { get; set; }
        [DisplayField]
        [Max2LengthField]
        public string UF { get; set; }
        [DisplayField]
        [Max255LengthField]
        public string StateName { get; set; }
        [DisplayField]
        [Max20LengthField]
        public string IBGE { get; set; }
        [DisplayField]
        [Max255LengthField]
        public string CityName { get; set; }
        [DisplayField]
        public decimal DisplayLat { get; set; }
        [DisplayField]
        public decimal DisplayLng { get; set; }
        [DisplayField]
        public decimal NavLat { get; set; }
        [DisplayField]
        public decimal NavLng { get; set; }
        [DisplayField]
        public string InscSuframa { get; set; }

        public static Expression<Func<GEEnterpriseGeo, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                (
                    (
                        r.CNPJCPF.ToUpper().Contains(pFilter.Trim()) ||
                        r.IE.Contains(pFilter.Trim()) ||
                        r.RG.Contains(pFilter.Trim()) ||
                        r.CellPhone.Contains(pFilter.Trim()) ||
                        r.Phone.Contains(pFilter.Trim()) ||
                        r.Email.Contains(pFilter.Trim()) ||
                        r.Address.Contains(pFilter.Trim()) ||
                        r.Complement.Contains(pFilter.Trim()) ||
                        r.Num.Contains(pFilter.Trim()) ||
                        r.ZipCode.Contains(pFilter.Trim()) ||
                        r.Site.Contains(pFilter.Trim()) ||
                        r.CountryIni.Contains(pFilter.Trim()) ||
                        r.CountryName.Contains(pFilter.Trim()) ||
                        r.StateName.Contains(pFilter.Trim()) ||
                        r.IBGE.Contains(pFilter.Trim()) ||
                        r.CityName.Contains(pFilter.Trim())
                    )
                );

            return r => r.EstablishmentKey == pEstablishmentKey;
        }

        public static GEEnterpriseGeoVM GetDefaultProspectGeo()
        {
            return new GEEnterpriseGeoVM()
            {
                Type = EAddressType.Commercial,
                ZipCode = "00000000",
                Address = "PROSPECT",
                Num = "0",
                District = "PROSPECT",
                CityName = "São Paulo",
                StateName = "São Paulo",
                UF = "SP",
                IBGE = "3550308",
                CountryCode = "076",
                CountryIni = "BRA",
                CountryName = "BRASIL"
            };
        }
    }
}
