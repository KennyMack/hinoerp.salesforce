﻿using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General.Business
{
    [EndPoint("General/Business/Enterprises/Group/{pEstablishmentKey}")]
    public class GEEnterpriseGroupVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public string Description { get; set; }

        [DisplayField]
        public string Identifier { get; set; }

        [DisplayField]
        [RequiredField]
        public long IdERP { get; set; }

        public static Expression<Func<GEEnterpriseGroup, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                (
                    (
                        r.Id.ToString().Contains(pFilter.Trim()) ||
                        r.Description.ToUpper().Contains(pFilter.Trim()) ||
                        r.Identifier.ToUpper().Contains(pFilter.Trim())
                    )
                );

            return r => r.EstablishmentKey == pEstablishmentKey;
        }
    }
}
