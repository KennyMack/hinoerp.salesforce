﻿using Hino.Salesforce.Application.ViewModels.General.Events;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General.Business
{
    [EndPoint("General/Business/Enterprises/{pEstablishmentKey}")]
    public class GEEnterprisesVM : BaseVM
    {
        public GEEnterprisesVM()
        {
            this.VEOrders = new HashSet<VEOrdersVM>();
            this.VEOrdersCarrier = new HashSet<VEOrdersVM>();
            this.VEOrdersRedispatch = new HashSet<VEOrdersVM>();
            this.GEEnterpriseGeo = new HashSet<GEEnterpriseGeoVM>();
            this.GEEnterpriseContacts = new HashSet<GEEnterpriseContactsVM>();
            this.GEUserEnterprises = new HashSet<GEUserEnterprisesVM>();
            this.GEFilesPath = new HashSet<GEFilesPathVM>();
            this.GEEnterpriseEvent = new HashSet<GEEnterpriseEventVM>();
            this.GEEnterpriseAnnotation = new HashSet<GEEnterpriseAnnotationVM>();
        }

        [RequiredField]
        [DisplayField]
        [Max60LengthField]
        public string RazaoSocial { get; set; }
        [RequiredField]
        [DisplayField]
        [Max60LengthField]
        public string NomeFantasia { get; set; }
        //[RequiredField]
        [DisplayField]
        [Max20LengthField]
        public string CNPJCPF { get; set; }
        [RequiredField]
        [DisplayField]
        [Max20LengthField]
        [DefaultValue("N/INFO")]
        public string IE { get; set; }
        [RequiredField]
        [DisplayField]
        [IntegerRangeField(0, 1)]
        public EEnterpriseType Type { get; set; }
        [RequiredField]
        [DisplayField]
        [IntegerRangeField(0, 3)]
        public EStatusEnterprise Status { get; set; }
        [RequiredField]
        [DisplayField]
        [IntegerRangeField(0, 2)]
        public EStatusSinc StatusSinc { get; set; }
        [DisplayField]
        [ForeignKey("GEPaymentCondition")]
        [IntegerRangeField(1, int.MaxValue)]
        public long? PayConditionId { get; set; }
        public virtual GEPaymentConditionVM GEPaymentCondition { get; set; }

        [DisplayField]
        [ForeignKey("GEEnterpriseFiscalGroup")]
        public long? FiscalGroupId { get; set; }
        public virtual GEEnterpriseFiscalGroupVM GEEnterpriseFiscalGroup { get; set; }

        [DisplayField]
        [ForeignKey("GEEnterpriseCategory")]
        public long? CategoryId { get; set; }
        public virtual GEEnterpriseCategoryVM GEEnterpriseCategory { get; set; }

        [DisplayField]
        [ForeignKey("GEEnterpriseGroup")]
        public long? GroupId { get; set; }
        public virtual GEEnterpriseGroupVM GEEnterpriseGroup { get; set; }

        [DisplayField]
        public long? TypeSaleId { get; set; }
        public VETypeSaleVM VETypeSale { get; set; }

        [DisplayField]
        public long IdERP { get; set; }

        [DisplayField]
        public long UserId { get; set; }

        [DisplayField]
        public long? CreatedById { get; set; }

        [DisplayField]
        [RequiredField]
        public long RegionId { get; set; }

        [DisplayField]
        [RequiredField]
        public decimal CreditLimit { get; set; }

        [DisplayField]
        [RequiredField]
        public decimal CreditUsed { get; set; }

        [DisplayField]
        [RequiredField]
        [IntegerRangeField(0, 2)]
        public EEnterpriseClassification Classification { get; set; }
        
        [DisplayField]
        [DefaultValue(false)]
        public bool SellerChangePrice { get; set; }
        [DisplayField]
        [DefaultValue(false)]
        public bool SellerPayCondition { get; set; }
        [DisplayField]
        [DefaultValue(false)]
        public bool SellerBonification { get; set; }
        [DisplayField]
        [DefaultValue(false)]
        public bool SellerSample { get; set; }
        [DisplayField]
        [IntegerRangeField(0, 1)]
        public short DiscountNFBoleto { get; set; }
        [DisplayField("ClientPercDiscount")]
        [DefaultValue(0)]
        public decimal PercDiscount { get; set; }

        [DisplayField]
        public string ClassifEmpresa { get; set; }

        [DisplayField]
        public long? CodPrVenda { get; set; }

        public VESalePriceVM SalePrice { get; set; }

        public ICollection<VEOrdersVM> VEOrders { get; set; }

        public ICollection<VEOrdersVM> VEOrdersCarrier { get; set; }

        public ICollection<VEOrdersVM> VEOrdersRedispatch { get; set; }

        public ICollection<GEEnterpriseGeoVM> GEEnterpriseGeo { get; set; }

        public ICollection<GEEnterpriseContactsVM> GEEnterpriseContacts { get; set; }

        public ICollection<GEUserEnterprisesVM> GEUserEnterprises { get; set; }
        public ICollection<GEFilesPathVM> GEFilesPath { get; set; }
        public ICollection<GEEnterpriseEventVM> GEEnterpriseEvent { get; set; }
        public ICollection<GEEnterpriseAnnotationVM> GEEnterpriseAnnotation { get; set; }

        public GEEnterpriseGeoVM CommercialAddress { get => this.GEEnterpriseGeo.FirstOrDefault(r => r.Type == EAddressType.Commercial); }

        public GEEnterpriseGeoVM DeliveryAddress { get => this.GEEnterpriseGeo.FirstOrDefault(r => r.Type == EAddressType.Delivery); }

        public GEEnterpriseGeoVM LevyAddress { get => this.GEEnterpriseGeo.FirstOrDefault(r => r.Type == EAddressType.Levy); }

        public ICollection<GEEnterpriseContactsVM> Phones { get; set; }
        public ICollection<GEEnterpriseContactsVM> Emails { get; set; }

        public string search { get; set; }

        [DisplayField]
        public DateTime? BirthDate { get; set; }

        public static Expression<Func<GEEnterprises, bool>> GetColumnsFilter(string pColumn, string pFilter, string pEstablishmentKey)
        {
            switch (pColumn)
            {
                case "Id":
                    return r => r.Id.ToString().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey;
                case "RazaoSocial":
                    return r => r.RazaoSocial.ToUpper().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey;
                case "NomeFantasia":
                    return r => r.NomeFantasia.ToUpper().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey;
                case "CNPJCPF":
                    return r => r.CNPJCPF.Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey;
                case "IE":
                    return r => r.IE.Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey;
                case "ClassifEmpresa":
                    return r => r.ClassifEmpresa.ToUpper().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey;
            };

            return r => r.EstablishmentKey == pEstablishmentKey;
        }

        public static Expression<Func<GEEnterprises, bool>> GetColumnsFilterClientOrProspect(string pColumn, string pFilter, string pEstablishmentKey)
        {
            switch (pColumn)
            {
                case "Id":
                    return r => (r.Id.ToString().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey) &&
                     (r.Classification == EEnterpriseClassification.Prospect ||
                     r.Classification == EEnterpriseClassification.Client);
                case "RazaoSocial":
                    return r => (r.RazaoSocial.ToUpper().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey) &&
                     (r.Classification == EEnterpriseClassification.Prospect ||
                     r.Classification == EEnterpriseClassification.Client);
                case "NomeFantasia":
                    return r => (r.NomeFantasia.ToUpper().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey) &&
                     (r.Classification == EEnterpriseClassification.Prospect ||
                     r.Classification == EEnterpriseClassification.Client);
                case "CNPJCPF":
                    return r => (r.CNPJCPF.Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey) &&
                     (r.Classification == EEnterpriseClassification.Prospect ||
                     r.Classification == EEnterpriseClassification.Client);
                case "IE":
                    return r => (r.IE.Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey) &&
                     (r.Classification == EEnterpriseClassification.Prospect ||
                     r.Classification == EEnterpriseClassification.Client);
                case "ClassifEmpresa":
                    return r => (r.ClassifEmpresa.ToUpper().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey) &&
                     (r.Classification == EEnterpriseClassification.Prospect ||
                     r.Classification == EEnterpriseClassification.Client);
            };

            return r => r.EstablishmentKey == pEstablishmentKey &&
                     (r.Classification == EEnterpriseClassification.Prospect ||
                     r.Classification == EEnterpriseClassification.Client);
        }

        public static Expression<Func<GEEnterprises, bool>> GetColumnsFilterClassification(string pColumn, string pFilter, string pEstablishmentKey, EEnterpriseClassification pClassification)
        {
            switch (pColumn)
            {
                case "Id":
                    return r => (r.Id.ToString().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey) &&
                     r.Classification == pClassification;
                case "RazaoSocial":
                    return r => (r.RazaoSocial.ToUpper().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey) &&
                     r.Classification == pClassification;
                case "NomeFantasia":
                    return r => (r.NomeFantasia.ToUpper().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey) &&
                     r.Classification == pClassification;
                case "CNPJCPF":
                    return r => (r.CNPJCPF.Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey) &&
                     r.Classification == pClassification;
                case "IE":
                    return r => (r.IE.Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey) &&
                     r.Classification == pClassification;
                case "ClassifEmpresa":
                    return r => (r.ClassifEmpresa.ToUpper().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey) &&
                     r.Classification == pClassification;
            };

            return r => r.EstablishmentKey == pEstablishmentKey &&
                     r.Classification == pClassification;
        }

        public static Expression<Func<GEEnterprises, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter, string pColumn)
        {
            if (!pFilter.IsEmpty() && pColumn.IsEmpty())
                return r => r.EstablishmentKey == pEstablishmentKey &&
                    r.search.Contains(pFilter);
            else if (!pFilter.IsEmpty() && !pColumn.IsEmpty())
                return GetColumnsFilter(pColumn, pFilter.Trim(), pEstablishmentKey);

            /*(
                (
                    r.Id.ToString().Contains(pFilter.Trim()) ||
                    r.RazaoSocial.ToUpper().Contains(pFilter.Trim()) ||
                    r.NomeFantasia.ToUpper().Contains(pFilter.Trim()) ||
                    r.CNPJCPF.ToUpper().Contains(pFilter.Trim()) ||
                    r.IE.ToUpper().Contains(pFilter.Trim()) ||
                    r.ClassifEmpresa.ToUpper().Contains(pFilter.Trim())
                )
            );*/

            return r => r.EstablishmentKey == pEstablishmentKey;
        }

        public static Expression<Func<GEEnterprises, bool>> GetDefaultFilterClientOrProspect(string pEstablishmentKey, string pFilter, string pColumn)
        {
            if (!pFilter.IsEmpty() && pColumn.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                     (r.Classification == EEnterpriseClassification.Prospect ||
                     r.Classification == EEnterpriseClassification.Client) &&
                    r.search.Contains(pFilter);
            else if (!pFilter.IsEmpty() && !pColumn.IsEmpty())
                return GetColumnsFilterClientOrProspect(pColumn, pFilter.Trim(), pEstablishmentKey);
            /* (
                 (
                     r.Id.ToString().Contains(pFilter.Trim()) ||
                     r.RazaoSocial.ToUpper().Contains(pFilter.Trim()) ||
                     r.NomeFantasia.ToUpper().Contains(pFilter.Trim()) ||
                     r.CNPJCPF.ToUpper().Contains(pFilter.Trim()) ||
                     r.IE.ToUpper().Contains(pFilter.Trim()) ||
                     r.ClassifEmpresa.ToUpper().Contains(pFilter.Trim())
                 )
             );*/

            return r => r.EstablishmentKey == pEstablishmentKey &&
                     (r.Classification == Infra.Cross.Utils.Enums.EEnterpriseClassification.Prospect ||
                     r.Classification == Infra.Cross.Utils.Enums.EEnterpriseClassification.Client);
        }

        public static Expression<Func<GEEnterprises, bool>> GetDefaultFilterByClassification(string pEstablishmentKey, string pFilter, string pColumn, EEnterpriseClassification pClassification)
        {
            if (!pFilter.IsEmpty() && pColumn.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey &&
                     r.Classification == pClassification) &&
                    r.search.Contains(pFilter);
            else if (!pFilter.IsEmpty() && !pColumn.IsEmpty())
                return GetColumnsFilterClassification(pColumn, pFilter.Trim(), pEstablishmentKey, pClassification);
            /*(
                    (
                        r.Id.ToString().Contains(pFilter.Trim()) ||
                        r.RazaoSocial.Contains(pFilter.Trim()) ||
                        r.NomeFantasia.Contains(pFilter.Trim()) ||
                        r.CNPJCPF.Contains(pFilter.Trim()) ||
                        r.IE.Contains(pFilter.Trim()) ||
                        r.ClassifEmpresa.Contains(pFilter.Trim())
                    )
                );*/

            return r => (r.EstablishmentKey == pEstablishmentKey &&
                     r.Classification == pClassification);
        }

    }

    [EndPoint("General/Business/Enterprises/{pEstablishmentKey}")]
    public class GESyncEnterprisesVM : BaseVM
    {
        public GESyncEnterprisesVM()
        {
            this.GEEnterpriseGeo = new HashSet<GEEnterpriseGeoVM>();
            this.GEEnterpriseContacts = new HashSet<GEEnterpriseContactsVM>();
            this.GEUserEnterprises = new HashSet<GEUserEnterprisesVM>();
            this.GEFilesPath = new HashSet<GEFilesPathVM>();
            this.GEEnterpriseEvent = new HashSet<GEEnterpriseEventVM>();
        }

        [RequiredField]
        [DisplayField]
        [Max60LengthField]
        public string RazaoSocial { get; set; }
        [RequiredField]
        [DisplayField]
        [Max60LengthField]
        public string NomeFantasia { get; set; }
        //[RequiredField]
        [DisplayField]
        [Max20LengthField]
        public string CNPJCPF { get; set; }
        [RequiredField]
        [DisplayField]
        [Max20LengthField]
        [DefaultValue("N/INFO")]
        public string IE { get; set; }
        [RequiredField]
        [DisplayField]
        [IntegerRangeField(0, 1)]
        public EEnterpriseType Type { get; set; }
        [RequiredField]
        [DisplayField]
        [IntegerRangeField(0, 3)]
        public EStatusEnterprise Status { get; set; }
        [RequiredField]
        [DisplayField]
        [IntegerRangeField(0, 2)]
        public EStatusSinc StatusSinc { get; set; }

        [DisplayField]
        [ForeignKey("GEPaymentCondition")]
        [IntegerRangeField(1, int.MaxValue)]
        public long? PayConditionId { get; set; }
        public virtual GEPaymentConditionVM GEPaymentCondition { get; set; }

        [DisplayField]
        [ForeignKey("GEEnterpriseFiscalGroup")]
        public long? FiscalGroupId { get; set; }
        public virtual GEEnterpriseFiscalGroupVM GEEnterpriseFiscalGroup { get; set; }

        [DisplayField]
        [ForeignKey("GEEnterpriseCategory")]
        public long? CategoryId { get; set; }
        public virtual GEEnterpriseCategoryVM GEEnterpriseCategory { get; set; }

        [DisplayField]
        [ForeignKey("GEEnterpriseGroup")]
        public long? GroupId { get; set; }
        public virtual GEEnterpriseGroupVM GEEnterpriseGroup { get; set; }

        [DisplayField]
        public long IdERP { get; set; }

        [DisplayField]
        [RequiredField]
        public long UserId { get; set; }

        [DisplayField]
        public long? CreatedById { get; set; }

        [DisplayField]
        [RequiredField]
        public long RegionId { get; set; }

        [DisplayField]
        [RequiredField]
        [IntegerRangeField(0, 2)]
        public EEnterpriseClassification Classification { get; set; }

        [DisplayField]
        public string ClassifEmpresa { get; set; }

        public ICollection<GEEnterpriseGeoVM> GEEnterpriseGeo { get; set; }
        public ICollection<GEEnterpriseContactsVM> GEEnterpriseContacts { get; set; }
        public ICollection<GEUserEnterprisesVM> GEUserEnterprises { get; set; }
        public ICollection<GEFilesPathVM> GEFilesPath { get; set; }
        public ICollection<GEEnterpriseEventVM> GEEnterpriseEvent { get; set; }

        [DisplayField]
        public DateTime? BirthDate { get; set; }
    }

    public class GECarriersVM
    {

        [Key]
        [DisplayField]
        public long Id { get; set; }
        [Min36LengthField]
        [Max36LengthField]
        [RequiredField]
        [DisplayField]
        public string EstablishmentKey { get; set; }
        [RequiredField]
        [Min36LengthField]
        [Max36LengthField]
        [DisplayField]
        public string UniqueKey { get; set; }
        [RequiredField]
        [DisplayField]
        [Max60LengthField]
        public string RazaoSocial { get; set; }
        [RequiredField]
        [DisplayField]
        [Max60LengthField]
        public string NomeFantasia { get; set; }
        //[RequiredField]
        [DisplayField]
        [Max20LengthField]
        public string CNPJCPF { get; set; }
        [RequiredField]
        [DisplayField]
        [IntegerRangeField(0, 1)]
        public EEnterpriseType Type { get; set; }
    }

    public class GEViewEnterpriseDetailVM
    {

        [Key]
        [DisplayField]
        public long Id { get; set; }
        [Min36LengthField]
        [Max36LengthField]
        [RequiredField]
        [DisplayField]
        public string EstablishmentKey { get; set; }
        [RequiredField]
        [Min36LengthField]
        [Max36LengthField]
        [DisplayField]
        public string UniqueKey { get; set; }
        [RequiredField]
        [DisplayField]
        [Max60LengthField]
        public string RazaoSocial { get; set; }
        [RequiredField]
        [DisplayField]
        [Max60LengthField]
        public string NomeFantasia { get; set; }
        //[RequiredField]
        [DisplayField]
        [Max20LengthField]
        public string CNPJCPF { get; set; }
        [RequiredField]
        [DisplayField]
        [IntegerRangeField(0, 1)]
        public EEnterpriseType Type { get; set; }

        [JsonIgnore]
        public ICollection<GEEnterpriseGeoVM> GEEnterpriseGeo { get; set; }

        public GEEnterpriseGeoVM Address { get => this.GEEnterpriseGeo.FirstOrDefault(r => r.Type == EAddressType.Commercial); }
    }

    public class GEClientsVM
    {
        [Key]
        [DisplayField]
        public long Id { get; set; }
        [Min36LengthField]
        [Max36LengthField]
        [RequiredField]
        [DisplayField]
        public string EstablishmentKey { get; set; }
        [RequiredField]
        [Min36LengthField]
        [Max36LengthField]
        [DisplayField]
        public string UniqueKey { get; set; }

        [RequiredField]
        [DisplayField]
        [Max60LengthField]
        public string RazaoSocial { get; set; }
        [RequiredField]
        [DisplayField]
        [Max60LengthField]
        public string NomeFantasia { get; set; }
        //[RequiredField]
        [DisplayField]
        [Max20LengthField]
        public string CNPJCPF { get; set; }
        [RequiredField]
        [DisplayField]
        [IntegerRangeField(0, 1)]
        public EEnterpriseType Type { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerRangeField(0, 2)]
        public EEnterpriseClassification Classification { get; set; }
        [DisplayField]
        [RequiredField]
        public long RegionId { get; set; }
        [DisplayField]
        public long? TypeSaleId { get; set; }
        [DisplayField]
        public long? PayConditionId { get; set; }

        [DisplayField]
        public long? CodPrVenda { get; set; }

        [DisplayField]
        [DefaultValue(false)]
        public bool SellerChangePrice { get; set; }
        [DisplayField]
        [DefaultValue(false)]
        public bool SellerPayCondition { get; set; }
        [DisplayField]
        [DefaultValue(false)]
        public bool SellerBonification { get; set; }
        [DisplayField]
        [DefaultValue(false)]
        public bool SellerSample { get; set; }
    }

    public class GEMainClientsVM
    {
        [DisplayField]
        public long Id { get; set; }

        [DisplayField]
        public string EstablishmentKey { get; set; }

        [DisplayField]
        public string UniqueKey { get; set; }

        [DisplayField]
        public string RazaoSocial { get; set; }

        [DisplayField]
        public string NomeFantasia { get; set; }

        [DisplayField]
        public string CNPJCPF { get; set; }

        [DisplayField]
        public EEnterpriseType Type { get; set; }

        [DisplayField]
        public EStatusEnterprise Status { get; set; }

        [DisplayField]
        public EStatusSinc StatusSinc { get; set; }

        [DisplayField]
        public EEnterpriseClassification Classification { get; set; }

        [DisplayField]
        public long RegionId { get; set; }

        [DisplayField]
        public decimal CreditLimit { get; set; }

        [DisplayField]
        public decimal CreditUsed { get; set; }

        public string UF => CommercialAddress?.UF;

        public string Address => CommercialAddress?.Address;

        public string Phone => CommercialAddress?.Phone;

        public string Email => CommercialAddress?.Email;

        [DisplayField]
        public DateTime Created { get; set; }

        [DisplayField]
        public DateTime Modified { get; set; }

        [JsonIgnore]
        public ICollection<GEEnterpriseGeoVM> GEEnterpriseGeo { get; set; }
        [JsonIgnore]
        public GEEnterpriseGeoVM CommercialAddress { get => this.GEEnterpriseGeo.FirstOrDefault(r => r.Type == EAddressType.Commercial); }
        [JsonIgnore]
        public GEEnterpriseGeoVM DeliveryAddress { get => this.GEEnterpriseGeo.FirstOrDefault(r => r.Type == EAddressType.Delivery); }
        [JsonIgnore]
        public GEEnterpriseGeoVM LevyAddress { get => this.GEEnterpriseGeo.FirstOrDefault(r => r.Type == EAddressType.Levy); }
    }

    public class GEMainCarriersVM
    {
        [DisplayField]
        public long Id { get; set; }

        [DisplayField]
        public string EstablishmentKey { get; set; }

        [DisplayField]
        public string UniqueKey { get; set; }

        [DisplayField]
        public string RazaoSocial { get; set; }

        [DisplayField]
        public string NomeFantasia { get; set; }

        [DisplayField]
        public string CNPJCPF { get; set; }

        [DisplayField]
        public EEnterpriseType Type { get; set; }

        [DisplayField]
        public EStatusEnterprise Status { get; set; }

        [DisplayField]
        public EStatusSinc StatusSinc { get; set; }

        [DisplayField]
        public EEnterpriseClassification Classification { get; set; }

        [DisplayField]
        public long RegionId { get; set; }

        [DisplayField]
        public decimal CreditLimit { get; set; }

        [DisplayField]
        public decimal CreditUsed { get; set; }

        public string UF => CommercialAddress?.UF;

        public string Address => CommercialAddress?.Address;

        public string Phone => CommercialAddress?.Phone;

        public string Email => CommercialAddress?.Email;

        [DisplayField]
        public DateTime Created { get; set; }

        [DisplayField]
        public DateTime Modified { get; set; }

        [JsonIgnore]
        public ICollection<GEEnterpriseGeoVM> GEEnterpriseGeo { get; set; }
        [JsonIgnore]
        public GEEnterpriseGeoVM CommercialAddress { get => this.GEEnterpriseGeo.FirstOrDefault(r => r.Type == EAddressType.Commercial); }
        [JsonIgnore]
        public GEEnterpriseGeoVM DeliveryAddress { get => this.GEEnterpriseGeo.FirstOrDefault(r => r.Type == EAddressType.Delivery); }
        [JsonIgnore]
        public GEEnterpriseGeoVM LevyAddress { get => this.GEEnterpriseGeo.FirstOrDefault(r => r.Type == EAddressType.Levy); }
    }
}
