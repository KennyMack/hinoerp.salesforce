﻿using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General.Business
{
    [EndPoint("General/Business/Payment/Condition/Installments/{pEstablishmentKey}")]
    public class GEPaymentCondInstallmentsVM : BaseVM
    {
        public int InstallmentNumber { get; set; }

        [ForeignKey("GEPaymentCondition")]
        [RequiredField]
        [DisplayField]
        public long CondPayId { get; set; }
        public virtual GEPaymentConditionVM GEPaymentCondition { get; set; }

        [RequiredField]
        [DisplayField]
        [DefaultValue(0)]
        public decimal Percent { get; set; }
        [RequiredField]
        [DisplayField]
        [DefaultValue(0)]
        public int Days { get; set; }

        public static Expression<Func<GEPaymentCondInstallments, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
