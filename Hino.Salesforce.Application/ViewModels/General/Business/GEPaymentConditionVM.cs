﻿using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General.Business
{
    [EndPoint("General/Business/Payment/Condition/{pEstablishmentKey}")]
    public class GEPaymentConditionVM : BaseVM
    {
        public GEPaymentConditionVM()
        {
            this.GEPaymentCondInstallments = new HashSet<GEPaymentCondInstallmentsVM>();
            _GEPaymentCondInstallments = new HashSet<GEPaymentCondInstallmentsVM>();
        }


        [ForeignKey("GEPaymentType")]
        [RequiredField]
        [DisplayField]
        public long TypePayID { get; set; }
        public virtual GEPaymentTypeVM GEPaymentType { get; set; }

        [RequiredField]
        [DisplayField]
        public string Description { get; set; }
        public short Installments { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal PercDiscount { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal PercIncrease { get; set; }

        ICollection<GEPaymentCondInstallmentsVM> _GEPaymentCondInstallments;
        public virtual ICollection<GEPaymentCondInstallmentsVM> GEPaymentCondInstallments
        {
            get => _GEPaymentCondInstallments.OrderBy(r => r.Days)
                .Select((r, i) =>
                {
                    r.InstallmentNumber = i + 1;
                    return r;
                })
                .ToList(); set => _GEPaymentCondInstallments = value;
        }

        public long IdERP { get; set; }

        public string GetDescription
        {
            get
            {
                if (GEPaymentType != null)
                    return $"{GEPaymentType.Id} - {GEPaymentType.Description}@{Id} - {Description}";

                return $"{Id} - {Description}";
            }
        }

        public static Expression<Func<GEPaymentCondition, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                (
                    (
                        r.Id.ToString().Contains(pFilter.Trim()) ||
                        r.Description.ToUpper().Contains(pFilter.Trim()) ||
                        r.GEPaymentType.Description.ToUpper().Contains(pFilter.Trim())
                    )
                );

            return r => r.EstablishmentKey == pEstablishmentKey;
        }
        public static Expression<Func<GEPaymentCondition, bool>> GetFilterByUser(string pEstablishmentKey, long pUserId, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey &&
                             r.GEPaymentType.GEUserPayType.Any(u => u.UserId == pUserId)) &&
                (
                    (
                        r.Id.ToString().Contains(pFilter.Trim()) ||
                        r.Description.ToUpper().Contains(pFilter.Trim()) ||
                        r.GEPaymentType.Description.ToUpper().Contains(pFilter.Trim())
                    )
                );

            return r => r.EstablishmentKey == pEstablishmentKey &&
                        r.GEPaymentType.GEUserPayType.Any(u => u.UserId == pUserId);
        }
    }

    [EndPoint("General/Business/Payment/Condition/{pEstablishmentKey}")]
    public class GEPaymentConditionCreateVM : BaseVM
    {
        public GEPaymentConditionCreateVM()
        {
            this.GEPaymentCondInstallments = new HashSet<GEPaymentCondInstallmentsVM>();
        }

        [ForeignKey("GEPaymentType")]
        [RequiredField]
        [DisplayField]
        public long TypePayID { get; set; }
        public virtual GEPaymentTypeVM GEPaymentType { get; set; }

        [RequiredField]
        [DisplayField]
        public string Description { get; set; }
        public short Installments { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal PercDiscount { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal PercIncrease { get; set; }

        public virtual ICollection<GEPaymentCondInstallmentsVM> GEPaymentCondInstallments { get; set; }

        public long IdERP { get; set; }

        public string GetDescription
        {
            get
            {
                if (GEPaymentType != null)
                    return $"{GEPaymentType.Id} - {GEPaymentType.Description}@{Id} - {Description}";

                return $"{Id} - {Description}";
            }
        }
    }
}
