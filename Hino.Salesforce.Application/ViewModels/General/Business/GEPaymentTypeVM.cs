﻿using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General.Business
{
    [EndPoint("General/Business/Payment/Type/{pEstablishmentKey}")]
    public class GEPaymentTypeVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public string Description { get; set; }

        public long IdERP { get; set; }

        public bool Boleto { get; set; }

        public static Expression<Func<GEPaymentType, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                (
                    (
                        r.Description.ToUpper().Contains(pFilter.Trim())
                    )
                );

            return r => r.EstablishmentKey == pEstablishmentKey;
        }
    }
}
