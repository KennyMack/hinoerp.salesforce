﻿using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General.Business
{
    [EndPoint("General/Business/User/Enterprises/{pEstablishmentKey}")]
    public class GEUserEnterprisesVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        [ForeignKey("GEUsers")]
        public long UserId { get; set; }
        public virtual GEUsersVM GEUsers { get; set; }

        [DisplayField]
        [RequiredField]
        [ForeignKey("GEEnterprises")]
        public long EnterpriseId { get; set; }
        public virtual GEEnterprisesVM GEEnterprises { get; set; }

        public static Expression<Func<GEUserEnterprises, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter, long pUserId)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId) &&
                   (
                     (
                        (r.Id.ToString().Contains(pFilter.Trim())) ||
                        (r.UserId.ToString().Contains(pFilter)) ||
                        (r.EnterpriseId.ToString().Contains(pFilter))
                     )
                   );

            return r => r.EstablishmentKey == pEstablishmentKey &&
                   r.UserId == pUserId;
        }

        public static Expression<Func<GEUserEnterprises, bool>> GetColumnsFilterClientOrProspect(string pColumn, string pFilter, long pUserId, string pEstablishmentKey)
        {
            switch (pColumn)
            {
                case "Id":
                    return r => (r.GEEnterprises.Id.ToString().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey) &&
                     (r.GEEnterprises.Classification == EEnterpriseClassification.Prospect ||
                     r.GEEnterprises.Classification == EEnterpriseClassification.Client);
                case "RazaoSocial":
                    return r => (r.GEEnterprises.RazaoSocial.ToUpper().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey) &&
                     (r.GEEnterprises.Classification == EEnterpriseClassification.Prospect ||
                     r.GEEnterprises.Classification == EEnterpriseClassification.Client);
                case "NomeFantasia":
                    return r => (r.GEEnterprises.NomeFantasia.ToUpper().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey) &&
                     (r.GEEnterprises.Classification == EEnterpriseClassification.Prospect ||
                     r.GEEnterprises.Classification == EEnterpriseClassification.Client);
                case "CNPJCPF":
                    return r => (r.GEEnterprises.CNPJCPF.Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey) &&
                     (r.GEEnterprises.Classification == EEnterpriseClassification.Prospect ||
                     r.GEEnterprises.Classification == EEnterpriseClassification.Client);
                case "IE":
                    return r => (r.GEEnterprises.IE.Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey) &&
                     (r.GEEnterprises.Classification == EEnterpriseClassification.Prospect ||
                     r.GEEnterprises.Classification == EEnterpriseClassification.Client);
                case "ClassifEmpresa":
                    return r => (r.GEEnterprises.ClassifEmpresa.ToUpper().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey) &&
                     (r.GEEnterprises.Classification == EEnterpriseClassification.Prospect ||
                     r.GEEnterprises.Classification == EEnterpriseClassification.Client);
            };

            return r => r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey &&
                     (r.GEEnterprises.Classification == EEnterpriseClassification.Prospect ||
                     r.GEEnterprises.Classification == EEnterpriseClassification.Client);
        }

        public static Expression<Func<GEUserEnterprises, bool>> GetColumnsFilterClassification(string pColumn, string pFilter, long pUserId, string pEstablishmentKey, EEnterpriseClassification pClassification)
        {
            switch (pColumn)
            {
                case "Id":
                    return r => (r.GEEnterprises.Id.ToString().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey) &&
                     r.GEEnterprises.Classification == pClassification;
                case "RazaoSocial":
                    return r => (r.GEEnterprises.RazaoSocial.ToUpper().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey) &&
                     r.GEEnterprises.Classification == pClassification;
                case "NomeFantasia":
                    return r => (r.GEEnterprises.NomeFantasia.ToUpper().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey) &&
                     r.GEEnterprises.Classification == pClassification;
                case "CNPJCPF":
                    return r => (r.GEEnterprises.CNPJCPF.Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey) &&
                     r.GEEnterprises.Classification == pClassification;
                case "IE":
                    return r => (r.GEEnterprises.IE.Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey) &&
                     r.GEEnterprises.Classification == pClassification;
                case "ClassifEmpresa":
                    return r => (r.GEEnterprises.ClassifEmpresa.ToUpper().Contains(pFilter) && r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey) &&
                     r.GEEnterprises.Classification == pClassification;
            };

            return r => r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey &&
                     r.GEEnterprises.Classification == pClassification;
        }

        public static Expression<Func<GEUserEnterprises, bool>> GetDefaultFilterClientOrProspect(string pEstablishmentKey, string pFilter, long pUserId, string pColumn)
        {
            if (!pFilter.IsEmpty() && pColumn.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey &&
                     (r.GEEnterprises.Classification == Infra.Cross.Utils.Enums.EEnterpriseClassification.Prospect ||
                     r.GEEnterprises.Classification == Infra.Cross.Utils.Enums.EEnterpriseClassification.Client)) &&
                (
                    (
                        r.GEEnterprises.Id.ToString().Contains(pFilter.Trim()) ||
                        r.GEEnterprises.RazaoSocial.ToUpper().Contains(pFilter.Trim()) ||
                        r.GEEnterprises.NomeFantasia.ToUpper().Contains(pFilter.Trim()) ||
                        r.GEEnterprises.CNPJCPF.ToUpper().Contains(pFilter.Trim()) ||
                        r.GEEnterprises.IE.ToUpper().Contains(pFilter.Trim()) ||
                        r.GEEnterprises.ClassifEmpresa.ToUpper().Contains(pFilter.Trim())
                    )
                );
            else if (!pFilter.IsEmpty() && !pColumn.IsEmpty())
                return GetColumnsFilterClientOrProspect(pColumn, pFilter.Trim(), pUserId, pEstablishmentKey);

            return r => r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey &&
                     (r.GEEnterprises.Classification == Infra.Cross.Utils.Enums.EEnterpriseClassification.Prospect ||
                     r.GEEnterprises.Classification == Infra.Cross.Utils.Enums.EEnterpriseClassification.Client);
        }

        public static Expression<Func<GEUserEnterprises, bool>> GetDefaultFilterByClassification(string pEstablishmentKey, string pFilter,
            long pUserId, string pColumn, EEnterpriseClassification pClassification)
        {
            if (!pFilter.IsEmpty() && pColumn.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey &&
                     r.GEEnterprises.Classification == pClassification) &&
                (
                    (
                        r.GEEnterprises.Id.ToString().Contains(pFilter.Trim()) ||
                        r.GEEnterprises.RazaoSocial.ToUpper().Contains(pFilter.Trim()) ||
                        r.GEEnterprises.NomeFantasia.ToUpper().Contains(pFilter.Trim()) ||
                        r.GEEnterprises.CNPJCPF.ToUpper().Contains(pFilter.Trim()) ||
                        r.GEEnterprises.IE.ToUpper().Contains(pFilter.Trim()) ||
                        r.GEEnterprises.ClassifEmpresa.ToUpper().Contains(pFilter.Trim())
                    )
                );
            else if (!pFilter.IsEmpty() && !pColumn.IsEmpty())
                return GetColumnsFilterClassification(pColumn, pFilter.Trim(), pUserId, pEstablishmentKey, pClassification);

            return r => (r.EstablishmentKey == pEstablishmentKey &&
                     r.UserId == pUserId &&
                     r.GEEnterprises.EstablishmentKey == pEstablishmentKey &&
                     r.GEEnterprises.Classification == pClassification);
        }

    }

    public class GEUserEnterprisesListVM
    {
        public GEUserEnterprisesVM[] results { get; set; }
    }
}
