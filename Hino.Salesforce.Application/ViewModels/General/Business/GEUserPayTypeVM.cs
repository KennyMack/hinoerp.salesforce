using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;

namespace Hino.Salesforce.Application.ViewModels.General.Business
{
    [EndPoint("General/Business/User/PaymentType/{pEstablishmentKey}")]
    public class GEUserPayTypeVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        [ForeignKey("GEPaymentType")]
        public long PaymentTypeId { get; set; }
        public virtual GEPaymentTypeVM GEPaymentType { get; set; }
        
        [DisplayField]
        [RequiredField]
        [ForeignKey("GEUsers")]
        public long UserId { get; set; }
        public virtual GEUsersVM GEUsers { get; set; }
    }

    public class GEUserPayTypeListVM
    {
        public GEUserPayTypeVM[] results { get; set; }
    }
}
