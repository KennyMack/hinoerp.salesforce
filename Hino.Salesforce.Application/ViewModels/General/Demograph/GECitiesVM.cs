﻿using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General.Demograph
{
    [EndPoint("General/Demograph/Cities/{pEstablishmentKey}")]
    public class GECitiesVM : BaseVM
    {
        public string Name { get; set; }
        public string IBGE { get; set; }
        public string DDD { get; set; }

        [ForeignKey("GEStates")]
        public long StateID { get; set; }
        public virtual GEStatesVM GEStates { get; set; }

        public long IdERP { get; set; }

        public static Expression<Func<GECities, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
