﻿using System.Collections.Generic;

namespace Hino.Salesforce.Application.ViewModels.General.Demograph
{
    public class GECountriesLoadVM
    {
        public GECountriesLoadVM()
        {
            callingCodes = new List<string>();
            latlng = new List<decimal>();
            translations = new GECountriesLoadTranslationsVM();
        }

        public string name { get; set; }
        public string alpha2Code { get; set; }
        public string alpha3Code { get; set; }
        public string numericCode { get; set; }
        public string flag { get; set; }
        public List<string> callingCodes { get; set; }
        public List<decimal> latlng { get; set; }
        public GECountriesLoadTranslationsVM translations { get; set; }
    }

    public class GECountriesLoadTranslationsVM
    {
        public string de { get; set; }
        public string es { get; set; }
        public string fr { get; set; }
        public string ja { get; set; }
        public string it { get; set; }
        public string br { get; set; }
        public string pt { get; set; }
        public string nl { get; set; }
        public string hr { get; set; }
        public string fa { get; set; }
    }

    public class CountryBacen
    {
        public string bacen { get; set; }
        public string description { get; set; }
    }
}
