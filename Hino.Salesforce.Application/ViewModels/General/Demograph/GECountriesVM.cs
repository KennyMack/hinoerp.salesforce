﻿using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.ComponentModel.DataAnnotations;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General.Demograph
{
    [EndPoint("General/Demograph/Countries/{pEstablishmentKey}")]
    public class GECountriesVM : BaseVM
    {
        [Required]
        [MinLength(2, ErrorMessage = "Deve ter no min. 3 caracteres")]
        [MaxLength(3, ErrorMessage = "Deve ter no max. 3 caracteres")]
        [Display(Name = "Sigla")]
        public string Initials { get; set; }
        [Required]
        [MaxLength(120, ErrorMessage = "Deve ter no máx. 120 caracteres")]
        [Display(Name = "Nome")]
        public string Name { get; set; }
        [Display(Name = "BACEN")]
        //[Required]
        [MaxLength(5, ErrorMessage = "Deve ter no máx. 5 caracteres")]
        public string BACEN { get; set; }
        [Display(Name = "DDI")]
        [MaxLength(5, ErrorMessage = "Deve ter no máx. 5 caracteres")]
        public string DDI { get; set; }

        [DisplayField("CountryLocalName")]
        [Max120LengthField]
        public string LocalName { get; set; }

        [DisplayField]
        [Max10LengthField]
        public string CIOC { get; set; }

        [DisplayField]
        [Max10LengthField]
        public string NumericCode { get; set; }

        [DisplayField]
        public decimal Lat { get; set; }

        [DisplayField]
        public decimal Lng { get; set; }

        [DisplayField]
        public string flag { get; set; }

        public long IdERP { get; set; }

        public static Expression<Func<GECountries, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
