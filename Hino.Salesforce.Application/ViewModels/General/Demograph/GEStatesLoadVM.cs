﻿using System.Collections.Generic;

namespace Hino.Salesforce.Application.ViewModels.General.Demograph
{
    public class GEStatesLoadVM
    {
        public GEStatesLoadBodyVM body { get; set; }
    }

    public class GEStatesLoadBodyVM
    {
        public GEStatesLoadBodyVM()
        {
            data = new List<GEStatesLoadContentVM>();
        }

        public List<GEStatesLoadContentVM> data { get; set; }
    }

    public class GEStatesLoadContentVM
    {
        public string countryCode { get; set; }
        public string fipsCode { get; set; }
        public string isoCode { get; set; }
        public string name { get; set; }
        public string wikiDataId { get; set; }
    }
}
