﻿using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General.Demograph
{
    [EndPoint("General/Demograph/States/{pEstablishmentKey}")]
    public class GEStatesVM : BaseVM
    {
        public string Name { get; set; }
        public string Initials { get; set; }
        public string IBGE { get; set; }
        public string CodeFIPS { get; set; }
        public decimal Lat { get; set; }
        public decimal Lng { get; set; }

        [ForeignKey("GECountries")]
        public long CountryID { get; set; }
        public virtual GECountriesVM GECountries { get; set; }

        public long IdERP { get; set; }

        public static Expression<Func<GEStates, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
