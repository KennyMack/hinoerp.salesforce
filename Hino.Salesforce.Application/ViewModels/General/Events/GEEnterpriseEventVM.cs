using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;

namespace Hino.Salesforce.Application.ViewModels.General.Events
{
    public class GEEnterpriseEventVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public long EventID { get; set; }
        public GEEventsVM GEEvents { get; set; }

        [DisplayField]
        [RequiredField]
        public long EnterpriseID { get; set; }
        public GEEnterprisesVM GEEnterprises { get; set; }
    }
}
