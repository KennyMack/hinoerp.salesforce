using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;

namespace Hino.Salesforce.Application.ViewModels.General.Events
{
    public class GEEventsClassificationVM : BaseVM
    {
        public GEEventsClassificationVM()
        {
            GEEvents = new HashSet<GEEventsVM>();
        }

        [DisplayField]
        [RequiredField]
        [Max60LengthField]
        public string Description { get; set; }

        public ICollection<GEEventsVM> GEEvents { get; set; }
    }
}
