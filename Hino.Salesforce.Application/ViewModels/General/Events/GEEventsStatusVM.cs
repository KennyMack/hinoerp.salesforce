﻿using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.ViewModels.General.Events
{
    public class GEEventsStatusVM
    {
        [RequiredField]
        [DisplayField]
        public long Id { get; set; }
        [Min36LengthField]
        [Max36LengthField]
        [RequiredField]
        [DisplayField]
        public string EstablishmentKey { get; set; }
        [RequiredField]
        [Min36LengthField]
        [Max36LengthField]
        [DisplayField]
        public string UniqueKey { get; set; }
        [DisplayField]
        [RequiredField]
        public bool IsComplete { get; set; }
        [DisplayField]
        [RequiredField]
        public bool IsSuccess { get; set; }
    }

    public class GEEventsChildVM
    {
        [RequiredField]
        [DisplayField]
        public long Id { get; set; }
        [Min36LengthField]
        [Max36LengthField]
        [RequiredField]
        [DisplayField]
        public string EstablishmentKey { get; set; }
        [RequiredField]
        [Min36LengthField]
        [Max36LengthField]
        [DisplayField]
        public string UniqueKey { get; set; }
        [DisplayField]
        [RequiredField]
        public bool IsComplete { get; set; }
        [DisplayField]
        [RequiredField]
        public bool IsSuccess { get; set; }

        [RequiredField]
        public GEEventsVM Event { get; set; }

    }
}
