using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using Hino.Salesforce.Infra.Cross.Utils.Enums;

namespace Hino.Salesforce.Application.ViewModels.General.Events
{
    public class GEEventsVM : BaseVM
    {
        public GEEventsVM()
        {
            this.GEUserCalendar = new HashSet<GEUserCalendarVM>();
            this.GEEstabCalendar = new HashSet<GEEstabCalendarVM>();
            this.GEEnterpriseEvent = new HashSet<GEEnterpriseEventVM>();
        }

        [DisplayField]
        [RequiredField]
        public EEventType Type { get; set; }
        [DisplayField("EventTitle")]
        [RequiredField]
        public string Title { get; set; }
        [DisplayField]
        public string Description { get; set; }
        [DisplayField]
        [RequiredField]
        public DateTime DtCalendar { get; set; }
        [DisplayField("StartEvent")]
        public DateTime? Start { get; set; }
        [DisplayField("EndEvent")]
        public DateTime? End { get; set; }
        [DisplayField]
        public string ZipCode { get; set; }
        [DisplayField]
        public string Address { get; set; }
        [DisplayField]
        public string District { get; set; }
        [DisplayField]
        public string Num { get; set; }
        [DisplayField]
        public string Complement { get; set; }
        [DisplayField]
        public decimal DisplayLat { get; set; }
        [DisplayField]
        public decimal DisplayLng { get; set; }
        [DisplayField]
        public decimal NavLat { get; set; }
        [DisplayField]
        public decimal NavLng { get; set; }
        [DisplayField]
        public string CityName { get; set; }
        [DisplayField]
        public string StateName { get; set; }
        [DisplayField]
        public string UF { get; set; }
        [DisplayField]
        public string IBGE { get; set; }
        [DisplayField]
        public string Email { get; set; }
        [DisplayField]
        public string Phone { get; set; }
        [DisplayField("EventClassificationID")]
        public long? ClassificationID { get; set; }
        public GEEventsClassificationVM GEEventsClassification { get; set; }
        [DisplayField]
        [RequiredField]
        public bool Priority { get; set; }
        [DisplayField]
        public long? MainEventID { get; set; }
        [DisplayField]
        public long? OriginEventID { get; set; }
        [DisplayField]
        public bool IsComplete { get; set; }
        [DisplayField]
        public bool IsSuccess { get; set; }

        [DisplayField]
        public string Contact { get; set; }
        [DisplayField]
        public EContactSectors? Sector { get; set; }

        public ICollection<GEUserCalendarVM> GEUserCalendar { get; set; }
        public ICollection<GEEstabCalendarVM> GEEstabCalendar { get; set; }
        public ICollection<GEEnterpriseEventVM> GEEnterpriseEvent { get; set; }
    }

    public class GECreateEventsVM: GEEventsVM
    {
        public long? EstabID { get; set; }
        public long? EnterpriseID { get; set; }
        public long? UserID { get; set; }
    }
}
