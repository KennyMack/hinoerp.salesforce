using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;

namespace Hino.Salesforce.Application.ViewModels.General.Events
{
    public class GEUserCalendarVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public long UserID { get; set; }
        public GEUsersVM GEUsers { get; set; }
        [DisplayField]
        [RequiredField]
        public long EventID { get; set; }
        public GEEventsVM GEEvents { get; set; }
    }
}
