using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General
{
    public class GEEstAtvRtVigVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public DateTime Dtinicio { get; set; }
        [DisplayField]
        [RequiredField]
        public long CodAtividade { get; set; }
        [DisplayField]
        [RequiredField]
        public long CodRegTrib { get; set; }
        [DisplayField]
        public long? CodCfgSimples { get; set; }
        [DisplayField]
        public decimal? VlIniRecbrSimples { get; set; }
        [DisplayField]
        public decimal? VlFimRecbrSimples { get; set; }
        [DisplayField]
        [RequiredField]
        public bool MajoraICMS { get; set; }

        public static Expression<Func<GEEstAtvRtVig, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
