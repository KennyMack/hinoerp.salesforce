﻿using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General
{
    public class GEEstabDevicesVM : BaseVM
    {
        [RequiredField]
        [Min36LengthField]
        [Max36LengthField]
        [DisplayField]
        public string UserKey { get; set; }
        [RequiredField]
        [DisplayField]
        [Min8LengthField]
        [Max120LengthField]
        public string NickName { get; set; }

        [RequiredField]
        [ForeignKey("GEEstabDevices")]
        [DisplayField("EstabID")]
        public long GEEstabID { get; set; }

        public virtual GEEstablishmentsVM GEEstablishments { get; set; }

        public static Expression<Func<GEEstabDevices, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
