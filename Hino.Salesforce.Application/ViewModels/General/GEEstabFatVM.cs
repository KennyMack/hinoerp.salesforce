using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General
{
    public class GEEstabFatVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public long EstabId { get; set; }
        [DisplayField]
        [RequiredField]
        public DateTime Period { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal Value { get; set; }

        public static Expression<Func<GEEstabFat, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
