using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General
{
    public class GEEstabMenuVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        [IntegerRangeField(0, 99)]
        public short Menu { get; set; }
        [DisplayField]
        [RequiredField]
        [Min8LengthField]
        [Max120LengthField]
        public string Name { get; set; }
        [DisplayField]
        [RequiredField]
        [Min8LengthField]
        [Max120LengthField]
        public string Description { get; set; }
        [DisplayField]
        [RequiredField]
        [DefaultValue(true)]
        public bool Visible { get; set; }

        [RequiredField]
        [ForeignKey("GEEstabMenu")]
        [DisplayField("EstabID")]
        public long GEEstabID { get; set; }

        public virtual GEEstablishmentsVM GEEstablishments { get; set; }

        public static Expression<Func<GEEstabMenu, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
