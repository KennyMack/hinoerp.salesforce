﻿using Hino.Salesforce.Application.ViewModels.Fiscal;
using Hino.Salesforce.Application.ViewModels.General.Events;
using Hino.Salesforce.Application.ViewModels.General.Kanban;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General
{
    [EndPoint("General/Establishments/{pEstablishmentKey}")]
    public class GEEstablishmentsVM : BaseVM
    {
        public GEEstablishmentsVM()
        {

            this.GEEstabDevices = new HashSet<GEEstabDevicesVM>();
            this.GEEstabFat = new HashSet<GEEstabFatVM>();
            this.GEEstabPay = new HashSet<GEEstabPayVM>();
            this.GEEstabMenu = new HashSet<GEEstabMenuVM>();
            this.GESettings = new HashSet<GESettingsVM>();
            this.GEEstabCalendar = new HashSet<GEEstabCalendarVM>();
        }

        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CNPJCPF { get; set; }
        public string TokenCNPJ { get; set; }
        public decimal PIS { get; set; }
        public decimal COFINS { get; set; }
        public int Devices { get; set; }
        public bool AllowEnterprise { get; set; }
        public bool AllowPayment { get; set; }
        public bool AllowChangePrice { get; set; }
        public bool AllowDiscount { get; set; }
        public long? EstabGroup { get; set; }
        public bool FatorR { get; set; }
        public string CapptaKey { get; set; }
        public string SitefIp { get; set; }
        public string AditionalInfo { get; set; }
        public string DefaultNoteOrder { get; set; }
        public bool OnlyWithStock { get; set; }
        public bool OnlyOnDate { get; set; }
        public int DaysPayment { get; set; }
        public short SearchPosition { get; set; }

        public bool UseChangePrice { get; set; }
        public bool UsePayCondition { get; set; }
        public bool UseBonification { get; set; }
        public bool UseSample { get; set; }

        [ForeignKey("PfFiscalGroup")]
        public long? PfFiscalGroupId { get; set; }
        public virtual GEEnterpriseFiscalGroup PfFiscalGroup { get; set; }

        [ForeignKey("PjFiscalGroup")]
        public long? PjFiscalGroupId { get; set; }
        public virtual GEEnterpriseFiscalGroup PjFiscalGroup { get; set; }

        public string PfClassifClient { get; set; }
        public string PjClassifClient { get; set; }

        [ForeignKey("FSFiscalOper")]
        public long? DefaultFiscalOperID { get; set; }
        public virtual FSFiscalOperVM FSFiscalOper { get; set; }

        [ForeignKey("DefaultProduct")]
        public long? DefaultProductId { get; set; }
        public virtual GEProducts DefaultProduct { get; set; }

        public virtual ICollection<GEEstabDevicesVM> GEEstabDevices { get; set; }
        public virtual ICollection<GEEstabFatVM> GEEstabFat { get; set; }
        public virtual ICollection<GEEstabPayVM> GEEstabPay { get; set; }
        public virtual ICollection<GEEstabMenuVM> GEEstabMenu { get; set; }
        public virtual ICollection<GESettingsVM> GESettings { get; set; }
        public virtual ICollection<GEEstabCalendarVM> GEEstabCalendar { get; set; }

        public static Expression<Func<GEEstablishments, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }

    public class GEEstablishmentsCreateVM
    {
        public GEEstablishmentsCreateVM()
        {
            this.GEEstabMenu = new HashSet<GEEstabMenuVM>();
        }

        [RequiredField]
        [Max60LengthField]
        [DisplayField]
        public string RazaoSocial { get; set; }
        [RequiredField]
        [Max60LengthField]
        [DisplayField]
        public string NomeFantasia { get; set; }
        [RequiredField]
        [Max120LengthField]
        [EmailAddress(
            ErrorMessageResourceName = "InvalidEmail",
            ErrorMessageResourceType = typeof(Hino.Salesforce.Infra.Cross.Resources.ValidationMessagesResource))]
        [DisplayField]
        public string Email { get; set; }
        [RequiredField]
        [Max20LengthField]
        [Phone(
            ErrorMessageResourceName = "PhoneInvalid",
            ErrorMessageResourceType = typeof(Hino.Salesforce.Infra.Cross.Resources.ValidationMessagesResource))]
        [DisplayField]
        public string Phone { get; set; }
        [RequiredField]
        [Max20LengthField]
        [DisplayField]
        public string CNPJCPF { get; set; }
        [DisplayField]
        [Max255LengthField]
        public string TokenCNPJ { get; set; }
        [RequiredField]
        [IntegerRangeField(1, 999)]
        [DisplayField]
        public int Devices { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal PIS { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal COFINS { get; set; }
        public long Id { get; set; }
        public string EstablishmentKey { get; set; }
        public string UniqueKey { get; set; }
        public bool IsActive { get; set; }
        [DisplayField]
        [RequiredField]
        public bool AllowEnterprise { get; set; }
        [DisplayField]
        [RequiredField]
        public bool AllowPayment { get; set; }
        [DisplayField]
        [RequiredField]
        public bool AllowChangePrice { get; set; }
        [DisplayField]
        [RequiredField]
        public bool AllowDiscount { get; set; }
        [DisplayField]
        public long? EstabGroup { get; set; }
        [DisplayField]
        [RequiredField]
        public bool FatorR { get; set; }
        [DisplayField]
        public string CapptaKey { get; set; }
        [DisplayField]
        public string SitefIp { get; set; }
        [DisplayField]
        public string AditionalInfo { get; set; }
        [DisplayField]
        public string DefaultNoteOrder { get; set; }
        [DisplayField]
        public bool OnlyWithStock { get; set; }
        [DisplayField]
        public bool OnlyOnDate { get; set; }
        [DisplayField]
        [DefaultValue(0)]
        public int DaysPayment { get; set; }
        [DisplayField]
        [DefaultValue(0)]
        public short SearchPosition { get; set; }

        [DisplayField]
        [DefaultValue(false)]
        public bool UseChangePrice { get; set; }

        [DisplayField]
        [DefaultValue(false)]
        public bool UsePayCondition { get; set; }

        [DisplayField]
        [DefaultValue(false)]
        public bool UseBonification { get; set; }

        [DisplayField]
        [DefaultValue(false)]
        public bool UseSample { get; set; }

        [DisplayField]
        [ForeignKey("FSFiscalOper")]
        public long? DefaultFiscalOperID { get; set; }
        public virtual FSFiscalOperVM FSFiscalOper { get; set; }

        [ForeignKey("PfFiscalGroup")]
        public long? PfFiscalGroupId { get; set; }
        public virtual GEEnterpriseFiscalGroup PfFiscalGroup { get; set; }

        [ForeignKey("PjFiscalGroup")]
        public long? PjFiscalGroupId { get; set; }
        public virtual GEEnterpriseFiscalGroup PjFiscalGroup { get; set; }

        public string PfClassifClient { get; set; }
        public string PjClassifClient { get; set; }

        [ForeignKey("DefaultProduct")]
        public long? DefaultProductId { get; set; }
        public virtual GEProducts DefaultProduct { get; set; }

        public virtual ICollection<GEEstabMenuVM> GEEstabMenu { get; set; }
    }
}
