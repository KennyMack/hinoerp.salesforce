using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General
{
    public class GEFilesPathVM : BaseVM
    {
        [ForeignKey("GEEstablishments")]
        [DisplayField("EstabID")]
        public long? GEEstabID { get; set; }

        [ForeignKey("GEEnterprises")]
        [DisplayField]
        public long? EnterpriseID { get; set; }

        [ForeignKey("GEUsers")]
        [DisplayField]
        public long? UserID { get; set; }

        [ForeignKey("GEProducts")]
        [DisplayField]
        public long? ProductID { get; set; }

        [DisplayField]
        [RequiredField]
        public string Name { get; set; }

        [DisplayField]
        [RequiredField]
        public string Path { get; set; }

        [DisplayField]
        public string Description { get; set; }

        [DisplayField]
        [RequiredField]
        [DefaultValue(0)]
        public long FileSize { get; set; }

        [DisplayField]
        [RequiredField]
        public string Extension { get; set; }

        [DisplayField]
        [RequiredField]
        public string MimeType { get; set; }

        [DisplayField]
        [RequiredField]
        public string Identifier { get; set; }

        public virtual GEEstablishmentsVM GEEstablishments { get; set; }
        public virtual GEEnterprisesVM GEEnterprises { get; set; }
        public virtual GEUsersVM GEUsers { get; set; }
        public virtual GEProductsVM GEProducts { get; set; }

        public static Expression<Func<GEFilesPathVM, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
