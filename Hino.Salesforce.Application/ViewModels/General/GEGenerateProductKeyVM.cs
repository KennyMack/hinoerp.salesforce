﻿using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.ViewModels.General
{
    public class GEGenerateProductKeyVM : BaseVM
    {
        [RequiredField]
        public string Family { get; set; }
        [RequiredField]
        public string Type { get; set; }
        public string Reference { get; set; }
        public string ProductKey { get; set; }
    }
}
