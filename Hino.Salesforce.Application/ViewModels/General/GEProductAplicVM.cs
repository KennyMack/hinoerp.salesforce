using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General
{
    public class GEProductAplicVM : BaseVM
    {

        [DisplayField]
        [RequiredField]
        public string Description { get; set; }

        [DisplayField]
        [RequiredField]
        public int Classification { get; set; }

        [DisplayField]
        [RequiredField]
        public long IdERP { get; set; }

        public static Expression<Func<GEProductAplic, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => r.EstablishmentKey == pEstablishmentKey &&
                    r.Description.ToUpper().Contains(pFilter.Trim());

            return r => r.EstablishmentKey == pEstablishmentKey;
        }
    }
}
