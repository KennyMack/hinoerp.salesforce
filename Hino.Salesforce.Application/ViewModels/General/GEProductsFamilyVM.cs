﻿using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General
{
    [EndPoint("General/Products/Family/{pEstablishmentKey}")]
    public class GEProductsFamilyVM : BaseVM
    {
        [RequiredField]
        [DisplayField("Family")]
        public string Family { get; set; }
        [RequiredField]
        [DisplayField("FamilyDescription")]
        public string Description { get; set; }
        [DisplayField("GroupDescription")]
        public string GroupDescription { get; set; }
        [DisplayField("ClassDescription")]
        public string ClassDescription { get; set; }
        [DisplayField("CatDescription")]
        public string CatDescription { get; set; }

        public string GetDescription
        {
            get
            {
                return Description + (GroupDescription.IsEmpty() ? "" : $"\\{GroupDescription}")
                    + (ClassDescription.IsEmpty() ? "" : $"\\{ClassDescription}")
                    + (CatDescription.IsEmpty() ? "" : $"\\{CatDescription}");
            }
        }

        public static Expression<Func<GEProductsFamily, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                (
                    (
                        r.Description.ToUpper().Contains(pFilter.Trim()) ||
                        r.GroupDescription.ToUpper().Contains(pFilter.Trim()) ||
                        r.ClassDescription.ToUpper().Contains(pFilter.Trim()) ||
                        r.CatDescription.ToUpper().Contains(pFilter.Trim())
                    )
                );

            return r => r.EstablishmentKey == pEstablishmentKey;
        }
    }
}
