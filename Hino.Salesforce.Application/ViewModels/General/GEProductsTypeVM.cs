﻿using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General
{
    [EndPoint("General/Products/Type/{pEstablishmentKey}")]
    public class GEProductsTypeVM : BaseVM
    {
        [RequiredField]
        [DisplayField("TypeProd")]
        public string TypeProd { get; set; }
        [RequiredField]
        [DisplayField("TypeDescription")]
        public string Description { get; set; }

        [DisplayField]
        public int? CodAnexoSimples { get; set; }

        public string GetDescription => $"{Description}";

        public static Expression<Func<GEProductsType, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                (
                    (
                        r.Description.ToUpper().Contains(pFilter.Trim())
                    )
                );

            return r => r.EstablishmentKey == pEstablishmentKey;
        }
    }
}
