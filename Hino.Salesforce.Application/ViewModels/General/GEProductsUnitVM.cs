﻿using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General
{
    [EndPoint("General/Products/Unit/{pEstablishmentKey}")]
    public class GEProductsUnitVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public string Unit { get; set; }
        [DisplayField("UnitDescription")]
        [RequiredField]
        public string Description { get; set; }

        public string GetDescription => $"{Unit} - {Description}";

        public static Expression<Func<GEProductsUnit, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                (
                    (
                        r.Description.ToUpper().Contains(pFilter.Trim()) ||
                        r.Unit.ToUpper().Contains(pFilter.Trim())
                    )
                );

            return r => r.EstablishmentKey == pEstablishmentKey;
        }
    }
}
