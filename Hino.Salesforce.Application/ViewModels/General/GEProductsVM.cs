﻿using Hino.Salesforce.Application.ViewModels.Fiscal.Taxes;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General
{
    [EndPoint("General/Products/{pEstablishmentKey}")]
    public class GEProductsVM : BaseVM
    {
        public GEProductsVM()
        {
            this.GEFilesPath = new HashSet<GEFilesPath>();
        }

        [RequiredField]
        [DisplayField]
        public string ProductKey { get; set; }
        public string Image { get; set; }
        [RequiredField]
        [DisplayField]
        public string Name { get; set; }
        [RequiredField]
        [DisplayField]
        public string Description { get; set; }

        public string GetDescription
        {
            get => $"{ProductKey} - {Name}";
        }

        [RequiredField]
        [DisplayField("TypeId")]
        [LongRangeField(1, long.MaxValue)]
        [ForeignKey("GEProductsType")]
        public long TypeId { get; set; }
        public virtual GEProductsTypeVM GEProductsType { get; set; }

        public string TypeProd { get; set; }

        [RequiredField]
        [DisplayField("FamilyId")]
        [LongRangeField(1, long.MaxValue)]
        [ForeignKey("GEProductsType")]
        public long FamilyId { get; set; }

        public string Family { get; set; }

        public virtual GEProductsFamilyVM GEProductsFamily { get; set; }

        [RequiredField]
        [DisplayField]
        public double PercIPI { get; set; }
        [RequiredField]
        [DisplayField]
        public double PercMaxDiscount { get; set; }
        [RequiredField]
        [DisplayField]
        public double Value { get; set; }
        [RequiredField]
        [DisplayField]
        public double StockBalance { get; set; }
        [RequiredField]
        [IntegerRangeField(0, 4)]
        [DisplayField]
        public int Status { get; set; }
        [RequiredField]
        [DisplayField]
        public string NCM { get; set; }

        [ForeignKey("FSNCM")]
        [DisplayField]
        [RequiredField]
        [LongGreaterThan(0)]
        public long? NCMId { get; set; }
        public virtual FSNCMVM FSNCM { get; set; }

        [DisplayField]
        public long? UnitId { get; set; }
        public virtual GEProductsUnitVM GEProductsUnit { get; set; }

        public string Unit { get; set; }

        public string GetUnit => GEProductsUnit != null ? $"{GEProductsUnit?.Unit} - {GEProductsUnit?.Description}" : "";

        [DisplayField]
        public decimal Weight { get; set; }
        [DisplayField]
        public decimal PackageQTD { get; set; }
        [DisplayField]
        public long? SaleUnitId { get; set; }
        public virtual GEProductsUnitVM GEProductsSaleUnit { get; set; }

        public string SaleUnit { get; set; }

        public string GetSaleUnit => GEProductsSaleUnit != null ? $"{GEProductsSaleUnit?.Unit} - {GEProductsSaleUnit?.Description}" : "";

        [DisplayField]
        public decimal SaleFactor { get; set; }

        public int CodOrigMerc { get; set; }

        public long? AplicationId { get; set; }
        public virtual GEProductAplicVM GEProductAplic { get; set; }

        public string AditionalInfo { get; set; }

        public bool ProductKeyGenerated { get; set; }
        public string Reference { get; set; }
        public bool IncludeInSalePrice { get; set; }
        public string Search { get; set; }

        public virtual ICollection<GEFilesPath> GEFilesPath { get; set; }

        public static Expression<Func<GEProducts, bool>> GetColumnsFilter(string pColumn, string pFilter, string pEstablishmentKey, string pSearchPosition)
        {
            var clearedFilter = pFilter.RemoveAccent().Trim().ToUpper();
            if (pSearchPosition == "0")
            {
                switch (pColumn)
                {
                    case "Id":
                        return r => r.Id.ToString().Contains(clearedFilter) && r.EstablishmentKey == pEstablishmentKey;
                    case "AditionalInfo":
                        return r => r.AditionalInfo.ToUpper().Contains(clearedFilter) && r.EstablishmentKey == pEstablishmentKey;
                    case "ProductKey":
                        return r => r.ProductKey.ToUpper().Contains(clearedFilter) && r.EstablishmentKey == pEstablishmentKey;
                    case "Name":
                        return r => r.Search.Contains(clearedFilter) && r.EstablishmentKey == pEstablishmentKey;
                    case "NCM":
                        return r => r.NCM.Contains(clearedFilter) && r.EstablishmentKey == pEstablishmentKey;
                    case "GEProductsType.Description":
                        return r => r.GEProductsType.Description.ToUpper().Contains(clearedFilter) && r.EstablishmentKey == pEstablishmentKey;
                    case "GEProductsFamily.Description":
                        return r => r.GEProductsFamily.Description.ToUpper().Contains(clearedFilter) && r.EstablishmentKey == pEstablishmentKey;
                };
            }
            else
            {
                switch (pColumn)
                {
                    case "Id":
                        return r => r.Id.ToString().StartsWith(clearedFilter) && r.EstablishmentKey == pEstablishmentKey;
                    case "AditionalInfo":
                        return r => r.AditionalInfo.ToUpper().StartsWith(clearedFilter) && r.EstablishmentKey == pEstablishmentKey;
                    case "ProductKey":
                        return r => r.ProductKey.ToUpper().StartsWith(clearedFilter) && r.EstablishmentKey == pEstablishmentKey;
                    case "Name":
                        return r => r.Search.StartsWith(clearedFilter) && r.EstablishmentKey == pEstablishmentKey;
                    case "NCM":
                        return r => r.NCM.StartsWith(clearedFilter) && r.EstablishmentKey == pEstablishmentKey;
                    case "GEProductsType.Description":
                        return r => r.GEProductsType.Description.ToUpper().StartsWith(clearedFilter) && r.EstablishmentKey == pEstablishmentKey;
                    case "GEProductsFamily.Description":
                        return r => r.GEProductsFamily.Description.ToUpper().StartsWith(clearedFilter) && r.EstablishmentKey == pEstablishmentKey;
                };
            }

            return r => r.EstablishmentKey == pEstablishmentKey;
        }

        public static Expression<Func<GEProducts, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter, string pColumn, string pSearchPosition)
        {
            var clearedFilter = pFilter.RemoveAccent().ToUpper();

            if (!pFilter.IsEmpty() && pColumn.IsEmpty() && pSearchPosition == "0")
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                (
                    (
                        (r.Search.Contains(clearedFilter.Trim())) ||
                        (r.ProductKey.ToUpper().Contains(clearedFilter.Trim())) ||
                        (r.NCM.ToUpper().StartsWith(clearedFilter.Trim())) ||
                        (r.GEProductsUnit.Unit.ToUpper().Contains(clearedFilter.Trim()))
                    )
                );
            else if (!pFilter.IsEmpty() && pColumn.IsEmpty() && pSearchPosition == "1")
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                (
                    (
                        (r.Search.Contains(clearedFilter.Trim())) ||
                        (r.ProductKey.ToUpper().StartsWith(clearedFilter.Trim())) ||
                        (r.NCM.ToUpper().StartsWith(clearedFilter.Trim())) ||
                        (r.GEProductsUnit.Unit.ToUpper().StartsWith(clearedFilter.Trim()))
                    )
                );
            else if (!pFilter.IsEmpty() && !pColumn.IsEmpty())
                return GetColumnsFilter(pColumn, pFilter.Trim(), pEstablishmentKey, pSearchPosition);

            return r => r.EstablishmentKey == pEstablishmentKey;
        }
    }

    public class GEProductsSearchVM
    {
        [Key]
        [DisplayField]
        public long Id { get; set; }
        [RequiredField]
        [DisplayField]
        public string ProductKey { get; set; }
        [RequiredField]
        [DisplayField]
        public string Name { get; set; }
        [RequiredField]
        [DisplayField]
        public string Description { get; set; }

        public string GetDescription
        {
            get => $"{ProductKey} - {Name}";
        }
    }

    public class GEMainProductsVM
    {
        [Key]
        [DisplayField]
        public long Id { get; set; }
        [RequiredField]
        [DisplayField]
        public string ProductKey { get; set; }
        [RequiredField]
        [DisplayField]
        public string Name { get; set; }
        [RequiredField]
        [DisplayField]
        public string Description { get; set; }

        public string GetDescription
        {
            get => $"{ProductKey} - {Name}";
        }

        public string GEProductsTypeDescription => GEProductsType.Description;
        public string GEProductsFamilyDescription => GEProductsFamily.Description;

        [RequiredField]
        [DisplayField("TypeId")]
        [LongRangeField(1, long.MaxValue)]
        [ForeignKey("GEProductsType")]
        public long TypeId { get; set; }
        [JsonIgnore]
        public virtual GEProductsTypeVM GEProductsType { get; set; }

        [RequiredField]
        [DisplayField("FamilyId")]
        [LongRangeField(1, long.MaxValue)]
        [ForeignKey("GEProductsType")]
        public long FamilyId { get; set; }

        [JsonIgnore]
        public virtual GEProductsFamilyVM GEProductsFamily { get; set; }

        public string AditionalInfo { get; set; }
        [DisplayField]
        public DateTime Created { get; set; }
        [DisplayField]
        public DateTime Modified { get; set; }


        [DisplayField]
        public long? UnitId { get; set; }
        [JsonIgnore]
        public virtual GEProductsUnitVM GEProductsUnit { get; set; }

        public string Unit { get; set; }

        public string GetUnit => GEProductsUnit != null ? $"{GEProductsUnit?.Unit} - {GEProductsUnit?.Description}" : "";

        [DisplayField]
        public decimal Weight { get; set; }
        [DisplayField]
        public decimal PackageQTD { get; set; }
        [RequiredField]
        [IntegerRangeField(0, 4)]
        [DisplayField]
        public int Status { get; set; }

        [RequiredField]
        [DisplayField]
        public string NCM { get; set; }
        [DisplayField]
        public decimal PercIpi { get; set; }
    }
}
