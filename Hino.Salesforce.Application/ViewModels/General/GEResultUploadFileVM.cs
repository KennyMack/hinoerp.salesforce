﻿using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System.Collections.Generic;

namespace Hino.Salesforce.Application.ViewModels.General
{
    public class GEResultUploadFileVM
    {
        public GEResultUploadFileVM()
        {
            Files = new List<GEResulUploadItemVM>();
        }

        public List<GEResulUploadItemVM> Files { get; set; }
    }

    public class GEResulUploadItemVM
    {
        [RequiredField]
        public string FileName { get; set; }
        [RequiredField]
        public string Identifier { get; set; }
        [RequiredField]
        public string Path { get; set; }
        public bool Success { get; set; }
        public string MimeType { get; set; }
        public string Extension { get; set; }
        public long FileSize { get; set; }
        public string ErrorMessage { get; set; }
    }
}
