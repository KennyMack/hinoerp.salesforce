﻿using Hino.Salesforce.Application.ViewModels.General.Events;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General
{
    [EndPoint("General/Users/{pEstablishmentKey}")]
    public class GEUsersVM : BaseVM
    {
        public GEUsersVM()
        {
            this.VEOrders = new HashSet<VEOrdersVM>();
            this.GEFilesPath = new HashSet<GEFilesPathVM>();
            this.GEUserCalendar = new HashSet<GEUserCalendarVM>();
        }

        [RequiredField]
        [DisplayField]
        public string UserName { get; set; }
        [RequiredField]
        [DisplayField]
        [Max120LengthField]
        public string Name { get; set; }
        [RequiredField]
        [DisplayField]
        [EmailAddress(
            ErrorMessageResourceName = "InvalidEmail",
            ErrorMessageResourceType = typeof(Infra.Cross.Resources.ValidationMessagesResource))]
        public string Email { get; set; }
        //[RequiredField]
        [DisplayField]
        public string Phone { get; set; }
        [RequiredField]
        [DisplayField]
        public string Password { get; set; }
        [DisplayField]
        public DateTime LastLogin { get; set; }
        [DisplayField]
        [IntegerRangeField(0, 6)]
        public EUserType UserType { get; set; }
        [RequiredField]
        [DisplayField]
        [DecimalRangeField(0, 99.99)]
        public decimal PercDiscount { get; set; }
        [RequiredField]
        [DisplayField]
        [DecimalRangeField(0, 99.99)]
        public decimal PercCommission { get; set; }
        [RequiredField]
        [DisplayField]
        public decimal PercDiscountPrice { get; set; }
        [RequiredField]
        [DisplayField]
        public decimal PercIncreasePrice { get; set; }
        [DisplayField]
        public string StoreId { get; set; }
        [DisplayField]
        public string TerminalId { get; set; }
        [DisplayField]
        [RequiredField]
        public bool IsBlockedByPay { get; set; }
        [DisplayField]
        [RequiredField]
        public float BoletoLimit { get; set; }
        [DisplayField]
        public string UserKey { get; set; }

        [NotMapped]
        public GEEstablishmentsVM GEEstablishments { get; set; }

        public ICollection<VEOrdersVM> VEOrders { get; set; }
        public ICollection<GEFilesPathVM> GEFilesPath { get; set; }
        public ICollection<GEUserCalendarVM> GEUserCalendar { get; set; }

        public static Expression<Func<GEUsers, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                (
                    (
                        r.Name.ToUpper().Contains(pFilter.Trim()) ||
                        r.Email.ToUpper().Contains(pFilter.Trim()) ||
                        r.UserName.ToUpper().Contains(pFilter.Trim())
                    )
                );

            return r => r.EstablishmentKey == pEstablishmentKey;
        }
    }

    public class GEUsersEditVM : BaseVM
    {
        public GEUsersEditVM()
        {
            this.VEOrders = new HashSet<VEOrdersVM>();
            this.GEFilesPath = new HashSet<GEFilesPathVM>();
            this.GEUserCalendar = new HashSet<GEUserCalendarVM>();
        }

        [RequiredField]
        [DisplayField]
        public string UserName { get; set; }
        [RequiredField]
        [DisplayField]
        [Max120LengthField]
        public string Name { get; set; }
        [RequiredField]
        [DisplayField]
        [EmailAddress(
            ErrorMessageResourceName = "InvalidEmail",
            ErrorMessageResourceType = typeof(Infra.Cross.Resources.ValidationMessagesResource))]
        public string Email { get; set; }
        //[RequiredField]
        [DisplayField]
        public string Phone { get; set; }
        [DisplayField]
        public DateTime LastLogin { get; set; }
        [DisplayField]
        [IntegerRangeField(0, 6)]
        public EUserType UserType { get; set; }
        [RequiredField]
        [DisplayField]
        [DecimalRangeField(0, 99.99)]
        public decimal PercDiscount { get; set; }
        [RequiredField]
        [DisplayField]
        [DecimalRangeField(0, 99.99)]
        public decimal PercCommission { get; set; }
        [RequiredField]
        [DisplayField]
        public decimal PercDiscountPrice { get; set; }
        [RequiredField]
        [DisplayField]
        public decimal PercIncreasePrice { get; set; }
        [DisplayField]
        public string StoreId { get; set; }
        [DisplayField]
        public string TerminalId { get; set; }
        [DisplayField]
        [RequiredField]
        public bool IsBlockedByPay { get; set; }
        [DisplayField]
        [RequiredField]
        public float BoletoLimit { get; set; }
        [DisplayField]
        public string UserKey { get; set; }

        [NotMapped]
        public GEEstablishmentsVM GEEstablishments { get; set; }

        public ICollection<VEOrdersVM> VEOrders { get; set; }
        public ICollection<GEFilesPathVM> GEFilesPath { get; set; }
        public ICollection<GEUserCalendarVM> GEUserCalendar { get; set; }

        public static Expression<Func<GEUsers, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                (
                    (
                        r.Name.ToUpper().Contains(pFilter.Trim()) ||
                        r.Email.ToUpper().Contains(pFilter.Trim()) ||
                        r.UserName.ToUpper().Contains(pFilter.Trim())
                    )
                );

            return r => r.EstablishmentKey == pEstablishmentKey;
        }
    }

    [EndPoint("General/Users/{pEstablishmentKey}")]
    public class GEUsersCreateVM : BaseVM
    {
        [RequiredField]
        [DisplayField]
        public string UserName { get; set; }
        [RequiredField]
        [DisplayField]
        [Max120LengthField]
        public string Name { get; set; }
        [RequiredField]
        [DisplayField]
        [EmailAddress(
            ErrorMessageResourceName = "InvalidEmail",
            ErrorMessageResourceType = typeof(Infra.Cross.Resources.ValidationMessagesResource))]
        public string Email { get; set; }
        //[RequiredField]
        [DisplayField]
        public string Phone { get; set; }
        [RequiredField]
        [DisplayField]
        public string Password { get; set; }
        [DisplayField]
        public DateTime LastLogin { get; set; }
        [DisplayField]
        [IntegerRangeField(0, 6)]
        public EUserType UserType { get; set; }
        [RequiredField]
        [DisplayField]
        [DecimalRangeField(0, 99.99)]
        public decimal PercDiscount { get; set; }
        [RequiredField]
        [DisplayField]
        [DecimalRangeField(0, 99.99)]
        public decimal PercCommission { get; set; }
        [RequiredField]
        [DisplayField]
        public decimal PercDiscountPrice { get; set; }
        [RequiredField]
        [DisplayField]
        public decimal PercIncreasePrice { get; set; }
        [DisplayField]
        public string StoreId { get; set; }
        [DisplayField]
        public string TerminalId { get; set; }
        [DisplayField]
        [RequiredField]
        public bool IsBlockedByPay { get; set; }
        [DisplayField]
        [RequiredField]
        public float BoletoLimit { get; set; }
        [DisplayField]
        [RequiredField]
        [Min36LengthField]
        [Max36LengthField]
        public string UserKey { get; set; }
        [DisplayField]
        [RequiredField]
        [Compare("Password",
            ErrorMessageResourceName = "PasswordNotEqual",
            ErrorMessageResourceType = typeof(Infra.Cross.Resources.ValidationMessagesResource))]
        public string PasswordConfirm { get; set; }

        [RequiredField]
        [DisplayField]
        public string OriginCreate { get; set; }
    }

    public class GEOrderUserVM
    {
        [DisplayField]
        public long Id { get; set; }

        [DisplayField]
        public string Name { get; set; }

        [DisplayField]
        public string Phone { get; set; }

        [DisplayField]
        public string UserName { get; set; }

        [DisplayField]
        public string Email { get; set; }

        [DisplayField]
        public EUserType UserType { get; set; }

        [DisplayField]
        public string UserKey { get; set; }
    }

    public class GEUsersPasswordVM
    {
        [DisplayField]
        [RequiredField]
        public long Id { get; set; }
        [RequiredField]
        [DisplayField]
        public string EstablishmentKey { get; set; }
        [RequiredField]
        [DisplayField]
        public string UniqueKey { get; set; }
        [RequiredField]
        [DisplayField]
        public string Password { get; set; }
    }
}
