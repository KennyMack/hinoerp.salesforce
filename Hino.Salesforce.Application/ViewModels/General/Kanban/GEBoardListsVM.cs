using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System.ComponentModel;

namespace Hino.Salesforce.Application.ViewModels.General.Kanban
{
    public class GEBoardListsVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public long BoardID { get; set; }
        public GEBoardsVM GEBoards { get; set; }

        [DisplayField("ListName")]
        [RequiredField]
        [Max60LengthField]
        public string Name { get; set; }

        [DisplayField("PositionList")]
        [RequiredField]
        [IntegerRangeField(0, 99)]
        public short Position { get; set; }

        [DisplayField("ListDescription")]
        [Max120LengthField]
        public string Description { get; set; }
        [DisplayField]
        [RequiredField]
        [DefaultValue(false)]
        public bool IsComplete { get; set; }
        [DisplayField]
        [RequiredField]
        [DefaultValue(false)]
        public bool IsSuccess { get; set; }
    }
}
