using Hino.Salesforce.Infra.Cross.Utils.Attributes;

namespace Hino.Salesforce.Application.ViewModels.General.Kanban
{
    public class GEBoardUsersVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public long UserID { get; set; }
        public GEUsersVM GEUsers { get; set; }

        [DisplayField]
        [RequiredField]
        public long BoardID { get; set; }
        public GEBoardsVM GEBoards { get; set; }
    }
}
