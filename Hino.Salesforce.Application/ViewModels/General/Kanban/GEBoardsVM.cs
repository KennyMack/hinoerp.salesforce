using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System.Collections.Generic;
using System.ComponentModel;

namespace Hino.Salesforce.Application.ViewModels.General.Kanban
{
    public class GEBoardsVM : BaseVM
    {
        public GEBoardsVM()
        {
            GECardFields = new HashSet<GECardFieldsVM>();
            GEBoardUsers = new HashSet<GEBoardUsersVM>();
            GEBoardLists = new HashSet<GEBoardListsVM>();
        }

        [DisplayField]
        [RequiredField]
        public long SettingsID { get; set; }
        public GESettingsVM GESettings { get; set; }

        [DisplayField("BoardName")]
        [RequiredField]
        [Max60LengthField]
        public string Name { get; set; }
        [DisplayField]
        [RequiredField]
        [DefaultValue(false)]
        public bool IsPublic { get; set; }

        public ICollection<GECardFieldsVM> GECardFields { get; set; }
        public ICollection<GEBoardUsersVM> GEBoardUsers { get; set; }
        public ICollection<GEBoardListsVM> GEBoardLists { get; set; }
    }
}
