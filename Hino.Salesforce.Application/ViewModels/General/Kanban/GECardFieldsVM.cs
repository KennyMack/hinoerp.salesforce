using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System.Collections.Generic;
using System.ComponentModel;

namespace Hino.Salesforce.Application.ViewModels.General.Kanban
{
    public class GECardFieldsVM : BaseVM
    {
        public GECardFieldsVM()
        {
            GEFieldUsers = new HashSet<GEFieldUsersVM>();
        }

        [DisplayField]
        [RequiredField]
        public long BoardID { get; set; }
        public GEBoardsVM GEBoards { get; set; }

        [DisplayField("NameField")]
        [RequiredField]
        [Max60LengthField]
        public string Name { get; set; }

        [DisplayField("TypeField")]
        [RequiredField]
        [Max10LengthField]
        public string Type { get; set; }

        [DisplayField]
        [Max500LengthField]
        public string Options { get; set; }

        [DisplayField("RequiredField")]
        [RequiredField]
        [DefaultValue(false)]
        public bool Required { get; set; }

        [DisplayField()]
        [RequiredField]
        [DefaultValue(false)]
        public bool IsPublic { get; set; }


        public ICollection<GEFieldUsersVM> GEFieldUsers { get; set; }
    }
}
