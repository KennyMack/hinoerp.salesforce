using Hino.Salesforce.Infra.Cross.Utils.Attributes;

namespace Hino.Salesforce.Application.ViewModels.General.Kanban
{
    public class GEFieldUsersVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public long UserID { get; set; }
        public GEUsersVM GEUsers { get; set; }

        [DisplayField]
        [RequiredField]
        public long FieldID { get; set; }
        public GECardFieldsVM GECardFields { get; set; }
    }
}
