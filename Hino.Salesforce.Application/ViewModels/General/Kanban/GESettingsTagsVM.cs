using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System.ComponentModel;

namespace Hino.Salesforce.Application.ViewModels.General.Kanban
{
    public class GESettingsTagsVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public long SettingsID { get; set; }
        public GESettingsVM GESettings { get; set; }

        [DisplayField]
        [RequiredField]
        [DefaultValue("#6699cc")]
        [Max8LengthField]
        public string Color { get; set; }
        [DisplayField("DescTag")]
        [RequiredField]
        [Max60LengthField]
        public string Name { get; set; }
        [DisplayField]
        [RequiredField]
        [DefaultValue(false)]
        public bool IsComplete { get; set; }
        [DisplayField]
        [RequiredField]
        [DefaultValue(false)]
        public bool IsSuccess { get; set; }
    }
}
