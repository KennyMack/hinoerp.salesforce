using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.General.Kanban
{
    public class GESettingsVM : BaseVM
    {
        public GESettingsVM()
        {
            GESettingsTags = new HashSet<GESettingsTagsVM>();
            GEBoards = new HashSet<GEBoardsVM>();
        }

        [DisplayField]
        [RequiredField]
        public long EstabID { get; set; }
        public GEEstablishmentsVM GEEstablishments { get; set; }

        public ICollection<GESettingsTagsVM> GESettingsTags { get; set; }
        public ICollection<GEBoardsVM> GEBoards { get; set; }

        public static Expression<Func<GESettings, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
