﻿using Hino.Salesforce.Infra.Cross.Entities.Logistics;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.Logistics
{
    public class LORoutesVM : BaseVM
    {
        public LORoutesVM()
        {
            this.LOTrips = new HashSet<LOTripsVM>();
            this.LOWaypoints = new HashSet<LOWaypointsVM>();
        }

        [DisplayField]
        [RequiredField]
        public decimal Weigth { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal Duration { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal Distance { get; set; }
        [DisplayField]
        public string Note { get; set; }


        public virtual ICollection<LOTripsVM> LOTrips { get; set; }
        public virtual ICollection<LOWaypointsVM> LOWaypoints { get; set; }

        public static Expression<Func<LORoutes, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
