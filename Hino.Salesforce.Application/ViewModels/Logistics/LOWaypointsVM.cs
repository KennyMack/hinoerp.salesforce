﻿using Hino.Salesforce.Infra.Cross.Entities.Logistics;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.Logistics
{
    public class LOWaypointsVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public long RouteID { get; set; }
        public virtual LORoutesVM LORoute { get; set; }

        [DisplayField]
        [RequiredField]
        public decimal Lat { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal Lng { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal Distance { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal Name { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal TripIndex { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal WayPointIndex { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal Address { get; set; }

        public static Expression<Func<LOWaypoints, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;

    }
}
