﻿using Hino.Salesforce.Infra.Cross.Utils.Attributes;

namespace Hino.Salesforce.Application.ViewModels.Route
{
    public class AddressesVM
    {
        public string name { get; set; }
        public int arrival { get; set; }
        public decimal distance { get; set; }
        public string address { get; set; }
        public decimal lat { get; set; }
        public decimal lng { get; set; }
    }

    public class AddressGeoVM
    {
        [RequiredField]
        [DisplayField]
        public string Street { get; set; }
        [RequiredField]
        [DisplayField]
        public string District { get; set; }
        [DisplayField]
        public string Number { get; set; }
        [RequiredField]
        [DisplayField]
        public string ZipCode { get; set; }
        [RequiredField]
        [DisplayField]
        public string City { get; set; }
        [RequiredField]
        [DisplayField]
        public string State { get; set; }
        [RequiredField]
        [DisplayField]
        public string Country { get; set; }

        public override string ToString()
        {
            if (Country == "US")
                return $"{(Number ?? "").Replace(" ", "%20")}%20{(Street ?? "").Replace(" ", "%20")}%20{(City ?? "")}%20{(State ?? "")}%20{(ZipCode ?? "")}";
            return $"{(Street ?? "").Replace(" ", "%20")},%20{(Number ?? "").Replace(" ", "%20")}%20-%20{(District ?? "").Replace(" ", "%20")},%20{(City ?? "")}%20-%20{(State ?? "")},%20{(ZipCode ?? "")}";
        }

        public string ToAddress()
        {
            var number = Number != "0" &&
                !string.IsNullOrEmpty(Number) ? Number : "";


            // 84+rua+tulio+taques+de+lemos+parque+das+nações+Limeira
            return $"{(Street ?? "").Replace(" ", "+")},+{(Number ?? "").Replace(" ", "+")}+-+{(District ?? "").Replace(" ", "+")},+{(City ?? "")}+-+{(State ?? "")},+{(ZipCode ?? "")}";
        }
    }
}
