﻿using System.Collections.Generic;

namespace Hino.Salesforce.Application.ViewModels.Route
{
    public class MapOptimizedVM
    {
        public List<MapPointVM> points { get; set; }
        public MapRouteGeometryVM route { get; set; }

        public MapOptimizedVM()
        {
            points = new List<MapPointVM>();
            route = new MapRouteGeometryVM();
        }
    }

    public class MapPointVM
    {
        public string type { get; set; }

        public MapGeometryVM geometry { get; set; }
        public MapPointProperyVM properties { get; set; }
    }

    public class MapRouteGeometryVM
    {
        public string type { get; set; }
        public List<decimal[]> coordinates { get; set; }

        public MapRouteGeometryVM()
        {
            coordinates = new List<decimal[]>();
        }
    }

    public class MapGeometryVM
    {
        public string type { get; set; }
        public List<decimal> coordinates { get; set; }

        public MapGeometryVM()
        {
            coordinates = new List<decimal>();
        }
    }

    public class MapPointProperyVM
    {
        public string title { get; set; }
        public string address { get; set; }
        public string icon { get; set; }
    }
}
