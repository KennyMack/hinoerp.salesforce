﻿namespace Hino.Salesforce.Application.ViewModels.Route
{
    public class MapVM
    {
        public string features { get; set; }
        public string coordinates { get; set; }
    }
}
