﻿using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System.Collections.Generic;
using System.Linq;

namespace Hino.Salesforce.Application.ViewModels.Route
{
    public class OptimizedVM
    {
        [RequiredField]
        [DisplayField]
        public string EstablishmentKey { get; set; }

        [RequiredField]
        public List<LocationVM> location { get; set; }

        public OptimizedVM()
        {
            location = new List<LocationVM>();
        }

        public override string ToString()
        {
            return string.Join(";", location.Select(r => $"{r.lng.ToString().Replace(",", ".")},{r.lat.ToString().Replace(",", ".")}").ToArray());
        }

        public string GetRadiuses()
        {
            return string.Join(";", location.Select(r => $"30").ToArray());
        }
        public List<LocationVM> GetDestinations()
        {
            var names = location.Where(r => r.name != location.First().name).Select(r => r.name).Distinct();

            var destinations = new List<LocationVM>();
            foreach (var name in names)
                destinations.Add(location.First(r => r.name == name));

            return destinations;
        }
    }

    public class LocationVM
    {
        [RequiredField]
        public string address { get; set; }

        [RequiredField]
        public string name { get; set; }

        [RequiredField]
        public decimal lng { get; set; }
        [RequiredField]
        public decimal lat { get; set; }

        public string ToDestination()
        {
            return $"{name};{lat.ToString().Replace(",", ".")},{lng.ToString().Replace(",", ".")}";
        }

        public string ToStart()
        {
            return $"Origem;{lat.ToString().Replace(",", ".")},{lng.ToString().Replace(",", ".")}";
        }

        public string ToEnd()
        {
            return $"Destino;{lat.ToString().Replace(",", ".")},{lng.ToString().Replace(",", ".")}";
        }
    }
}
