﻿using System.Collections.Generic;

namespace Hino.Salesforce.Application.ViewModels.Route
{
    public class OptimizedTripResultVM
    {
        public long Id { get; set; }
        public string EstablishmentKey { get; set; }
        public string UniqueKey { get; set; }
        public string code { get; set; }
        public List<WaypointsVM> waypoints { get; set; }
        public List<TripsVM> trips { get; set; }

        public OptimizedTripResultVM()
        {
            waypoints = new List<WaypointsVM>();
            trips = new List<TripsVM>();
        }
    }

    public class WaypointsVM
    {
        public decimal distance { get; set; }
        public string name { get; set; }
        public List<decimal> location { get; set; }
        public int waypoint_index { get; set; }
        public int trips_index { get; set; }

        public WaypointsVM()
        {
            location = new List<decimal>();
        }

    }

    public class TripsVM
    {
        public OptmizedGeometryVM geometry { get; set; }
        public string weight_name { get; set; }
        public decimal weight { get; set; }
        public decimal duration { get; set; }
        public decimal distance { get; set; }

        public TripsVM()
        {
            geometry = new OptmizedGeometryVM();
        }

    }

    public class OptmizedGeometryVM
    {
        public string type { get; set; }
        public List<decimal[]> coordinates { get; set; }

        public OptmizedGeometryVM()
        {
            coordinates = new List<decimal[]>();
        }
    }

}
