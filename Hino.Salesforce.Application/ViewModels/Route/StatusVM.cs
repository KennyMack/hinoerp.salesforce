﻿using System;

namespace Hino.Salesforce.Application.ViewModels.Route
{
    public class StatusVM
    {
        public string id { get; set; }
        public DateTime datetime { get; set; }
        public int load { get; set; }
        public SolversVM solvers { get; set; }
        public string echo { get; set; }
        public int max_locations { get; set; }

        /*
        {
            "id":"RouteXL API 1.3",
            "datetime":"2020-05-31 05:33:12",
            "load":98,
            "solvers":{
                "LB@Amazon":0,
                "WS2611@WBC":3,
                "selected":{
                    "server":"LB@Amazon",
                    "load":0
                }
            },
            "echo":"Dorime",
            "max_locations":10
            }
        */

    }
}
