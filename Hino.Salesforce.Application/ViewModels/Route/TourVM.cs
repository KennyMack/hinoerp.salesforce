﻿using System.Collections.Generic;

namespace Hino.Salesforce.Application.ViewModels.Route
{
    public class TourVM
    {
        public string id { get; set; }
        public int count { get; set; }
        public bool feasible { get; set; }
        public Dictionary<string, AddressesVM> route { get; set; }
    }
}
