﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Hino.Salesforce.Application.ViewModels.Sales
{
    public class DefaultSalesResumeFilterModel
    {
        [Required]
        [Range(1, long.MaxValue)]
        public long UserId { get; set; }
        [Required]
        public DateTime InitialDate { get; set; }
        [Required]
        public DateTime FinalDate { get; set; }
    }
}
