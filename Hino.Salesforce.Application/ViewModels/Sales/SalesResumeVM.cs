﻿using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.Sales
{
    public class SalesResumeVM
    {

    }

    public class ProposalsPendingVM
    {
        public int Quantity { get; set; }
    }

    public class PaymentPendingVM
    {
        public bool HasPaymentPending { get; set; }
    }

    public class BestProductsVM
    {
        public string ProductKey { get; set; }
        public string Name { get; set; }
        public decimal Quantity { get; set; }
    }

    public class CommissionByProductVM
    {
        public string ProductKey { get; set; }
        public string Name { get; set; }
        public decimal TotalCommission { get; set; }
    }

    public class TotalCommissionVM
    { 
        public decimal TotalCommission { get; set; }
    }

    public class DetailedSalesVM
    {
        public VEOrderItemsVM VEOrderItems { get; set; }
        public GEEnterprisesVM GEEnterprises { get; set; }

        public DateTime Created { get; set; }
    }

    public class SalesLimitVM
    {
        public float BoletoLimit { get; set; }
        public float LimitUsed { get; set; }

        public List<VEOrdersVM> VEOrders { get; set; }
    }

    public class SalesByProductVM
    {
        public string ProductKey { get; set; }
        public string Name { get; set; }
        public decimal TotalValue { get; set; }
        public decimal TotalQuantity { get; set; }
    }

    public class SalesByMonthVM
    {
        public DateTime Date;
        public decimal TotalValue { get; set; }
        public decimal TotalQuantity { get; set; }
    }

    public class TotalSalesVM
    {
        public decimal TotalValue { get; set; }
        public decimal TotalQuantity { get; set; }
    }
}
