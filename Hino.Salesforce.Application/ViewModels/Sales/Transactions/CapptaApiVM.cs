﻿using System;
using System.Collections.Generic;

namespace Hino.Salesforce.Application.ViewModels.Sales.Transactions
{
    public class CapptaApiResponseVM
    {
        public List<CapptaApiVM> results { get; set; }
    }

    public class CapptaApiVM
    {
        public string cnpj { get; set; }
        public string originCnpj { get; set; }
        public int checkoutNumber { get; set; }
        public int originCheckoutNumber { get; set; }
        public string uniqueSequentialNumber { get; set; }
        public string cardNumber { get; set; }
        public Decimal amountInCent { get; set; }
        public short installments { get; set; }
        public string administrativeCode { get; set; }
        public String requestkey { get; set; }
        public string paymentMethod { get; set; }
        public string acquirer { get; set; }
        public string cardBrand { get; set; }
        public string status { get; set; }
        public string statusInfo { get; set; }
        public string acquirerAuthorizationCode { get; set; }
        public DateTime date { get; set; }
        public DateTime dateUTC { get; set; }
        public string acquirerUniqueSequentialNumber { get; set; }
        public Decimal fee { get; set; }
        public string feeType { get; set; }
    }

    public class CapptaApiSearchVM
    {
        public List<string> cnpjs { get; set; }
        public DateTime initialdate { get; set; }
        public DateTime finalDate { get; set; }
        public string uniqueSequentialNumber { get; set; }
    }
}