using Hino.Salesforce.Infra.Cross.Entities.Sales.Transactions;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.Sales.Transactions
{
    [EndPoint("Sales/Transactions/{pEstablishmentKey}")]
    public class VETransactionsVM : BaseVM
    {
        [DisplayField]
        [ForeignKey("VEOrders")]
        [RequiredField]
        [LongRangeField(0, long.MaxValue)]
        public long OrderID { get; set; }
        public virtual VEOrdersVM VEOrders { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerRangeField(0, 2)]
        public ETransactionStatus Status { get; set; }
        [DisplayField]
        [RequiredField]
        [IntegerRangeField(0, 2)]
        public ETransactionType Type { get; set; }
        [DisplayField]
        public string AuthCode { get; set; }
        [DisplayField]
        public DateTime? AuthDate { get; set; }
        [DisplayField]
        public string AcquirerName { get; set; }
        [DisplayField]
        public string NSU { get; set; }
        [DisplayField]
        public string NSUHost { get; set; }
        [DisplayField]
        public string AdminCode { get; set; }
        [DisplayField]
        public string CardBrand { get; set; }
        [DisplayField]
        public string CardLastDigits { get; set; }
        [DisplayField]
        public decimal? Installments { get; set; }
        [DisplayField]
        public decimal? Amount { get; set; }
        [DisplayField]
        public string Description { get; set; }
        [DisplayField]
        public string CustomerReceipt { get; set; }
        [DisplayField]
        public string MerchantReceipt { get; set; }
        [DisplayField]
        [RequiredField]
        public bool Reversed { get; set; }
        [DisplayField]
        public long IdERP { get; set; }
        [RequiredField]
        [DisplayField]
        [IntegerRangeField(0, 2)]
        public EStatusSinc StatusSinc { get; set; }
        [RequiredField]
        [DisplayField]
        [IntegerRangeField(0, 1)]
        public ETransactionOrigin Origin { get; set; }
        [DisplayField]
        public string TerminalId { get; set; }
        [DisplayField]
        public string Operator { get; set; }
        [DisplayField]
        public string StoreId { get; set; }

        public static Expression<Func<VETransactions, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
