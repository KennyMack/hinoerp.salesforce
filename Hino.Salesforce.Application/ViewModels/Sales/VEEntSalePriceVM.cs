using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;

namespace Hino.Salesforce.Application.ViewModels.Sales
{
    public class VEEntSalePriceVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public long SalePriceID { get; set; }
        [DisplayField]
        [RequiredField]
        public long EnterpriseID { get; set; }
    }
}
