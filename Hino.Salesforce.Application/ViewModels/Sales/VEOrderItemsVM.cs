﻿using Hino.Salesforce.Application.ViewModels.Fiscal;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.Sales
{
    [EndPoint("Sales/Orders/{pEstablishmentKey}")]
    public class VEOrderItemsVM : BaseVM
    {
        public VEOrderItemsVM()
        {
            this.VEOrderTaxes = new HashSet<VEOrderTaxesVM>();
            this.GEFilesPath = new HashSet<GEFilesPathVM>();
        }

        [RequiredField]
        [DisplayField]
        [ForeignKey("VEOrders")]
        [LongRangeField(0, long.MaxValue)]
        public long OrderID { get; set; }
        // public virtual VEOrdersVM VEOrders { get; set; }

        [RequiredField]
        [DisplayField]
        [ForeignKey("GEProducts")]
        [LongRangeField(1, long.MaxValue)]
        public long ProductID { get; set; }
        public virtual GEProductsVM GEProducts { get; set; }

        [RequiredField]
        [DisplayField]
        [ForeignKey("FSFiscalOper")]
        public long FiscalOperID { get; set; }
        public virtual FSFiscalOperVM FSFiscalOper { get; set; }

        [RequiredField]
        [DisplayField]
        [DefaultValue(0)]
        public decimal TableValue { get; set; }
        [RequiredField]
        [DisplayField]
        [DefaultValue(0)]
        [FloatGreaterThan(0)]
        public float Value { get; set; }
        [RequiredField]
        [DisplayField]
        [DefaultValue(0)]
        [FloatGreaterThan(0)]
        public float Quantity { get; set; }

        [RequiredField]
        [DisplayField]
        [DefaultValue(0)]
        [FloatGreaterThan(0)]
        public float QuantityReference { get; set; }
        [RequiredField]
        [DisplayField]
        [DefaultValue(0)]
        public float PercDiscount { get; set; }
        [DisplayField]
        [Max2000LengthField]
        public string Note { get; set; }
        [RequiredField]
        [DisplayField]
        [DefaultValue(0)]
        [IntegerRangeField(0, int.MaxValue)]
        public int Item { get; set; }
        [RequiredField]
        [DisplayField]
        [DefaultValue(0)]
        [IntegerRangeField(0, int.MaxValue)]
        public int ItemLevel { get; set; }

        [DisplayField]
        public string ClientOrder { get; set; }

        [DisplayField]
        public string ClientItem { get; set; }
        [DisplayField]
        [RequiredField]
        public System.DateTime DeliveryDate { get; set; }

        [DisplayField]
        [RequiredField]
        public float PercDiscountHead { get; set; }
        [DisplayField]
        [RequiredField]
        public float PercCommission { get; set; }
        [DisplayField]
        [RequiredField]
        public float PercCommissionHead { get; set; }
        [DisplayField]
        [RequiredField]
        [DefaultValue(1)]
        [IntegerRangeField(0, 500)]
        public int ShippingDays { get; set; }

        [DisplayField]
        [RequiredField]
        [DefaultValue(false)]
        public bool AltDescription { get; set; }

        [DisplayField]
        [RequiredField]
        [DefaultValue(0f)]
        public float QuantityReturned { get; set; }

        [NotMapped]
        public float ValueWithDiscount
        {
            get => Value - (Value * (PercDiscount / 100)) - ((Value - (Value * (PercDiscount / 100))) * (PercDiscountHead / 100));
        }

        [NotMapped]
        [DisplayField]
        public float TotalValue
        {
            get
            {
                return Quantity * Value;
            }
        }

        [NotMapped]
        [DisplayField]
        public float TotalValueWithDiscount
        {
            get => TotalValue - (TotalValue * (PercDiscount / 100)) - ((TotalValue - (TotalValue * (PercDiscount / 100))) * (PercDiscountHead / 100));
        }

        [NotMapped]
        [DisplayField]
        public float BaseCommission
        {
            get => (Quantity - QuantityReturned) * ValueWithDiscount;
        }

        [NotMapped]
        [DisplayField]
        public float TotalValueCommission
        {
            get => PercCommissionHead > 0 ?
                (BaseCommission * (PercCommissionHead / 100)) :
                (BaseCommission * (PercCommission / 100));
        }

        [NotMapped]
        public decimal PercICMS
        {
            get
            {
                if (VEOrderTaxes != null && VEOrderTaxes.Where(r => r.Type == 0).Any())
                    return VEOrderTaxes.Where(r => r.Type == 0).Max(r => r.Aliquot);

                return 0;
            }
        }

        [NotMapped]
        public decimal TotalIPI
        {
            get
            {
                if (VEOrderTaxes != null && VEOrderTaxes.Where(r => r.Type == 3).Any())
                    return VEOrderTaxes.Where(r => r.Type == 3).Max(r => r.Value);

                return 0;
            }
        }

        [NotMapped]
        public decimal PercIPI
        {
            get
            {
                if (VEOrderTaxes != null && VEOrderTaxes.Where(r => r.Type == 3).Any())
                    return VEOrderTaxes.Where(r => r.Type == 3).Max(r => r.Aliquot);

                return 0;
            }
        }

        [NotMapped]
        public decimal PercICMSST
        {
            get
            {
                if (VEOrderTaxes != null && VEOrderTaxes.Where(r => r.Type == 1 || r.Type == 2 || r.Type == 18).Any())
                    return VEOrderTaxes.Where(r => r.Type == 1 || r.Type == 2 || r.Type == 18).Max(r => r.Value);

                return 0;
            }
        }

        [NotMapped]
        public decimal TotalICMSST
        {
            get
            {
                if (VEOrderTaxes != null && VEOrderTaxes.Where(r => r.Type == 1 || r.Type == 18).Any())
                    return VEOrderTaxes.Where(r => r.Type == 1 || r.Type == 18).Max(r => r.Value);

                return 0;
            }
        }

        [NotMapped]
        public decimal TotalICMSDifAliquota
        {
            get
            {
                if (VEOrderTaxes != null && VEOrderTaxes.Where(r => r.Type == 2).Any())
                    return VEOrderTaxes.Where(r => r.Type == 2).Max(r => r.Value);

                return 0;
            }
        }

        public long IdERP { get; set; }

        public virtual ICollection<VEOrderTaxesVM> VEOrderTaxes { get; set; }
        public virtual ICollection<GEFilesPathVM> GEFilesPath { get; set; }

        public static Expression<Func<VEOrderItems, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
