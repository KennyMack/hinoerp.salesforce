﻿using Hino.Salesforce.Infra.Cross.Utils.Attributes;

namespace Hino.Salesforce.Application.ViewModels.Sales
{
    public class VEOrderStatusVM
    {
        [Min36LengthField]
        [Max36LengthField]
        [RequiredField]
        [DisplayField]
        public string EstablishmentKey { get; set; }

        [RequiredField]
        [Min36LengthField]
        [Max36LengthField]
        [DisplayField]
        public string UniqueKey { get; set; }

        [RequiredField]
        [DisplayField]
        [LongRangeField(0, long.MaxValue)]
        public long OrderID { get; set; }

        [DisplayField]
        [RequiredField]
        public string Status { get; set; }

        [DisplayField]
        public string RevisionReason { get; set; }

        [DisplayField]
        [RequiredField]
        public long DigitizerID { get; set; }

        public bool CanChangeValue { get; set; }

    }
}
