﻿using Hino.Salesforce.Infra.Cross.Entities.General;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.ViewModels.Sales
{
    public class VEOrderStockBalanceVM
    {
        public long ProductID { get; set; }
        public GEProducts GEProducts { get; set; }

        public decimal Quantity { get; set; }
        public decimal QtdBudgeted { get; set; }
        public decimal QtdOrders { get; set; }
        public decimal StockBalance { get; set; }
    }
}
