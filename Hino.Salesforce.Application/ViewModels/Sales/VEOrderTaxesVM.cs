using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Application.ViewModels.Sales
{
    public class VEOrderTaxesVM : BaseVM
    {

        [DisplayField]
        [RequiredField]
        [ForeignKey("VEOrderItems")]
        public long OrderItemID { get; set; }
        public virtual VEOrderItemsVM VEOrderItems { get; set; }

        [DisplayField]
        [RequiredField]
        public short Type { get; set; }
        [DisplayField]
        public string Tax
        {
            get
            {
                switch (Type)
                {
                    case 0:
                        return "ICMS";
                    case 1:
                        return "ICMS ST (MVA)";
                    case 2:
                        return "ICMS DIFERENCIAL DE AL�QUOTA";
                    case 3:
                        return "IPI";
                    case 4:
                        return "PIS";
                    case 5:
                        return "PIS RETIDO";
                    case 6:
                        return "COFINS";
                    case 7:
                        return "COFINS RETIDO";
                    case 8:
                        return "CSLL";
                    case 9:
                        return "CSLL RETIDO";
                    case 10:
                        return "IRPJ";
                    case 11:
                        return "IRRF";
                    case 12:
                        return "INSS";
                    case 13:
                        return "INSS RETIDO";
                    case 14:
                        return "ISSQN', ";
                    case 15:
                        return "ISSQN RETIDO";
                    case 16:
                        return "II";
                    case 17:
                        return "CPP";
                    case 18:
                        return "ICMS ST (CTM)";
                    case 19:
                        return "FCP";
                    case 20:
                        return "ICMS DIF. AL�Q. DESTINO";
                    case 21:
                        return "ICMS DIF. ALIQ. ORIGEM";
                    case 22:
                        return "FCP-ST";
                    case 23:
                        return "FUNRURAL";
                    default:
                        return "OUTROS";
                }
            }
        }
        [DisplayField]
        [RequiredField]
        public decimal FreeZone { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal Aliquot { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal Basis { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal Value { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal ExemptValue { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal OtherValue { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal NoteValue { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal MVA { get; set; }
        [DisplayField]
        [RequiredField]
        public decimal PerRedBC { get; set; }
    }
}
