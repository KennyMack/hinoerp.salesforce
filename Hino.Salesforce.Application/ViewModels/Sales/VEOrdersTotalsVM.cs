﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Application.ViewModels.Sales
{
    public class VEOrdersTotalsVM: BaseVM
    {
        public string Description { get; set; }
        public float Value { get; set; }
    }
}
