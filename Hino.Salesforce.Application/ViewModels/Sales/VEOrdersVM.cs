﻿using Hino.Salesforce.Application.ViewModels.Fiscal;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Application.ViewModels.Sales.Transactions;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.Sales
{
    [EndPoint("Sales/Orders/{pEstablishmentKey}")]
    public class VEOrdersVM : BaseVM
    {
        public VEOrdersVM()
        {
            VEOrderItems = new HashSet<VEOrderItemsVM>();
            VETransactions = new HashSet<VETransactionsVM>();
            VEOrdersTotals = new HashSet<VEOrdersTotalsVM>();
            GEFilesPath = new HashSet<GEFilesPathVM>();
        }

        [RequiredField]
        [DisplayField]
        [ForeignKey("GEEnterprises")]
        [LongRangeField(1, long.MaxValue)]
        [Column(Order = 0)]
        public long EnterpriseID { get; set; }
        public virtual GEEnterprisesVM GEEnterprises { get; set; }

        [DisplayField]
        [ForeignKey("GECarriers")]
        [Column(Order = 1)]
        public long? CarrierID { get; set; }
        public virtual GEEnterprisesVM GECarriers { get; set; }

        [ForeignKey("GERedispatch")]
        public long? RedispatchID { get; set; }
        [InverseProperty("VEOrdersRedispatch")]
        public virtual GEEnterprisesVM GERedispatch { get; set; }

        [RequiredField]
        [DisplayField]
        [ForeignKey("GEUsers")]
        [LongRangeField(1, long.MaxValue)]
        public long UserID { get; set; }
        public virtual GEUsersVM GEUsers { get; set; }

        public long UserCreatedID { get; set; }

        [RequiredField]
        [DisplayField]
        [ForeignKey("GEPaymentType")]
        [LongRangeField(1, long.MaxValue)]
        public long TypePaymentID { get; set; }
        public virtual GEPaymentTypeVM GEPaymentType { get; set; }

        [RequiredField]
        [DisplayField]
        [ForeignKey("GEPaymentCondition")]
        [LongRangeField(1, long.MaxValue)]
        public long PayConditionID { get; set; }
        public virtual GEPaymentConditionVM GEPaymentCondition { get; set; }

        [DisplayField]
        public long CodPedVenda { get; set; }
        [DisplayField]
        public long NumPedMob { get; set; }
        [DisplayField]
        [RequiredField]
        public System.DateTime DeliveryDate { get; set; }
        [DisplayField]
        [Max2000LengthField]
        public string Note { get; set; }
        [DisplayField]
        [Max2000LengthField]
        public string InnerNote { get; set; }
        [DisplayField]
        [Max2000LengthField]
        public string DetailedNote { get; set; }
        [DisplayField]
        [RequiredField]
        public string Status { get; set; }
        [DisplayField]
        [RequiredField]
        public string StatusCRM { get; set; }
        [DisplayField]
        [RequiredField]
        public int StatusSinc { get; set; }
        [DisplayField]
        [RequiredField]
        public bool IsProposal { get; set; }

        [RequiredField]
        [DisplayField]
        [DefaultValue(9)]
        [IntegerRangeField(0, 9)]
        public EFreightPaidBy FreightPaidBy { get; set; }

        [RequiredField]
        [DisplayField]
        [DefaultValue(9)]
        [IntegerRangeField(0, 9)]
        public EFreightPaidBy RedispatchPaidBy { get; set; }

        [DisplayField]
        public decimal FreightValue { get; set; }

        [DisplayField]
        public long IdERP { get; set; }

        [DisplayField]
        public string ClientOrder { get; set; }

        [DisplayField]
        public long? OriginOrderID { get; set; }

        [DisplayField]
        public long? MainOrderID { get; set; }

        [DisplayField]
        [RequiredField]
        public int OrderVersion { get; set; }

        [DisplayField]
        public string RevisionReason { get; set; }

        [DisplayField]
        [RequiredField]
        public long DigitizerID { get; set; }
        public GEUsersVM GEUserDigitizer { get; set; }

        [DisplayField]
        [RequiredField]
        public float PercDiscount { get; set; }

        [DisplayField]
        [RequiredField]
        public float PercCommission { get; set; }

        public bool Converted { get; set; }

        [DisplayField]
        public string ContactPhone { get; set; }

        [DisplayField]
        public string ContactEmail { get; set; }

        [DisplayField]
        public string Contact { get; set; }

        [DisplayField]
        [RequiredField]
        [DefaultValue(0)]
        public decimal FinancialTaxes { get; set; }

        [DisplayField]
        [RequiredField]
        [DefaultValue(true)]
        public bool OnlyOnDate { get; set; }

        [DisplayField]
        [RequiredField]
        [DefaultValue(false)]
        public bool AllowPartial { get; set; }

        [DisplayField("ContactSector")]
        [IntegerRangeField(0, 999)]
        public EContactSectors Sector { get; set; }

        [DisplayField]
        [IntegerRangeField(0, 1)]
        public bool InPerson { get; set; }

        [DisplayField("FiscalOperID")]
        public long? MainFiscalOperID { get; set; }
        public virtual FSFiscalOperVM MainFiscalOper { get; set; }

        [DisplayField]
        public DateTime? PaymentDueDate { get; set; }

        [RequiredField]
        [DisplayField]
        [DefaultValue(0)]
        [IntegerRangeField(0, 3)]
        public int StatusPay { get; set; }

        [RequiredField]
        [DisplayField]
        [DefaultValue(0)]
        public float PaidAmount { get; set; }

        [RequiredField]
        [DefaultValue(0)]
        public float AdvanceAmount { get; set; }

        [DisplayField]
        public long? TypeSaleId { get; set; }
        public VETypeSale VETypeSale { get; set; }

        [NotMapped]
        [DisplayField]
        public float TotalValue
        {
            get
            {
                if (VEOrderItems != null)
                {
                    return (float)Math.Round(VEOrderItems.Where(r => r.ItemLevel == 0).Sum(r =>
                        (r.Quantity * r.Value)
                    ), 2);
                }

                return 0;
            }
        }

        [NotMapped]
        [DisplayField]
        public float TotalValueDiscount
        {
            get
            {
                if (VEOrderItems != null)
                {
                    return (float)Math.Round(TotalValue - VEOrderItems.Where(r => r.ItemLevel == 0).Sum(r =>
                        (r.Quantity * r.Value) * (r.PercDiscount / 100)
                    ), 2);
                }

                return 0;
            }
        }

        [NotMapped]
        [DisplayField]
        public decimal TotalValueWithDiscount
        {
            get => Math.Round((decimal)TotalValueDiscount - ((decimal)TotalValueDiscount * ((decimal)PercDiscount / 100)), 2);
        }

        [NotMapped]
        public decimal TotalValueWithDiscountAndTaxes
        {
            get
            {
                if (VEOrderItems != null && VEOrderItems.Any())
                {
                    return Math.Round(FreightValue + TotalValueWithDiscount +
                        VEOrderItems.Sum(r => r.TotalIPI + r.TotalICMSST + r.TotalICMSDifAliquota), 2);
                }

                return Math.Round(FreightValue + TotalValueWithDiscount, 2);
            }
        }

        [NotMapped]
        public decimal TotalICMSDiferencial
        {
            get
            {
                if (VEOrderItems != null && VEOrderItems.Any())
                {
                    return Math.Round(VEOrderItems.Sum(r => r.TotalICMSDifAliquota), 2);
                }

                return 0;
            }
        }

        [NotMapped]
        [DisplayField]
        public float TotalValueCommission
        {
            //get => (TotalValueWithDiscount * (PercCommission / 100));
            // Alterei para calcular comissão pelos itens para ficar igual ao ERP
            get
            {
                if (VEOrderItems != null && VEOrderItems.Any())
                {
                    return (float)Math.Round(VEOrderItems.Where(r => r.ItemLevel == 0).Sum(r => (r.TotalValueCommission)), 2);
                }

                return 0;
            }
        }

        [NotMapped]
        [DisplayField]
        public decimal ShippingDays
        {
            get
            {
                if (VEOrderItems != null && VEOrderItems.Any())
                {
                    return VEOrderItems.Max(r => r.ShippingDays);
                }

                return 0;
            }
        }

        public virtual ICollection<VEOrderItemsVM> VEOrderItems { get; set; }
        public virtual ICollection<VETransactionsVM> VETransactions { get; set; }
        public ICollection<VEOrdersTotalsVM> VEOrdersTotals { get; set; }
        public virtual ICollection<GEFilesPathVM> GEFilesPath { get; set; }

        public static Expression<Func<VEOrders, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter, bool pIsProposal)
        {
            if (!pFilter.IsEmpty() && pIsProposal)
                return r => (r.EstablishmentKey == pEstablishmentKey &&
                 r.IsProposal) &&
                (
                    (
                        (r.Id.ToString().Contains(pFilter.Trim())) ||
                        (r.IdERP.ToString().Contains(pFilter.Trim())) ||
                        (r.GEEnterprises.Id.ToString().Contains(pFilter.Trim())) ||
                        (r.GEEnterprises.RazaoSocial.ToUpper().Contains(pFilter.Trim())) ||
                        (r.GEEnterprises.NomeFantasia.ToUpper().Contains(pFilter.Trim())) ||
                        (r.GEEnterprises.CNPJCPF.ToUpper().Contains(pFilter.Trim()))
                    )
                );
            else if (!pFilter.IsEmpty() && !pIsProposal)
                return r => (r.EstablishmentKey == pEstablishmentKey &&
                 !r.IsProposal) &&
                (
                    (
                        (r.Id.ToString().Contains(pFilter.Trim())) ||
                        (r.IdERP.ToString().Contains(pFilter.Trim())) ||
                        (r.GEEnterprises.Id.ToString().Contains(pFilter.Trim())) ||
                        (r.GEEnterprises.RazaoSocial.ToUpper().Contains(pFilter.Trim())) ||
                        (r.GEEnterprises.NomeFantasia.ToUpper().Contains(pFilter.Trim())) ||
                        (r.GEEnterprises.CNPJCPF.ToUpper().Contains(pFilter.Trim()))
                    )
                );
            else if (pFilter.IsEmpty() && !pIsProposal)
                return r => r.EstablishmentKey == pEstablishmentKey &&
                    !r.IsProposal;
            else
                return r => r.EstablishmentKey == pEstablishmentKey &&
                    r.IsProposal;
        }

        public static Expression<Func<VEOrders, bool>> GetDefaultFilterAll(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                (
                    (
                        (r.Id.ToString().Contains(pFilter.Trim())) ||
                        (r.IdERP.ToString().Contains(pFilter.Trim())) ||
                        (r.GEEnterprises.Id.ToString().Contains(pFilter.Trim())) ||
                        (r.GEEnterprises.RazaoSocial.ToUpper().Contains(pFilter.Trim())) ||
                        (r.GEEnterprises.NomeFantasia.ToUpper().Contains(pFilter.Trim())) ||
                        (r.GEEnterprises.CNPJCPF.ToUpper().Contains(pFilter.Trim()))
                    )
                );

            return r => r.EstablishmentKey == pEstablishmentKey;
        }

        public static Expression<Func<VEOrders, bool>> GetDefaultFilterByUser(string pEstablishmentKey, string pFilter, long pUserId, bool pIsProposal)
        {
            if (!pFilter.IsEmpty() && pIsProposal)
                return r => (r.EstablishmentKey == pEstablishmentKey &&
                    r.UserID == pUserId &&
                    r.IsProposal) &&
                    (
                        (
                            (r.Id.ToString().Contains(pFilter.Trim())) ||
                            (r.IdERP.ToString().Contains(pFilter.Trim())) ||
                            (r.GEEnterprises.Id.ToString().Contains(pFilter.Trim())) ||
                            (r.GEEnterprises.RazaoSocial.ToUpper().Contains(pFilter.Trim())) ||
                            (r.GEEnterprises.NomeFantasia.ToUpper().Contains(pFilter.Trim())) ||
                            (r.GEEnterprises.CNPJCPF.ToUpper().Contains(pFilter.Trim()))
                        )
                    );
            else if (!pFilter.IsEmpty() && !pIsProposal)
                return r => (r.EstablishmentKey == pEstablishmentKey &&
                    r.UserID == pUserId &&
                    !r.IsProposal) &&
                    (
                        (
                            (r.Id.ToString().Contains(pFilter.Trim())) ||
                            (r.IdERP.ToString().Contains(pFilter.Trim())) ||
                            (r.GEEnterprises.Id.ToString().Contains(pFilter.Trim())) ||
                            (r.GEEnterprises.RazaoSocial.ToUpper().Contains(pFilter.Trim())) ||
                            (r.GEEnterprises.NomeFantasia.ToUpper().Contains(pFilter.Trim())) ||
                            (r.GEEnterprises.CNPJCPF.ToUpper().Contains(pFilter.Trim()))
                        )
                    );
            else if (pFilter.IsEmpty() && !pIsProposal)
                return r => r.EstablishmentKey == pEstablishmentKey &&
                    r.UserID == pUserId &&
                    !r.IsProposal;
            else
                return r => r.EstablishmentKey == pEstablishmentKey &&
                    r.UserID == pUserId &&
                    r.IsProposal;
        }
    }

    public class VEMainOrdersVM
    {
        [DisplayField]
        public long Id { get; set; }

        [DisplayField]
        public int OrderVersion { get; set; }

        [DisplayField]
        public long? MainOrderID { get; set; }

        [DisplayField]
        public long? OriginOrderID { get; set; }

        [DisplayField]
        public long IdERP { get; set; }

        [DisplayField]
        public long EnterpriseID { get; set; }
        public GEMainClientsVM GEEnterprises { get; set; }

        [DisplayField]
        public long? CarrierID { get; set; }

        [DisplayField]
        public long? RedispatchID { get; set; }

        [DisplayField]
        public long UserID { get; set; }

        public GEOrderUserVM GEUsers { get; set; }

        [DisplayField]
        public long DigitizerID { get; set; }
        public GEOrderUserVM GEUserDigitizer { get; set; }

        [DisplayField]
        public float PercDiscount { get; set; }

        [DisplayField]
        public string Status { get; set; }

        [DisplayField]
        public string StatusCRM { get; set; }
        
        [DisplayField]
        public int StatusSinc { get; set; }

        [DisplayField]
        public int StatusPay { get; set; }
        [DisplayField]
        public float PaidAmount { get; set; }

        [DisplayField]
        public decimal FinancialTaxes { get; set; }

        [DisplayField]
        public bool OnlyOnDate { get; set; }

        [DisplayField]
        public bool AllowPartial { get; set; }

        [DisplayField]
        public string RevisionReason { get; set; }

        [DisplayField]
        public DateTime? PaymentDueDate { get; set; }

        [DisplayField]
        public System.DateTime DeliveryDate { get; set; }

        [DisplayField]
        public string UniqueKey { get; set; }

        [DisplayField]
        public DateTime Created { get; set; }

        [DisplayField]
        public DateTime Modified { get; set; }

        [DisplayField]
        public bool IsActive { get; set; }

        [DisplayField]
        public float TotalValue
        {
            get
            {
                if (VEOrderItems != null)
                {
                    return (float)Math.Round(VEOrderItems.Where(r => r.ItemLevel == 0).Sum(r =>
                        (r.Quantity * r.Value)
                    ), 2);
                }

                return 0;
            }
        }

        [DisplayField]
        public float TotalValueDiscount
        {
            get
            {
                if (VEOrderItems != null)
                {
                    return (float)Math.Round(TotalValue - VEOrderItems.Where(r => r.ItemLevel == 0).Sum(r =>
                        (r.Quantity * r.Value) * (r.PercDiscount / 100)
                    ), 2);
                }

                return 0;
            }
        }

        [DisplayField]
        public float TotalValueCommission
        {
            //get => (TotalValueWithDiscount * (PercCommission / 100));
            // Alterei para calcular comissão pelos itens para ficar igual ao ERP
            get
            {
                if (VEOrderItems != null && VEOrderItems.Any())
                {
                    return (float)Math.Round(VEOrderItems.Where(r => r.ItemLevel == 0).Sum(r => (r.TotalValueCommission)), 2);
                }

                return 0;
            }
        }

        [DisplayField]
        public decimal TotalValueWithDiscount
        {
            get => Math.Round((decimal)TotalValueDiscount - ((decimal)TotalValueDiscount * ((decimal)PercDiscount / 100)), 2);
        }

        [JsonIgnore]
        public virtual ICollection<VEOrderItemsVM> VEOrderItems { get; set; }
    }
}
