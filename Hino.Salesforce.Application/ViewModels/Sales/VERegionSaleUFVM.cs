﻿using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.Sales
{
    public class VERegionSaleUFVM : BaseVM
    {
        public VERegionSaleUFVM()
        {

        }

        [RequiredField]
        [DisplayField]
        public long RegionId { get; set; }
        public VERegionSaleVM VERegionSale { get; set; }

        [RequiredField]
        [DisplayField]
        public string UF { get; set; }

        public static Expression<Func<VERegionSale, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;
    }
}
