﻿using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.Sales
{
    [EndPoint("Sales/Region/{pEstablishmentKey}")]
    public class VERegionSaleVM : BaseVM
    {
        public VERegionSaleVM()
        {
            this.VESalePrice = new HashSet<VESalePriceVM>();
        }

        [RequiredField]
        [DisplayField]
        public string Description { get; set; }

        [DisplayField]
        public long IdERP { get; set; }

        public virtual ICollection<VESalePriceVM> VESalePrice { get; set; }

        public static Expression<Func<VERegionSale, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter) =>
            r => r.EstablishmentKey == pEstablishmentKey;

    }
}
