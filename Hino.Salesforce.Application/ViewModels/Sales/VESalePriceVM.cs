﻿using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.Sales
{
    [EndPoint("Sales/Prices/{pEstablishmentKey}")]
    public class VESalePriceVM : BaseVM
    {
        public VESalePriceVM()
        {

        }

        [RequiredField]
        [DisplayField]
        public long CodPrVenda { get; set; }
        [ForeignKey("VERegionSale")]

        [RequiredField]
        [DisplayField]
        public long RegionId { get; set; }
        public virtual VERegionSaleVM VERegionSale { get; set; }

        [RequiredField]
        [DisplayField]
        public string Description { get; set; }

        [ForeignKey("GEProducts")]
        [RequiredField]
        [DisplayField]
        public long ProductId { get; set; }

        public virtual GEProductsVM GEProducts { get; set; }

        [RequiredField]
        [DisplayField]
        public decimal Value { get; set; }

        [NotMapped]
        public string ProductKey { get; set; }

        public static Expression<Func<VESalePrice, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                (
                    (
                        (r.Description.ToUpper().Contains(pFilter.Trim())) ||
                        (r.VERegionSale.Id.ToString().ToUpper().Contains(pFilter.Trim())) ||
                        (r.VERegionSale.Description.ToUpper().Contains(pFilter.Trim())) ||
                        (r.GEProducts.ProductKey.ToUpper().Contains(pFilter.Trim())) ||
                        (r.GEProducts.Description.ToUpper().Contains(pFilter.Trim())) ||
                        (r.Value.ToString().Contains(pFilter.Trim()))
                    )
                );

            return r => r.EstablishmentKey == pEstablishmentKey;
        }
    }
}
