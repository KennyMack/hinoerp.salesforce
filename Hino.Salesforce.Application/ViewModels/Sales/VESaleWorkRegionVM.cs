﻿using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.Sales
{
    [EndPoint("Sales/Work/Region/{pEstablishmentKey}")]
    public class VESaleWorkRegionVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        [Max120LengthField]
        public string Description { get; set; }
        [DisplayField]
        [RequiredField]
        [Min8LengthField]
        public string ZIPCodeStart { get; set; }
        [DisplayField]
        [RequiredField]
        [Min8LengthField]
        public string ZIPCodeEnd { get; set; }

        public static Expression<Func<VESaleWorkRegion, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                (
                    r.Id.ToString().Contains(pFilter.Trim()) ||
                    r.Description.ToUpper().Contains(pFilter.Trim()) ||
                    r.ZIPCodeEnd.ToUpper().Contains(pFilter.Trim()) ||
                    r.ZIPCodeStart.ToUpper().Contains(pFilter.Trim())
                );

            return r => r.EstablishmentKey == pEstablishmentKey;
        }
    }
}
