using System;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;

namespace Hino.Salesforce.Application.ViewModels.Sales
{
    public class VETypeSaleVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        [Max120LengthField]
        public string Description { get; set; }
        [DisplayField]
        public long? Iderp { get; set; }
    }
}
