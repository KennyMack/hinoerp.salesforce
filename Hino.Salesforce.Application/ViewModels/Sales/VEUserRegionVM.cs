﻿using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System;
using System.Linq.Expressions;

namespace Hino.Salesforce.Application.ViewModels.Sales
{
    [EndPoint("Sales/User/Region/{pEstablishmentKey}")]
    public class VEUserRegionVM : BaseVM
    {
        [DisplayField]
        [RequiredField]
        public long UserId { get; set; }
        public virtual GEUsersVM GEUsers { get; set; }

        [DisplayField]
        [RequiredField]
        public long SaleWorkId { get; set; }
        public virtual VESaleWorkRegionVM VESaleWorkRegion { get; set; }

        public static Expression<Func<VEUserRegion, bool>> GetDefaultFilter(string pEstablishmentKey, string pFilter)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey) &&
                (
                    r.Id.ToString().Contains(pFilter.Trim()) ||
                    r.UserId.ToString().Contains(pFilter.Trim()) ||
                    r.SaleWorkId.ToString().Contains(pFilter.Trim())
                );

            return r => r.EstablishmentKey == pEstablishmentKey;
        }

        public static Expression<Func<VEUserRegion, bool>> GetDefaultFilterByUser(string pEstablishmentKey, string pFilter, long pUserId)
        {
            if (!pFilter.IsEmpty())
                return r => (r.EstablishmentKey == pEstablishmentKey &&
                r.UserId == pUserId) &&
                (
                    r.Id.ToString().Contains(pFilter.Trim()) ||
                    r.UserId.ToString().Contains(pFilter.Trim()) ||
                    r.SaleWorkId.ToString().Contains(pFilter.Trim())
                );

            return r => r.EstablishmentKey == pEstablishmentKey &&
                r.UserId == pUserId;
        }
    }

    public class VEUserRegionListVM
    {
        public VEUserRegionVM[] results { get; set; }
    }
}
