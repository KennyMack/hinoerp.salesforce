using Hino.Salesforce.Sync.Core.Logging;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Credit.Sync
{
    public class Program
    {
        public static void Main(string[] args)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("pt-BR");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("pt-BR");

            try
            {
                LogData.Info($"Servico iniciado");

                CreateHostBuilder(args).Build().Run();
            }
            catch (Exception ex)
            {
                LogData.Error("Ocorreu um erro na execu��o.", ex, "99999");
                throw;
            }
            finally
            {
                LogData.Info($"Servico finalizado");
            }
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureServices((hostContext, services) =>
                {
                    services.AddSerilogServices("", "credit.sync");
                    services.AddHostedService<Worker>();
                });
    }
}
