using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Sync.Core.Configuration;
using Hino.Salesforce.Sync.Core.DataBase;
using Hino.Salesforce.Sync.Core.Logging;
using Hino.Salesforce.Sync.Models.General;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Hino.Salesforce.Credit.Sync
{
    public class Worker : BackgroundService
    {
        private IConfiguration Configuration { get; }
        private AppSettingsParameters AppSettings { get; }

        public Worker(IConfiguration config)
        {
            Configuration = config;
            AppSettings = Configuration.Get<AppSettingsParameters>();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                LogData.Info($"Iniciando processamento");

                LogData.Info($"{AppSettings.ServiceSettings.DataBases.Length} empresas encontradas para sincronizar");

                if (AppSettings.ServiceSettings.DataBases.Length > 0)
                {
                    LogData.Info($"Percorrendo empresas encontradas para sincronizar");

                    foreach (var ERPDatabase in AppSettings.ServiceSettings.DataBases)
                    {
                        LogData.Info($"Percorrendo empresa: {ERPDatabase.Description}");

                        var ListConfig = new List<GESincConfig>();
                        var ListEstab = new List<GEEstab>();
                        var ContextFactory = new ERPContextFactory();
                        LogData.Info($"Realizando conexao com o banco de dados do ERP ({ERPDatabase.Description}).");
                        using (var ERPContext = ContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
                        {
                            ListEstab = ERPContext.GEEstab.ToList();
                            ListConfig = ERPContext.GESincConfig.ToList();

                            ERPContext.Dispose();
                        }

                        if (ListConfig.Any())
                        {
                            foreach (var item in ListConfig)
                            {
                                var Estab = ListEstab.Where(r => r.codestab == item.codestab).FirstOrDefault();

                                LogData.Info($"Percorrendo: {Estab.codestab} - {Estab.razaosocial}");
                                LogData.Info($"Buscando altera��es no limite de cr�dito");

                                LogData.Info($"Realizando conexao com o banco de dados do ERP ({ERPDatabase.Description}).");
                                using (var ERPContext = ContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
                                {
                                    var LimiteItens = ERPContext.GEVWSincLimiteCredito.Where(r =>
                                        r.CodEstab == Estab.codestab &&
                                        r.EstablishmentKey == item.establishmentkey
                                    );
                                    LogData.Info($"{LimiteItens.Count()} Limites de cr�dito para atualizar");
                                    foreach (var Limite in LimiteItens)
                                    {
                                        LogData.Info($"Atualizando empresa: {Limite.CodEmpresa}");

                                        var SalesContextFactory = new SalesforceContextFactory();

                                        LogData.Info("Realizando conexao com o banco de dados do Salesforce.");
                                        using (var SalesforceContext = SalesContextFactory.CreateDbContext(
                                            AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters()))
                                        {
                                            var Enterprise = SalesforceContext.GEEnterprises.Where(r => r.Id == Limite.IdApi &&
                                            r.EstablishmentKey == item.establishmentkey).FirstOrDefault();

                                            if (Enterprise != null)
                                            {
                                                Enterprise.CreditLimit = Limite.LimiteAtual;
                                                Enterprise.CreditUsed = Limite.LimiteUsadoAtual;

                                                SalesforceContext.Update(Enterprise);
                                                SalesforceContext.SaveChanges();

                                                GESincLimiteCredito _GESincLimiteCredito = ERPContext.GESincLimiteCredito.Where(r =>
                                                    r.codempresa == Limite.CodEmpresa &&
                                                    r.codestab == Limite.CodEstab).FirstOrDefault();
                                                var exists = true;

                                                if (_GESincLimiteCredito == null)
                                                {
                                                    exists = false;
                                                    _GESincLimiteCredito = new GESincLimiteCredito()
                                                    {
                                                        codestab = Limite.CodEstab,
                                                        codempresa = Limite.CodEmpresa
                                                    };
                                                }

                                                _GESincLimiteCredito.datasincronizado = DateTime.Now;
                                                _GESincLimiteCredito.valorlimite = Limite.LimiteAtual;
                                                _GESincLimiteCredito.valorlimiteusado = Limite.LimiteUsadoAtual;

                                                if (exists)
                                                    ERPContext.Update(_GESincLimiteCredito);
                                                else
                                                    ERPContext.Add(_GESincLimiteCredito);
                                                ERPContext.SaveChanges();
                                            }
                                        }

                                    }
                                }

                            }
                        }
                    }

                    LogData.Info($"Finalizando as empresas a sincronizar");
                }

                LogData.Info($"Finalizando processamento");
                LogData.Info($"  ");
                await Task.Delay(AppSettings.Interval, stoppingToken);
            }
        }
    }
}
