﻿using Hino.Salesforce.Infra.Cross.Entities.Auth;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;
using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;
using Hino.Salesforce.Infra.Cross.Entities.Logistics;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Entities.Sales.Transactions;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using Hino.Salesforce.Infra.Cross.Utils.Settings;
using Hino.Salesforce.Infra.DataBase.Exceptions;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.Validation;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.DataBase.Context
{
    [DbConfigurationType(typeof(AppDbContextConfig))]
    public class AppDbContext : DbContext
    {
        public virtual DbSet<FSICMSSTAliqUF> FSICMSSTAliqUF { get; set; }
        public virtual DbSet<FSNCM> FSNCM { get; set; }
        public virtual DbSet<FSFiscalOper> FSFiscalOper { get; set; }
        public virtual DbSet<GECountries> GECountries { get; set; }
        public virtual DbSet<GEStates> GEStates { get; set; }
        public virtual DbSet<GECities> GECities { get; set; }
        public virtual DbSet<GEEnterprises> GEEnterprises { get; set; }
        public virtual DbSet<GEEnterpriseGeo> GEEnterpriseGeo { get; set; }
        public virtual DbSet<GEEstabDevices> GEEstabDevices { get; set; }
        public virtual DbSet<GEEstabMenu> GEEstabMenu { get; set; }
        public virtual DbSet<GEEstablishments> GEEstablishments { get; set; }
        public virtual DbSet<GEFilesPath> GEFilesPath { get; set; }
        public virtual DbSet<GEPaymentCondition> GEPaymentCondition { get; set; }
        public virtual DbSet<GEPaymentCondInstallments> GEPaymentCondInstallments { get; set; }
        public virtual DbSet<GEEnterpriseFiscalGroup> GEEnterpriseFiscalGroup { get; set; }
        public virtual DbSet<GEEnterpriseCategory> GEEnterpriseCategory { get; set; }
        public virtual DbSet<GEEnterpriseGroup> GEEnterpriseGroup { get; set; }
        public virtual DbSet<GEEnterpriseAnnot> GEEnterpriseAnnot { get; set; }
        public virtual DbSet<GEPaymentType> GEPaymentType { get; set; }
        public virtual DbSet<GEProducts> GEProducts { get; set; }
        public virtual DbSet<GEProductsFamily> GEProductsFamily { get; set; }
        public virtual DbSet<GEProductsType> GEProductsType { get; set; }
        public virtual DbSet<GEProductsUnit> GEProductsUnit { get; set; }
        public virtual DbSet<GEUsers> GEUsers { get; set; }
        public virtual DbSet<GEUserEnterprises> GEUserEnterprises { get; set; }
        public virtual DbSet<GEUserPayType> GEUserPayType { get; set; }
        public virtual DbSet<VEOrderItems> VEOrderItems { get; set; }
        public virtual DbSet<VEOrders> VEOrders { get; set; }
        public virtual DbSet<VEUserRegion> VEUserRegion { get; set; }
        public virtual DbSet<VERegionSale> VERegionSale { get; set; }
        public virtual DbSet<VERegionSaleUF> VERegionSaleUF { get; set; }
        public virtual DbSet<VESalePrice> VESalePrice { get; set; }
        public virtual DbSet<VEEntSalePrice> VEEntSalePrice { get; set; }
        public virtual DbSet<VESaleWorkRegion> VESaleWorkRegion { get; set; }
        public virtual DbSet<AURefreshToken> AURefreshToken { get; set; }
        public virtual DbSet<AUExpiredToken> AUExpiredToken { get; set; }
        public virtual DbSet<LORoutes> LORoutes { get; set; }
        public virtual DbSet<LOTrips> LOTrips { get; set; }
        public virtual DbSet<LOWaypoints> LOWaypoints { get; set; }
        public virtual DbSet<VETransactions> VETransactions { get; set; }
        public virtual DbSet<GEProductKey> GEProductKey { get; set; }

        public virtual DbSet<GESettings> GESettings { get; set; }
        public virtual DbSet<GEBoards> GEBoards { get; set; }
        public virtual DbSet<GEBoardUsers> GEBoardUsers { get; set; }
        public virtual DbSet<GEBoardLists> GEBoardLists { get; set; }
        public virtual DbSet<GESettingsTags> GESettingsTags { get; set; }
        public virtual DbSet<GECardFields> GECardFields { get; set; }
        public virtual DbSet<GEFieldUsers> GEFieldUsers { get; set; }

        public virtual DbSet<GEEnterpriseEvent> GEEnterpriseEvent { get; set; }
        public virtual DbSet<GEEstabCalendar> GEEstabCalendar { get; set; }
        public virtual DbSet<GEEvents> GEEvents { get; set; }
        public virtual DbSet<GEEventsClassification> GEEventsClassification { get; set; }
        public virtual DbSet<GEUserCalendar> GEUserCalendar { get; set; }
        public virtual DbSet<VETypeSale> VETypeSale { get; set; }


        public AppDbContext() : base("name=AppDbContext")
        {
            Database.SetInitializer<AppDbContext>(null);
            this.Database.Log = Console.Write;
            this.Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //ConfigurationManager.AppSettings["DEFAULTSCHEMA"]
            modelBuilder.HasDefaultSchema(AppSettings.DEFAULTSCHEMA);
            // ConfigurationManager.AppSettings["DEFAULTSCHEMA"]);

            modelBuilder.Types().Configure(entity => entity.ToTable(entity.ClrType.Name.ToUpper()));

            modelBuilder.Properties().Configure(c =>
            {
                c.HasColumnName(c.ClrPropertyInfo.Name.ToUpper());
            });

            modelBuilder.Entity<LORoutes>().
                Property(p => p.Weigth)
                .HasPrecision(14, 10);
            modelBuilder.Entity<LORoutes>().
                Property(p => p.Duration)
                .HasPrecision(14, 10);
            modelBuilder.Entity<LORoutes>().
                Property(p => p.Distance)
                .HasPrecision(14, 10);

            modelBuilder.Entity<LOTrips>().
                Property(p => p.Lat)
                .HasPrecision(14, 10);
            modelBuilder.Entity<LOTrips>().
                Property(p => p.Lng)
                .HasPrecision(14, 10);

            modelBuilder.Entity<LOWaypoints>().
                Property(p => p.Lat)
                .HasPrecision(14, 10);
            modelBuilder.Entity<LOWaypoints>().
                Property(p => p.Lng)
                .HasPrecision(14, 10);
            modelBuilder.Entity<LOWaypoints>().
                Property(p => p.Distance)
                .HasPrecision(14, 10);

            modelBuilder.Entity<GEEnterpriseGeo>().
                Property(p => p.DisplayLat)
                .HasPrecision(14, 10);
            modelBuilder.Entity<GEEnterpriseGeo>().
                Property(p => p.DisplayLng)
                .HasPrecision(14, 10);
            modelBuilder.Entity<GEEnterpriseGeo>().
                Property(p => p.NavLat)
                .HasPrecision(14, 10);
            modelBuilder.Entity<GEEnterpriseGeo>().
                Property(p => p.NavLng)
                .HasPrecision(14, 10);

            modelBuilder.Entity<GECountries>().
                Property(p => p.Lat)
                .HasPrecision(14, 10);
            modelBuilder.Entity<GECountries>().
                Property(p => p.Lng)
                .HasPrecision(14, 10);

            modelBuilder.Entity<GEStates>().
                Property(p => p.Lat)
                .HasPrecision(14, 10);
            modelBuilder.Entity<GEStates>().
                Property(p => p.Lng)
                .HasPrecision(14, 10);

            base.OnModelCreating(modelBuilder);
        }

        void GetNextSequence(DbEntityEntry pEntity)
        {
            if (pEntity.Property("Id").CurrentValue.ToString() == "0")
            {
                var sql = $"SELECT SEQ_{pEntity.Entity.GetType().Name}.NEXTVAL FROM DUAL";
                var idItem = this.Database.SqlQuery<long>(sql);

                pEntity.Property("Id").CurrentValue = idItem.First();
            }
        }

        void DefaultData()
        {
            var props = ChangeTracker.Entries()
                             .Where(p =>
                             p.Entity.GetType().GetProperty("Created") != null
                             );

            foreach (var item in props)
            {
                switch (item.State)
                {
                    case EntityState.Detached:
                        break;
                    case EntityState.Unchanged:
                        if (item.Property("Id").CurrentValue.ToString() == "0")
                        {
                            item.State = EntityState.Added;
                            item.Property("Created").CurrentValue = DateTime.Now;
                            item.Property("Modified").CurrentValue = DateTime.Now;
                            item.Property("IsActive").CurrentValue = true;
                            item.Property("UniqueKey").CurrentValue = Guid.NewGuid().ToString();
                            GetNextSequence(item);
                        }
                        else
                        {
                            item.Property("Created").IsModified = false;
                            item.Property("Modified").CurrentValue = DateTime.Now;
                            item.State = EntityState.Modified;
                        }
                        break;
                    case EntityState.Added:
                        item.Property("Created").CurrentValue = DateTime.Now;
                        item.Property("Modified").CurrentValue = DateTime.Now;
                        item.Property("IsActive").CurrentValue = true;
                        item.Property("UniqueKey").CurrentValue = Guid.NewGuid().ToString();
                        GetNextSequence(item);
                        break;
                    case EntityState.Deleted:
                        // item.Property("IsActive").CurrentValue = false;
                        // item.State = EntityState.Modified;
                        break;
                    case EntityState.Modified:
                        item.Property("Created").IsModified = false;
                        item.Property("Modified").CurrentValue = DateTime.Now;
                        break;
                    default:
                        break;
                }
            }
        }

        public override async Task<int> SaveChangesAsync()
        {
            var ret = -1;
            DefaultData();
            try
            {
                ret = await base.SaveChangesAsync();
            }
            catch (DbEntityValidationException e)
            {
                Cross.Utils.Exceptions.Logging.Exception(e);
                throw new ModelEntityValidationException(e);
            }
            return ret;
        }

        public override int SaveChanges()
        {
            DefaultData();
            try
            {
                return base.SaveChanges();
            }
            catch (DbEntityValidationException e)
            {
                Cross.Utils.Exceptions.Logging.Exception(e);
                throw new ModelEntityValidationException(e);
            }
        }

        #region Generate Entry Queue
        private void GenerateEntryQueue(DbEntityEntry entry)
        {
            var EntryName = "NONE";
            try
            {
                EntryName =
                    ((QueueAttribute)entry.Entity.GetType().GetCustomAttributes(typeof(QueueAttribute), false).FirstOrDefault()).Queue;
            }
            catch (Exception)
            {
                try
                {
                    var custom = (QueueAttribute)(entry.Entity.GetType().BaseType.GetCustomAttributes(typeof(QueueAttribute), true).First());

                    EntryName = custom.Queue;
                    // ((QueueAttribute)(entry.Entity.GetType().BaseType.CustomAttributes.Where(r => r.GetType() == typeof(QueueAttribute))).FirstOrDefault().).Queue;
                }
                catch (Exception)
                {
                    EntryName = "NONE";
                }
            }

            if (EntryName != "NONE")
            {

                // var Configuration = ConfigurationManager.AppSettings;
                // 
                // var HostName = Configuration.GetValues("RABBIT_URL").FirstOrDefault() ?? "192.168.1.1";
                // var UserName = Configuration.GetValues("RABBIT_USER").FirstOrDefault() ?? "ADMIN";
                // var Password = Configuration.GetValues("RABBIT_PASS").FirstOrDefault() ?? "HOME";
                // var Port = ConvertEx.ToInt32(Configuration.GetValues("RABBIT_PORT").FirstOrDefault()) ?? 15672;

                var factory = new ConnectionFactory()
                {
                    HostName = AppSettings.RABBIT_URL ?? "192.168.1.1",
                    UserName = AppSettings.RABBIT_USER ?? "ADMIN",
                    Password = AppSettings.RABBIT_PASS ?? "HOME"
                };

                using (var conn = factory.CreateConnection())
                using (var channel = conn.CreateModel())
                {
                    var listType = "NONE";
                    switch (entry.State)
                    {
                        case EntityState.Detached:
                            listType = "DELETE";
                            break;
                        case EntityState.Unchanged:
                            listType = "NEW";
                            break;
                        case EntityState.Added:
                            listType = "NEW";
                            break;
                        case EntityState.Deleted:
                            listType = "DELETE";
                            break;
                        case EntityState.Modified:
                            listType = "CHANGE";
                            break;
                        default:
                            break;
                    }

                    if (listType != "NONE")
                    {
                        var queue = $"{listType}_{EntryName}";
                        channel.QueueDeclare(
                           queue: queue,
                           durable: true,
                           exclusive: false,
                           autoDelete: false,
                           arguments: null);

                        var properties = channel.CreateBasicProperties();
                        properties.Persistent = true;
                        properties.DeliveryMode = 2;

                        var obj = JsonConvert.SerializeObject(entry.Entity, new JsonSerializerSettings
                        {
                            Culture = new System.Globalization.CultureInfo("pt-BR"),
                            Formatting = Formatting.Indented,
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
                        var body = Encoding.UTF8.GetBytes(obj);

                        channel.BasicPublish(
                            exchange: "",
                            routingKey: queue,
                            basicProperties: properties,
                            body: body);
                    }
                }
            }
        }
        #endregion

    }
}
