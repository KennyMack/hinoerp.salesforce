﻿using System;
using System.Data;
using System.Data.Common;
using System.Data.Entity.Infrastructure.DependencyResolution;
using System.Data.Entity.Infrastructure.Interception;

namespace Hino.Salesforce.Infra.DataBase.Context
{
    public class ConnectionInterceptor : IDbConnectionInterceptor
    {

        public void BeganTransaction(DbConnection connection, BeginTransactionInterceptionContext interceptionContext)
        {
            //throw new NotImplementedException();
        }

        public void BeginningTransaction(DbConnection connection, BeginTransactionInterceptionContext interceptionContext)
        {
            //throw new NotImplementedException();
        }

        public void Closed(DbConnection connection, DbConnectionInterceptionContext interceptionContext)
        {
            // throw new NotImplementedException();
        }

        public void Closing(DbConnection connection, DbConnectionInterceptionContext interceptionContext)
        {
            //throw new NotImplementedException();
        }

        public void ConnectionStringGetting(DbConnection connection, DbConnectionInterceptionContext<string> interceptionContext)
        {
            connection.ConnectionString = ConnectionStringBuilder.Construct();
            // connection.ConnectionString = connection.ConnectionString.Replace("#DBCONNECTION#", "bdh1n0s4l3sf0rc319");
            //bdh1n0s4l3sf0rc319

            // throw new NotImplementedException();
        }

        public void ConnectionStringGot(DbConnection connection, DbConnectionInterceptionContext<string> interceptionContext)
        {
            // throw new NotImplementedException();
        }

        public void ConnectionStringSet(DbConnection connection, DbConnectionPropertyInterceptionContext<string> interceptionContext)
        {
            //throw new NotImplementedException();
        }

        public void ConnectionStringSetting(DbConnection connection, DbConnectionPropertyInterceptionContext<string> interceptionContext)
        {
            // throw new NotImplementedException();
        }

        public void ConnectionTimeoutGetting(DbConnection connection, DbConnectionInterceptionContext<int> interceptionContext)
        {
            //throw new NotImplementedException();
        }

        public void ConnectionTimeoutGot(DbConnection connection, DbConnectionInterceptionContext<int> interceptionContext)
        {
            //throw new NotImplementedException();
        }

        public void DatabaseGetting(DbConnection connection, DbConnectionInterceptionContext<string> interceptionContext)
        {
            // throw new NotImplementedException();
        }

        public void DatabaseGot(DbConnection connection, DbConnectionInterceptionContext<string> interceptionContext)
        {
            // throw new NotImplementedException();
        }

        public void DataSourceGetting(DbConnection connection, DbConnectionInterceptionContext<string> interceptionContext)
        {
            //throw new NotImplementedException();
        }

        public void DataSourceGot(DbConnection connection, DbConnectionInterceptionContext<string> interceptionContext)
        {
            //throw new NotImplementedException();
        }

        public void Disposed(DbConnection connection, DbConnectionInterceptionContext interceptionContext)
        {
            //throw new NotImplementedException();
        }

        public void Disposing(DbConnection connection, DbConnectionInterceptionContext interceptionContext)
        {
            //throw new NotImplementedException();
        }

        public void EnlistedTransaction(DbConnection connection, EnlistTransactionInterceptionContext interceptionContext)
        {
            //throw new NotImplementedException();
        }

        public void EnlistingTransaction(DbConnection connection, EnlistTransactionInterceptionContext interceptionContext)
        {
            //throw new NotImplementedException();
        }

        public void Loaded(DbConfigurationLoadedEventArgs loadedEventArgs, DbConfigurationInterceptionContext interceptionContext)
        {

            //throw new NotImplementedException();
        }

        public void Opened(DbConnection connection, DbConnectionInterceptionContext interceptionContext)
        {
            //connection.ConnectionString = ConnectionStringBuilder.Construct();
            // throw new NotImplementedException();
        }

        public void Opening(DbConnection connection, DbConnectionInterceptionContext interceptionContext)
        {
            connection.ConnectionString = ConnectionStringBuilder.Construct();
            //throw new NotImplementedException();
        }

        public void ServerVersionGetting(DbConnection connection, DbConnectionInterceptionContext<string> interceptionContext)
        {
            //throw new NotImplementedException();
        }

        public void ServerVersionGot(DbConnection connection, DbConnectionInterceptionContext<string> interceptionContext)
        {
            //throw new NotImplementedException();
        }

        public void StateGetting(DbConnection connection, DbConnectionInterceptionContext<ConnectionState> interceptionContext)
        {
            // throw new NotImplementedException();
        }

        public void StateGot(DbConnection connection, DbConnectionInterceptionContext<ConnectionState> interceptionContext)
        {
            // throw new NotImplementedException();
        }
    }

    public class CommandLogInterceptor : IDbCommandInterceptor
    {
        private void LogInfo(string command, string commandText)
        {
            // NLog.LogManager.GetCurrentClassLogger().Info($"DB Command: {command} :- {commandText} ");
        }

        public void NonQueryExecuted(System.Data.Common.DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            LogInfo("NonQueryExecuted", String.Format(" IsAsync: {0}, Command Text: {1}", interceptionContext.IsAsync, command.CommandText));
        }

        public void NonQueryExecuting(System.Data.Common.DbCommand command, DbCommandInterceptionContext<int> interceptionContext)
        {
            LogInfo("NonQueryExecuting", String.Format(" IsAsync: {0}, Command Text: {1}", interceptionContext.IsAsync, command.CommandText));
        }

        public void ReaderExecuted(System.Data.Common.DbCommand command, DbCommandInterceptionContext<System.Data.Common.DbDataReader> interceptionContext)
        {
            LogInfo("ReaderExecuted", String.Format(" IsAsync: {0}, Command Text: {1}", interceptionContext.IsAsync, command.CommandText));
        }

        public void ReaderExecuting(System.Data.Common.DbCommand command, DbCommandInterceptionContext<System.Data.Common.DbDataReader> interceptionContext)
        {
            LogInfo("ReaderExecuting", String.Format(" IsAsync: {0}, Command Text: {1}", interceptionContext.IsAsync, command.CommandText));
        }

        public void ScalarExecuted(System.Data.Common.DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            LogInfo("ScalarExecuted", String.Format(" IsAsync: {0}, Command Text: {1}", interceptionContext.IsAsync, command.CommandText));
        }

        public void ScalarExecuting(System.Data.Common.DbCommand command, DbCommandInterceptionContext<object> interceptionContext)
        {
            LogInfo("ScalarExecuting", String.Format(" IsAsync: {0}, Command Text: {1}", interceptionContext.IsAsync, command.CommandText));
        }
    }
}
