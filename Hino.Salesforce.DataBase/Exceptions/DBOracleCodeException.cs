﻿using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Text.RegularExpressions;

namespace Hino.Salesforce.Infra.DataBase.Exceptions
{
    public class DBOracleCodeException : Exception
    {
        private readonly OracleException orclException;
        private readonly string _Message;
        public override string StackTrace => orclException?.StackTrace;
        public override string HelpLink { get => base.HelpLink; set => base.HelpLink = value; }
        public override string Message => _Message;

        public DBOracleCodeException(OracleException baseException)
        {
            orclException = baseException;
            base.HelpLink = null;
            base.Source = orclException.Source;

            switch (orclException.Number)
            {
                case 1:
                    _Message = Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA00001;
                    break;
                case 911:
                    _Message = Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA00911;
                    break;
                case 932:
                    _Message = Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA00932;
                    break;
                case 947:
                    _Message = Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA00947;
                    break;
                case 1000:
                    _Message = Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA01000;
                    break;
                case 1400:
                    _Message = Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA01400;
                    break;
                case 1403:
                    _Message = Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA01403;
                    break;
                case 1422:
                    _Message = Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA01422;
                    break;
                case 1438:
                    _Message = Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA01438;
                    break;
                case 1476:
                    _Message = Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA01476;
                    break;
                case 1722:
                    _Message = Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA01722;
                    break;
                case 2292:
                    var TableName = GetChildTable(orclException.Message);
                    _Message = string.Format(
                        Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA02292,
                        TableName);
                    break;
                case 3113:
                    _Message = Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA03113;
                    break;
                case 3135:
                    _Message = Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA03135;
                    break;
                case 12154:
                    _Message = Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA12154;
                    break;
                case 12537:
                    _Message = Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA12537;
                    break;
                case 12899:
                    _Message = Hino.Salesforce.Infra.Cross.Resources.OracleExceptionMessagesResource.ORA12899;
                    break;
                default:
                    _Message = orclException.Message;
                    break;
            }
        }

        private string GetChildTable(string pMessage)
        {
            var reg = new Regex(@"(\.)([A-Z_]+)");

            var matches = reg.Matches(pMessage);

            if (matches.Count > 0)
            {
                foreach (Match item in matches)
                {
                    try
                    {
                        var TBL = item.Value.Split('_')[1];

                        if (TBL == ETableNames.AUREFRESHTOKEN.ToString())
                        {
                            return $" na tabela de {ETableNames.AUREFRESHTOKEN.GetDescription()}";
                        }
                        else if (TBL == ETableNames.AUEXPIREDTOKEN.ToString())
                        {
                            return $" na tabela de {ETableNames.AUEXPIREDTOKEN.GetDescription()}";
                        }
                        else if (TBL == ETableNames.FSICMSSTALIQUF.ToString())
                        {
                            return $" na tabela de {ETableNames.FSICMSSTALIQUF.GetDescription()}";
                        }
                        else if (TBL == ETableNames.GEENTERPRISES.ToString())
                        {
                            return $" na tabela de {ETableNames.GEENTERPRISES.GetDescription()}";
                        }
                        else if (TBL == ETableNames.GEPAYMENTCONDITION.ToString())
                        {
                            return $" na tabela de {ETableNames.GEPAYMENTCONDITION.GetDescription()}";
                        }
                        else if (TBL == ETableNames.GEPAYMENTTYPE.ToString())
                        {
                            return $" na tabela de {ETableNames.GEPAYMENTTYPE.GetDescription()}";
                        }
                        else if (TBL == ETableNames.GECITIES.ToString())
                        {
                            return $" na tabela de {ETableNames.GECITIES.GetDescription()}";
                        }
                        else if (TBL == ETableNames.GESTATES.ToString())
                        {
                            return $" na tabela de {ETableNames.GESTATES.GetDescription()}";
                        }
                        else if (TBL == ETableNames.GECOUNTRIES.ToString())
                        {
                            return $" na tabela de {ETableNames.GECOUNTRIES.GetDescription()}";
                        }
                        else if (TBL == ETableNames.GEUSERS.ToString())
                        {
                            return $" na tabela de {ETableNames.GEUSERS.GetDescription()}";
                        }
                        else if (TBL == ETableNames.GEESTABDEVICES.ToString())
                        {
                            return $" na tabela de {ETableNames.GEESTABDEVICES.GetDescription()}";
                        }
                        else if (TBL == ETableNames.GEESTABLISHMENTS.ToString())
                        {
                            return $" na tabela de {ETableNames.GEESTABLISHMENTS.GetDescription()}";
                        }
                        else if (TBL == ETableNames.GEPRODUCTS.ToString())
                        {
                            return $" na tabela de {ETableNames.GEPRODUCTS.GetDescription()}";
                        }
                        else if (TBL == ETableNames.VESALEPRICE.ToString())
                        {
                            return $" na tabela de {ETableNames.VESALEPRICE.GetDescription()}";
                        }
                        else if (TBL == ETableNames.VEORDERITEMS.ToString())
                        {
                            return $" na tabela de {ETableNames.VEORDERITEMS.GetDescription()}";
                        }
                        else if (TBL == ETableNames.VEORDERS.ToString())
                        {
                            return $" na tabela de {ETableNames.VEORDERS.GetDescription()}";
                        }
                        else if (TBL == ETableNames.VEREGIONSALE.ToString())
                        {
                            return $" na tabela de {ETableNames.VEREGIONSALE.GetDescription()}";
                        }
                        else if (TBL == ETableNames.LOROUTES.ToString())
                        {
                            return $" na tabela de {ETableNames.LOROUTES.GetDescription()}";
                        }
                        else if (TBL == ETableNames.LOTRIPS.ToString())
                        {
                            return $" na tabela de {ETableNames.LOTRIPS.GetDescription()}";
                        }
                        else if (TBL == ETableNames.LOWAYPOINTS.ToString())
                        {
                            return $" na tabela de {ETableNames.LOWAYPOINTS.GetDescription()}";
                        }
                    }
                    catch (Exception)
                    {

                    }
                }
            }

            return "";
        }

        public override string ToString()
        {
            return $"{_Message} - ({orclException.Message})";
        }
    }
}
