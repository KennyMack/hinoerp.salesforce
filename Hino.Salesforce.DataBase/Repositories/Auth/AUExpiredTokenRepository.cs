using Hino.Salesforce.Domain.Auth.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Auth;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.Auth
{
    public class AUExpiredTokenRepository : BaseRepository<AUExpiredToken>, IAUExpiredTokenRepository
    {
        public AUExpiredTokenRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
