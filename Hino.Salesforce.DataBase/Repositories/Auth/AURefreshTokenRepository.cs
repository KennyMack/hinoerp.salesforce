using Hino.Salesforce.Domain.Auth.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Auth;
using Hino.Salesforce.Infra.DataBase.Context;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.DataBase.Repositories.Auth
{
    public class AURefreshTokenRepository : BaseRepository<AURefreshToken>, IAURefreshTokenRepository
    {
        public AURefreshTokenRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<AURefreshToken> GetBySessionId(string sessionId, params Expression<Func<AURefreshToken, object>>[] includeProperties)
        {
            IQueryable<AURefreshToken> query = base.DbEntity;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return await query.Where(r => r.SessionID == sessionId).FirstAsync();
        }

        public async Task<AURefreshToken> GetByRefreshToken(string sessionId, string refreshTokenId)
        {
            return await DbEntity.Where(r =>
                ((r.SessionID == sessionId) &&
                (r.RefreshTokenID == refreshTokenId))
            ).FirstAsync();
        }
    }
}
