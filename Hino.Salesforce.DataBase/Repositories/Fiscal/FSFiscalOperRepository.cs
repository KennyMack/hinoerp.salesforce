using Hino.Salesforce.Domain.Fiscal.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.Fiscal
{
    public class FSFiscalOperRepository : BaseRepository<FSFiscalOper>, IFSFiscalOperRepository
    {
        public FSFiscalOperRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
