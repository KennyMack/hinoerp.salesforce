﻿using Hino.Salesforce.Domain.Fiscal.Interfaces.Repositories.Taxes;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.Fiscal.Taxes
{
    public class FSNCMRepository : BaseRepository<FSNCM>, IFSNCMRepository
    {
        public FSNCMRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
