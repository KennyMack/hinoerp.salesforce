﻿using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General.Business
{
    public class GEEnterpriseAnnotationRepository : BaseRepository<GEEnterpriseAnnot>, IGEEnterpriseAnnotationRepository
    {
        public GEEnterpriseAnnotationRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        DbRawSqlQuery<long> GetNextSequency()
        {
            var sql = $"SELECT SEQ_GEENTERPRISEANNOT.NEXTVAL FROM DUAL";

            return DbConn.Database.SqlQuery<long>(sql);
        }

        public override long NextSequence() =>
            GetNextSequency().First();

        public async override Task<long> NextSequenceAsync() =>
            await GetNextSequency().FirstAsync();
    }
}
