using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General.Business
{
    public class GEEnterpriseFiscalGroupRepository : BaseRepository<GEEnterpriseFiscalGroup>, IGEEnterpriseFiscalGroupRepository
    {
        public GEEnterpriseFiscalGroupRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
