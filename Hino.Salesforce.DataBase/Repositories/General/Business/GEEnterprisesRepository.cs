using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.DataBase.Context;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General.Business
{
    public class GEEnterprisesRepository : BaseRepository<GEEnterprises>, IGEEnterprisesRepository
    {
        public GEEnterprisesRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task UpdateEnterpriseRegionIdAsync(string pEstablishmentKey, string pUF, long pRegionId)
        {
            var sql = @"UPDATE GEENTERPRISES
                  SET GEENTERPRISES.REGIONID         = :pREGIONID
                WHERE GEENTERPRISES.ESTABLISHMENTKEY = :pESTABLISHMENTKEY
                  AND EXISTS (SELECT 1
                                FROM GEENTERPRISEGEO
                               WHERE GEENTERPRISEGEO.ESTABLISHMENTKEY = GEENTERPRISES.ESTABLISHMENTKEY
                                 AND GEENTERPRISEGEO.ENTERPRISEID     = GEENTERPRISES.ID
                                 AND GEENTERPRISEGEO.TYPE             = 0
                                 AND GEENTERPRISEGEO.UF               = :pUF)";

            await DbConn.Database.ExecuteSqlCommandAsync(sql,
                new OracleParameter("pREGIONID", OracleDbType.Int64, pRegionId, ParameterDirection.Input),
                new OracleParameter("pESTABLISHMENTKEY", OracleDbType.Varchar2, pEstablishmentKey, ParameterDirection.Input),
                new OracleParameter("pUF", OracleDbType.Varchar2, pUF, ParameterDirection.Input)
            );
        }

        public override void Add(GEEnterprises model)
        {
            if (model.Id == 0)
                model.Id = NextSequence();
            model.search = $"{model.Id}|{model.RazaoSocial}|{model.NomeFantasia}|{model.CNPJCPF}|{model.IE}|{model.ClassifEmpresa}"
                .RemoveAccent()
                .ToUpper();
            base.Add(model);
        }

        public override void Update(GEEnterprises model)
        {
            model.search = $"{model.Id}|{model.RazaoSocial}|{model.NomeFantasia}|{model.CNPJCPF}|{model.IE}|{model.ClassifEmpresa}"
                .RemoveAccent()
                .ToUpper();
            base.Update(model);
        }

        public async Task<GEEnterprises> GetEnterprise(string pEstablishmentKey, long pId)
        {
            return (await QueryAsync(r =>
                r.Id == pId &&
                r.EstablishmentKey == pEstablishmentKey,
                s => s.GEPaymentCondition,
                x => x.GEPaymentCondition.GEPaymentType,
                g => g.GEEnterpriseGeo,
                t => t.GEEnterpriseContacts,
                j => j.GEEnterpriseCategory,
                o => o.GEEnterpriseGroup,
                s => s.VETypeSale,
                h => h.GEEnterpriseFiscalGroup,
                a => a.GEEnterpriseAnnotation
            )).FirstOrDefault();
        }

        /*public async Task<PagedResult<GEEnterprises>> SearchPagedAsync(string pEstablishmentKey, string pSearch)
        {
            var sql = @"UPDATE GEENTERPRISES
                  SET GEENTERPRISES.REGIONID         = :pREGIONID
                WHERE GEENTERPRISES.ESTABLISHMENTKEY = :pESTABLISHMENTKEY
                  AND EXISTS (SELECT 1
                                FROM GEENTERPRISEGEO
                               WHERE GEENTERPRISEGEO.ESTABLISHMENTKEY = GEENTERPRISES.ESTABLISHMENTKEY
                                 AND GEENTERPRISEGEO.ENTERPRISEID     = GEENTERPRISES.ID
                                 AND GEENTERPRISEGEO.TYPE             = 0
                                 AND GEENTERPRISEGEO.UF               = :pUF)";

            await DbConn.Database.SqlQuery(sql,
                new OracleParameter("pREGIONID", OracleDbType.Int64, pRegionId, ParameterDirection.Input),
                new OracleParameter("pESTABLISHMENTKEY", OracleDbType.Varchar2, pEstablishmentKey, ParameterDirection.Input),
                new OracleParameter("pUF", OracleDbType.Varchar2, pUF, ParameterDirection.Input)
            );

        }*/
    }
}
