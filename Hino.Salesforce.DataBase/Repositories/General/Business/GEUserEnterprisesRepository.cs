using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using Hino.Salesforce.Infra.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General.Business
{
    public class GEUserEnterprisesRepository : BaseRepository<GEUserEnterprises>, IGEUserEnterprisesRepository
    {
        public GEUserEnterprisesRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<PagedResult<GEUserEnterprises>> QuerySearchPagedAsync(int page, int pageSize,
            Expression<Func<GEUserEnterprises, bool>> predicate,
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
            await PaginateQueryAsync(pAddQueryProperties(DbEntity
                .Include(r => r.GEEnterprises)
                .Include(r => r.GEUsers), includeProperties)
                .AsNoTracking()
                .Where(predicate), page, pageSize);

        public async override Task<PagedResult<GEUserEnterprises>> QueryPagedAsync(int page, int pageSize,
            Expression<Func<GEUserEnterprises, bool>> predicate,
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
            await PaginateQueryAsync(pAddQueryProperties(DbEntity
                .Include(r => r.GEEnterprises)
                .Include(r => r.GEUsers)
                .Include("GEEnterprises.GEPaymentCondition")
                .Include("GEEnterprises.GEPaymentCondition.GEPaymentType")
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts")
                .Include("GEEnterprises.GEEnterpriseCategory")
                .Include("GEEnterprises.GEEnterpriseGroup")
                .Include("GEEnterprises.GEEnterpriseFiscalGroup"), includeProperties)
                .AsNoTracking()
                .Where(predicate), page, pageSize);

        public override async Task<PagedResult<GEUserEnterprises>> GetAllPagedAsync(int page, int pageSize,
                params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
            await PaginateQueryAsync(pAddQueryProperties(DbEntity
                .Include(r => r.GEEnterprises)
                .Include(r => r.GEUsers)
                .Include("GEEnterprises.GEPaymentCondition")
                .Include("GEEnterprises.GEPaymentCondition.GEPaymentType")
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts")
                .Include("GEEnterprises.GEEnterpriseCategory")
                .Include("GEEnterprises.GEEnterpriseGroup")
                .Include("GEEnterprises.GEEnterpriseFiscalGroup"), includeProperties)
                .AsNoTracking(), page, pageSize);

        public override async Task<IEnumerable<GEUserEnterprises>> QueryAsync(
            Expression<Func<GEUserEnterprises, bool>> predicate,
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
            await pAddQueryProperties(DbEntity
                .Include(r => r.GEEnterprises)
                .Include(r => r.GEUsers)
                .Include("GEEnterprises.GEPaymentCondition")
                .Include("GEEnterprises.GEPaymentCondition.GEPaymentType")
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts")
                .Include("GEEnterprises.GEEnterpriseCategory")
                .Include("GEEnterprises.GEEnterpriseGroup")
                .Include("GEEnterprises.GEEnterpriseFiscalGroup"), includeProperties)
                .Where(predicate)
                .AsNoTracking()
                .ToListAsync();

        public override async Task<IEnumerable<GEUserEnterprises>> GetAllAsync(
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
            await pAddQueryProperties(DbEntity
                .Include(r => r.GEEnterprises)
                .Include(r => r.GEUsers)
                .Include("GEEnterprises.GEPaymentCondition")
                .Include("GEEnterprises.GEPaymentCondition.GEPaymentType")
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts")
                .Include("GEEnterprises.GEEnterpriseCategory")
                .Include("GEEnterprises.GEEnterpriseGroup")
                .Include("GEEnterprises.GEEnterpriseFiscalGroup"), includeProperties).AsNoTracking().ToListAsync();

        public override async Task<GEUserEnterprises> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey,
             params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
             await DbEntity
                .Include(r => r.GEEnterprises)
                .Include(r => r.GEUsers)
                .Include("GEEnterprises.GEPaymentCondition")
                .Include("GEEnterprises.GEPaymentCondition.GEPaymentType")
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts")
                .Include("GEEnterprises.GEEnterpriseCategory")
                .Include("GEEnterprises.GEEnterpriseGroup")
                .Include("GEEnterprises.GEEnterpriseFiscalGroup")
                .AsNoTracking().Where(r =>
                       r.Id == id &&
                       r.EstablishmentKey == pEstablishmentKey &&
                       r.UniqueKey == pUniqueKey).FirstOrDefaultAsync();

        IQueryable<GEUserEnterprises> pAddQueryProperties(IQueryable<GEUserEnterprises> query,
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties)
        {
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public override GEUserEnterprises GetById(long id, string pEstablishmentKey, string pUniqueKey,
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
            pAddQueryProperties(DbEntity
                .Include(r => r.GEEnterprises)
                .Include(r => r.GEUsers)
                .Include("GEEnterprises.GEPaymentCondition")
                .Include("GEEnterprises.GEPaymentCondition.GEPaymentType")
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts")
                .Include("GEEnterprises.GEEnterpriseCategory")
                .Include("GEEnterprises.GEEnterpriseGroup")
                .Include("GEEnterprises.GEEnterpriseFiscalGroup"), includeProperties)
                .Where(r =>
                       r.Id == id &&
                       r.EstablishmentKey == pEstablishmentKey &&
                       r.UniqueKey == pUniqueKey)
                .AsNoTracking().FirstOrDefault();
    }
}
