﻿using Hino.Salesforce.Domain.General.Interfaces.Repositories.Demograph;
using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General.Demograph
{
    public class GECitiesRepository : BaseRepository<GECities>, IGECitiesRepository
    {
        public GECitiesRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
