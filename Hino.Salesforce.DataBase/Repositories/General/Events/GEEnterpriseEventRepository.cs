using Hino.Salesforce.Domain.General.Events.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Infra.DataBase.Context;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General.Events
{
    public class GEEnterpriseEventRepository : BaseRepository<GEEnterpriseEvent>, IGEEnterpriseEventRepository
    {
        public GEEnterpriseEventRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async override Task<PagedResult<GEEnterpriseEvent>> QueryPagedAsync(int page, int pageSize,
            Expression<Func<GEEnterpriseEvent, bool>> predicate,
            params Expression<Func<GEEnterpriseEvent, object>>[] includeProperties) =>
            await PaginateQueryAsync(pAddQueryProperties(DbEntity
                .Include(e => e.GEEnterprises)
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments"), includeProperties)
                            .AsNoTracking()
                            .Where(predicate), page, pageSize);

        public override async Task<PagedResult<GEEnterpriseEvent>> GetAllPagedAsync(int page, int pageSize,
                params Expression<Func<GEEnterpriseEvent, object>>[] includeProperties) =>
            await PaginateQueryAsync(pAddQueryProperties(DbEntity
                .Include(e => e.GEEnterprises)
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments"), includeProperties)
                            .AsNoTracking(), page, pageSize);

        public override async Task<IEnumerable<GEEnterpriseEvent>> QueryAsync(
            Expression<Func<GEEnterpriseEvent, bool>> predicate,
            params Expression<Func<GEEnterpriseEvent, object>>[] includeProperties) =>
            await pAddQueryProperties(DbEntity
                .Include(e => e.GEEnterprises)
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments"), includeProperties)
                            .Where(predicate)
                            .AsNoTracking()
                            .ToListAsync();

        public override async Task<IEnumerable<GEEnterpriseEvent>> GetAllAsync(
            params Expression<Func<GEEnterpriseEvent, object>>[] includeProperties) =>
            await pAddQueryProperties(DbEntity
                            .AsNoTracking()
                .Include(e => e.GEEnterprises)
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments"), includeProperties).ToListAsync();

        public override async Task<GEEnterpriseEvent> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey,
             params Expression<Func<GEEnterpriseEvent, object>>[] includeProperties) =>
             await DbEntity.AsNoTracking()
                .Include(e => e.GEEnterprises)
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments")
                    .AsNoTracking().Where(r =>
                       r.Id == id &&
                       r.EstablishmentKey == pEstablishmentKey &&
                       r.UniqueKey == pUniqueKey).FirstOrDefaultAsync();

        IQueryable<GEEnterpriseEvent> pAddQueryProperties(IQueryable<GEEnterpriseEvent> query,
            params Expression<Func<GEEnterpriseEvent, object>>[] includeProperties)
        {
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public override GEEnterpriseEvent GetById(long id, string pEstablishmentKey, string pUniqueKey,
            params Expression<Func<GEEnterpriseEvent, object>>[] includeProperties) =>
            pAddQueryProperties(DbEntity, includeProperties)
                .Include(e => e.GEEnterprises)
                .Include(e => e.GEEvents)
                .Include(e => e.GEEvents.GEEventsClassification)
                .Include(e => e.GEEvents.GEEstabCalendar)
                .Include(e => e.GEEvents.GEEnterpriseEvent)
                .Include(e => e.GEEvents.GEUserCalendar)
                .Include("GEEvents.GEUserCalendar.GEUsers")
                .Include("GEEvents.GEEnterpriseEvent.GEEnterprises")
                .Include("GEEvents.GEEstabCalendar.GEEstablishments")
            .Where(r =>
                   r.Id == id &&
                   r.EstablishmentKey == pEstablishmentKey &&
                   r.UniqueKey == pUniqueKey)
                            .AsNoTracking().FirstOrDefault();

    }
}
