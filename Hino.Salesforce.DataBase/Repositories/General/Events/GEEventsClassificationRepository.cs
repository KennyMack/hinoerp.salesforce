using Hino.Salesforce.Domain.General.Events.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General.Events
{
    public class GEEventsClassificationRepository : BaseRepository<GEEventsClassification>, IGEEventsClassificationRepository
    {
        public GEEventsClassificationRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
