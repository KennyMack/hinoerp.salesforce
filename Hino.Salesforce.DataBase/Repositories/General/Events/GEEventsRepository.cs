using Hino.Salesforce.Domain.General.Events.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using Hino.Salesforce.Infra.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General.Events
{
    public class GEEventsRepository : BaseRepository<GEEvents>, IGEEventsRepository
    {
        public GEEventsRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async override Task<PagedResult<GEEvents>> QueryPagedAsync(int page, int pageSize,
            Expression<Func<GEEvents, bool>> predicate,
            params Expression<Func<GEEvents, object>>[] includeProperties) =>
            await PaginateQueryAsync(pAddQueryProperties(DbEntity
                .Include(e => e.GEEventsClassification)
                .Include(e => e.GEEstabCalendar)
                .Include(e => e.GEEnterpriseEvent)
                .Include(e => e.GEUserCalendar)
                .Include("GEUserCalendar.GEUsers")
                .Include("GEEnterpriseEvent.GEEnterprises")
                .Include("GEEnterpriseEvent.GEEnterprises.GEEnterpriseContacts")
                .Include("GEEstabCalendar.GEEstablishments"), includeProperties)
                            .AsNoTracking()
                            .Where(predicate), page, pageSize);

        public override async Task<PagedResult<GEEvents>> GetAllPagedAsync(int page, int pageSize,
                params Expression<Func<GEEvents, object>>[] includeProperties) =>
            await PaginateQueryAsync(pAddQueryProperties(DbEntity
                .Include(e => e.GEEventsClassification)
                .Include(e => e.GEEstabCalendar)
                .Include(e => e.GEEnterpriseEvent)
                .Include(e => e.GEUserCalendar)
                .Include("GEUserCalendar.GEUsers")
                .Include("GEEnterpriseEvent.GEEnterprises")
                .Include("GEEnterpriseEvent.GEEnterprises.GEEnterpriseContacts")
                .Include("GEEstabCalendar.GEEstablishments"), includeProperties)
                            .AsNoTracking(), page, pageSize);

        public override async Task<IEnumerable<GEEvents>> QueryAsync(
            Expression<Func<GEEvents, bool>> predicate,
            params Expression<Func<GEEvents, object>>[] includeProperties) =>
            await pAddQueryProperties(DbEntity
                .Include(e => e.GEEventsClassification)
                .Include(e => e.GEEstabCalendar)
                .Include(e => e.GEEnterpriseEvent)
                .Include(e => e.GEUserCalendar)
                .Include("GEUserCalendar.GEUsers")
                .Include("GEEnterpriseEvent.GEEnterprises")
                .Include("GEEnterpriseEvent.GEEnterprises.GEEnterpriseContacts")
                .Include("GEEstabCalendar.GEEstablishments"), includeProperties)
                            .Where(predicate)
                            .AsNoTracking()
                            .ToListAsync();

        public override async Task<IEnumerable<GEEvents>> GetAllAsync(
            params Expression<Func<GEEvents, object>>[] includeProperties) =>
            await pAddQueryProperties(DbEntity
                            .AsNoTracking()
                .Include(e => e.GEEventsClassification)
                .Include(e => e.GEEstabCalendar)
                .Include(e => e.GEEnterpriseEvent)
                .Include(e => e.GEUserCalendar)
                .Include("GEUserCalendar.GEUsers")
                .Include("GEEnterpriseEvent.GEEnterprises")
                .Include("GEEnterpriseEvent.GEEnterprises.GEEnterpriseContacts")
                .Include("GEEstabCalendar.GEEstablishments"), includeProperties).ToListAsync();

        public override async Task<GEEvents> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey,
             params Expression<Func<GEEvents, object>>[] includeProperties) =>
             await DbEntity.AsNoTracking()
                .Include(e => e.GEEventsClassification)
                .Include(e => e.GEEstabCalendar)
                .Include(e => e.GEEnterpriseEvent)
                .Include(e => e.GEUserCalendar)
                .Include("GEUserCalendar.GEUsers")
                .Include("GEEnterpriseEvent.GEEnterprises")
                .Include("GEEnterpriseEvent.GEEnterprises.GEEnterpriseContacts")
                .Include("GEEstabCalendar.GEEstablishments")
                    .AsNoTracking().Where(r =>
                       r.Id == id &&
                       r.EstablishmentKey == pEstablishmentKey &&
                       r.UniqueKey == pUniqueKey).FirstOrDefaultAsync();

        IQueryable<GEEvents> pAddQueryProperties(IQueryable<GEEvents> query,
            params Expression<Func<GEEvents, object>>[] includeProperties)
        {
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public override GEEvents GetById(long id, string pEstablishmentKey, string pUniqueKey,
            params Expression<Func<GEEvents, object>>[] includeProperties) =>
            pAddQueryProperties(DbEntity, includeProperties)
                .Include(e => e.GEEventsClassification)
                .Include(e => e.GEEstabCalendar)
                .Include(e => e.GEEnterpriseEvent)
                .Include(e => e.GEUserCalendar)
                .Include("GEUserCalendar.GEUsers")
                .Include("GEEnterpriseEvent.GEEnterprises")
                .Include("GEEnterpriseEvent.GEEnterprises.GEEnterpriseContacts")
                .Include("GEEstabCalendar.GEEstablishments")
            .Where(r =>
                   r.Id == id &&
                   r.EstablishmentKey == pEstablishmentKey &&
                   r.UniqueKey == pUniqueKey)
                            .AsNoTracking().FirstOrDefault();
    }
}
