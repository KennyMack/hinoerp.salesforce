using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.DataBase.Context;


namespace Hino.Salesforce.Infra.DataBase.Repositories.General
{
    public class GEEstabDevicesRepository : BaseRepository<GEEstabDevices>, IGEEstabDevicesRepository
    {
        public GEEstabDevicesRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
