using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General
{
    public class GEEstabMenuRepository : BaseRepository<GEEstabMenu>, IGEEstabMenuRepository
    {
        public GEEstabMenuRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
