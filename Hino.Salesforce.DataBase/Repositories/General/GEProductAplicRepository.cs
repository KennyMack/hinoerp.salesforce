﻿using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General
{
    public class GEProductAplicRepository : BaseRepository<GEProductAplic>, IGEProductAplicRepository
    {
        public GEProductAplicRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
