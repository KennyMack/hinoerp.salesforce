﻿using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.DataBase.Context;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General
{
    public class GEProductKeyRepository : BaseRepository<GEProductKey>, IGEProductKeyRepository
    {
        public GEProductKeyRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<int> GetSequenceByReferenceAsync(string pEstablishmentKey, string pReference)
        {
            var Reference = (await QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                r.Reference == pReference)).FirstOrDefault();

            return (Reference?.Sequence ?? 0) + 1;
        }
    }
}
