using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General
{
    public class GEProductsFamilyRepository : BaseRepository<GEProductsFamily>, IGEProductsFamilyRepository
    {
        public GEProductsFamilyRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
