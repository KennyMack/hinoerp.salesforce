using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General
{
    public class GEProductsRepository : BaseRepository<GEProducts>, IGEProductsRepository
    {
        public GEProductsRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        string ConverSearchField(GEProducts model)
        {
           return model.Name
                .RemoveAccent()
                .ToUpper();
        }

        public override void Add(GEProducts model)
        {
            if (model.Id == 0)
                model.Id = NextSequence();

            model.Search = ConverSearchField(model);
            base.Add(model);
        }

        public override void Update(GEProducts model)
        {
            model.Search = ConverSearchField(model);
            base.Update(model);
        }
    }
}
