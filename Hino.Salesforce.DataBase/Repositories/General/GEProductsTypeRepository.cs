using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General
{
    public class GEProductsTypeRepository : BaseRepository<GEProductsType>, IGEProductsTypeRepository
    {
        public GEProductsTypeRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
