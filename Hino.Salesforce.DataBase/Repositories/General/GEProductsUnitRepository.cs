using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General
{
    public class GEProductsUnitRepository : BaseRepository<GEProductsUnit>, IGEProductsUnitRepository
    {
        public GEProductsUnitRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
