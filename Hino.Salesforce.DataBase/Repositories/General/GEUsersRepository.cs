using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.DataBase.Context;
using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General
{
    public class GEUsersRepository : BaseRepository<GEUsers>, IGEUsersRepository
    {
        public GEUsersRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<GEUsers> GetByUsernameAsync(string username, string pEstablishmentKey, params Expression<Func<GEUsers, object>>[] includeProperties)
        {
            IQueryable<GEUsers> query = base.DbEntity;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return await query.Where(r => r.UserName.ToUpper() == username.ToUpper() && r.EstablishmentKey == pEstablishmentKey).AsNoTracking().FirstAsync();
        }

        public async Task<GEUsers> GetByEmailAsync(string email, string pEstablishmentKey, params Expression<Func<GEUsers, object>>[] includeProperties)
        {
            IQueryable<GEUsers> query = base.DbEntity;
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return await query.Where(r => r.Email.ToUpper() == email.ToUpper() && r.EstablishmentKey == pEstablishmentKey).AsNoTracking().FirstAsync();
        }
    }
}
