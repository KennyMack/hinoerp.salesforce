using Hino.Salesforce.Domain.General.Kanban.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General.Kanban
{
    public class GEBoardsRepository : BaseRepository<GEBoards>, IGEBoardsRepository
    {
        public GEBoardsRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
