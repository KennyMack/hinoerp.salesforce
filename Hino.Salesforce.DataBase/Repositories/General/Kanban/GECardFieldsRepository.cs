using Hino.Salesforce.Domain.General.Kanban.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General.Kanban
{
    public class GECardFieldsRepository : BaseRepository<GECardFields>, IGECardFieldsRepository
    {
        public GECardFieldsRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
