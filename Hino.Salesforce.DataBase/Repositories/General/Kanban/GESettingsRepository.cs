using Hino.Salesforce.Domain.General.Kanban.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using Hino.Salesforce.Infra.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.DataBase.Repositories.General.Kanban
{
    public class GESettingsRepository : BaseRepository<GESettings>, IGESettingsRepository
    {
        public GESettingsRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async override Task<PagedResult<GESettings>> QueryPagedAsync(int page, int pageSize,
            Expression<Func<GESettings, bool>> predicate,
            params Expression<Func<GESettings, object>>[] includeProperties) =>
            await PaginateQueryAsync(pAddQueryProperties(DbEntity
                .Include(s => s.GESettingsTags)
                .Include(b => b.GEBoards)
                .Include("GEBoards.GECardFields")
                .Include("GEBoards.GECardFields.GEFieldUsers")
                .Include("GEBoards.GEBoardUsers")
                .Include("GEBoards.GEBoardLists"), includeProperties)
                            .AsNoTracking()
                            .Where(predicate), page, pageSize);

        public override async Task<PagedResult<GESettings>> GetAllPagedAsync(int page, int pageSize,
                params Expression<Func<GESettings, object>>[] includeProperties) =>
            await PaginateQueryAsync(pAddQueryProperties(DbEntity
                .Include(s => s.GESettingsTags)
                .Include(b => b.GEBoards)
                .Include("GEBoards.GECardFields")
                .Include("GEBoards.GECardFields.GEFieldUsers")
                .Include("GEBoards.GEBoardUsers")
                .Include("GEBoards.GEBoardLists"), includeProperties)
                            .AsNoTracking(), page, pageSize);

        public override async Task<IEnumerable<GESettings>> QueryAsync(
            Expression<Func<GESettings, bool>> predicate,
            params Expression<Func<GESettings, object>>[] includeProperties) =>
            await pAddQueryProperties(DbEntity
                .Include(s => s.GESettingsTags)
                .Include(b => b.GEBoards)
                .Include("GEBoards.GECardFields")
                .Include("GEBoards.GECardFields.GEFieldUsers")
                .Include("GEBoards.GEBoardUsers")
                .Include("GEBoards.GEBoardLists"), includeProperties)
                            .Where(predicate)
                            .AsNoTracking()
                            .ToListAsync();

        public override async Task<IEnumerable<GESettings>> GetAllAsync(
            params Expression<Func<GESettings, object>>[] includeProperties) =>
            await pAddQueryProperties(DbEntity
                .Include(s => s.GESettingsTags)
                .Include(b => b.GEBoards)
                .Include("GEBoards.GECardFields")
                .Include("GEBoards.GECardFields.GEFieldUsers")
                .Include("GEBoards.GEBoardUsers")
                .Include("GEBoards.GEBoardLists"), includeProperties)
            .AsNoTracking()
            .ToListAsync();

        public override async Task<GESettings> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey,
             params Expression<Func<GESettings, object>>[] includeProperties) =>
             await DbEntity
                .Include(s => s.GESettingsTags)
                .Include(b => b.GEBoards)
                .Include("GEBoards.GECardFields")
                .Include("GEBoards.GECardFields.GEFieldUsers")
                .Include("GEBoards.GEBoardUsers")
                .Include("GEBoards.GEBoardLists")
                    .AsNoTracking().Where(r =>
                       r.Id == id &&
                       r.EstablishmentKey == pEstablishmentKey &&
                       r.UniqueKey == pUniqueKey).FirstOrDefaultAsync();

        IQueryable<GESettings> pAddQueryProperties(IQueryable<GESettings> query,
            params Expression<Func<GESettings, object>>[] includeProperties)
        {
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public override GESettings GetById(long id, string pEstablishmentKey, string pUniqueKey,
            params Expression<Func<GESettings, object>>[] includeProperties) =>
            pAddQueryProperties(DbEntity, includeProperties)
            .Include(s => s.GESettingsTags)
            .Include(b => b.GEBoards)
            .Include("GEBoards.GECardFields")
            .Include("GEBoards.GECardFields.GEFieldUsers")
            .Include("GEBoards.GEBoardUsers")
            .Include("GEBoards.GEBoardLists")
            .Where(r =>
                   r.Id == id &&
                   r.EstablishmentKey == pEstablishmentKey &&
                   r.UniqueKey == pUniqueKey)
                            .AsNoTracking().FirstOrDefault();
    }
}
