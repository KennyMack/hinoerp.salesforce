using Hino.Salesforce.Domain.Logistics.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Logistics;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.Logistics
{
    public class LOWaypointsRepository : BaseRepository<LOWaypoints>, ILOWaypointsRepository
    {
        public LOWaypointsRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
