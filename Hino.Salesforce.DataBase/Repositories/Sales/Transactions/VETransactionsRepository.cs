using Hino.Salesforce.Domain.Sales.Interfaces.Repositories.Transaction;
using Hino.Salesforce.Infra.Cross.Entities.Sales.Transactions;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.Sales.Transactions
{
    public class VETransactionsRepository : BaseRepository<VETransactions>, IVETransactionsRepository
    {
        public VETransactionsRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
