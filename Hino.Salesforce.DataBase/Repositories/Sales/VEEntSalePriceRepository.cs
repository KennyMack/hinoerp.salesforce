using Hino.Salesforce.Domain.Sales.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.DataBase.Context;

namespace Hino.Salesforce.Infra.DataBase.Repositories.Sales
{
    public class VEEntSalePriceRepository : BaseRepository<VEEntSalePrice>, IVEEntSalePriceRepository
    {
        public VEEntSalePriceRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }
    }
}
