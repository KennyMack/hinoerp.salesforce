using Hino.Salesforce.Domain.Sales.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.DataBase.Context;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Hino.Salesforce.Infra.DataBase.Repositories.Sales
{
    public class VEOrderItemsRepository : BaseRepository<VEOrderItems>, IVEOrderItemsRepository
    {
        public VEOrderItemsRepository(AppDbContext appDbContext) : base(appDbContext)
        {

        }

        public IEnumerable<VEOrderItems> GetItemsByOrderId(long id, string pEstablishmentKey) =>
            DbEntity.Where(r => r.OrderID == id &&
            r.EstablishmentKey == pEstablishmentKey)
            .AsNoTracking()
            .ToList();
    }
}
