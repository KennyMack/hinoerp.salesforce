using Hino.Salesforce.Domain.Sales.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using Hino.Salesforce.Infra.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.DataBase.Repositories.Sales
{
    public class VEOrdersRepository : BaseRepository<VEOrders>, IVEOrdersRepository
    {
        public VEOrdersRepository(AppDbContext appDbContext) : base(appDbContext)
        {
        }

        public async Task<PagedResult<VEOrders>> QueryViewAsync(int page, int pageSize,
            Expression<Func<VEOrders, bool>> predicate)
        {
            var count = DbEntity.Count(predicate);

            var Query = DbEntity
                .AsNoTracking()
                .Include(order => order.VEOrderItems)
                .Include(enterprise => enterprise.GEEnterprises)
                .Include(user => user.GEUsers)
                .Include(user => user.GEUserDigitizer)
                .Where(predicate);

            return await PaginateQueryAsync(Query, page, pageSize, count);
        }

        public async override Task<PagedResult<VEOrders>> QueryPagedAsync(int page, int pageSize,
            Expression<Func<VEOrders, bool>> predicate,
            params Expression<Func<VEOrders, object>>[] includeProperties)
        {
            var count = DbEntity.Count(predicate);

            return await PaginateQueryAsync(pAddQueryProperties(DbEntity.Include(r => r.VEOrderItems)
                .Include("VEOrderItems.GEProducts")
                .Include("VEOrderItems.FSFiscalOper")
                .Include("VEOrderItems.GEProducts.FSNCM")
                .Include("VEOrderItems.VEOrderTaxes")
                .Include(e => e.GEUsers)
                .Include(e => e.GEUserDigitizer)
                .Include(e => e.GEPaymentType)
                .Include(e => e.GEPaymentCondition)
                .Include(e => e.GECarriers)
                .Include(e => e.GERedispatch)
                .Include(e => e.GEEnterprises)
                .Include(e => e.VETransactions)
                .Include(e => e.MainFiscalOper)
                .Include(e => e.VETypeSale)
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts"), includeProperties)
                .AsNoTracking()
                .Where(predicate), page, pageSize, count);
        }

        public override async Task<PagedResult<VEOrders>> GetAllPagedAsync(int page, int pageSize,
                params Expression<Func<VEOrders, object>>[] includeProperties) =>
            await PaginateQueryAsync(pAddQueryProperties(DbEntity.Include(r => r.VEOrderItems)
                .Include("VEOrderItems.GEProducts")
                .Include("VEOrderItems.FSFiscalOper")
                .Include("VEOrderItems.GEProducts.FSNCM")
                .Include("VEOrderItems.VEOrderTaxes")
                .Include(e => e.GEUsers)
                .Include(e => e.GEUserDigitizer)
                .Include(e => e.GEPaymentType)
                .Include(e => e.GEPaymentCondition)
                .Include(e => e.GECarriers)
                .Include(e => e.GERedispatch)
                .Include(e => e.GEEnterprises)
                .Include(e => e.VETransactions)
                .Include(e => e.MainFiscalOper)
                .Include(e => e.VETypeSale)
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts"), includeProperties)
                .AsNoTracking(), page, pageSize);

        public override async Task<IEnumerable<VEOrders>> QueryAsync(
            Expression<Func<VEOrders, bool>> predicate,
            params Expression<Func<VEOrders, object>>[] includeProperties) =>
            await pAddQueryProperties(DbEntity.Include(r => r.VEOrderItems)
                .Include("VEOrderItems.GEProducts")
                .Include("VEOrderItems.FSFiscalOper")
                .Include("VEOrderItems.GEProducts.FSNCM")
                .Include("VEOrderItems.VEOrderTaxes")
                .Include(e => e.GEUsers)
                .Include(e => e.GEUserDigitizer)
                .Include(e => e.GEPaymentType)
                .Include(e => e.GEPaymentCondition)
                .Include(e => e.GECarriers)
                .Include(e => e.GERedispatch)
                .Include(e => e.GEEnterprises)
                .Include(e => e.VETransactions)
                .Include(e => e.MainFiscalOper)
                .Include(e => e.VETypeSale)
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts"), includeProperties)
                .Where(predicate)
                .AsNoTracking()
                .ToListAsync();

        public override async Task<IEnumerable<VEOrders>> GetAllAsync(
            params Expression<Func<VEOrders, object>>[] includeProperties) =>
            await pAddQueryProperties(DbEntity.Include(r => r.VEOrderItems)
                .Include("VEOrderItems.GEProducts")
                .Include("VEOrderItems.FSFiscalOper")
                .Include("VEOrderItems.GEProducts.FSNCM")
                .Include("VEOrderItems.VEOrderTaxes")
                .Include(e => e.GEUsers)
                .Include(e => e.GEUserDigitizer)
                .Include(e => e.GEPaymentType)
                .Include(e => e.GEPaymentCondition)
                .Include(e => e.GECarriers)
                .Include(e => e.GERedispatch)
                .Include(e => e.GEEnterprises)
                .Include(e => e.VETransactions)
                .Include(e => e.MainFiscalOper)
                .Include(e => e.VETypeSale)
                    .Include("GEEnterprises.GEEnterpriseGeo")
                    .Include("GEEnterprises.GEEnterpriseContacts"), includeProperties)
                .AsNoTracking()
                .ToListAsync();

        public override async Task<VEOrders> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey,
             params Expression<Func<VEOrders, object>>[] includeProperties) =>
             await DbEntity.Include(r => r.VEOrderItems)
                .Include("VEOrderItems.GEProducts")
                .Include("VEOrderItems.FSFiscalOper")
                .Include("VEOrderItems.GEProducts.FSNCM")
                .Include("VEOrderItems.VEOrderTaxes")
                .Include("VEOrderItems.GEFilesPath")
                .Include(e => e.GEUsers)
                .Include(e => e.GEUserDigitizer)
                .Include(e => e.GEPaymentType)
                .Include(e => e.GEPaymentCondition)
                .Include(e => e.GECarriers)
                .Include(e => e.GERedispatch)
                .Include(e => e.GEEnterprises)
                .Include(e => e.VETransactions)
                .Include(e => e.MainFiscalOper)
                .Include(e => e.GEFilesPath)
                .Include(e => e.VETypeSale)
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts")
                .AsNoTracking()
                .Where(r =>
                    r.Id == id &&
                    r.EstablishmentKey == pEstablishmentKey)
                .FirstOrDefaultAsync();

        IQueryable<VEOrders> pAddQueryProperties(IQueryable<VEOrders> query,
            params Expression<Func<VEOrders, object>>[] includeProperties)
        {
            foreach (var includeProperty in includeProperties)
            {
                query = query.Include(includeProperty);
            }

            return query;
        }

        public override VEOrders GetById(long id, string pEstablishmentKey, string pUniqueKey,
            params Expression<Func<VEOrders, object>>[] includeProperties) =>
            pAddQueryProperties(DbEntity, includeProperties)
                .Include(r => r.VEOrderItems)
                .Include("VEOrderItems.GEProducts")
                .Include("VEOrderItems.FSFiscalOper")
                .Include("VEOrderItems.GEProducts.FSNCM")
                .Include("VEOrderItems.VEOrderTaxes")
                .Include("VEOrderItems.GEFilesPath")
                .Include(e => e.GEUsers)
                .Include(e => e.GEUserDigitizer)
                .Include(e => e.GEPaymentType)
                .Include(e => e.GEPaymentCondition)
                .Include(e => e.GECarriers)
                .Include(e => e.GERedispatch)
                .Include(e => e.GEEnterprises)
                .Include(e => e.VETransactions)
                .Include(e => e.MainFiscalOper)
                .Include(e => e.GEFilesPath)
                .Include(e => e.VETypeSale)
                .Include("GEEnterprises.GEEnterpriseGeo")
                .Include("GEEnterprises.GEEnterpriseContacts")
                .Where(r =>
                   r.Id == id &&
                   r.EstablishmentKey == pEstablishmentKey)
                .AsNoTracking()
                .FirstOrDefault();

    }
}
