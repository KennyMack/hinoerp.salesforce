﻿using Hino.Salesforce.Domain.Sales.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using Hino.Salesforce.Infra.DataBase.Context;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.DataBase.Repositories.Sales
{
    public class VESalePriceRepository : BaseRepository<VESalePrice>, IVESalePriceRepository
    {
        public VESalePriceRepository(AppDbContext appDbContext) : base(appDbContext)
        {
            
        }

        public Task<PagedResult<VESalePrice>> GetAllPagedSearchProductWithStock(
            int pageNumber, int pageSize, string pEstablishmentKey, 
            long pRegionId, long pCodPrVenda, 
            string filter, string searchPosition)
        {
            if (pCodPrVenda > 0 && searchPosition == "1")
            {
                return this.QueryPagedAsync(pageNumber,
                                            pageSize,
                                            r => r.EstablishmentKey == pEstablishmentKey &&
                                                 r.CodPrVenda == pCodPrVenda &&
                                                 r.GEProducts.StockBalance > 0 &&
                                                 r.GEProducts.ProductKey.ToUpper().StartsWith(filter),
                                            p => p.GEProducts,
                                            s => s.VERegionSale);
            } 
            else if (pCodPrVenda > 0)
            {
                return this.QueryPagedAsync(pageNumber,
                                        pageSize,
                                        r => r.EstablishmentKey == pEstablishmentKey &&
                                             r.CodPrVenda == pCodPrVenda &&
                                             r.GEProducts.StockBalance > 0 &&
                                             (DbFunctions.Like(r.GEProducts.Name.ToUpper(), "%" + filter + "%") ||
                                             r.GEProducts.ProductKey.ToUpper().Contains(filter)),
                                        p => p.GEProducts,
                                        s => s.VERegionSale);
            }

            if (searchPosition == "1")
            {
                return this.QueryPagedAsync(pageNumber,
                                            pageSize,
                                            r => r.EstablishmentKey == pEstablishmentKey &&
                                                 r.RegionId == pRegionId &&
                                                 r.GEProducts.StockBalance > 0 &&
                                                 r.GEProducts.ProductKey.ToUpper().StartsWith(filter),
                                            p => p.GEProducts,
                                            s => s.VERegionSale);
            }

            return this.QueryPagedAsync(pageNumber,
                                        pageSize,
                                        r => r.EstablishmentKey == pEstablishmentKey &&
                                             r.RegionId == pRegionId &&
                                             r.GEProducts.StockBalance > 0 &&
                                             (DbFunctions.Like(r.GEProducts.Name.ToUpper(), "%" + filter + "%") ||
                                             r.GEProducts.ProductKey.ToUpper().Contains(filter)),
                                        p => p.GEProducts,
                                        s => s.VERegionSale);
        }

        public Task<PagedResult<VESalePrice>> GetAllPagedSearchProduct(
            int pageNumber, int pageSize, string pEstablishmentKey, 
            long pRegionId, long pCodPrVenda, 
            string filter, string searchPosition)
        {
            var clearedFilter = filter.RemoveAccent().ToUpper();

            if (pCodPrVenda > 0 && searchPosition == "1")
            {
                return this.QueryPagedAsync(pageNumber,
                                            pageSize,
                                            r => r.EstablishmentKey == pEstablishmentKey &&
                                                 r.CodPrVenda == pCodPrVenda &&
                                                 r.GEProducts.ProductKey.ToUpper().StartsWith(filter),
                                            p => p.GEProducts,
                                            s => s.VERegionSale);
            }
            else if (pCodPrVenda > 0)
            {
                return this.QueryPagedAsync(pageNumber,
                                        pageSize,
                                        r => r.EstablishmentKey == pEstablishmentKey &&
                                             r.CodPrVenda == pCodPrVenda &&
                                             DbFunctions.Like(r.GEProducts.Name.ToUpper(), "%" + filter + "%"),
                                        p => p.GEProducts,
                                        s => s.VERegionSale);
            }

            if (searchPosition == "1")
            {
                return this.QueryPagedAsync(pageNumber,
                                            pageSize,
                                            r => r.EstablishmentKey == pEstablishmentKey &&
                                                 r.RegionId == pRegionId &&
                                                 r.GEProducts.ProductKey.ToUpper().StartsWith(filter),
                                            p => p.GEProducts,
                                            s => s.VERegionSale);
            }

            return this.QueryPagedAsync(pageNumber,
                                        pageSize,
                                        r => r.EstablishmentKey == pEstablishmentKey &&
                                             r.RegionId == pRegionId &&
                                             DbFunctions.Like(r.GEProducts.Name.ToUpper(), "%" + filter + "%"),
                                        p => p.GEProducts,
                                        s => s.VERegionSale);
        }

        public async Task<IList<VESalePrice>> GetListCodPRVendaAsync(string pEstablishmentKey)
        {
            var retorno = new List<VESalePrice>();
            var sql = @"SELECT VESALEPRICE.ESTABLISHMENTKEY, VESALEPRICE.CODPRVENDA,
                               VESALEPRICE.DESCRIPTION
                          FROM VESALEPRICE
                         WHERE VESALEPRICE.ESTABLISHMENTKEY = :pESTABLISHMENTKEY
                         GROUP BY VESALEPRICE.ESTABLISHMENTKEY,
                                  VESALEPRICE.DESCRIPTION,
                                  VESALEPRICE.CODPRVENDA";

            using (var cmd = DbConn.Database.Connection.CreateCommand())
            {
                await DbConn.Database.Connection.OpenAsync();
                cmd.CommandText = sql;

                cmd.Parameters.Add(new Oracle.ManagedDataAccess.Client.OracleParameter("pESTABLISHMENTKEY", pEstablishmentKey));

                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    while(reader.Read())
                        retorno.Add(new VESalePrice()
                        {
                            Description = Convert.ToString(reader["DESCRIPTION"]),
                            CodPrVenda = Convert.ToInt64(reader["CODPRVENDA"])
                        });
                }
                DbConn.Database.Connection.Close();
            }

            return retorno;
        }
    }
}
