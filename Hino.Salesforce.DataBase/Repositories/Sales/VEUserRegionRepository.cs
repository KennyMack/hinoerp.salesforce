using Hino.Salesforce.Domain.Sales.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.DataBase.Context;
using Oracle.ManagedDataAccess.Client;
using System.Data;
using System.Linq;

namespace Hino.Salesforce.Infra.DataBase.Repositories.Sales
{
    public class VEUserRegionRepository : BaseRepository<VEUserRegion>, IVEUserRegionRepository
    {
        readonly AppDbContext _AppDbContext;
        public VEUserRegionRepository(AppDbContext appDbContext) : base(appDbContext)
        {
            _AppDbContext = appDbContext;
        }

        public bool ZipCodeExists(string pEstablishmentKey, long pUserId, string pZipCode)
        {
            var exists = _AppDbContext.Database.SqlQuery<int>(@"SELECT COUNT(1)
                                                FROM VESALEWORKREGION,
                                                     VEUSERREGION 
                                               WHERE VEUSERREGION.ESTABLISHMENTKEY = VESALEWORKREGION.ESTABLISHMENTKEY
                                                 AND VEUSERREGION.SALEWORKID       = VESALEWORKREGION.ID
                                                 AND VEUSERREGION.ESTABLISHMENTKEY = :pESTABLISHMENTKEY
                                                 AND VEUSERREGION.USERID           = :pUSERID
                                                 AND :pZIPCODE BETWEEN REPLACE(VESALEWORKREGION.ZIPCODESTART, '-', '')
                                                                 AND REPLACE(VESALEWORKREGION.ZIPCODEEND, '-', '')",
                   new OracleParameter("pESTABLISHMENTKEY", OracleDbType.Varchar2, pEstablishmentKey, ParameterDirection.Input),
                   new OracleParameter("pUSERID", OracleDbType.Int64, pUserId, ParameterDirection.Input),
                   new OracleParameter("pZIPCODE", OracleDbType.Varchar2, pZipCode.OnlyNumbers(), ParameterDirection.Input)
            )
            .Single();

            return exists > 0;
        }
    }
}
