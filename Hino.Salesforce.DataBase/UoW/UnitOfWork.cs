﻿using Hino.Salesforce.Domain.Base.Interfaces.UoW;
using Hino.Salesforce.Infra.DataBase.Context;
using System;
using System.Linq;

namespace Hino.Salesforce.Infra.DataBase.UoW
{
    public class UnitOfWork : IUnitOfWork
    {
        public long RowsAffected { get; private set; }

        private readonly AppDbContext _AppDbContext;

        public UnitOfWork(AppDbContext appDbContext)
        {
            _AppDbContext = appDbContext;
        }

        public void Commit()
        {
            RowsAffected = _AppDbContext.SaveChanges();
        }

        public void Rollback()
        {
            _AppDbContext
                .ChangeTracker
                .Entries()
                .ToList()
                .ForEach(x =>
                {
                    try
                    {
                        x.Reload();
                    }
                    catch (Exception e)
                    {
                        Hino.Salesforce.Infra.Cross.Utils.Exceptions.Logging.Exception(e);
                    }
                });

        }

        public void Dispose()
        {
        }

    }
}
