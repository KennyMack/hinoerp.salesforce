using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Auth;

namespace Hino.Salesforce.Domain.Auth.Interfaces.Repositories
{
    public interface IAUExpiredTokenRepository : IBaseRepository<AUExpiredToken>
    {
    }
}
