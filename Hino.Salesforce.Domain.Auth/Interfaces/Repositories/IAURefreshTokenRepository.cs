using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Auth;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Auth.Interfaces.Repositories
{
    public interface IAURefreshTokenRepository : IBaseRepository<AURefreshToken>
    {
        Task<AURefreshToken> GetBySessionId(string sessionId, params Expression<Func<AURefreshToken, object>>[] includeProperties);
        Task<AURefreshToken> GetByRefreshToken(string sessionId, string refreshTokenId);
    }
}
