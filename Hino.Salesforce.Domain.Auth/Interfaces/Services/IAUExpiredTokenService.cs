using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Auth;

namespace Hino.Salesforce.Domain.Auth.Interfaces.Services
{
    public interface IAUExpiredTokenService : IBaseService<AUExpiredToken>
    {
    }
}
