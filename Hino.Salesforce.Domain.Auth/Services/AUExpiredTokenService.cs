using Hino.Salesforce.Domain.Auth.Interfaces.Repositories;
using Hino.Salesforce.Domain.Auth.Interfaces.Services;
using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Infra.Cross.Entities.Auth;

namespace Hino.Salesforce.Domain.Auth.Services
{
    public class AUExpiredTokenService : BaseService<AUExpiredToken>, IAUExpiredTokenService
    {
        private readonly IAUExpiredTokenRepository _IAUExpiredTokenRepository;

        public AUExpiredTokenService(IAUExpiredTokenRepository pIAUExpiredTokenRepository) :
             base(pIAUExpiredTokenRepository)
        {
            _IAUExpiredTokenRepository = pIAUExpiredTokenRepository;
        }
    }
}
