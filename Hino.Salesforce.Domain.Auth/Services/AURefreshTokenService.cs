using Hino.Salesforce.Domain.Auth.Interfaces.Repositories;
using Hino.Salesforce.Domain.Auth.Interfaces.Services;
using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Infra.Cross.Entities.Auth;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Auth.Services
{
    public class AURefreshTokenService : BaseService<AURefreshToken>, IAURefreshTokenService
    {
        private readonly IAURefreshTokenRepository _IAURefreshTokenRepository;

        public AURefreshTokenService(IAURefreshTokenRepository pIAURefreshTokenRepository) :
             base(pIAURefreshTokenRepository)
        {
            _IAURefreshTokenRepository = pIAURefreshTokenRepository;
        }

        public async Task<AURefreshToken> GetByRefreshTokenId(string pRefreshTokenId, string pEstablishmentKey) =>
            (await _IAURefreshTokenRepository.QueryAsync(r => r.RefreshTokenID == pRefreshTokenId
            && r.EstablishmentKey == pEstablishmentKey, x => x.GEUsers)).FirstOrDefault();


        public async Task<AURefreshToken> GetByRefreshTokenId(string pRefreshTokenId) =>
            (await _IAURefreshTokenRepository.QueryAsync(r => r.RefreshTokenID == pRefreshTokenId)).FirstOrDefault();

        public async Task RemoveByRefreshTokenId(string pRefreshTokenId)
        {
            try
            {
                var RefreshToken = await GetByRefreshTokenId(pRefreshTokenId);

                if (RefreshToken != null)
                {
                    await RemoveById(RefreshToken.Id, RefreshToken.EstablishmentKey, RefreshToken.UniqueKey);
                    await SaveChanges();
                }

            }
            catch (System.Exception e)
            {
                Hino.Salesforce.Infra.Cross.Utils.Exceptions.Logging.Exception(e);
            }
        }
    }
}
