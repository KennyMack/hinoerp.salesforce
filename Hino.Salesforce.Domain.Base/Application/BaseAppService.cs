﻿using Hino.Salesforce.Domain.Base.Exceptions;
using Hino.Salesforce.Domain.Base.Interfaces.Application;
using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Domain.Base.Paging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Base.Application
{
    public class BaseAppService<T> : IBaseAppService<T>, IDisposable where T : class
    {
        private readonly IBaseService<T> _BaseService;
        public List<ModelException> Errors
        {
            get
            {
                return _BaseService.Errors;
            }
            set
            {
                _BaseService.Errors = value;
            }
        }

        public BaseAppService(IBaseService<T> baseService)
        {
            _BaseService = baseService;
        }

        public virtual void Add(T model)
        {
            _BaseService.Add(model);
        }

        public virtual void Update(T model)
        {
            _BaseService.Update(model);
        }

        public virtual void Remove(T model)
        {
            _BaseService.Remove(model);
        }

        public virtual void RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            _BaseService.RemoveById(id, pEstablishmentKey, pUniqueKey);
        }

        public async virtual Task<IEnumerable<T>> GetAll(params Expression<Func<T, object>>[] includeProperties) =>
            await _BaseService.GetAll(includeProperties);

        public virtual PagedResult<T> GetAllPaged(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties) =>
            _BaseService.GetAllPaged(page, pageSize, includeProperties);

        public async virtual Task<T> GetById(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties) =>
            await _BaseService.GetById(id, pEstablishmentKey, pUniqueKey, includeProperties);

        public virtual IEnumerable<T> Query(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
            _BaseService.Query(predicate, includeProperties);

        public PagedResult<T> QueryPaged(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
            _BaseService.QueryPaged(page, pageSize, predicate, includeProperties);

        public virtual long NextSequence() =>
            _BaseService.NextSequence();

        public virtual async Task<long> NextSequenceAsync() =>            
            await _BaseService.NextSequenceAsync();

        public virtual async Task<int> SaveChanges() =>            
            await _BaseService.SaveChanges();

        public void Dispose()
        {
            _BaseService.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
