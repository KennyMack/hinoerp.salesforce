﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Salesforce.Domain.Base.Interfaces.Entities;

namespace Hino.Salesforce.Domain.Base.Entities
{
    public class BaseEntity : IBaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }
        public string EstablishmentKey { get; set; }
        public string UniqueKey { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        
        public bool IsActive { get; set; }
    }
}
