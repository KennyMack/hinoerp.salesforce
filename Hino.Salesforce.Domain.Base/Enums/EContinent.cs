﻿using System.ComponentModel.DataAnnotations;

namespace Hino.Salesforce.Domain.Base.Enums
{
    public enum EContinent
    {
        [Display(Description = "America do norte")]
        NorthAmerica = 0,

        [Display(Description = "America do sul")]
        SouthAmerica = 1,

        [Display(Description = "Europa")]
        Europe = 2,

        [Display(Description = "Africa")]
        Africa = 3,

        [Display(Description = "Ásia")]
        Asia = 4,

        [Display(Description = "Oceania")]
        Oceania = 5,

        [Display(Description = "Antártida")]
        Antarctica = 6
    }
}
