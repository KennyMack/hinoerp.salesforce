﻿using System.ComponentModel.DataAnnotations;

namespace Hino.Salesforce.Domain.Base.Enums
{
    public enum ERegion
    {
        [Display(Description = "Norte")]
        North = 0,
        [Display(Description = "Sul")]
        South = 1,
        [Display(Description = "Leste")]
        East = 2,
        [Display(Description = "Oeste")]
        West = 3,
        [Display(Description = "Noroeste")]
        NorthWest = 4,
        [Display(Description = "Nordeste")]
        NorthEast = 5,
        [Display(Description = "Sudoeste")]
        SouthWest = 6,
        [Display(Description = "Sudeste")]
        SouthEast = 7
    }
}
