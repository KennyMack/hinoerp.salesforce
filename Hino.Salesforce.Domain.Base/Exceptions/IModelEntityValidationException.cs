﻿using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System.Collections.Generic;

namespace Hino.Salesforce.Domain.Base.Exceptions
{
    public interface IModelEntityValidationException
    {
        string Message { get; }
        List<ModelException> MException { get; }
    }
}
