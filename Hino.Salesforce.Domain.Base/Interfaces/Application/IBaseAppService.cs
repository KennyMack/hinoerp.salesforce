﻿using Hino.Salesforce.Domain.Base.Exceptions;
using Hino.Salesforce.Domain.Base.Paging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Base.Interfaces.Application
{
    public interface IBaseAppService<T> where T : class
    {
        List<ModelException> Errors { get; set; }
        void Add(T model);
        Task<T> GetById(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties);
        PagedResult<T> GetAllPaged(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> GetAll(params Expression<Func<T, object>>[] includeProperties);
        IEnumerable<T> Query(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        PagedResult<T> QueryPaged(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        void Update(T model);
        void Remove(T model);
        void RemoveById(long id, string pEstablishmentKey, string pUniqueKey);
        long NextSequence();
        Task<long> NextSequenceAsync();
        Task<int> SaveChanges();
        void Dispose();

    }
}
