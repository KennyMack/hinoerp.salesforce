﻿using System;
using System.Collections.Generic;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Base.Interfaces.Email
{
    public interface ISendEmail : IDisposable
    {
        string LocalEmail { get; set; }
        MailAddress From { get; set; }
        MailAddressCollection To { get; set; }
        List<Dictionary<string, string>> ReplaceTags { get; set; }

        Task<bool> Send(string pEstablishmentKey, MailMessage pMailMessage);
    }
}
