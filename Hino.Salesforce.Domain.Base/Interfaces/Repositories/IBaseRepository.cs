﻿using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Base.Interfaces.Repositories
{
    public interface IBaseRepository<T> where T : class
    {
        void Add(T model);
        int CountResults(Expression<Func<T, bool>> predicate);
        Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties);
        Task<T> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties);
        T GetById(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties);
        T GetByIdToUpdate(long id, string pEstablishmentKey, string pUniqueKey);
        Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        void Update(T model);
        void Remove(T model);
        Task<T> RemoveById(long id, string pEstablishmentKey, string pUniqueKey);
        long NextSequence();
        Task<long> NextSequenceAsync();
        void SetModelState(T model, EModelDataState pState);
        Task<int> SaveChanges();
        void RollBackChanges();
        void Dispose();
    }
}
