﻿using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Base.Interfaces.Services
{
    public interface IBaseService<T> where T : class
    {
        bool DontSendToQueue { get; set; }
        IList<T> AddedOrUpdatedItems { get; }
        IList<T> RemovedItems { get; }
        int CountResults(Expression<Func<T, bool>> predicate);
        List<ModelException> Errors { get; set; }
        IBaseRepository<T> DataRepository { get; }
        T Add(T model);
        Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties);
        T GetById(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties);
        T GetByIdToUpdate(long id, string pEstablishmentKey, string pUniqueKey);
        Task<T> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties);
        Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties);
        T Update(T model);
        void Remove(T model);
        Task<T> RemoveById(long id, string pEstablishmentKey, string pUniqueKey);
        long NextSequence();
        Task<long> NextSequenceAsync();
        void SetModelState(T model, EModelDataState pState);
        Task<int> SaveChanges();
        void RollBackChanges();
        void Dispose();
        Task GenerateEntryQueueAsync();
    }
}
