﻿using System;

namespace Hino.Salesforce.Domain.Base.Interfaces.UoW
{
    public interface IUnitOfWork : IDisposable
    {
        long RowsAffected { get; }
        void Commit();
        void Rollback();
    }
}
