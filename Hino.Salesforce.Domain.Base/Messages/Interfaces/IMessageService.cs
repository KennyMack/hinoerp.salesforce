﻿namespace Hino.Salesforce.Domain.Base.Messages.Interfaces
{
    public interface IMessageService<T> where T : class
    {
        void SendCreatedOrUpdatedQueue(T Item);
        void SendCreatedOrUpdatedQueue(T Item, string QueueName);
        void SendCreatedOrUpdatedQueue(T Item, string QueueName, string RoutingKey);
        void SendRemovedQueue(T Item);
        void SendRemovedQueue(T Item, string pQueueName);
        void SendRemovedQueue(T Item, string QueueName, string RoutingKey);
    }
}
