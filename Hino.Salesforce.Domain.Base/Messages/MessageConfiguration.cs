﻿using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Settings;

namespace Hino.Salesforce.Domain.Base.Messages
{
    public class MessageConfiguration
    {
        public string HostName { get; private set; }
        public string UserName { get; private set; }
        public string Password { get; private set; }
        public int Port { get; private set; }

        public MessageConfiguration()
        {
            // var Configuration = ConfigurationManager.AppSettings;
            HostName = AppSettings.RABBIT_URL ?? "192.168.1.1";
            UserName = AppSettings.RABBIT_USER ?? "ADMIN";
            Password = AppSettings.RABBIT_PASS ?? "HOME";
            Port = ConvertEx.ToInt32(AppSettings.RABBIT_PORT) ?? 15672;

            // HostName = Configuration.GetValues("RABBIT_URL").FirstOrDefault() ?? "192.168.1.1";
            // UserName = Configuration.GetValues("RABBIT_USER").FirstOrDefault() ?? "ADMIN";
            // Password = Configuration.GetValues("RABBIT_PASS").FirstOrDefault() ?? "HOME";
            // Port = ConvertEx.ToInt32(Configuration.GetValues("RABBIT_PORT").FirstOrDefault()) ?? 15672;
        }
    }
}
