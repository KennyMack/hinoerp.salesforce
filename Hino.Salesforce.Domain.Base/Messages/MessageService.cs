﻿using Hino.Salesforce.Domain.Base.Messages.Interfaces;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Entities.Interfaces;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using RabbitMQ.Client;
using System;
using System.Linq;
using System.Text;

namespace Hino.Salesforce.Domain.Base.Messages
{
    public class MessageService<T> : IMessageService<T> where T : class
    {
        private readonly MessageConfiguration MessageCfg;
        private readonly ConnectionFactory FactoryConn;

        public MessageService()
        {
            MessageCfg = new MessageConfiguration();
            FactoryConn = new ConnectionFactory
            {
                HostName = MessageCfg.HostName,
                UserName = MessageCfg.UserName,
                Password = MessageCfg.Password,
                ContinuationTimeout = new TimeSpan(0, 0, 3, 0),
                RequestedConnectionTimeout = new TimeSpan(0, 0, 3, 0),
                HandshakeContinuationTimeout = new TimeSpan(0, 0, 3, 0),
                SocketReadTimeout = new TimeSpan(0, 0, 3, 0),
                SocketWriteTimeout = new TimeSpan(0, 0, 3, 0)
            };
        }

        private void SendListQueue(string Type, T Item, string QueueName, string RoutingKey)
        {
            string EntryName;
            try
            {
                EntryName =
                    ((QueueAttribute)Item.GetType().GetCustomAttributes(typeof(QueueAttribute), false).FirstOrDefault()).Queue;
            }
            catch (Exception)
            {
                try
                {
                    var custom = (QueueAttribute)(Item.GetType().BaseType.GetCustomAttributes(typeof(QueueAttribute), true).First());

                    EntryName = custom.Queue;
                }
                catch (Exception)
                {
                    EntryName = "NONE";
                }
            }

            if (EntryName != "NONE")
            {
                try
                {
                    using (var conn = FactoryConn.CreateConnection())
                    using (var channel = conn.CreateModel())
                    {
                        var exchangeName = ((IBaseEntity)Item).EstablishmentKey;

                        channel.ExchangeDeclare(
                            exchange: exchangeName,
                            type: "topic",
                            durable: true, 
                            autoDelete: true);

                        channel.QueueDeclare(
                           queue: QueueName,
                           durable: true,
                           exclusive: false,
                           autoDelete: true,
                           arguments: null);

                        channel.QueueBind(
                            queue: QueueName,
                            exchange: exchangeName,
                            routingKey: RoutingKey
                        );

                        var properties = channel.CreateBasicProperties();
                        properties.Persistent = true;
                        properties.DeliveryMode = 2;

                        var obj = JsonConvert.SerializeObject(new GEQueueItem
                        {
                            Type = Type,
                            EntryName = EntryName,
                            EstablishmentKey = ((IBaseEntity)Item).EstablishmentKey,
                            UniqueKey = ((IBaseEntity)Item).UniqueKey,
                            Id = ((IBaseEntity)Item).Id,
                            Created = ((IBaseEntity)Item).Created,
                            Modified = ((IBaseEntity)Item).Modified,
                            IsActive = ((IBaseEntity)Item).IsActive
                        }, new JsonSerializerSettings
                        {
                            Culture = new System.Globalization.CultureInfo("pt-BR"),
                            Formatting = Formatting.Indented,
                            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                        });
                        var body = Encoding.UTF8.GetBytes(obj);

                        channel.BasicPublish(
                            exchange: ((IBaseEntity)Item).EstablishmentKey,
                            routingKey: RoutingKey,
                            basicProperties: properties,
                            body: body);
                    }
                }
                catch (Exception e)
                {
                    Logging.Exception(e);
                }
            }
        }

        public void SendCreatedOrUpdatedQueue(T Item, string QueueName, string RoutingKey)
        {
            if (Item != null)
                SendListQueue("NEW", Item, QueueName, RoutingKey);
        }

        public void SendCreatedOrUpdatedQueue(T Item, string pQueueName)
        {
            if (Item != null)
                SendListQueue("NEW", Item, pQueueName, ((IBaseEntity)Item).EstablishmentKey);
        }

        public void SendCreatedOrUpdatedQueue(T Item)
        {
            if (Item != null)
                SendListQueue("NEW", Item, ((IBaseEntity)Item).EstablishmentKey, ((IBaseEntity)Item).EstablishmentKey);
        }

        public void SendRemovedQueue(T Item)
        {
            if (Item != null)
                SendListQueue("REMOVE", Item, ((IBaseEntity)Item).EstablishmentKey, ((IBaseEntity)Item).EstablishmentKey);
        }

        public void SendRemovedQueue(T Item, string pQueueName)
        {
            if (Item != null)
                SendListQueue("REMOVE", Item, pQueueName, ((IBaseEntity)Item).EstablishmentKey);
        }

        public void SendRemovedQueue(T Item, string pQueueName, string RoutingKey)
        {
            if (Item != null)
                SendListQueue("REMOVE", Item, pQueueName, RoutingKey);
        }
    }
}
