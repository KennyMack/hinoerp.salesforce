﻿using Hino.Salesforce.Domain.Base.Paging;
using System.Collections.Generic;

namespace Hino.Salesforce.Domain.Base.Paging
{
    public class PagedResult<T>: BasePagedResult where T : class
    {
        public IList<T> Results { get; set; }

        public PagedResult()
        {
            Results = new List<T>();
        }
    }
}
