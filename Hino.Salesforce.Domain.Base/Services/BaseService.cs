﻿using Hino.Salesforce.Domain.Base.Exceptions;
using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Domain.Base.Messages;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Base.Services
{
    public class BaseService<T> : IDisposable, IBaseService<T> where T : class
    {
        public bool DontSendToQueue { get; set; }
        public List<ModelException> Errors { get; set; }
        public IBaseRepository<T> DataRepository { get; }
        public IList<T> AddedOrUpdatedItems { get; }
        public IList<T> RemovedItems { get; }

        public BaseService(IBaseRepository<T> repo)
        {
            DataRepository = repo;
            AddedOrUpdatedItems = new List<T>();
            RemovedItems = new List<T>();
            Errors = new List<ModelException>();
        }

        public virtual T Add(T model)
        {
            try
            {
                DataRepository.Add(model);
                AddedOrUpdatedItems.Add(model);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InsertSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.Message,
                                              e?.InnerException?.InnerException?.Message  }
                });
            }
            return model;
        }

        public virtual T Update(T model)
        {
            try
            {
                DataRepository.Update(model);
                AddedOrUpdatedItems.Add(model);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.UpdateSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.Message,
                                              e?.InnerException?.InnerException?.Message }
                });
            }
            return model;
        }

        public virtual void Remove(T model)
        {
            try
            {
                DataRepository.Remove(model);
                RemovedItems.Remove(model);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.DeleteSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.Message,
                                              e?.InnerException?.InnerException?.Message  }
                });
            }
        }

        public virtual async Task<T> RemoveById(long id, string pEstablishmentKey, string pUniqueKey)
        {
            try
            {
                var model = await DataRepository.RemoveById(id, pEstablishmentKey, pUniqueKey);
                RemovedItems.Add(model);
                return model;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.DeleteSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.Message,
                                              e?.InnerException?.InnerException?.Message }
                });
            }
            return default;
        }

        public virtual async Task<PagedResult<T>> GetAllPagedAsync(int page, int pageSize, params Expression<Func<T, object>>[] includeProperties) =>
            await DataRepository.GetAllPagedAsync(page, pageSize, includeProperties);

        public virtual async Task<IEnumerable<T>> GetAllAsync(params Expression<Func<T, object>>[] includeProperties) =>
            await DataRepository.GetAllAsync(includeProperties);

        public virtual async Task<T> GetByIdAsync(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties) =>
            await DataRepository.GetByIdAsync(id, pEstablishmentKey, pUniqueKey, includeProperties);

        public virtual T GetById(long id, string pEstablishmentKey, string pUniqueKey, params Expression<Func<T, object>>[] includeProperties) =>
            DataRepository.GetById(id, pEstablishmentKey, pUniqueKey, includeProperties);

        public virtual async Task<IEnumerable<T>> QueryAsync(Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
            await DataRepository.QueryAsync(predicate, includeProperties);

        public virtual async Task<PagedResult<T>> QueryPagedAsync(int page, int pageSize, Expression<Func<T, bool>> predicate, params Expression<Func<T, object>>[] includeProperties) =>
            await DataRepository.QueryPagedAsync(page, pageSize, predicate, includeProperties);

        public T GetByIdToUpdate(long id, string pEstablishmentKey, string pUniqueKey) =>
            DataRepository.GetByIdToUpdate(id, pEstablishmentKey, pUniqueKey);

        public virtual long NextSequence() => DataRepository.NextSequence();

        public virtual async Task<long> NextSequenceAsync() => await DataRepository.NextSequenceAsync();

        public virtual async Task<int> SaveChanges()
        {
            try
            {
                var ret = await DataRepository.SaveChanges();

                if (ret > 0)
                    await GenerateEntryQueueAsync();

                return ret;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                if (e is IModelEntityValidationException exception)
                    Errors = exception.MException;
                else
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.SaveSQLError,
                        Field = e.HelpLink,
                        Value = e.Source,
                        Messages = new string[] { e.Message,
                                              e?.InnerException?.Message,
                                              e?.InnerException?.InnerException?.Message }
                    });
                }
            }
            return -1;
        }

        public void RollBackChanges()
        {
            try
            {
                DataRepository.RollBackChanges();
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                if (e is IModelEntityValidationException exception)
                    Errors = exception.MException;
                else
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.SaveSQLError,
                        Field = e.HelpLink,
                        Value = e.Source,
                        Messages = new string[] { e.Message,
                                              e?.InnerException?.Message,
                                              e?.InnerException?.InnerException?.Message }
                    });
                }
            }
        }

        public void SetModelState(T model, EModelDataState pState)
        {
            try
            {
                DataRepository.SetModelState(model, pState);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                if (e is IModelEntityValidationException exception)
                    Errors = exception.MException;
                else
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.SaveSQLError,
                        Field = e.HelpLink,
                        Value = e.Source,
                        Messages = new string[] { e.Message,
                                              e?.InnerException?.Message,
                                              e?.InnerException?.InnerException?.Message }
                    });
                }
            }
        }

        public void Dispose()
        {
            DataRepository.Dispose();
            // GC.SuppressFinalize(this);
            AddedOrUpdatedItems.Clear();
            RemovedItems.Clear();
        }

        #region Generate Entry Queue
        public virtual async Task GenerateEntryQueueAsync()
        {
            await Task.Delay(1);
            if (!DontSendToQueue)
            {
                var queue = new MessageService<T>();
                foreach (var item in AddedOrUpdatedItems)
                {
                    // var Entity = (IBaseEntity)item;
                    // var dbItem = await GetByIdAsync(Entity.Id, Entity.EstablishmentKey, Entity.UniqueKey);
                    queue.SendCreatedOrUpdatedQueue(item);
                }

                foreach (var item in RemovedItems)
                    queue.SendRemovedQueue(item);
            }
            AddedOrUpdatedItems.Clear();
            RemovedItems.Clear();
        }
        #endregion

        public int CountResults(Expression<Func<T, bool>> predicate) =>
            DataRepository.CountResults(predicate);
    }
}
