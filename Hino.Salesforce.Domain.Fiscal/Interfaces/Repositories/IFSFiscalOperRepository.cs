using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal;

namespace Hino.Salesforce.Domain.Fiscal.Interfaces.Repositories
{
    public interface IFSFiscalOperRepository : IBaseRepository<FSFiscalOper>
    {
    }
}
