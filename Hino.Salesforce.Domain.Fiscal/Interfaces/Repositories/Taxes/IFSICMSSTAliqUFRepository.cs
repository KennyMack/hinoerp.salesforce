using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;

namespace Hino.Salesforce.Domain.Fiscal.Interfaces.Repositories.Taxes
{
    public interface IFSICMSSTAliqUFRepository : IBaseRepository<FSICMSSTAliqUF>
    {
    }
}
