using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal;

namespace Hino.Salesforce.Domain.Fiscal.Interfaces.Services
{
    public interface IFSFiscalOperService : IBaseService<FSFiscalOper>
    {
    }
}
