using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;

namespace Hino.Salesforce.Domain.Fiscal.Interfaces.Services.Taxes
{
    public interface IFSICMSSTAliqUFService : IBaseService<FSICMSSTAliqUF>
    {
    }
}
