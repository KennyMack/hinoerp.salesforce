using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.Fiscal.Interfaces.Repositories;
using Hino.Salesforce.Domain.Fiscal.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal;

namespace Hino.Salesforce.Domain.Fiscal.Services
{
    public class FSFiscalOperService : BaseService<FSFiscalOper>, IFSFiscalOperService
    {
        private readonly IFSFiscalOperRepository _IFSFiscalOperRepository;

        public FSFiscalOperService(IFSFiscalOperRepository pIFSFiscalOperRepository) :
             base(pIFSFiscalOperRepository)
        {
            _IFSFiscalOperRepository = pIFSFiscalOperRepository;
        }
    }
}
