using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.Fiscal.Interfaces.Repositories.Taxes;
using Hino.Salesforce.Domain.Fiscal.Interfaces.Services.Taxes;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;

namespace Hino.Salesforce.Domain.Fiscal.Services.Taxes
{
    public class FSICMSSTAliqUFService : BaseService<FSICMSSTAliqUF>, IFSICMSSTAliqUFService
    {
        private readonly IFSICMSSTAliqUFRepository _IFSICMSSTAliqUFRepository;

        public FSICMSSTAliqUFService(IFSICMSSTAliqUFRepository pIFSICMSSTAliqUFRepository) :
             base(pIFSICMSSTAliqUFRepository)
        {
            _IFSICMSSTAliqUFRepository = pIFSICMSSTAliqUFRepository;
        }
    }
}
