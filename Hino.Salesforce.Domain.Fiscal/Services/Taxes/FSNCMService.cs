﻿using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.Fiscal.Interfaces.Repositories.Taxes;
using Hino.Salesforce.Domain.Fiscal.Interfaces.Services.Taxes;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;

namespace Hino.Salesforce.Domain.Fiscal.Services.Taxes
{
    public class FSNCMService : BaseService<FSNCM>, IFSNCMService
    {
        private readonly IFSNCMRepository _IFSNCMRepository;

        public FSNCMService(IFSNCMRepository repo) : base(repo)
        {
            _IFSNCMRepository = repo;
        }
    }
}
