using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Domain.Base.Interfaces.Repositories;

namespace Hino.Salesforce.Domain.General.Events.Interfaces.Repositories
{
    public interface IGEEventsClassificationRepository : IBaseRepository<GEEventsClassification>
    {
    }
}
