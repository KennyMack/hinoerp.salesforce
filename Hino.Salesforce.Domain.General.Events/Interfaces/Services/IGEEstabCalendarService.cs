using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Domain.Base.Interfaces.Services;

namespace Hino.Salesforce.Domain.General.Events.Interfaces.Services
{
    public interface IGEEstabCalendarService : IBaseService<GEEstabCalendar>
    {
    }
}
