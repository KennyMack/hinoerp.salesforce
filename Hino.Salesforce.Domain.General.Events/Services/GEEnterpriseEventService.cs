using Hino.Salesforce.Domain.General.Events.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Events.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Domain.Base.Services;

namespace Hino.Salesforce.Domain.General.Events.Services
{
    public class GEEnterpriseEventService : BaseService<GEEnterpriseEvent>, IGEEnterpriseEventService
    {
        private readonly IGEEnterpriseEventRepository _IGEEnterpriseEventRepository;

        public GEEnterpriseEventService(IGEEnterpriseEventRepository pIGEEnterpriseEventRepository) : 
             base(pIGEEnterpriseEventRepository)
        {
            _IGEEnterpriseEventRepository = pIGEEnterpriseEventRepository;
        }
    }
}
