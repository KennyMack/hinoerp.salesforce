using Hino.Salesforce.Domain.General.Events.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Events.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Domain.Base.Services;

namespace Hino.Salesforce.Domain.General.Events.Services
{
    public class GEEstabCalendarService : BaseService<GEEstabCalendar>, IGEEstabCalendarService
    {
        private readonly IGEEstabCalendarRepository _IGEEstabCalendarRepository;

        public GEEstabCalendarService(IGEEstabCalendarRepository pIGEEstabCalendarRepository) : 
             base(pIGEEstabCalendarRepository)
        {
            _IGEEstabCalendarRepository = pIGEEstabCalendarRepository;
        }
    }
}
