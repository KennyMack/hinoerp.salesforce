using Hino.Salesforce.Domain.General.Events.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Events.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Domain.Base.Services;

namespace Hino.Salesforce.Domain.General.Events.Services
{
    public class GEEventsClassificationService : BaseService<GEEventsClassification>, IGEEventsClassificationService
    {
        private readonly IGEEventsClassificationRepository _IGEEventsClassificationRepository;

        public GEEventsClassificationService(IGEEventsClassificationRepository pIGEEventsClassificationRepository) : 
             base(pIGEEventsClassificationRepository)
        {
            _IGEEventsClassificationRepository = pIGEEventsClassificationRepository;
        }
    }
}
