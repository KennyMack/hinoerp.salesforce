using Hino.Salesforce.Domain.General.Events.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Events.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Domain.Base.Services;

namespace Hino.Salesforce.Domain.General.Events.Services
{
    public class GEEventsService : BaseService<GEEvents>, IGEEventsService
    {
        private readonly IGEEventsRepository _IGEEventsRepository;

        public GEEventsService(IGEEventsRepository pIGEEventsRepository) : 
             base(pIGEEventsRepository)
        {
            _IGEEventsRepository = pIGEEventsRepository;
        }
    }
}
