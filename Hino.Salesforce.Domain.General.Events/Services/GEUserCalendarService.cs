using Hino.Salesforce.Domain.General.Events.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Events.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Domain.Base.Services;

namespace Hino.Salesforce.Domain.General.Events.Services
{
    public class GEUserCalendarService : BaseService<GEUserCalendar>, IGEUserCalendarService
    {
        private readonly IGEUserCalendarRepository _IGEUserCalendarRepository;

        public GEUserCalendarService(IGEUserCalendarRepository pIGEUserCalendarRepository) : 
             base(pIGEUserCalendarRepository)
        {
            _IGEUserCalendarRepository = pIGEUserCalendarRepository;
        }
    }
}
