using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;

namespace Hino.Salesforce.Domain.General.Kanban.Interfaces.Repositories
{
    public interface IGEBoardUsersRepository : IBaseRepository<GEBoardUsers>
    {
    }
}
