using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;

namespace Hino.Salesforce.Domain.General.Kanban.Interfaces.Services
{
    public interface IGEFieldUsersService : IBaseService<GEFieldUsers>
    {
    }
}
