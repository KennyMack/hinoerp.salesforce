using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;

namespace Hino.Salesforce.Domain.General.Kanban.Services
{
    public class GEBoardListsService : BaseService<GEBoardLists>, IGEBoardListsService
    {
        private readonly IGEBoardListsRepository _IGEBoardListsRepository;

        public GEBoardListsService(IGEBoardListsRepository pIGEBoardListsRepository) :
             base(pIGEBoardListsRepository)
        {
            _IGEBoardListsRepository = pIGEBoardListsRepository;
        }
    }
}
