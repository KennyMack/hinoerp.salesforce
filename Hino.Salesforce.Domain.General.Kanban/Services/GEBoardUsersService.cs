using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;

namespace Hino.Salesforce.Domain.General.Kanban.Services
{
    public class GEBoardUsersService : BaseService<GEBoardUsers>, IGEBoardUsersService
    {
        private readonly IGEBoardUsersRepository _IGEBoardUsersRepository;

        public GEBoardUsersService(IGEBoardUsersRepository pIGEBoardUsersRepository) :
             base(pIGEBoardUsersRepository)
        {
            _IGEBoardUsersRepository = pIGEBoardUsersRepository;
        }
    }
}
