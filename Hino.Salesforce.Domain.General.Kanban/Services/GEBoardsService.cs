using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;

namespace Hino.Salesforce.Domain.General.Kanban.Services
{
    public class GEBoardsService : BaseService<GEBoards>, IGEBoardsService
    {
        private readonly IGEBoardsRepository _IGEBoardsRepository;

        public GEBoardsService(IGEBoardsRepository pIGEBoardsRepository) :
             base(pIGEBoardsRepository)
        {
            _IGEBoardsRepository = pIGEBoardsRepository;
        }
    }
}
