using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;

namespace Hino.Salesforce.Domain.General.Kanban.Services
{
    public class GECardFieldsService : BaseService<GECardFields>, IGECardFieldsService
    {
        private readonly IGECardFieldsRepository _IGECardFieldsRepository;

        public GECardFieldsService(IGECardFieldsRepository pIGECardFieldsRepository) :
             base(pIGECardFieldsRepository)
        {
            _IGECardFieldsRepository = pIGECardFieldsRepository;
        }
    }
}
