using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;

namespace Hino.Salesforce.Domain.General.Kanban.Services
{
    public class GEFieldUsersService : BaseService<GEFieldUsers>, IGEFieldUsersService
    {
        private readonly IGEFieldUsersRepository _IGEFieldUsersRepository;

        public GEFieldUsersService(IGEFieldUsersRepository pIGEFieldUsersRepository) :
             base(pIGEFieldUsersRepository)
        {
            _IGEFieldUsersRepository = pIGEFieldUsersRepository;
        }
    }
}
