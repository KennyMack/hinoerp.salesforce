using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;

namespace Hino.Salesforce.Domain.General.Kanban.Services
{
    public class GESettingsService : BaseService<GESettings>, IGESettingsService
    {
        private readonly IGESettingsRepository _IGESettingsRepository;

        public GESettingsService(IGESettingsRepository pIGESettingsRepository) :
             base(pIGESettingsRepository)
        {
            _IGESettingsRepository = pIGESettingsRepository;
        }
    }
}
