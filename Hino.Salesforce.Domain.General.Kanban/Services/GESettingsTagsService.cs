using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Kanban;

namespace Hino.Salesforce.Domain.General.Kanban.Services
{
    public class GESettingsTagsService : BaseService<GESettingsTags>, IGESettingsTagsService
    {
        private readonly IGESettingsTagsRepository _IGESettingsTagsRepository;

        public GESettingsTagsService(IGESettingsTagsRepository pIGESettingsTagsRepository) :
             base(pIGESettingsTagsRepository)
        {
            _IGESettingsTagsRepository = pIGESettingsTagsRepository;
        }
    }
}
