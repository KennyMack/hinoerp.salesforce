﻿using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Interfaces.Repositories.Business
{
    public interface IGEEnterpriseAnnotationRepository : IBaseRepository<GEEnterpriseAnnot>
    {
    }
}
