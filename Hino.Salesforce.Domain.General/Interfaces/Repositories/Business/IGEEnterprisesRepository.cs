using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Interfaces.Repositories.Business
{
    public interface IGEEnterprisesRepository : IBaseRepository<GEEnterprises>
    {
        Task UpdateEnterpriseRegionIdAsync(string pEstablishmentKey, string pUF, long pRegionId);
        Task<GEEnterprises> GetEnterprise(string pEstablishmentKey, long pId);
    }
}
