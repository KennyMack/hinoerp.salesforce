using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;

namespace Hino.Salesforce.Domain.General.Interfaces.Repositories.Business
{
    public interface IGEPaymentConditionRepository : IBaseRepository<GEPaymentCondition>
    {
    }
}
