using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Interfaces.Repositories.Business
{
    public interface IGEUserEnterprisesRepository : IBaseRepository<GEUserEnterprises>
    {
        Task<PagedResult<GEUserEnterprises>> QuerySearchPagedAsync(int page, int pageSize,
            Expression<Func<GEUserEnterprises, bool>> predicate,
            params Expression<Func<GEUserEnterprises, object>>[] includeProperties);
    }
}
