using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Domain.Base.Interfaces.Repositories;

namespace Hino.Salesforce.Domain.General.Interfaces.Repositories.Business
{
    public interface IGEUserPayTypeRepository : IBaseRepository<GEUserPayType>
    {
    }
}
