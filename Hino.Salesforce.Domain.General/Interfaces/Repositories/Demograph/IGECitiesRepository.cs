﻿using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;

namespace Hino.Salesforce.Domain.General.Interfaces.Repositories.Demograph
{
    public interface IGECitiesRepository : IBaseRepository<GECities>
    {
    }
}
