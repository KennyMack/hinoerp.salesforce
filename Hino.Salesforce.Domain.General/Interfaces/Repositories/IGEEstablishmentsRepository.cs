using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Interfaces.Repositories
{
    public interface IGEEstablishmentsRepository : IBaseRepository<GEEstablishments>
    {
        Task<bool> HasEstablishmentDependencyAsync(string pEstablishmentKey);
        Task<IEnumerable<GEEstablishments>> GetByIdAndEstablishmentKeyAsync(long pId, string pEstablishmentKey);
        Task<bool> ExistsEstablishmentAsync(string pEstablishmentKey);
        Task<IEnumerable<GEEstabDevices>> GetDevicesEstablishment(string pEstablishmentKey, string pUniqueKey, long pId);
        Task<GEEstablishments> GetByEstablishmentKeyAsync(string pEstablishmentKey);
    }
}
