﻿using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General;

namespace Hino.Salesforce.Domain.General.Interfaces.Repositories
{
    public interface IGEProductAplicRepository : IBaseRepository<GEProductAplic>
    {
    }
}
