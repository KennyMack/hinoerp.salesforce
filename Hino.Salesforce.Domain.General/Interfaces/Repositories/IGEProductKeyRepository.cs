﻿using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Interfaces.Repositories
{
    public interface IGEProductKeyRepository : IBaseRepository<GEProductKey>
    {
        Task<int> GetSequenceByReferenceAsync(string pEstablishmentKey, string pReference);
    }
}
