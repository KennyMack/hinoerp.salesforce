using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.General;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Interfaces.Repositories
{
    public interface IGEUsersRepository : IBaseRepository<GEUsers>
    {
        Task<GEUsers> GetByUsernameAsync(string username, string pEstablishmentKey, params Expression<Func<GEUsers, object>>[] includeProperties);
        Task<GEUsers> GetByEmailAsync(string email, string pEstablishmentKey, params Expression<Func<GEUsers, object>>[] includeProperties);
    }
}
