using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;

namespace Hino.Salesforce.Domain.General.Interfaces.Services.Business
{
    public interface IGEEnterpriseFiscalGroupService : IBaseService<GEEnterpriseFiscalGroup>
    {
    }
}
