using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Interfaces.Services.Business
{
    public interface IGEEnterprisesService : IBaseService<GEEnterprises>
    {
        GEEnterprises CreateEnterprise(GEEnterprises model);
        Task UpdateEnterpriseRegionId(string pEstablishmentKey, string pUF, long pRegionId);
        Task<GEEnterprises> GetEnterprise(string pEstablishmentKey, long pId);
    }
}
