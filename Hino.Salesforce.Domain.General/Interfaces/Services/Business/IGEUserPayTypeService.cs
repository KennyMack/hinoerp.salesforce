using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Domain.Base.Interfaces.Services;

namespace Hino.Salesforce.Domain.General.Interfaces.Services.Business
{
    public interface IGEUserPayTypeService : IBaseService<GEUserPayType>
    {
    }
}
