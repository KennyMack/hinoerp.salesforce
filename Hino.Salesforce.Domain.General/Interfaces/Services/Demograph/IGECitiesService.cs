using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;

namespace Hino.Salesforce.Domain.General.Interfaces.Services.Demograph
{
    public interface IGECitiesService : IBaseService<GECities>
    {
    }
}
