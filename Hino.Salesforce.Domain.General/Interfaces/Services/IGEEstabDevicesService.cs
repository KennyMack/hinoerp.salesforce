using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Interfaces.Services
{
    public interface IGEEstabDevicesService : IBaseService<GEEstabDevices>
    {
        Task<GEEstabDevices> CreateDeviceAsync(GEEstabDevices model);
        int DevicesCount(long pIdEstablishment, string pEstablishmentKey);
    }
}
