using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;

namespace Hino.Salesforce.Domain.General.Interfaces.Services
{
    public interface IGEEstabMenuService : IBaseService<GEEstabMenu>
    {
    }
}
