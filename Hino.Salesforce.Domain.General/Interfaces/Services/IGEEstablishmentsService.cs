using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Interfaces.Services
{
    public interface IGEEstablishmentsService : IBaseService<GEEstablishments>
    {
        Task<bool> HasEstablishmentDependencyAsync(string pEstablishmentKey);
        Task<IEnumerable<GEEstablishments>> GetByIdAndEstablishmentKeyAsync(long pId, string pEstablishmentKey);
        Task<GEEstablishments> UpdateEstablishmentAsync(GEEstablishments pEstab);
        Task<bool> ExistsEstablishmentAsync(string pEstablishmentKey);
        Task<IEnumerable<GEEstabDevices>> GetDevicesEstablishment(string pEstablishmentKey, string pUniqueKey, long pId);
        Task<GEEstablishments> GetByEstablishmentKeyAsync(string pEstablishmentKey);
    }
}
