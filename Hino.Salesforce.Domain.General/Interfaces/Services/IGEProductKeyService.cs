﻿using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Interfaces.Services
{
    public interface IGEProductKeyService : IBaseService<GEProductKey>
    {
        Task<int> GetSequenceByReferenceAsync(string pEstablishmentKey, string pReference);
        Task NextSequenceByReferenceAsync(string pEstablishmentKey, string pReference);
    }
}
