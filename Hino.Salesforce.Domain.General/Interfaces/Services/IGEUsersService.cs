using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Interfaces.Services
{
    public interface IGEUsersService : IBaseService<GEUsers>
    {
        Task<GEUsers> GetByUsernameAsync(string username, string pEstablishmentKey, params Expression<Func<GEUsers, object>>[] includeProperties);
        Task<GEUsers> GetByEmailAsync(string email, string pEstablishmentKey, params Expression<Func<GEUsers, object>>[] includeProperties);
        Task<GEUsers> CreateAdminUserAsync(string pHinoId, GEUsers pGEUsersVM);
        Task<GEUsers> CreateUserAsync(GEUsers pGEUsers, string OriginCreate);
        Task<GEUsers> UpdateUser(GEUsers model);
        Task<GEUsers> ChangePassword(GEUsers model);
    }
}
