﻿using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Services.Business
{
    public class GEEnterpriseAnnotationService : BaseService<GEEnterpriseAnnot>, IGEEnterpriseAnnotationService
    {
        private readonly IGEEnterpriseAnnotationRepository _IGEEnterpriseAnnotationRepository;

        public GEEnterpriseAnnotationService(IGEEnterpriseAnnotationRepository pIGEEnterpriseAnnotationRepository) :
             base(pIGEEnterpriseAnnotationRepository)
        {
            _IGEEnterpriseAnnotationRepository = pIGEEnterpriseAnnotationRepository;
        }
    }
}
