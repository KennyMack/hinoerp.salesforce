using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;

namespace Hino.Salesforce.Domain.General.Services.Business
{
    public class GEEnterpriseCategoryService : BaseService<GEEnterpriseCategory>, IGEEnterpriseCategoryService
    {
        private readonly IGEEnterpriseCategoryRepository _IGEEnterpriseCategoryRepository;

        public GEEnterpriseCategoryService(IGEEnterpriseCategoryRepository pIGEEnterpriseCategoryRepository) :
             base(pIGEEnterpriseCategoryRepository)
        {
            _IGEEnterpriseCategoryRepository = pIGEEnterpriseCategoryRepository;
        }
    }
}
