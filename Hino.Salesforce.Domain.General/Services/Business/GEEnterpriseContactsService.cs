using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;

namespace Hino.Salesforce.Domain.General.Services.Business
{
    public class GEEnterpriseContactsService : BaseService<GEEnterpriseContacts>, IGEEnterpriseContactsService
    {
        private readonly IGEEnterpriseContactsRepository _IGEEnterpriseContactsRepository;

        public GEEnterpriseContactsService(IGEEnterpriseContactsRepository pIGEEnterpriseContactsRepository) :
             base(pIGEEnterpriseContactsRepository)
        {
            _IGEEnterpriseContactsRepository = pIGEEnterpriseContactsRepository;
        }
    }
}
