using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;

namespace Hino.Salesforce.Domain.General.Services.Business
{
    public class GEEnterpriseFiscalGroupService : BaseService<GEEnterpriseFiscalGroup>, IGEEnterpriseFiscalGroupService
    {
        private readonly IGEEnterpriseFiscalGroupRepository _IGEEnterpriseFiscalGroupRepository;

        public GEEnterpriseFiscalGroupService(IGEEnterpriseFiscalGroupRepository pIGEEnterpriseFiscalGroupRepository) :
             base(pIGEEnterpriseFiscalGroupRepository)
        {
            _IGEEnterpriseFiscalGroupRepository = pIGEEnterpriseFiscalGroupRepository;
        }
    }
}
