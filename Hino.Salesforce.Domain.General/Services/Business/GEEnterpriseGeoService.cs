using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;

namespace Hino.Salesforce.Domain.General.Services.Business
{
    public class GEEnterpriseGeoService : BaseService<GEEnterpriseGeo>, IGEEnterpriseGeoService
    {
        private readonly IGEEnterpriseGeoRepository _IGEEnterpriseGeoRepository;

        public GEEnterpriseGeoService(IGEEnterpriseGeoRepository pIGEEnterpriseGeoRepository) :
             base(pIGEEnterpriseGeoRepository)
        {
            _IGEEnterpriseGeoRepository = pIGEEnterpriseGeoRepository;
        }
    }
}
