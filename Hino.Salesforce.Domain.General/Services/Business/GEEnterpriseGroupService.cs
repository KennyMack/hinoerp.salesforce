using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;

namespace Hino.Salesforce.Domain.General.Services.Business
{
    public class GEEnterpriseGroupService : BaseService<GEEnterpriseGroup>, IGEEnterpriseGroupService
    {
        private readonly IGEEnterpriseGroupRepository _IGEEnterpriseGroupRepository;

        public GEEnterpriseGroupService(IGEEnterpriseGroupRepository pIGEEnterpriseGroupRepository) :
             base(pIGEEnterpriseGroupRepository)
        {
            _IGEEnterpriseGroupRepository = pIGEEnterpriseGroupRepository;
        }
    }
}
