using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Services.Business
{
    public class GEEnterprisesService : BaseService<GEEnterprises>, IGEEnterprisesService
    {
        private readonly IGEEnterprisesRepository _IGEEnterprisesRepository;

        public GEEnterprisesService(IGEEnterprisesRepository pIGEEnterprisesRepository) :
             base(pIGEEnterprisesRepository)
        {
            _IGEEnterprisesRepository = pIGEEnterprisesRepository;
        }

        public async Task UpdateEnterpriseRegionId(string pEstablishmentKey, string pUF, long pRegionId) =>
            await _IGEEnterprisesRepository.UpdateEnterpriseRegionIdAsync(pEstablishmentKey, pUF, pRegionId);

        public GEEnterprises CreateEnterprise(GEEnterprises model)
        {
            /*model.StatusSinc = Infra.Cross.Utils.Enums.EStatusSinc.Waiting;
            model.Status = Infra.Cross.Utils.Enums.EStatusEnterprise.New;*/

            try
            {
                _IGEEnterprisesRepository.Add(model);
                AddedOrUpdatedItems.Add(model);
            }
            catch (Exception e)
            {
                Hino.Salesforce.Infra.Cross.Utils.Exceptions.Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InsertSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.InnerException?.Message  }
                });
            }

            return model;
        }

        public async Task<GEEnterprises> GetEnterprise(string pEstablishmentKey, long pId) =>
            await _IGEEnterprisesRepository.GetEnterprise(pEstablishmentKey, pId);

        /*
        public async override Task GenerateEntryQueueAsync()
        {
            var queue = new MessageService<GEEnterprises>();

            foreach (var item in AddedOrUpdatedItems)
            {
                var dbItem = await GetByIdAsync(item.Id, item.EstablishmentKey, item.UniqueKey,
                    s => s.GEPaymentCondition,
                    x => x.GEPaymentCondition.GEPaymentType,
                    g => g.GEEnterpriseGeo,
                    t => t.GEEnterpriseContacts,
                    h => h.GEEnterpriseFiscalGroup);
                queue.SendCreatedOrUpdatedQueue(dbItem);
            }

            foreach (var item in RemovedItems)
                queue.SendRemovedQueue(item);

            AddedOrUpdatedItems.Clear();
            RemovedItems.Clear();
        }*/
    }
}
