using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;

namespace Hino.Salesforce.Domain.General.Services.Business
{
    public class GEPaymentCondInstallmentsService : BaseService<GEPaymentCondInstallments>, IGEPaymentCondInstallmentsService
    {
        private readonly IGEPaymentCondInstallmentsRepository _IGEPaymentCondInstallmentsRepository;

        public GEPaymentCondInstallmentsService(IGEPaymentCondInstallmentsRepository pIGEPaymentCondInstallmentsRepository) :
             base(pIGEPaymentCondInstallmentsRepository)
        {
            _IGEPaymentCondInstallmentsRepository = pIGEPaymentCondInstallmentsRepository;
        }
    }
}
