using Hino.Salesforce.Domain.Base.Messages;
using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Services.Business
{
    public class GEPaymentConditionService : BaseService<GEPaymentCondition>, IGEPaymentConditionService
    {
        private readonly IGEPaymentConditionRepository _IGEPaymentConditionRepository;

        public GEPaymentConditionService(IGEPaymentConditionRepository pIGEPaymentConditionRepository) :
             base(pIGEPaymentConditionRepository)
        {
            _IGEPaymentConditionRepository = pIGEPaymentConditionRepository;
        }

        public async override Task GenerateEntryQueueAsync()
        {
            var queue = new MessageService<GEPaymentCondition>();

            foreach (var item in AddedOrUpdatedItems)
            {
                var dbItem = await GetByIdAsync(item.Id, item.EstablishmentKey, item.UniqueKey,
                    r => r.GEPaymentType,
                    s => s.GEPaymentCondInstallments);
                queue.SendCreatedOrUpdatedQueue(dbItem);
            }

            foreach (var item in RemovedItems)
                queue.SendRemovedQueue(item);

            AddedOrUpdatedItems.Clear();
            RemovedItems.Clear();
        }
    }
}
