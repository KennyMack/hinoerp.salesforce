using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;

namespace Hino.Salesforce.Domain.General.Services.Business
{
    public class GEPaymentTypeService : BaseService<GEPaymentType>, IGEPaymentTypeService
    {
        private readonly IGEPaymentTypeRepository _IGEPaymentTypeRepository;

        public GEPaymentTypeService(IGEPaymentTypeRepository pIGEPaymentTypeRepository) :
             base(pIGEPaymentTypeRepository)
        {
            _IGEPaymentTypeRepository = pIGEPaymentTypeRepository;
        }
    }
}
