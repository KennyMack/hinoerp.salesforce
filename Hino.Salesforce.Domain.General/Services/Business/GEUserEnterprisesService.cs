using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Services.Business
{
    public class GEUserEnterprisesService : BaseService<GEUserEnterprises>, IGEUserEnterprisesService
    {
        private readonly IGEUserEnterprisesRepository _IGEUserEnterprisesRepository;

        public GEUserEnterprisesService(IGEUserEnterprisesRepository pIGEUserEnterprisesRepository) :
             base(pIGEUserEnterprisesRepository)
        {
            _IGEUserEnterprisesRepository = pIGEUserEnterprisesRepository;
        }

        public async Task<PagedResult<GEUserEnterprises>> QuerySearchPagedAsync(int page, int pageSize, Expression<Func<GEUserEnterprises, bool>> predicate, params Expression<Func<GEUserEnterprises, object>>[] includeProperties) =>
            await _IGEUserEnterprisesRepository.QuerySearchPagedAsync(page, pageSize, predicate, includeProperties);
    }
}
