using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Domain.Base.Services;

namespace Hino.Salesforce.Domain.General.Services.Business
{
    public class GEUserPayTypeService : BaseService<GEUserPayType>, IGEUserPayTypeService
    {
        private readonly IGEUserPayTypeRepository _IGEUserPayTypeRepository;

        public GEUserPayTypeService(IGEUserPayTypeRepository pIGEUserPayTypeRepository) : 
             base(pIGEUserPayTypeRepository)
        {
            _IGEUserPayTypeRepository = pIGEUserPayTypeRepository;
        }
    }
}
