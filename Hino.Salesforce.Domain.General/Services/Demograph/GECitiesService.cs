using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories.Demograph;
using Hino.Salesforce.Domain.General.Interfaces.Services.Demograph;
using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;

namespace Hino.Salesforce.Domain.General.Services.Demograph
{
    public class GECitiesService : BaseService<GECities>, IGECitiesService
    {
        private readonly IGECitiesRepository _IGECitiesRepository;

        public GECitiesService(IGECitiesRepository pIGECitiesRepository) :
             base(pIGECitiesRepository)
        {
            _IGECitiesRepository = pIGECitiesRepository;
        }
    }
}
