﻿using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories.Demograph;
using Hino.Salesforce.Domain.General.Interfaces.Services.Demograph;
using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;

namespace Hino.Salesforce.Domain.General.Services.Demograph
{
    public class GECountriesService : BaseService<GECountries>, IGECountriesService
    {
        private readonly IGECountriesRepository _IGECountriesRepository;

        public GECountriesService(IGECountriesRepository pIGECountriesRepository) :
            base(pIGECountriesRepository)
        {
            _IGECountriesRepository = pIGECountriesRepository;
        }
    }
}
