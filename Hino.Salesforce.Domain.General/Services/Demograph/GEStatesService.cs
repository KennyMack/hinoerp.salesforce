using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories.Demograph;
using Hino.Salesforce.Domain.General.Interfaces.Services.Demograph;
using Hino.Salesforce.Infra.Cross.Entities.General.Demograph;

namespace Hino.Salesforce.Domain.General.Services.Demograph
{
    public class GEStatesService : BaseService<GEStates>, IGEStatesService
    {
        private readonly IGEStatesRepository _IGEStatesRepository;

        public GEStatesService(IGEStatesRepository pIGEStatesRepository) :
             base(pIGEStatesRepository)
        {
            _IGEStatesRepository = pIGEStatesRepository;
        }
    }
}
