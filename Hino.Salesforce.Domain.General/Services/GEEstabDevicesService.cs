using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Services
{
    public class GEEstabDevicesService : BaseService<GEEstabDevices>, IGEEstabDevicesService
    {
        private readonly IGEEstabDevicesRepository _IGEEstabDevicesRepository;
        private readonly IGEEstablishmentsRepository _IGEEstablishmentsRepository;

        public GEEstabDevicesService(IGEEstabDevicesRepository pIGEEstabDevicesRepository,
            IGEEstablishmentsRepository pIGEEstablishmentsRepository) :
             base(pIGEEstabDevicesRepository)
        {
            _IGEEstabDevicesRepository = pIGEEstabDevicesRepository;
            _IGEEstablishmentsRepository = pIGEEstablishmentsRepository;
        }

        public async Task<GEEstabDevices> CreateDeviceAsync(GEEstabDevices model)
        {
            var Establishment = (await _IGEEstablishmentsRepository.
                GetByIdAndEstablishmentKeyAsync(
                    model.GEEstabID,
                    model.EstablishmentKey))
                .FirstOrDefault();
            try
            {
                if (Establishment == null)
                {
                    var resource = new System.Resources.ResourceManager(typeof(Infra.Cross.Resources.ErrorMessagesResource));

                    throw new Exception(resource.GetString("IdAndEstablishmentKeyError"))
                    {
                        HelpLink = "pEstablishmentKey",
                        Source = model.EstablishmentKey
                    };
                }
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message }
                });
                return model;
            }

            try
            {
                if ((DevicesCount(model.GEEstabID, model.EstablishmentKey) + 1) > Establishment.Devices)
                {
                    var resource = new System.Resources.ResourceManager(typeof(Infra.Cross.Resources.ErrorMessagesResource));

                    throw new Exception(resource.GetString("TotalDevicesReached"))
                    {
                        HelpLink = "Devices",
                        Source = Establishment.Devices.ToString()
                    };
                }

                model.UserKey = Guid.NewGuid().ToString();
                _IGEEstabDevicesRepository.Add(model);
                await _IGEEstabDevicesRepository.SaveChanges();
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InsertSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.InnerException?.Message  }
                });
            }

            return model;
        }

        public int DevicesCount(long pIdEstablishment, string pEstablishmentKey)
        {
            var result = _IGEEstabDevicesRepository.QueryAsync(r =>
                  r.GEEstabID == pIdEstablishment &&
                  r.EstablishmentKey == pEstablishmentKey &&
                  r.IsActive).Result;
            return result.Count();
        }
    }
}
