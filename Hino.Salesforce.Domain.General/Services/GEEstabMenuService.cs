using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;

namespace Hino.Salesforce.Domain.General.Services
{
    public class GEEstabMenuService : BaseService<GEEstabMenu>, IGEEstabMenuService
    {
        private readonly IGEEstabMenuRepository _IGEEstabMenuRepository;

        public GEEstabMenuService(IGEEstabMenuRepository pIGEEstabMenuRepository) :
             base(pIGEEstabMenuRepository)
        {
            _IGEEstabMenuRepository = pIGEEstabMenuRepository;
        }
    }
}
