using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Services
{
    public class GEEstablishmentsService : BaseService<GEEstablishments>, IGEEstablishmentsService
    {
        private readonly IGEEstablishmentsRepository _IGEEstablishmentsRepository;

        public GEEstablishmentsService(IGEEstablishmentsRepository pIGEEstablishmentsRepository) :
             base(pIGEEstablishmentsRepository)
        {
            _IGEEstablishmentsRepository = pIGEEstablishmentsRepository;
        }

        public async Task<bool> ExistsEstablishmentAsync(string pEstablishmentKey)
        {
            try
            {
                return (await _IGEEstablishmentsRepository.ExistsEstablishmentAsync(pEstablishmentKey));
            }
            catch (Exception e)
            {
                Logging.Exception(e);
            }
            return false;
        }


        public async Task<IEnumerable<GEEstablishments>> GetByIdAndEstablishmentKeyAsync(long pId, string pEstablishmentKey) =>
            await _IGEEstablishmentsRepository.GetByIdAndEstablishmentKeyAsync(pId, pEstablishmentKey);

        public async Task<bool> HasEstablishmentDependencyAsync(string pEstablishmentKey)
        {
            try
            {
                return await _IGEEstablishmentsRepository.HasEstablishmentDependencyAsync(pEstablishmentKey);
            }
            catch (System.Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.SQLCommandError,
                    Field = "pEstablishmentKey",
                    Messages = new[] {
                        e.Message,
                        e?.InnerException?.InnerException?.Message
                    },
                    Value = pEstablishmentKey
                });
                return false;
            }
        }

        public async Task<GEEstablishments> UpdateEstablishmentAsync(GEEstablishments pEstab)
        {
            var EstabDB = _IGEEstablishmentsRepository.GetByIdToUpdate(
                    pEstab.Id,
                    pEstab.EstablishmentKey,
                    pEstab.UniqueKey);

            if (EstabDB == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "Estab",
                    Value = pEstab.Id.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.ValidationMessagesResource.NotFound }
                });
                return null;
            }

            try
            {
                EstabDB.RazaoSocial = pEstab.RazaoSocial;
                EstabDB.NomeFantasia = pEstab.NomeFantasia;
                EstabDB.Email = pEstab.Email;
                EstabDB.Phone = pEstab.Phone;
                EstabDB.CNPJCPF = pEstab.CNPJCPF;
                EstabDB.Devices = pEstab.Devices;
                EstabDB.IsActive = pEstab.IsActive;
                EstabDB.PIS = pEstab.PIS;
                EstabDB.COFINS = pEstab.COFINS;
                EstabDB.AllowChangePrice = pEstab.AllowChangePrice;
                EstabDB.AllowDiscount = pEstab.AllowDiscount;
                EstabDB.AllowEnterprise = pEstab.AllowEnterprise;
                EstabDB.AllowPayment = pEstab.AllowPayment;
                EstabDB.AditionalInfo = pEstab.AditionalInfo;
                EstabDB.DefaultFiscalOperID = pEstab.DefaultFiscalOperID;
                EstabDB.DefaultNoteOrder = pEstab.DefaultNoteOrder;
                EstabDB.CapptaKey = pEstab.CapptaKey;
                EstabDB.SitefIp = pEstab.SitefIp;
                EstabDB.FatorR = pEstab.FatorR;
                EstabDB.TokenCNPJ = pEstab.TokenCNPJ;
                EstabDB.OnlyWithStock = pEstab.OnlyWithStock;
                EstabDB.OnlyOnDate = pEstab.OnlyOnDate;
                EstabDB.PfFiscalGroupId = pEstab.PfFiscalGroupId;
                EstabDB.PjFiscalGroupId = pEstab.PjFiscalGroupId;
                EstabDB.PfClassifClient = pEstab.PfClassifClient;
                EstabDB.PjClassifClient = pEstab.PjClassifClient;
                EstabDB.DaysPayment = pEstab.DaysPayment;
                EstabDB.SearchPosition = pEstab.SearchPosition;
                EstabDB.DefaultProductId = pEstab.DefaultProductId;

                EstabDB.UseChangePrice = pEstab.UseChangePrice;
                EstabDB.UsePayCondition = pEstab.UsePayCondition;
                EstabDB.UseBonification = pEstab.UseBonification;
                EstabDB.UseSample = pEstab.UseSample;

                _IGEEstablishmentsRepository.Update(EstabDB);
                await _IGEEstablishmentsRepository.SaveChanges();
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.UpdateSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.InnerException?.Message  }
                });
            }

            return EstabDB;
        }

        public async Task<IEnumerable<GEEstabDevices>> GetDevicesEstablishment(string pEstablishmentKey, string pUniqueKey, long pId) =>
            await _IGEEstablishmentsRepository.GetDevicesEstablishment(pEstablishmentKey, pUniqueKey, pId);

        public async Task<GEEstablishments> GetByEstablishmentKeyAsync(string pEstablishmentKey) =>
            await _IGEEstablishmentsRepository.GetByEstablishmentKeyAsync(pEstablishmentKey);
    }
}
