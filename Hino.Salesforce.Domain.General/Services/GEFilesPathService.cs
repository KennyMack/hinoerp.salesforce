using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;

namespace Hino.Salesforce.Domain.General.Services
{
    public class GEFilesPathService : BaseService<GEFilesPath>, IGEFilesPathService
    {
        private readonly IGEFilesPathRepository _IGEFilesPathRepository;

        public GEFilesPathService(IGEFilesPathRepository pIGEFilesPathRepository) :
             base(pIGEFilesPathRepository)
        {
            _IGEFilesPathRepository = pIGEFilesPathRepository;
        }
    }
}
