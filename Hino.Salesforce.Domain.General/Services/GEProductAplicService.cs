﻿using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;

namespace Hino.Salesforce.Domain.General.Services
{
    public class GEProductAplicService : BaseService<GEProductAplic>, IGEProductAplicService
    {
        private readonly IGEProductAplicRepository _IGEProductAplicRepository;

        public GEProductAplicService(IGEProductAplicRepository pIGEProductAplicRepository) :
             base(pIGEProductAplicRepository)
        {
            _IGEProductAplicRepository = pIGEProductAplicRepository;
        }
    }
}
