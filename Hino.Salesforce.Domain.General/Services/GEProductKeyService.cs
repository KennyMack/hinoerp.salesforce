﻿using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;
using System.Threading.Tasks;
using System.Linq;

namespace Hino.Salesforce.Domain.General.Services
{
    public class GEProductKeyService : BaseService<GEProductKey>, IGEProductKeyService
    {
        private readonly IGEProductKeyRepository _IGEProductKeyRepository;

        public GEProductKeyService(IGEProductKeyRepository pIGEProductKeyRepository) :
             base(pIGEProductKeyRepository)
        {
            _IGEProductKeyRepository = pIGEProductKeyRepository;
        }

        public async Task<int> GetSequenceByReferenceAsync(string pEstablishmentKey, string pReference) =>
            await _IGEProductKeyRepository.GetSequenceByReferenceAsync(pEstablishmentKey, pReference);

        public async Task NextSequenceByReferenceAsync(string pEstablishmentKey, string pReference)
        {
            var ProductKeyFind = (await QueryAsync(r => r.EstablishmentKey == pEstablishmentKey &&
                r.Reference == pReference)).FirstOrDefault();

            GEProductKey ProductKey = GetByIdToUpdate(ProductKeyFind?.Id ?? 0, pEstablishmentKey, ProductKeyFind?.UniqueKey ?? "");
            if (ProductKey == null)
                ProductKey = new GEProductKey();

            ProductKey.EstablishmentKey = pEstablishmentKey;
            ProductKey.Reference = pReference;
            ProductKey.Sequence++;

            if (ProductKey.Id > 0)
                _IGEProductKeyRepository.Update(ProductKey);
            else
                _IGEProductKeyRepository.Add(ProductKey);
            await _IGEProductKeyRepository.SaveChanges();
        }
    }
}
