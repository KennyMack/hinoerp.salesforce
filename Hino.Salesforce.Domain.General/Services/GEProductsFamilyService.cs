using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;

namespace Hino.Salesforce.Domain.General.Services
{
    public class GEProductsFamilyService : BaseService<GEProductsFamily>, IGEProductsFamilyService
    {
        private readonly IGEProductsFamilyRepository _IGEProductsFamilyRepository;

        public GEProductsFamilyService(IGEProductsFamilyRepository pIGEProductsFamilyRepository) :
             base(pIGEProductsFamilyRepository)
        {
            _IGEProductsFamilyRepository = pIGEProductsFamilyRepository;
        }
    }
}
