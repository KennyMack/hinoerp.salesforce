using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;

namespace Hino.Salesforce.Domain.General.Services
{
    public class GEProductsService : BaseService<GEProducts>, IGEProductsService
    {
        private readonly IGEProductsRepository _IGEProductsRepository;

        public GEProductsService(IGEProductsRepository pIGEProductsRepository) :
             base(pIGEProductsRepository)
        {
            _IGEProductsRepository = pIGEProductsRepository;
        }
        /*
        public async override Task GenerateEntryQueueAsync()
        {
            var queue = new MessageService<GEProducts>();

            foreach (var item in AddedOrUpdatedItems)
            {
                var dbItem = await GetByIdAsync(item.Id, item.EstablishmentKey, item.UniqueKey,
                    s => s.GEProductsFamily,
                    t => t.GEProductsType,
                    g => g.GEProductsSaleUnit,
                    l => l.GEProductsUnit,
                    f => f.FSNCM);
                queue.SendCreatedOrUpdatedQueue(dbItem);
            }

            foreach (var item in RemovedItems)
                queue.SendRemovedQueue(item);

            AddedOrUpdatedItems.Clear();
            RemovedItems.Clear();
        }
        */
    }
}
