using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;

namespace Hino.Salesforce.Domain.General.Services
{
    public class GEProductsTypeService : BaseService<GEProductsType>, IGEProductsTypeService
    {
        private readonly IGEProductsTypeRepository _IGEProductsTypeRepository;

        public GEProductsTypeService(IGEProductsTypeRepository pIGEProductsTypeRepository) :
             base(pIGEProductsTypeRepository)
        {
            _IGEProductsTypeRepository = pIGEProductsTypeRepository;
        }
    }
}
