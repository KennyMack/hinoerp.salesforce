using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;

namespace Hino.Salesforce.Domain.General.Services
{
    public class GEProductsUnitService : BaseService<GEProductsUnit>, IGEProductsUnitService
    {
        private readonly IGEProductsUnitRepository _IGEProductsUnitRepository;

        public GEProductsUnitService(IGEProductsUnitRepository pIGEProductsUnitRepository) :
             base(pIGEProductsUnitRepository)
        {
            _IGEProductsUnitRepository = pIGEProductsUnitRepository;
        }
    }
}
