using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Identity;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.General.Services
{
    public class GEUsersService : BaseService<GEUsers>, IGEUsersService
    {
        private readonly IGEUsersRepository _IGEUsersRepository;

        public GEUsersService(IGEUsersRepository pIGEUsersRepository) :
             base(pIGEUsersRepository)
        {
            _IGEUsersRepository = pIGEUsersRepository;
        }

        public async Task<GEUsers> GetByEmailAsync(string email, string pEstablishmentKey, params Expression<Func<GEUsers, object>>[] includeProperties) =>
            await _IGEUsersRepository.GetByEmailAsync(email, pEstablishmentKey, includeProperties);

        public async Task<GEUsers> GetByUsernameAsync(string username, string pEstablishmentKey, params Expression<Func<GEUsers, object>>[] includeProperties) =>
            await _IGEUsersRepository.GetByUsernameAsync(username, pEstablishmentKey, includeProperties);

        public async Task<GEUsers> CreateAdminUserAsync(string pHinoId, GEUsers pGEUsers)
        {
            try
            {
                if (pHinoId != pGEUsers.EstablishmentKey)
                    throw new Exception(Infra.Cross.Resources.ErrorMessagesResource.InvalidInternalEstabID);

                pGEUsers.LastLogin = DateTime.Now;
                pGEUsers.UserKey = Guid.NewGuid().ToString();
                pGEUsers.Password = Metis.CryptPass(pGEUsers.Password);


                _IGEUsersRepository.Add(pGEUsers);
                await _IGEUsersRepository.SaveChanges();
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InsertSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.InnerException?.Message  }
                });
            }

            return pGEUsers;
        }

        public async Task<GEUsers> CreateUserAsync(GEUsers pGEUsers, string OriginCreate)
        {
            try
            {
                var results = await _IGEUsersRepository.QueryAsync(r =>
                        r.EstablishmentKey == pGEUsers.EstablishmentKey &&
                        r.Email == pGEUsers.Email);

                if (results.Count() > 0)
                    throw new Exception(Infra.Cross.Resources.ValidationMessagesResource.EmailWasUsed);

                results = await _IGEUsersRepository.QueryAsync(r =>
                        r.EstablishmentKey == pGEUsers.EstablishmentKey &&
                        r.UserName == pGEUsers.UserName);

                if (results.Count() > 0)
                    throw new Exception(Infra.Cross.Resources.ValidationMessagesResource.UserNameWasUsed);

                if (OriginCreate == "MOBILE")
                {
                    results = await _IGEUsersRepository.QueryAsync(r =>
                        r.UserKey == pGEUsers.UserKey &&
                        r.EstablishmentKey == pGEUsers.EstablishmentKey &&
                        r.IsActive);

                    var result = results.Count();

                    if (result > 0)
                        throw new Exception(Infra.Cross.Resources.ValidationMessagesResource.UserKeyWasUsed);

                    // pGEUsers.UserType = Infra.Cross.Utils.EUserType.Sallesman;
                }
                else if (OriginCreate == "CLIENT")
                {
                    pGEUsers.UserKey = Guid.NewGuid().ToString();
                    // pGEUsers.UserType = Infra.Cross.Utils.EUserType.Master;
                }

                pGEUsers.LastLogin = DateTime.Now;
                pGEUsers.Password = Metis.CryptPass(pGEUsers.Password);

                _IGEUsersRepository.Add(pGEUsers);
                await _IGEUsersRepository.SaveChanges();
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InsertSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { string.Concat(e.Message,
                                              (e?.InnerException?.InnerException?.Message) ?? "" ) }
                });
            }

            return pGEUsers;
        }

        public async Task<GEUsers> UpdateUser(GEUsers model)
        {
            var dbUser = _IGEUsersRepository.GetByIdToUpdate(
                model.Id,
                model.EstablishmentKey,
                model.UniqueKey);

            if (dbUser == null)
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.RegisterNotFound,
                    Field = "Id",
                    Value = model.Id.ToString(),
                    Messages = new string[] { Infra.Cross.Resources.ValidationMessagesResource.NotFound }
                });
                return null;
            }

            try
            {
                dbUser.UserType = model.UserType;
                dbUser.Name = model.Name;
                dbUser.PercDiscount = model.PercDiscount;
                dbUser.PercCommission = model.PercCommission;
                dbUser.PercDiscountPrice = model.PercDiscountPrice;
                dbUser.PercIncreasePrice = model.PercIncreasePrice;
                dbUser.StoreId = model.StoreId;
                dbUser.TerminalId = model.TerminalId;
                dbUser.Phone = model.Phone;
                dbUser.IsActive = model.IsActive;
                dbUser.IsBlockedByPay = model.IsBlockedByPay;
                dbUser.BoletoLimit = model.BoletoLimit;
                if (!model.Password.IsEmpty() && model.Password != "no-pass")
                    dbUser.Password = Metis.CryptPass(model.Password);

                _IGEUsersRepository.Update(dbUser);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InsertSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.InnerException?.Message  }
                });
            }

            await _IGEUsersRepository.SaveChanges();

            return model;
        }

        public async Task<GEUsers> ChangePassword(GEUsers model)
        {
            var dbUser = _IGEUsersRepository.GetByIdToUpdate(
                model.Id,
                model.EstablishmentKey,
                model.UniqueKey);
            dbUser.Password = Metis.CryptPass(model.Password);
            model.Password = "";
            _IGEUsersRepository.Update(dbUser);
            await _IGEUsersRepository.SaveChanges();
            return model;
        }
    }
}
