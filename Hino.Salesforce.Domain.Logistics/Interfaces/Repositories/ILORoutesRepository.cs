using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Logistics;

namespace Hino.Salesforce.Domain.Logistics.Interfaces.Repositories
{
    public interface ILORoutesRepository : IBaseRepository<LORoutes>
    {
    }
}
