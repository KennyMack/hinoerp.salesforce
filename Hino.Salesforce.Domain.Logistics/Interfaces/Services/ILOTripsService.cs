using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Logistics;

namespace Hino.Salesforce.Domain.Logistics.Interfaces.Services
{
    public interface ILOTripsService : IBaseService<LOTrips>
    {
    }
}
