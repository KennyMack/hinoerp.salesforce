using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.Logistics.Interfaces.Repositories;
using Hino.Salesforce.Domain.Logistics.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Logistics;

namespace Hino.Salesforce.Domain.Logistics.Services
{
    public class LORoutesService : BaseService<LORoutes>, ILORoutesService
    {
        private readonly ILORoutesRepository _ILORoutesRepository;

        public LORoutesService(ILORoutesRepository pILORoutesRepository) :
             base(pILORoutesRepository)
        {
            _ILORoutesRepository = pILORoutesRepository;
        }
    }
}
