using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.Logistics.Interfaces.Repositories;
using Hino.Salesforce.Domain.Logistics.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Logistics;

namespace Hino.Salesforce.Domain.Logistics.Services
{
    public class LOTripsService : BaseService<LOTrips>, ILOTripsService
    {
        private readonly ILOTripsRepository _ILOTripsRepository;

        public LOTripsService(ILOTripsRepository pILOTripsRepository) :
             base(pILOTripsRepository)
        {
            _ILOTripsRepository = pILOTripsRepository;
        }
    }
}
