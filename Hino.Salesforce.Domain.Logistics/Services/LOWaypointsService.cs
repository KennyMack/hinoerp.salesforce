using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.Logistics.Interfaces.Repositories;
using Hino.Salesforce.Domain.Logistics.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Logistics;

namespace Hino.Salesforce.Domain.Logistics.Services
{
    public class LOWaypointsService : BaseService<LOWaypoints>, ILOWaypointsService
    {
        private readonly ILOWaypointsRepository _ILOWaypointsRepository;

        public LOWaypointsService(ILOWaypointsRepository pILOWaypointsRepository) :
             base(pILOWaypointsRepository)
        {
            _ILOWaypointsRepository = pILOWaypointsRepository;
        }
    }
}
