using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using System.Collections.Generic;

namespace Hino.Salesforce.Domain.Sales.Interfaces.Repositories
{
    public interface IVEOrderItemsRepository : IBaseRepository<VEOrderItems>
    {
        IEnumerable<VEOrderItems> GetItemsByOrderId(long OrderId, string pEstablishmentKey);
    }
}
