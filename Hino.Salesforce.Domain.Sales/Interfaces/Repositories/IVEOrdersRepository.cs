using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Sales.Interfaces.Repositories
{
    public interface IVEOrdersRepository : IBaseRepository<VEOrders>
    {
        Task<PagedResult<VEOrders>> QueryViewAsync(int page, int pageSize,
            Expression<Func<VEOrders, bool>> predicate);
    }
}
