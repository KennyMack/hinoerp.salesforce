﻿using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Sales;

namespace Hino.Salesforce.Domain.Sales.Interfaces.Repositories
{
    public interface IVERegionSaleRepository : IBaseRepository<VERegionSale>
    {
    }
}
