﻿using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Sales.Interfaces.Repositories
{
    public interface IVESalePriceRepository : IBaseRepository<VESalePrice>
    {
        Task<PagedResult<VESalePrice>> GetAllPagedSearchProductWithStock(int pageNumber, int pageSize, string pEstablishmentKey, long pRegionId, long pCodPrVenda, string filter, string searchPosition);
        Task<PagedResult<VESalePrice>> GetAllPagedSearchProduct(int pageNumber, int pageSize, string pEstablishmentKey, long pRegionId, long pCodPrVenda, string filter, string searchPosition);
        Task<IList<VESalePrice>> GetListCodPRVendaAsync(string pEstablishmentKey);
    }
}
