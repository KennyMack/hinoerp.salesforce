using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Domain.Base.Interfaces.Repositories;

namespace Hino.Salesforce.Domain.Sales.Interfaces.Repositories
{
    public interface IVETypeSaleRepository : IBaseRepository<VETypeSale>
    {
    }
}
