using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Sales;

namespace Hino.Salesforce.Domain.Sales.Interfaces.Repositories
{
    public interface IVEUserRegionRepository : IBaseRepository<VEUserRegion>
    {
        bool ZipCodeExists(string pEstablishmentKey, long pUserId, string pZipCode);
    }
}
