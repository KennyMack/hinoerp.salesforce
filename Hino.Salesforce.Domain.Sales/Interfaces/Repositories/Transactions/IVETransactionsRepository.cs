using Hino.Salesforce.Domain.Base.Interfaces.Repositories;
using Hino.Salesforce.Infra.Cross.Entities.Sales.Transactions;

namespace Hino.Salesforce.Domain.Sales.Interfaces.Repositories.Transaction
{
    public interface IVETransactionsRepository : IBaseRepository<VETransactions>
    {
    }
}
