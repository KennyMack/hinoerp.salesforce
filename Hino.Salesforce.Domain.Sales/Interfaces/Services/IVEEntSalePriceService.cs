using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Domain.Base.Interfaces.Services;

namespace Hino.Salesforce.Domain.Sales.Interfaces.Services
{
    public interface IVEEntSalePriceService : IBaseService<VEEntSalePrice>
    {
    }
}
