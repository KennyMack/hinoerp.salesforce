using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Sales.Interfaces.Services
{
    public interface IVEOrderItemsService : IBaseService<VEOrderItems>
    {
        IEnumerable<VEOrderItems> GetItemsByOrderId(long OrderId, string pEstablishmentKey);
        Task<bool> RemoveAllItemsAsync(long pId, string pEstablishmentKey);
    }
}
