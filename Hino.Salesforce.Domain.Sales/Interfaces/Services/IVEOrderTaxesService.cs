using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Sales.Interfaces.Services
{
    public interface IVEOrderTaxesService : IBaseService<VEOrderTaxes>
    {
        Task ClearOrderTaxesAsync(string EstablishmentKey, long pOrderId);
    }
}
