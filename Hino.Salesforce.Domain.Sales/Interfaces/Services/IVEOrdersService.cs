using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Sales.Interfaces.Services
{
    public interface IVEOrdersService : IBaseService<VEOrders>
    {
        VEOrders CreateOrder(VEOrders pOrder);
        EStatusSinc ConvertStatusSinc(int pStatus);
        EFreightPaidBy ConvertFreightPaidBy(int pStatus);
        Task<PagedResult<VEOrders>> QueryViewAsync(int page, int pageSize, Expression<Func<VEOrders, bool>> predicate);
    }
}
