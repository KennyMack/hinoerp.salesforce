﻿using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Sales.Interfaces.Services
{
    public interface IVESalePriceService : IBaseService<VESalePrice>
    {
        Task<bool> ExistsProductRegion(string pEstablishmentKey, long pRegionId, long pProductId);
        Task CreateProductToAllRegionAsync(string pEstablishmentKey, GEProducts pProduct);
        Task<VESalePrice> CreateSalePrice(VESalePrice pSalePrice);
        Task<VESalePrice> UpdateSalePrice(VESalePrice pSalePrice);
        Task<IList<VESalePrice>> GetListCodPRVendaAsync(string pEstablishmentKey);
        Task<PagedResult<VESalePrice>> GetAllPagedSearchProductWithStock(int page, int pageSize, string pEstablishmentKey, long pRegionId, long pCodPrVenda, string filter, string searchPosition);
        Task<PagedResult<VESalePrice>> GetAllPagedSearchProduct(int page, int pageSize, string pEstablishmentKey, long pRegionId, long pCodPrVenda, string filter, string searchPosition);
    }
}
