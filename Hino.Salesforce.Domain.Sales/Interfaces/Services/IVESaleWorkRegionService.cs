using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;

namespace Hino.Salesforce.Domain.Sales.Interfaces.Services
{
    public interface IVESaleWorkRegionService : IBaseService<VESaleWorkRegion>
    {
    }
}
