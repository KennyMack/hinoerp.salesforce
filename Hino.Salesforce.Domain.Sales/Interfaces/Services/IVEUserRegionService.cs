using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;

namespace Hino.Salesforce.Domain.Sales.Interfaces.Services
{
    public interface IVEUserRegionService : IBaseService<VEUserRegion>
    {
        bool ZipCodeExists(string pEstablishmentKey, long pUserId, string pZipCode);
    }
}
