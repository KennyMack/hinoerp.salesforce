using Hino.Salesforce.Domain.Base.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales.Transactions;

namespace Hino.Salesforce.Domain.Sales.Interfaces.Services.Transaction
{
    public interface IVETransactionsService : IBaseService<VETransactions>
    {
    }
}
