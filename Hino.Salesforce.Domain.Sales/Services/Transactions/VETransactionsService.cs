using Hino.Salesforce.Domain.Base.Exceptions;
using Hino.Salesforce.Domain.Base.Messages;
using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.Sales.Interfaces.Repositories.Transaction;
using Hino.Salesforce.Domain.Sales.Interfaces.Services.Transaction;
using Hino.Salesforce.Infra.Cross.Entities.Sales.Transactions;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using System;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Sales.Services.Transaction
{
    public class VETransactionsService : BaseService<VETransactions>, IVETransactionsService
    {
        private readonly IVETransactionsRepository _IVETransactionsRepository;

        public VETransactionsService(IVETransactionsRepository pIVETransactionsRepository) :
             base(pIVETransactionsRepository)
        {
            _IVETransactionsRepository = pIVETransactionsRepository;
        }

        public override async Task<int> SaveChanges()
        {
            try
            {
                var ret = await DataRepository.SaveChanges();

                return ret;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                if (e is IModelEntityValidationException exception)
                    Errors = exception.MException;
                else
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.SaveSQLError,
                        Field = e.HelpLink,
                        Value = e.Source,
                        Messages = new string[] { e.Message,
                                              e?.InnerException?.Message,
                                              e?.InnerException?.InnerException?.Message }
                    });
                }
            }
            return -1;
        }

        public async override Task GenerateEntryQueueAsync()
        {
            if (!DontSendToQueue)
            {
                var queue = new MessageService<VETransactions>();
                foreach (var item in AddedOrUpdatedItems)
                    queue.SendCreatedOrUpdatedQueue(item, $"Orders", $"Orders");

                foreach (var item in RemovedItems)
                    queue.SendRemovedQueue(item, $"Orders", $"Orders");
            }

            await base.GenerateEntryQueueAsync();

            AddedOrUpdatedItems.Clear();
            RemovedItems.Clear();
        }
    }
}
