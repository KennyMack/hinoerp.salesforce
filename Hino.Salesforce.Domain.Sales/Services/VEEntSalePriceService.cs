using Hino.Salesforce.Domain.Sales.Interfaces.Repositories;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Domain.Base.Services;

namespace Hino.Salesforce.Domain.Sales.Services
{
    public class VEEntSalePriceService : BaseService<VEEntSalePrice>, IVEEntSalePriceService
    {
        private readonly IVEEntSalePriceRepository _IVEEntSalePriceRepository;

        public VEEntSalePriceService(IVEEntSalePriceRepository pIVEEntSalePriceRepository) : 
             base(pIVEEntSalePriceRepository)
        {
            _IVEEntSalePriceRepository = pIVEEntSalePriceRepository;
        }
    }
}
