using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.Sales.Interfaces.Repositories;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Sales.Services
{
    public class VEOrderItemsService : BaseService<VEOrderItems>, IVEOrderItemsService
    {
        private readonly IVEOrderItemsRepository _IVEOrderItemsRepository;

        public VEOrderItemsService(IVEOrderItemsRepository pIVEOrderItemsRepository) :
             base(pIVEOrderItemsRepository)
        {
            _IVEOrderItemsRepository = pIVEOrderItemsRepository;
        }

        public IEnumerable<VEOrderItems> GetItemsByOrderId(long OrderId, string pEstablishmentKey) =>
            _IVEOrderItemsRepository.GetItemsByOrderId(OrderId, pEstablishmentKey);

        public async Task<bool> RemoveAllItemsAsync(long pId, string pEstablishmentKey)
        {
            var Items = await _IVEOrderItemsRepository.QueryAsync(
                r => r.OrderID == pId &&
                     r.EstablishmentKey == pEstablishmentKey);


            foreach (VEOrderItems item in Items)
                await _IVEOrderItemsRepository.RemoveById(item.Id, item.EstablishmentKey, item.UniqueKey);

            return true;
        }
    }
}
