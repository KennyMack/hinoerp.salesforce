using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.Sales.Interfaces.Repositories;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Sales.Services
{
    public class VEOrderTaxesService : BaseService<VEOrderTaxes>, IVEOrderTaxesService
    {
        private readonly IVEOrderTaxesRepository _IVEOrderTaxesRepository;

        public VEOrderTaxesService(IVEOrderTaxesRepository pIVEOrderTaxesRepository) :
             base(pIVEOrderTaxesRepository)
        {
            _IVEOrderTaxesRepository = pIVEOrderTaxesRepository;
        }

        public async Task ClearOrderTaxesAsync(string pEstablishmentKey, long pOrderId)
        {
            var Taxes = await _IVEOrderTaxesRepository.QueryAsync(r =>
                r.VEOrderItems.OrderID == pOrderId &&
                r.EstablishmentKey == pEstablishmentKey);

            foreach (var tax in Taxes)
                await _IVEOrderTaxesRepository.RemoveById(tax.Id, tax.EstablishmentKey, tax.UniqueKey);
        }
    }
}
