using Hino.Salesforce.Domain.Base.Exceptions;
using Hino.Salesforce.Domain.Base.Messages;
using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.Sales.Interfaces.Repositories;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Sales.Services
{
    public class VEOrdersService : BaseService<VEOrders>, IVEOrdersService
    {
        private readonly IVEOrdersRepository _IVEOrdersRepository;

        public VEOrdersService(IVEOrdersRepository pIVEOrdersRepository) :
             base(pIVEOrdersRepository)
        {
            _IVEOrdersRepository = pIVEOrdersRepository;
        }

        public EFreightPaidBy ConvertFreightPaidBy(int pStatus)
        {
            switch (pStatus)
            {
                case 0:
                    return EFreightPaidBy.Emitente;
                case 1:
                    return EFreightPaidBy.Destinatario;
                case 2:
                    return EFreightPaidBy.Terceiros;
                case 3:
                    return EFreightPaidBy.ProprioEmitente;
                case 4:
                    return EFreightPaidBy.ProprioDestinatario;
                case 9:
                    return EFreightPaidBy.SemFrete;
                default:
                    return EFreightPaidBy.SemFrete;
            }
        }

        public EStatusSinc ConvertStatusSinc(int pStatus)
        {
            switch (pStatus)
            {
                case 0:
                    return EStatusSinc.Waiting;
                case 1:
                    return EStatusSinc.Sincronized;
                case 2:
                    return EStatusSinc.Integrated;
                default:
                    return EStatusSinc.Waiting;
            }
        }

        public VEOrders CreateOrder(VEOrders pOrder)
        {
            try
            {
                _IVEOrdersRepository.Add(pOrder);
                AddedOrUpdatedItems.Add(pOrder);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InsertSQLError,
                    Field = e.HelpLink,
                    Value = e.Source,
                    Messages = new string[] { e.Message,
                                              e?.InnerException?.InnerException?.Message  }
                });
            }

            return pOrder;
        }

        public override async Task<int> SaveChanges()
        {
            try
            {
                var ret = await DataRepository.SaveChanges();

                return ret;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                if (e is IModelEntityValidationException exception)
                    Errors = exception.MException;
                else
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.SaveSQLError,
                        Field = e.HelpLink,
                        Value = e.Source,
                        Messages = new string[] { e.Message,
                                              e?.InnerException?.Message,
                                              e?.InnerException?.InnerException?.Message }
                    });
                }
            }
            return -1;
        }

        public async override Task GenerateEntryQueueAsync()
        {
            if (!DontSendToQueue)
            {
                var queue = new MessageService<VEOrders>();
                foreach (var item in AddedOrUpdatedItems)
                    queue.SendCreatedOrUpdatedQueue(item, $"Orders", $"Orders");

                foreach (var item in RemovedItems)
                    queue.SendRemovedQueue(item, $"Orders", $"Orders");
            }

            await base.GenerateEntryQueueAsync();

            AddedOrUpdatedItems.Clear();
            RemovedItems.Clear();
        }

        public async Task<PagedResult<VEOrders>> QueryViewAsync(int page, int pageSize, Expression<Func<VEOrders, bool>> predicate) =>
            await _IVEOrdersRepository.QueryViewAsync(page, pageSize, predicate);
    }
}
