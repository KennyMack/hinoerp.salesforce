﻿using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.Sales.Interfaces.Repositories;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;

namespace Hino.Salesforce.Domain.Sales.Services
{
    public class VERegionSaleService : BaseService<VERegionSale>, IVERegionSaleService
    {
        public VERegionSaleService(IVERegionSaleRepository repo) : base(repo)
        {
        }
    }
}
