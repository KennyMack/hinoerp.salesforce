using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.Sales.Interfaces.Repositories;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;

namespace Hino.Salesforce.Domain.Sales.Services
{
    public class VERegionSaleUFService : BaseService<VERegionSaleUF>, IVERegionSaleUFService
    {
        private readonly IVERegionSaleUFRepository _IVERegionSaleUFRepository;

        public VERegionSaleUFService(IVERegionSaleUFRepository pIVERegionSaleUFRepository) :
             base(pIVERegionSaleUFRepository)
        {
            _IVERegionSaleUFRepository = pIVERegionSaleUFRepository;
        }
    }
}
