﻿using Hino.Salesforce.Domain.Base.Exceptions;
using Hino.Salesforce.Domain.Base.Messages;
using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.Sales.Interfaces.Repositories;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Resources;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Domain.Sales.Services
{
    public class VESalePriceService : BaseService<VESalePrice>, IVESalePriceService
    {
        private readonly IVESalePriceRepository _veSalePriceRepository;
        private readonly IVERegionSaleRepository _IVERegionSaleRepository;

        public VESalePriceService(IVESalePriceRepository repo,
            IVERegionSaleRepository pIVERegionSaleRepository) : base(repo)
        {
            _veSalePriceRepository = repo;
            _IVERegionSaleRepository = pIVERegionSaleRepository;
        }

        public Task<PagedResult<VESalePrice>> GetAllPagedSearchProductWithStock(int page, int pageSize, string pEstablishmentKey, long pRegionId, long pCodPrVenda, string filter, string searchPosition)
        {
            return _veSalePriceRepository.GetAllPagedSearchProductWithStock(page, pageSize, pEstablishmentKey, pRegionId, pCodPrVenda, filter, searchPosition);
        }

        public Task<PagedResult<VESalePrice>> GetAllPagedSearchProduct(int page, int pageSize, string pEstablishmentKey, long pRegionId, long pCodPrVenda, string filter, string searchPosition)
        {
            return _veSalePriceRepository.GetAllPagedSearchProduct(page, pageSize, pEstablishmentKey, pRegionId, pCodPrVenda, filter, searchPosition);
        }

        public override async Task<int> SaveChanges()
        {
            try
            {
                var ret = await DataRepository.SaveChanges();

                return ret;
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                if (e is IModelEntityValidationException exception)
                    Errors = exception.MException;
                else
                {
                    Errors.Add(new ModelException
                    {
                        ErrorCode = (int)EExceptionErrorCodes.SaveSQLError,
                        Field = e.HelpLink,
                        Value = e.Source,
                        Messages = new string[] { e.Message,
                                              e?.InnerException?.Message,
                                              e?.InnerException?.InnerException?.Message }
                    });
                }
            }
            return -1;
        }

        public async override Task GenerateEntryQueueAsync()
        {
            if (!DontSendToQueue)
            {
                var queue = new MessageService<VESalePrice>();
                foreach (var item in AddedOrUpdatedItems)
                    queue.SendCreatedOrUpdatedQueue(item, $"SalePrice", $"SalePrice");

                foreach (var item in RemovedItems)
                    queue.SendRemovedQueue(item, $"SalePrice", $"SalePrice");
            }

            await base.GenerateEntryQueueAsync();

            AddedOrUpdatedItems.Clear();
            RemovedItems.Clear();
        }

        public async Task<bool> ExistsProductRegion(string pEstablishmentKey, long pRegionId, long pProductId)
        {
            var Result = await QueryAsync(r =>
                r.EstablishmentKey == pEstablishmentKey &&
                r.ProductId == pProductId &&
                r.RegionId == pRegionId);
            return Result.Any();
        }

        async Task<IEnumerable<VESalePrice>> OthersSalePriceAsync(string pEstablishmentKey, long pRegionId)
        {
            var Result = await QueryAsync(r =>
                r.EstablishmentKey == pEstablishmentKey &&
                r.RegionId == pRegionId &&
                r.CodPrVenda > 0);
            return Result;
        }

        public async Task<VESalePrice> CreateSalePrice(VESalePrice pSalePrice)
        {
            if (await ExistsProductRegion(
                pSalePrice.EstablishmentKey,
                pSalePrice.RegionId,
                pSalePrice.ProductId))
            {
                Errors.Add(new ModelException
                {
                    ErrorCode = (int)EExceptionErrorCodes.InvalidRequest,
                    Field = "ProductId",
                    Value = pSalePrice.ProductId.ToString(),
                    Messages = new string[]
                    {
                        ValidationMessagesResource.ProductExistsRegion
                    }
                });
                return null;
            }

            if (pSalePrice.CodPrVenda <= 0)
            {
                var CodPrVenda = (await OthersSalePriceAsync(
                    pSalePrice.EstablishmentKey,
                    pSalePrice.RegionId)).Max(r => r.CodPrVenda);
                if (CodPrVenda > 0)
                    pSalePrice.CodPrVenda = CodPrVenda;
            }

            return Add(pSalePrice);
        }

        public async Task<VESalePrice> UpdateSalePrice(VESalePrice pSalePrice)
        {
            if (pSalePrice.CodPrVenda <= 0)
            {
                var CodPrVenda = (await OthersSalePriceAsync(
                    pSalePrice.EstablishmentKey,
                    pSalePrice.RegionId)).Max(r => r.CodPrVenda);
                if (CodPrVenda > 0)
                    pSalePrice.CodPrVenda = CodPrVenda;
            }

            var Result = Update(pSalePrice);

            if (!Errors.Any())
                await SaveChanges();

            return Result;
        }

        public async Task CreateProductToAllRegionAsync(string pEstablishmentKey, GEProducts pProduct)
        {
            var Regions = await _IVERegionSaleRepository.QueryAsync(r => r.EstablishmentKey == pEstablishmentKey);
            var ListPrVenda = await _veSalePriceRepository.GetListCodPRVendaAsync(pEstablishmentKey);
            if (!ListPrVenda.Any())
                ListPrVenda.Add(new VESalePrice()
                {
                    Description = "GERAL",
                    CodPrVenda = 0
                });

            foreach (var prVenda in ListPrVenda)
            {
                foreach (var region in Regions)
                {
                    await CreateSalePrice(new VESalePrice
                    {
                        ProductId = pProduct.Id,
                        CodPrVenda = prVenda.CodPrVenda,
                        RegionId = region.Id,
                        Description = prVenda.Description,
                        EstablishmentKey = pEstablishmentKey,
                        UniqueKey = pEstablishmentKey,
                        Id = 0,
                        Value = 0.01M
                    });
                    await SaveChanges();
                }
            }

        }

        public async Task<IList<VESalePrice>> GetListCodPRVendaAsync(string pEstablishmentKey) =>
            await _veSalePriceRepository.GetListCodPRVendaAsync(pEstablishmentKey);
    }
}
