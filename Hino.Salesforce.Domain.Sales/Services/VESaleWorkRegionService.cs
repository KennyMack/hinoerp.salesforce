using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.Sales.Interfaces.Repositories;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;

namespace Hino.Salesforce.Domain.Sales.Services
{
    public class VESaleWorkRegionService : BaseService<VESaleWorkRegion>, IVESaleWorkRegionService
    {
        private readonly IVESaleWorkRegionRepository _IVESaleWorkRegionRepository;

        public VESaleWorkRegionService(IVESaleWorkRegionRepository pIVESaleWorkRegionRepository) :
             base(pIVESaleWorkRegionRepository)
        {
            _IVESaleWorkRegionRepository = pIVESaleWorkRegionRepository;
        }
    }
}
