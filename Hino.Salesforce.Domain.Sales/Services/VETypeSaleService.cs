using Hino.Salesforce.Domain.Sales.Interfaces.Repositories;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Domain.Base.Services;

namespace Hino.Salesforce.Domain.Sales.Services
{
    public class VETypeSaleService : BaseService<VETypeSale>, IVETypeSaleService
    {
        private readonly IVETypeSaleRepository _IVETypeSaleRepository;

        public VETypeSaleService(IVETypeSaleRepository pIVETypeSaleRepository) : 
             base(pIVETypeSaleRepository)
        {
            _IVETypeSaleRepository = pIVETypeSaleRepository;
        }
    }
}
