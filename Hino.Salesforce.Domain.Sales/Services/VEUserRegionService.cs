using Hino.Salesforce.Domain.Base.Services;
using Hino.Salesforce.Domain.Sales.Interfaces.Repositories;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Infra.Cross.Entities.Sales;

namespace Hino.Salesforce.Domain.Sales.Services
{
    public class VEUserRegionService : BaseService<VEUserRegion>, IVEUserRegionService
    {
        private readonly IVEUserRegionRepository _IVEUserRegionRepository;

        public VEUserRegionService(IVEUserRegionRepository pIVEUserRegionRepository) :
             base(pIVEUserRegionRepository)
        {
            _IVEUserRegionRepository = pIVEUserRegionRepository;
        }

        public bool ZipCodeExists(string pEstablishmentKey, long pUserId, string pZipCode) =>
            _IVEUserRegionRepository.ZipCodeExists(pEstablishmentKey, pUserId, pZipCode);
    }
}
