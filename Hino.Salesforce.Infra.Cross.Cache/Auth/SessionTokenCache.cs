﻿using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using ServiceStack;
using ServiceStack.Redis;
using StackExchange.Redis;
using System;

namespace Hino.Salesforce.Infra.Cross.Cache.Auth
{
    public class SessionTokenCache : IDisposable
    {
        RedisEndpoint EndPoint;
        private Lazy<ConnectionMultiplexer> lazyConnection;


        public ConnectionMultiplexer Connection
        {
            get
            {
                return lazyConnection?.Value;
            }
        }

        public SessionTokenCache(RedisConfigurationSection pRedisCfg)
        {
            lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
            {
                return ConnectionMultiplexer.Connect(
                    $"{pRedisCfg.Host},abortConnect=false,ssl=true,password={pRedisCfg.Password}");
            });
            EndPoint = new RedisEndpoint(pRedisCfg.Host,
                pRedisCfg.Port,
                pRedisCfg.Password,
                pRedisCfg.DatabaseID);
        }

        public T Get<T>(string key)
        {
            T result = default;

            IDatabase cache = Connection.GetDatabase();

            var value = cache.StringGet("AUTH:" + key);
            try
            {
                result = (T)JSON.parse(value);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
            }

            /*using (RedisClient client = new RedisClient(EndPoint.ToString()))
            {
                var wrapper = client.As<T>();

                result = wrapper.GetValue("AUTH:" + key);
            }*/

            return result;
        }

        public bool IsInCache(string key)
        {
            bool isInCache = false;

            using (RedisClient client = new RedisClient(EndPoint))
                isInCache = client.ContainsKey("AUTH:" + key);

            return isInCache;
        }

        public bool Remove(string key)
        {
            bool removed = false;

            IDatabase cache = Connection.GetDatabase();

            cache.KeyDelete("AUTH:" + key);
            /*using (RedisClient client = new RedisClient(EndPoint))
                removed = client.Remove("AUTH:" + key);*/

            return removed;
        }

        public void Set<T>(string key, T value)
        {
            Set(key, value, TimeSpan.Zero);
        }

        public void Set<T>(string key, T value, TimeSpan timeout)
        {
            IDatabase cache = Connection.GetDatabase();

            cache.StringSet("AUTH:" + key, JSON.stringify(value), timeout);
            /*using (RedisClient client = new RedisClient(EndPoint))
                client.As<T>().SetValue("AUTH:" + key, value, timeout);*/
        }

        public void SetTime(string Key, TimeSpan timeout)
        {
            IDatabase cache = Connection.GetDatabase();

            cache.KeyExpire("AUTH:" + Key, timeout);
            // using (RedisClient client = new RedisClient(EndPoint))
            //     client.ExpireEntryIn("AUTH:" + Key, timeout);
        }

        public void Dispose()
        {
            EndPoint = null;
            GC.SuppressFinalize(this);
        }

    }
}
