﻿using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Newtonsoft.Json;
using ServiceStack;
using ServiceStack.Redis;
using StackExchange.Redis;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.Cross.Cache.Establishment
{
    public class EstablishmentInfoCache : IDisposable
    {
        RedisEndpoint EndPoint;
        private Lazy<ConnectionMultiplexer> lazyConnection;
        protected JsonSerializerSettings _JsonSerializerSettings = new JsonSerializerSettings
        {
            Culture = new System.Globalization.CultureInfo("pt-BR"),
            Formatting = Formatting.Indented,
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore
        };

        public ConnectionMultiplexer Connection
        {
            get
            {
                return lazyConnection?.Value;
            }
        }

        public EstablishmentInfoCache(RedisConfigurationSection pRedisCfg)
        {
            lazyConnection = new Lazy<ConnectionMultiplexer>(() =>
            {
                return ConnectionMultiplexer.Connect(
                    $"{pRedisCfg.Host},abortConnect=false,ssl=true,password={pRedisCfg.Password}");
            });
            EndPoint = new RedisEndpoint(pRedisCfg.Host,
                pRedisCfg.Port,
                pRedisCfg.Password,
                2);
        }

        public T Get<T>(string key)
        {
            T result = default;

            IDatabase cache = Connection.GetDatabase();

            var value = cache.StringGet("ESTAB:" + key);
            try
            {
                result = JsonConvert.DeserializeObject<T>(value, _JsonSerializerSettings);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
            }

            return result;
        }

        public bool IsInCache(string key)
        {
            bool isInCache = false;

            using (RedisClient client = new RedisClient(EndPoint))
                isInCache = client.ContainsKey("ESTAB:" + key);

            return isInCache;
        }

        public bool Remove(string key)
        {
            bool removed = false;

            // IDatabase cache = Connection.GetDatabase();

            SetTime(key, TimeSpan.FromMilliseconds(2));

            return removed;
        }

        public void Set<T>(string key, T value)
        {
            Set(key, value, TimeSpan.Zero);
        }

        public void Set<T>(string key, T value, TimeSpan timeout)
        {
            IDatabase cache = Connection.GetDatabase();

            cache.StringSet("ESTAB:" + key, JsonConvert.SerializeObject(value, _JsonSerializerSettings), timeout);
        }

        public void SetTime(string Key, TimeSpan timeout)
        {
            IDatabase cache = Connection.GetDatabase();

            cache.KeyExpire("ESTAB:" + Key, timeout);
        }

        public void Dispose()
        {
            EndPoint = null;
            GC.SuppressFinalize(this);
        }

    }
}
