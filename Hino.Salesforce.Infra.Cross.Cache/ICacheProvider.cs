﻿using System;

namespace Hino.Salesforce.Infra.Cross.Cache.Auth
{
    public interface ICacheProvider
    {
        void Set<T>(string key, T value);

        void Set<T>(string key, T value, TimeSpan timeout);

        T Get<T>(string key);

        bool Remove(string key);

        bool IsInCache(string key);

        void SetTime(string Key, TimeSpan timeout);
    }
}
