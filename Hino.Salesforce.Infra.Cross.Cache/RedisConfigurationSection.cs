﻿using Hino.Salesforce.Infra.Cross.Utils.Settings;
using System;

namespace Hino.Salesforce.Infra.Cross.Cache
{
    public class RedisConfigurationSection
    {
        public string ConnectionString { get; private set; }
        public const string InstanceName = "HINOERP-";
        public string Host { get; private set; }
        public int Port { get; private set; }
        public string Password { get; private set; }
        public int DatabaseID { get; private set; }


        public RedisConfigurationSection()
        {
            Host = AppSettings.Redis_Host;
            Port = Convert.ToInt32(AppSettings.Redis_Port);
            Password = AppSettings.Redis_Password;
            DatabaseID = Convert.ToInt32(AppSettings.Redis_DatabaseID);
        }
    }
}
