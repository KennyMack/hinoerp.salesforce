﻿using Hino.Salesforce.Domain.Base.Interfaces.Email;
using System;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.Cross.Email
{
    public class BaseEmail<T> : IDisposable, IBaseEmail<T> where T : class
    {
        private readonly ISendEmail _ISendEmail;
        public BaseEmail(ISendEmail pISendEmail)
        {
            _ISendEmail = pISendEmail;
        }

        public void Dispose()
        {
            _ISendEmail.Dispose();
        }

        public virtual Task<bool> SendCreate(T model, string pFrom, string pTo)
        {
            throw new NotImplementedException();
        }

        public virtual Task<bool> SendRemove(T model, string pFrom, string pTo)
        {
            throw new NotImplementedException();
        }

        public virtual Task<bool> SendUpdate(T model, string pFrom, string pTo)
        {
            throw new NotImplementedException();
        }
    }
}
