﻿using Hino.Salesforce.Infra.Cross.Utils.Settings;
using System;
using System.Net;
using System.Net.Mail;

namespace Hino.Salesforce.Infra.Cross.Email.Configurations
{
    public class EmailConfiguration
    {
        public string LocalEmail { get; set; }
        public string SMTPHost { get; set; }
        public int SMTPPort { get; set; }
        public string SMTPUsername { get; set; }
        public string SMTPPassword { get; set; }
        public NetworkCredential Credential { get; private set; }
        public SmtpDeliveryMethod DeliveryMethod { get; private set; }
        public bool UseDefaultCredentials { get; private set; }
        public bool UseSSL { get; private set; }

        public EmailConfiguration()
        {
            LocalEmail = AppSettings.EMAIL_LocalEmail;
            SMTPHost = AppSettings.EMAIL_SMTPHost;
            SMTPPort = Convert.ToInt32(AppSettings.EMAIL_SMTPPort);
            SMTPUsername = AppSettings.EMAIL_SMTPUsername;
            SMTPPassword = AppSettings.EMAIL_SMTPPassword;
            UseSSL = Convert.ToBoolean(AppSettings.EMAIL_UseSSL);

            Credential = new NetworkCredential(SMTPUsername, SMTPPassword);
            UseDefaultCredentials = false;
            DeliveryMethod = SmtpDeliveryMethod.Network;
        }
    }
}
