﻿using Hino.Salesforce.Domain.Base.Interfaces.Email;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils;
using System;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;


namespace Hino.Salesforce.Infra.Cross.Email.General
{
    public class EstablishmentEmail : BaseEmail<GEEstablishments>, IEstablishmentEmail
    {
        private readonly ISendEmail _ISendEmail;

        public EstablishmentEmail(ISendEmail pISendEmail) :
            base(pISendEmail)
        {
            _ISendEmail = pISendEmail;
        }

        public override async Task<bool> SendCreate(GEEstablishments pGEEstablishments, string pFrom, string pTo)
        {
            try
            {
                var Content = GetFileContent.GetFile("Templates\\tplRegisterEstablishment.html");

                StringBuilder sbBody = new StringBuilder();

                foreach (var line in Content)
                {
                    if (line.IndexOf("[{ESTABLISHMENTKEY}]") > -1)
                        sbBody.Append(line.Replace("[{ESTABLISHMENTKEY}]", pGEEstablishments.EstablishmentKey));
                    else if (line.IndexOf("[{EMAIL}]") > -1)
                        sbBody.Append(line.Replace("[{EMAIL}]", pGEEstablishments.Email));
                    else
                        sbBody.Append(line);
                }


                _ISendEmail.From = new MailAddress(_ISendEmail.LocalEmail);
                _ISendEmail.To.Add(new MailAddress(pTo));

                using (var newMessage = new MailMessage())
                {
                    newMessage.Subject = "Seja bem vindo";

                    newMessage.Body = sbBody.ToString();
                    await _ISendEmail.Send("", newMessage);
                }
            }
            catch (Exception)
            {
                return false;
            }
            return true;
        }
    }
}
