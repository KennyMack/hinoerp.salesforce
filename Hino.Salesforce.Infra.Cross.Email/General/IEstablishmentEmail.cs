﻿using Hino.Salesforce.Domain.Base.Interfaces.Email;
using Hino.Salesforce.Infra.Cross.Entities.General;

namespace Hino.Salesforce.Infra.Cross.Email.General
{
    public interface IEstablishmentEmail : IBaseEmail<GEEstablishments>
    {
    }
}
