﻿using Hino.Salesforce.Domain.Base.Interfaces.Email;
using Hino.Salesforce.Infra.Cross.Email.Configurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.Cross.Email
{
    public class SendEmail : ISendEmail, IDisposable
    {
        private readonly EmailConfiguration _EmailConfiguration;
        public MailAddress From { get; set; }
        public MailAddressCollection To { get; set; }
        public List<Dictionary<string, string>> ReplaceTags { get; set; }
        public string LocalEmail { get; set; }

        public SendEmail(EmailConfiguration pEmailConfiguration)
        {
            _EmailConfiguration = pEmailConfiguration;
            LocalEmail = _EmailConfiguration.LocalEmail;
            To = new MailAddressCollection();
            ReplaceTags = new List<Dictionary<string, string>>();
        }

        public MailAddressCollection ConvertAddressTo(string pEmail)
        {
            pEmail = pEmail.Replace(" ", "");

            if (pEmail.Contains(','))
                pEmail.Replace(',', ';');

            pEmail.Split(';').ToList().ForEach(r => To.Add(r));

            return To;
        }

        public async Task<bool> Send(string pEstablishmentKey, MailMessage pMailMessage)
        {
            using (var smtp = new SmtpClient())
            {
                try
                {
                    smtp.DeliveryMethod = _EmailConfiguration.DeliveryMethod;
                    smtp.Credentials = _EmailConfiguration.Credential;
                    smtp.Port = _EmailConfiguration.SMTPPort;
                    smtp.Host = _EmailConfiguration.SMTPHost;
                    smtp.EnableSsl = _EmailConfiguration.UseSSL;

                    pMailMessage.Subject = string.IsNullOrEmpty(pMailMessage.Subject) ? "HINO" : pMailMessage.Subject;

                    pMailMessage.IsBodyHtml = true;
                    pMailMessage.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
                    pMailMessage.From = new MailAddress(From.Address, "Hino Sistemas");
                    pMailMessage.Sender = new MailAddress(From.Address, "Hino Sistemas");
                    pMailMessage.ReplyToList.Add(From);

                    foreach (var item in To)
                        pMailMessage.To.Add(item);

                    foreach (var item in ReplaceTags)
                        pMailMessage.Body.Replace(item.Keys.ToString(), item.Values.ToString());

                    await smtp.SendMailAsync(pMailMessage);
                    To.Clear();
                    ReplaceTags.Clear();
                }
                catch (Exception)
                {
                    To.Clear();
                    ReplaceTags.Clear();
                    return false;
                }

            }
            return true;
        }

        public void Dispose()
        {
            GC.SuppressFinalize(true);
        }
    }
}
