﻿
namespace Hino.Salesforce.Infra.Cross.Entities.Auth
{
    public class AUExpiredToken : BaseEntity
    {
        public string TokenValue { get; set; }
    }
}
