﻿using Hino.Salesforce.Infra.Cross.Entities.General;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.Auth
{
    public class AURefreshToken : BaseEntity
    {
        [ForeignKey("GEUsers")]
        public long UserID { get; set; }
        public virtual GEUsers GEUsers { get; set; }

        public string UserKey { get; set; }
        public string SessionID { get; set; }
        public string RefreshTokenID { get; set; }
        public string ProtectedTicket { get; set; }

    }
}
