﻿using Hino.Salesforce.Infra.Cross.Entities.Sales;
using System.Collections.Generic;

namespace Hino.Salesforce.Infra.Cross.Entities.Fiscal
{
    public class FSFiscalOper : BaseEntity
    {
        public FSFiscalOper()
        {
            this.VEOrderItems = new HashSet<VEOrderItems>();
            this.VEOrders = new HashSet<VEOrders>();
        }

        public string Description { get; set; }
        public long IdERP { get; set; }
        public bool IsBonification  { get; set; }
        public bool IsSample { get; set; }

        public virtual ICollection<VEOrders> VEOrders { get; set; }
        public virtual ICollection<VEOrderItems> VEOrderItems { get; set; }
    }
}
