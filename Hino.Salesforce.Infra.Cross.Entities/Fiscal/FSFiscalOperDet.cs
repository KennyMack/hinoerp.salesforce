namespace Hino.Salesforce.Infra.Cross.Entities.Fiscal
{
    public class FSFiscalOperDet : BaseEntity
    {
        public long fiscaloperid { get; set; }
        public long cfopid { get; set; }
    }
}
