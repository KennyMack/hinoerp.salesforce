namespace Hino.Salesforce.Infra.Cross.Entities.Fiscal
{
    public class FSTipoMov : BaseEntity
    {
        public string codtipo { get; set; }
        public string tipo { get; set; }
        public string classificacao { get; set; }
    }
}
