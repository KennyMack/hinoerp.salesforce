﻿using System;

namespace Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes
{
    public class FSAliqSimples : BaseEntity
    {
        public DateTime DtInicio { get; set; }
        public long CodAtividade { get; set; }
        public long CodAliqSimples { get; set; }
        public int CodAnexoSimples { get; set; }
    }
}
