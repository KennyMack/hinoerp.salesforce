using System;

namespace Hino.Salesforce.Infra.Cross.Entities.FiscalTaxes
{
    public class FSAliqSimplesEfet : BaseEntity
    {
        public long codaliqsimples { get; set; }
        public long codanexosimples { get; set; }
        public DateTime data { get; set; }
        public long codimposto { get; set; }
        public decimal aliquota { get; set; }
        public long AliqSimplesId { get; set; }
    }
}
