namespace Hino.Salesforce.Infra.Cross.Entities.FiscalTaxes
{
    public class FSAliqSimplesFaixaImp : BaseEntity
    {
        public long codaliqsimples { get; set; }
        public decimal valorinicial { get; set; }
        public decimal valorfinal { get; set; }
        public long? codimposto { get; set; }
        public decimal? aliquota { get; set; }
        public long AliqSimplesId { get; set; }
    }
}
