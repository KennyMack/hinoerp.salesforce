namespace Hino.Salesforce.Infra.Cross.Entities.FiscalTaxes
{
    public class FSAliqSimplesImp : BaseEntity
    {
        public long codaliqsimples { get; set; }
        public long codimposto { get; set; }
        public long AliqSimplesId { get; set; }
    }
}
