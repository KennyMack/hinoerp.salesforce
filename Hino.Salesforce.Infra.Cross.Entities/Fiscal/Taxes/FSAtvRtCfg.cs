using System;

namespace Hino.Salesforce.Infra.Cross.Entities.FiscalTaxes
{
    public class FSAtvRtCfg : BaseEntity
    {
        public long codatividade { get; set; }
        public long codregtrib { get; set; }
        public long codimposto { get; set; }
        public DateTime dtinicio { get; set; }
        public decimal aliqsaida { get; set; }
    }
}
