namespace Hino.Salesforce.Infra.Cross.Entities.FiscalTaxes
{
    public class FSCFOP : BaseEntity
    {
        public long codcfop { get; set; }
        public short cfopestadual { get; set; }
        public short cfopinterestadual { get; set; }
        public short cfopinternacional { get; set; }
        public long codaplicacao { get; set; }
        public long codtipo { get; set; }
        public bool status { get; set; }
        public bool ipiembbaseicms { get; set; }
        public bool trcat012019 { get; set; }
    }
}
