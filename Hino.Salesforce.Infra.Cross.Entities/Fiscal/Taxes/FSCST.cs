namespace Hino.Salesforce.Infra.Cross.Entities.FiscalTaxes
{
    public class FSCST : BaseEntity
    {
        public long codimposto { get; set; }
        public string codcst { get; set; }
        public decimal situacao { get; set; }
        public decimal tipooperacao { get; set; }
    }
}
