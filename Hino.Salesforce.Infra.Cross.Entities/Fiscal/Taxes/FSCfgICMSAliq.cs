namespace Hino.Salesforce.Infra.Cross.Entities.FiscalTaxes
{
    public class FSCfgICMSAliq : BaseEntity
    {
        public string uforigem { get; set; }
        public string ufdestino { get; set; }
        public decimal aliquota { get; set; }
    }
}
