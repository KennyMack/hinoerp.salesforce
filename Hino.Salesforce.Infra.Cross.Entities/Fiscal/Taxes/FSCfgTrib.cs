using System;

namespace Hino.Salesforce.Infra.Cross.Entities.FiscalTaxes
{
    public class FSCfgTrib : BaseEntity
    {
        public DateTime dtiniciovig { get; set; }
        public long fiscaloperid { get; set; }
        public string ncm { get; set; }
        public string codserv { get; set; }
        public bool todosncm { get; set; }
        public long? codcfgtrib { get; set; }
    }
}
