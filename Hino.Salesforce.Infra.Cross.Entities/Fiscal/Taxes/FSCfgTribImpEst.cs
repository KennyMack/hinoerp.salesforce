namespace Hino.Salesforce.Infra.Cross.Entities.FiscalTaxes
{
    public class FSCfgTribImpEst : BaseEntity
    {
        public long fiscfgid { get; set; }
        public long codimposto { get; set; }
        public string uforigem { get; set; }
        public string ufdestino { get; set; }
        public string codcst { get; set; }
    }
}
