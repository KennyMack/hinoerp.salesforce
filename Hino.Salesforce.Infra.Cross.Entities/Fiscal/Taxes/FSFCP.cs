namespace Hino.Salesforce.Infra.Cross.Entities.FiscalTaxes
{
    public class FSFCP : BaseEntity
    {
        public string uf { get; set; }
        public decimal aliqbase { get; set; }
    }
}
