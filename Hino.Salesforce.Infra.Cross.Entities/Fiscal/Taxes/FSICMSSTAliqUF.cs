﻿
namespace Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes
{
    public class FSICMSSTAliqUF : BaseEntity
    {
        public string UF { get; set; }
        public string NCM { get; set; }
        public decimal ALIQICMSST { get; set; }
        public decimal ALIQICMS { get; set; }
        public decimal MVA { get; set; }
    }
}
