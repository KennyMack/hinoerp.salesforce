namespace Hino.Salesforce.Infra.Cross.Entities.FiscalTaxes
{
    public class FSImpostos : BaseEntity
    {
        public long codimposto { get; set; }
        public short classificacao { get; set; }
        public bool status { get; set; }
    }
}
