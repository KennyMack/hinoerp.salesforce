using System;

namespace Hino.Salesforce.Infra.Cross.Entities.FiscalTaxes
{
    public class FSLegTrib : BaseEntity
    {
        public long codlegtrib { get; set; }
        public long codimposto { get; set; }
        public bool sublimite { get; set; }
        public DateTime dtinicio { get; set; }
        public DateTime? dtfim { get; set; }
        public bool zonafranca { get; set; }
        public bool todasorigmerc { get; set; }
        public bool todosgrpfisemp { get; set; }
        public bool todosncm { get; set; }
        public short aliquota { get; set; }
        public int ivamva { get; set; }
        public long reducao { get; set; }
        public string codcst { get; set; }
        public string codenqipi { get; set; }
        public bool adcfcpst { get; set; }
    }
}
