namespace Hino.Salesforce.Infra.Cross.Entities.FiscalTaxes
{
    public class FSLegTribCFOP : BaseEntity
    {
        public long legid { get; set; }
        public long cfopid { get; set; }
    }
}
