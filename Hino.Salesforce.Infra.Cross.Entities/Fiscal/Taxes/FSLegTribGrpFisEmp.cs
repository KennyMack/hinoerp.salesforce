namespace Hino.Salesforce.Infra.Cross.Entities.FiscalTaxes
{
    public class FSLegTribGrpFisEmp : BaseEntity
    {
        public long legid { get; set; }
        public long fiscalgroupid { get; set; }
    }
}
