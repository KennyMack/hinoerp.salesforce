namespace Hino.Salesforce.Infra.Cross.Entities.FiscalTaxes
{
    public class FSLegTribNCM : BaseEntity
    {
        public long legid { get; set; }
        public string ncm { get; set; }
    }
}
