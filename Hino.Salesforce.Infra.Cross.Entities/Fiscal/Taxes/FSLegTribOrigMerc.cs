namespace Hino.Salesforce.Infra.Cross.Entities.FiscalTaxes
{
    public class FSLegTribOrigMerc : BaseEntity
    {
        public long legid { get; set; }
        public long codorigmerc { get; set; }
    }
}
