﻿namespace Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes
{
    public class FSNCM : BaseEntity
    {
        public string Category { get; set; }
        public string NCM { get; set; }
        public string Description { get; set; }
        public decimal IPI { get; set; }
        public decimal AliquotaII { get; set; }
    }
}
