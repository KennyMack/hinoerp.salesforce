﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Business
{
    public class GEEnterpriseAnnot : BaseEntity
    {
        [ForeignKey("GEEnterprises")]
        public long EnterpriseId { get; set; }
        public virtual GEEnterprises GEEnterprises { get; set; }

        public long UserID { get; set; }
        public string Annotation { get; set; }



    }
}
