﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Business
{
    public class GEEnterpriseFiscalGroup : BaseEntity
    {
        public string Description { get; set; }
        public string Type { get; set; }
        public long IdERP { get; set; }
    }
}
