﻿using Hino.Salesforce.Infra.Cross.Utils.Attributes;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Business
{
    [Queue("GEEnterpriseGroup")]
    public class GEEnterpriseGroup : BaseEntity
    {
        public string Description { get; set; }
        public string Identifier { get; set; }
        public long IdERP { get; set; }
    }
}
