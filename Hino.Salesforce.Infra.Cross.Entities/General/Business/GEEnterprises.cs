﻿using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Business
{
    [Queue("GEEnterprises")]
    public class GEEnterprises : BaseEntity
    {
        public GEEnterprises()
        {
            this.VEOrders = new HashSet<VEOrders>();
            this.VEOrdersCarrier = new HashSet<VEOrders>();
            this.VEOrdersRedispatch = new HashSet<VEOrders>();
            this.GEEnterpriseGeo = new HashSet<GEEnterpriseGeo>();
            this.GEEnterpriseContacts = new HashSet<GEEnterpriseContacts>();
            this.GEUserEnterprises = new HashSet<GEUserEnterprises>();
            this.GEFilesPath = new HashSet<GEFilesPath>();
            this.GEEnterpriseEvent = new HashSet<GEEnterpriseEvent>();
            this.GEEnterpriseAnnotation = new HashSet<GEEnterpriseAnnot>();
        }

        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string CNPJCPF { get; set; }
        public string IE { get; set; }
        public EEnterpriseType Type { get; set; }
        public EStatusEnterprise Status { get; set; }
        public EStatusSinc StatusSinc { get; set; }
        public long IdERP { get; set; }
        public long RegionId { get; set; }
        public string search { get; set; }
        public decimal CreditLimit { get; set; }
        public decimal CreditUsed { get; set; }
        public bool SellerChangePrice { get; set; }
        public bool SellerPayCondition { get; set; }
        public bool SellerBonification { get; set; }
        public bool SellerSample { get; set; }
        public short DiscountNFBoleto { get; set; }
        public decimal PercDiscount { get; set; }
        public long? CodPrVenda { get; set; }

        public DateTime? BirthDate { get; set; }

        public EEnterpriseClassification Classification { get; set; }

        // public long UserId { get; set; }

        [ForeignKey("GEUsersCreatedBy")]
        public long? CreatedById { get; set; }
        public virtual GEUsers GEUsersCreatedBy { get; set; }

        public string ClassifEmpresa { get; set; }

        [ForeignKey("GEEnterpriseFiscalGroup")]
        public long? FiscalGroupId { get; set; }
        public virtual GEEnterpriseFiscalGroup GEEnterpriseFiscalGroup { get; set; }

        [ForeignKey("GEPaymentCondition")]
        public long? PayConditionId { get; set; }
        public virtual GEPaymentCondition GEPaymentCondition { get; set; }


        [ForeignKey("GEEnterpriseCategory")]
        public long? CategoryId { get; set; }
        public virtual GEEnterpriseCategory GEEnterpriseCategory { get; set; }

        [ForeignKey("GEEnterpriseGroup")]
        public long? GroupId { get; set; }
        public virtual GEEnterpriseGroup GEEnterpriseGroup { get; set; }

        [ForeignKey("VETypeSale")]
        public long? TypeSaleId { get; set; }
        public virtual VETypeSale VETypeSale { get; set; }

        [InverseProperty("GEEnterprises")]
        public virtual ICollection<VEOrders> VEOrders { get; set; }

        [InverseProperty("GECarriers")]
        public virtual ICollection<VEOrders> VEOrdersCarrier { get; set; }

        [InverseProperty("GERedispatch")]
        public virtual ICollection<VEOrders> VEOrdersRedispatch { get; set; }

        public virtual ICollection<GEEnterpriseGeo> GEEnterpriseGeo { get; set; }

        public virtual ICollection<GEEnterpriseContacts> GEEnterpriseContacts { get; set; }

        public virtual ICollection<GEUserEnterprises> GEUserEnterprises { get; set; }
        public virtual ICollection<GEFilesPath> GEFilesPath { get; set; }
        public virtual ICollection<GEEnterpriseEvent> GEEnterpriseEvent { get; set; }
        public virtual ICollection<GEEnterpriseAnnot> GEEnterpriseAnnotation { get; set; }
    }
}
