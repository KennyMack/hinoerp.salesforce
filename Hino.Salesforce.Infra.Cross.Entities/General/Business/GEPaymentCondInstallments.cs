﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Business
{
    public class GEPaymentCondInstallments : BaseEntity
    {
        [ForeignKey("GEPaymentCondition")]
        public long CondPayId { get; set; }
        public virtual GEPaymentCondition GEPaymentCondition { get; set; }

        public decimal Percent { get; set; }
        public int Days { get; set; }
    }
}
