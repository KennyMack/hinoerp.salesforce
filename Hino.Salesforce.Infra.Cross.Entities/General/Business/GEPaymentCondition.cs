﻿using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Business
{
    [Queue("GEPaymentCondition")]
    public class GEPaymentCondition : BaseEntity
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public GEPaymentCondition()
        {
            this.VEOrders = new HashSet<VEOrders>();
            this.GEPaymentCondInstallments = new HashSet<GEPaymentCondInstallments>();
        }

        [ForeignKey("GEPaymentType")]
        public long TypePayID { get; set; }
        public virtual GEPaymentType GEPaymentType { get; set; }

        public string Description { get; set; }
        public short Installments { get; set; }
        public decimal PercDiscount { get; set; }
        public decimal PercIncrease { get; set; }
        public long IdERP { get; set; }

        public virtual ICollection<VEOrders> VEOrders { get; set; }
        public virtual ICollection<GEPaymentCondInstallments> GEPaymentCondInstallments { get; set; }
    }
}
