﻿using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System.Collections.Generic;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Business
{
    [Queue("GEPaymentType")]
    public class GEPaymentType : BaseEntity
    {
        public GEPaymentType()
        {
            this.GEPaymentCondition = new HashSet<GEPaymentCondition>();
            this.VEOrders = new HashSet<VEOrders>();
        }

        public string Description { get; set; }
        public long IdERP { get; set; }
        public bool Boleto { get; set; }

        public virtual ICollection<GEUserPayType> GEUserPayType { get; set; }
        public virtual ICollection<GEPaymentCondition> GEPaymentCondition { get; set; }

        public virtual ICollection<VEOrders> VEOrders { get; set; }
    }
}
