﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Business
{
    public class GEUserEnterprises : BaseEntity
    {
        [ForeignKey("GEUsers")]
        public long UserId { get; set; }
        public virtual GEUsers GEUsers { get; set; }

        [ForeignKey("GEEnterprises")]
        public long EnterpriseId { get; set; }
        public virtual GEEnterprises GEEnterprises { get; set; }
    }
}
