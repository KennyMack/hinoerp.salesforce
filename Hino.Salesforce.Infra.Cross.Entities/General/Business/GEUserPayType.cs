using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Business
{
    public class GEUserPayType : BaseEntity
    {
        [ForeignKey("GEPaymentType")]
        public long PaymentTypeId { get; set; }
        public virtual GEPaymentType GEPaymentType { get; set; }
        
        [ForeignKey("GEUsers")]
        public long UserId { get; set; }
        public virtual GEUsers GEUsers { get; set; }
    }
}
