using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Events
{
    [Table("GEEVENTSCLASSIF")]
    public class GEEventsClassification : BaseEntity
    {
        public GEEventsClassification()
        {
            GEEvents = new HashSet<GEEvents>();
        }

        public string Description { get; set; }

        public virtual ICollection<GEEvents> GEEvents { get; set; }
    }
}
