using System;

namespace Hino.Salesforce.Infra.Cross.Entities.General
{
    public class GEEstAtvRtVig : BaseEntity
    {
        public DateTime Dtinicio { get; set; }
        public long CodAtividade { get; set; }
        public long CodRegTrib { get; set; }
        public long? CodCfgSimples { get; set; }
        public decimal? VlIniRecbrSimples { get; set; }
        public decimal? VlFimRecbrSimples { get; set; }
        public bool MajoraICMS { get; set; }
    }
}
