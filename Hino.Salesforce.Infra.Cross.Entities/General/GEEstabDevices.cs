﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General
{
    public class GEEstabDevices : BaseEntity
    {
        public string UserKey { get; set; }
        public string NickName { get; set; }

        [ForeignKey("GEEstablishments")]
        public long GEEstabID { get; set; }

        public virtual GEEstablishments GEEstablishments { get; set; }
    }
}
