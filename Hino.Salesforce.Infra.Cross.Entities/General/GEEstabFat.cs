using System;

namespace Hino.Salesforce.Infra.Cross.Entities.General
{
    public class GEEstabFat : BaseEntity
    {
        public long EstabId { get; set; }
        public DateTime Period { get; set; }
        public decimal Value { get; set; }
    }
}
