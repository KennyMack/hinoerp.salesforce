using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General
{
    public class GEEstabMenu : BaseEntity
    {
        public short Menu { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool Visible { get; set; }

        [ForeignKey("GEEstablishments")]
        public long GEEstabID { get; set; }

        public virtual GEEstablishments GEEstablishments { get; set; }
    }
}
