﻿using Hino.Salesforce.Infra.Cross.Entities.Fiscal;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General
{
    public class GEEstablishments : BaseEntity
    {
        public GEEstablishments()
        {
            this.GEEstabDevices = new HashSet<GEEstabDevices>();
            this.GEEstabFat = new HashSet<GEEstabFat>();
            this.GEEstabPay = new HashSet<GEEstabPay>();
            this.GEEstabMenu = new HashSet<GEEstabMenu>();
            this.GEFilesPath = new HashSet<GEFilesPath>();
            this.GEEstabCalendar = new HashSet<GEEstabCalendar>();
        }

        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public string CNPJCPF { get; set; }
        public int Devices { get; set; }
        public decimal PIS { get; set; }
        public decimal COFINS { get; set; }
        public string TokenCNPJ { get; set; }
        public bool AllowEnterprise { get; set; }
        public bool AllowPayment { get; set; }
        public bool AllowChangePrice { get; set; }
        public bool AllowDiscount { get; set; }
        public long? EstabGroup { get; set; }
        public bool FatorR { get; set; }
        public string CapptaKey { get; set; }
        public string SitefIp { get; set; }
        [ForeignKey("FSFiscalOper")]
        public long? DefaultFiscalOperID { get; set; }
        public virtual FSFiscalOper FSFiscalOper { get; set; }
        public string AditionalInfo { get; set; }
        public string DefaultNoteOrder { get; set; }
        public bool OnlyWithStock { get; set; }
        public bool OnlyOnDate { get; set; }
        public short SearchPosition { get; set; }

        public bool UseChangePrice { get; set; }
        public bool UsePayCondition { get; set; }
        public bool UseBonification { get; set; }
        public bool UseSample { get; set; }

        [ForeignKey("PfFiscalGroup")]
        public long? PfFiscalGroupId { get; set; }
        public virtual GEEnterpriseFiscalGroup PfFiscalGroup { get; set; }

        [ForeignKey("PjFiscalGroup")]
        public long? PjFiscalGroupId { get; set; }
        public virtual GEEnterpriseFiscalGroup PjFiscalGroup { get; set; }

        public string PfClassifClient { get; set; }
        public string PjClassifClient { get; set; }

        public int DaysPayment { get; set; }

        [ForeignKey("DefaultProduct")]
        public long? DefaultProductId { get; set; }
        public virtual GEProducts DefaultProduct { get; set; }

        public virtual ICollection<GEEstabDevices> GEEstabDevices { get; set; }
        public virtual ICollection<GEEstabFat> GEEstabFat { get; set; }
        public virtual ICollection<GEEstabPay> GEEstabPay { get; set; }
        public virtual ICollection<GEEstabMenu> GEEstabMenu { get; set; }
        public virtual ICollection<GEFilesPath> GEFilesPath { get; set; }
        public virtual ICollection<GEEstabCalendar> GEEstabCalendar { get; set; }

    }
}
