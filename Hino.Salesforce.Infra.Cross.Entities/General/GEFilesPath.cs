using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General
{
    public class GEFilesPath : BaseEntity
    {
        [ForeignKey("GEEstablishments")]
        public long? GEEstabID { get; set; }
        [ForeignKey("GEEnterprises")]
        public long? EnterpriseID { get; set; }
        [ForeignKey("GEUsers")]
        public long? UserID { get; set; }
        [ForeignKey("GEProducts")]
        public long? ProductID { get; set; }
        [ForeignKey("VEOrders")]
        public long? OrderID { get; set; }
        [ForeignKey("VEOrderItems")]
        public long? OrderItemID { get; set; }
        public string Name { get; set; }
        public string Path { get; set; }
        public string Description { get; set; }
        public string MimeType { get; set; }
        public string Extension { get; set; }
        public long FileSize { get; set; }
        public string Identifier { get; set; }

        public virtual GEEstablishments GEEstablishments { get; set; }
        public virtual GEEnterprises GEEnterprises { get; set; }
        public virtual GEUsers GEUsers { get; set; }
        public virtual GEProducts GEProducts { get; set; }
        public virtual VEOrders VEOrders { get; set; }
        public virtual VEOrderItems VEOrderItems { get; set; }
    }
}
