namespace Hino.Salesforce.Infra.Cross.Entities.General
{
    public class GEProductAplic : BaseEntity
    {
        public string Description { get; set; }
        public int Classification { get; set; }
        public long IdERP { get; set; }
    }
}
