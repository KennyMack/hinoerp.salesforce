﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.Cross.Entities.General
{
    public class GEProductKey : BaseEntity
    {
        public string Reference { get; set; }
        public int Sequence { get; set; }
    }
}
