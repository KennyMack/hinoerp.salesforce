﻿namespace Hino.Salesforce.Infra.Cross.Entities.General
{
    public class GEProductsType : BaseEntity
    {
        public string Description { get; set; }
        public string TypeProd { get; set; }
        public int? CodAnexoSimples { get; set; }
    }
}
