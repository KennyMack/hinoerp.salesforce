﻿namespace Hino.Salesforce.Infra.Cross.Entities.General
{
    public class GEProductsUnit : BaseEntity
    {
        public string Unit { get; set; }
        public string Description { get; set; }
    }
}
