﻿namespace Hino.Salesforce.Infra.Cross.Entities.General
{
    public class GEQueueItem : BaseEntity
    {
        public string EntryName { get; set; }
        public string Type { get; set; }
    }
}
