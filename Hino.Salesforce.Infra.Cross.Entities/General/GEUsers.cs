﻿using Hino.Salesforce.Infra.Cross.Entities.General.Events;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General
{
    public class GEUsers : BaseEntity
    {
        public GEUsers()
        {
            this.VEOrders = new HashSet<VEOrders>();
            this.GEUserDigitizer = new HashSet<VEOrders>();
            this.GEFilesPath = new HashSet<GEFilesPath>();
            this.GEUserCalendar = new HashSet<GEUserCalendar>();
        }
        public string UserName { get; set; }
        public string Name { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Password { get; set; }
        public DateTime LastLogin { get; set; }
        public EUserType UserType { get; set; }
        public decimal PercDiscount { get; set; }
        public decimal PercCommission { get; set; }
        public decimal PercDiscountPrice { get; set; }
        public decimal PercIncreasePrice { get; set; }
        public string StoreId { get; set; }
        public string TerminalId { get; set; }
        public string UserKey { get; set; }
        public bool IsBlockedByPay { get; set; }
        public float BoletoLimit { get; set; }

        public virtual ICollection<VEOrders> VEOrders { get; set; }
        public virtual ICollection<GEFilesPath> GEFilesPath { get; set; }
        public virtual ICollection<GEUserCalendar> GEUserCalendar { get; set; }
        [InverseProperty("GEUserDigitizer")]
        public virtual ICollection<VEOrders> GEUserDigitizer { get; set; }
        [NotMapped]
        public GEEstablishments GEEstablishments { get; set; }
    }
}
