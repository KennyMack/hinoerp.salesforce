using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Kanban
{
    public class GEBoardLists : BaseEntity
    {
        [ForeignKey("GEBoards")]
        public long BoardID { get; set; }
        public virtual GEBoards GEBoards { get; set; }

        public string Name { get; set; }
        public short Position { get; set; }
        public string Description { get; set; }
        public bool IsComplete { get; set; }
        public bool IsSuccess { get; set; }
    }
}
