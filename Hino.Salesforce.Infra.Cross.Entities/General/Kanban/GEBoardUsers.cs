using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Kanban
{
    public class GEBoardUsers : BaseEntity
    {
        [ForeignKey("GEUsers")]
        public long UserID { get; set; }
        public virtual GEUsers GEUsers { get; set; }

        [ForeignKey("GEBoards")]
        public long BoardID { get; set; }
        public virtual GEBoards GEBoards { get; set; }
    }
}
