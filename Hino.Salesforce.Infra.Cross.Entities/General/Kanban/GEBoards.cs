using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Kanban
{
    public class GEBoards : BaseEntity
    {
        public GEBoards()
        {
            GECardFields = new HashSet<GECardFields>();
            GEBoardUsers = new HashSet<GEBoardUsers>();
            GEBoardLists = new HashSet<GEBoardLists>();
        }

        [ForeignKey("GESettings")]
        public long SettingsID { get; set; }
        public virtual GESettings GESettings { get; set; }

        public string Name { get; set; }
        public bool IsPublic { get; set; }

        public virtual ICollection<GECardFields> GECardFields { get; set; }
        public virtual ICollection<GEBoardUsers> GEBoardUsers { get; set; }
        public virtual ICollection<GEBoardLists> GEBoardLists { get; set; }
    }
}
