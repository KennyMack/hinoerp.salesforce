using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Kanban
{
    public class GECardFields : BaseEntity
    {
        public GECardFields()
        {
            GEFieldUsers = new HashSet<GEFieldUsers>();
        }

        [ForeignKey("GEBoards")]
        public long BoardID { get; set; }
        public virtual GEBoards GEBoards { get; set; }

        public string Name { get; set; }
        public string Type { get; set; }
        public string Options { get; set; }
        public bool Required { get; set; }
        public bool IsPublic { get; set; }


        public virtual ICollection<GEFieldUsers> GEFieldUsers { get; set; }
    }
}
