using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Kanban
{
    public class GEFieldUsers : BaseEntity
    {
        [ForeignKey("GEUsers")]
        public long UserID { get; set; }
        public virtual GEUsers GEUsers { get; set; }

        [ForeignKey("GECardFields")]
        public long FieldID { get; set; }
        public virtual GECardFields GECardFields { get; set; }
    }
}
