using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Kanban
{
    public class GESettings : BaseEntity
    {
        public GESettings()
        {
            GESettingsTags = new HashSet<GESettingsTags>();
            GEBoards = new HashSet<GEBoards>();
        }

        [ForeignKey("GEEstablishments")]
        public long EstabID { get; set; }
        public virtual GEEstablishments GEEstablishments { get; set; }

        public virtual ICollection<GESettingsTags> GESettingsTags { get; set; }
        public virtual ICollection<GEBoards> GEBoards { get; set; }
    }
}
