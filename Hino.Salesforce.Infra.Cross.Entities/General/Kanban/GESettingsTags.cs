using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.General.Kanban
{
    public class GESettingsTags : BaseEntity
    {
        [ForeignKey("GESettings")]
        public long SettingsID { get; set; }
        public virtual GESettings GESettings { get; set; }

        public string Color { get; set; }
        public string Name { get; set; }
        public bool IsComplete { get; set; }
        public bool IsSuccess { get; set; }
    }
}
