﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.Cross.Entities.Sales
{
    public class VEEntSalePrice : BaseEntity
    {
        public long EnterpriseID { get; set; }
        public long SalePriceID { get; set; }
    }
}
