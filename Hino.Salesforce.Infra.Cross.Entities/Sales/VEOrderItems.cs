﻿using Hino.Salesforce.Infra.Cross.Entities.Fiscal;
using Hino.Salesforce.Infra.Cross.Entities.General;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;

namespace Hino.Salesforce.Infra.Cross.Entities.Sales
{
    public class VEOrderItems : BaseEntity
    {
        public VEOrderItems()
        {
            this.VEOrderTaxes = new HashSet<VEOrderTaxes>();
            this.GEFilesPath = new HashSet<GEFilesPath>();
        }

        [ForeignKey("VEOrders")]
        public long OrderID { get; set; }
        public virtual VEOrders VEOrders { get; set; }

        [ForeignKey("GEProducts")]
        public long ProductID { get; set; }
        public virtual GEProducts GEProducts { get; set; }

        [ForeignKey("FSFiscalOper")]
        public long FiscalOperID { get; set; }
        public virtual FSFiscalOper FSFiscalOper { get; set; }

        public decimal TableValue { get; set; }
        public float Value { get; set; }
        public float Quantity { get; set; }
        public float QuantityReference { get; set; }
        public float PercDiscount { get; set; }
        public string Note { get; set; }
        public int Item { get; set; }
        public int ItemLevel { get; set; }
        public long IdERP { get; set; }
        public string ClientOrder { get; set; }
        public string ClientItem { get; set; }
        public System.DateTime DeliveryDate { get; set; }
        public float PercDiscountHead { get; set; }
        public float PercCommission { get; set; }
        public float PercCommissionHead { get; set; }
        public int ShippingDays { get; set; }
        public bool AltDescription { get; set; }
        public float QuantityReturned { get; set; }

        public virtual ICollection<VEOrderTaxes> VEOrderTaxes { get; set; }
        public virtual ICollection<GEFilesPath> GEFilesPath { get; set; }

        [NotMapped]
        public float ValueWithDiscount
        {
            get => Value - (Value * (PercDiscount / 100)) - (Value * (PercDiscountHead / 100));
        }

        [NotMapped]
        public decimal PercICMS
        {
            get
            {
                if (VEOrderTaxes != null && VEOrderTaxes.Count > 0)
                    return VEOrderTaxes.Where(r => r.Type == 0).Max(r => r.Aliquot);

                return 0;
            }
        }

        [NotMapped]
        public decimal TotalIPI
        {
            get
            {
                if (VEOrderTaxes != null && VEOrderTaxes.Count > 0)
                    return VEOrderTaxes.Where(r => r.Type == 3).Sum(r => r.Value);

                return 0;
            }
        }

        [NotMapped]
        public decimal PercIPI
        {
            get
            {
                if (VEOrderTaxes != null && VEOrderTaxes.Count > 0)
                    return VEOrderTaxes.Where(r => r.Type == 3).Max(r => r.Aliquot);

                return 0;
            }
        }

        [NotMapped]
        public decimal PercICMSST
        {
            get
            {
                if (VEOrderTaxes != null && VEOrderTaxes.Count > 0)
                    return VEOrderTaxes.Where(r => r.Type == 1 || r.Type == 18).Sum(r => r.Value);

                return 0;
            }
        }

        [NotMapped]
        public decimal TotalICMSST
        {
            get
            {
                if (VEOrderTaxes != null && VEOrderTaxes.Count > 0)
                    return VEOrderTaxes.Where(r => r.Type == 1 || r.Type == 18).Sum(r => r.Value);

                return 0;
            }
        }

        [NotMapped]
        public float TotalValue
        {
            get
            {
                return Quantity * Value;
            }
        }

        [NotMapped]
        public float TotalValueWithDiscount
        {
            get => TotalValue - (TotalValue * (PercDiscount / 100)) - (TotalValue * (PercCommissionHead / 100));
        }

        [NotMapped]
        public float BaseCommission
        {
            get
            {
                return (Quantity - QuantityReturned) * Value;
            }
        }
    }
}
