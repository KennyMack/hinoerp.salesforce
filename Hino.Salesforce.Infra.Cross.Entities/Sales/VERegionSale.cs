﻿using System.Collections.Generic;

namespace Hino.Salesforce.Infra.Cross.Entities.Sales
{
    public class VERegionSale : BaseEntity
    {
        public VERegionSale()
        {
            this.VERegionSaleUF = new HashSet<VERegionSaleUF>();
            //this.VESalePrice = new HashSet<VESalePrice>();
        }

        public string Description { get; set; }
        public long IdERP { get; set; }

        public virtual ICollection<VERegionSaleUF> VERegionSaleUF { get; set; }
        //public virtual ICollection<VERegionSale> VESalePrice { get; set; }

    }
}
