﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.Sales
{
    public class VERegionSaleUF : BaseEntity
    {
        public VERegionSaleUF()
        {

        }
        [ForeignKey("VERegionSale")]
        public long RegionId { get; set; }
        public virtual VERegionSale VERegionSale { get; set; }

        public string UF { get; set; }
    }
}
