﻿using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.Sales
{
    [Queue("VESalePrice")]
    public class VESalePrice : BaseEntity
    {
        public VESalePrice()
        {

        }

        public long CodPrVenda { get; set; }
        [ForeignKey("VERegionSale")]
        public long RegionId { get; set; }
        public virtual VERegionSale VERegionSale { get; set; }

        public string Description { get; set; }

        [ForeignKey("GEProducts")]
        public long ProductId { get; set; }

        public virtual GEProducts GEProducts { get; set; }

        public decimal Value { get; set; }

        [NotMapped]
        public string ProductKey { get; set; }
        [NotMapped]
        public long CodRegiao { get; set; }
    }
}
