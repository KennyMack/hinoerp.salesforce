﻿using Hino.Salesforce.Infra.Cross.Utils;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.Sales
{
    public class VESaleWorkRegion : BaseEntity
    {
        public string Description { get; set; }
        public string ZIPCodeStart { get; set; }
        [NotMapped]
        public string ZIPCodeStartCleaned { get => ZIPCodeStart.OnlyNumbers(); }
        public string ZIPCodeEnd { get; set; }
        [NotMapped]
        public string ZIPCodeEndCleaned { get => ZIPCodeEnd.OnlyNumbers(); }
    }
}
