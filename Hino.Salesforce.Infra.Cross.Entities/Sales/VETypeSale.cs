
namespace Hino.Salesforce.Infra.Cross.Entities.Sales
{
    public class VETypeSale : BaseEntity
    {
        public string Description { get; set; }
        public long? IdERP { get; set; }
    }
}
