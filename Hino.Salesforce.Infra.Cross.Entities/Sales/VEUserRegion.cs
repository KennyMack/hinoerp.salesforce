﻿using Hino.Salesforce.Infra.Cross.Entities.General;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Infra.Cross.Entities.Sales
{
    public class VEUserRegion : BaseEntity
    {
        [ForeignKey("GEUsers")]
        public long UserId { get; set; }
        public virtual GEUsers GEUsers { get; set; }

        [ForeignKey("VESaleWorkRegion")]
        public long SaleWorkId { get; set; }
        public virtual VESaleWorkRegion VESaleWorkRegion { get; set; }
    }
}
