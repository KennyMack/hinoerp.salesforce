﻿using Hino.Salesforce.Infra.Cross.Utils.Settings;
using Microsoft.IdentityModel.Tokens;
using System.Security.Cryptography;
using System.Text;

namespace Hino.Salesforce.Infra.Cross.Identity.Configurations
{
    public class SigningConfigurations
    {
        public SecurityKey Key { get; }
        public SigningCredentials SigningCredentials { get; }
        public SymmetricSecurityKey SymmKey { get; private set; }

        public SigningConfigurations()
        {

            var securityKey = AppSettings.SecurityKey;

            using (var provider = new RSACryptoServiceProvider(2048))
            {
                Key = new SymmetricSecurityKey(
                    Encoding.UTF8.GetBytes(securityKey));
                //Encoding.UTF8.GetBytes("YXNzYXN1c2hhdWhzYXVhaHN1c2FodWFzaHN1c2FodXNhaHVzYXN1c3NhaHVz"));//new RsaSecurityKey(provider.ExportParameters(true));
            }

            SigningCredentials = new SigningCredentials(
                Key, SecurityAlgorithms.HmacSha256);

            SymmKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
               securityKey
            ));

        }
    }
}
