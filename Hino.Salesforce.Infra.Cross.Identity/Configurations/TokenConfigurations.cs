﻿using Hino.Salesforce.Infra.Cross.Utils.Settings;
using System;

namespace Hino.Salesforce.Infra.Cross.Identity.Configurations
{
    public class TokenConfigurations
    {
        public string Audience { get; private set; }
        public string Issuer { get; private set; }
        public int Seconds { get; private set; }
        public string SecurityKey { get; private set; }
        public int FinalExpiration { get; private set; }

        public TokenConfigurations()
        {
            Audience = AppSettings.Audience;
            Issuer = AppSettings.Issuer;
            Seconds = Convert.ToInt32(AppSettings.Seconds);
            FinalExpiration = Convert.ToInt32(AppSettings.FinalExpiration);
            SecurityKey = AppSettings.SecurityKey;
        }
    }
}
