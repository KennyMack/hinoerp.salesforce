﻿namespace Hino.Salesforce.Infra.Cross.Identity
{
    public static class Metis
    {
        public static string CryptPass(this string password)
        {
            string Salt = BCrypt.Net.BCrypt.GenerateSalt(11);

            return BCrypt.Net.BCrypt.HashPassword(string.Concat(password, "HinoDummy"), Salt);
        }

        public static bool ValidatePass(this string password, string hashedPassword)
        {
            if (string.IsNullOrEmpty(hashedPassword))
                return false;

            return BCrypt.Net.BCrypt.Verify(string.Concat(password, "HinoDummy"), hashedPassword);
        }
    }
}
