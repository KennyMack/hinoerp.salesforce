﻿using Hino.Salesforce.Infra.Cross.Identity.Configurations;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;

namespace Hino.Salesforce.Infra.Cross.Identity.Token
{
    public class JWTToken
    {
        public string PID { get; set; }

        public string SessionId { get; set; }

        public long UserId { get; set; }

        public string Email { get; set; }

        public string Username { get; set; }

        public int UserType { get; set; }

        public DateTime ExpireAt { get; set; }

        public ClaimsIdentity Identity { get; private set; }

        public ClaimsPrincipal GetPrincipal(string token,
            SigningConfigurations signingConfigurations,
            TokenConfigurations tokenConfigurations)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var jwtToken = tokenHandler.ReadToken(token) as JwtSecurityToken;

                if (jwtToken == null)
                    return null;

                var validationParameters = new TokenValidationParameters()
                {
                    RequireExpirationTime = true,
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    IssuerSigningKey = signingConfigurations.SymmKey
                };

                SecurityToken securityToken;
                var principal = tokenHandler.ValidateToken(token, validationParameters, out securityToken);

                object dtExpiration = null;
                if (jwtToken.Payload.TryGetValue("ExpireAt", out dtExpiration))
                {
                    if (DateTime.Now > Convert.ToDateTime(dtExpiration.ToString()))
                        return null;
                }

                return principal;
            }

            catch (Exception e)
            {
                Logging.Exception(e);
                return null;
            }

        }

        public JWTTokenValue GenerateToken(
            SigningConfigurations signingConfigurations,
            TokenConfigurations tokenConfigurations)
        {
            var RefreshToken = Guid.NewGuid().ToString("N");

            var CreatedAt = DateTime.Now;

            Identity = new ClaimsIdentity(
                // new GenericIdentity(TokenData.SessionId, "Login"),
                new[]
                {
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                    new Claim(JwtRegisteredClaimNames.UniqueName, SessionId),
                    new Claim(TokenClaim.PID, PID),
                    new Claim(TokenClaim.SessionId, SessionId),
                    new Claim(TokenClaim.CreatedAt, CreatedAt.ToString("yyyy-MM-dd HH:mm:ss")),
                    new Claim(TokenClaim.ExpireAt, ExpireAt.ToString("yyyy-MM-dd HH:mm:ss")),
                }
                /*new GenericIdentity(userID, "Login"),
                new[] {
                        new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString("N")),
                        new Claim(JwtRegisteredClaimNames.UniqueName, userID)
                }*/
            );

            var handler = new JwtSecurityTokenHandler();
            var securityToken = handler.CreateToken(new SecurityTokenDescriptor
            {
                Issuer = tokenConfigurations.Issuer,
                Audience = tokenConfigurations.Audience,
                Subject = Identity,
                NotBefore = CreatedAt,
                Expires = ExpireAt,
                IssuedAt = DateTime.Now,
                SigningCredentials = signingConfigurations.SigningCredentials
            });

            return new JWTTokenValue
            {
                RefreshToken = RefreshToken,
                TokenValue = handler.WriteToken(securityToken),
                CreatedAt = CreatedAt,
                ExpireAt = ExpireAt,
                IsAuthenticated = true
            };
        }
    }
}
