﻿using System;

namespace Hino.Salesforce.Infra.Cross.Identity.Token
{
    public class JWTTokenValue
    {
        public string TokenValue { get; set; }

        public string RefreshToken { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime ExpireAt { get; set; }

        public bool IsAuthenticated { get; set; }
    }
}
