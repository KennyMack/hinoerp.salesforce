﻿namespace Hino.Salesforce.Infra.Cross.Identity.Token
{
    public class TokenClaim
    {
        public const string PID = "PID";
        public const string SessionId = "SessionId";
        public const string CreatedAt = "CreatedAt";
        public const string ExpireAt = "ExpireAt";
    }
}
