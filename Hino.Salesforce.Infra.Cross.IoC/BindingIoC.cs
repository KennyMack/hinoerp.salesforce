﻿using Hino.Salesforce.Application.Interfaces.Auth;
using Hino.Salesforce.Application.Interfaces.AWS;
using Hino.Salesforce.Application.Interfaces.Email;
using Hino.Salesforce.Application.Interfaces.Email.General;
using Hino.Salesforce.Application.Interfaces.Fiscal;
using Hino.Salesforce.Application.Interfaces.Fiscal.Taxes;
using Hino.Salesforce.Application.Interfaces.General;
using Hino.Salesforce.Application.Interfaces.General.Business;
using Hino.Salesforce.Application.Interfaces.General.Demograph;
using Hino.Salesforce.Application.Interfaces.General.Events;
using Hino.Salesforce.Application.Interfaces.General.Kanban;
using Hino.Salesforce.Application.Interfaces.Logistics;
using Hino.Salesforce.Application.Interfaces.Route;
using Hino.Salesforce.Application.Interfaces.Sales;
using Hino.Salesforce.Application.Interfaces.Sales.Transactions;
using Hino.Salesforce.Application.Services.Auth;
using Hino.Salesforce.Application.Services.AWS;
using Hino.Salesforce.Application.Services.Email;
using Hino.Salesforce.Application.Services.Email.General;
using Hino.Salesforce.Application.Services.Fiscal;
using Hino.Salesforce.Application.Services.Fiscal.Taxes;
using Hino.Salesforce.Application.Services.General;
using Hino.Salesforce.Application.Services.General.Business;
using Hino.Salesforce.Application.Services.General.Demograph;
using Hino.Salesforce.Application.Services.General.Events;
using Hino.Salesforce.Application.Services.General.Kanban;
using Hino.Salesforce.Application.Services.Logistics;
using Hino.Salesforce.Application.Services.Route;
using Hino.Salesforce.Application.Services.Sales;
using Hino.Salesforce.Application.Services.Sales.Transactions;
using Hino.Salesforce.Domain.Auth.Interfaces.Repositories;
using Hino.Salesforce.Domain.Auth.Interfaces.Services;
using Hino.Salesforce.Domain.Auth.Services;
using Hino.Salesforce.Domain.Base.Interfaces.Email;
using Hino.Salesforce.Domain.Base.Interfaces.UoW;
using Hino.Salesforce.Domain.Fiscal.Interfaces.Repositories;
using Hino.Salesforce.Domain.Fiscal.Interfaces.Repositories.Taxes;
using Hino.Salesforce.Domain.Fiscal.Interfaces.Services;
using Hino.Salesforce.Domain.Fiscal.Interfaces.Services.Taxes;
using Hino.Salesforce.Domain.Fiscal.Services;
using Hino.Salesforce.Domain.Fiscal.Services.Taxes;
using Hino.Salesforce.Domain.General.Events.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Events.Interfaces.Services;
using Hino.Salesforce.Domain.General.Events.Services;
using Hino.Salesforce.Domain.General.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Interfaces.Repositories.Business;
using Hino.Salesforce.Domain.General.Interfaces.Repositories.Demograph;
using Hino.Salesforce.Domain.General.Interfaces.Services;
using Hino.Salesforce.Domain.General.Interfaces.Services.Business;
using Hino.Salesforce.Domain.General.Interfaces.Services.Demograph;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Repositories;
using Hino.Salesforce.Domain.General.Kanban.Interfaces.Services;
using Hino.Salesforce.Domain.General.Kanban.Services;
using Hino.Salesforce.Domain.General.Services;
using Hino.Salesforce.Domain.General.Services.Business;
using Hino.Salesforce.Domain.General.Services.Demograph;
using Hino.Salesforce.Domain.Logistics.Interfaces.Repositories;
using Hino.Salesforce.Domain.Logistics.Interfaces.Services;
using Hino.Salesforce.Domain.Logistics.Services;
using Hino.Salesforce.Domain.Sales.Interfaces.Repositories;
using Hino.Salesforce.Domain.Sales.Interfaces.Repositories.Transaction;
using Hino.Salesforce.Domain.Sales.Interfaces.Services;
using Hino.Salesforce.Domain.Sales.Interfaces.Services.Transaction;
using Hino.Salesforce.Domain.Sales.Services;
using Hino.Salesforce.Domain.Sales.Services.Transaction;
using Hino.Salesforce.Infra.Cross.Cache;
using Hino.Salesforce.Infra.Cross.Cache.Auth;
using Hino.Salesforce.Infra.Cross.Cache.Establishment;
using Hino.Salesforce.Infra.Cross.Email;
using Hino.Salesforce.Infra.Cross.Email.Configurations;
using Hino.Salesforce.Infra.Cross.Email.General;
using Hino.Salesforce.Infra.Cross.Identity.Configurations;
using Hino.Salesforce.Infra.DataBase.Context;
using Hino.Salesforce.Infra.DataBase.Repositories.Auth;
using Hino.Salesforce.Infra.DataBase.Repositories.Fiscal;
using Hino.Salesforce.Infra.DataBase.Repositories.Fiscal.Taxes;
using Hino.Salesforce.Infra.DataBase.Repositories.General;
using Hino.Salesforce.Infra.DataBase.Repositories.General.Business;
using Hino.Salesforce.Infra.DataBase.Repositories.General.Demograph;
using Hino.Salesforce.Infra.DataBase.Repositories.General.Events;
using Hino.Salesforce.Infra.DataBase.Repositories.General.Kanban;
using Hino.Salesforce.Infra.DataBase.Repositories.Logistics;
using Hino.Salesforce.Infra.DataBase.Repositories.Sales;
using Hino.Salesforce.Infra.DataBase.Repositories.Sales.Transactions;
using Hino.Salesforce.Infra.DataBase.UoW;
using Microsoft.Extensions.DependencyInjection;

namespace Hino.Salesforce.Infra.Cross.IoC
{
    public static class BindingIoC
    {
        public static void RegisterServices(this IServiceCollection services)
        {
            services.AddSingleton(new SessionTokenCache(new RedisConfigurationSection()));
            services.AddSingleton(new EstablishmentInfoCache(new RedisConfigurationSection()));
            services.AddSingleton(new SigningConfigurations());
            services.AddSingleton(new EmailConfiguration());
            services.AddSingleton(new TokenConfigurations());

            services.AddScoped<ISendEmail, SendEmail>();
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddTransient<AppDbContext>();

            services.AddTransient<EstablishmentEmail>();

            services.AddTransient<IRouteXL, RouteXL>();
            services.AddTransient<IMapQuest, MapQuest>();
            services.AddTransient<IMapBox, MapBox>();
            services.AddTransient<IHereMaps, HereMaps>();

            services.AddTransient<IVEOrderTaxesRepository, VEOrderTaxesRepository>();
            services.AddTransient<IVEOrderTaxesService, VEOrderTaxesService>();
            services.AddTransient<IVEOrderTaxesAS, VEOrderTaxesAS>();

            services.AddTransient<IGEProductAplicRepository, GEProductAplicRepository>();
            services.AddTransient<IAUExpiredTokenRepository, AUExpiredTokenRepository>();
            services.AddTransient<IAURefreshTokenRepository, AURefreshTokenRepository>();
            services.AddTransient<IGEEnterprisesRepository, GEEnterprisesRepository>();
            services.AddTransient<IGEEnterpriseAnnotationRepository, GEEnterpriseAnnotationRepository>();
            services.AddTransient<IGEUserEnterprisesRepository, GEUserEnterprisesRepository>();
            services.AddTransient<IGEEnterpriseGeoRepository, GEEnterpriseGeoRepository>();
            services.AddTransient<IGEEnterpriseContactsRepository, GEEnterpriseContactsRepository>();
            services.AddTransient<IGEEnterpriseFiscalGroupRepository, GEEnterpriseFiscalGroupRepository>();
            services.AddTransient<IGEPaymentConditionRepository, GEPaymentConditionRepository>();
            services.AddTransient<IGEPaymentCondInstallmentsRepository, GEPaymentCondInstallmentsRepository>();
            services.AddTransient<IGEEnterpriseCategoryRepository, GEEnterpriseCategoryRepository>();
            services.AddTransient<IGEEnterpriseGroupRepository, GEEnterpriseGroupRepository>();
            services.AddTransient<IGEPaymentTypeRepository, GEPaymentTypeRepository>();
            services.AddTransient<IGECitiesRepository, GECitiesRepository>();
            services.AddTransient<IGECountriesRepository, GECountriesRepository>();
            services.AddTransient<IGEStatesRepository, GEStatesRepository>();
            services.AddTransient<IGEEstabDevicesRepository, GEEstabDevicesRepository>();
            services.AddTransient<IGEEstablishmentsRepository, GEEstablishmentsRepository>();
            services.AddTransient<IGEProductsRepository, GEProductsRepository>();
            services.AddTransient<IGEUsersRepository, GEUsersRepository>();
            services.AddTransient<IVEOrderItemsRepository, VEOrderItemsRepository>();
            services.AddTransient<IVEOrdersRepository, VEOrdersRepository>();
            services.AddTransient<IVERegionSaleRepository, VERegionSaleRepository>();
            services.AddTransient<IVERegionSaleUFRepository, VERegionSaleUFRepository>();
            services.AddTransient<IVESalePriceRepository, VESalePriceRepository>();
            services.AddTransient<IVESaleWorkRegionRepository, VESaleWorkRegionRepository>();
            services.AddTransient<IVEUserRegionRepository, VEUserRegionRepository>();
            services.AddTransient<IFSICMSSTAliqUFRepository, FSICMSSTAliqUFRepository>();
            services.AddTransient<IFSNCMRepository, FSNCMRepository>();
            services.AddTransient<IFSFiscalOperRepository, FSFiscalOperRepository>();
            services.AddTransient<ILORoutesRepository, LORoutesRepository>();
            services.AddTransient<ILOTripsRepository, LOTripsRepository>();
            services.AddTransient<ILOWaypointsRepository, LOWaypointsRepository>();
            services.AddTransient<IGEProductsFamilyRepository, GEProductsFamilyRepository>();
            services.AddTransient<IGEProductsTypeRepository, GEProductsTypeRepository>();
            services.AddTransient<IGEProductsUnitRepository, GEProductsUnitRepository>();
            services.AddTransient<IVETransactionsRepository, VETransactionsRepository>();
            services.AddTransient<IGEEstabMenuRepository, GEEstabMenuRepository>();
            services.AddTransient<IGEFilesPathRepository, GEFilesPathRepository>();
            services.AddTransient<IGEUserPayTypeRepository, GEUserPayTypeRepository>();
            services.AddTransient<IGEProductKeyRepository, GEProductKeyRepository>();

            services.AddTransient<IGEProductAplicService, GEProductAplicService>();
            services.AddTransient<IAUExpiredTokenService, AUExpiredTokenService>();
            services.AddTransient<IAURefreshTokenService, AURefreshTokenService>();
            services.AddTransient<IGEEnterprisesService, GEEnterprisesService>();
            services.AddTransient<IGEUserEnterprisesService, GEUserEnterprisesService>();
            services.AddTransient<IGEEnterpriseAnnotationService, GEEnterpriseAnnotationService>();
            services.AddTransient<IGEEnterpriseGeoService, GEEnterpriseGeoService>();
            services.AddTransient<IGEEnterpriseContactsService, GEEnterpriseContactsService>();
            services.AddTransient<IGEPaymentConditionService, GEPaymentConditionService>();
            services.AddTransient<IGEPaymentCondInstallmentsService, GEPaymentCondInstallmentsService>();
            services.AddTransient<IGEEnterpriseFiscalGroupService, GEEnterpriseFiscalGroupService>();
            services.AddTransient<IGEEnterpriseCategoryService, GEEnterpriseCategoryService>();
            services.AddTransient<IGEEnterpriseGroupService, GEEnterpriseGroupService>();
            services.AddTransient<IGEPaymentTypeService, GEPaymentTypeService>();
            services.AddTransient<IGECitiesService, GECitiesService>();
            services.AddTransient<IGECountriesService, GECountriesService>();
            services.AddTransient<IGEStatesService, GEStatesService>();
            services.AddTransient<IGEEstabDevicesService, GEEstabDevicesService>();
            services.AddTransient<IGEEstablishmentsService, GEEstablishmentsService>();
            services.AddTransient<IGEProductsService, GEProductsService>();
            services.AddTransient<IGEUsersService, GEUsersService>();
            services.AddTransient<IVEOrderItemsService, VEOrderItemsService>();
            services.AddTransient<IVEOrdersService, VEOrdersService>();
            services.AddTransient<IVERegionSaleService, VERegionSaleService>();
            services.AddTransient<IVERegionSaleUFService, VERegionSaleUFService>();
            services.AddTransient<IVESalePriceService, VESalePriceService>();
            services.AddTransient<IVESaleWorkRegionService, VESaleWorkRegionService>();
            services.AddTransient<IVEUserRegionService, VEUserRegionService>();
            services.AddTransient<IFSICMSSTAliqUFService, FSICMSSTAliqUFService>();
            services.AddTransient<IFSNCMService, FSNCMService>();
            services.AddTransient<IFSFiscalOperService, FSFiscalOperService>();
            services.AddTransient<ILORoutesService, LORoutesService>();
            services.AddTransient<ILOTripsService, LOTripsService>();
            services.AddTransient<ILOWaypointsService, LOWaypointsService>();
            services.AddTransient<IGEProductsFamilyService, GEProductsFamilyService>();
            services.AddTransient<IGEProductsTypeService, GEProductsTypeService>();
            services.AddTransient<IGEProductsUnitService, GEProductsUnitService>();
            services.AddTransient<IVETransactionsService, VETransactionsService>();
            services.AddTransient<IGEEstabMenuService, GEEstabMenuService>();
            services.AddTransient<IGEFilesPathService, GEFilesPathService>();
            services.AddTransient<IGEUserPayTypeService, GEUserPayTypeService>();
            services.AddTransient<IGEProductKeyService, GEProductKeyService>();

            services.AddTransient<IGEProductAplicAS, GEProductAplicAS>();
            services.AddTransient<IAUExpiredTokenAS, AUExpiredTokenAS>();
            services.AddTransient<IAURefreshTokenAS, AURefreshTokenAS>();
            services.AddTransient<IGEEnterprisesAS, GEEnterprisesAS>();
            services.AddTransient<IGEUserEnterprisesAS, GEUserEnterprisesAS>();
            services.AddTransient<IGEEnterpriseContactsAS, GEEnterpriseContactsAS>();
            services.AddTransient<IGEEnterpriseGeoAS, GEEnterpriseGeoAS>();
            services.AddTransient<IGEPaymentConditionAS, GEPaymentConditionAS>();
            services.AddTransient<IGEEnterpriseAnnotationAS, GEEnterpriseAnnotationAS>();
            services.AddTransient<IGEPaymentCondInstallmentsAS, GEPaymentCondInstallmentsAS>();
            services.AddTransient<IGEEnterpriseFiscalGroupAS, GEEnterpriseFiscalGroupAS>();
            services.AddTransient<IGEEnterpriseCategoryAS, GEEnterpriseCategoryAS>();
            services.AddTransient<IGEEnterpriseGroupAS, GEEnterpriseGroupAS>();
            services.AddTransient<IGEPaymentTypeAS, GEPaymentTypeAS>();
            services.AddTransient<IGECitiesAS, GECitiesAS>();
            services.AddTransient<IGECountriesAS, GECountriesAS>();
            services.AddTransient<IGEStatesAS, GEStatesAS>();
            services.AddTransient<IGEEstabDevicesAS, GEEstabDevicesAS>();
            services.AddTransient<IGEEstablishmentsAS, GEEstablishmentsAS>();
            services.AddTransient<IGEProductsAS, GEProductsAS>();
            services.AddTransient<IGEUsersAS, GEUsersAS>();
            services.AddTransient<IVEOrderItemsAS, VEOrderItemsAS>();
            services.AddTransient<IVEOrdersAS, VEOrdersAS>();
            services.AddTransient<ISendEmailAS, SendEmailAS>();
            services.AddTransient<IVERegionSaleAS, VERegionSaleAS>();
            services.AddTransient<IVERegionSaleUFAS, VERegionSaleUFAS>();
            services.AddTransient<IVESalePriceAS, VESalePriceAS>();
            services.AddTransient<IVESaleWorkRegionAS, VESaleWorkRegionAS>();
            services.AddTransient<IVEUserRegionAS, VEUserRegionAS>();
            services.AddTransient<IFSICMSSTAliqUFAS, FSICMSSTAliqUFAS>();
            services.AddTransient<IFSNCMAS, FSNCMAS>();
            services.AddTransient<IFSFiscalOperAS, FSFiscalOperAS>();
            services.AddTransient<ILORoutesAS, LORoutesAS>();
            services.AddTransient<ILOTripsAS, LOTripsAS>();
            services.AddTransient<ILOWaypointsAS, LOWaypointsAS>();
            services.AddTransient<IGEProductsFamilyAS, GEProductsFamilyAS>();
            services.AddTransient<IGEProductsTypeAS, GEProductsTypeAS>();
            services.AddTransient<IGEProductsUnitAS, GEProductsUnitAS>();
            services.AddTransient<ICEPAS, CEPAS>();
            services.AddTransient<IReceitaWSAS, ReceitaWSAS>();
            services.AddTransient<IVETransactionsAS, VETransactionsAS>();
            services.AddTransient<ICapptaApiAS, CapptaApiAS>();
            services.AddTransient<IGEEstabMenuAS, GEEstabMenuAS>();
            services.AddTransient<IGEFilesPathAS, GEFilesPathAS>();
            services.AddTransient<IGEUserPayTypeAS, GEUserPayTypeAS>();

            services.AddTransient<IEstablishmentEmail, EstablishmentEmail>();
            services.AddTransient<IEstablishmentEmailAS, EstablishmentEmailAS>();
            services.AddTransient<IAWSFileAS, AWSFileAS>();

            services.AddTransient<IGESettingsRepository, GESettingsRepository>();
            services.AddTransient<IGESettingsService, GESettingsService>();
            services.AddTransient<IGESettingsAS, GESettingsAS>();

            services.AddTransient<IGEBoardsRepository, GEBoardsRepository>();
            services.AddTransient<IGEBoardsService, GEBoardsService>();
            services.AddTransient<IGEBoardsAS, GEBoardsAS>();

            services.AddTransient<IGEBoardUsersRepository, GEBoardUsersRepository>();
            services.AddTransient<IGEBoardUsersService, GEBoardUsersService>();
            services.AddTransient<IGEBoardUsersAS, GEBoardUsersAS>();

            services.AddTransient<IGESettingsTagsRepository, GESettingsTagsRepository>();
            services.AddTransient<IGESettingsTagsService, GESettingsTagsService>();
            services.AddTransient<IGESettingsTagsAS, GESettingsTagsAS>();

            services.AddTransient<IGECardFieldsRepository, GECardFieldsRepository>();
            services.AddTransient<IGECardFieldsService, GECardFieldsService>();
            services.AddTransient<IGECardFieldsAS, GECardFieldsAS>();

            services.AddTransient<IGEFieldUsersRepository, GEFieldUsersRepository>();
            services.AddTransient<IGEFieldUsersService, GEFieldUsersService>();
            services.AddTransient<IGEFieldUsersAS, GEFieldUsersAS>();

            services.AddTransient<IGEBoardListsRepository, GEBoardListsRepository>();
            services.AddTransient<IGEBoardListsService, GEBoardListsService>();
            services.AddTransient<IGEBoardListsAS, GEBoardListsAS>();

            services.AddTransient<IGEEventsRepository, GEEventsRepository>();
            services.AddTransient<IGEEventsService, GEEventsService>();
            services.AddTransient<IGEEventsAS, GEEventsAS>();

            services.AddTransient<IGEEventsClassificationRepository, GEEventsClassificationRepository>();
            services.AddTransient<IGEEventsClassificationService, GEEventsClassificationService>();
            services.AddTransient<IGEEventsClassificationAS, GEEventsClassificationAS>();

            services.AddTransient<IGEEstabCalendarRepository, GEEstabCalendarRepository>();
            services.AddTransient<IGEEstabCalendarService, GEEstabCalendarService>();
            services.AddTransient<IGEEstabCalendarAS, GEEstabCalendarAS>();

            services.AddTransient<IGEUserCalendarRepository, GEUserCalendarRepository>();
            services.AddTransient<IGEUserCalendarService, GEUserCalendarService>();
            services.AddTransient<IGEUserCalendarAS, GEUserCalendarAS>();

            services.AddTransient<IGEEnterpriseEventRepository, GEEnterpriseEventRepository>();
            services.AddTransient<IGEEnterpriseEventService, GEEnterpriseEventService>();
            services.AddTransient<IGEEnterpriseEventAS, GEEnterpriseEventAS>();

            services.AddTransient<IVETypeSaleRepository, VETypeSaleRepository>();
            services.AddTransient<IVETypeSaleService, VETypeSaleService>();
            services.AddTransient<IVETypeSaleAS, VETypeSaleAS>();

            services.AddTransient<IVEEntSalePriceRepository, VEEntSalePriceRepository>();
            services.AddTransient<IVEEntSalePriceService, VEEntSalePriceService>();
            services.AddTransient<IVEEntSalePriceAS, VEEntSalePriceAS>();
        }
    }
}

