﻿using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Hino.Salesforce.Infra.Cross.Utils.Files;
using System;
using System.Data.Entity;

namespace Hino.Salesforce.Infra.Cross.Utils
{
    public static class ConvertEx
    {
        public static int? ToInt32(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            var val = 0;
            if (int.TryParse(value.ToString(), out val))
                return val;

            return null;
        }

        public static short? ToInt16(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            short val = 0;
            if (short.TryParse(value.ToString(), out val))
                return val;

            return null;
        }

        public static long? ToInt64(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            long val = 0;
            if (long.TryParse(value.ToString(), out val))
                return val;

            return null;
        }

        public static bool ToBoolean(this object value)
        {
            if (value == null || value == DBNull.Value)
                return false;

            if (bool.TryParse(value.ToString(), out bool val))
                return val;

            return val;
        }

        public static decimal? ToDecimal(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            decimal val = 0;
            if (decimal.TryParse(value.ToString(), out val))
                return val;

            return null;
        }

        public static sbyte? ToSByte(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            sbyte val = 0;
            if (sbyte.TryParse(value.ToString(), out val))
                return val;

            return null;
        }

        public static DateTime? ToDateTime(this object value)
        {
            if (value == null || value == DBNull.Value)
                return null;

            DateTime val = DateTime.Now;
            if (DateTime.TryParse(value.ToString(), out val))
                return val;

            return null;
        }

        public static EContactSectors ToContactSectors(this int value)
        {
            switch (value)
            {
                case 0:
                    return EContactSectors.Naoinformado;
                case 1:
                    return EContactSectors.Compras;
                case 2:
                    return EContactSectors.Comercial;
                case 3:
                    return EContactSectors.Contabil;
                case 4:
                    return EContactSectors.Contas;
                case 5:
                    return EContactSectors.DepartamentoTecnico;
                case 6:
                    return EContactSectors.Engenharia;
                case 7:
                    return EContactSectors.Financeiro;
                case 8:
                    return EContactSectors.Fiscal;
                case 9:
                    return EContactSectors.Juridico;
                case 10:
                    return EContactSectors.Manutencao;
                case 11:
                    return EContactSectors.Produtivo;
                case 12:
                    return EContactSectors.Qualidade;
                case 13:
                    return EContactSectors.Sac;
                case 14:
                    return EContactSectors.Suporte; 
                case 15:
                    return EContactSectors.TI;
                case 16:
                    return EContactSectors.Vendas;
                case 17:
                    return EContactSectors.NFE;
                case 18:
                    return EContactSectors.Faturamento;
                case 19:
                    return EContactSectors.Cobranca;
                case 20:
                    return EContactSectors.Entrega;
                case 999:
                    return EContactSectors.Outros;
                default:
                    return 0;
            }
        }

        public static EntityState ToEntityState(this EModelDataState value)
        {
            switch (value)
            {
                case EModelDataState.Detached:
                    return EntityState.Detached;
                case EModelDataState.Unchanged:
                    return EntityState.Unchanged;
                case EModelDataState.Added:
                    return EntityState.Added;
                case EModelDataState.Deleted:
                    return EntityState.Deleted;
                case EModelDataState.Modified:
                    return EntityState.Modified;
                default:
                    return EntityState.Unchanged;
            }
        }

        public static UploadDestinationFolder ToUploadFolder(this string url)
        {
            if (url.ToLower().Contains("product"))
                return UploadDestinationFolder.Products;
            else if (url.ToLower().Contains("order/item"))
                return UploadDestinationFolder.OrderItem;
            else if (url.ToLower().Contains("proposal/item"))
                return UploadDestinationFolder.OrderItem;
            else if (url.ToLower().Contains("orders"))
                return UploadDestinationFolder.Order;
            else if (url.ToLower().Contains("proposals"))
                return UploadDestinationFolder.Order;
            else if (url.ToLower().Contains("enterprise"))
                return UploadDestinationFolder.Enterprises;
            else if (url.ToLower().Contains("user"))
                return UploadDestinationFolder.Users;
            else if (url.ToLower().Contains("establishments"))
                return UploadDestinationFolder.Establishments;

            return UploadDestinationFolder.NotIdentified;
        }
    }
}
