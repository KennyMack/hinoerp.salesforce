﻿using System.ComponentModel.DataAnnotations;

namespace Hino.Salesforce.Infra.Cross.Utils.Enums
{
    public enum EContactReceptivity
    {
        [Display(Description = "Não Informado")]
        NaoInformado = -1,

        [Display(Description = "Muito Baixa")]
        MuitoBaixa = 0,

        [Display(Description = "Baixa")]
        Baixa = 1,

        [Display(Description = "Média")]
        Media = 2,

        [Display(Description = "Alta")]
        Alta = 3,

        [Display(Description = "Muito Alta")]
        MuitoAlta = 4
    }

    public enum EContactSectors
    {
        [Display(Description = "Não informado")]
        Naoinformado = 0,

        [Display(Description = "Compras")]
        Compras = 1,

        [Display(Description = "Comercial")]
        Comercial = 2,

        [Display(Description = "Contábil")]
        Contabil = 3,

        [Display(Description = "Contas")]
        Contas = 4,

        [Display(Description = "Departamento técnico")]
        DepartamentoTecnico = 5,

        [Display(Description = "Engenharia")]
        Engenharia = 6,

        [Display(Description = "Financeiro")]
        Financeiro = 7,

        [Display(Description = "Fiscal")]
        Fiscal = 8,

        [Display(Description = "Jurídico")]
        Juridico = 9,

        [Display(Description = "Manutenção")]
        Manutencao = 10,

        [Display(Description = "Produtivo")]
        Produtivo = 11,

        [Display(Description = "Qualidade")]
        Qualidade = 12,

        [Display(Description = "Sac")]
        Sac = 13,

        [Display(Description = "Suporte")]
        Suporte = 14,

        [Display(Description = "TI")]
        TI = 15,

        [Display(Description = "Vendas")]
        Vendas = 16,

        [Display(Description = "NFE")]
        NFE = 17,

        [Display(Description = "Faturamento")]
        Faturamento = 18,

        [Display(Description = "Cobrança")]
        Cobranca = 19,

        [Display(Description = "Entrega")]
        Entrega = 20,

        [Display(Description = "Outros")]
        Outros = 999
    }
}
