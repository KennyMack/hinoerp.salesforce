﻿namespace Hino.Salesforce.Infra.Cross.Utils.Enums
{
    /// <summary>
    /// 0 - Pessoa Física 1 - Pessoa Jurídica
    /// </summary>
    public enum EEnterpriseType
    {
        PessoaFisica = 0,
        PessoaJuridica = 1
    }

    /// <summary>
    /// 0-Comercial 1-Entrega 2-Cobranca 3-Residencial
    /// </summary>
    public enum EAddressType
    {
        Commercial = 0,
        Delivery = 1,
        Levy = 2,
        Residential = 3
    }
}
