﻿using System.ComponentModel;

namespace Hino.Salesforce.Infra.Cross.Utils.Enums
{
    /// <summary>
    /// 0 - Emitente 1 - Destinatario 2 - Terceiros 3 - Próprio Emitente 4 Próprio Destinatário 9 - Sem frete
    /// </summary>
    public enum EFreightPaidBy
    {
        [Description("Contratação por conta do emitente (CIF)")]
        Emitente = 0,
        [Description("Contratação por conta do destinatário (FOB)")]
        Destinatario = 1,
        [Description("Contratação por conta de Terceiros")]
        Terceiros = 2,
        [Description("Transporte próprio por conta do emitente")]
        ProprioEmitente = 3,
        [Description("Transporte próprio por conta do destinatário")]
        ProprioDestinatario = 4,
        [Description("Sem ocorrência de transporte")]
        SemFrete = 9
    }
}
