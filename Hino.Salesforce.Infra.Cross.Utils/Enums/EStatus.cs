﻿using System.ComponentModel.DataAnnotations;

namespace Hino.Salesforce.Infra.Cross.Utils.Enums
{
    public enum EStatusEnterprise
    {
        [Display(Description = "Novo")]
        New = 0,
        [Display(Description = "Ativo")]
        Active = 1,
        [Display(Description = "Inativo")]
        Inactive = 2,
        [Display(Description = "Bloqueado")]
        Blocked = 3
    }
}
