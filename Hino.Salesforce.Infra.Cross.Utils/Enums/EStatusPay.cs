﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.Cross.Utils.Enums
{
    public enum EStatusPay
    {
        [Description("Pendente")]
        Pending = 0,
        [Description("Em processamento")]
        Processing = 1,
        [Description("Parcial")]
        Parcial = 2,
        [Description("Total")]
        Total = 3
    }
}
