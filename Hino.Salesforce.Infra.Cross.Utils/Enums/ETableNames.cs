﻿using System.ComponentModel;

namespace Hino.Salesforce.Infra.Cross.Utils.Enums
{
    public enum ETableNames
    {
        [Description("Refresh tokens")]
        AUREFRESHTOKEN,
        [Description("Tokens exprirados")]
        AUEXPIREDTOKEN,
        [Description("Aliquotas ICMS")]
        FSICMSSTALIQUF,
        [Description("Empresas")]
        GEENTERPRISES,
        [Description("Condição de pagamento")]
        GEPAYMENTCONDITION,
        [Description("Forma de pagamento")]
        GEPAYMENTTYPE,
        [Description("Cidades")]
        GECITIES,
        [Description("Estados")]
        GESTATES,
        [Description("Países")]
        GECOUNTRIES,
        [Description("Usuários")]
        GEUSERS,
        [Description("Dispositivos do estabelecimento")]
        GEESTABDEVICES,
        [Description("Estabelecimentos")]
        GEESTABLISHMENTS,
        [Description("Produtos")]
        GEPRODUCTS,
        [Description("Tabela de preço")]
        VESALEPRICE,
        [Description("Itens do pedido")]
        VEORDERITEMS,
        [Description("Pedidos")]
        VEORDERS,
        [Description("Regiões de venda")]
        VEREGIONSALE,
        [Description("Rotas")]
        LOROUTES,
        [Description("Paradas da rota")]
        LOWAYPOINTS,
        [Description("Viagens da rota")]
        LOTRIPS
    }
}
