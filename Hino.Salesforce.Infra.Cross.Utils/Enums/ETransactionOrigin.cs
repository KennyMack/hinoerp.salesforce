﻿namespace Hino.Salesforce.Infra.Cross.Utils.Enums
{
    public enum ETransactionOrigin
    {
        Tef = 1,
        Pos = 0
    }
}
