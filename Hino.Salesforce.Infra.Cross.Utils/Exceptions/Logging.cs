﻿using NLog;
using System;

namespace Hino.Salesforce.Infra.Cross.Utils.Exceptions
{
    public static class Logging
    {
        public static void Info(string pMessage) =>
            LogManager.GetLogger("Hino.Salesforce.API")?.Info(pMessage);

        public static void Exception(Exception pException) =>
            LogManager.GetLogger("Hino.Salesforce.API")?.Error(pException);
    }
}
