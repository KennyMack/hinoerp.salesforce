﻿using System;
using System.IO;

namespace Hino.Salesforce.Infra.Cross.Utils.Files
{
    public static class UploadFolder
    {
        #region Generate Destination folder
        public static string GenerateDestinationFolder(UploadDestinationFolder pDestination)
        {
            var retorno = "";
            try
            {
                var BaseDir = System.Web.HttpContext.Current.Server.MapPath("~/");
                if (!Directory.Exists(Path.Combine(BaseDir, "Uploads")))
                    Directory.CreateDirectory(Path.Combine(BaseDir, "Uploads"));

                var UploadDir = Path.Combine(Path.Combine(BaseDir, "Uploads"), pDestination.ToString());

                if (!Directory.Exists(UploadDir))
                    Directory.CreateDirectory(UploadDir);

                retorno = UploadDir;
            }
            catch (Exception)
            {
                retorno = "";
            }

            return retorno;
        }
        #endregion

        #region Remove File by path
        public static bool RemoveFileByPath(string pPathFile)
        {
            try
            {
                File.Delete(pPathFile);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }
        #endregion
    }
}
