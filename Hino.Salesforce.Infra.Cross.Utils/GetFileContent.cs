﻿using System;
using System.IO;

namespace Hino.Salesforce.Infra.Cross.Utils
{
    public static class GetFileContent
    {
        public static string[] GetFile(string pFile)
        {
            try
            {
                var path = System.Web.Hosting.HostingEnvironment.MapPath("~/bin");

                return File.ReadAllLines(string.Concat(path, "\\", pFile));
            }
            catch (Exception)
            {

            }

            return null;
        }


        public static string BasePath()
        {
            return System.Web.Hosting.HostingEnvironment.MapPath("~/bin");
        }
    }
}
