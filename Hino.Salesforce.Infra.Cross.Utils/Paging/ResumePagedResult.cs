﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.Cross.Utils.Paging
{
    public class ResumePagedResult<T> : BasePagedResult where T : class
    {
        public IList<T> Results { get; set; }
        public float TotalValue { get; set; }
        public float TotalQuantity { get; set; }
        public float UserLimit { get; set; }
        public float LimitUsed { get; set; }

        public ResumePagedResult()
        {
            Results = new List<T>();
        }
    }
}
