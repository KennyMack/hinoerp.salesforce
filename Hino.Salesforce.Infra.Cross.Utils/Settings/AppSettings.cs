﻿using System;
using System.Collections.Specialized;

namespace Hino.Salesforce.Infra.Cross.Utils.Settings
{
    public static class AppSettings
    {
        public static string SecurityKey { get; set; }
        public static string Hino_Id { get; set; }

        public static bool IsDebug { get; set; }
        public static bool Orders { get; set; }
        public static string Audience { get; set; }
        public static string Issuer { get; set; }

        public static string Seconds { get; set; }
        public static string FinalExpiration { get; set; }

        public static string Redis_Host { get; set; }
        public static string Redis_Port { get; set; }
        public static string Redis_Password { get; set; }
        public static string Redis_DatabaseID { get; set; }

        public static string EMAIL_SMTPHost { get; set; }
        public static string EMAIL_SMTPPort { get; set; }
        public static string EMAIL_SMTPUsername { get; set; }
        public static string EMAIL_SMTPPassword { get; set; }
        public static string EMAIL_UseSSL { get; set; }
        public static string EMAIL_LocalEmail { get; set; }

        public static string RABBIT_USER { get; set; }
        public static string RABBIT_PASS { get; set; }
        public static string RABBIT_URL { get; set; }
        public static string RABBIT_PORT { get; set; }

        public static string DEFAULTSCHEMA { get; set; }
        public static string ORCL_UserID { get; set; }
        public static string ORCL_Password { get; set; }
        public static string ORCL_DataSource { get; set; }

        public static string AWS_AccessKey { get; set; }
        public static string AWS_SecretAccess { get; set; }
        public static string AWS_Region { get; set; }
        public static string AWS_Bucket { get; set; }

        public static string PATH_TO_HINO_CONN { get; set; }
        public static string BaseURL { get; set; }
        public static string Email { get; set; }
        public static string Password { get; set; }

        #region Load Settings
        public static void LoadSettingsAPI(this NameValueCollection settings)
        {
            SecurityKey = settings["SecurityKey"];
            Hino_Id = settings["Hino_Id"];
            IsDebug = ConvertEx.ToBoolean(settings["IsDebug"]);

            Audience = settings["Audience"];
            Issuer = settings["Issuer"];

            Seconds = settings["Seconds"];
            FinalExpiration = settings["FinalExpiration"];

            Redis_Host = settings["Redis_Host"];
            Redis_Port = settings["Redis_Port"];
            Redis_Password = settings["Redis_Password"];
            Redis_DatabaseID = settings["Redis_DatabaseID"];

            EMAIL_SMTPHost = settings["EMAIL_SMTPHost"];
            EMAIL_SMTPPort = settings["EMAIL_SMTPPort"];
            EMAIL_SMTPUsername = settings["EMAIL_SMTPUsername"];
            EMAIL_SMTPPassword = settings["EMAIL_SMTPPassword"];
            EMAIL_UseSSL = settings["EMAIL_UseSSL"];
            EMAIL_LocalEmail = settings["EMAIL_LocalEmail"];

            RABBIT_USER = settings["RABBIT_USER"];
            RABBIT_PASS = settings["RABBIT_PASS"];
            RABBIT_URL = settings["RABBIT_URL"];
            RABBIT_PORT = settings["RABBIT_PORT"];

            DEFAULTSCHEMA = settings["DEFAULTSCHEMA"];
            ORCL_UserID = settings["ORCL_UserID"];
            ORCL_Password = settings["ORCL_Password"];
            ORCL_DataSource = settings["ORCL_DataSource"];


            AWS_AccessKey = settings["AWS_AccessKey"];
            AWS_SecretAccess = settings["AWS_SecretAccess"];
            AWS_Region = settings["AWS_Region"];
            AWS_Bucket = settings["AWS_Bucket"];
        }
        #endregion

        #region Load Settings Serviço de sincronia
        public static void LoadSettingsSync(this NameValueCollection settings)
        {
            IsDebug = ConvertEx.ToBoolean(settings["IsDebug"]);

            try
            {
                Orders = ConvertEx.ToBoolean(settings["Orders"]);
            }
            catch (Exception)
            {
                Orders = false;
            }
            PATH_TO_HINO_CONN = settings["PATH_TO_HINO_CONN"];
            BaseURL = settings["BASEURL"];
            Email = settings["EMAIL"];
            Password = settings["PASSWORD"];

            SecurityKey = settings["SecurityKey"];
            Hino_Id = settings["Hino_Id"];

            Audience = settings["Audience"];
            Issuer = settings["Issuer"];

            Seconds = settings["Seconds"];
            FinalExpiration = settings["FinalExpiration"];

            Redis_Host = settings["Redis_Host"];
            Redis_Port = settings["Redis_Port"];
            Redis_Password = settings["Redis_Password"];
            Redis_DatabaseID = settings["Redis_DatabaseID"];

            EMAIL_SMTPHost = settings["EMAIL_SMTPHost"];
            EMAIL_SMTPPort = settings["EMAIL_SMTPPort"];
            EMAIL_SMTPUsername = settings["EMAIL_SMTPUsername"];
            EMAIL_SMTPPassword = settings["EMAIL_SMTPPassword"];
            EMAIL_UseSSL = settings["EMAIL_UseSSL"];
            EMAIL_LocalEmail = settings["EMAIL_LocalEmail"];

            RABBIT_USER = settings["RABBIT_USER"];
            RABBIT_PASS = settings["RABBIT_PASS"];
            RABBIT_URL = settings["RABBIT_URL"];
            RABBIT_PORT = settings["RABBIT_PORT"];

            DEFAULTSCHEMA = settings["DEFAULTSCHEMA"];
            ORCL_UserID = settings["ORCL_UserID"];
            ORCL_Password = settings["ORCL_Password"];
            ORCL_DataSource = settings["ORCL_DataSource"];


            AWS_AccessKey = settings["AWS_AccessKey"];
            AWS_SecretAccess = settings["AWS_SecretAccess"];
            AWS_Region = settings["AWS_Region"];
            AWS_Bucket = settings["AWS_Bucket"];
        }
        #endregion
    }
}
