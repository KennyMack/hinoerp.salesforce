﻿using System;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;

namespace Hino.Salesforce.Infra.Cross.Utils
{
    public static class SqlDataOperator
    {

        #region Descrição do Metodo
        public static bool Like(string pValue, string pColValue) =>
            DbFunctions.Like(pColValue, $"%{pValue}%", "~");
        //SqlFunctions.PatIndex($"%{pValue}%", pColValue) > 0;
        #endregion

        public static Expression<Func<T, bool>> True<T>() { return f => true; }
        public static Expression<Func<T, bool>> False<T>() { return f => false; }

        public static Expression<Func<T, bool>> Or<T>(this Expression<Func<T, bool>> expr1,
                                                            Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>
                  (Expression.OrElse(expr1.Body, invokedExpr), expr1.Parameters);
        }

        public static Expression<Func<T, bool>> And<T>(this Expression<Func<T, bool>> expr1,
                                                             Expression<Func<T, bool>> expr2)
        {
            var invokedExpr = Expression.Invoke(expr2, expr1.Parameters.Cast<Expression>());
            return Expression.Lambda<Func<T, bool>>
                  (Expression.AndAlso(expr1.Body, invokedExpr), expr1.Parameters);
        }
    }


}
