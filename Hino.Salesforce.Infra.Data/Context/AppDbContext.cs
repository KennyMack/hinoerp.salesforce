﻿//using Hino.Salesforce.Domain.Auth.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Infra.Data.Context
{
    public class AppDbContext: DbContext
    {
        //public DbSet<GEUser> GEUSERS { get; set; }
        //public DbSet<GELogin> GELOGINS { get; set; }

        public AppDbContext(): base("name=AppDbContext")
        {
            Database.SetInitializer<AppDbContext>(null);
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("HINOERP_SALESFORCE");

            modelBuilder.Properties().Configure(c =>
            {                
                c.HasColumnName(c.ClrPropertyInfo.Name.ToUpper());
            });


            base.OnModelCreating(modelBuilder);
        }

        void DefaultData()
        {
            var props = ChangeTracker.Entries()
                             .Where(p => p.Entity.GetType().GetProperty("Created") != null);

            foreach (var item in props)
            {
                switch (item.State)
                {
                    case EntityState.Detached:
                        break;
                    case EntityState.Unchanged:
                        break;
                    case EntityState.Added:
                        item.Property("Created").CurrentValue = DateTime.Now;
                        item.Property("Modified").CurrentValue = DateTime.Now;
                        item.Property("IsActive").CurrentValue = true;
                        break;
                    case EntityState.Deleted:
                        item.Property("IsActive").CurrentValue = false;
                        item.State = EntityState.Modified;
                        break;
                    case EntityState.Modified:
                        item.Property("Created").IsModified = false;
                        item.Property("Modified").CurrentValue = DateTime.Now;
                        break;
                    default:
                        break;
                }
            }

        }

        public override Task<int> SaveChangesAsync()
        {
            DefaultData();

            return base.SaveChangesAsync();
        }

        public override int SaveChanges()
        {
            DefaultData();

            return base.SaveChanges();
        }

    }
}
