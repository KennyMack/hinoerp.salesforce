﻿using Hino.Salesforce.Infra.Cross.Entities.Fiscal;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Hino.Salesforce.Sync.Core.Configuration;
using Hino.Salesforce.Sync.Core.DataBase;
using Hino.Salesforce.Sync.Core.Utils;
using Microsoft.EntityFrameworkCore;
using Hino.Salesforce.Sync.Models.Views;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Data;

namespace Hino.Salesforce.Initial.Sync
{
    public class SaveToERP
    {
        private AppSettingsParameters AppSettings { get; }
        private readonly ERPContextFactory ERPContextFactory;
        private readonly DbERPContext ERPContext;

        public SaveToERP(AppSettingsParameters AppSettings, DbERPContext ERPContext)
        {
            this.ERPContext = ERPContext;
            ERPContextFactory = new ERPContextFactory();
            this.AppSettings = AppSettings;
        }

        #region Salvar formas de pagamento
        public async Task SaveToVEFormaPgtoAsync(GEPaymentType[] RegistersToSave)
        {
            using var cmd = ERPContext.Database.GetDbConnection().CreateCommand();
            var SqlIn = string.Join(',', RegistersToSave.Select(r => $"{r.IdERP}"));
            var SqlIdAPI = string.Join(',', RegistersToSave.Select(r => $"{r.IdERP}, {r.Id}"));
            var SqlUniqueKeyAPI = string.Join(',', RegistersToSave.Select(r => $"{r.IdERP}, '{r.UniqueKey}'"));

            var SqlUpdate = @$"UPDATE VEFORMAPGTO
                                  SET VEFORMAPGTO.IDAPI     = DECODE(VEFORMAPGTO.CODFORMAPGTO, {SqlIdAPI}, NULL),
                                      VEFORMAPGTO.UNIQUEKEY = DECODE(VEFORMAPGTO.CODFORMAPGTO, {SqlUniqueKeyAPI}, NULL)
                                WHERE VEFORMAPGTO.CODFORMAPGTO IN ({SqlIn})";

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = SqlUpdate;

            cmd.Connection.Open();
            await cmd.ExecuteNonQueryAsync();
            cmd.Connection.Close();
        }
        #endregion

        #region Salvar condição de pagamento
        public async Task SaveToVECondPgtoAsync(GEPaymentCondition[] RegistersToSave)
        {
            using var cmd = ERPContext.Database.GetDbConnection().CreateCommand();
            var Pages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(RegistersToSave.Length) / 240d));
            for (int i = 0; i < Pages; i++)
            {
                var SqlIn = string.Join(',', RegistersToSave.Skip(i * 240).Take(240).Select(r => $"{r.IdERP}"));
                var SqlIdAPI = string.Join(',', RegistersToSave.Skip(i * 240).Take(240).Select(r => $"{r.IdERP}, {r.Id}"));
                var SqlUniqueKeyAPI = string.Join(',', RegistersToSave.Skip(i * 240).Take(240).Select(r => $"{r.IdERP}, '{r.UniqueKey}'"));

                var SqlUpdate = @$"UPDATE VECONDPGTO
                                      SET VECONDPGTO.IDAPI     = DECODE(VECONDPGTO.CODCONDPGTO, {SqlIdAPI}, NULL),
                                          VECONDPGTO.UNIQUEKEY = DECODE(VECONDPGTO.CODCONDPGTO, {SqlUniqueKeyAPI}, NULL)
                                    WHERE VECONDPGTO.CODCONDPGTO IN ({SqlIn})";

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = SqlUpdate;

                cmd.Connection.Open();
                await cmd.ExecuteNonQueryAsync();
                cmd.Connection.Close();
            }
        }
        #endregion

        #region Salvar empresa
        public async Task SaveToGEEmpresaAsync(GEEnterprises[] RegistersToSave, int pCodEstab)
        {
            var exec = @"";
            // using var cmd = ERPContext.Database.GetDbConnection().CreateCommand();
            var Pages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(RegistersToSave.Length) / 240d));
            for (int i = 0; i < Pages; i++)
            {
                var SqlIn = string.Join(',', RegistersToSave.Skip(i * 240).Take(240).Select(r => $"{r.IdERP}"));
                var SqlIdAPI = string.Join(',', RegistersToSave.Skip(i * 240).Take(240).Select(r => $"{r.IdERP}, {r.Id}"));
                var SqlUniqueKeyAPI = string.Join(',', RegistersToSave.Skip(i * 240).Take(240).Select(r => $"{r.IdERP}, '{r.UniqueKey}'"));

                var SqlUpdate = @$"UPDATE GEEMPRESAPARAMESTAB
                                      SET GEEMPRESAPARAMESTAB.IDAPI      = DECODE(GEEMPRESAPARAMESTAB.CODEMPRESA, {SqlIdAPI}, NULL),
                                          GEEMPRESAPARAMESTAB.UNIQUEKEY  = DECODE(GEEMPRESAPARAMESTAB.CODEMPRESA, {SqlUniqueKeyAPI}, NULL)
                                    WHERE GEEMPRESAPARAMESTAB.CODEMPRESA IN ({SqlIn})
                                      AND GEEMPRESAPARAMESTAB.CODESTAB   = {pCodEstab};\r\n";

                exec += SqlUpdate;
                /*cmd.CommandType = CommandType.Text;
                cmd.CommandText = SqlUpdate;

                cmd.Connection.Open();
                await cmd.ExecuteNonQueryAsync();
                cmd.Connection.Close();*/
            }
            
        }
        #endregion

        #region Salvar regiões de venda
        public async Task SaveToVERegiaoPrecoAsync(VERegionSale[] RegistersToSave, int pCodEstab)
        {
            using var cmd = ERPContext.Database.GetDbConnection().CreateCommand();
            var SqlIn = string.Join(',', RegistersToSave.Select(r => $"{r.IdERP}"));
            var SqlIdAPI = string.Join(',', RegistersToSave.Select(r => $"{r.IdERP}, {r.Id}"));
            var SqlUniqueKeyAPI = string.Join(',', RegistersToSave.Select(r => $"{r.IdERP}, '{r.UniqueKey}'"));

            var SqlUpdate = @$"UPDATE VEREGIAOPRECO
                                  SET VEREGIAOPRECO.IDAPI     = DECODE(VEREGIAOPRECO.CODREGIAO, {SqlIdAPI}, NULL),
                                      VEREGIAOPRECO.UNIQUEKEY = DECODE(VEREGIAOPRECO.CODREGIAO, {SqlUniqueKeyAPI}, NULL)
                                WHERE VEREGIAOPRECO.CODREGIAO IN ({SqlIn})
                                  AND VEREGIAOPRECO.CODESTAB   = {pCodEstab}";

            cmd.CommandType = CommandType.Text;
            cmd.CommandText = SqlUpdate;

            cmd.Connection.Open();
            await cmd.ExecuteNonQueryAsync();
            cmd.Connection.Close();
        }
        #endregion

        #region Salvar Produto
        public async Task SaveToFSProdutoAsync(GEProducts[] RegistersToSave, int pCodEstab)
        {
            var Pages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(RegistersToSave.Length) / 240d));
            for (int i = 0; i < Pages; i++)
            {
                using var cmd = ERPContext.Database.GetDbConnection().CreateCommand();
                var SqlIn = string.Join(',', RegistersToSave.Skip(i * 240).Take(240).Select(r => $"'{r.ProductKey}'"));
                var SqlIdAPI = string.Join(',', RegistersToSave.Skip(i * 240).Take(240).Select(r => $"'{r.ProductKey}', {r.Id}"));
                var SqlUniqueKeyAPI = string.Join(',', RegistersToSave.Skip(i * 240).Take(240).Select(r => $"'{r.ProductKey}', '{r.UniqueKey}'"));

                var SqlUpdate = @$"UPDATE FSPRODUTOPARAMESTAB
                                      SET FSPRODUTOPARAMESTAB.IDAPI     = DECODE(CODPRODUTO, {SqlIdAPI}, NULL),
                                          FSPRODUTOPARAMESTAB.UNIQUEKEY = DECODE(CODPRODUTO, {SqlUniqueKeyAPI}, NULL)
                                    WHERE FSPRODUTOPARAMESTAB.CODPRODUTO IN ({SqlIn})
                                      AND FSPRODUTOPARAMESTAB.CODESTAB   = {pCodEstab}";

                cmd.CommandType = CommandType.Text;
                cmd.CommandText = SqlUpdate;

                cmd.Connection.Open();
                await cmd.ExecuteNonQueryAsync();

                cmd.CommandText = SqlUpdate.Replace("FSPRODUTOPARAMESTAB", "FSPRODUTOPCP");

                await cmd.ExecuteNonQueryAsync();
                cmd.Connection.Close();
            }
        }
        #endregion
        
        #region Salvar tabela de preço
        public async Task SaveToVEPrVendaProdAsync(VESalePrice[] RegistersToSave, int pCodEstab)
        {
            //var Pages = Convert.ToInt32(Math.Ceiling(Convert.ToDouble(RegistersToSave.Length) / 240d));
            //for (int i = 0; i < Pages; i++)
            //{
            //    using var cmd = ERPContext.Database.GetDbConnection().CreateCommand();
            //    var SqlIn = string.Join(',', RegistersToSave.Skip(i * 240).Take(240).Select(r => $"{r.IdERP}"));
            //    var SqlIdAPI = string.Join(',', RegistersToSave.Skip(i * 240).Take(240).Select(r => $"{r.IdERP}, {r.Id}"));
            //    var SqlUniqueKeyAPI = string.Join(',', RegistersToSave.Skip(i * 240).Take(240).Select(r => $"{r.IdERP}, '{r.UniqueKey}'"));
            //
            //    var SqlUpdate = @$"UPDATE VECONDPGTO
            //                          SET VECONDPGTO.IDAPI     = DECODE(VECONDPGTO.CODCONDPGTO, {SqlIdAPI}, NULL),
            //                              VECONDPGTO.UNIQUEKEY = DECODE(VECONDPGTO.CODCONDPGTO, {SqlUniqueKeyAPI}, NULL)
            //                        WHERE VECONDPGTO.CODCONDPGTO IN ({SqlIn})";
            //
            //    cmd.CommandType = CommandType.Text;
            //    cmd.CommandText = SqlUpdate;
            //
            //    cmd.Connection.Open();
            //    await cmd.ExecuteNonQueryAsync();
            //    cmd.Connection.Close();
            //}
        }
        #endregion
    }
}
