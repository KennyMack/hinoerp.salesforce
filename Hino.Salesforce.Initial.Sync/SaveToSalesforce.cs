﻿using Hino.Salesforce.Infra.Cross.Entities.Fiscal;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Hino.Salesforce.Sync.Core.Configuration;
using Hino.Salesforce.Sync.Core.DataBase;
using Hino.Salesforce.Sync.Core.Utils;
using Hino.Salesforce.Sync.Models.Views;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Initial.Sync
{
    public class SaveToSalesforce
    {
        private static readonly Regex EmailRegex = new(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$",
               RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex PhoneRegex = new(@"^(\(\d{2,3}\)\s\d{4,5}\-\d{4})$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        private AppSettingsParameters AppSettings { get; }
        private readonly SalesforceContextFactory SalesContextFactory;

        public SaveToSalesforce(AppSettingsParameters AppSettings)
        {
            SalesContextFactory = new SalesforceContextFactory();
            this.AppSettings = AppSettings;
        }

        #region Salvando categorias de empresa
        public async Task<GEEnterpriseCategory[]> SaveToCategoryAsync(GEVWEnterpriseCategory[] Categories, string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());
            var NewCategories = Categories.Select(r => new GEEnterpriseCategory
            {
                Id = 0,
                EstablishmentKey = EstablishmentKey,
                UniqueKey = Guid.NewGuid().ToString(),
                Description = r.Description,
                Identifier = r.Identifier.ToString(),
                IdERP = r.Identifier,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true
            });
            await SalesforceContext.GEEnterpriseCategory.AddRangeAsync(NewCategories);

            await SalesforceContext.SaveChangesAsync();

            return SalesforceContext.GEEnterpriseCategory
                        .AsNoTracking()
                        .Where(r => r.EstablishmentKey == EstablishmentKey).ToArray();
        }
        #endregion

        #region Salvando operação fiscal
        public async Task SaveToFiscalOperAsync(FSVWFiscalOper[] FiscalOperations, string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());
            var NewFiscalOperations = FiscalOperations.Select(r => new FSFiscalOper
            {
                Id = 0,
                EstablishmentKey = EstablishmentKey,
                UniqueKey = Guid.NewGuid().ToString(),
                Description = r.Description,
                IdERP = r.IDERP,
                IsSample = r.Amostra,
                IsBonification = r.Bonificacao,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true
            });
            await SalesforceContext.FSFiscalOper.AddRangeAsync(NewFiscalOperations);

            await SalesforceContext.SaveChangesAsync();
        }
        #endregion

        #region Salvando grupo de empresas
        public async Task<GEEnterpriseGroup[]> SaveToEnterpriseGroupAsync(GEVWEnterpriseGroup[] EnterpriseGroups, string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());
            var NewRegisters = EnterpriseGroups.ToList().Select((r, i) => new GEEnterpriseGroup
            {
                Id = 0,
                EstablishmentKey = EstablishmentKey,
                UniqueKey = Guid.NewGuid().ToString(),
                Description = r.Description,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true
            }).ToArray();
            await SalesforceContext.GEEnterpriseGroup.AddRangeAsync(NewRegisters);

            await SalesforceContext.SaveChangesAsync();

            return SalesforceContext.GEEnterpriseGroup
                        .AsNoTracking()
                        .Where(r => r.EstablishmentKey == EstablishmentKey).ToArray();
        }
        #endregion

        #region Salvando grupo fiscal de empresas
        public async Task<GEEnterpriseFiscalGroup[]> SaveToEnterpriseFiscalGroupAsync(GEVWEnterpriseFiscalGroup[] RegistersToSave, string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());
            var NewRegisters = RegistersToSave.Select(r => new GEEnterpriseFiscalGroup
            {
                Id = 0,
                EstablishmentKey = EstablishmentKey,
                UniqueKey = Guid.NewGuid().ToString(),
                Description = r.Description,
                IdERP = r.IDERP,
                Type = r.Type,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true
            });
            await SalesforceContext.GEEnterpriseFiscalGroup.AddRangeAsync(NewRegisters);

            await SalesforceContext.SaveChangesAsync();

            return SalesforceContext.GEEnterpriseFiscalGroup
                    .AsNoTracking()
                    .Where(r => r.EstablishmentKey == EstablishmentKey).ToArray();
        }
        #endregion

        #region Salvando forma de pagamento
        public async Task<GEPaymentType[]> SaveToPaymentTypeAsync(GEVWPaymentType[] RegistersToSave, string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());
            var NewRegisters = RegistersToSave.Select(r => new GEPaymentType
            {
                Id = 0,
                EstablishmentKey = EstablishmentKey,
                UniqueKey = Guid.NewGuid().ToString(),
                Description = r.Description,
                IdERP = r.IDERP,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true
            });
            await SalesforceContext.GEPaymentType.AddRangeAsync(NewRegisters.ToList());

            await SalesforceContext.SaveChangesAsync();

            return SalesforceContext.GEPaymentType
                .AsNoTracking()
                .Where(r => r.EstablishmentKey == EstablishmentKey).ToArray();
        }
        #endregion

        #region Salvando condição de pagamento
        public async Task<GEPaymentCondition[]> SaveToPaymentConditionAsync(GEVWPaymentCondition[] RegistersToSave, string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());
            var NewRegisters = RegistersToSave.Select(r => new GEPaymentCondition
            {
                Id = 0,
                TypePayID = r.TypePayID,
                EstablishmentKey = EstablishmentKey,
                UniqueKey = Guid.NewGuid().ToString(),
                Description = r.Description,
                IdERP = r.IDERP,
                PercDiscount = r.PercDiscount,
                PercIncrease = r.PercIncrease,
                Installments = r.Installments,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true
            });
            await SalesforceContext.GEPaymentCondition.AddRangeAsync(NewRegisters);

            await SalesforceContext.SaveChangesAsync();

            return SalesforceContext.GEPaymentCondition
                .AsNoTracking()
                .Where(r => r.EstablishmentKey == EstablishmentKey).ToArray();
        }
        #endregion

        #region Salvando parcelas da condição de pagamento
        public async Task SaveToPaymentCondInstallmentsAsync(GEVWPaymentCondInstallments[] RegistersToSave, string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());
            var NewRegisters = RegistersToSave.Select(r => new GEPaymentCondInstallments
            {
                Id = 0,
                CondPayId = r.CondPayID,
                Days = r.Days,
                Percent = r.Percent,
                EstablishmentKey = EstablishmentKey,
                UniqueKey = Guid.NewGuid().ToString(),
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true
            });
            await SalesforceContext.GEPaymentCondInstallments.AddRangeAsync(NewRegisters);

            await SalesforceContext.SaveChangesAsync();
        }
        #endregion

        #region Salvando empresas
        EStatusEnterprise GetEnterpriseStatus(int pType)
        {
            return pType switch
            {
                1 => EStatusEnterprise.Active,
                2 => EStatusEnterprise.Inactive,
                3 => EStatusEnterprise.Blocked,
                _ => EStatusEnterprise.New,
            };
        }
        EEnterpriseClassification GetEnterpriseClassification(int pType)
        {
            return pType switch
            {
                1 => EEnterpriseClassification.Prospect,
                2 => EEnterpriseClassification.Carrier,
                _ => EEnterpriseClassification.Client,
            };
        }
        public async Task<GEEnterprises[]> SaveToEnterprisesAsync(GEVWSincEmpresas[] RegistersToSave,
            GEEnterpriseFiscalGroup[] EnterpriseFiscalGroup,
            GEEnterpriseGroup[] EnterpriseGroup,
            GEEnterpriseCategory[] EnterpriseCategory,
            GEPaymentCondition[] PaymentConditions,
            VERegionSaleUF[] RegionSalesUF,
            string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());
            var NewRegisters = RegistersToSave.Select(r => new GEEnterprises
            {
                Id = 0,
                EstablishmentKey = EstablishmentKey,
                UniqueKey = Guid.NewGuid().ToString(),
                RazaoSocial = r.RazaoSocial.SubStr(0, 60),
                NomeFantasia = r.NomeFantasia.SubStr(0, 60),
                CNPJCPF = r.CpfCnpj,
                Type = r.TipoCli == 0 ? EEnterpriseType.PessoaFisica : EEnterpriseType.PessoaJuridica,
                StatusSinc = EStatusSinc.Integrated,
                IdERP = r.IDERP,
                RegionId = RegionSalesUF.FirstOrDefault(s => s.UF == r.UF).RegionId,
                PayConditionId = PaymentConditions.FirstOrDefault(s => s.IdERP == r.PayConditionID)?.Id,
                CategoryId = EnterpriseCategory.FirstOrDefault(s => s.IdERP == r.CategoryId)?.Id,
                GroupId = EnterpriseGroup.FirstOrDefault(s => s.IdERP == r.GroupId)?.Id,
                Classification = GetEnterpriseClassification(r.Classification),
                ClassifEmpresa = r.ClassifEmpresa,
                FiscalGroupId = EnterpriseFiscalGroup.FirstOrDefault(s => s.IdERP == r.FiscalGroupID)?.Id,
                IE = r.IE.IsEmpty() ? "N/INFO" : r.IE,
                Status = GetEnterpriseStatus(r.Status),
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true
            });
            await SalesforceContext.GEEnterprises.AddRangeAsync(NewRegisters);

            await SalesforceContext.SaveChangesAsync();

            return SalesforceContext.GEEnterprises
                .AsNoTracking()
                .Where(r => r.EstablishmentKey == EstablishmentKey).ToArray();
        }
        #endregion

        #region Salvando Endereço
        EAddressType GetAddressType(int pType)
        {
            return pType switch
            {
                1 => EAddressType.Delivery,
                2 => EAddressType.Levy,
                3 => EAddressType.Residential,
                _ => EAddressType.Commercial
            };
        }
        public async Task<GEEnterpriseGeo[]> SaveToEnterpriseGeoAsync(GEVWSincEmpresasGeo[] RegistersToSave, string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());
            var NewRegisters = RegistersToSave.Select(Address => new GEEnterpriseGeo
            {
                Id = 0,
                EstablishmentKey = EstablishmentKey,
                UniqueKey = Guid.NewGuid().ToString(),
                IE = Address.InscEstadual.IsEmpty() ? "N/INFO" : Address.InscEstadual,
                Type = GetAddressType(Address.Type),
                CNPJCPF = Address.CpfCnpj,
                RG = "",
                CellPhone = PhoneRegex.IsMatch(Address.CellPhone ?? "") ? Address.CellPhone : "",
                Phone = PhoneRegex.IsMatch(Address.Phone ?? "") ? Address.Phone : "",
                Email = EmailRegex.IsMatch(Address.Email ?? "") ? Address.Email : "",
                Address = Address.Endereco,
                Complement = Address.Complemento,
                District = Address.Bairro,
                Num = Address.Num.ToString(),
                ZipCode = Address.ZipCode,
                Site = Address.Site,
                CountryIni = "BR",
                CountryCode = "BR",
                CountryName = "BR",
                UF = Address.UF,
                StateName = Address.StateName,
                IBGE = Address.CodIBGE,
                CityName = Address.CityName,
                DisplayLat = Address.DisplayLat,
                DisplayLng = Address.DisplayLng,
                NavLat = Address.NavLat,
                NavLng = Address.NavLng,
                CodBacen = Address.CodBacen,
                InscSuframa = Address.InscSuframa,
                Enterpriseid = Address.EnterpriseID,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true
            });
            await SalesforceContext.GEEnterpriseGeo.AddRangeAsync(NewRegisters);

            await SalesforceContext.SaveChangesAsync();

            return SalesforceContext.GEEnterpriseGeo
                .AsNoTracking()
                .Where(r => r.EstablishmentKey == EstablishmentKey).ToArray();
        }
        #endregion

        #region Salvando unidade de medida
        public async Task<GEProductsUnit[]> SaveToProductUnitAsync(GEVWProductsUnit[] RegistersToSave, string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());
            var NewRegisters = RegistersToSave.Select(r => new GEProductsUnit
            {
                Id = 0,
                EstablishmentKey = EstablishmentKey,
                UniqueKey = Guid.NewGuid().ToString(),
                Description = r.Description,
                Unit = r.Unit,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true
            });
            await SalesforceContext.GEProductsUnit.AddRangeAsync(NewRegisters);

            await SalesforceContext.SaveChangesAsync();

            return SalesforceContext.GEProductsUnit
                .AsNoTracking()
                .Where(r => r.EstablishmentKey == EstablishmentKey).ToArray();
        }
        #endregion

        #region Salvando tipo do produto
        public async Task<GEProductsType[]> SaveToProductTypeAsync(GEVWProductsType[] RegistersToSave, string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());
            var NewRegisters = RegistersToSave.Select(r => new GEProductsType
            {
                Id = 0,
                EstablishmentKey = EstablishmentKey,
                UniqueKey = Guid.NewGuid().ToString(),
                Description = r.Description,
                TypeProd = r.TypeProd,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true
            });
            await SalesforceContext.GEProductsType.AddRangeAsync(NewRegisters);

            await SalesforceContext.SaveChangesAsync();

            return SalesforceContext.GEProductsType
                .AsNoTracking()
                .Where(r => r.EstablishmentKey == EstablishmentKey).ToArray();
        }
        #endregion

        #region Salvando familia do produto
        public async Task<GEProductsFamily[]> SaveToProductFamilyAsync(GEVWProductsFamily[] RegistersToSave, string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());
            var NewRegisters = RegistersToSave.Select(r => new GEProductsFamily
            {
                Id = 0,
                EstablishmentKey = EstablishmentKey,
                UniqueKey = Guid.NewGuid().ToString(),
                Family = r.Family,
                Description = r.Description,
                GroupDescription = r.GroupDescription,
                CatDescription = r.CatDescription,
                ClassDescription = r.ClassDescription,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true
            });
            await SalesforceContext.GEProductsFamily.AddRangeAsync(NewRegisters);

            await SalesforceContext.SaveChangesAsync();

            return SalesforceContext.GEProductsFamily
                .AsNoTracking()
                .Where(r => r.EstablishmentKey == EstablishmentKey).ToArray();
        }
        #endregion

        #region Salvando Aplicação do produto
        public async Task<GEProductAplic[]> SaveToProductAplicAsync(GEVWProductAplic[] RegistersToSave, string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());
            var NewRegisters = RegistersToSave.Select(r => new GEProductAplic
            {
                Id = 0,
                EstablishmentKey = EstablishmentKey,
                UniqueKey = Guid.NewGuid().ToString(),
                Description = r.Description,
                Classification = r.Classification,
                IdERP = r.IDERP,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true
            });
            await SalesforceContext.GEProductAplic.AddRangeAsync(NewRegisters);

            await SalesforceContext.SaveChangesAsync();

            return SalesforceContext.GEProductAplic
                .AsNoTracking()
                .Where(r => r.EstablishmentKey == EstablishmentKey).ToArray();
        }
        #endregion

        #region Salvando produto
        public async Task<GEProducts[]> SaveToProductscAsync(GEVWSincProduto[] RegistersToSave,
            GEProductsUnit[] UnitsMeassure,
            GEProductsType[] Types,
            GEProductsFamily[] Families,
            GEProductAplic[] ProdAplications,
            string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());

            var NCMsFilter = RegistersToSave.Select(r => r.NCM).ToArray();

            var NCMs = SalesforceContext.FSNCM.Where(r =>
                NCMsFilter.Contains(r.NCM)).ToArray();

            var NewRegisters = RegistersToSave.Select(result => {
                var ncm = NCMs.FirstOrDefault(r => r.NCM == result.NCM);
                return new GEProducts
                {
                    Id = 0,
                    EstablishmentKey = EstablishmentKey,
                    UniqueKey = Guid.NewGuid().ToString(),

                    ProductKey = result.CodProduto,
                    Image = result.Image,
                    Name = result.Name,
                    Description = result.Description,
                    TypeId = Types.FirstOrDefault(r => r.TypeProd == result.TypeProd &&
                        r.EstablishmentKey == EstablishmentKey).Id,
                    FamilyId = Families.FirstOrDefault(r => r.Family == result.Family &&
                        r.EstablishmentKey == EstablishmentKey).Id,
                    PercIPI = ncm?.IPI ?? 0,
                    PercMaxDiscount = Convert.ToDecimal(result.LimiteDesconto),
                    Value = Convert.ToDecimal(result.ValorUnitario),
                    StockBalance = Convert.ToDecimal(result.Saldo),
                    Status = result.Status,
                    NCM = result.NCM,
                    NCMId = ncm?.Id,
                    UnitId = UnitsMeassure.FirstOrDefault(r => r.Unit == result.CodUnidade &&
                        r.EstablishmentKey == EstablishmentKey)?.Id,
                    PackageQTD = result.QtdePorEmb,
                    Weight = result.PesoBruto,
                    SaleUnitId = UnitsMeassure.FirstOrDefault(r => r.Unit == result.CodUnidade &&
                        r.EstablishmentKey == EstablishmentKey)?.Id,
                    SaleFactor = result.FatorUmVenda,
                    CodOrigMerc = result.CodOrigMerc,
                    AplicationId = ProdAplications.FirstOrDefault(r => r.IdERP == result.AplicationId &&
                        r.EstablishmentKey == EstablishmentKey)?.Id,

                    Created = DateTime.Now,
                    Modified = DateTime.Now,
                    IsActive = true
                };
            });
            await SalesforceContext.GEProducts.AddRangeAsync(NewRegisters);

            await SalesforceContext.SaveChangesAsync();

            return SalesforceContext.GEProducts
                .AsNoTracking()
                .Where(r => r.EstablishmentKey == EstablishmentKey).ToArray();
        }
        #endregion

        #region Salvando região de venda
        public async Task<VERegionSale[]> SaveToRegionSaleAsync(VEVWSincRegiaoVenda[] RegistersToSave, string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());
            var NewRegisters = RegistersToSave.Select(r => new VERegionSale
            {
                Id = 0,
                EstablishmentKey = EstablishmentKey,
                UniqueKey = Guid.NewGuid().ToString(),
                Description = r.Descricao,
                IdERP = r.CodRegiao,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true
            });
            await SalesforceContext.VERegionSale.AddRangeAsync(NewRegisters);

            await SalesforceContext.SaveChangesAsync();

            return SalesforceContext.VERegionSale
                .AsNoTracking()
                .Where(r => r.EstablishmentKey == EstablishmentKey).ToArray();
        }
        #endregion

        #region Salvando estados da região de venda
        public async Task<VERegionSaleUF[]> SaveToRegionSaleUFAsync(
            VEVWRegionSaleUF[] RegistersToSave, string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());
            var NewRegisters = RegistersToSave.Select(r => new VERegionSaleUF
            {
                Id = 0,
                EstablishmentKey = EstablishmentKey,
                UniqueKey = Guid.NewGuid().ToString(),
                UF = r.UF,
                RegionId = r.RegionIdApi,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true
            });
            await SalesforceContext.VERegionSaleUF.AddRangeAsync(NewRegisters);

            await SalesforceContext.SaveChangesAsync();

            return SalesforceContext.VERegionSaleUF
                .AsNoTracking()
                .Where(r => r.EstablishmentKey == EstablishmentKey).ToArray();
        }
        #endregion

        #region Salvando preços de venda
        public async Task<VESalePrice[]> SaveToPriceTableAsync(
            VEVWSincTabPreco[] RegistersToSave,
            GEProducts[] Products,
            string EstablishmentKey)
        {
            using var SalesforceContext = SalesContextFactory.CreateDbContext(
                AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters());
            var NewRegisters = RegistersToSave.Select(r => new VESalePrice
            {
                Id = 0,
                EstablishmentKey = EstablishmentKey,
                UniqueKey = Guid.NewGuid().ToString(),
                ProductId = Products.FirstOrDefault(s => s.ProductKey == r.CodProduto &&
                        s.EstablishmentKey == EstablishmentKey).Id,
                ProductKey = r.CodProduto,
                CodPrVenda = r.CodPrvenda,
                Description = r.Descricao,
                RegionId = r.CodRegiaoApi,
                Value = r.ValorUnitario,
                Created = DateTime.Now,
                Modified = DateTime.Now,
                IsActive = true
            });
            await SalesforceContext.VESalePrice.AddRangeAsync(NewRegisters);

            await SalesforceContext.SaveChangesAsync();

            return SalesforceContext.VESalePrice
                .AsNoTracking()
                .Where(r => r.EstablishmentKey == EstablishmentKey).ToArray();
        }
        #endregion
    }
}
