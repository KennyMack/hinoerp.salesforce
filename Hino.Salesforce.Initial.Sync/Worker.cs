using Hino.Salesforce.Sync.Core.Configuration;
using Hino.Salesforce.Sync.Core.DataBase;
using Hino.Salesforce.Sync.Core.Logging;
using Hino.Salesforce.Sync.Models.General;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Hino.Salesforce.Initial.Sync
{
    public class Worker : BackgroundService
    {
        private IConfiguration Configuration { get; }
        private AppSettingsParameters AppSettings { get; }

        public Worker(IConfiguration config)
        {
            Configuration = config;
            AppSettings = Configuration.Get<AppSettingsParameters>();
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            while (!stoppingToken.IsCancellationRequested)
            {
                LogData.Info($"Iniciando processamento");

                LogData.Info($"{AppSettings.ServiceSettings.DataBases.Length} empresas encontradas para sincronizar");

                if (AppSettings.ServiceSettings.DataBases.Length > 0)
                {
                    LogData.Info($"Percorrendo empresas encontradas para sincronizar");
                    foreach (var ERPDatabase in AppSettings.ServiceSettings.DataBases)
                    {
                        LogData.Info($"Percorrendo empresa: {ERPDatabase.Description}");
                        var ListConfig = new List<GESincConfig>();
                        var ListEstab = new List<GEEstab>();
                        var ContextFactory = new ERPContextFactory();
                        LogData.Info($"Realizando conexao com o banco de dados do ERP ({ERPDatabase.Description}).");
                        using (var ERPContext = ContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
                        {
                            ListEstab = ERPContext.GEEstab.ToList();
                            ListConfig = ERPContext.GESincConfig.ToList();

                            ERPContext.Dispose();
                        }

                        if (ListConfig.Any())
                        {
                            foreach (var item in ListConfig)
                            {
                                var Estab = ListEstab.Where(r => r.codestab == item.codestab).FirstOrDefault();

                                LogData.Info($"Percorrendo: {Estab.codestab} - {Estab.razaosocial}");
                                LogData.Info($"Buscando altera��es no limite de cr�dito");

                                LogData.Info($"Realizando conexao com o banco de dados do ERP ({ERPDatabase.Description}).");
                                using (var ERPContext = ContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
                                {
                                    var SaveFrom = new SaveToERP(AppSettings, ERPContext);
                                    var SaveTo = new SaveToSalesforce(AppSettings);
                                    
                                    //LogData.Info($"Salvando categorias de empresas no portal");
                                    //var EnterpriseCategory = await SaveTo.SaveToCategoryAsync(ERPContext.GEVWEnterpriseCategory.AsNoTracking().ToArray(), item.establishmentkey);
                                    //
                                    //LogData.Info($"Salvando grupo de empresas no portal");
                                    //var EnterpriseGroup = await SaveTo.SaveToEnterpriseGroupAsync(ERPContext.GEVWEnterpriseGroup.AsNoTracking().ToArray(), item.establishmentkey);
                                    //
                                    //LogData.Info($"Salvando natureza de opera��o no portal");
                                    //await SaveTo.SaveToFiscalOperAsync(ERPContext.FSVWFiscalOper.AsNoTracking().ToArray(), item.establishmentkey);
                                    //
                                    //LogData.Info($"Salvando grupo fiscal no portal");
                                    //var EnterpriseFiscalGroup = await SaveTo.SaveToEnterpriseFiscalGroupAsync(ERPContext.GEVWEnterpriseFiscalGroup.AsNoTracking().ToArray(), item.establishmentkey);
                                    //
                                    //LogData.Info($"Salvando forma de pagamento no portal");
                                    //var PaymentTypes = await SaveTo.SaveToPaymentTypeAsync(ERPContext.GEVWPaymentType.AsNoTracking().ToArray(), item.establishmentkey);                                    
                                    //LogData.Info($"Atualizando a forma de pagamento no ERP");
                                    //await SaveFrom.SaveToVEFormaPgtoAsync(PaymentTypes);
                                    //
                                    //LogData.Info($"Salvando condi��o de pagamento no portal");
                                    //var PaymentConditions = await SaveTo.SaveToPaymentConditionAsync(ERPContext.GEVWPaymentCondition.AsNoTracking().ToArray(), item.establishmentkey);
                                    //LogData.Info($"Atualizando a condi��o de pagamento no ERP");
                                    //await SaveFrom.SaveToVECondPgtoAsync(PaymentConditions);
                                    //
                                    //LogData.Info($"Salvando as parcelas da condi��o de pagamento no portal");
                                    //await SaveTo.SaveToPaymentCondInstallmentsAsync(ERPContext.GEVWPaymentCondInstallments.AsNoTracking().ToArray(), item.establishmentkey);
                                    //
                                    //LogData.Info($"Salvando as parcelas da condi��o de pagamento no portal");
                                    //var RegionSales = await SaveTo.SaveToRegionSaleAsync(ERPContext.VEVWSincRegiaoVenda.Where(r => r.CodEstab == Estab.codestab).AsNoTracking().ToArray(), item.establishmentkey);
                                    //LogData.Info($"Atualizando a regi�o de venda no ERP");
                                    //await SaveFrom.SaveToVERegiaoPrecoAsync(RegionSales, Estab.codestab);
                                    //
                                    //LogData.Info($"Salvando os estados da regi�o de venda no portal");
                                    //var RegionSaleUF = await SaveTo.SaveToRegionSaleUFAsync(ERPContext.VEVWRegionSaleUF.Where(r => r.CodEstab == Estab.codestab).AsNoTracking().ToArray(), item.establishmentkey);
                                    //
                                    //LogData.Info($"Salvando as empresas no portal");
                                    //var Enterprises = await SaveTo.SaveToEnterprisesAsync(
                                    //    ERPContext.GEVWSincEmpresas.Where(r => r.CodEstab == Estab.codestab).AsNoTracking().ToArray(),
                                    //    EnterpriseFiscalGroup, EnterpriseGroup, EnterpriseCategory,
                                    //    PaymentConditions, RegionSaleUF, item.establishmentkey);
                                    //
                                    //try
                                    //{
                                    //    LogData.Info($"Atualizando as empresas no ERP");
                                    //    await SaveFrom.SaveToGEEmpresaAsync(Enterprises, Estab.codestab);
                                    //}
                                    //catch (Exception ex)
                                    //{
                                    //}

                                    // LogData.Info($"Salvando os endere�os no portal");
                                    // await SaveTo.SaveToEnterpriseGeoAsync(ERPContext.GEVWSincEmpresasGeo.Where(r => r.CodEstab == Estab.codestab).AsNoTracking().ToArray(), item.establishmentkey);
                                    
                                    // var UnitsOfMeasure = await SaveTo.SaveToProductUnitAsync(ERPContext.GEVWProductsUnit.AsNoTracking().ToArray(), item.establishmentkey);
                                    // var ProductTypes = await SaveTo.SaveToProductTypeAsync(ERPContext.GEVWProductsType.AsNoTracking().ToArray(), item.establishmentkey);
                                    // var ProductFamilies = await SaveTo.SaveToProductFamilyAsync(ERPContext.GEVWProductsFamily.AsNoTracking().ToArray(), item.establishmentkey);
                                    // var ProductAplications = await SaveTo.SaveToProductAplicAsync(ERPContext.GEVWProductAplic.AsNoTracking().ToArray(), item.establishmentkey);
                                    // 
                                    // var ProductsSalesforce = await SaveTo.SaveToProductscAsync(
                                    //     ERPContext.GEVWSincProduto.Where(r => r.CodEstab == Estab.codestab).AsNoTracking().ToArray(),
                                    //     UnitsOfMeasure, ProductTypes, ProductFamilies,
                                    //     ProductAplications,
                                    //     item.establishmentkey);
                                    // 
                                    // await SaveFrom.SaveToFSProdutoAsync(ProductsSalesforce, Estab.codestab);

                                    //var PriceTables = await SaveTo.SaveToPriceTableAsync(ERPContext.VEVWSincTabPreco.Where(r => r.CodEstab == Estab.codestab).AsNoTracking().ToArray(), ProductsSalesforce, item.establishmentkey);
                                    //await SaveFrom.SaveToVEPrVendaProdAsync(PriceTables, Estab.codestab);


                                }
                            }
                        }
                    }
                    LogData.Info($"Finalizando as empresas a sincronizar");
                }

                LogData.Info($"Finalizando processamento");
                LogData.Info($"  ");
                await Task.Delay(AppSettings.Interval, stoppingToken);
            }
        }
    }
}
