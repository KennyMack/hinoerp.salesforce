﻿using Hino.Salesforce.Sync.Core.Configuration;
using Hino.Salesforce.Sync.Core.DataBase;
using Hino.Salesforce.Sync.Core.Logging;
using Hino.Salesforce.Sync.Core.Messages;
using Hino.Salesforce.Sync.Core.Utils;
using Hino.Salesforce.Sync.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Hino.Salesforce.Sync.Models;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Sync.Models.Views;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Sync.Models.General;

namespace Hino.Salesforce.Orders.Sync
{
    public class Download
    {
        private AppSettingsParameters AppSettings { get; }

        public Download(AppSettingsParameters config)
        {
            AppSettings = config;
        }

        public async Task<GEQueueItem> ProcessMessageAsync(MessageReceivedEventArgs messageToProcess)
        {
            if (!messageToProcess.EntryName.Equals(Constants.VEOrders, StringComparison.OrdinalIgnoreCase) &&
                !messageToProcess.EntryName.Equals(Constants.VETransactions, StringComparison.OrdinalIgnoreCase) &&
                !messageToProcess.EntryName.Equals(Constants.VESalePrice, StringComparison.OrdinalIgnoreCase))
            {
                LogData.Info($"EntryName inválido: {messageToProcess.EntryName}");
                return null;
            }

            return await new QueueService(AppSettings)
                    .GenerateQueueByMessageAsync(messageToProcess);
        }

        public async Task<GEQueueItem> ProcessTransactionOrderToERPAsync(GEQueueItem QueueItem)
        {
            LogData.Info(" ");
            LogData.Info($"Iniciando o processando da transação {QueueItem.IdReference}.");
            var OrderService = new OrderService(AppSettings);

            if (QueueItem.Type.Equals("NEW", StringComparison.OrdinalIgnoreCase))
            {
                var SalesforceContextFactory = new SalesforceContextFactory();

                using (var SalesforceContext = SalesforceContextFactory.CreateDbContext(AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters()))
                {
                    LogData.Info("Buscando dados da transação.");
                    var Transaction = SalesforceContext
                         .VETransactions
                         .Include(order => order.VEOrders)
                         .AsNoTracking()
                         .FirstOrDefault(r =>
                         r.Id == QueueItem.IdReference &&
                         r.EstablishmentKey == QueueItem.EstablishmentKey);

                    LogData.Info($"Transação vinculada ao pedido: {Transaction.OrderID}.");
                    QueueItem.IdReference = Transaction.OrderID;
                    QueueItem.UniqueKey = Transaction.VEOrders.UniqueKey;

                    LogData.Info($"Processando a transação no pedido: {Transaction.OrderID}.");
                    await ProcessOrderToERPAsync(QueueItem);
                }
            }
            else if (QueueItem.Type.Equals("REMOVE", StringComparison.OrdinalIgnoreCase))
            {
                LogData.Info($"Processando a exclusão da transação: {QueueItem.IdReference}.");
                await OrderService.RemoveTransactionFromERPAsync(QueueItem);
            }

            var Queue = new QueueService(AppSettings);
            QueueItem = await Queue.SaveChangesQueueAsync(QueueItem);

            LogData.Info($"Finalizando o processamento da transação {QueueItem.IdReference}.");
            LogData.Info(" ");

            return QueueItem;
        }

        public async Task<GEQueueItem> ProcessOrderToERPAsync(GEQueueItem QueueItem)
        {
            LogData.Info(" ");
            LogData.Info($"Iniciando o processando do pedido {QueueItem.IdReference}.");
            var OrderService = new OrderService(AppSettings);

            if (QueueItem.Type.Equals("NEW", StringComparison.OrdinalIgnoreCase))
            {
                var Order = await OrderService.SaveToERPCROrdersAsync(QueueItem);
                if (Order == null)
                    LogData.Info("Não foi possivel salvar os dados do pedido na CROrders");
                else
                {
                    var CodPedVenda = await OrderService.ProcessOrderAsync(QueueItem, Order);
                    await OrderService.UpdateSalesforceOrderIDERPAsync(QueueItem, CodPedVenda, Order);
                }
            }

            var Queue = new QueueService(AppSettings);
            QueueItem = await Queue.SaveChangesQueueAsync(QueueItem);

            LogData.Info($"Finalizando o processamento do pedido {QueueItem.IdReference}.");
            LogData.Info(" ");

            return QueueItem;
        }

        public async Task<GEQueueItem> ProcessSalePriceToERPAsync(GEQueueItem QueueItem)
        {
            LogData.Info(" ");
            LogData.Info($"Iniciando o processando da  tabela de preço {QueueItem.IdReference}.");
            var OrderService = new OrderService(AppSettings);

            if (QueueItem.Type.Equals("NEW", StringComparison.OrdinalIgnoreCase))
            {
                await OrderService.SaveToERPCRSalePriceAsync(QueueItem);
            }

            var Queue = new QueueService(AppSettings);
            QueueItem = await Queue.SaveChangesQueueAsync(QueueItem);

            LogData.Info($"Finalizando o processamento da tabela de preço {QueueItem.IdReference}.");
            LogData.Info(" ");

            return QueueItem;
        }
    }
}
