﻿using Hino.Salesforce.Sync.Core.Configuration;
using Hino.Salesforce.Sync.Models;
using Hino.Salesforce.Sync.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Hino.Salesforce.Sync.Core.Services;
using Hino.Salesforce.Sync.Models.Vendas;
using Hino.Salesforce.Sync.Core.DataBase;
using Microsoft.EntityFrameworkCore;

namespace Hino.Salesforce.Orders.Sync
{
    public class Upload
    {
        private AppSettingsParameters AppSettings { get; }

        public Upload(AppSettingsParameters config)
        {
            AppSettings = config;
        }

        public async Task SincTypeSaleAsync(string pEstablishmentKey) =>
            await new OrderService(AppSettings).SincTypeSaleAsync(pEstablishmentKey);

        public async Task UploadDataToAPIBySyncItemAsync(ISincData pItem, string pEstablishmentKey, int pCodEstab, string EntryName)
        {
            var result = false;
            var OrderService = new OrderService(AppSettings);

            switch (EntryName.ToUpper())
            {
                case Constants.VESincPrVenda:
                    result = await OrderService.UploadVESincPrVendaAsync((VESincPrVenda)pItem, pEstablishmentKey, pCodEstab);
                    break;
                case Constants.VEPedidoSinc:
                    result = await OrderService.UploadVEPedidoSincAsync((VEPedidoSinc)pItem, pEstablishmentKey, pCodEstab);
                    break;
                case Constants.VESincTransacoes:
                    result = await OrderService.UploadVESincTransacoesAsync((VESincTransacoes)pItem, pEstablishmentKey, pCodEstab);
                    break;
            }

            if (result && pItem.CodSinc > 0)
            {
                pItem.Sincronizado = true;
                pItem.DataSinc = DateTime.Now;

                var ERPContextFactory = new ERPContextFactory();
                var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(pEstablishmentKey);

                using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
                {
                    ERPContext.Entry(pItem).State = EntityState.Detached;
                    ERPContext.Entry(pItem).State = EntityState.Modified;
                    await ERPContext.SaveChangesAsync();
                }
            }
        }
    }
}
