using Hino.Salesforce.Sync.Core.Configuration;
using Hino.Salesforce.Sync.Core.DataBase;
using Hino.Salesforce.Sync.Core.Logging;
using Hino.Salesforce.Sync.Core.Messages;
using Hino.Salesforce.Sync.Core.Utils;
using Hino.Salesforce.Sync.Models;
using Hino.Salesforce.Sync.Models.General;
using Hino.Salesforce.Sync.Models.Interfaces;
using Hino.Salesforce.Sync.Models.Vendas;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Oracle.ManagedDataAccess.Client;
using Polly;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Hino.Salesforce.Orders.Sync
{
    public class Worker : BackgroundService
    {
        private RabbitMQAdapter SalePriceRabbitAdapter;
        private RabbitMQAdapter OrderRabbitAdapter;
        private IConfiguration Configuration { get; }
        private AppSettingsParameters AppSettings { get; }

        public Worker(IConfiguration config)
        {
            Configuration = config;
            AppSettings = Configuration.Get<AppSettingsParameters>();
        }

        public override async Task StartAsync(CancellationToken cancellationToken)
        {
            // DO YOUR STUFF HERE
            await base.StartAsync(cancellationToken);
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            // DO YOUR STUFF HERE
            await base.StopAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            LogData.Info("Conectando na lista Orders");

            OrderRabbitAdapter = new RabbitMQAdapter(
                "Orders",
                "Orders",
                AppSettings.ServiceSettings.RabbitMQ
            );
            OrderRabbitAdapter.MessageReceived += AdapterRabbit_MessageReceived;
            OrderRabbitAdapter.StartConsuming();

            LogData.Info("Conectando na lista SalePrices");

            SalePriceRabbitAdapter = new RabbitMQAdapter(
                "SalePrice",
                "SalePrice",
                AppSettings.ServiceSettings.RabbitMQ
            );
            SalePriceRabbitAdapter.MessageReceived += SalePriceRabbitAdapter_MessageReceived;
            SalePriceRabbitAdapter.StartConsuming();

            while (!stoppingToken.IsCancellationRequested)
            {
                LogData.Info($"Iniciando processamento");

                LogData.Info($"{AppSettings.ServiceSettings.DataBases.Length} empresas encontradas para sincronizar");
                
                if (AppSettings.ServiceSettings.DataBases.Length > 0)
                {
                    LogData.Info($"Percorrendo empresas encontradas para sincronizar");

                    foreach (var ERPDatabase in AppSettings.ServiceSettings.DataBases)
                    {
                        LogData.Info($"Percorrendo empresa: {ERPDatabase.Description}");

                        var ListQueueItem = new List<GEQueueItem>();
                        var ContextFactory = new ERPContextFactory();
                        VESincPrVenda[] ListSincPrVenda;
                        VESincTransacoes[] ListSincTransacoes;
                        VEPedidoSinc[] ListPedidoSinc;

                        LogData.Info($"Realizando conexao com o banco de dados do ERP ({ERPDatabase.Description}).");
                        
                        using (var ERPContext = ContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
                        {
                            ListQueueItem = ERPContext.GEQueueItem.Where(r => 
                                !r.Processed &&
                                r.EntryName.ToUpper() == Constants.VEOrders).ToList();
                            LogData.Info($"Itens na fila para sincronizar {ListQueueItem.Count}.");

                            LogData.Info($"Percorrendo itens para sincronizar");
                            foreach (var item in ListQueueItem)
                                ProcessMessage(item);

                            LogData.Info($"Verificando se h� dados para upload.");
                            ListPedidoSinc = ERPContext.VEPedidoSinc.Where(r => 
                                !r.Sincronizado && 
                                r.CodEstab == ERPDatabase.CodEstab).ToArray();
                            ListSincPrVenda = ERPContext.VESincPrVenda.Where(r => 
                                !r.Sincronizado &&
                                r.CodEstab == ERPDatabase.CodEstab)
                                .Include(s => s.FSProdutoParamEstab).ToArray();
                            ListSincTransacoes = ERPContext.VESincTransacoes.Where(r => !r.Sincronizado).ToArray();
                        }

                        LogData.Info($"Sincronizando itens da tabela VESINCPRVENDA.");
                        LogData.Info($"Listas de pre�os para enviar ao portal: {ListSincPrVenda?.Length}");
                        foreach (var item in ListSincPrVenda)
                            await ProcessItemToUploadAsync(item, ERPDatabase.Name, ERPDatabase.CodEstab, Constants.VESincPrVenda);

                        LogData.Info($"Sincronizando itens da tabela VEPEDIDOSINC.");
                        LogData.Info($"Pedidos para enviar ao portal: {ListPedidoSinc?.Length}");
                        if (ListPedidoSinc?.Length > 0)
                            await ProcessSincTipoVendaAsync(ERPDatabase.Name);
                        foreach (var item in ListPedidoSinc)
                            await ProcessItemToUploadAsync(item, ERPDatabase.Name, ERPDatabase.CodEstab, Constants.VEPedidoSinc);

                        LogData.Info($"Sincronizando itens da tabela VESINCTRANSACOES.");
                        LogData.Info($"Transa��es para enviar ao portal: {ListSincTransacoes?.Length}");
                        foreach (var item in ListSincTransacoes)
                            await ProcessItemToUploadAsync(item, ERPDatabase.Name, ERPDatabase.CodEstab, Constants.VESincTransacoes);
                        
                    }

                    LogData.Info($"Finalizando as empresas a sincronizar");
                }

                LogData.Info($"Finalizando processamento");
                LogData.Info($"  ");
                await Task.Delay(AppSettings.Interval, stoppingToken);
            }
        }

        async Task ProcessSincTipoVendaAsync(string pEstablishmentKey) =>
            await new Upload(AppSettings).SincTypeSaleAsync(pEstablishmentKey);

        async Task ProcessItemToUploadAsync(ISincData pItem, string pEstablishmentKey, int pCodEstab, string EntryName) =>
            await new Upload(AppSettings).UploadDataToAPIBySyncItemAsync(pItem, pEstablishmentKey, pCodEstab, EntryName);

        void ProcessMessage(GEQueueItem QueueItem)
        {
            var policy = Policy.Handle<Exception>().Or<OracleException>()
                   .WaitAndRetry(3, op => TimeSpan.FromSeconds(Math.Pow(2, op)), (ex, time) =>
                   {
                       LogData.Error("N�o foi possivel processar a mensagem", ex);

                   });
            policy.Execute(() =>
            {
                var DownloadInfo = new Download(AppSettings);
                Task.Run(async () => {
                    if (QueueItem.EntryName.Equals(Constants.VETransactions, StringComparison.OrdinalIgnoreCase))
                        await DownloadInfo.ProcessTransactionOrderToERPAsync(QueueItem);
                    else if (QueueItem.EntryName.Equals(Constants.VESalePrice, StringComparison.OrdinalIgnoreCase))
                        await DownloadInfo.ProcessSalePriceToERPAsync(QueueItem);
                    else if (QueueItem.EntryName.Equals(Constants.VEOrders, StringComparison.OrdinalIgnoreCase))
                        await DownloadInfo.ProcessOrderToERPAsync(QueueItem);
                });
            });
        }

        void MoveToInvalids(RabbitMQAdapter adapter, string originQueue, MessageReceivedEventArgs args)
        {
            LogData.Info($"Mensagem inv�lida Entry Name: {args.EntryName} movendo para fila de inv�lidas.");

            args.ProcessName = "Hino.Salesforce.Orders.Sync";
            adapter.BasicPublish("InvalidOrders", "InvalidOrders", "InvalidOrders", args);
            LogData.Info($"Mensagem movida com sucesso.");

            LogData.Info($"Removendo mensagem da fila de {originQueue}.");
            adapter.AckMessage(args.DeliveryTag);
            LogData.Info($"Mensagem removida da fila de {originQueue} com sucesso.");

        }

        private async void SalePriceRabbitAdapter_MessageReceived(MessageReceivedEventArgs args)
        {
            if (!args.EntryName.Equals(Constants.VESalePrice, StringComparison.OrdinalIgnoreCase))
            {
                MoveToInvalids(SalePriceRabbitAdapter, "SalePrice", args);
                /*
                LogData.Info($"Mensagem inv�lida Entry Name: {args.EntryName} movendo para fila de inv�lidas.");

                args.ProcessName = "Hino.Salesforce.Orders.Sync";
                SalePriceRabbitAdapter.BasicPublish("InvalidOrders", "InvalidOrders", "InvalidOrders", args);
                LogData.Info($"Mensagem movida com sucesso.");

                LogData.Info($"Removendo mensagem da fila de SalePrice.");
                SalePriceRabbitAdapter.AckMessage(args.DeliveryTag);
                LogData.Info($"Mensagem removida da fila de SalePrice com sucesso.");
                */
                return;
            }

            LogData.Info($"Processando a mensagem {args.EntryName}");
            var DownloadInfo = new Download(AppSettings);
            GEQueueItem QueueItem;
            try
            {
                QueueItem = await DownloadInfo.ProcessMessageAsync(args);
            }
            catch (Exception ex)
            {
                LogData.Error("Falha ao processar a mensagem", ex);
                QueueItem = null;
            }

            if (QueueItem == null)
            {
                MoveToInvalids(SalePriceRabbitAdapter, "SalePrice", args);
                // SalePriceRabbitAdapter.NackMessage(args.DeliveryTag);
                return;
            }

            LogData.Info("Confirmando o recebimento da mensagem");
            SalePriceRabbitAdapter.AckMessage(args.DeliveryTag);

            ProcessMessage(QueueItem);
        }

        private async void AdapterRabbit_MessageReceived(MessageReceivedEventArgs args)
        {
            if (!args.EntryName.Equals(Constants.VEOrders, StringComparison.OrdinalIgnoreCase) &&
                !args.EntryName.Equals(Constants.VETransactions, StringComparison.OrdinalIgnoreCase))
            {
                MoveToInvalids(OrderRabbitAdapter, "Orders", args);
                /*
                LogData.Info($"Mensagem inv�lida Entry Name: {args.EntryName} movendo para fila de inv�lidas.");

                args.ProcessName = "Hino.Salesforce.Orders.Sync";
                OrderRabbitAdapter.BasicPublish("InvalidOrders", "InvalidOrders", "InvalidOrders", args);
                LogData.Info($"Mensagem movida com sucesso.");

                LogData.Info($"Removendo mensagem da fila de Orders.");
                OrderRabbitAdapter.AckMessage(args.DeliveryTag);
                LogData.Info($"Mensagem removida da fila de orders com sucesso.");
                */
                return;
            }

            LogData.Info($"Processando a mensagem {args.EntryName}");
            var DownloadInfo = new Download(AppSettings);
            GEQueueItem QueueItem;
            try
            {
                QueueItem = await DownloadInfo.ProcessMessageAsync(args);
            }
            catch (Exception ex)
            {
                LogData.Error("Falha ao processar a mensagem", ex);
                QueueItem = null;
            }

            if (QueueItem == null)
            {
                MoveToInvalids(OrderRabbitAdapter, "Orders", args);
                // OrderRabbitAdapter.NackMessage(args.DeliveryTag);
                return;
            }

            LogData.Info("Confirmando o recebimento da mensagem");
            OrderRabbitAdapter.AckMessage(args.DeliveryTag);
            
            ProcessMessage(QueueItem);
        }
    }
}
