﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Core.Configuration
{
    public class AppSettingsParameters
    {
        public string InvoicePath { get; set; }
        public string BaseURL { get; set; }
        public int Interval { get; set; }
        public ServiceSettings ServiceSettings { get; set; }
    }
}
