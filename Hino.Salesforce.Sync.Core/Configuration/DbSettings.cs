﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Core.Configuration
{
    public class DbSettings
    {
        public int CodEstab { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string DataSource { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public string[] GetParameters()
        {
            return new[] {
                Description,
                DataSource,
                UserName,
                Password
            };
        }
    }
}
