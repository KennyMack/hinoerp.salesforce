﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Core.Configuration
{
    public class ServiceSettings
    {
        public RabbitMQSettings RabbitMQ { get; set; }
        public DbSettings DataBaseSalesforce { get; set; }
        public DbSettings[] DataBases { get; set; }
        public DbSettings GetDatabaseByEstablishmentKey(string pEstablishmentKey) =>
            DataBases.FirstOrDefault(r => r.Name == pEstablishmentKey);
        public string EstablishmentKey { get; set; }
    }
}
