﻿using Hino.Salesforce.Sync.Models;
using Hino.Salesforce.Sync.Models.Fiscal;
using Hino.Salesforce.Sync.Models.General;
using Hino.Salesforce.Sync.Models.Vendas;
using Hino.Salesforce.Sync.Models.Views;
using Microsoft.EntityFrameworkCore;
using Oracle.ManagedDataAccess.Client;
using System.Linq;

namespace Hino.Salesforce.Sync.Core.DataBase
{
    public class DbERPContext: DbContext
    {
        private OracleConnectionStringBuilder OracleConnectionString { get; }

        public virtual DbSet<GESincLimiteCredito> GESincLimiteCredito { get; set; }
        public virtual DbSet<GESincConfig> GESincConfig { get; set; }
        public virtual DbSet<GEEstab> GEEstab { get; set; }
        public virtual DbSet<GEQueueItem> GEQueueItem { get;  set; }
        public virtual DbSet<CRGrupo> CRGrupo { get; set; }
        public virtual DbSet<CRCategoria> CRCategoria { get; set; }
        public virtual DbSet<VESincPrVenda> VESincPrVenda { get; set; }
        public virtual DbSet<FSSincProduto> FSSincProdutos { get; set; }
        public virtual DbSet<VEPedidoSinc> VEPedidoSinc { get; set; }
        public virtual DbSet<CRTransactions> CRTransactions { get; set; }
        public virtual DbSet<VESincTransacoes> VESincTransacoes { get; set; }
        public virtual DbSet<CRSalePrice> CRSalePrice { get; set; }

        public virtual DbSet<CRProducts> CRProducts { get; set; }
        public virtual DbSet<CROrders> CROrders { get; set; }
        public virtual DbSet<CROrderItems> CROrderItems { get; set; }
        public virtual DbSet<CROrderFiles> CROrderFiles { get; set; }
        public virtual DbSet<CREnterprises> CREnterprises { get; set; }
        public virtual DbSet<CREnterpriseGeo> CREnterpriseGeo { get; set; }
        public virtual DbSet<CREnterpriseContacts> CREnterpriseContacts { get; set; }
        public virtual DbSet<GEVWSincLimiteCredito> GEVWSincLimiteCredito { get; set; }
        public virtual DbSet<GEVWEnterpriseCategory> GEVWEnterpriseCategory { get; set; }
        public virtual DbSet<GEVWEnterpriseGroup> GEVWEnterpriseGroup { get; set; }
        public virtual DbSet<FSVWFiscalOper> FSVWFiscalOper { get; set; }
        public virtual DbSet<GEVWEnterpriseFiscalGroup> GEVWEnterpriseFiscalGroup { get; set; }
        public virtual DbSet<GEVWPaymentType> GEVWPaymentType { get; set; }
        public virtual DbSet<GEVWPaymentCondition> GEVWPaymentCondition { get; set; }
        public virtual DbSet<GEVWPaymentCondInstallments> GEVWPaymentCondInstallments { get; set; }
        public virtual DbSet<GEVWSincEmpresas> GEVWSincEmpresas { get; set; }
        public virtual DbSet<GEVWSincEmpresasGeo> GEVWSincEmpresasGeo { get; set; }
        public virtual DbSet<GEVWProductsUnit> GEVWProductsUnit { get; set; }
        public virtual DbSet<GEVWProductsType> GEVWProductsType { get; set; }
        public virtual DbSet<GEVWProductsFamily> GEVWProductsFamily { get; set; }
        public virtual DbSet<GEVWProductAplic> GEVWProductAplic { get; set; }
        public virtual DbSet<GEVWSincProduto> GEVWSincProduto { get; set; }
        public virtual DbSet<VEVWSincRegiaoVenda> VEVWSincRegiaoVenda { get; set; }
        public virtual DbSet<VEVWRegionSaleUF> VEVWRegionSaleUF { get; set; }
        public virtual DbSet<VEVWSincTabPreco> VEVWSincTabPreco { get; set; }
        public virtual DbSet<VEVWSincCabecPedido> VEVWSincCabecPedido { get; set; }
        public virtual DbSet<VEVWSincItemPedido> VEVWSincItemPedido { get; set; }
        public virtual DbSet<VEVWSincTransacoes> VEVWSincTransacoes { get; set; }
        public virtual DbSet<FSProdutoParamEstab> FSProdutoParamEstab { get; set; }
        public virtual DbSet<VEVWSincTipoVenda> VEVWSincTipoVenda { get; set; }
        public virtual DbSet<FSVWSincDanfe> FSVWSincDanfe { get; set; }
        public virtual DbSet<FSNfSaida> FSNfSaida { get; set; }


        public DbERPContext(DbContextOptions<DbERPContext> options, OracleConnectionStringBuilder pConnStr)
           : base(options)
        {
            OracleConnectionString = pConnStr;
        }
        
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.AddInterceptors(new DbERPContextConfig());
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<GESincLimiteCredito>(entity =>
                entity.HasKey(e => new { e.codestab, e.codempresa }));

            modelBuilder.Entity<FSProdutoParamEstab>(entity =>
                entity.HasKey(e => new { e.CodEstab, e.CodProduto }));
            
            modelBuilder.Entity<FSSincProduto>(entity =>
                entity.HasKey(e => new { e.CodEstab, e.CodProduto }));

            modelBuilder.Entity<VEPedidoSinc>(entity =>
                entity.HasKey(e => new { e.CodEstab, e.CodPedVenda }));

            modelBuilder.Entity<VESincPrVenda>(entity =>
                entity.HasKey(e => new { e.CodEstab, e.CodPrVenda, e.CodProduto, e.CodRegiao }));

            modelBuilder.Entity<VESincTransacoes>(entity =>
                entity.HasKey(e => new { e.CodEstab, e.CodSinc }));

            modelBuilder.Entity<VEVWSincCabecPedido>(entity =>
                entity.HasKey(e => new { e.CodEstab, e.IDERP }));

            modelBuilder.Entity<VEVWSincTipoVenda>(entity =>
                entity.HasKey(e => e.CodTipoVenda));

            modelBuilder.Entity<FSVWSincDanfe>(entity =>
                entity.HasNoKey());

            modelBuilder.Entity<FSNfSaida>(entity =>
                entity.HasKey(e => new { e.IndiceNfSai, e.CodEstab }));

            // modelBuilder.HasDefaultSchema(OracleConnectionString.UserID);

            /*
            modelBuilder.Entity<FSSincProduto>(entity =>
                entity.HasKey(e => new { e.CodEstab, e.CodSinc }));
            */

            var cascadeFKs = modelBuilder.Model.GetEntityTypes()
                .SelectMany(t => t.GetForeignKeys())
                .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            foreach (var fk in cascadeFKs)
                fk.DeleteBehavior = DeleteBehavior.Restrict;

            var tables = modelBuilder.Model.GetEntityTypes();

            foreach (var table in tables)
            {
                table.SetTableName(table.GetTableName().ToUpper());
                foreach (var col in table.GetProperties())
                    col.SetColumnName(col.GetDefaultColumnBaseName().ToUpper());
            }

            base.OnModelCreating(modelBuilder);
        }
    }
}
