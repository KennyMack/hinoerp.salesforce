﻿using Microsoft.EntityFrameworkCore.Diagnostics;
using System;
using System.Data;
using System.Data.Common;
using System.Threading;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Core.DataBase
{
    public class DbERPContextConfig : DbConnectionInterceptor
    {
        public override void ConnectionOpened(DbConnection connection, ConnectionEndEventData eventData)
        {
            try
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "BEGIN DBMS_APPLICATION_INFO.SET_CLIENT_INFO('HINOERP.SYNC'); END;";
                    cmd.CommandType = CommandType.Text;

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
            }
            base.ConnectionOpened(connection, eventData);
        }

        public override Task ConnectionOpenedAsync(DbConnection connection, ConnectionEndEventData eventData, CancellationToken cancellationToken = default)
        {
            try
            {
                using (var cmd = connection.CreateCommand())
                {
                    cmd.CommandText = "BEGIN DBMS_APPLICATION_INFO.SET_CLIENT_INFO('HINOERP.SYNC'); END;";
                    cmd.CommandType = CommandType.Text;

                    cmd.ExecuteNonQuery();
                }
            }
            catch (Exception)
            {
            }
            return base.ConnectionOpenedAsync(connection, eventData, cancellationToken);
        }
    }
}
