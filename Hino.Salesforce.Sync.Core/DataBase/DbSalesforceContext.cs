﻿using Hino.Salesforce.Infra.Cross.Entities.Fiscal;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal.Taxes;
using Hino.Salesforce.Infra.Cross.Entities.General;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Infra.Cross.Entities.Sales.Transactions;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.EntityFrameworkCore.ValueGeneration;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Core.DataBase
{
    public class DbSalesforceContext: DbContext
    {
        private OracleConnectionStringBuilder OracleConnectionString { get; }

        public virtual DbSet<GEEnterprises> GEEnterprises { get; set; }
        public virtual DbSet<GEEnterpriseCategory> GEEnterpriseCategory { get; set; }
        public virtual DbSet<FSFiscalOper> FSFiscalOper { get; set; }
        public virtual DbSet<GEEnterpriseGroup> GEEnterpriseGroup { get; set; }
        public virtual DbSet<GEEnterpriseFiscalGroup> GEEnterpriseFiscalGroup { get; set; }
        public virtual DbSet<GEPaymentType> GEPaymentType { get; set; }
        public virtual DbSet<GEPaymentCondition> GEPaymentCondition { get; set; }
        public virtual DbSet<GEPaymentCondInstallments> GEPaymentCondInstallments { get; set; }
        public virtual DbSet<GEEnterpriseGeo> GEEnterpriseGeo { get; set; }
        public virtual DbSet<GEProductsUnit> GEProductsUnit { get; set; }
        public virtual DbSet<GEProductsType> GEProductsType { get; set; }
        public virtual DbSet<GEProductsFamily> GEProductsFamily { get; set; }
        public virtual DbSet<GEProductAplic> GEProductAplic { get; set; }
        public virtual DbSet<GEProducts> GEProducts { get; set; }
        public virtual DbSet<VERegionSale> VERegionSale { get; set; }
        public virtual DbSet<VERegionSaleUF> VERegionSaleUF { get; set; }
        public virtual DbSet<VESalePrice> VESalePrice { get; set; }
        public virtual DbSet<VEOrders> VEOrders { get; set; }
        public virtual DbSet<VETransactions> VETransactions { get; set; }
        public virtual DbSet<VEOrderItems> VEOrderItems { get; set; }
        public virtual DbSet<VEOrderTaxes> VEOrderTaxes { get; set; }
        public virtual DbSet<FSNCM> FSNCM { get; set; }
        public virtual DbSet<VETypeSale> VETypeSale { get; set; }

        public DbSalesforceContext(DbContextOptions<DbSalesforceContext> options, OracleConnectionStringBuilder pConnStr)
           : base(options)
        {
            OracleConnectionString = pConnStr;
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            /*
            modelBuilder.Entity<FSSincProduto>(entity =>
                entity.HasKey(e => new { e.CodEstab, e.CodSinc }));
            */


            var Entities = modelBuilder.Model.GetEntityTypes();

            foreach (var item in Entities)
            {
                // Fazendo gerar a sequence de forma automatica
                var type = item.ClrType;
                modelBuilder.Entity(type)
                    .Property("Id")
                    .HasValueGenerator<SequenceGenerator>(); 
                modelBuilder.HasSequence<long>($"SEQ_{type.Name.ToUpper()}");

                // definindo todas as tabelas e campos para 
                // serem em Maiusculo
                item.SetTableName(item.GetTableName().ToUpper());
                foreach (var col in item.GetProperties())
                    col.SetColumnName(col.GetDefaultColumnBaseName().ToUpper());

                // Buscando as Fks para remover a função de 
                // padrão do EF de cascade ao excluir
                var fks = item.GetForeignKeys();
                var cascadeFKs = item.GetForeignKeys()
                    .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);
                foreach (var fk in cascadeFKs)
                    fk.DeleteBehavior = DeleteBehavior.Restrict;
            }

            // modelBuilder.Entity<GEEnterpriseGroup>()
            //     .Property(e => e.Id)
            //     .HasValueGenerator<SequenceGenerator>();

            // modelBuilder.HasSequence<long>("SEQ_GEEnterpriseGroup");

            // var cascadeFKs = modelBuilder.Model.GetEntityTypes()
            //     .SelectMany(t => t.GetForeignKeys())
            //     .Where(fk => !fk.IsOwnership && fk.DeleteBehavior == DeleteBehavior.Cascade);

            // foreach (var fk in cascadeFKs)
            //     fk.DeleteBehavior = DeleteBehavior.Restrict;

            // var tables = modelBuilder.Model.GetEntityTypes();
            // 
            // foreach (var table in tables)
            // {
            //     table.SetTableName(table.GetTableName().ToUpper());
            //     foreach (var col in table.GetProperties())
            //         col.SetColumnName(col.GetDefaultColumnBaseName().ToUpper());
            // }

            base.OnModelCreating(modelBuilder);
        }
    }

    public class SequenceGenerator : ValueGenerator<long>
    {
        public override long Next(EntityEntry entry) =>
            GetNextID(
            entry.Context.Database.GetDbConnection(),
            entry.Entity.GetType().Name);

        public override bool GeneratesTemporaryValues => false;

        private long GetNextID(DbConnection conn, string tableName)
        {
            long nextID = -1;
            var sqlStr = $"SELECT SEQ_{tableName.ToUpper()}.NEXTVAL FROM DUAL";
            try
            {
                conn.Open();
                var cmd = conn.CreateCommand();
                cmd.CommandText = sqlStr;
                nextID = Convert.ToInt64(cmd.ExecuteScalar());
            }
            finally
            {
                conn.Close();
            }
            return nextID;
        }
    }
}
