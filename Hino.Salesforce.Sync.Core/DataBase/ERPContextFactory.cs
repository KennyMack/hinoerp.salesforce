﻿using Hino.Salesforce.Sync.Core.Logging;
using Hino.Salesforce.Sync.Models;
using Hino.Salesforce.Sync.Models.Fiscal;
using Hino.Salesforce.Sync.Models.General;
using Hino.Salesforce.Sync.Models.Vendas;
using Hino.Salesforce.Sync.Models.Views;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Core.DataBase
{
    public class ERPContextFactory : IDesignTimeDbContextFactory<DbERPContext>
    {
        public DbERPContext CreateDbContext(string[] args)
        {
            var connStr = new OracleConnectionStringBuilder
            {
                DataSource = args[1],
                UserID = args[2],
                Password = args[3]
            };

            var optionsBuilder = new DbContextOptionsBuilder<DbERPContext>();
            optionsBuilder.UseOracle(connStr.ToString());

            return new DbERPContext(optionsBuilder.Options, connStr);
        }

        public long GetNextSequence(DbContext context, string tableName)
        {
            long nextID = -1;
            var sqlStr = $"SELECT SEQ_{tableName.ToUpper()}.NEXTVAL FROM DUAL";
            var conn = context.Database.GetDbConnection();
            try
            {
                
                conn.Open();
                var cmd = conn.CreateCommand();
                cmd.CommandText = sqlStr;
                nextID = Convert.ToInt64(cmd.ExecuteScalar());
            }
            finally
            {
                conn.Close();
            }
            return nextID;
        }
    }
}
