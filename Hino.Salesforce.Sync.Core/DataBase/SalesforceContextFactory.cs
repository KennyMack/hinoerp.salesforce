﻿using Hino.Salesforce.Sync.Core.Logging;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Core.DataBase
{
    public class SalesforceContextFactory : IDesignTimeDbContextFactory<DbSalesforceContext>
    {
        public DbSalesforceContext CreateDbContext(string[] args)
        {
            var connStr = new OracleConnectionStringBuilder
            {
                DataSource = args[1],
                UserID = args[2],
                Password = args[3]
            };

            var optionsBuilder = new DbContextOptionsBuilder<DbSalesforceContext>();
            optionsBuilder.UseOracle(connStr.ToString());

            return new DbSalesforceContext(optionsBuilder.Options, connStr);
        }
    }
}
