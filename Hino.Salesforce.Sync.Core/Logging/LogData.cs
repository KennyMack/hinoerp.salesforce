﻿using Hino.Salesforce.Sync.Core.Exceptions;
using Hino.Salesforce.Sync.Core.Utils;
using Serilog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Core.Logging
{
    public static class LogData
    {
        public static void Info(string pMessage) =>
            Log.Information(pMessage);

        public static void Error(string pMessage) =>
            Log.Error(pMessage);

        public static string Error(string pText, List<ModelException> pException, string ExceptionCode = "")
        {
            var ErrorLog = string.Join(", ", pException.Select(r => string.Join(", ", r.Messages)).ToArray());
            if (ExceptionCode.IsEmpty())
                ExceptionCode = Guid.NewGuid().ToString();

            Log.Error(pText);
            Log.Error($"Erro: { ExceptionCode }");
            Log.Error($"Motivo: { ErrorLog }");

            return ErrorLog;
        }

        public static string Error(string pText, Exception pException, string ExceptionCode = "")
        {
            var ErrorLog = pException.Message;
            if (ExceptionCode.IsEmpty())
                ExceptionCode = Guid.NewGuid().ToString();
            Log.Error(pText);
            Log.Error($"Erro: { ExceptionCode }");
            Log.Error($"Motivo: { ErrorLog }");
            Log.Error($"Stack Inicio -----------------------------------");
            Log.Error(pException, "");
            if (pException.InnerException != null)
            {
                Log.Error($"InnerException Stack Inicio -----------------------------------");
                Log.Error(pException.InnerException, "");
                Log.Error($"InnerException Stack Termino -----------------------------------");
            }
            Log.Error($"Stack Termino -----------------------------------");

            return ErrorLog;
        }
    }
}
