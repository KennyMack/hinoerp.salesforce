﻿using Microsoft.Extensions.DependencyInjection;
using Serilog;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Core.Logging
{
    public static class SerilogInjection
    {
        public static IServiceCollection AddSerilogServices(this IServiceCollection services, string BasePath, string fileName)
        {
            Log.Logger = new LoggerConfiguration()
                  .MinimumLevel.Debug()
                  .WriteTo.Console()
                  //.WriteTo.File("logs/credit.sync.log", rollingInterval: RollingInterval.Day)
                  .WriteTo.File($"{BasePath}/logs/{fileName}.log", rollingInterval: RollingInterval.Day)
                  .CreateLogger();
            return services;
        }
    }
}
