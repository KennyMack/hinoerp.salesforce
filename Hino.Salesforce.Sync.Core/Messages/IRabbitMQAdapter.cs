﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Core.Messages
{
    public delegate void MessageReceived(MessageReceivedEventArgs args);

    public interface IRabbitMQAdapter
    {
        event MessageReceived MessageReceived;
        void TryConnect();
        // void BasicPublish(ICommand command, string destination);
        void BasicPublish(string routingKey, MessageReceivedEventArgs messageToPublish);
        void BasicPublish(string pQueueName, string pExchangeName, string routingKey, MessageReceivedEventArgs messageToPublish);
        void StartConsuming();
        void AckMessage(ulong pDeliveryTag);
        void NackMessage(ulong pDeliveryTag);
    }
}
