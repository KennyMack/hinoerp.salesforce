﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Core.Messages
{
    public class MessageReceivedEventArgs: EventArgs
    {
        public ulong DeliveryTag { get; set; }
        public string EntryName { get; set; }
        public string Type { get; set; }
        public long Id { get; set; }
        public string EstablishmentKey { get; set; }
        public string UniqueKey { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }
        public bool IsActive { get; set; }
        public string FailureBody { get; set; }
        public string ErrorMessage { get; set; }
        public string ProcessName { get; set; }
    }
}
