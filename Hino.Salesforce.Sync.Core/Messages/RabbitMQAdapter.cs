﻿using System;
using System.Net.Sockets;
using System.Text;
using Hino.Salesforce.Sync.Core.Configuration;
using Hino.Salesforce.Sync.Core.Logging;
using Newtonsoft.Json;
using Polly;
using Polly.Retry;
using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using RabbitMQ.Client.Exceptions;

namespace Hino.Salesforce.Sync.Core.Messages
{
    public class RabbitMQAdapter : IRabbitMQAdapter
    {
        /// <summary>
        /// Sending/Publishing can be performed via multiple channels, but subscribing/consuming channel must be single.
        /// </summary>
        private IModel consumerChannel;
        private bool _isConnected = false;
        readonly string exchangeName;
        readonly int retryCount = 3;
        readonly string queueName;
        private IConnection _connection;
        readonly ConnectionFactory factory;
        public event MessageReceived MessageReceived;

        private IConnection connection
        {
            get
            {
                if (!_isConnected)
                {
                    TryConnect();
                }

                return _connection;
            }
        }

        public RabbitMQAdapter(string queueName, string exchangeName, RabbitMQSettings Settings)
        {
            this.queueName = queueName;
            this.exchangeName = exchangeName;
            factory = new ConnectionFactory() 
            {
                // Port = Settings.RabbitPort,
                HostName = Settings.RabbitHostName,
                UserName = Settings.RabbitUserName,
                Password = Settings.RabbitPassword
            };
        }

        public void TryConnect()
        {
            var policy = Policy.Handle<SocketException>().Or<BrokerUnreachableException>()
                   .WaitAndRetry(retryCount, op => TimeSpan.FromSeconds(Math.Pow(2, op)), (ex, time) =>
                   {
                       LogData.Info("Servidor do Rabbit não encontrado");
                   });

            policy.Execute(() =>
            {
                _connection = factory.CreateConnection();
                _isConnected = true;
                LogData.Info("Conectado ao Rabbit!");
            });
        }

        public void AckMessage(ulong pDeliveryTag) =>
            consumerChannel.BasicAck(deliveryTag: pDeliveryTag, multiple: false);
        
        public void NackMessage(ulong pDeliveryTag) =>
            consumerChannel.BasicNack(pDeliveryTag, false, true);

        public void StartConsuming()
        {
            consumerChannel = connection.CreateModel();
            consumerChannel.ExchangeDeclare(
                exchange: exchangeName,
                type: "topic",
                durable: true,
                autoDelete: true);

            var queue = consumerChannel.QueueDeclare(
                queue: queueName,
                durable: true,
                exclusive: false,
                autoDelete: true,
                arguments: null);

            consumerChannel.QueueBind(queue, exchangeName, routingKey: exchangeName);

            var consumer = new EventingBasicConsumer(consumerChannel);

            consumerChannel.BasicConsume(
                queue: queueName,
                autoAck: false,
                consumer: consumer
            );

            consumer.Received += (model, ea) => {
                LogData.Info("Mensagem recebida.");
                LogData.Info("Decodificando a mensagem.");
                var messageBody = ea.Body.ToArray();
                var Message = Encoding.UTF8.GetString(messageBody);
                LogData.Info("Gerando JSON.");
                MessageReceivedEventArgs args;
                try
                {
                    args = JsonConvert.DeserializeObject<MessageReceivedEventArgs>(Message, new JsonSerializerSettings
                    {
                        Culture = new System.Globalization.CultureInfo("pt-BR"),
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                        DateFormatString = "dd/MM/yyyy HH:mm:ss"
                    });
                }
                catch (Exception ex)
                {
                    args = new MessageReceivedEventArgs
                    {
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        DeliveryTag = ea.DeliveryTag,
                        EntryName = Models.Constants.NotFound,
                        ErrorMessage = ex.Message,
                        FailureBody = Message,
                        EstablishmentKey = "",
                        Type = "Invalid",
                        UniqueKey = "",
                        IsActive = false,
                        Id = 0
                    };
                }
                args.DeliveryTag = ea.DeliveryTag;
                MessageReceived.Invoke(args);
            };
        }

        void PublishMessage(string pQueueName, string pExchangeName, string pRoutingKey, MessageReceivedEventArgs messageToPublish)
        {
            using (var channel = connection.CreateModel())
            {
                channel.ExchangeDeclare(
                    exchange: pExchangeName,
                    type: "topic",
                    durable: true,
                    autoDelete: true);

                channel.QueueDeclare(
                           queue: pQueueName,
                           durable: true,
                           exclusive: false,
                           autoDelete: true,
                           arguments: null);

                channel.QueueBind(
                    queue: pQueueName,
                    exchange: pExchangeName,
                    routingKey: pRoutingKey
                );

                var properties = channel.CreateBasicProperties();
                properties.Persistent = true;
                properties.DeliveryMode = 2;

                var obj = JsonConvert.SerializeObject(messageToPublish, new JsonSerializerSettings
                {
                    Culture = new System.Globalization.CultureInfo("pt-BR"),
                    Formatting = Formatting.Indented,
                    ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                });

                var body = Encoding.UTF8.GetBytes(obj);

                channel.BasicPublish(
                    exchange: pExchangeName,
                    routingKey: pRoutingKey,
                    basicProperties: properties,
                    body: body);
            }

        }

        public void BasicPublish(string routingKey, MessageReceivedEventArgs messageToPublish) =>
            PublishMessage(queueName, exchangeName, routingKey, messageToPublish);

        public void BasicPublish(string pQueueName, string pExchangeName, string routingKey, MessageReceivedEventArgs messageToPublish) =>
            PublishMessage(pQueueName, pExchangeName, routingKey, messageToPublish);
    }
}
