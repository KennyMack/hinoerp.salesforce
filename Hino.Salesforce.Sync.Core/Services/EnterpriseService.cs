﻿using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Sync.Core.Configuration;
using Hino.Salesforce.Sync.Core.DataBase;
using Hino.Salesforce.Sync.Core.Logging;
using Hino.Salesforce.Sync.Core.Utils;
using Hino.Salesforce.Sync.Models;
using Hino.Salesforce.Sync.Models.General;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Core.Services
{
    public class EnterpriseService
    {
        readonly DbERPContext ERPContext;
        private AppSettingsParameters AppSettings { get; }
        public EnterpriseService(DbERPContext ERPContext, AppSettingsParameters config)
        {
            this.ERPContext = ERPContext;
            AppSettings = config;
        }

        public async Task ClearEnterpriseContactsAsync(OracleConnection conn, long pIdEnterprise)
        {
            using var cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"DELETE
                                      FROM CRENTERPRISECONTACTS
                                     WHERE CRENTERPRISECONTACTS.ENTERPRISEID = :pENTERPRISEID";

            var queueIdParam = new OracleParameter("pENTERPRISEID", OracleDbType.Int64, pIdEnterprise, ParameterDirection.Input);

            cmd.Parameters.AddRange(new[] {
                    queueIdParam
                });
            await cmd.ExecuteNonQueryAsync();
        }

        public async Task<bool> EnterpriseExists(OracleConnection conn, long pIdEnterprise, int pCodEstab)
        {
            using var cmd = new OracleCommand();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = @"SELECT COUNT(1)
                                      FROM GEEMPRESAPARAMESTAB
                                     WHERE GEEMPRESAPARAMESTAB.CODESTAB = :pCODESTAB
                                       AND GEEMPRESAPARAMESTAB.IDAPI = :pIDAPI";

            var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input);
            var queueIdParam = new OracleParameter("pIDAPI", OracleDbType.Int64, pIdEnterprise, ParameterDirection.Input);

            cmd.Parameters.AddRange(new[] {
                    codeEstabParam,
                    queueIdParam
                });

            return Convert.ToInt64(await cmd.ExecuteScalarAsync()) > 0;
        }

        public async Task<CRCategoria> SaveToERPEnterpriseCategoryAsync(GEQueueItem QueueItem)
        {
            var SalesforceContextFactory = new SalesforceContextFactory();

            LogData.Info($"Realizando conexao com o banco de dados do Portal.");

            GEEnterpriseCategory EnterpriseGroup;
            CRCategoria NewCRCategoria = null;
            using (var SalesforceContext = SalesforceContextFactory.CreateDbContext(AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters()))
            {
                LogData.Info("Buscando dados da categoria da empresa.");
                EnterpriseGroup = SalesforceContext
                     .GEEnterpriseCategory
                     .AsNoTracking()
                     .FirstOrDefault(r =>
                         r.Id == QueueItem.IdReference &&
                         r.EstablishmentKey == QueueItem.EstablishmentKey);
            }

            if (EnterpriseGroup == null)
            {
                QueueItem.Processed = true;
                QueueItem.Uploaded = true;
                QueueItem.Note = Constants.NotFound;
                return null;
            }

            LogData.Info($"Buscando dados da conexão da EstablishmentKey: {QueueItem.EstablishmentKey}");
            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(QueueItem.EstablishmentKey);

            if (ERPDatabase == null)
            {
                QueueItem.Processed = true;
                QueueItem.Uploaded = true;
                QueueItem.Note = Constants.NotFound;
                LogData.Info($"Dados de conexão da EstablishmentKey: {QueueItem.EstablishmentKey} não encontrados");
                return null;
            }

            var ERPContextFactory = new ERPContextFactory();
            try
            {
                LogData.Info("Salvando categoria da empresa na tabela intermediária.");
                LogData.Info($"Realizando conexao com o banco de dados do ERP ({ERPDatabase.Description}).");
                using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
                {
                    var OldCategoria = ERPContext.CRCategoria.FirstOrDefault(r => r.CodCategoria == EnterpriseGroup.IdERP);

                    NewCRCategoria = new CRCategoria();
                    if (OldCategoria != null)
                        NewCRCategoria = OldCategoria;

                    NewCRCategoria.Descricao = EnterpriseGroup.Description.ToUpper();

                    if (OldCategoria == null)
                    {
                        NewCRCategoria.CodCategoria = Convert.ToInt32(ERPContextFactory.GetNextSequence(ERPContext, "CRCATEGORIA"));

                        ERPContext.CRCategoria.Add(NewCRCategoria);
                    }
                    else
                        ERPContext.Entry(NewCRCategoria).State = EntityState.Modified;

                    EnterpriseGroup.IdERP = NewCRCategoria.CodCategoria;

                    await ERPContext.SaveChangesAsync();
                    LogData.Info($"categoria da empresa: {NewCRCategoria.CodCategoria} salvo com sucesso na CRCategoria.");
                }

                QueueItem.Processed = true;
                QueueItem.Uploaded = true;
                QueueItem.Note = $"categoria da empresa: {NewCRCategoria.CodCategoria} salvo com sucesso";
                QueueItem.ExceptionCode = null;
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = LogData.Error("Não foi possível salvar os dados na tabela intermediária.", ex, ExceptionCode);

                QueueItem.Note = ErrorLog.SubStr(0, 255);
                QueueItem.ExceptionCode = ExceptionCode;

                return null;
            }

            return NewCRCategoria;
        }

        public async Task<CRGrupo> SaveToERPEnterpriseGroupAsync(GEQueueItem QueueItem)
        {
            var SalesforceContextFactory = new SalesforceContextFactory();

            LogData.Info($"Realizando conexao com o banco de dados do Portal.");

            GEEnterpriseGroup EnterpriseGroup;
            CRGrupo NewGroup = null;
            using (var SalesforceContext = SalesforceContextFactory.CreateDbContext(AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters()))
            {
                LogData.Info("Buscando dados do grupo da empresa.");
                EnterpriseGroup = SalesforceContext
                     .GEEnterpriseGroup
                     .AsNoTracking()
                     .FirstOrDefault(r =>
                         r.Id == QueueItem.IdReference &&
                         r.EstablishmentKey == QueueItem.EstablishmentKey);
            }

            if (EnterpriseGroup == null)
            {
                QueueItem.Processed = true;
                QueueItem.Uploaded = true;
                QueueItem.Note = Constants.NotFound;
                return null;
            }

            LogData.Info($"Buscando dados da conexão da EstablishmentKey: {QueueItem.EstablishmentKey}");
            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(QueueItem.EstablishmentKey);

            if (ERPDatabase == null)
            {
                QueueItem.Processed = true;
                QueueItem.Uploaded = true;
                QueueItem.Note = Constants.NotFound;
                LogData.Info($"Dados de conexão da EstablishmentKey: {QueueItem.EstablishmentKey} não encontrados");
                return null;
            }

            var ERPContextFactory = new ERPContextFactory();
            try
            {
                LogData.Info("Salvando grupo da empresa na tabela intermediária.");
                LogData.Info($"Realizando conexao com o banco de dados do ERP ({ERPDatabase.Description}).");
                using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
                {
                    var OldGroup = ERPContext.CRGrupo.FirstOrDefault(r => r.CodGrupo == EnterpriseGroup.IdERP);

                    NewGroup = new CRGrupo();
                    if (OldGroup != null)
                        NewGroup = OldGroup;

                    NewGroup.Descricao = EnterpriseGroup.Description.ToUpper();

                    if (OldGroup == null)
                    {
                        NewGroup.CodGrupo = Convert.ToInt32(ERPContextFactory.GetNextSequence(ERPContext, "CRGRUPO"));

                        ERPContext.CRGrupo.Add(NewGroup);
                    }
                    else
                        ERPContext.Entry(NewGroup).State = EntityState.Modified;

                    EnterpriseGroup.IdERP = NewGroup.CodGrupo;

                    await ERPContext.SaveChangesAsync();
                    LogData.Info($"Grupo da empresa: {NewGroup.CodGrupo} salvo com sucesso na CRGrupo.");
                }

                QueueItem.Processed = true;
                QueueItem.Uploaded = true;
                QueueItem.Note = $"Grupo da empresa: {NewGroup.CodGrupo} salvo com sucesso";
                QueueItem.ExceptionCode = null;
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = LogData.Error("Não foi possível salvar os dados na tabela intermediária.", ex, ExceptionCode);

                QueueItem.Note = ErrorLog.SubStr(0, 255);
                QueueItem.ExceptionCode = ExceptionCode;

                return null;
            }

            return NewGroup;
        }

        public async Task<CREnterprises> SaveToERPEnterpriseAsync(GEQueueItem QueueItem)
        {
            var SalesforceContextFactory = new SalesforceContextFactory();

            LogData.Info($"Realizando conexao com o banco de dados do Portal.");

            GEEnterprises Enterprise;
            CREnterprises NewEnterprise;
            using (var SalesforceContext = SalesforceContextFactory.CreateDbContext(AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters()))
            {
                LogData.Info("Buscando dados da empresa.");
                Enterprise = SalesforceContext
                     .GEEnterprises
                     .AsNoTracking()
                     .Include(enterprise => enterprise.GEEnterpriseAnnotation)
                     .Include(enterprise => enterprise.GEEnterpriseCategory)
                     .Include(enterprise => enterprise.GEEnterpriseContacts)
                     .Include(enterprise => enterprise.GEEnterpriseFiscalGroup)
                     .Include(enterprise => enterprise.GEEnterpriseGroup)
                     .Include(enterprise => enterprise.GEEnterpriseGeo)
                     .FirstOrDefault(r =>
                         r.Id == QueueItem.IdReference &&
                         r.EstablishmentKey == QueueItem.EstablishmentKey);
            }

            if (Enterprise == null)
            {
                QueueItem.Processed = true;
                QueueItem.Uploaded = true;
                QueueItem.Note = Constants.NotFound;
                return null;
            }

            LogData.Info($"Buscando dados da conexão da EstablishmentKey: {QueueItem.EstablishmentKey}");
            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(QueueItem.EstablishmentKey);

            if (ERPDatabase == null)
            {
                QueueItem.Processed = true;
                QueueItem.Uploaded = true;
                QueueItem.Note = Constants.NotFound;
                LogData.Info($"Dados de conexão da EstablishmentKey: {QueueItem.EstablishmentKey} não encontrados");
                return null;
            }

            var ERPContextFactory = new ERPContextFactory();
            try
            {
                LogData.Info("Salvando na empresa tabela intermediária.");
                LogData.Info($"Realizando conexao com o banco de dados do ERP ({ERPDatabase.Description}).");
                using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
                {
                    LogData.Info($"Verificando se já existe a empresa: {Enterprise.Id} na CREnterprises.");
                    var OldEnterprise = ERPContext.CREnterprises
                         .Include(enterprise => enterprise.GEEnterpriseContacts)
                         .Include(enterprise => enterprise.GEEnterpriseGeo)
                         .FirstOrDefault(r => r.Id == Enterprise.Id);

                    NewEnterprise = new CREnterprises();
                    if (OldEnterprise != null)
                    {
                        LogData.Info($"Já existe a empresa: {Enterprise.Id} na CREnterprises.");
                        NewEnterprise = OldEnterprise;
                    }

                    NewEnterprise.Id = Enterprise.Id;
                    NewEnterprise.EstablishmentKey = Enterprise.EstablishmentKey;
                    NewEnterprise.UniqueKey = Enterprise.UniqueKey;
                    NewEnterprise.Created = Enterprise.Created;
                    NewEnterprise.Modified = Enterprise.Modified;
                    NewEnterprise.IsActive = Enterprise.IsActive;
                    NewEnterprise.RazaoSocial = Enterprise.RazaoSocial;
                    NewEnterprise.NomeFantasia = Enterprise.NomeFantasia;
                    NewEnterprise.CNPJCPF = Enterprise.CNPJCPF;
                    NewEnterprise.IE = Enterprise.IE;
                    NewEnterprise.Type = (int)Enterprise.Type;
                    NewEnterprise.Status = (int)Enterprise.Status;
                    NewEnterprise.StatusSinc = (int)Enterprise.StatusSinc;
                    NewEnterprise.IdERP = Enterprise.IdERP;
                    NewEnterprise.RegionId = Enterprise.RegionId;
                    NewEnterprise.Classification = (int)Enterprise.Classification;
                    NewEnterprise.UserId = 0;
                    NewEnterprise.CreatedById = Enterprise.CreatedById;
                    NewEnterprise.PayConditionId = Enterprise.PayConditionId;
                    NewEnterprise.ClassifEmpresa = Enterprise.ClassifEmpresa;

                    NewEnterprise.SellerChangePrice = Enterprise.SellerChangePrice;
                    NewEnterprise.SellerPayCondition = Enterprise.SellerPayCondition;
                    NewEnterprise.SellerBonification = Enterprise.SellerBonification;
                    NewEnterprise.SellerSample = Enterprise.SellerSample;
                    NewEnterprise.DiscountNFBoleto = Enterprise.DiscountNFBoleto;
                    NewEnterprise.PercDiscount = Enterprise.PercDiscount;

                    NewEnterprise.QueueId = QueueItem.Id;
                    NewEnterprise.FiscalGroupId = null;
                    NewEnterprise.CategoryId = null;
                    NewEnterprise.GroupId = null;

                    if (Enterprise.GEEnterpriseFiscalGroup != null && Enterprise.GEEnterpriseFiscalGroup.IdERP > 0)
                        NewEnterprise.FiscalGroupId = Enterprise.GEEnterpriseFiscalGroup.IdERP;

                    if (Enterprise.GEEnterpriseCategory != null && Enterprise.GEEnterpriseCategory.IdERP > 0)
                        NewEnterprise.CategoryId = Enterprise.GEEnterpriseCategory.IdERP;

                    if (Enterprise.GEEnterpriseGroup != null && Enterprise.GEEnterpriseGroup.IdERP > 0)
                        NewEnterprise.GroupId = Enterprise.GEEnterpriseGroup.IdERP;

                    LogData.Info($"Atualizando a empresa: {Enterprise.Id} na CREnterprises.");
                    if (OldEnterprise == null)
                    {
                        NewEnterprise.Id = Enterprise.Id;
                        ERPContext.CREnterprises.Add(NewEnterprise);
                    }
                    else
                        ERPContext.Entry(NewEnterprise).State = EntityState.Modified;

                    LogData.Info($"Limpando os contatos da empresa: {Enterprise.Id} na CREnterprises.");
                    foreach (var item in NewEnterprise.GEEnterpriseContacts)
                        ERPContext.CREnterpriseContacts.Remove(item);

                    LogData.Info($"Limpando os endereços da empresa: {Enterprise.Id} na CREnterprises.");
                    foreach (var item in NewEnterprise.GEEnterpriseGeo)
                        ERPContext.CREnterpriseGeo.Remove(item);

                    NewEnterprise.GEEnterpriseContacts.Clear();
                    NewEnterprise.GEEnterpriseGeo.Clear();

                    LogData.Info($"Adicionando os endereços da empresa: {Enterprise.Id} na CREnterprises.");
                    foreach (var item in Enterprise.GEEnterpriseGeo)
                    {
                        var NewEnterpriseGeo = new CREnterpriseGeo();

                        NewEnterpriseGeo.Id = item.Id;
                        NewEnterpriseGeo.EstablishmentKey = Enterprise.EstablishmentKey;
                        NewEnterpriseGeo.UniqueKey = item.UniqueKey;
                        NewEnterpriseGeo.Created = item.Created;
                        NewEnterpriseGeo.Modified = item.Modified;
                        NewEnterpriseGeo.IsActive = item.IsActive;
                        NewEnterpriseGeo.Enterpriseid = NewEnterprise.Id;
                        NewEnterpriseGeo.IE = item.IE;
                        NewEnterpriseGeo.Type = (int)item.Type;
                        NewEnterpriseGeo.CNPJCPF = item.CNPJCPF;
                        NewEnterpriseGeo.RG = item.RG;
                        NewEnterpriseGeo.CellPhone = item.CellPhone;
                        NewEnterpriseGeo.Phone = item.Phone;
                        NewEnterpriseGeo.Email = item.Email;
                        NewEnterpriseGeo.Address = item.Address;
                        NewEnterpriseGeo.Complement = item.Complement;
                        NewEnterpriseGeo.District = item.District;
                        NewEnterpriseGeo.Num = item.Num;
                        NewEnterpriseGeo.ZipCode = item.ZipCode;
                        NewEnterpriseGeo.Site = item.Site;
                        NewEnterpriseGeo.CountryIni = item.CountryIni;
                        NewEnterpriseGeo.CountryCode = item.CountryCode;
                        NewEnterpriseGeo.CountryName = item.CountryName;
                        NewEnterpriseGeo.UF = item.UF;
                        NewEnterpriseGeo.StateName = item.StateName;
                        NewEnterpriseGeo.IBGE = item.IBGE;
                        NewEnterpriseGeo.CityName = item.CityName;
                        NewEnterpriseGeo.DisplayLat = item.DisplayLat;
                        NewEnterpriseGeo.DisplayLng = item.DisplayLng;
                        NewEnterpriseGeo.NavLat = item.NavLat;
                        NewEnterpriseGeo.NavLng = item.NavLng;
                        NewEnterpriseGeo.InscSuframa = item.InscSuframa;


                        NewEnterprise.GEEnterpriseGeo.Add(NewEnterpriseGeo);
                    }

                    LogData.Info($"Adicionando os contatos da empresa: {Enterprise.Id} na CREnterprises.");
                    foreach (var item in Enterprise.GEEnterpriseContacts)
                    {
                        var NewEnterpriseContact = new CREnterpriseContacts
                        {
                            Id = item.Id,
                            EstablishmentKey = Enterprise.EstablishmentKey,
                            UniqueKey = item.UniqueKey,
                            Created = item.Created,
                            Modified = item.Modified,
                            IsActive = item.IsActive,
                            EnterpriseId = NewEnterprise.Id,
                            Email = item.Email,
                            Contact = item.Contact,
                            Ramal = item.Ramal,
                            Phone = item.Phone,
                            Note = item.Note,
                            ReceptivityIndex = (int)item.ReceptivityIndex,
                            Sector = (int)item.Sector
                        };
                        NewEnterprise.GEEnterpriseContacts.Add(NewEnterpriseContact);
                    }

                    await ERPContext.SaveChangesAsync();
                    ERPContext.Dispose();
                }

                if (Enterprise.GroupId != null)
                {
                    LogData.Info($"Salvando grupo da empresa.");
                    var QueueItemGroup = new GEQueueItem
                    {
                        Id = 0,
                        EstablishmentKey = NewEnterprise.EstablishmentKey,
                        UniqueKey = Enterprise.GEEnterpriseGroup.UniqueKey,
                        IdReference = Enterprise.GEEnterpriseGroup.Id,
                        EntryName = Constants.GEEnterpriseGroup,
                        Processed = false,
                        Uploaded = false,
                        IsActive = true,
                        Note = "",
                        Type = "NEW",
                        ExceptionCode = "",
                        Created = DateTime.Now,
                        Modified = DateTime.Now
                    };
                    var QueueService = new QueueService(AppSettings);
                    QueueItemGroup = await QueueService.SaveNewQueueAsync(QueueItemGroup);

                    var Group = await SaveToERPEnterpriseGroupAsync(QueueItemGroup);
                    await QueueService.SaveChangesQueueAsync(QueueItemGroup);
                }

                if (Enterprise.CategoryId != null)
                {
                    LogData.Info($"Salvando categoria da empresa.");
                    var QueueItemCategory = new GEQueueItem
                    {
                        Id = 0,
                        EstablishmentKey = NewEnterprise.EstablishmentKey,
                        UniqueKey = Enterprise.GEEnterpriseCategory.UniqueKey,
                        IdReference = Enterprise.GEEnterpriseCategory.Id,
                        EntryName = Constants.GEEnterpriseCategory,
                        Processed = false,
                        Uploaded = false,
                        IsActive = true,
                        Note = "",
                        Type = "NEW",
                        ExceptionCode = "",
                        Created = DateTime.Now,
                        Modified = DateTime.Now
                    };

                    var QueueService = new QueueService(AppSettings);
                    QueueItemCategory = await QueueService.SaveNewQueueAsync(QueueItemCategory);

                    var Category = await SaveToERPEnterpriseGroupAsync(QueueItemCategory);
                    await QueueService.SaveChangesQueueAsync(QueueItemCategory);
                }

                using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
                {
                    await ERPContext.Database.ExecuteSqlRawAsync("BEGIN PCKG_INTEGRACAO.INSERTENTERPRISE(:pQUEUEID); END;",
                       new OracleParameter("pQUEUEID", OracleDbType.Int64, QueueItem.Id, ParameterDirection.Input));

                    var Item = await ERPContext.GEQueueItem.Where(r => r.Id == QueueItem.Id).FirstAsync();
                    QueueItem.Note = Item.Note;

                    NewEnterprise = ERPContext.CREnterprises
                         .Include(enterprise => enterprise.GEEnterpriseContacts)
                         .Include(enterprise => enterprise.GEEnterpriseGeo)
                         .FirstOrDefault(r => r.Id == Enterprise.Id);

                    LogData.Info(QueueItem.Note);
                }

                if (NewEnterprise.IdERP > 0)
                {
                    LogData.Info($"Atualizando a empresa: {Enterprise.Id} no portal com o IDERP: {NewEnterprise.IdERP}.");
                    using (var SalesforceContext = SalesforceContextFactory.CreateDbContext(AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters()))
                    {
                        LogData.Info("Buscando dados da empresa no portal.");
                        Enterprise = SalesforceContext
                             .GEEnterprises
                             .FirstOrDefault(r =>
                                 r.Id == Enterprise.Id &&
                                 r.EstablishmentKey == Enterprise.EstablishmentKey);

                        Enterprise.IdERP = NewEnterprise.IdERP;
                        Enterprise.StatusSinc = Infra.Cross.Utils.Enums.EStatusSinc.Integrated;

                        await SalesforceContext.SaveChangesAsync();
                    }
                }

                QueueItem.Processed = true;
                QueueItem.Uploaded = true;
                QueueItem.ExceptionCode = null;
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = LogData.Error("Não foi possível salvar os dados na tabela intermediária.", ex, ExceptionCode);

                QueueItem.Note = ErrorLog.SubStr(0, 255);
                QueueItem.ExceptionCode = ExceptionCode;

                return null;
            }

            return NewEnterprise;
        }
    }
}
