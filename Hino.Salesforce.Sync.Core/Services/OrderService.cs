﻿using Hino.Salesforce.Sync.Core.Configuration;
using Hino.Salesforce.Sync.Core.DataBase;
using Hino.Salesforce.Sync.Core.Logging;
using Hino.Salesforce.Sync.Core.Messages;
using Hino.Salesforce.Sync.Core.Utils;
using Hino.Salesforce.Sync.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Hino.Salesforce.Sync.Models;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Sync.Models.Views;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Sync.Models.General;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Data.Common;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Hino.Salesforce.Sync.Models.Vendas;
using Hino.Salesforce.Sync.Models.Fiscal;
using Hino.Salesforce.Infra.Cross.Entities.Fiscal;
using Hino.Salesforce.Infra.Cross.Entities.Sales.Transactions;

namespace Hino.Salesforce.Sync.Core.Services
{
    public class OrderService
    {
        private AppSettingsParameters AppSettings { get; }
        public OrderService(AppSettingsParameters config)
        {
            AppSettings = config;
        }

        public async Task<CROrders> GenerateOrderCabecAsync(OracleConnection conn, int pCodEstab, CROrders pOrder)
        {
            long codPedVenda;
            string msErro;

            using var cmd = new OracleCommand();
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_INTEGRACAO.INTEGRACABECPEDIDO";

                var returnParam = new OracleParameter("nRETURN", OracleDbType.Int64, ParameterDirection.ReturnValue);
                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input);
                var queueIdParam = new OracleParameter("pQUEUEID", OracleDbType.Int64, pOrder.Id, ParameterDirection.Input);
                var codPedVendaOutParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, ParameterDirection.Output);
                var msErroOutParam = new OracleParameter("pMSERRO", OracleDbType.Clob, ParameterDirection.Output);

                cmd.Parameters.AddRange(new[] {
                    returnParam,
                    codeEstabParam,
                    queueIdParam,
                    codPedVendaOutParam,
                    msErroOutParam
                });
                await cmd.ExecuteNonQueryAsync();

                codPedVenda = Convert.ToInt64((((OracleDecimal)(codPedVendaOutParam.Value)).Value));
                msErro = (((OracleClob)(msErroOutParam.Value)).Value).ToString();
                msErro = msErro == "*OK*" ? "" : msErro;
            }

            pOrder.CodPedVenda = codPedVenda;
            pOrder.MsErro = msErro;

            return pOrder;
        }

        public async Task<CROrders> GenerateOrderItemsAsync(OracleConnection conn, int pCodEstab, CROrders pOrder)
        {
            string msErro;

            using var cmd = new OracleCommand();
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_INTEGRACAO.INTEGRAITEMPEDIDO";

                var returnParam = new OracleParameter("nRETURN", OracleDbType.Int64, ParameterDirection.ReturnValue);
                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input);
                var queueIdParam = new OracleParameter("pQUEUEID", OracleDbType.Int64, pOrder.Id, ParameterDirection.Input);
                var codPedVendaParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pOrder.CodPedVenda, ParameterDirection.Input);
                var msErroOutParam = new OracleParameter("pMSERRO", OracleDbType.Clob, ParameterDirection.Output);

                cmd.Parameters.AddRange(new[] {
                    returnParam,
                    codeEstabParam,
                    queueIdParam,
                    codPedVendaParam,
                    msErroOutParam
                });

                await cmd.ExecuteNonQueryAsync();

                msErro = (((OracleClob)(msErroOutParam.Value)).Value).ToString();
                msErro = msErro == "*OK*" ? "" : msErro;
            }

            pOrder.MsErro = msErro;

            return pOrder;
        }

        public async Task ProcessOrderStatusAsync(OracleConnection conn, int pCodEstab, long pCodPedVenda, long pCodIntegracao)
        {
            using var cmd = new OracleCommand();
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_INTEGRACAO.PROCESSORDERSTATUS";

                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input);
                var queueIdParam = new OracleParameter("pIDINTEGRACAO", OracleDbType.Int64, pCodIntegracao, ParameterDirection.Input);
                var codPedParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pCodPedVenda, ParameterDirection.Input);
                

                cmd.Parameters.AddRange(new[] {
                    codeEstabParam,
                    queueIdParam,
                    codPedParam
                });
                await cmd.ExecuteNonQueryAsync();
            }

        }

        public async Task GenerateTaxValuesAsync(OracleConnection conn, int pCodEstab, long pCodPedVenda)
        {
            using var cmd = new OracleCommand();
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_INTEGRACAO.GERARIMPOSTOSPEDIDO";

                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input);
                var queueIdParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pCodPedVenda, ParameterDirection.Input);

                cmd.Parameters.AddRange(new[] {
                    codeEstabParam,
                    queueIdParam
                });
                await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task TotalizeOrderValuesAsync(OracleConnection conn, int pCodEstab, long pCodPedVenda)
        {
            using var cmd = new OracleCommand();
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_INTEGRACAO.TOTALIZAPEDIDO";

                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input);
                var queueIdParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pCodPedVenda, ParameterDirection.Input);

                cmd.Parameters.AddRange(new[] {
                    codeEstabParam,
                    queueIdParam
                });
                await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task GenerateOrderCommissionAsync(OracleConnection conn, int pCodEstab, long pCodPedVenda)
        {
            using var cmd = new OracleCommand();
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_INTEGRACAO.GERARCOMISSAOPEDIDO";

                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input);
                var queueIdParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pCodPedVenda, ParameterDirection.Input);

                cmd.Parameters.AddRange(new[] {
                    codeEstabParam,
                    queueIdParam
                });
                await cmd.ExecuteNonQueryAsync();
            }
        }

        public async Task<string> GetStatusPedidoAsync(OracleConnection conn, int pCodEstab, long pCodPedVenda)
        {
            var status = "P";
            using var cmd = new OracleCommand();
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT VEPEDVENDA.STATUS
                                      FROM VEPEDVENDA
                                     WHERE VEPEDVENDA.CODESTAB    = :pCODESTAB
                                       AND VEPEDVENDA.CODPEDVENDA = :pCODPEDVENDA";

                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input);
                var codPedVendaParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pCodPedVenda, ParameterDirection.Input);

                cmd.Parameters.AddRange(new[] {
                    codeEstabParam,
                    codPedVendaParam
                });

                try
                {
                    status = (await cmd.ExecuteScalarAsync()).ToString();
                }
                catch (Exception ex)
                {
                    status = "P";
                }
                
            }
            return status;
        }

        public async Task<List<VEItemPedImp>> LoadOrderTaxesAsync(OracleConnection conn, int pCodEstab, long pCodPedVenda)
        {
            var Taxes = new List<VEItemPedImp>();
            using var cmd = new OracleCommand();
            {
                cmd.Connection = conn;
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT FSIMPOSTOS.CLASSIFICACAO, 
                                           VEITEMPEDIMP.ALIQUOTA, VEITEMPEDIMP.BASECALCULO, VEITEMPEDIMP.CODESTAB, 
                                           FSIMPOSTOS.CODIMPOSTO, VEITEMPEDIMP.CODPEDVENDA, VEITEMPEDIMP.CODPRODUTO, 
                                           VEITEMPEDIMP.MVA, VEITEMPEDIMP.PERREDBC, VEITEMPEDIMP.VALORIMPOSTO, 
                                           VEITEMPEDIMP.VALORISENTO, VEITEMPEDIMP.VALOROBSERVACAO, VEITEMPEDIMP.VALOROUTROS, 
                                           VEITEMPEDIMP.ZONAFRANCA, NVL(FSPRODUTOPARAMESTAB.IDAPI, 0) PRODUCTID,
                                           VEITEMPEDIDO.QUANTIDADE
                                      FROM VEITEMPEDIMP,
                                           FSIMPOSTOS,
                                           FSPRODUTOPARAMESTAB,
                                           VEITEMPEDIDO
                                     WHERE VEITEMPEDIMP.CODESTAB          = :pCODESTAB
                                       AND VEITEMPEDIMP.CODPEDVENDA       = :pCODPEDVENDA
                                       AND FSIMPOSTOS.CODIMPOSTO          = VEITEMPEDIMP.CODIMPOSTO
                                       AND FSPRODUTOPARAMESTAB.CODPRODUTO = VEITEMPEDIMP.CODPRODUTO
                                       AND FSPRODUTOPARAMESTAB.CODESTAB   = VEITEMPEDIMP.CODESTAB
                                       AND VEITEMPEDIDO.CODESTAB          = VEITEMPEDIMP.CODESTAB
                                       AND VEITEMPEDIDO.CODPEDVENDA       = VEITEMPEDIMP.CODPEDVENDA
                                       AND VEITEMPEDIDO.CODPRODUTO        = VEITEMPEDIMP.CODPRODUTO
                                     ORDER BY NVL(FSPRODUTOPARAMESTAB.IDAPI, 0)";

                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input);
                var codPedVendaParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pCodPedVenda, ParameterDirection.Input);

                cmd.Parameters.AddRange(new[] {
                    codeEstabParam,
                    codPedVendaParam
                });

                using (var r = await cmd.ExecuteReaderAsync())
                {
                    while (r.Read())
                    {
                        Taxes.Add(new VEItemPedImp
                        {
                            ProductId = Convert.ToInt64(r["PRODUCTID"]),
                            codpedvenda = Convert.ToInt64(r["CODPEDVENDA"]),
                            codestab = Convert.ToInt32(r["CODESTAB"]),
                            codproduto = Convert.ToString(r["CODPRODUTO"]),
                            codimposto = Convert.ToInt16(r["CLASSIFICACAO"]),
                            zonafranca = Convert.ToDecimal(r["ZONAFRANCA"]),
                            aliquota = Convert.ToDecimal(r["ALIQUOTA"]),
                            basecalculo = Convert.ToDecimal(r["BASECALCULO"]),
                            valorimposto = Convert.ToDecimal(r["VALORIMPOSTO"]),
                            valorisento = Convert.ToDecimal(r["VALORISENTO"]),
                            valoroutros = Convert.ToDecimal(r["VALOROUTROS"]),
                            valorobservacao = Convert.ToDecimal(r["VALOROBSERVACAO"]),
                            mva = Convert.ToDecimal(r["MVA"]),
                            perredbc = Convert.ToDecimal(r["PERREDBC"]),
                            Quantidade = Convert.ToDecimal(r["QUANTIDADE"]),
                        });
                    }
                }
            }
            return Taxes;
        }

        public async Task<List<VEItemPedido>> LoadOrderCommissionAsync(DbConnection conn, int pCodEstab, long pCodPedVenda)
        {
            var Items = new List<VEItemPedido>();
            using var cmd = conn.CreateCommand();
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT VEITEMPEDIDO.CODPEDVENDA, VEITEMPEDIDO.CODESTAB,
                                           VEITEMPEDIDO.CODPRODUTO, VEITEMPEDIDO.PERCOMISSAO,
                                           NVL(FSPRODUTOPARAMESTAB.IDAPI, 0) PRODUCTID
                                      FROM VEITEMPEDIDO,
                                           FSPRODUTOPARAMESTAB
                                     WHERE VEITEMPEDIDO.CODESTAB = :pCODESTAB
                                       AND VEITEMPEDIDO.CODPEDVENDA = :pCODPEDVENDA
                                       AND FSPRODUTOPARAMESTAB.CODESTAB = VEITEMPEDIDO.CODESTAB
                                       AND FSPRODUTOPARAMESTAB.CODPRODUTO = VEITEMPEDIDO.CODPRODUTO";

                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input);
                var codPedVendaParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pCodPedVenda, ParameterDirection.Input);

                cmd.Parameters.AddRange(new[] {
                    codeEstabParam,
                    codPedVendaParam
                });

                using (var r = await cmd.ExecuteReaderAsync())
                {
                    while (r.Read())
                    {
                        Items.Add(new VEItemPedido
                        {
                            ProductId = Convert.ToInt64(r["PRODUCTID"]),
                            codpedvenda = Convert.ToInt64(r["CODPEDVENDA"]),
                            codestab = Convert.ToInt32(r["CODESTAB"]),
                            codproduto = Convert.ToString(r["CODPRODUTO"]),
                            percomissao = (float)Convert.ToDecimal(r["PERCOMISSAO"])
                        });
                    }
                }
            }
            return Items;
        }

        public async Task<List<VETransactions>> LoadOrderTransactionsAsync(DbConnection conn, int pCodEstab, long pCodPedVenda)
        {
            var Transactions = new List<VETransactions>();
            using var cmd = conn.CreateCommand();
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT VETRANSACOES.CODESTAB, VETRANSACOES.CODTRANSACAO,
                                           VETRANSACOES.UNIQUEKEY, VETRANSACOES.IDAPI
                                      FROM VETRANSACOES
                                     WHERE VETRANSACOES.CODESTAB    = :pCODESTAB
                                       AND VETRANSACOES.CODPEDVENDA = :pCODPEDVENDA";

                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input);
                var codPedVendaParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pCodPedVenda, ParameterDirection.Input);

                cmd.Parameters.AddRange(new[] {
                    codeEstabParam,
                    codPedVendaParam
                });

                using (var r = await cmd.ExecuteReaderAsync())
                {
                    while (r.Read())
                    {
                        Transactions.Add(new VETransactions
                        {
                            Id = Convert.ToInt64(r["IDAPI"]),
                            IdERP = Convert.ToInt64(r["CODTRANSACAO"]),
                            UniqueKey = r["UNIQUEKEY"].ToString()
                        });
                    }
                }
            }
            return Transactions;
        }
        
        public async Task SincTypeSaleAsync(string pEstablishmentKey)
        {
            var ERPContextFactory = new ERPContextFactory();
            var SalesforceContextFactory = new SalesforceContextFactory();
            LogData.Info($"Sincronizando os tipos de vendas.");

            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(pEstablishmentKey);

            using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
            {
                var TiposVenda = ERPContext.VEVWSincTipoVenda
                    .ToList();

                using (var SalesforceContext = SalesforceContextFactory.CreateDbContext(AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters()))
                {
                    foreach (var Tipo in TiposVenda)
                    {
                        var OldTypeSale = SalesforceContext.VETypeSale.FirstOrDefault(r => 
                            r.IdERP == Tipo.CodTipoVenda &&
                            r.EstablishmentKey == pEstablishmentKey);

                        var TypeSale = new VETypeSale();
                        if (OldTypeSale != null)
                            TypeSale = OldTypeSale;

                        TypeSale.Description = Tipo.Descricao;
                        TypeSale.IdERP = Tipo.CodTipoVenda;

                        if (OldTypeSale == null)
                        {
                            TypeSale.Id = ERPContextFactory.GetNextSequence(SalesforceContext, "VETYPESALE");
                            TypeSale.UniqueKey = Guid.NewGuid().ToString();
                            TypeSale.Created = DateTime.Now;
                            TypeSale.Modified = DateTime.Now;
                            TypeSale.EstablishmentKey = pEstablishmentKey;
                            TypeSale.UniqueKey = Guid.NewGuid().ToString();
                            TypeSale.IsActive = true;
                            SalesforceContext.VETypeSale.Add(TypeSale);
                        }
                        else
                            SalesforceContext.Entry(TypeSale).State = EntityState.Modified;

                        Tipo.IdApi = TypeSale.Id;
                        Tipo.UniqueKey = TypeSale.UniqueKey;
                        await ERPContext.Database.ExecuteSqlRawAsync(@"UPDATE VETIPOVENDA
                                                                          SET UNIQUEKEY    = NVL(UNIQUEKEY, :pUNIQUEKEY),
                                                                              IDAPI        = :pIDAPI
                                                                        WHERE CODTIPOVENDA = :pCODTIPOVENDA",

                           new OracleParameter("pUNIQUEKEY", OracleDbType.Varchar2, Tipo.UniqueKey, ParameterDirection.Input),
                           new OracleParameter("pIDAPI", OracleDbType.Int64, Tipo.IdApi, ParameterDirection.Input),
                           new OracleParameter("pCODTIPOVENDA", OracleDbType.Int64, Tipo.CodTipoVenda, ParameterDirection.Input));
                    }
                    await SalesforceContext.SaveChangesAsync();
                }
            }
        }

        public async Task<CROrders> SaveToERPCROrdersAsync(GEQueueItem QueueItem)
        {
            var ERPContextFactory = new ERPContextFactory();
            var SalesforceContextFactory = new SalesforceContextFactory();
            LogData.Info($"Realizando conexao com o banco de dados do Portal.");

            VEOrders Order;
            CROrders NewOrder = null;
            using (var SalesforceContext = SalesforceContextFactory.CreateDbContext(AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters()))
            {
                LogData.Info("Buscando dados do pedido.");
                Order = SalesforceContext
                     .VEOrders
                     .AsNoTracking()
                     .Include(order => order.VETransactions)
                     .Include(order => order.VEOrderItems)
                     .Include(order => order.VETypeSale)
                     .Include(enterprise => enterprise.GEEnterprises)
                     .Include(files => files.GEFilesPath)
                     .Include("VEOrderItems.FSFiscalOper")
                     .FirstOrDefault(r =>
                     r.Id == QueueItem.IdReference &&
                     r.EstablishmentKey == QueueItem.EstablishmentKey);
            }

            if (Order == null)
            {
                QueueItem.Processed = true;
                QueueItem.Uploaded = false;
                QueueItem.Note = "Pedido não encontrado.";
                return null;
            }

            await SincTypeSaleAsync(QueueItem.EstablishmentKey);

            LogData.Info("Dados do pedido encontrados.");
            LogData.Info("Salvando na tabela intermediária.");

            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(QueueItem.EstablishmentKey);

            LogData.Info($"Realizando conexao com o banco de dados do ERP ({ERPDatabase.Description}).");
            using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
            {
                LogData.Info($"Verificando se a empresa do pedido: {Order.Id} existe no ERP.");
                var EnterpriseService = new EnterpriseService(ERPContext, AppSettings);
                var exists = false;
                using (var conn = new OracleConnection(ERPContext.Database.GetConnectionString()))
                {
                    conn.Open();
                    exists = await EnterpriseService.EnterpriseExists(conn, Order.GEEnterprises.Id, ERPDatabase.CodEstab);
                    conn.Close();
                }

                if (!exists)
                {
                    LogData.Info($"Empresa: {Order.GEEnterprises.Id} não existe no ERP.");
                    LogData.Info("Salvando os dados da empresa.");
                    var QueueEnterprise = new GEQueueItem()
                    {
                        Id = 0,
                        EstablishmentKey = QueueItem.EstablishmentKey,
                        IdReference = Order.GEEnterprises.Id,
                        UniqueKey = Order.GEEnterprises.UniqueKey,
                        EntryName = Constants.GEEnterprises,
                        Type = "NEW",
                        Processed = false,
                        Uploaded = false,
                        IsActive = true,
                        Note = "",
                        ExceptionCode = "",
                        Created = DateTime.Now,
                        Modified = DateTime.Now
                    };

                    var QueueService = new QueueService(AppSettings);

                    QueueEnterprise = await QueueService.SaveNewQueueAsync(QueueEnterprise);

                    var NewEnterprise = await EnterpriseService.SaveToERPEnterpriseAsync(QueueEnterprise);
                    await QueueService.SaveChangesQueueAsync(QueueEnterprise);

                    Order.GEEnterprises.IdERP = NewEnterprise.IdERP;
                }

                try
                {
                    LogData.Info($"Verificando se o pedido:{Order.Id} existe na CROrders.");
                    var OldOrder = ERPContext.CROrders
                        .Include(order => order.VEOrderItems)
                        .Include(files => files.CROrderFiles)
                        .Where(r => r.Id == Order.Id).FirstOrDefault();

                    NewOrder = new CROrders();
                    if (OldOrder != null)
                        NewOrder = OldOrder;

                    NewOrder.QueueId = QueueItem.Id;
                    NewOrder.Id = Order.Id;
                    NewOrder.EstablishmentKey = Order.EstablishmentKey;
                    NewOrder.UniqueKey = Order.UniqueKey;
                    NewOrder.Created = Order.Created;
                    NewOrder.Modified = Order.Modified;
                    NewOrder.IsActive = Order.IsActive;
                    NewOrder.CodPedVenda = Order.CodPedVenda;
                    NewOrder.EnterpriseID = Order.EnterpriseID;
                    NewOrder.CarrierID = Order.CarrierID;
                    NewOrder.RedispatchID = Order.RedispatchID;
                    NewOrder.UserID = Order.UserID;
                    NewOrder.TypePaymentID = Order.TypePaymentID;
                    NewOrder.PayConditionID = Order.PayConditionID;
                    NewOrder.CodPedVenda = Order.CodPedVenda;
                    NewOrder.NumPedMob = Order.NumPedMob;
                    NewOrder.DeliveryDate = Order.DeliveryDate;
                    NewOrder.Note = Order.Note;
                    NewOrder.InnerNote = Order.InnerNote;
                    NewOrder.Status = Order.Status;
                    NewOrder.StatusCRM = Order.StatusCRM;
                    NewOrder.StatusSinc = (int)Order.StatusSinc;
                    NewOrder.IsProposal = Order.IsProposal;
                    NewOrder.FreightPaidBy = (int)Order.FreightPaidBy;
                    NewOrder.RedispatchPaidBy = (int)Order.RedispatchPaidBy;
                    NewOrder.FreightValue = Order.FreightValue;
                    NewOrder.ClientOrder = Order.ClientOrder;
                    if (NewOrder.IdERP <= 0)
                        NewOrder.IdERP = Order.IdERP;
                    NewOrder.OriginOrderID = Order.OriginOrderID;
                    NewOrder.MainOrderID = Order.MainOrderID;
                    NewOrder.OrderVersion = Order.OrderVersion;
                    NewOrder.PercDiscount = Order.PercDiscount;
                    NewOrder.PercCommission = Order.PercCommission;
                    NewOrder.ContactPhone = Order.ContactPhone;
                    NewOrder.ContactEmail = Order.ContactEmail;
                    NewOrder.Contact = Order.Contact;
                    NewOrder.Sector = (int)Order.Sector;
                    NewOrder.DigitizerID = Order.DigitizerID;
                    NewOrder.FinancialTaxes = Order.FinancialTaxes;
                    NewOrder.OnlyOnDate = Order.OnlyOnDate;
                    NewOrder.AllowPartial = Order.AllowPartial;
                    NewOrder.RevisionReason = Order.RevisionReason;
                    NewOrder.InPerson = Order.InPerson;
                    NewOrder.PaymentDueDate = Order.PaymentDueDate;
                    NewOrder.StatusPay = (int)Order.StatusPay;
                    NewOrder.PaidAmount = Order.PaidAmount;
                    NewOrder.AdvanceAmount = Order.AdvanceAmount;
                    NewOrder.TypeSaleId = Order.TypeSaleId;

                    if (OldOrder == null)
                    {
                        NewOrder.Id = Order.Id;
                        ERPContext.CROrders.Add(NewOrder);
                    }
                    else
                        ERPContext.Entry(NewOrder).State = EntityState.Modified;

                    LogData.Info($"Limpando os arquivos antigos do pedido:{Order.Id} na CROrderFiles.");
                    foreach (var item in NewOrder.CROrderFiles)
                        ERPContext.CROrderFiles.Remove(item);

                    LogData.Info($"Incluindo os arquivos novos do pedido:{Order.Id} na CROrderFiles.");
                    foreach (var item in Order.GEFilesPath)
                    {
                        var NewCROrderFiles = new CROrderFiles();
                        NewCROrderFiles.Id = item.Id;
                        NewCROrderFiles.OrderID = NewOrder.Id;
                        NewCROrderFiles.EstablishmentKey = Order.EstablishmentKey;
                        NewCROrderFiles.UniqueKey = item.UniqueKey;
                        NewCROrderFiles.Name = item.Name;
                        NewCROrderFiles.Path = item.Path;
                        NewCROrderFiles.Description = item.Description;
                        NewCROrderFiles.MimeType = item.MimeType;
                        NewCROrderFiles.Extension = item.Extension;
                        NewCROrderFiles.Identifier = item.Identifier;

                        NewOrder.CROrderFiles.Add(NewCROrderFiles);
                    }

                    LogData.Info($"Limpando os itens antigos do pedido:{Order.Id} na CROrderItems.");
                    foreach (var item in NewOrder.VEOrderItems)
                        ERPContext.CROrderItems.Remove(item);

                    LogData.Info($"Incluindo os itens novos do pedido:{Order.Id} na CROrderItems.");
                    foreach (var item in Order.VEOrderItems)
                    {
                        var NewOrderItems = new CROrderItems();
                        NewOrderItems.Id = item.Id;
                        NewOrderItems.EstablishmentKey = Order.EstablishmentKey;
                        NewOrderItems.UniqueKey = item.UniqueKey;
                        NewOrderItems.Created = item.Created;
                        NewOrderItems.Modified = item.Modified;
                        NewOrderItems.IsActive = item.IsActive;
                        NewOrderItems.OrderID = item.OrderID;
                        NewOrderItems.ProductID = item.ProductID;
                        NewOrderItems.FiscalOperID = item.FSFiscalOper.IdERP;
                        NewOrderItems.TableValue = item.TableValue;
                        NewOrderItems.Value = item.Value;
                        NewOrderItems.Quantity = item.Quantity;
                        NewOrderItems.QuantityReference = item.QuantityReference;
                        NewOrderItems.PercDiscount = item.PercDiscount;
                        NewOrderItems.Note = item.Note;
                        NewOrderItems.Item = item.Item;
                        NewOrderItems.ItemLevel = item.ItemLevel;
                        NewOrderItems.IdERP = item.IdERP;
                        NewOrderItems.ClientOrder = item.ClientOrder;
                        NewOrderItems.ClientItem = item.ClientItem;
                        NewOrderItems.DeliveryDate = item.DeliveryDate;
                        NewOrderItems.PercDiscountHead = item.PercDiscountHead;
                        NewOrderItems.PercCommission = item.PercCommission;
                        NewOrderItems.PercCommissionHead = item.PercCommissionHead;
                        NewOrderItems.ShippingDays = item.ShippingDays;
                        NewOrderItems.AltDescription = item.AltDescription;
                        NewOrder.VEOrderItems.Add(NewOrderItems);
                    }

                    LogData.Info($"Percorrendo as transações do pedido:{Order.Id}.");
                    foreach (var Transaction in Order.VETransactions)
                    {
                        var QueueTransaction = new GEQueueItem()
                        {
                            Id = 0,
                            EstablishmentKey = QueueItem.EstablishmentKey,
                            IdReference = Transaction.Id,
                            UniqueKey = Transaction.UniqueKey,
                            EntryName = Constants.VETransactions,
                            Type = "NEW",
                            Processed = false,
                            Uploaded = false,
                            IsActive = true,
                            Note = "",
                            ExceptionCode = "",
                            Created = DateTime.Now,
                            Modified = DateTime.Now
                        };

                        var QueueService = new QueueService(AppSettings);

                        QueueTransaction = await QueueService.SaveNewQueueAsync(QueueTransaction);

                        var OldTransaction = ERPContext.CRTransactions.Where(r => r.Id == Transaction.Id).FirstOrDefault();

                        LogData.Info($"Verificando se as transações do pedido:{Order.Id} já existem na CRTransactions.");
                        var NewTransaction = new CRTransactions();
                        if (OldTransaction != null)
                            NewTransaction = OldTransaction;

                        NewTransaction.Id = Transaction.Id;
                        NewTransaction.EstablishmentKey = Transaction.EstablishmentKey;
                        NewTransaction.UniqueKey = Transaction.UniqueKey;
                        NewTransaction.Created = Transaction.Created;
                        NewTransaction.Modified = Transaction.Modified;
                        NewTransaction.IsActive = Transaction.IsActive;
                        NewTransaction.Type = (int)Transaction.Type;
                        NewTransaction.OrderID = Transaction.OrderID;
                        NewTransaction.AuthCode = Transaction.AuthCode;
                        NewTransaction.AuthDate = Transaction.AuthDate;
                        NewTransaction.AcquirerName = Transaction.AcquirerName;
                        NewTransaction.NSU = Transaction.NSU;
                        NewTransaction.NSUHost = Transaction.NSUHost;
                        NewTransaction.AdminCode = Transaction.AdminCode;
                        NewTransaction.CardBrand = Transaction.CardBrand;
                        NewTransaction.CardLastDigits = Transaction.CardLastDigits;
                        NewTransaction.Installments = Transaction.Installments;
                        NewTransaction.Amount = Transaction.Amount;
                        NewTransaction.Description = Transaction.Description;
                        NewTransaction.Status = (int)Transaction.Status;
                        NewTransaction.MerchantReceipt = Transaction.MerchantReceipt;
                        NewTransaction.CustomerReceipt = Transaction.CustomerReceipt;
                        NewTransaction.Reversed = Transaction.Reversed;
                        NewTransaction.StoreId = Transaction.StoreId;
                        NewTransaction.TerminalId = Transaction.StoreId;
                        NewTransaction.Operator = Transaction.Operator;
                        NewTransaction.Origin = ((int)Transaction.Origin).ToString();
                        NewTransaction.QueueId = QueueTransaction.Id;

                        if (OldTransaction == null)
                        {
                            NewTransaction.Id = Transaction.Id;
                            ERPContext.CRTransactions.Add(NewTransaction);
                        }
                        else
                        {
                            ERPContext.Entry(NewTransaction).State = EntityState.Modified;
                        }

                        await ERPContext.SaveChangesAsync();

                        LogData.Info($"Processando as transações do pedido:{Order.Id}.");

                        await ERPContext.Database.ExecuteSqlRawAsync("BEGIN PCKG_INTEGRACAO.INSERTTRANSACTION(:pQUEUEID); END;",
                           new OracleParameter("pQUEUEID", OracleDbType.Int64, QueueTransaction.Id, ParameterDirection.Input));

                        var Item = await ERPContext.GEQueueItem.Where(r => r.Id == QueueTransaction.Id).FirstAsync();
                        QueueTransaction.Note = Item.Note ?? "Transação removida com sucesso.";
                        QueueTransaction.Processed = true;
                        QueueTransaction.Uploaded = true;

                        LogData.Info(QueueTransaction.Note);

                        await QueueService.SaveChangesQueueAsync(QueueTransaction);
                    }

                    QueueItem.Processed = true;
                    QueueItem.Uploaded = true;

                    LogData.Info("Salvando pedido na CRORDERS.");
                    await ERPContext.SaveChangesAsync();
                }
                catch (Exception ex)
                {
                    var ExceptionCode = Guid.NewGuid().ToString();

                    var ErrorLog = LogData.Error("Não foi possível salvar os dados na tabela intermediária.", ex, ExceptionCode);

                    QueueItem.Note = ErrorLog.SubStr(0, 255);
                    QueueItem.ExceptionCode = ExceptionCode;

                    return null;
                }
            }

            return NewOrder;
        }

        public async Task SaveToERPOrderProductsAsync(GEQueueItem QueueItem, int pCodEstab, long pOrderId)
        {
            var SalesforceContextFactory = new SalesforceContextFactory();
            var SalesforceDatabase = AppSettings.ServiceSettings.DataBaseSalesforce;

            using (var SalesforceContext = SalesforceContextFactory.CreateDbContext(SalesforceDatabase.GetParameters()))
            {
                var ProdService = new ProductService(AppSettings);
                var OrderItems = SalesforceContext.VEOrderItems.Where(r =>
                    r.OrderID == pOrderId &&
                    r.EstablishmentKey == QueueItem.EstablishmentKey)
                    .ToList();
                foreach (var item in OrderItems)
                {
                    LogData.Info($"Verificando se o produto: {item.ProductID} do pedido: {item.OrderID} existe no ERP");
                    var exists = ProdService.ExistsProductERP(item.EstablishmentKey, pCodEstab, item.ProductID);
                    var desc = exists ? "Já" : "Não";
                    LogData.Info($"Produto: {item.ProductID} do pedido: {item.OrderID} {desc} existe no ERP");

                    if (!exists)
                    {
                        var QueueProduct = new GEQueueItem()
                        {
                            Id = 0,
                            EstablishmentKey = QueueItem.EstablishmentKey,
                            IdReference = item.ProductID,
                            UniqueKey = item.UniqueKey,
                            EntryName = Constants.GEProducts,
                            Type = "NEW",
                            Processed = false,
                            Uploaded = false,
                            IsActive = true,
                            Note = "",
                            ExceptionCode = "",
                            Created = DateTime.Now,
                            Modified = DateTime.Now
                        };

                        var QueueService = new QueueService(AppSettings);

                        QueueProduct = await QueueService.SaveNewQueueAsync(QueueProduct);
                        await ProdService.SaveToERPCRProductsAsync(QueueProduct);
                        await QueueService.SaveChangesQueueAsync(QueueProduct);
                    }
                }
            }
        }

        public async Task<long> ProcessOrderAsync(GEQueueItem QueueItem, CROrders Order)
        {
            var ERPContextFactory = new ERPContextFactory();
            long CodPedVenda = -1;
            try
            {
                var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(QueueItem.EstablishmentKey);

                using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
                {
                    LogData.Info($"Buscando dados do pedido: {Order.Id} na CROrders");
                    var CrOrder = ERPContext.CROrders.FirstOrDefault(r => r.Id == Order.Id);
                    var msErro = "";
                    using var conn = new OracleConnection(ERPContext.Database.GetConnectionString());
                    {
                        conn.Open();
                        LogData.Info($"Gerando o cabeçalho do pedido: {Order.Id}");
                        var orderGenerated = await GenerateOrderCabecAsync(conn, ERPDatabase.CodEstab, CrOrder);
                        CROrders orderItemGenerated;
                        msErro = orderGenerated.MsErro;
                        CrOrder.CodPedVenda = orderGenerated.CodPedVenda;

                        if (msErro.IsEmpty())
                        {
                            LogData.Info($"Cabeçalho do pedido: {Order.Id} gerado. Ped venda: {CrOrder.CodPedVenda}");
                            LogData.Info($"Verificando se os produtos do pedido já foram integrados: {Order.Id}");

                            await SaveToERPOrderProductsAsync(QueueItem, ERPDatabase.CodEstab, CrOrder.Id);
                        }

                        if (msErro.IsEmpty())
                        {
                            LogData.Info($"Gerando os itens do pedido: {Order.Id}");
                            orderItemGenerated = await GenerateOrderItemsAsync(conn, ERPDatabase.CodEstab, CrOrder);
                        }

                        msErro = CrOrder.MsErro;

                        if (orderGenerated.CodPedVenda > 0 && msErro.IsEmpty())
                        {
                            CrOrder.IdERP = orderGenerated.CodPedVenda;

                            LogData.Info($"Gerando os impostos do pedido: {Order.Id}");
                            await GenerateTaxValuesAsync(conn, ERPDatabase.CodEstab, CrOrder.IdERP);

                            LogData.Info($"Totalizando os valores do pedido: {Order.Id}");
                            await TotalizeOrderValuesAsync(conn, ERPDatabase.CodEstab, CrOrder.IdERP);

                            LogData.Info($"Gerando a comissão do pedido: {Order.Id}");
                            await GenerateOrderCommissionAsync(conn, ERPDatabase.CodEstab, CrOrder.IdERP);

                            LogData.Info("Buscando impostos novos");
                            var Taxes = await LoadOrderTaxesAsync(conn, ERPDatabase.CodEstab, CrOrder.IdERP);

                            LogData.Info($"Impostos novos {Taxes.Count}");

                            LogData.Info($"Atualizando o status do pedido {CrOrder.IdERP} no ERP");
                            await ProcessOrderStatusAsync(conn, ERPDatabase.CodEstab, CrOrder.IdERP, CrOrder.Id);
                        }

                        conn.Close();
                    }

                    ERPContext.Entry(CrOrder).State = EntityState.Modified;
                    await ERPContext.SaveChangesAsync();

                    if (!msErro.IsEmpty())
                        throw new Exception(msErro.SubStr(0, 255));

                    CodPedVenda = CrOrder.CodPedVenda;
                    QueueItem.Note = $"Pedido {CodPedVenda} gerado";

                    LogData.Info(QueueItem.Note);
                }

                QueueItem.Processed = true;
                QueueItem.Uploaded = true;
                QueueItem.ExceptionCode = null;
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = LogData.Error("Não foi possível salvar os dados na tabela intermediária.", ex, ExceptionCode);

                QueueItem.Note = ErrorLog.SubStr(0, 255);
                QueueItem.ExceptionCode = ExceptionCode;
                return -1;
            }
            return CodPedVenda;
        }

        public async Task<bool> UpdateSalesforceOrderIDERPAsync(GEQueueItem QueueItem, long pCodPedVenda, CROrders Order)
        {
            var ERPContextFactory = new ERPContextFactory();
            var SalesforceContextFactory = new SalesforceContextFactory();

            if (pCodPedVenda <= 0)
            {
                QueueItem.Processed = true;
                QueueItem.Uploaded = true;
                QueueItem.Note = $"Não é permitido atualizar o  IDPORTAL: {Order.Id} com o IDERP: {pCodPedVenda}.";
                LogData.Info(QueueItem.Note);
                return false;
            }

            LogData.Info($"Atualizando o id do pedido gerado no portal IDERP: {pCodPedVenda} IDPORTAL: {Order.Id}.");

            using (var SalesforceContext = SalesforceContextFactory.CreateDbContext(AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters()))
            {
                LogData.Info("Buscando dados do pedido.");
                var VEOrder = SalesforceContext
                     .VEOrders
                     .Include(order => order.VETransactions)
                     .Include("VEOrderItems.VEOrderTaxes")
                     .FirstOrDefault(r =>
                        r.Id == Order.Id &&
                        r.EstablishmentKey == Order.EstablishmentKey);

                LogData.Info("Limpando os dados dos impostos anteriores do pedido.");
                foreach (var orderItem in VEOrder.VEOrderItems)
                {
                    foreach (var itemTaxes in orderItem.VEOrderTaxes)
                        SalesforceContext.VEOrderTaxes.Remove(itemTaxes);
                }

                foreach (var orderItem in VEOrder.VEOrderItems)
                    orderItem.VEOrderTaxes.Clear();

                var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(QueueItem.EstablishmentKey);

                List<VEItemPedImp> Taxes;
                List<VEItemPedido> Items;
                List<VETransactions> Transactions;
                var Status = "P";

                using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
                {
                    using var conn = new OracleConnection(ERPContext.Database.GetConnectionString());
                    {
                        conn.Open();
                        Taxes = await LoadOrderTaxesAsync(conn, ERPDatabase.CodEstab, pCodPedVenda);
                        Items = await LoadOrderCommissionAsync(conn, ERPDatabase.CodEstab, pCodPedVenda);
                        Transactions = await LoadOrderTransactionsAsync(conn, ERPDatabase.CodEstab, pCodPedVenda);
                        Status = await GetStatusPedidoAsync(conn, ERPDatabase.CodEstab, pCodPedVenda);
                        conn.Close();
                    }
                }

                LogData.Info($"Impostos encontrados: {Taxes.Count}");
                LogData.Info($"Comissões encontradas: {Items.Count}");

                if (Taxes.Any() || Items.Any())
                {
                    LogData.Info("Incluindo os novos impostos e os dados da comissão nos itens do pedido no portal.");
                    foreach (var orderItem in VEOrder.VEOrderItems)
                    {
                        var itemCommission = Items.FirstOrDefault(r => r.ProductId == orderItem.ProductID);

                        if (itemCommission != null)
                            orderItem.PercCommission = itemCommission.percomissao;

                        var ItemTaxes = Taxes.Where(r => r.ProductId == orderItem.ProductID);

                        LogData.Info($"{ItemTaxes.Count()} impostos do produto: {orderItem.ProductID}");
                        foreach (var tax in ItemTaxes)
                        {
                            var OldTax = orderItem.VEOrderTaxes.FirstOrDefault(r => r.OrderItemID == orderItem.Id &&
                                r.Type == tax.codimposto);

                            if (OldTax != null)
                                orderItem.VEOrderTaxes.Remove(OldTax);

                            var NewOrderTaxes = new VEOrderTaxes
                            {
                                EstablishmentKey = orderItem.EstablishmentKey,
                                UniqueKey = Guid.NewGuid().ToString(),
                                Created = DateTime.Now,
                                Modified = DateTime.Now,
                                IsActive = true,
                                OrderItemID = orderItem.Id,
                                Type = tax.codimposto,
                                FreeZone = tax.zonafranca,
                                Aliquot = tax.aliquota,
                                Basis = tax.basecalculo,
                                Value = Math.Round(tax.valorimposto * ((Convert.ToDecimal(orderItem.Quantity) / tax.Quantidade)), 2),
                                ExemptValue = Math.Round(tax.valorisento * ((Convert.ToDecimal(orderItem.Quantity) / tax.Quantidade)), 2),
                                OtherValue = Math.Round(tax.valoroutros * ((Convert.ToDecimal(orderItem.Quantity) / tax.Quantidade)), 2),
                                NoteValue = Math.Round(tax.valorobservacao * ((Convert.ToDecimal(orderItem.Quantity) / tax.Quantidade)), 2),
                                MVA = tax.mva,
                                PerRedBC = tax.perredbc,
                                Id = ERPContextFactory.GetNextSequence(SalesforceContext, "VEORDERTAXES")
                            };

                            orderItem.VEOrderTaxes.Add(NewOrderTaxes);
                        }
                    }
                }

                if (Transactions.Any())
                {
                    foreach (var orderTransaction in VEOrder.VETransactions)
                    {
                        var Transaction = Transactions.FirstOrDefault(r => r.Id == orderTransaction.Id);
                        orderTransaction.IdERP = Transaction?.IdERP ?? 0;
                    }
                }

                VEOrder.CodPedVenda = pCodPedVenda;
                VEOrder.IdERP = pCodPedVenda;
                VEOrder.StatusSinc = EStatusSinc.Integrated;
                VEOrder.Status = Status;

                QueueItem.Processed = true;
                QueueItem.Uploaded = true;
                QueueItem.Note = $"Pedido {Order.Id} atualizado com sucesso no portal";

                LogData.Info(QueueItem.Note);

                await SalesforceContext.SaveChangesAsync();
            }
            return true;
        }

        private VESalePrice GetSalePriceDataToUpload(long pCodPrVenda, int pCodEstab, string pCodProduto, string pEstablishmentKey)
        {
            var ERPContextFactory = new ERPContextFactory();
            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(pEstablishmentKey);

            using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
            {
                var result = ERPContext.VEVWSincTabPreco.FirstOrDefault(r => r.CodEstab == pCodEstab &&
                                                                 r.CodProduto == pCodProduto &&
                                                                 r.CodPrvenda == pCodPrVenda);
                if (result != null)
                {
                    return new VESalePrice
                    {
                        Id = result.IDApi,
                        ProductKey = result.CodProduto,
                        CodPrVenda = result.CodPrvenda,
                        Description = result.Descricao,
                        EstablishmentKey = pEstablishmentKey,
                        ProductId = result.CodProdutoApi,
                        RegionId = result.CodRegiaoApi,
                        Value = result.ValorUnitario,
                        CodRegiao = result.CodRegiao,
                        UniqueKey = result.UniqueKey ?? pEstablishmentKey
                    };
                }
            }

            return null;
        }

        public async Task<bool> UploadVESincPrVendaAsync(VESincPrVenda pVESincPrVenda, string pEstablishmentKey, int pCodEstab)
        {
            var result = false;
            var ExceptionCode = "";
            try
            {
                LogData.Info($"Sincronizando dados do produto: { pVESincPrVenda.CodProduto } antes de sinc. a tabela de preço");
                var prodSinc = new FSSincProduto
                {
                    CodProduto = pVESincPrVenda.CodProduto
                };

                if (!pVESincPrVenda.FSProdutoParamEstab.PermSinc)
                {
                    LogData.Info($"Produto: { pVESincPrVenda.CodProduto } não está marcado para ser sincronizado (Produto|Permite sincronizar)");
                    throw new Exception($"Produto: { pVESincPrVenda.CodProduto } não está marcado para ser sincronizado (Produto|Permite sincronizar)");
                }

                var ProdService = new ProductService(AppSettings);
                prodSinc.Sincronizado = await ProdService.UploadFSSincProdutoAsync(prodSinc, pEstablishmentKey, pCodEstab);

                if (prodSinc.Sincronizado)
                {
                    var salePrice = GetSalePriceDataToUpload(pVESincPrVenda.CodPrVenda, pCodEstab, pVESincPrVenda.CodProduto, pEstablishmentKey);

                    if (salePrice == null)
                        throw new Exception($"Dados da tabela de preço: { pVESincPrVenda.CodPrVenda} - { pVESincPrVenda.CodProduto } não localizados.");

                    LogData.Info($"Sincronização dos dados do produto: { pVESincPrVenda.CodProduto } finalizada com sucesso");

                    LogData.Info($"Sincronizando tabela de preço do produto: { pVESincPrVenda.CodProduto } ");

                    var ERPContextFactory = new ERPContextFactory();
                    var SalesforceContextFactory = new SalesforceContextFactory();
                    LogData.Info($"Realizando conexao com o banco de dados do Portal.");

                    string UniqueKey;
                    long IdApi;
                    VESalePrice NewSalePrice = null;
                    using (var SalesforceContext = SalesforceContextFactory.CreateDbContext(AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters()))
                    {
                        LogData.Info("Buscando dados do preço de venda no portal.");
                        var OldSalePrice = SalesforceContext
                             .VESalePrice
                             .Include(price => price.GEProducts)
                             .FirstOrDefault(r =>
                                 r.ProductId == salePrice.ProductId &&
                                 r.CodPrVenda == salePrice.CodPrVenda &&
                                 r.RegionId == salePrice.RegionId &&
                                 r.EstablishmentKey == pEstablishmentKey);

                        NewSalePrice = new VESalePrice();
                        if (OldSalePrice != null)
                            NewSalePrice = OldSalePrice;

                        NewSalePrice.CodPrVenda = salePrice.CodPrVenda;
                        NewSalePrice.RegionId = salePrice.RegionId;
                        NewSalePrice.Description = salePrice.Description;
                        NewSalePrice.ProductId = salePrice.ProductId;
                        NewSalePrice.Value = salePrice.Value;

                        if (OldSalePrice == null)
                        {
                            NewSalePrice.UniqueKey = Guid.NewGuid().ToString();
                            NewSalePrice.EstablishmentKey = pEstablishmentKey;
                            NewSalePrice.Created = DateTime.Now;
                            NewSalePrice.Modified = DateTime.Now;
                            NewSalePrice.IsActive = true;
                            NewSalePrice.Id = ERPContextFactory.GetNextSequence(SalesforceContext, "VESALEPRICE");
                            SalesforceContext.VESalePrice.Add(NewSalePrice);
                        }
                        else
                        {
                            NewSalePrice.Modified = DateTime.Now;
                            SalesforceContext.Entry(NewSalePrice).State = EntityState.Modified;
                        }
                        IdApi = NewSalePrice.Id;
                        UniqueKey = NewSalePrice.UniqueKey;

                        LogData.Info("Salvando o item na tabela de preço do portal.");
                        await SalesforceContext.SaveChangesAsync();
                    }
                    var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(pEstablishmentKey);

                    LogData.Info("Atualizando o IDAPI e UniqueKey no ERP.");
                    using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
                    {
                        await ERPContext.Database.ExecuteSqlRawAsync(@"UPDATE VEPRVENDAPROD
                                                                          SET UNIQUEKEY  = NVL(UNIQUEKEY, :pUNIQUEKEY),
                                                                              IDAPI      = :pIDAPI
                                                                        WHERE CODESTAB   = :pCODESTAB
                                                                          AND CODPRODUTO = :pCODPRODUTO
                                                                          AND CODREGIAO  = :pCODREGIAO
                                                                          AND CODPRVENDA = :pCODPRVENDA",

                            new OracleParameter("pUNIQUEKEY", OracleDbType.Varchar2, UniqueKey, ParameterDirection.Input),
                            new OracleParameter("pIDAPI", OracleDbType.Int64, IdApi, ParameterDirection.Input),
                            new OracleParameter("pCODESTAB", OracleDbType.Int64, pCodEstab, ParameterDirection.Input),
                            new OracleParameter("pCODPRODUTO", OracleDbType.Varchar2, salePrice.ProductKey, ParameterDirection.Input),
                            new OracleParameter("pCODREGIAO", OracleDbType.Int64, salePrice.CodRegiao, ParameterDirection.Input),
                            new OracleParameter("pCODPRVENDA", OracleDbType.Int64, salePrice.CodPrVenda, ParameterDirection.Input));

                    }

                    result = true;
                }
                else
                {
                    result = false;
                    throw new Exception($"Produto: { pVESincPrVenda.CodProduto } não foi sincronizado com o portal.");
                }
            }
            catch (Exception ex)
            {
                ExceptionCode = ex.Message.Contains("não localizados") ? "not-found" : Guid.NewGuid().ToString();
                ExceptionCode = ex.Message.Contains("(Produto|Permite sincronizar)") ? "not-found" : ExceptionCode;
                ExceptionCode = ex.Message.Contains("não foi sincronizado com o portal") ? "not-found" : ExceptionCode;

                result = ExceptionCode == "not-found";

                LogData.Error("Não foi possível atualizar os dados na API.", ex, ExceptionCode);
            }

            return result;
        }

        private VEOrders GetOrderDataToUpload(int pCodEstab, long pCodPedVenda, string pEstablishmentKey)
        {
            var ERPContextFactory = new ERPContextFactory();
            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(pEstablishmentKey);

            using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
            {
                var result = ERPContext.VEVWSincCabecPedido.FirstOrDefault(
                    r => r.CodEstab == pCodEstab &&
                    r.IDERP == pCodPedVenda);
                if (result != null)
                {
                    var freightPaidBy = EFreightPaidBy.SemFrete;
                    switch (result.FreightPaidBy)
                    {
                        case 0:
                            freightPaidBy = EFreightPaidBy.Emitente;
                            break;
                        case 1:
                            freightPaidBy = EFreightPaidBy.Destinatario;
                            break;
                        case 2:
                            freightPaidBy = EFreightPaidBy.Terceiros;
                            break;
                        case 3:
                            freightPaidBy = EFreightPaidBy.ProprioEmitente;
                            break;
                        case 4:
                            freightPaidBy = EFreightPaidBy.ProprioDestinatario;
                            break;
                    }
                    var redispatchPaidBy = EFreightPaidBy.SemFrete;
                    switch (result.RedispatchPaidBy)
                    {
                        case 0:
                            redispatchPaidBy = EFreightPaidBy.Emitente;
                            break;
                        case 1:
                            redispatchPaidBy = EFreightPaidBy.Destinatario;
                            break;
                        case 2:
                            redispatchPaidBy = EFreightPaidBy.Terceiros;
                            break;
                        case 3:
                            redispatchPaidBy = EFreightPaidBy.ProprioEmitente;
                            break;
                        case 4:
                            redispatchPaidBy = EFreightPaidBy.ProprioDestinatario;
                            break;
                    }

                    var Order = new VEOrders
                    {
                        StatusSinc = EStatusSinc.Integrated,
                        EstablishmentKey = pEstablishmentKey,
                        UniqueKey = result.UniqueKey.IsEmpty() ? pEstablishmentKey : result.UniqueKey,
                        Id = result.ID,
                        IdERP = result.IDERP,
                        NumPedMob = 0,
                        Status = result.Status,
                        StatusCRM = result.StatusCRM,
                        CarrierID = result.CarrierID,
                        RedispatchID = result.RedispatchID,
                        ClientOrder = result.ClientOrder,
                        CodPedVenda = result.IDERP,
                        EnterpriseID = result.EnterpriseID,
                        IsProposal = result.IsProposal,
                        DeliveryDate = result.DeliveryDate,
                        FreightPaidBy = freightPaidBy,
                        RedispatchPaidBy = redispatchPaidBy,
                        FreightValue = result.FreightValue,
                        MainOrderID = result.MainOrderID,
                        UserID = result.UserID,
                        TypePaymentID = result.TypePaymentID,
                        OrderVersion = result.OrderVersion,
                        Note = result.Note,
                        OriginOrderID = result.OriginOrderID,
                        PayConditionID = result.PayConditionID,
                        PercCommission = result.PercCommission,
                        PercDiscount = result.PercDiscount,
                        InnerNote = result.InnerNote,
                        OnlyOnDate = result.SomenteData == 1,
                        AllowPartial = result.FaturarParc == 1,
                        FinancialTaxes = result.FinancialTaxes,
                        ContactPhone = result.ContatoFone,
                        ContactEmail = result.ContatoEmail,
                        Contact = result.Contato,
                        DigitizerID = result.DigitizerID,
                        Sector = (EContactSectors)result.Sector,
                        RevisionReason = result.RevisionReason,
                        InPerson = result.InPerson,
                        PaymentDueDate = result.PaymentDueDate,
                        StatusPay = (EStatusPay)result.StatusPay,
                        PaidAmount = result.PaidAmount,
                        AdvanceAmount = result.AdvanceAmount,
                        TypeSaleId = result.TypeSaleId
                    };

                    var Items = ERPContext.VEVWSincItemPedido.AsNoTracking().Where(r => r.IDERP == result.IDERP &&
                        r.CodEstab == pCodEstab).ToList();

                    foreach (var item in Items)
                    {
                        var NewOrderItem = new VEOrderItems
                        {
                            OrderID = Order.Id,
                            ProductID = item.ProductID,
                            FiscalOperID = item.FiscalOperID,
                            TableValue = item.TableValue,
                            Value = item.Value,
                            Quantity = item.Quantity,
                            QuantityReference = item.QuantityReference,
                            PercDiscount = item.PercDiscount,
                            Note = item.Note,
                            Item = item.Item,
                            ItemLevel = item.ItemLevel,
                            ClientOrder = item.ClientOrder,
                            ClientItem = item.ClientItem,
                            DeliveryDate = item.DeliveryDate,
                            PercCommission = item.PercCommission,
                            PercCommissionHead = item.PercCommissionHead,
                            PercDiscountHead = item.PercDiscountHead,
                            ShippingDays = item.ShippingDays,
                            AltDescription = item.AltDescription,
                            QuantityReturned = item.QuantityReturned,
                            IdERP = item.IDERP,
                            Id = item.ID,
                            EstablishmentKey = pEstablishmentKey,
                            UniqueKey = item.UniqueKey.IsEmpty() ? pEstablishmentKey : item.UniqueKey,
                            FSFiscalOper = new FSFiscalOper
                            {
                                Id = 0,
                                IdERP = item.FiscalOperID,
                                EstablishmentKey = pEstablishmentKey,
                                UniqueKey = Guid.NewGuid().ToString(),
                                Description = item.FSNatOperDescricao,
                                IsBonification = item.FSNatOperBonificacao,
                                IsSample = item.FSNatOperAmostra
                            }
                        };

                        Order.VEOrderItems.Add(NewOrderItem);
                    }

                    return Order;
                }
            }

            return null;
        }

        public async Task<bool> UploadVEPedidoSincAsync(VEPedidoSinc pVEPedidoSinc, string pEstablishmentKey, int pCodEstab)
        {
            var result = false;
            var ExceptionCode = "";
            try
            {
                if (pVEPedidoSinc.Tipo == 0)
                {
                    LogData.Info($"Subindo dados do pedido: {pVEPedidoSinc.CodPedVenda} para o portal.");

                    var OrderERP = GetOrderDataToUpload(pCodEstab, pVEPedidoSinc.CodPedVenda, pEstablishmentKey);
                    if (OrderERP == null)
                        throw new Exception($"Dados do pedido: {pVEPedidoSinc.CodPedVenda} não localizados.");

                    var ERPContextFactory = new ERPContextFactory();
                    var SalesforceContextFactory = new SalesforceContextFactory();
                    LogData.Info($"Realizando conexao com o banco de dados do Portal.");

                    VEOrders OrderPortal = null;
                    using (var SalesforceContext = SalesforceContextFactory.CreateDbContext(AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters()))
                    {
                        LogData.Info("Buscando dados do pedido de venda no portal.");
                        OrderPortal = SalesforceContext
                             .VEOrders
                             .Include(order => order.VEOrderItems)
                             .FirstOrDefault(r =>
                                r.IdERP == OrderERP.IdERP &&
                                r.EstablishmentKey == pEstablishmentKey);

                        if (OrderPortal == null)
                            throw new Exception($"Dados do pedido: {pVEPedidoSinc.CodPedVenda} não localizados no portal.");

                        LogData.Info("Atualizando o pedido de venda no portal.");
                        if (OrderERP.DeliveryDate != OrderPortal.DeliveryDate)
                            OrderPortal.DeliveryDate = OrderERP.DeliveryDate;

                        if (OrderERP.FreightPaidBy != OrderPortal.FreightPaidBy)
                        {
                            OrderPortal.FreightPaidBy = OrderERP.FreightPaidBy;
                            OrderPortal.CarrierID = OrderERP.CarrierID;
                        }

                        if (OrderERP.RedispatchPaidBy != OrderPortal.RedispatchPaidBy)
                        {
                            OrderPortal.RedispatchPaidBy = OrderERP.RedispatchPaidBy;
                            OrderPortal.RedispatchID = OrderERP.RedispatchID;
                        }

                        if (OrderERP.Status != OrderPortal.Status)
                            OrderPortal.Status = OrderERP.Status;

                        if (OrderERP.Status.In("O", "R")) // Concluido ou reprovado, sobrescreve o status do portal
                            OrderPortal.StatusCRM = OrderERP.Status;
                        else if (OrderERP.Status.In("A", "M", "E")) // Aprovado, andamento e análise, mantém o pedido como aprovado no CRM
                            OrderPortal.StatusCRM = "A";
                        else if (OrderPortal.StatusCRM.In("O", "R") && !OrderERP.Status.In("O", "R")) // Se o portal estiver concluido ou encerrado e o ERP não está mais
                            OrderPortal.StatusCRM = "A";

                        if (OrderERP.TypePaymentID != OrderPortal.TypePaymentID)
                            OrderPortal.TypePaymentID = OrderERP.TypePaymentID;

                        if (OrderERP.PayConditionID != OrderPortal.PayConditionID)
                            OrderPortal.PayConditionID = OrderERP.PayConditionID;

                        if (OrderERP.ClientOrder != OrderPortal.ClientOrder)
                            OrderPortal.ClientOrder = OrderERP.ClientOrder;

                        if (OrderERP.OnlyOnDate != OrderPortal.OnlyOnDate)
                            OrderPortal.OnlyOnDate = OrderERP.OnlyOnDate;

                        if (OrderERP.AllowPartial != OrderPortal.AllowPartial)
                            OrderPortal.AllowPartial = OrderERP.AllowPartial;

                        if (OrderERP.Note != OrderPortal.Note)
                            OrderPortal.Note = OrderERP.Note;

                        if (OrderERP.InnerNote != OrderPortal.InnerNote)
                            OrderPortal.InnerNote = OrderERP.InnerNote;

                        if (OrderERP.TypeSaleId != OrderPortal.TypeSaleId)
                            OrderPortal.TypeSaleId = OrderERP.TypeSaleId;

                        if (OrderERP.StatusPay != OrderPortal.StatusPay)
                            OrderPortal.StatusPay = OrderERP.StatusPay;

                        if (OrderERP.PaidAmount != OrderPortal.PaidAmount)
                            OrderPortal.PaidAmount = OrderERP.PaidAmount;

                        if (OrderERP.AdvanceAmount != OrderPortal.AdvanceAmount)
                            OrderPortal.PaidAmount = OrderERP.PaidAmount;

                        foreach (var itemERP in OrderERP.VEOrderItems)
                        {
                            var ItemsPortal = OrderPortal.VEOrderItems.Where(r => r.OrderID == OrderPortal.Id && r.Id == itemERP.Id);

                            foreach (var ItemPortal in ItemsPortal)
                            {
                                if (itemERP.ClientOrder != ItemPortal.ClientOrder)
                                    ItemPortal.ClientOrder = itemERP.ClientOrder;

                                if (itemERP.ClientItem != ItemPortal.ClientItem)
                                    ItemPortal.ClientItem = itemERP.ClientItem;

                                if (itemERP.PercCommission != ItemPortal.PercCommission)
                                    ItemPortal.PercCommission = itemERP.PercCommission;

                                if (itemERP.AltDescription != ItemPortal.AltDescription)
                                    ItemPortal.AltDescription = itemERP.AltDescription;

                                if (itemERP.Note != ItemPortal.Note)
                                    ItemPortal.Note = itemERP.Note;

                                if (itemERP.Quantity != ItemPortal.Quantity)
                                    ItemPortal.Quantity = itemERP.Quantity;

                                if (itemERP.QuantityReference != ItemPortal.QuantityReference)
                                    ItemPortal.QuantityReference = itemERP.QuantityReference;

                                if (itemERP.QuantityReturned != ItemPortal.QuantityReturned)
                                    ItemPortal.QuantityReturned = itemERP.QuantityReturned;
                            }
                        }

                        SalesforceContext.Entry(OrderPortal).State = EntityState.Modified;

                        LogData.Info("Salvando alterações no pedido de venda no portal.");
                        await SalesforceContext.SaveChangesAsync();
                    }
                }
                else if (pVEPedidoSinc.Tipo == 1)
                {
                    LogData.Info($"Atualizando os dados do pedido: {pVEPedidoSinc.CodPedVenda} para não sincronizado no portal.");

                    var ERPContextFactory = new ERPContextFactory();
                    var SalesforceContextFactory = new SalesforceContextFactory();
                    LogData.Info($"Realizando conexao com o banco de dados do Portal.");
                    VEOrders OrderPortal = null;
                    using (var SalesforceContext = SalesforceContextFactory.CreateDbContext(AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters()))
                    {
                        LogData.Info("Buscando dados do pedido de venda no portal.");
                        OrderPortal = SalesforceContext
                             .VEOrders
                             .Include(order => order.VEOrderItems)
                             .FirstOrDefault(r =>
                                r.IdERP == pVEPedidoSinc.CodPedVenda &&
                                r.EstablishmentKey == pEstablishmentKey);

                        if (OrderPortal == null)
                            throw new Exception($"Dados do pedido: {pVEPedidoSinc.CodPedVenda} não localizados no portal.");


                        LogData.Info("Atualizando o pedido de venda no portal.");

                        OrderPortal.StatusSinc = EStatusSinc.Waiting;
                        OrderPortal.Modified = DateTime.Now;
                        OrderPortal.IdERP = 0;

                        SalesforceContext.Entry(OrderPortal).State = EntityState.Modified;

                        LogData.Info("Salvando alterações no pedido de venda no portal.");
                        await SalesforceContext.SaveChangesAsync();
                    }
                }

                result = true;
            }
            catch (Exception ex)
            {
                ExceptionCode = ex.Message.Contains("não localizados") ? "not-found" : Guid.NewGuid().ToString();

                LogData.Error("Não foi possível atualizar os dados na API.", ex, ExceptionCode);

                result = ExceptionCode == "not-found";
            }

            return result;
        }

        private VETransactions GetTransactionDataToUpload(int pCodEstab, long pCodTransacoes, string pEstablishmentKey)
        {
            var ERPContextFactory = new ERPContextFactory();
            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(pEstablishmentKey);

            using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
            {
                var result = ERPContext.VEVWSincTransacoes.FirstOrDefault(r =>
                    r.CodEstab == pCodEstab &&
                    r.CodTransacao == pCodTransacoes);

                if (result != null)
                {
                    return new VETransactions
                    {
                        Id = result.IdApi,
                        AcquirerName = result.AcquirerName,
                        AdminCode = result.AdminCode,
                        Amount = result.Valor,
                        AuthCode = result.AuthCode,
                        AuthDate = result.AuthDate,
                        CardBrand = result.CardBrand,
                        CardLastDigits = result.CardLastDigits,
                        CustomerReceipt = result.CustomerReceipt,
                        Description = result.Descricao,
                        EstablishmentKey = pEstablishmentKey,
                        Installments = result.Parcelas,
                        IsActive = true,
                        MerchantReceipt = result.MerchantReceipt,
                        NSU = result.Nsu,
                        IdERP = result.CodTransacao,
                        OrderID = result.OrderId,
                        Origin = (ETransactionOrigin)result.Origem,
                        Reversed = result.Estornado == 1,
                        Status = (ETransactionStatus)result.Status,
                        StatusSinc = EStatusSinc.Sincronized,
                        Type = (ETransactionType)result.Tipo,
                        UniqueKey = result.UniqueKey
                    };
                }
            }

            return null;
        }

        public async Task<bool> UploadVESincTransacoesAsync(VESincTransacoes pVESincTransacoes, string pEstablishmentKey, int pCodEstab)
        {
            var result = false;
            var ExceptionCode = "";
            try
            {
                var TransactionsERP = GetTransactionDataToUpload(pCodEstab, pVESincTransacoes.CodTransacao, pEstablishmentKey);
                if (TransactionsERP == null)
                    throw new Exception($"Dados da transação: {pVESincTransacoes.CodTransacao} não localizados.");

                var ERPContextFactory = new ERPContextFactory();
                var SalesforceContextFactory = new SalesforceContextFactory();
                LogData.Info($"Realizando conexao com o banco de dados do Portal.");

                VETransactions TransactionPortal = null;
                using (var SalesforceContext = SalesforceContextFactory.CreateDbContext(AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters()))
                {
                    LogData.Info("Buscando dados do pedido de venda no portal.");
                    TransactionPortal = SalesforceContext
                         .VETransactions
                         .FirstOrDefault(r =>
                            r.IdERP == TransactionsERP.IdERP &&
                            r.EstablishmentKey == pEstablishmentKey);

                    if (TransactionPortal == null)
                        throw new Exception($"Dados da transação: {pVESincTransacoes.CodTransacao} não localizados no portal.");

                    if (TransactionsERP.Status != TransactionPortal.Status)
                        TransactionPortal.Status = TransactionsERP.Status;

                    if (TransactionsERP.Type != TransactionPortal.Type)
                        TransactionPortal.Type = TransactionsERP.Type;

                    if (TransactionsERP.AuthCode != TransactionPortal.AuthCode)
                        TransactionPortal.AuthCode = TransactionsERP.AuthCode;

                    if (TransactionsERP.AuthDate != TransactionPortal.AuthDate)
                        TransactionPortal.AuthDate = TransactionsERP.AuthDate;

                    if (TransactionsERP.AcquirerName != TransactionPortal.AcquirerName)
                        TransactionPortal.AcquirerName = TransactionsERP.AcquirerName;

                    if (TransactionsERP.NSU != TransactionPortal.NSU)
                        TransactionPortal.NSU = TransactionsERP.NSU;

                    if (TransactionsERP.NSUHost != TransactionPortal.NSUHost)
                        TransactionPortal.NSUHost = TransactionsERP.NSUHost;

                    if (TransactionsERP.AdminCode != TransactionPortal.AdminCode)
                        TransactionPortal.AdminCode = TransactionsERP.AdminCode;

                    if (TransactionsERP.CardBrand != TransactionPortal.CardBrand)
                        TransactionPortal.CardBrand = TransactionsERP.CardBrand;

                    if (TransactionsERP.CardLastDigits != TransactionPortal.CardLastDigits)
                        TransactionPortal.CardLastDigits = TransactionsERP.CardLastDigits;

                    if (TransactionsERP.Installments != TransactionPortal.Installments)
                        TransactionPortal.Installments = TransactionsERP.Installments;

                    if (TransactionsERP.Amount != TransactionPortal.Amount)
                        TransactionPortal.Amount = TransactionsERP.Amount;

                    if (TransactionsERP.Description != TransactionPortal.Description)
                        TransactionPortal.Description = TransactionsERP.Description;

                    if (TransactionsERP.CustomerReceipt != TransactionPortal.CustomerReceipt)
                        TransactionPortal.CustomerReceipt = TransactionsERP.CustomerReceipt;

                    if (TransactionsERP.MerchantReceipt != TransactionPortal.MerchantReceipt)
                        TransactionPortal.MerchantReceipt = TransactionsERP.MerchantReceipt;

                    if (TransactionsERP.TerminalId != TransactionPortal.TerminalId)
                        TransactionPortal.TerminalId = TransactionsERP.TerminalId;

                    if (TransactionsERP.Operator != TransactionPortal.Operator)
                        TransactionPortal.Operator = TransactionsERP.Operator;

                    if (TransactionsERP.StoreId != TransactionPortal.StoreId)
                        TransactionPortal.StoreId = TransactionsERP.StoreId;

                    if (TransactionsERP.Reversed != TransactionPortal.Reversed)
                        TransactionPortal.Reversed = TransactionsERP.Reversed;

                    if (TransactionsERP.IdERP != TransactionPortal.IdERP)
                        TransactionPortal.IdERP = TransactionsERP.IdERP;

                    TransactionPortal.StatusSinc = EStatusSinc.Integrated;
                    TransactionPortal.IdERP = TransactionsERP.IdERP;

                    SalesforceContext.Entry(TransactionPortal).State = EntityState.Modified;

                    await SalesforceContext.SaveChangesAsync();
                }

                result = true;
            }
            catch (Exception ex)
            {
                ExceptionCode = ex.Message.Contains("não localizados") ? "not-found" : Guid.NewGuid().ToString();

                LogData.Error("Não foi possível atualizar os dados na API.", ex, ExceptionCode);

                result = ExceptionCode == "not-found";
            }

            return result;
        }

        public async Task RemoveTransactionFromERPAsync(GEQueueItem QueueItem)
        {
            var ERPContextFactory = new ERPContextFactory();
            var SalesforceContextFactory = new SalesforceContextFactory();

            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(QueueItem.EstablishmentKey);

            using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
            {
                await ERPContext.Database.ExecuteSqlRawAsync("BEGIN PCKG_INTEGRACAO.DELETETRANSACTION(:pQUEUEID); END;",
                   new OracleParameter("pQUEUEID", OracleDbType.Int64, QueueItem.Id, ParameterDirection.Input));

                var Item = await ERPContext.GEQueueItem.Where(r => r.Id == QueueItem.Id).FirstAsync();
                QueueItem.Note = Item.Note;
                QueueItem.Processed = true;
                QueueItem.Uploaded = true;
            }
        }

        public async Task<CRSalePrice> SaveToERPCRSalePriceAsync(GEQueueItem QueueItem)
        {
            var ERPContextFactory = new ERPContextFactory();
            var SalesforceContextFactory = new SalesforceContextFactory();
            LogData.Info($"Realizando conexao com o banco de dados do Portal.");

            VESalePrice SalePrice;
            CRSalePrice NewSalePrice = null;
            using (var SalesforceContext = SalesforceContextFactory.CreateDbContext(AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters()))
            {
                LogData.Info("Buscando dados da tabela de preço.");
                SalePrice = SalesforceContext
                     .VESalePrice
                     .AsNoTracking()
                     .Include(price => price.GEProducts)
                     .FirstOrDefault(r =>
                     r.Id == QueueItem.IdReference &&
                     r.EstablishmentKey == QueueItem.EstablishmentKey);
            }

            if (SalePrice == null)
            {
                QueueItem.Processed = true;
                QueueItem.Uploaded = false;
                QueueItem.Note = "tabela de preço não encontrada.";
                return null;
            }

            LogData.Info("Dados da tabela de preço encontradoa.");
            LogData.Info("Salvando na tabela intermediária.");

            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(QueueItem.EstablishmentKey);

            LogData.Info($"Realizando conexao com o banco de dados do ERP ({ERPDatabase.Description}).");
            using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
            {
                try
                {
                    LogData.Info($"Verificando se a tabela de preço:{SalePrice.Id} existe na CRSalePrice.");
                    var OldSalePrice = ERPContext.CRSalePrice
                        .Where(r => r.Id == SalePrice.Id).FirstOrDefault();

                    NewSalePrice = new CRSalePrice();
                    if (OldSalePrice != null)
                        NewSalePrice = OldSalePrice;

                    NewSalePrice.Id = SalePrice.Id;
                    NewSalePrice.EstablishmentKey = SalePrice.EstablishmentKey;
                    NewSalePrice.UniqueKey = SalePrice.UniqueKey;
                    NewSalePrice.Created = SalePrice.Created;
                    NewSalePrice.Modified = SalePrice.Modified;
                    NewSalePrice.IsActive = SalePrice.IsActive;
                    NewSalePrice.CodPrVenda = SalePrice.CodPrVenda;
                    NewSalePrice.Description = SalePrice.Description;
                    NewSalePrice.ProductId = SalePrice.ProductId;
                    NewSalePrice.RegionId = SalePrice.RegionId;
                    NewSalePrice.Value = SalePrice.Value;

                    NewSalePrice.QueueId = QueueItem.Id;

                    if (OldSalePrice == null)
                    {
                        NewSalePrice.Id = SalePrice.Id;
                        ERPContext.CRSalePrice.Add(NewSalePrice);
                    }
                    else
                        ERPContext.Entry(NewSalePrice).State = EntityState.Modified;

                    LogData.Info($"Salvando a tabela de preço:{SalePrice.Id} na CRSalePrice.");
                    await ERPContext.SaveChangesAsync();

                    LogData.Info($"Processando a tabela de preço:{SalePrice.Id}.");

                    await ERPContext.Database.ExecuteSqlRawAsync("BEGIN PCKG_INTEGRACAO.INSERTSALEPRICE(:pQUEUEID); END;",
                        new OracleParameter("pQUEUEID", OracleDbType.Int64, QueueItem.Id, ParameterDirection.Input));

                    var Item = await ERPContext.GEQueueItem.Where(r => r.Id == QueueItem.Id).FirstAsync();
                    QueueItem.Note = Item.Note;
                    QueueItem.Processed = true;
                    QueueItem.Uploaded = true;

                    LogData.Info(QueueItem.Note);

                }
                catch (Exception ex)
                {
                    var ExceptionCode = Guid.NewGuid().ToString();

                    var ErrorLog = LogData.Error("Não foi possível salvar os dados na tabela intermediária.", ex, ExceptionCode);

                    QueueItem.Note = ErrorLog.SubStr(0, 255);
                    QueueItem.ExceptionCode = ExceptionCode;

                    return null;
                }
            }

            return NewSalePrice;
        }
    }
}
