﻿using Hino.Salesforce.Sync.Core.Configuration;
using Hino.Salesforce.Sync.Core.DataBase;
using Hino.Salesforce.Sync.Core.Logging;
using Hino.Salesforce.Sync.Core.Messages;
using Hino.Salesforce.Sync.Core.Utils;
using Hino.Salesforce.Sync.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Hino.Salesforce.Sync.Models;
using Hino.Salesforce.Infra.Cross.Entities.Sales;
using Hino.Salesforce.Sync.Models.Views;
using Hino.Salesforce.Infra.Cross.Entities.General.Business;
using Hino.Salesforce.Sync.Models.General;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System.Data.Common;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Hino.Salesforce.Sync.Models.Vendas;
using Hino.Salesforce.Sync.Models.Fiscal;

namespace Hino.Salesforce.Sync.Core.Services
{
    public class ProductService
    {
        private AppSettingsParameters AppSettings { get; }
        public ProductService(AppSettingsParameters config)
        {
            AppSettings = config;
        }

        public async Task UpdateProductUniqueKeyAndIdAPIAsync(string pEstablishmentKey, int pCodEstab, 
            string pCodProduto, string pUniqueKey, long pIdApi)
        {
            var ERPContextFactory = new ERPContextFactory();
            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(pEstablishmentKey);
            LogData.Info("Atualizando o IDAPI e UniqueKey no ERP.");
            using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
            {
                await ERPContext.Database.ExecuteSqlRawAsync(@"UPDATE FSPRODUTOPARAMESTAB
                                                                  SET UNIQUEKEY  = NVL(UNIQUEKEY, :pUNIQUEKEY),
                                                                      IDAPI      = :pIDAPI
                                                                WHERE CODESTAB   = :pCODESTAB
                                                                  AND CODPRODUTO = :pCODPRODUTO",

                    new OracleParameter("pUNIQUEKEY", OracleDbType.Varchar2, pUniqueKey, ParameterDirection.Input),
                    new OracleParameter("pIDAPI", OracleDbType.Int64, pIdApi, ParameterDirection.Input),
                    new OracleParameter("pCODESTAB", OracleDbType.Int64, pCodEstab, ParameterDirection.Input),
                    new OracleParameter("pCODPRODUTO", OracleDbType.Varchar2, pCodProduto, ParameterDirection.Input));

            }
        }

        private CRProducts GetProductDataToUpload(int pCodEstab, string pCodProduto, string pEstablishmentKey)
        {
            var ERPContextFactory = new ERPContextFactory();
            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(pEstablishmentKey);

            using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
            {
                var result = ERPContext.GEVWSincProduto.FirstOrDefault(r => r.CodEstab == pCodEstab &&
                                                                 r.CodProduto == pCodProduto);
                if (result != null)
                {
                    return new CRProducts
                    {
                        Id = result.IDApi,
                        ProductKey = result.CodProduto,
                        Image = result.Image,
                        Name = result.Name,
                        Description = result.Description,
                        TypeProd = result.TypeProd,
                        TypeId = 1,
                        FamilyProd = result.Family,
                        FamilyId = 1,
                        PercIPI = 0,
                        PercMaxDiscount = result.LimiteDesconto,
                        Value = result.ValorUnitario,
                        StockBalance = result.Saldo,
                        Status = result.Status,
                        NCM = result.NCM,
                        NCMId = 1,
                        EstablishmentKey = pEstablishmentKey,
                        UniqueKey = result.UniqueKey.IsEmpty() ? pEstablishmentKey : result.UniqueKey,
                        Unit = result.CodUnidade,
                        PackageQTD = result.QtdePorEmb,
                        Weight = result.PesoBruto,
                        SaleUnit = result.UmVenda,
                        SaleFactor = result.FatorUmVenda,
                        IsActive = true,
                        CodOrigMerc = result.CodOrigMerc,
                        AplicationId = result.AplicationId,
                    };
                }
            }

            return null;
        }

        public async Task<bool> UploadFSSincProdutoAsync(FSSincProduto pFSSincProduto, string pEstablishmentKey, int pCodEstab)
        {
            var result = false;
            var ExceptionCode = "";
            try
            {
                var product = GetProductDataToUpload(pCodEstab, pFSSincProduto.CodProduto, pEstablishmentKey);
                if (product == null)
                    throw new Exception($"Dados do produto: {pFSSincProduto.CodProduto} não localizados.");

                var ERPContextFactory = new ERPContextFactory();
                var SalesforceContextFactory = new SalesforceContextFactory();
                LogData.Info($"Realizando conexao com o banco de dados do Portal.");

                string UniqueKey;
                long IdProduct;
                Infra.Cross.Entities.General.GEProducts NewProduct = null;
                using (var SalesforceContext = SalesforceContextFactory.CreateDbContext(AppSettings.ServiceSettings.DataBaseSalesforce.GetParameters()))
                {
                    LogData.Info("Buscando dados do preço de venda no portal.");
                    var OldProduct = SalesforceContext
                         .GEProducts
                         .FirstOrDefault(r =>
                            r.Id == product.Id &&
                            r.EstablishmentKey == pEstablishmentKey);

                    var NCMData = product.NCM == "" ?
                        SalesforceContext.FSNCM.FirstOrDefault(r => r.NCM == product.NCM) :
                        SalesforceContext.FSNCM.First();
                    var UnitData = SalesforceContext.GEProductsUnit.FirstOrDefault(r =>
                        r.Unit == product.Unit &&
                        r.EstablishmentKey == pEstablishmentKey
                    );
                    var SaleUnitData = SalesforceContext.GEProductsUnit.FirstOrDefault(r =>
                        r.Unit == product.SaleUnit &&
                        r.EstablishmentKey == pEstablishmentKey
                    );
                    var FamilyData = SalesforceContext.GEProductsFamily.FirstOrDefault(r =>
                        r.Family == product.FamilyProd &&
                        r.EstablishmentKey == pEstablishmentKey
                    );
                    var TypeData = SalesforceContext.GEProductsType.FirstOrDefault(r =>
                       r.TypeProd == product.TypeProd &&
                       r.EstablishmentKey == pEstablishmentKey
                    );
                    var AplicatonData = SalesforceContext.GEProductAplic.FirstOrDefault(r =>
                       r.IdERP == product.AplicationId &&
                       r.EstablishmentKey == pEstablishmentKey
                    );

                    NewProduct = new Infra.Cross.Entities.General.GEProducts();
                    if (OldProduct != null)
                        NewProduct = OldProduct;

                    NewProduct.ProductKey = product.ProductKey;
                    NewProduct.Image = product.Image;
                    NewProduct.Name = product.Name;
                    NewProduct.Description = product.Description;
                    NewProduct.TypeId = TypeData.Id;
                    NewProduct.FamilyId = FamilyData.Id;
                    NewProduct.PercIPI = Convert.ToDecimal(product.PercIPI);
                    NewProduct.PercMaxDiscount = Convert.ToDecimal(product.PercMaxDiscount);
                    NewProduct.Value = Convert.ToDecimal(product.Value);
                    NewProduct.StockBalance = Convert.ToDecimal(product.StockBalance);
                    NewProduct.Status = product.Status;
                    NewProduct.NCM = NCMData.NCM;
                    NewProduct.NCMId = NCMData.Id;
                    NewProduct.UnitId = UnitData.Id;
                    NewProduct.Weight = product.Weight;
                    NewProduct.PackageQTD = product.PackageQTD;
                    NewProduct.SaleUnitId = SaleUnitData?.Id;
                    NewProduct.SaleFactor = product.SaleFactor;
                    NewProduct.AplicationId = AplicatonData.Id;
                    NewProduct.CodOrigMerc = product.CodOrigMerc;
                    NewProduct.AditionalInfo = "";

                    if (OldProduct == null)
                    {
                        NewProduct.UniqueKey = Guid.NewGuid().ToString();
                        NewProduct.EstablishmentKey = pEstablishmentKey;
                        NewProduct.Created = DateTime.Now;
                        NewProduct.Modified = DateTime.Now;
                        NewProduct.IsActive = true;
                        NewProduct.Id = ERPContextFactory.GetNextSequence(SalesforceContext, "GEPRODUCTS");
                        SalesforceContext.GEProducts.Add(NewProduct);
                    }
                    else
                    {
                        NewProduct.Modified = DateTime.Now;
                        SalesforceContext.Entry(NewProduct).State = EntityState.Modified;
                    }

                    IdProduct = NewProduct.Id;
                    UniqueKey = NewProduct.UniqueKey;

                    await SalesforceContext.SaveChangesAsync();
                }

                var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(pEstablishmentKey);

                using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
                {
                    using (var conn = new OracleConnection(ERPContext.Database.GetConnectionString()))
                    using (var cmd = new OracleCommand())
                    {
                        conn.Open();
                        conn.ClientInfo = "HINOERP.SYNC";
                        conn.ClientId = "HINOERP.SYNC";

                        cmd.Connection = conn;
                        cmd.CommandType = CommandType.Text;
                        cmd.CommandText = @"UPDATE FSPRODUTOPARAMESTAB
                                               SET UNIQUEKEY = NVL(UNIQUEKEY, :pUNIQUEKEY),
                                                   IDAPI = :pIDAPI
                                             WHERE CODESTAB = :pCODESTAB
                                               AND CODPRODUTO = :pCODPRODUTO";
                        var uniqueKeyParam = new OracleParameter("pUNIQUEKEY", OracleDbType.Varchar2, UniqueKey, ParameterDirection.Input);
                        var idAPIParam = new OracleParameter("pIDAPI", OracleDbType.Int64, IdProduct, ParameterDirection.Input);
                        var codEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input);
                        var codProduto = new OracleParameter("pCODPRODUTO", OracleDbType.Varchar2, product.ProductKey, ParameterDirection.Input);

                        cmd.Parameters.AddRange(new[] {
                                uniqueKeyParam,
                                idAPIParam,
                                codEstabParam,
                                codProduto
                            });

                        await cmd.ExecuteNonQueryAsync();
                        conn.Close();
                    }
                }

                result = true;
            }
            catch (Exception ex)
            {
                ExceptionCode = ex.Message.Contains("não localizados") ? "not-found" : Guid.NewGuid().ToString();
                if (pFSSincProduto.CodSinc > 0)
                    ExceptionCode = ex.Message.Contains("NCM é de preenchimento obrigatório.") ? "not-found" : ExceptionCode;

                LogData.Error("Não foi possível atualizar os dados na API.", ex, ExceptionCode);

                result = ExceptionCode == "not-found";
            }

            return result;
        }

        public bool ExistsProductERP(string pEstablishmentKey, int pCodEstab, long pIdProduct)
        {
            var ERPContextFactory = new ERPContextFactory();
            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(pEstablishmentKey);

            using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
            {
                return ERPContext.FSProdutoParamEstab
                    .Any(r => r.CodEstab == pCodEstab && r.IdApi == pIdProduct);
            }
        }

        public async Task<CRProducts> SaveToERPCRProductsAsync(GEQueueItem QueueItem)
        {
            var SalesforceContextFactory = new SalesforceContextFactory();
            var SalesforceDatabase = AppSettings.ServiceSettings.DataBaseSalesforce;

            Infra.Cross.Entities.General.GEProducts Product;
            using (var SalesforceContext = SalesforceContextFactory.CreateDbContext(SalesforceDatabase.GetParameters()))
            {
                LogData.Info($"Buscando dados do produto: {QueueItem.IdReference} no Portal");
                Product = SalesforceContext.GEProducts
                    .AsNoTracking()
                    .Include(type => type.GEProductsType)
                    .Include(family => family.GEProductsFamily)
                    .Include(aplic => aplic.GEProductAplic)
                    .Include(unit => unit.GEProductsUnit)
                    .Include(unit => unit.GEProductsSaleUnit)
                    .FirstOrDefault(r =>
                        r.EstablishmentKey == QueueItem.EstablishmentKey &&
                        r.Id == QueueItem.IdReference);
            }
            var desc = Product == null ? "não" : "";

            QueueItem.Note = $"Dados do produto: {QueueItem.IdReference} {desc} encontrado no Portal";
            LogData.Info(QueueItem.Note);
            if (Product == null)
            {
                QueueItem.Processed = true;
                QueueItem.Uploaded = false;
                return null;
            }

            var ERPContextFactory = new ERPContextFactory();
            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(QueueItem.EstablishmentKey);

            CRProducts ProductRet = null;
            using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
            {
                try
                {
                    LogData.Info("Salvando na tabela intermediária.");

                    var OldProduct = ERPContext.CRProducts.Where(r => r.Id == Product.Id).FirstOrDefault();

                    var NewProduct = new CRProducts();
                    if (OldProduct != null)
                        NewProduct = OldProduct;


                    NewProduct.Created = Product.Created;
                    NewProduct.Modified = Product.Modified;
                    NewProduct.IsActive = Product.IsActive;
                    NewProduct.Id = Product.Id;
                    NewProduct.UniqueKey = Product.UniqueKey;
                    NewProduct.EstablishmentKey = QueueItem.EstablishmentKey;
                    NewProduct.ProductKey = Product.ProductKey;
                    NewProduct.Image = Product.Image;
                    NewProduct.Name = Product.Name;
                    NewProduct.Description = Product.Description;
                    NewProduct.TypeId = Product.TypeId;
                    if (Product.GEProductsType != null)
                        NewProduct.TypeProd = Product.GEProductsType.Description.Split('-')[0].Trim();
                    NewProduct.FamilyId = Product.FamilyId;
                    if (Product.GEProductsFamily != null)
                        NewProduct.FamilyProd = Product.GEProductsFamily.Description.Split('-')[0].Trim();
                    NewProduct.PercIPI = Convert.ToDouble(Product.PercIPI);
                    NewProduct.PercMaxDiscount = Convert.ToDouble(Product.PercMaxDiscount);
                    NewProduct.Value = Convert.ToDouble(Product.Value);
                    NewProduct.StockBalance = Convert.ToDouble(Product.StockBalance);
                    NewProduct.Status = Product.Status;
                    NewProduct.NCM = Product.NCM;
                    NewProduct.NCMId = Product.NCMId;
                    NewProduct.Weight = Product.Weight;
                    NewProduct.PackageQTD = Product.PackageQTD;
                    NewProduct.SaleUnitId = Product.SaleUnitId;
                    NewProduct.SaleFactor = Product.SaleFactor;
                    NewProduct.CodOrigMerc = Product.CodOrigMerc;
                    NewProduct.QueueId = QueueItem.Id;
                    NewProduct.UnitId = Product.UnitId;

                    if (Product.GEProductAplic != null)
                        NewProduct.AplicationId = Product.GEProductAplic.IdERP;

                    if (Product.GEProductsUnit != null)
                        NewProduct.Unit = Product.GEProductsUnit.Unit;

                    if (Product.GEProductsSaleUnit != null)
                        NewProduct.SaleUnit = Product.GEProductsSaleUnit.Unit;

                    if (OldProduct == null)
                    {
                        NewProduct.Id = Product.Id;
                        ERPContext.CRProducts.Add(NewProduct);
                    }
                    else
                        ERPContext.Entry(NewProduct).State = EntityState.Modified;

                    await ERPContext.SaveChangesAsync();

                    await ERPContext.Database.ExecuteSqlRawAsync("BEGIN PCKG_INTEGRACAO.INSERTPRODUCT(:pQUEUEID); END;",
                       new OracleParameter("pQUEUEID", OracleDbType.Int64, QueueItem.Id, ParameterDirection.Input));

                    var Item = await ERPContext.GEQueueItem.Where(r => r.Id == QueueItem.Id).FirstAsync();
                    QueueItem.Note = Item.Note ?? "Producto salvo com sucesso.";
                    QueueItem.Processed = true;
                    QueueItem.Uploaded = true;

                    await UpdateProductUniqueKeyAndIdAPIAsync(QueueItem.EstablishmentKey, ERPDatabase.CodEstab,
                        NewProduct.ProductKey, Product.UniqueKey, Product.Id);

                    LogData.Info(QueueItem.Note);
                    QueueItem.ExceptionCode = null;
                }
                catch (Exception ex)
                {
                    var ExceptionCode = Guid.NewGuid().ToString();

                    var ErrorLog = LogData.Error("Não foi possível salvar os dados na tabela intermediária.", ex, ExceptionCode);

                    QueueItem.Processed = true;
                    QueueItem.Uploaded = true;
                    QueueItem.Note = ErrorLog.SubStr(0, 255);
                    QueueItem.ExceptionCode = ExceptionCode;

                    return ProductRet;
                }
            }
            return ProductRet;
        }
    }
}
