﻿using Hino.Salesforce.Sync.Core.Configuration;
using Hino.Salesforce.Sync.Core.DataBase;
using Hino.Salesforce.Sync.Core.Logging;
using Hino.Salesforce.Sync.Core.Messages;
using Hino.Salesforce.Sync.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Core.Services
{
    public class QueueService
    {
        private AppSettingsParameters AppSettings { get; }
        public QueueService(AppSettingsParameters config)
        {
            AppSettings = config;
        }

        public async Task<GEQueueItem> GenerateQueueByMessageAsync(MessageReceivedEventArgs messageToProcess)
        {
            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(messageToProcess.EstablishmentKey);

            if (ERPDatabase == null)
            {
                LogData.Info($"Dados de conexão da EstablishmentKey: {messageToProcess.EstablishmentKey} não encontrados");
                return null;
            }

            var ERPContextFactory = new ERPContextFactory();
            var QueueItem = new GEQueueItem
            {
                Id = 0,
                Created = messageToProcess.Created,
                Modified = messageToProcess.Modified,
                EntryName = messageToProcess.EntryName,
                EstablishmentKey = messageToProcess.EstablishmentKey,
                IdReference = messageToProcess.Id,
                IsActive = messageToProcess.IsActive,
                Type = messageToProcess.Type,
                UniqueKey = messageToProcess.UniqueKey,
                Processed = true,
                Uploaded = false,
                Note = "",
                ExceptionCode = ""
            };

            using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
            {
                QueueItem.Id = ERPContextFactory.GetNextSequence(ERPContext, "GEQUEUEITEM");

                LogData.Info($"Salvando a mensagem no banco. Id. {QueueItem.Id} - Id. Ref. {QueueItem.IdReference}");
                ERPContext.GEQueueItem.Add(QueueItem);

                await ERPContext.SaveChangesAsync();
                ERPContext.Dispose();
            }

            return QueueItem;
        }

        public async Task<GEQueueItem> SaveNewQueueAsync(GEQueueItem QueueItem)
        {
            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(QueueItem.EstablishmentKey);

            if (ERPDatabase == null)
            {
                LogData.Info($"Dados de conexão da EstablishmentKey: {QueueItem.EstablishmentKey} não encontrados");
                return null;
            }

            var ERPContextFactory = new ERPContextFactory();

            using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
            {
                QueueItem.Id = ERPContextFactory.GetNextSequence(ERPContext, "GEQUEUEITEM");

                ERPContext.GEQueueItem.Add(QueueItem);

                await ERPContext.SaveChangesAsync();
                ERPContext.Dispose();
            }

            return QueueItem;
        }

        public async Task<GEQueueItem> SaveChangesQueueAsync(GEQueueItem QueueItem)
        {
            var ERPDatabase = AppSettings.ServiceSettings.GetDatabaseByEstablishmentKey(QueueItem.EstablishmentKey);

            if (ERPDatabase == null)
            {
                LogData.Info($"Dados de conexão da EstablishmentKey: {QueueItem.EstablishmentKey} não encontrados");
                return null;
            }

            var ERPContextFactory = new ERPContextFactory();

            using (var ERPContext = ERPContextFactory.CreateDbContext(ERPDatabase.GetParameters()))
            {
                ERPContext.Entry(QueueItem).State = EntityState.Modified;

                await ERPContext.SaveChangesAsync();
                ERPContext.Dispose();
            }

            return QueueItem;
        }
    }
}
