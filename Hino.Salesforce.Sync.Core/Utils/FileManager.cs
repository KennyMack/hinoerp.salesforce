﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Core.Utils
{
    public static class FileManager
    {
        public static bool FileExists(string filePath)
        {
            try
            {
                return File.Exists(filePath);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool CopyFile(string pathOrigin, string pathDestiny)
        {
            try
            {
                File.Copy(pathOrigin, pathDestiny);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool PathExists(string path)
        {
            try
            {
                return Directory.Exists(path);
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static DirectoryInfo CreatePath(string path)
        {
            try
            {
                return Directory.CreateDirectory(path);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static FileInfo[] GetPathFiles(string searchPath, string filter)
        {
            try
            {
                //  "*.pdf"
                var Files = Directory.GetFiles(searchPath, filter, SearchOption.TopDirectoryOnly);

                var FilesInfo = new FileInfo[Files.Length];

                for (int i = 0, length = Files.Length; i < length; i++)
                {
                    if (FileExists(Files[i]))
                        FilesInfo[i] = new FileInfo(Files[i]);
                }

                return FilesInfo;
            }
            catch (Exception)
            {

                return null;
            }
        }

        public static FileInfo FileMove(string filePath, string newFileName)
        {
            try
            {
                File.Move(filePath, newFileName);
                return new FileInfo(newFileName);
            }
            catch (Exception)
            {
                return null;
            }
        }

        public static bool RemoveFile(string filePath)
        {
            try
            {
                File.Delete(filePath);
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}
