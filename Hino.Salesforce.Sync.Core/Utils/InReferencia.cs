﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Core.Utils
{
    public static class InReferencia
    {
        public static bool In<T>(this T item, params T[] items)
        {
            if (items == null)
                throw new ArgumentNullException("itens");

            return items.Contains(item);
        }
    }
}
