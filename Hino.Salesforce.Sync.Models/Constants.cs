﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Models
{
    public static class Constants
    {
        public const string GEEnterprises = "GEENTERPRISES";
        public const string VEOrders = "VEORDERS";
        public const string VEPedidoSinc = "VEPEDIDOSINC";
        public const string GESincEmpresa = "GESINCEMPRESA";
        public const string FSSincProduto = "FSSINCPRODUTO";

        public const string GEProducts = "GEPRODUCTS";
        public const string GEEnterpriseCategory = "GEENTERPRISECATEGORY";
        public const string GEEnterpriseGroup = "GEENTERPRISEGROUP";

        public const string VETransactions = "VETRANSACTIONS";
        public const string VESincPrVenda = "VESINCPRVENDA";
        public const string VESalePrice = "VESALEPRICE";
        public const string VESincTransacoes = "VESINCTRANSACOES";

        public const string NotFound = "NotFound";
    }
}
