﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Models.General
{
    public class CREnterpriseContacts : BaseEntity
    {
        [ForeignKey("GEEnterprises")]
        public long EnterpriseId { get; set; }
        public virtual CREnterprises GEEnterprises { get; set; }
        public int ReceptivityIndex { get; set; }
        public int Sector { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public string Ramal { get; set; }
        public string Phone { get; set; }
        public string Note { get; set; }
    }
}
