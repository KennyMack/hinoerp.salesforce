﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.General
{
    [Table("CRGRUPO")]
    public class CRGrupo
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodGrupo { get; set; }
        public string Descricao { get; set; }
        public int? CodSegmento { get; set; }
        public int? CodCLucro { get; set; }
    }
}
