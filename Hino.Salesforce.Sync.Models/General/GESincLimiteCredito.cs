﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Models.General
{
    public class GESincLimiteCredito
    {
        [Key]
        public int codempresa { get; set; }
        [Key]
        public int codestab { get; set; }
        public DateTime datasincronizado { get; set; }
        public decimal valorlimite { get; set; }
        public decimal valorlimiteusado { get; set; }
    }
}
