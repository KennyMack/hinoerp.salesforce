﻿using System;

namespace Hino.Salesforce.Sync.Models.Interfaces
{
    public interface IBaseEntity
    {
        long Id { get; set; }
        string EstablishmentKey { get; set; }
        string UniqueKey { get; set; }
        DateTime Created { get; set; }
        DateTime Modified { get; set; }
        bool IsActive { get; set; }
    }
}
