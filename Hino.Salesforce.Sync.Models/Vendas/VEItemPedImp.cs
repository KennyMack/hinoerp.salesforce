﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Vendas
{
    [Table("VEITEMPEDIMP")]
    public class VEItemPedImp
    {
        [Key]
        [Column(Order = 0)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long codpedvenda { get; set; }
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public string codproduto { get; set; }
        [Key]
        [Column(Order = 3)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public short codimposto { get; set; }
        public decimal zonafranca { get; set; }
        public decimal aliquota { get; set; }
        public decimal basecalculo { get; set; }
        public decimal valorimposto { get; set; }
        public decimal valorisento { get; set; }
        public decimal valoroutros { get; set; }
        public decimal valorobservacao { get; set; }
        public decimal mva { get; set; }
        public decimal perredbc { get; set; }
        [NotMapped]
        public long ProductId { get; set; }
        [NotMapped]
        public decimal Quantidade { get; set; }
    }
}
