﻿using Hino.Salesforce.Sync.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Hino.Salesforce.Sync.Models.Fiscal;

namespace Hino.Salesforce.Sync.Models.Vendas
{
    [Table("VESINCPRVENDA")]
    public class VESincPrVenda : ISincData
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(Order = 0)]
        public long CodSinc { get; set; }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(Order = 1)]
        public long CodEstab { get; set; }
        public string CodProduto { get; set; }
        public long CodPrVenda { get; set; }
        public long CodRegiao { get; set; }
        public DateTime DataAlt { get; set; }
        public DateTime? DataSinc { get; set; }
        public bool Sincronizado { get; set; }
        public string UniqueKey { get; set; }
        public long? IdApi { get; set; }
        public int Tipo { get; set; }

        public virtual FSProdutoParamEstab FSProdutoParamEstab { get; set; }
    }
}
