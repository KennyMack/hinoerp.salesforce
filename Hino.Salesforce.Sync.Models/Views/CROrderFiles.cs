﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("CRORDERFILES")]
    public class CROrderFiles
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }

        [ForeignKey("CROrders")]
        public long OrderID { get; set; }
        public virtual CROrders CROrders { get; set; }
        public string EstablishmentKey { get; set; }
        public string UniqueKey { get; set; }

        public string Name { get; set; }
        public string Path { get; set; }
        public string Description { get; set; }
        public string MimeType { get; set; }
        public string Extension { get; set; }
        public string Identifier { get; set; }
    }
}
