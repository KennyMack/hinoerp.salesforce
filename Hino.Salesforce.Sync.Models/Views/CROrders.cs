﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    public class CROrders : BaseEntity
    {
        public CROrders()
        {
            this.VEOrderItems = new HashSet<CROrderItems>();
            CROrderFiles = new HashSet<CROrderFiles>();
        }

        public long EnterpriseID { get; set; }
        public long? CarrierID { get; set; }
        public long? RedispatchID { get; set; }
        public long UserID { get; set; }
        public long TypePaymentID { get; set; }
        public long PayConditionID { get; set; }
        public long CodPedVenda { get; set; }
        public long NumPedMob { get; set; }
        public System.DateTime DeliveryDate { get; set; }
        public string Note { get; set; }
        public string InnerNote { get; set; }
        public string Status { get; set; }
        public string StatusCRM { get; set; }
        public int StatusSinc { get; set; }
        public bool IsProposal { get; set; }
        public int FreightPaidBy { get; set; }
        public int RedispatchPaidBy { get; set; }
        public decimal FreightValue { get; set; }
        public string ClientOrder { get; set; }

        public long IdERP { get; set; }
        public long? OriginOrderID { get; set; }
        public long? MainOrderID { get; set; }
        public int OrderVersion { get; set; }

        public string ContactPhone { get; set; }
        public string ContactEmail { get; set; }
        public string Contact { get; set; }
        public int Sector { get; set; }

        public long DigitizerID { get; set; }
        public decimal FinancialTaxes { get; set; }
        public bool OnlyOnDate { get; set; }
        public bool AllowPartial { get; set; }
        public string RevisionReason { get; set; }

        public float PercDiscount { get; set; }
        public float PercCommission { get; set; }
        [ForeignKey("GEQueueItem")]
        public long QueueId { get; set; }
        public virtual GEQueueItem GEQueueItem { get; set; }

        public bool InPerson { get; set; }
        public System.DateTime? PaymentDueDate { get; set; }
        public int StatusPay { get; set; }
        public float PaidAmount { get; set; }

        public float AdvanceAmount { get; set; }
        public long? TypeSaleId { get; set; }

        public virtual ICollection<CROrderItems> VEOrderItems { get; set; }
        public virtual ICollection<CROrderFiles> CROrderFiles { get; set; }

        [NotMapped]
        public string MsErro { get; set; }
    }
}
