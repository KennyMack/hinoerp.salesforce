﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Fiscal
{
    [Table("FSVWSINCDANFE")]
    public class FSVWSincDanfe
    {
        public long IndiceNfSai { get; set; }
        public string notaFiscal { get; set; }
        public int CodEstab { get; set; }
        public long CodPedVenda { get; set; }
        public long IdApi { get; set; }
        public string Xml { get; set; }

    }
}
