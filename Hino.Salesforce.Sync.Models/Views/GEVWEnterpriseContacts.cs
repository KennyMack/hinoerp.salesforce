using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("GEVWENTERPRISECONTACTS")]
    public class GEVWEnterpriseContacts
    {
        [Key]
        public long? IDApi { get; set; }
        public string UniqueKey { get; set; }
        public long Identifier { get; set; }
        public long EnterpriseID { get; set; }
        public long Sector { get; set; }
        public string Contact { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int ReceptivityIndex { get; set; }
    }
}