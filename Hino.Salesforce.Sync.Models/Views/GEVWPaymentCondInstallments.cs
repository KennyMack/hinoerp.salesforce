using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("GEVWPAYMENTCONDINSTALLMENTS")]
    public class GEVWPaymentCondInstallments
    {
        [Key]
        public long? IDApi { get; set; }
        public string UniqueKey { get; set; }
        public long CondPayID { get; set; }
        public decimal Percent { get; set; }
        public int Days { get; set; }
    }
}