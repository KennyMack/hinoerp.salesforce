using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("GEVWPAYMENTCONDITION")]
    public class GEVWPaymentCondition
    {
        public long? IDApi { get; set; }
        public string UniqueKey { get; set; }
        public long TypePayID { get; set; }
        public string Description { get; set; }
        [Key]
        public long IDERP { get; set; }
        public short Installments { get; set; }
        public decimal PercDiscount { get; set; }
        public decimal PercIncrease { get; set; }
        public DateTime DataModificacao { get; set; }
    }
}