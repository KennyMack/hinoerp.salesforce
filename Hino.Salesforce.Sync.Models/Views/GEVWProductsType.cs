using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("GEVWPRODUCTSTYPE")]
    public class GEVWProductsType
    {
        [Key]
        public string ID { get; set; }
        public string UniqueKey { get; set; }
        public string Description { get; set; }
        public string TypeProd { get; set; }
    }
}