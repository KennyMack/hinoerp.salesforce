using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("GEVWPRODUCTSUNIT")]
    public class GEVWProductsUnit
    {
        public string ID { get; set; }
        public string UniqueKey { get; set; }
        [Key]
        public string Unit { get; set; }
        public string Description { get; set; }
    }
}