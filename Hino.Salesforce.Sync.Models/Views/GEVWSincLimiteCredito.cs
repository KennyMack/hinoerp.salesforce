﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Models.Views
{
    public class GEVWSincLimiteCredito
    {
        [Key]
        public long IdApi { get; set; }
        public string EstablishmentKey { get; set; }
        public string UniqueKey { get; set; }
        public DateTime DataModificacao { get; set; }
        public int CodEmpresa { get; set; }
        public int CodEstab { get; set; }
        public decimal LimiteAtual { get; set; }
        public decimal LimiteAnterior { get; set; }
        public decimal LimiteUsadoAtual { get; set; }
        public decimal LimiteUsadoAnterior { get; set; }
    }
}
