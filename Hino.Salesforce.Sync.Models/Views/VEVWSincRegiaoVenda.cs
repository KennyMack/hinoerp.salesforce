using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("VEVWSINCREGIAOVENDA")]
    public class VEVWSincRegiaoVenda
    {
        public int CodEstab { get; set; }
        public string Descricao { get; set; }
        public int CodRegiao { get; set; }
        [Key]
        public long IDApi { get; set; }
        public string UniqueKey { get; set; }
        public DateTime DataModificacao { get; set; }
    }
}