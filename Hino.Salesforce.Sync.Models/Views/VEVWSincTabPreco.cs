using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("VEVWSINCTABPRECO")]
    public class VEVWSincTabPreco
    {
        public long CodPrvenda { get; set; }
        public long CodRegiao { get; set; }
        public long CodRegiaoApi { get; set; }
        public long CodProdutoApi { get; set; }
        public decimal ValorUnitario { get; set; }
        public int CodEstab { get; set; }
        public DateTime DataModificacao { get; set; }
        [Key]
        public long IDApi { get; set; }
        public string UniqueKey { get; set; }
        public string CodProduto { get; set; }
        public string Descricao { get; set; }
    }
}