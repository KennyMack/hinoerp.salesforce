﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("VEVWSINCTIPOVENDA")]
    public class VEVWSincTipoVenda
    {
        public int CodTipoVenda { get; set; }
        public string Descricao { get; set; }
        public short Contrato { get; set; }
        public short DiaProgVenda { get; set; }
        public short? CodModalidadePgto { get; set; }
        public short? IndPres { get; set; }
        public long? IdApi { get; set; }
        public string UniqueKey { get; set; }
    }
}
