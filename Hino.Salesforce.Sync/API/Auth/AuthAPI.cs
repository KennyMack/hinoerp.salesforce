﻿using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Application.ViewModels.Auth;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Hino.Salesforce.Sync.API.Interfaces.Auth;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.API.Auth
{
    public class AuthAPI : BaseAPI<LoginVM>, IAuthAPI
    {
        public async Task<AuthResultModel> AuthenticateAsync(LoginVM pLogin)
        {
            AuthResultModel result = new AuthResultModel();

            using (HttpClient client = new HttpClient())
            {
                HttpContent content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("username", ""),
                    new KeyValuePair<string, string>("email", pLogin.Email),
                    new KeyValuePair<string, string>("password", pLogin.Password),
                    new KeyValuePair<string, string>("grant_type", "password"),
                    new KeyValuePair<string, string>("establishmentkey", pLogin.EstablishmentKey),
                    new KeyValuePair<string, string>("origin_login", "CLIENT"),
                });

                client.Timeout = new System.TimeSpan(0, 0, 1000);
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded")
                );

                try
                {
                    var cont = content.ReadAsStringAsync().Result;
                    var url = $"{base.BASEURL}/{base.EndPoint}";
                    var respMessage = await client.PostAsync(url, content);

                    if (respMessage != null)
                    {
                        var json = respMessage.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<AuthResultModel>(json, base._JsonSerializerSettings);
                    }
                    else
                    {
                        result.error = "no-comunication";
                        result.error_description = Infra.Cross.Resources.ErrorMessagesResource.NoComunication;
                    }
                }
                catch (System.Exception ex)
                {
                    result.error = "no-comunication";
                    result.error_description = ex.Message;
                }
            }

            return result;
        }

        public async Task<DefaultResultModel> Me()
        {
            try
            {
                var result = await _Request.GetAsync("Auth/Mordor/me", true);

                return result.ToObject<DefaultResultModel>(base._JsonSerializer);
            }
            catch (Exception e)
            {
                Logging.Exception(e);
                var errors = new List<ModelException>();
                errors.Add(new ModelException
                {
                    ErrorCode = 20001,
                    Field = "Me",
                    Messages = new string[] { Infra.Cross.Resources.ErrorMessagesResource.NoComunication },
                    Value = ""

                });

                return new DefaultResultModel
                {
                    data = null,
                    error = errors,
                    status = 400,
                    success = false
                };
            }
        }

        public async Task<AuthResultModel> RefreshTokenAsync()
        {
            AuthResultModel result = new AuthResultModel();

            using (HttpClient client = new HttpClient())
            {
                HttpContent content = new FormUrlEncodedContent(new[]
                {
                    new KeyValuePair<string, string>("refresh_token", Globals.REFRESH_TOKEN),
                    new KeyValuePair<string, string>("establishmentkey", Globals.EstablishmentKey),
                    new KeyValuePair<string, string>("grant_type", "refresh_token"),
                    new KeyValuePair<string, string>("origin_login", "CLIENT"),
                });

                client.Timeout = new System.TimeSpan(0, 0, 1000);
                client.DefaultRequestHeaders.Accept.Add(
                    new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/x-www-form-urlencoded")
                );

                try
                {
                    var cont = content.ReadAsStringAsync().Result;
                    var url = $"{base.BASEURL}/{base.EndPoint}";
                    var respMessage = await client.PostAsync(url, content);

                    if (respMessage != null)
                    {
                        var json = respMessage.Content.ReadAsStringAsync().Result;
                        result = JsonConvert.DeserializeObject<AuthResultModel>(json, base._JsonSerializerSettings);
                    }
                    else
                    {
                        result.error = "no-comunication";
                        result.error_description = Infra.Cross.Resources.ErrorMessagesResource.NoComunication;
                    }
                }
                catch (System.Exception ex)
                {
                    result.error = "no-comunication";
                    result.error_description = ex.Message;
                }
            }

            return result;
        }
    }
}
