﻿using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Infra.Cross.Utils.Attributes;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using Hino.Salesforce.Sync.API.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.API
{
    public class BaseAPI<T> : IBaseAPI<T> where T : BaseVM
    {
        protected readonly Request _Request;
        public List<ModelException> Errors { get => _Request.Errors; set => _Request.Errors = value; }
        public JsonSerializerSettings _JsonSerializerSettings { get => _Request._JsonSerializerSettings; }
        public JsonSerializer _JsonSerializer { get => _Request._JsonSerializer; }

        public readonly string EndPoint;
        public string BASEURL
        {
            get => _Request.BASEURL;
        }

        public BaseAPI()
        {
            _Request = new Request();
            try
            {
                Type baseType = typeof(T);
                EndPoint =
                    ((EndPointAttribute)baseType.GetCustomAttributes(typeof(EndPointAttribute), false).FirstOrDefault()).EndPoint;
            }
            catch (Exception)
            {
                throw new NotImplementedException("FALTOU O EndPointAttribute BAHIA !!!");
            }
        }

        public async Task<PagedResult<T>> GetAllSyncAsync(DateTime pDate)
        {
            var result =
                await _Request.GetAsync(
                    $"{EndPoint.Replace("{pEstablishmentKey}", Globals.EstablishmentKey)}/sync/date/{pDate.ToString("ddMMyyyyHHmmss")}", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PagedResult<T>>(this._JsonSerializer);
        }

        public async Task<PagedResult<T>> GetPushNewAsync()
        {
            var result =
                await _Request.GetAsync(
                    $"{EndPoint.Replace("{pEstablishmentKey}", Globals.EstablishmentKey)}/push/new", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PagedResult<T>>(this._JsonSerializer);
        }

        public async Task<T[]> PostSyncDataAsync(T[] model)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(model),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pEstablishmentKey}", Globals.EstablishmentKey)}/sync", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<T[]>(this._JsonSerializer);
        }

        public async Task<PagedResult<T>> GetAllAsync()
        {
            var result =
                await _Request.GetAsync(
                    $"{EndPoint.Replace("{pEstablishmentKey}", Globals.EstablishmentKey)}/all", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PagedResult<T>>(this._JsonSerializer);
        }

        public async Task<T> GetByIdAsync(string pUniqueKey, long pId)
        {
            var result =
            await _Request.GetAsync(
                $"{EndPoint.Replace("{pEstablishmentKey}", Globals.EstablishmentKey)}/id/{pId}", true);
            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            try
            {
                return (data.data.ToObject<PagedResult<T>>(this._JsonSerializer)).Results.FirstOrDefault();
            }
            catch (Exception)
            {

                return data.data.ToObject<T>(this._JsonSerializer);
            }


        }

        public async Task<T> PostAsync(T model)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(model),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pEstablishmentKey}", Globals.EstablishmentKey)}/save", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<T>(this._JsonSerializer);
        }

        public async Task<T> PutAsync(T model, string pUniqueKey, long id)
        {
            var content = new StringContent(
                   JsonConvert.SerializeObject(model),
                   Encoding.UTF8, "application/json");

            var result =
               await _Request.PutAsync(
                   $"{EndPoint.Replace("{pEstablishmentKey}", Globals.EstablishmentKey)}/key/{pUniqueKey}/id/{id}/save", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<T>(this._JsonSerializer);
        }

        public async Task<T> DeleteAsync(string pUniqueKey, long id)
        {
            var result =
               await _Request.DeleteAsync(
                   $"{EndPoint.Replace("{pEstablishmentKey}", Globals.EstablishmentKey)}/key/{pUniqueKey}/id/{id}/delete", true);

            return null;
        }
    }
}
