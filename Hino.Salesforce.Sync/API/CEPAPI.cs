﻿using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Sync.Models;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.API
{
    public class CEPAPI
    {
        public async Task<GECepModel> GetCepAsync(string pCep)
        {
            var _Request = new Request();
            var result =
                await _Request.GetAsync($"cep/{pCep.OnlyNumbers()}", true);

            var data = result.ToObject<DefaultResultModel>(new JsonSerializer
            {
                Culture = new System.Globalization.CultureInfo("pt-BR"),
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatString = "dd/MM/yyyy HH:mm:ss"
            });

            if (!data.success)
                return null;

            return data.data.ToObject<GECepModel>(new JsonSerializer
            {
                Culture = new System.Globalization.CultureInfo("pt-BR"),
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatString = "dd/MM/yyyy HH:mm:ss"
            });
        }

    }
}
