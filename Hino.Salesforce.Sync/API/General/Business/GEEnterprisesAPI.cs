﻿using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Sync.API.Interfaces.General.Business;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.API.General.Business
{
    public class GEEnterprisesAPI : BaseAPI<GEEnterprisesVM>, IGEEnterprisesAPI
    {
        public async Task<GEEnterprisesVM[]> PostSyncDataERPAsync(GEEnterprisesVM[] model)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(model),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pEstablishmentKey}", Globals.EstablishmentKey)}/sync/erp", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<GEEnterprisesVM[]>(this._JsonSerializer);
        }
    }
}
