﻿using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Sync.API.Interfaces.General.Business;

namespace Hino.Salesforce.Sync.API.General.Business
{
    public class GEPaymentConditionAPI : BaseAPI<GEPaymentConditionVM>, IGEPaymentConditionAPI
    {
    }
}
