﻿using Hino.Salesforce.Application.ViewModels.General.Demograph;
using Hino.Salesforce.Sync.API.Interfaces.General.Demograph;

namespace Hino.Salesforce.Sync.API.General.Demograph
{
    public class GECitiesAPI : BaseAPI<GECitiesVM>, IGECitiesAPI
    {
    }
}
