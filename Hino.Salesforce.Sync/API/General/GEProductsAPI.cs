﻿using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Sync.API.Interfaces.General;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.API.General
{
    public class GEProductsAPI : BaseAPI<GEProductsVM>, IGEProductsAPI
    {

        public async Task<GEProductsVM[]> PostSyncStockBalanceAsync(GEProductsVM[] model)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(model),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PutAsync(
                   $"{EndPoint.Replace("{pEstablishmentKey}", Globals.EstablishmentKey)}/stock-balance", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<GEProductsVM[]>(this._JsonSerializer);
        }
    }
}
