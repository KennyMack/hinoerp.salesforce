﻿using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Sync.API.Interfaces.General;

namespace Hino.Salesforce.Sync.API.General
{
    public class UsersAPI : BaseAPI<GEUsersVM>, IUsersAPI
    {
    }
}
