﻿using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Application.ViewModels.Auth;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.API.Interfaces.Auth
{
    public interface IAuthAPI : IBaseAPI<LoginVM>
    {
        Task<AuthResultModel> AuthenticateAsync(LoginVM pLogin);
        Task<AuthResultModel> RefreshTokenAsync();
        Task<DefaultResultModel> Me();
    }
}
