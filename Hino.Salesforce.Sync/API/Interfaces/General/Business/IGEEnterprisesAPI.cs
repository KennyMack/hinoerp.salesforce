﻿using Hino.Salesforce.Application.ViewModels.General.Business;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.API.Interfaces.General.Business
{
    public interface IGEEnterprisesAPI : IBaseAPI<GEEnterprisesVM>
    {
        Task<GEEnterprisesVM[]> PostSyncDataERPAsync(GEEnterprisesVM[] model);
    }
}
