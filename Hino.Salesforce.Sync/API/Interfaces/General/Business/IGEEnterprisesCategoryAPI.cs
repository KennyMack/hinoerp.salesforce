﻿using Hino.Salesforce.Application.ViewModels.General.Business;

namespace Hino.Salesforce.Sync.API.Interfaces.General.Business
{
    public interface IGEEnterprisesCategoryAPI : IBaseAPI<GEEnterpriseCategoryVM>
    {
    }
}
