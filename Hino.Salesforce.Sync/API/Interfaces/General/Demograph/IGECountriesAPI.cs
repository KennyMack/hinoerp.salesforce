﻿using Hino.Salesforce.Application.ViewModels.General.Demograph;

namespace Hino.Salesforce.Sync.API.Interfaces.General.Demograph
{
    public interface IGECountriesAPI : IBaseAPI<GECountriesVM>
    {
    }
}
