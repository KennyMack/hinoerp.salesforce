﻿using Hino.Salesforce.Application.ViewModels.General;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.API.Interfaces.General
{
    public interface IGEProductsAPI : IBaseAPI<GEProductsVM>
    {
        Task<GEProductsVM[]> PostSyncStockBalanceAsync(GEProductsVM[] model);
    }
}
