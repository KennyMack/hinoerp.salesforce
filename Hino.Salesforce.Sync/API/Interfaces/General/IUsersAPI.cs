﻿using Hino.Salesforce.Application.ViewModels.General;

namespace Hino.Salesforce.Sync.API.Interfaces.General
{
    public interface IUsersAPI : IBaseAPI<GEUsersVM>
    {
    }
}
