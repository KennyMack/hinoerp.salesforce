﻿using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.API.Interfaces
{
    public interface IBaseAPI<T> where T : BaseVM
    {
        string BASEURL { get; }
        Task<PagedResult<T>> GetAllSyncAsync(DateTime pDate);
        Task<PagedResult<T>> GetPushNewAsync();
        Task<PagedResult<T>> GetAllAsync();
        Task<T[]> PostSyncDataAsync(T[] model);
        Task<T> GetByIdAsync(string pUniqueKey, long pId);
        Task<T> PostAsync(T model);
        Task<T> PutAsync(T model, string pUniqueKey, long id);
        Task<T> DeleteAsync(string pUniqueKey, long id);
    }
}
