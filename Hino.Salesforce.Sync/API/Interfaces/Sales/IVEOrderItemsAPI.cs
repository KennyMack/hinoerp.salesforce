﻿using Hino.Salesforce.Application.ViewModels.Sales;

namespace Hino.Salesforce.Sync.API.Interfaces.Sales
{
    public interface IVEOrderItemsAPI : IBaseAPI<VEOrderItemsVM>
    {
    }
}
