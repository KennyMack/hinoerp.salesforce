﻿using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using System;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.API.Interfaces.Sales
{
    public interface IVEOrdersAPI : IBaseAPI<VEOrdersVM>
    {
        Task<PagedResult<VEOrdersVM>> GetAllSyncUserAsync(long pUserId, DateTime pDate);
        Task<VEOrdersVM[]> PostSyncDataERPAsync(VEOrdersVM[] model);
    }
}
