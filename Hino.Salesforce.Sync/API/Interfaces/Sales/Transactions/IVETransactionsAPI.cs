﻿using Hino.Salesforce.Application.ViewModels.Sales.Transactions;

namespace Hino.Salesforce.Sync.API.Interfaces.Sales.Transactions
{
    public interface IVETransactionsAPI : IBaseAPI<VETransactionsVM>
    {
    }
}
