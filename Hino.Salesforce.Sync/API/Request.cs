﻿using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using Hino.Salesforce.Sync.API.Interfaces;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.API
{
    public class Request : IRequest
    {
        public readonly JsonSerializerSettings _JsonSerializerSettings;
        public readonly JsonSerializer _JsonSerializer;

        public string BASEURL = $"http://{Globals.BASEURL}";

        public List<ModelException> Errors { get; set; }

        public Request()
        {
            _JsonSerializer = new JsonSerializer
            {
                Culture = new System.Globalization.CultureInfo("pt-BR"),
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatString = "dd/MM/yyyy HH:mm:ss"
            };

            _JsonSerializerSettings = new JsonSerializerSettings
            {
                Culture = new System.Globalization.CultureInfo("pt-BR"),
                ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
                DateFormatString = "dd/MM/yyyy HH:mm:ss"
            };
            Errors = new List<ModelException>();
        }

        #region Default Headers
        private void DefaultHeaders(HttpClient pClient)
        {
            pClient.Timeout = new System.TimeSpan(0, 0, 1000);
            pClient.DefaultRequestHeaders.Accept.Add(
                new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json")
            );
        }
        #endregion

        #region Create Token Header
        private void CreateTokenHeader(HttpClient pClient, bool Token)
        {
            if (Token)
            {
                pClient.DefaultRequestHeaders.Authorization =
                    new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", Globals.USER_TOKEN);
                pClient.DefaultRequestHeaders.Add("Refresh-Token", Globals.REFRESH_TOKEN);
            }
        }
        #endregion

        #region Create request Fail
        private JContainer CreateRequestFail(Exception pException)
        {
            var Exception = new ModelException
            {
                ErrorCode = 900,
                Field = "",
                Value = "",
                Messages = new string[] { pException.Message }
            };

            var DefError = new DefaultResultModel
            {
                status = 500,
                success = false,
                data = null
            };

            DefError.error.Add(Exception);

            return (JContainer)JsonConvert.DeserializeObject(JsonConvert.SerializeObject(DefError), _JsonSerializerSettings);
        }
        #endregion

        public async Task<JContainer> GetAsync(string path, bool Token)
        {
            HttpResponseMessage respMessage = null;
            JContainer data = null;

            using (HttpClient client = new HttpClient())
            {
                DefaultHeaders(client);

                CreateTokenHeader(client, Token);
                try
                {
                    client.Timeout = new System.TimeSpan(0, 0, 1000);
                    respMessage = await client.GetAsync($"{BASEURL}/{path}");

                    if (respMessage != null)
                    {
                        var json = respMessage.Content.ReadAsStringAsync().Result;
                        var dt = JObject.Parse(json);

                        var t = dt.GetValue("Message");
                        if (dt.GetValue("Message") != null)
                            data = (JContainer)JsonConvert.DeserializeObject(dt.GetValue("Message").ToString(), _JsonSerializerSettings);
                        else
                            data = (JContainer)JsonConvert.DeserializeObject(JObject.Parse(json).GetValue("Data").ToString(), _JsonSerializerSettings);
                    }

                }
                catch (Exception e)
                {
                    data = CreateRequestFail(e);
                }
            }

            return data;
        }

        public async Task<JContainer> PostAsync(string path, bool Token, HttpContent content)
        {
            HttpResponseMessage respMessage = null;
            JContainer data = null;

            using (HttpClient client = new HttpClient())
            {
                DefaultHeaders(client);

                CreateTokenHeader(client, Token);

                try
                {
                    var cont = content.ReadAsStringAsync().Result;
                    var url = $"{BASEURL}/{path}";
                    respMessage = await client.PostAsync(url, content);

                    if (respMessage != null)
                    {
                        var json = respMessage.Content.ReadAsStringAsync().Result;
                        var dt = JObject.Parse(json);

                        var t = dt.GetValue("Message");
                        if (dt.GetValue("Message") != null)
                            data = (JContainer)JsonConvert.DeserializeObject(dt.GetValue("Message").ToString(), _JsonSerializerSettings);
                        else
                            data = (JContainer)JsonConvert.DeserializeObject(JObject.Parse(json).GetValue("Data").ToString(), _JsonSerializerSettings);

                    }
                }
                catch (System.Exception ex)
                {
                    data = CreateRequestFail(ex);
                }
            }

            return data;
        }

        public async Task<JContainer> PutAsync(string path, bool Token, HttpContent content)
        {
            HttpResponseMessage respMessage = null;
            JContainer data = null;

            using (HttpClient client = new HttpClient())
            {
                DefaultHeaders(client);

                CreateTokenHeader(client, Token);

                try
                {
                    respMessage = await client.PutAsync($"{BASEURL}/{path}", content);

                    if (respMessage != null)
                    {
                        var json = respMessage.Content.ReadAsStringAsync().Result;
                        var dt = JObject.Parse(json);

                        var t = dt.GetValue("Message");
                        if (dt.GetValue("Message") != null)
                            data = (JContainer)JsonConvert.DeserializeObject(dt.GetValue("Message").ToString(), _JsonSerializerSettings);
                        else
                            data = (JContainer)JsonConvert.DeserializeObject(JObject.Parse(json).GetValue("Data").ToString(), _JsonSerializerSettings);

                    }
                }
                catch (System.Exception ex)
                {
                    data = CreateRequestFail(ex);
                }
            }

            return data;
        }

        public async Task<JContainer> DeleteAsync(string path, bool Token)
        {
            HttpResponseMessage respMessage = null;
            JContainer data = null;

            using (HttpClient client = new HttpClient())
            {
                DefaultHeaders(client);

                CreateTokenHeader(client, Token);

                try
                {
                    respMessage = await client.DeleteAsync($"{BASEURL}/{path}");

                    if (respMessage != null)
                    {
                        var json = respMessage.Content.ReadAsStringAsync().Result;
                        var dt = JObject.Parse(json);

                        var t = dt.GetValue("Message");
                        if (dt.GetValue("Message") != null)
                            data = (JContainer)JsonConvert.DeserializeObject(dt.GetValue("Message").ToString(), _JsonSerializerSettings);
                        else
                            data = (JContainer)JsonConvert.DeserializeObject(JObject.Parse(json).GetValue("Data").ToString(), _JsonSerializerSettings);

                    }
                }
                catch (System.Exception ex)
                {
                    data = CreateRequestFail(ex);
                }
            }

            return data;
        }
    }
}
