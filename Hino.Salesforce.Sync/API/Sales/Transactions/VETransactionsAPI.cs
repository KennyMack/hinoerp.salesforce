﻿using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Application.ViewModels.Sales.Transactions;
using Hino.Salesforce.Sync.API.Interfaces.Sales.Transactions;
using Newtonsoft.Json;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.API.Sales.Transactions
{
    public class VETransactionsAPI : BaseAPI<VETransactionsVM>, IVETransactionsAPI
    {
        public async Task<VETransactionsVM[]> PostSyncDataERPAsync(VETransactionsVM[] model)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(model),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pEstablishmentKey}", Globals.EstablishmentKey)}/sync/erp", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<VETransactionsVM[]>(this._JsonSerializer);
        }
    }
}
