﻿using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using Hino.Salesforce.Sync.API.Interfaces.Sales;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.API.Sales
{
    public class VEOrderItemsAPI : BaseAPI<VEOrderItemsVM>, IVEOrderItemsAPI
    {
        public async Task<IEnumerable<VEOrderItemsVM>> GetOrderItemsAsync(long orderId)
        {
            var result =
                await _Request.GetAsync(
                    $"{EndPoint.Replace("{pEstablishmentKey}", Globals.EstablishmentKey)}/order/{orderId}/all", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<IEnumerable<VEOrderItemsVM>>(this._JsonSerializer);
        }
    }
}
