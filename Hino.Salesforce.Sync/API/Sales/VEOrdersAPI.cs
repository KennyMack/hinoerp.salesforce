﻿using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Infra.Cross.Utils.Paging;
using Hino.Salesforce.Sync.API.Interfaces.Sales;
using Newtonsoft.Json;
using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.API.Sales
{
    public class VEOrdersAPI : BaseAPI<VEOrdersVM>, IVEOrdersAPI
    {
        public async Task<PagedResult<VEOrdersVM>> GetAllSyncUserAsync(long pUserId, DateTime pDate)
        {
            var result =
                await _Request.GetAsync(
                    $"{EndPoint.Replace("{pEstablishmentKey}", Globals.EstablishmentKey)}/user/{pUserId}/sync/date/{pDate.ToString("ddMMyyyyHHmmss")}", true);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<PagedResult<VEOrdersVM>>(this._JsonSerializer);
        }

        public async Task<VEOrdersVM[]> PostSyncDataERPAsync(VEOrdersVM[] model)
        {
            var content = new StringContent(
                JsonConvert.SerializeObject(model),
                Encoding.UTF8, "application/json");

            var result =
               await _Request.PostAsync(
                   $"{EndPoint.Replace("{pEstablishmentKey}", Globals.EstablishmentKey)}/sync/erp", true, content);

            var data = result.ToObject<DefaultResultModel>(this._JsonSerializer);

            if (!data.success)
            {
                Errors = data.error;
                return null;
            }

            return data.data.ToObject<VEOrdersVM[]>(this._JsonSerializer);
        }

    }
}
