﻿using Hino.Salesforce.Application.ViewModels.Auth;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Sync.API.Auth;
using Hino.Salesforce.Sync.API.Interfaces.Auth;
using Hino.Salesforce.Sync.Data;
using Hino.Salesforce.Sync.Utils;
using System;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Application.Auth
{
    public class AuthenticationService
    {
        async Task<AuthResultModel> LoginUser()
        {
            Logger.Info("Autenticando usuário.");
            var ret = await LoginAsync(new LoginVM
            {
                EstablishmentKey = Globals.EstablishmentKey,
                Email = Globals.Email,
                Password = Globals.Password
            });

            if (!ret.success)
                Logger.Error(ret.error_description, new Exception(ret.error_description));
            else
                Logger.Info("Autenticado com sucesso.");

            return ret;
        }

        public async Task SaveTokenToDbAsync()
        {
            try
            {
                using (var context = new HinoContext())
                {
                    var config = context.GESincConfig.Where(r => r.codestab == Globals.CodEstab).FirstOrDefault();
                    if (config != null)
                    {
                        config.usertoken = Globals.USER_TOKEN;
                        config.refreshtoken = Globals.REFRESH_TOKEN;
                        context.Entry(config).State = EntityState.Modified;
                        await context.SaveChangesAsync();
                    }
                }
            }
            catch (Exception)
            {
            }
        }

        public async Task<bool> AuthenticateAsync()
        {
            Logger.Info("Validando token.");
            var me = await GetMe();

            if (me == null && Globals.USER_TOKEN.IsEmpty())
            {
                var ret = await LoginUser();
                return ret.success;
            }
            else if (me == null && !Globals.USER_TOKEN.IsEmpty())
            {
                var ret = await CheckAndRefreshToken();
                if (!ret)
                {
                    var retVal = await LoginUser();
                    return retVal.success;
                }
                else
                    return true;
            }
            else
                return true;
        }

        public async Task<AuthResultModel> LoginAsync(LoginVM pLoginModel)
        {
            IAuthAPI _IAuthAPI = new AuthAPI();
            var ret = await _IAuthAPI.AuthenticateAsync(pLoginModel);

            if (!string.IsNullOrEmpty(ret.error))
            {
                return new AuthResultModel
                {
                    success = false,
                    error = ret.error,
                    error_description = ret.error_description
                };
            }

            ret.success = true;

            //Globals.EstablishmentKey = pLoginModel.EstablishmentKey;
            Globals.USER_TOKEN = ret.access_token;
            Globals.REFRESH_TOKEN = ret.refresh_token;

            await SaveTokenToDbAsync();

            return ret;
        }

        public async Task<AuthResultModel> RefreshLoginAsync()
        {
            IAuthAPI _IAuthAPI = new AuthAPI();
            var ret = await _IAuthAPI.RefreshTokenAsync();

            if (!string.IsNullOrEmpty(ret.error))
            {
                return new AuthResultModel
                {
                    success = false,
                    error = ret.error,
                    error_description = ret.error_description
                };
            }

            ret.success = true;

            Globals.USER_TOKEN = ret.access_token;
            Globals.REFRESH_TOKEN = ret.refresh_token;

            await SaveTokenToDbAsync();
            return ret;
        }

        public async Task<GEUsersVM> GetMe()
        {
            IAuthAPI _IAuthAPI = new AuthAPI();
            var me = await _IAuthAPI.Me();
            if (!me.success)
                return null;

            return me.data.ToObject<GEUsersVM>();
        }

        public async Task<string> SetUserLogged()
        {
            IAuthAPI _IAuthAPI = new AuthAPI();
            var me = await _IAuthAPI.Me();

            if (!me.success)
                return me.error[0].Messages.ToArray()[0];

            return "";
        }

        #region Check and refresh token
        public async Task<bool> CheckAndRefreshToken()
        {
            var me = await GetMe();

            bool retval;
            if (me == null)
            {
                var refresh = await RefreshLoginAsync();
                retval = refresh.success;
            }
            else
                retval = true;

            return retval;
        }
        #endregion
    }
}
