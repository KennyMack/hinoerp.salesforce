﻿using Hino.Salesforce.Sync.Data;
using Hino.Salesforce.Sync.Models.General;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Application
{
    public class FirstUpload
    {
        private GEEstab Estab;
        private GESincConfig GESincConfig;
        public FirstUpload(GEEstab pEstab, GESincConfig pGESincConfig)
        {
            Estab = pEstab;
            GESincConfig = pGESincConfig;
        }


        #region Upload dados
        public async Task UploadDados()
        {
            using (var context = new HinoContext())
            {
                await UploadFiscalOper(context);
            }
        }
        #endregion

        #region Upload Fiscal Oper
        private Task UploadFiscalOper(HinoContext context)
        {
            return Task.FromResult(false);
            /*
            var Items = context.FSVWFiscalOper.Where(r => r.IDApi == 0);

            foreach (var item in Items)
            {

            }
            */
        }
        #endregion
    }
}
