﻿using Hino.Salesforce.Sync.Data;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Application.Gerais
{
    public class EnterpriseService
    {
        public async Task ClearEnterpriseContactsAsync(HinoContext pContext, long pIdEnterprise)
        {
            using (var cmd = pContext.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"DELETE
                                      FROM CRENTERPRISECONTACTS
                                     WHERE CRENTERPRISECONTACTS.ENTERPRISEID = :pENTERPRISEID";

                var queueIdParam = new OracleParameter("pENTERPRISEID", OracleDbType.Int64, pIdEnterprise, ParameterDirection.Input);

                cmd.Parameters.AddRange(new[] {
                    queueIdParam
                });
                cmd.Connection.Open();
                await cmd.ExecuteNonQueryAsync();
                cmd.Connection.Close();
            }
        }

        public async Task<bool> EnterpriseExists(HinoContext pContext, long pIdEnterprise)
        {
            var ret = false;
            using (var cmd = pContext.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT COUNT(1)
                                      FROM GEEMPRESAPARAMESTAB
                                     WHERE GEEMPRESAPARAMESTAB.CODESTAB = :pCODESTAB
                                       AND GEEMPRESAPARAMESTAB.IDAPI = :pIDAPI";

                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, Globals.CodEstab, ParameterDirection.Input);
                var queueIdParam = new OracleParameter("pIDAPI", OracleDbType.Int64, pIdEnterprise, ParameterDirection.Input);

                cmd.Parameters.AddRange(new[] {
                    codeEstabParam,
                    queueIdParam
                });
                cmd.Connection.Open();
                ret = Convert.ToInt64(await cmd.ExecuteScalarAsync()) > 0;
                cmd.Connection.Close();
            }
            return ret;
        }
    }
}
