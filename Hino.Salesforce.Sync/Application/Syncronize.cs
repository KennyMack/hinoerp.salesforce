﻿using Hino.Salesforce.Application.ViewModels;
using Hino.Salesforce.Application.ViewModels.Auth;
using Hino.Salesforce.Application.ViewModels.Fiscal;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Application.ViewModels.General.Business;
using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Application.ViewModels.Sales.Transactions;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Enums;
using Hino.Salesforce.Sync.API;
using Hino.Salesforce.Sync.API.General;
using Hino.Salesforce.Sync.API.General.Business;
using Hino.Salesforce.Sync.API.Interfaces.Sales;
using Hino.Salesforce.Sync.API.Sales;
using Hino.Salesforce.Sync.API.Sales.Transactions;
using Hino.Salesforce.Sync.Application.Auth;
using Hino.Salesforce.Sync.Application.Gerais;
using Hino.Salesforce.Sync.Application.Vendas;
using Hino.Salesforce.Sync.Data;
using Hino.Salesforce.Sync.Models;
using Hino.Salesforce.Sync.Models.Fiscal;
using Hino.Salesforce.Sync.Models.General;
using Hino.Salesforce.Sync.Models.Interfaces;
using Hino.Salesforce.Sync.Models.Vendas;
using Hino.Salesforce.Sync.Models.Views;
using Hino.Salesforce.Sync.Utils;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Application
{
    public class Syncronize
    {
        public bool Synchonizing { get; private set; }
        private static readonly Regex EmailRegex = new Regex(@"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$",
            RegexOptions.Compiled | RegexOptions.IgnoreCase);
        private static readonly Regex PhoneRegex = new Regex(@"^(\(\d{2,3}\)\s\d{4,5}\-\d{4})$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public Syncronize()
        {
        }

        public async Task<bool> AuthenticateUser()
        {
            var _AuthenticationService = new AuthenticationService();
            return await _AuthenticationService.AuthenticateAsync();
        }

        public async Task DownloadQueue()
        {
            Logger.Info("Processando itens para download.");
            GESincConfig[] ListConfig;
            GEQueueItem[] Queue;
            GEEstab[] ListEstab;
            using (var context = new HinoContext())
            {
                ListEstab = context.GEEstab.ToArray();
                ListConfig = context.GESincConfig.ToArray();
                Queue = context.GEQueueItem.Where(r => !r.Processed).ToArray();
            }

            Logger.Info($"Itens na fila para download {Queue?.Length ?? 0}.");

            if ((Queue?.Length ?? 0) > 0 && !Synchonizing)
            {
                Synchonizing = true;
                foreach (var config in ListConfig)
                {
                    var Estab = ListEstab.Where(r => r.codestab == config.codestab).FirstOrDefault();

                    Logger.Info($"Percorrendo: {Estab.codestab} - {Estab.razaosocial}");
                    Globals.EstablishmentKey = config.establishmentkey;
                    Globals.CodEstab = Estab.codestab;

                    var ItemCount = 1;
                    if (!await AuthenticateUser())
                    {
                        Synchonizing = false;
                        return;
                    }

                    var QueueEstab = Queue.Where(r => r.EstablishmentKey == Globals.EstablishmentKey).OrderBy(r => r.Priority).ThenBy(r => r.Created);

                    foreach (var item in QueueEstab)
                    {
                        Logger.Info($"Percorrendo itens na fila {ItemCount}/{Queue?.Length ?? 0}.");

                        await DownloadAndUploadData(item);
                        ItemCount++;
                    }
                }
                Synchonizing = false;
            }
        }

        #region Download And upload Data Item Queue
        public async Task DownloadAndUploadData(GEQueueItem pItem)
        {
            switch (pItem.EntryName.ToUpper())
            {
                case Constants.GEEnterprises:
                    Logger.Info($"Processando item {pItem.Id} da fila de empresa.");
                    await DownloadAndUploadEnterprise(pItem);
                    break;
                case Constants.GEEnterpriseCategory:
                    Logger.Info($"Processando item {pItem.Id} da fila de categoria de empresa.");
                    await DownloadAndUploadEnterpriseCategory(pItem);
                    break;
                case Constants.GEEnterpriseGroup:
                    Logger.Info($"Processando item {pItem.Id} da fila de grupo de empresa.");
                    await DownloadAndUploadEnterpriseGroup(pItem);
                    break;
                case Constants.GEProducts:
                    Logger.Info($"Processando item {pItem.Id} da fila de produto.");
                    await DownloadAndUploadProducts(pItem);
                    break;
                case Constants.VEOrders:
                    Logger.Info($"Processando item {pItem.Id} da fila de pedidos.");
                    await DownloadAndUploadOrders(pItem);
                    break;
                case Constants.VETransactions:
                    Logger.Info($"Processando item {pItem.Id} da fila de transações.");
                    await DownloadAndUploadTransactions(pItem);
                    break;
                case Constants.VESalePrice:
                    Logger.Info($"Processando item {pItem.Id} da fila de preço de venda.");
                    await DownloadAndUploadSalePrice(pItem);
                    break;
                default:
                    break;
            }

            Logger.Info($"Item {pItem.Id} da fila processado.");
            Logger.Info($"Atualizando status do {pItem.Id} da fila.");

            if (pItem.Id > 0)
            {
                pItem.Modified = DateTime.Now;
                if (pItem.ExceptionCode == null)
                {
                    pItem.Processed = true;
                    pItem.Uploaded = true;
                }
                else if (pItem.ExceptionCode == "not-found")
                {
                    pItem.Processed = true;
                    pItem.Uploaded = true;
                }

                try
                {
                    Logger.Info("Atualizando item na fila");
                    using (var context = new HinoContext())
                    {
                        context.Entry(pItem).State = EntityState.Detached;
                        context.Entry(pItem).State = EntityState.Modified;
                        await context.SaveChangesAsync();
                    }
                }
                catch (Exception ex)
                {
                    Logger.Error("Não foi possível atualizar o item na fila", ex);
                }
            }
        }
        #endregion

        public async Task UploadQueue()
        {
            Logger.Info("Processando itens para upload.");
            GESincConfig[] ListConfig;
            GEQueueItem[] Queue;
            GEEstab[] ListEstab;
            using (var context = new HinoContext())
            {
                ListEstab = context.GEEstab.ToArray();
                ListConfig = context.GESincConfig.ToArray();
                Queue = context.GEQueueItem.Where(r => !r.Uploaded).ToArray();
            }

            Logger.Info($"Itens na fila para upload {Queue?.Length ?? 0}.");

            if ((Queue?.Length ?? 0) > 0 && !Synchonizing)
            {
                Synchonizing = true;
                foreach (var config in ListConfig)
                {
                    var Estab = ListEstab.Where(r => r.codestab == config.codestab).FirstOrDefault();

                    Logger.Info($"Percorrendo: {Estab.codestab} - {Estab.razaosocial}");
                    Globals.EstablishmentKey = config.establishmentkey;
                    Globals.CodEstab = Estab.codestab;

                    var ItemCount = 1;
                    if (!await AuthenticateUser())
                    {
                        Synchonizing = false;
                        return;
                    }

                    var QueueEstab = Queue.Where(r => r.EstablishmentKey == Globals.EstablishmentKey).OrderBy(r => r.Priority).ThenBy(r => r.Created);

                    foreach (var item in QueueEstab)
                    {
                        Logger.Info($"Percorrendo itens na fila {ItemCount}/{Queue?.Length ?? 0}.");
                        if (item.Type == "NEW")
                        {
                            Logger.Info("Atualizando API com os dados gerados.");
                            await UploadDataToAPIByQueueItem(item);
                        }
                        ItemCount++;
                    }
                }
                Synchonizing = false;
            }
        }

        public async Task FirstUpload()
        {
            Logger.Info("Processando itens para o primeiro upload.");
            GESincConfig[] ListConfig;
            GEEstab[] ListEstab;
            using (var context = new HinoContext())
            {
                ListEstab = context.GEEstab.ToArray();
                ListConfig = context.GESincConfig.ToArray();
            }

            Synchonizing = true;
            foreach (var Config in ListConfig)
            {
                var Estab = ListEstab.Where(r => r.codestab == Config.codestab).FirstOrDefault();
                Logger.Info($"Percorrendo: {Estab.codestab} - {Estab.razaosocial}");
                Logger.Info($"Verificando se há atualizações a fazer");
                if (Config.ultimasincronia.ToString("dd/MM/yyyy") == "01/01/0001")
                {
                    Logger.Info($"Sem atualizações para enviar");
                    continue;
                }

                if (!await AuthenticateUser())
                {
                    Synchonizing = false;
                    return;
                }

                var First = new FirstUpload(Estab, Config);
                await First.UploadDados();

            }
            Synchonizing = false;
        }

        #region Upload Sync tables
        public async Task UploadSync()
        {
            Logger.Info("Processando dados das tabelas de sincronia.");
            GESincConfig[] ListConfig;
            GEEstab[] ListEstab;
            VEPedidoSinc[] ListPedidoSinc;
            GESincEmpresa[] ListSincEmpresa;
            FSSincProduto[] ListSincProduto;
            VESincPrVenda[] ListSincPrVenda;
            VESincTransacoes[] ListSincTransacoes;
            CRCategoria[] ListCategoria;
            CRGrupo[] ListGrupo;
            using (var context = new HinoContext())
            {
                ListEstab = context.GEEstab.ToArray();
                ListConfig = context.GESincConfig.ToArray();
                ListPedidoSinc = context.VEPedidoSinc.Where(r => !r.Sincronizado).ToArray();
                ListSincEmpresa = context.GESincEmpresa.Where(r => !r.Sincronizado).ToArray();
                ListSincProduto = context.FSSincProduto.Where(r => !r.Sincronizado).ToArray();
                ListSincPrVenda = context.VESincPrVenda.Where(r => !r.Sincronizado)
                    .Include(s => s.FSProdutoParamEstab).ToArray();
                ListSincTransacoes = context.VESincTransacoes.Where(r => !r.Sincronizado).ToArray();
                ListCategoria = context.CRCategoria.Where(r => !r.Uploaded).ToArray();
                ListGrupo = context.CRGrupo.Where(r => !r.Uploaded).ToArray();
            }

            int TotalItens = (ListPedidoSinc?.Length ?? 0) + (ListSincEmpresa?.Length ?? 0) + (ListSincProduto?.Length ?? 0) +
                             (ListSincPrVenda?.Length ?? 0) + (ListSincTransacoes?.Length ?? 0) + (ListCategoria?.Length ?? 0) +
                             (ListGrupo?.Length ?? 0);

            Logger.Info($"Itens na fila para sincronizar { TotalItens }.");

            if (TotalItens > 0 && !Synchonizing)
            {
                Synchonizing = true;
                foreach (var config in ListConfig)
                {
                    var Estab = ListEstab.Where(r => r.codestab == config.codestab).FirstOrDefault();

                    Logger.Info($"Percorrendo: {Estab.codestab} - {Estab.razaosocial}");
                    Globals.EstablishmentKey = config.establishmentkey;
                    Globals.CodEstab = Estab.codestab;

                    var ItemCount = 1;
                    if (!await AuthenticateUser())
                    {
                        Synchonizing = false;
                        return;
                    }

                    Logger.Info($"Sincronizando itens da tabela GESINCEMPRESA.");

                    var QueueEstabEmpresa = ListSincEmpresa.Where(r => r.CodEstab == Globals.CodEstab).OrderBy(t => t.DataAlt);
                    ItemCount = 1;
                    foreach (var item in QueueEstabEmpresa)
                    {
                        Logger.Info($"Percorrendo itens na fila {ItemCount}/{ListSincEmpresa?.Length ?? 0} da tabela GESINCEMPRESA.");
                        Logger.Info($"Id da tabela de sincronização: {item.CodSinc}.");
                        if (item.Tipo == 0)
                        {
                            Logger.Info("Atualizando API com os dados gerados.");
                            await UploadDataToAPIBySyncItem(item, Constants.GESincEmpresa);
                        }
                        ItemCount++;
                    }

                    Logger.Info($"Sincronizando itens da tabela FSSINCPRODUTO.");

                    var QueueEstabProduto = ListSincProduto.Where(r => r.CodEstab == Globals.CodEstab).OrderBy(t => t.DataAlt);
                    ItemCount = 1;
                    foreach (var item in QueueEstabProduto)
                    {
                        Logger.Info($"Percorrendo itens na fila {ItemCount}/{ListSincProduto?.Length ?? 0} da tabela FSSINCPRODUTO.");
                        Logger.Info($"Id da tabela de sincronização: {item.CodSinc}.");
                        if (item.Tipo == 0)
                        {
                            Logger.Info("Atualizando API com os dados gerados.");
                            await UploadDataToAPIBySyncItem(item, Constants.FSSincProduto);
                        }
                        ItemCount++;
                    }

                    Logger.Info($"Sincronizando itens da tabela VEPEDIDOSINC.");

                    var QueueEstabPedido = ListPedidoSinc.Where(r => r.CodEstab == Globals.CodEstab).OrderBy(t => t.DataAlt);
                    ItemCount = 1;
                    foreach (var item in QueueEstabPedido)
                    {
                        Logger.Info($"Percorrendo itens na fila {ItemCount}/{ListPedidoSinc?.Length ?? 0} da tabela VEPEDIDOSINC.");
                        Logger.Info($"Id da tabela de sincronização: {item.CodSinc}.");
                        if (item.Tipo == 0)
                        {
                            Logger.Info("Atualizando API com os dados gerados.");
                            await UploadDataToAPIBySyncItem(item, Constants.VEPedidoSinc);
                        }
                        ItemCount++;
                    }

                    Logger.Info($"Sincronizando itens da tabela VESINCPRVENDA.");
                    var QueueEstabPrVenda = ListSincPrVenda.Where(r => r.CodEstab == Globals.CodEstab).OrderBy(t => t.DataAlt);
                    ItemCount = 1;
                    foreach (var item in QueueEstabPrVenda)
                    {
                        Logger.Info($"Percorrendo itens na fila {ItemCount}/{ListSincPrVenda?.Length ?? 0} da tabela VESINCPRVENDA.");
                        Logger.Info($"Id da tabela de sincronização: {item.CodSinc}.");
                        if (item.Tipo == 0)
                        {
                            Logger.Info("Atualizando API com os dados gerados.");
                            await UploadDataToAPIBySyncItem(item, Constants.VESincPrVenda);
                        }
                        ItemCount++;
                    }

                    Logger.Info($"Sincronizando itens da tabela VESINCTRANSACOES.");
                    var QueueEstabTransacoes = ListSincTransacoes.Where(r => r.CodEstab == Globals.CodEstab).OrderBy(t => t.DataAlt);
                    ItemCount = 1;
                    foreach (var item in QueueEstabTransacoes)
                    {
                        Logger.Info($"Percorrendo itens na fila {ItemCount}/{ListSincTransacoes?.Length ?? 0} da tabela VESINCTRANSACOES.");
                        Logger.Info($"Id da tabela de sincronização: {item.CodSinc}.");
                        if (item.Tipo == 0)
                        {
                            Logger.Info("Atualizando API com os dados gerados.");
                            await UploadDataToAPIBySyncItem(item, Constants.VESincTransacoes);
                        }
                        ItemCount++;
                    }

                    Logger.Info($"Sincronizando itens da tabela CRCATEGORIA.");

                    ItemCount = 1;
                    foreach (var item in ListCategoria)
                    {
                        Logger.Info($"Percorrendo itens na fila {ItemCount}/{ListCategoria?.Length ?? 0} da tabela CRCATEGORIA.");
                        Logger.Info($"Id da tabela de sincronização: {item.CodCategoria}.");
                        
                        Logger.Info("Atualizando API com os dados gerados.");
                        await UploadEnterpriseCategoryAsync(new GEQueueItem(),
                            new GEEnterpriseCategoryVM()
                            {
                                EstablishmentKey = Globals.EstablishmentKey,
                                IdERP = item.CodCategoria,
                                Description = item.Descricao,
                                Id = 0,
                                UniqueKey = Globals.EstablishmentKey,
                                IsActive = true,
                                Created = DateTime.Now,
                                Modified = DateTime.Now
                            });
                        ItemCount++;
                    }
                    ItemCount = 1;
                    foreach (var item in ListGrupo)
                    {
                        Logger.Info($"Percorrendo itens na fila {ItemCount}/{ListGrupo?.Length ?? 0} da tabela CRGRUPO.");
                        Logger.Info($"Id da tabela de sincronização: {item.CodGrupo}.");

                        Logger.Info("Atualizando API com os dados gerados.");
                        await UploadEnterpriseGroupAsync(new GEQueueItem(),
                            new GEEnterpriseGroupVM()
                            {
                                EstablishmentKey = Globals.EstablishmentKey,
                                IdERP = item.CodGrupo,
                                Description = item.Descricao,
                                Id = 0,
                                UniqueKey = Globals.EstablishmentKey,
                                IsActive = true,
                                Created = DateTime.Now,
                                Modified = DateTime.Now
                            });
                        ItemCount++;
                    }
                }
                Synchonizing = false;
            }
        }
        #endregion

        #region Download and upload
        #region Download And Upload Enterprise
        private async Task DownloadAndUploadEnterprise(GEQueueItem pItem)
        {
            if (pItem.Type == "NEW")
            {
                var Enterprise = await DownloadEnterprise(pItem);
                Logger.Info("Atualizando API com os dados gerados.");
                await UploadEnterpriseAsync(pItem, Enterprise);
            }
            else
                await DeleteEnterprise(pItem);
        }
        #endregion

        #region Download And Upload Enterprise Category
        private async Task DownloadAndUploadEnterpriseCategory(GEQueueItem pItem)
        {
            if (pItem.Type == "NEW")
            {
                var EnterpriseCategory = await DownloadEnterpriseCategory(pItem);
                Logger.Info("Atualizando API com os dados gerados.");
                await UploadEnterpriseCategoryAsync(pItem, EnterpriseCategory);
            }
            else
                await DeleteEnterpriseCategory(pItem);
        }
        #endregion

        #region Download And Upload Enterprise Group
        private async Task DownloadAndUploadEnterpriseGroup(GEQueueItem pItem)
        {
            if (pItem.Type == "NEW")
            {
                var EnterpriseGroup = await DownloadEnterpriseGroup(pItem);
                Logger.Info("Atualizando API com os dados gerados.");
                await UploadEnterpriseGroupAsync(pItem, EnterpriseGroup);
            }
            else
                await DeleteEnterpriseCategory(pItem);
        }
        #endregion

        #region Download And Upload Products
        private async Task DownloadAndUploadProducts(GEQueueItem pItem)
        {
            if (pItem.Type == "NEW")
            {
                // var EnterpriseGroup =
                await DownloadProductAsync(pItem);
                // Logger.Info("Atualizando API com os dados gerados.");
                // await UploadProductAsync(pItem, EnterpriseGroup);
            }
            else
                await DeleteProductAsync(pItem);
        }
        #endregion

        #region Download And Upload Orders
        private async Task DownloadAndUploadOrders(GEQueueItem pItem)
        {
            if (!Globals.Orders)
            {
                pItem.Note = "Ignorado.";
                Logger.Info(pItem.Note);
                return;
            }

            if (pItem.Type == "NEW")
            {
                var order = await DownloadOrderAsync(pItem);
                if (pItem.ExceptionCode == null)
                {
                    Logger.Info("Atualizando API com os dados gerados.");
                    await UploadOrderAsync(pItem, order);
                }
            }
            else
                await DeleteOrderAsync(pItem);
        }
        #endregion

        #region Download And Upload Transactions
        private async Task DownloadAndUploadTransactions(GEQueueItem pItem)
        {
            if (pItem.Type == "NEW")
            {
                var Transaction = await DownloadTransaction(pItem);
                Logger.Info("Atualizando API com os dados gerados.");
                await UploadTransactionAsync(pItem, Transaction);
            }
            else
                await DeleteTransaction(pItem);
        }
        #endregion

        #region Download And Upload SalePrice
        private async Task DownloadAndUploadSalePrice(GEQueueItem pItem)
        {
            if (pItem.Type == "NEW")
            {
                var SalePrice = await DownloadSalePrice(pItem);
                Logger.Info("Atualizando API com os dados gerados.");
                await UploadSalePriceAsync(pItem, SalePrice);
            }
            else
                await DeleteSalePrice(pItem);
        }
        #endregion

        #endregion

        #region Upload Data

        #region Get Product data to upload
        private GEProductsVM GetProductDataToUpload(string pCodProduto)
        {
            using (var context = new HinoContext())
            {
                var result = context.GEVWSincProduto.Where(r => r.CodEstab == Globals.CodEstab && r.CodProduto == pCodProduto)
                    .FirstOrDefault();
                if (result != null)
                {
                    return new GEProductsVM
                    {
                        Id = result.IDApi,
                        ProductKey = result.CodProduto,
                        Image = result.Image,
                        Name = result.Name,
                        Description = result.Description,
                        TypeProd = result.TypeProd,
                        TypeId = 1,
                        Family = result.Family,
                        FamilyId = 1,
                        PercIPI = 0,
                        PercMaxDiscount = result.LimiteDesconto,
                        Value = result.ValorUnitario,
                        StockBalance = result.Saldo,
                        Status = result.Status,
                        NCM = result.NCM,
                        NCMId = 1,
                        EstablishmentKey = Globals.EstablishmentKey,
                        UniqueKey = result.UniqueKey.IsEmpty() ? Globals.EstablishmentKey : result.UniqueKey,
                        Unit = result.CodUnidade,
                        PackageQTD = result.QtdePorEmb,
                        Weight = result.PesoBruto,
                        SaleUnit = result.UmVenda,
                        SaleFactor = result.FatorUmVenda,
                        IsActive = true,
                        CodOrigMerc = result.CodOrigMerc,
                        AplicationId = result.AplicationId,
                    };
                }
            }

            return null;
        }
        #endregion

        #region Get Enterprise data to upload
        private GEEnterprisesVM GetEnterpriseDataToUpload(int pCodEmpresa)
        {
            using (var context = new HinoContext())
            {
                var result = context.GEVWSincEmpresas.Where(r => r.CodEstab == Globals.CodEstab && r.IDERP == pCodEmpresa)
                    .FirstOrDefault();
                if (result != null)
                {
                    var classification = EEnterpriseClassification.Client;
                    var status = EStatusEnterprise.New;
                    switch (result.Classification)
                    {
                        case 1:
                            classification = EEnterpriseClassification.Prospect;
                            break;
                        case 2:
                            classification = EEnterpriseClassification.Carrier;
                            break;
                    }

                    switch (result.Status)
                    {
                        case 1:
                            status = EStatusEnterprise.Active;
                            break;
                        case 2:
                            status = EStatusEnterprise.Inactive;
                            break;
                        case 3:
                            status = EStatusEnterprise.Blocked;
                            break;
                    }

                    var Enterprises = new GEEnterprisesVM
                    {
                        RazaoSocial = result.RazaoSocial,
                        NomeFantasia = result.NomeFantasia,
                        CNPJCPF = result.CpfCnpj,
                        Type = result.TipoCli == 0 ? EEnterpriseType.PessoaFisica : EEnterpriseType.PessoaJuridica,
                        StatusSinc = EStatusSinc.Integrated,
                        EstablishmentKey = Globals.EstablishmentKey,
                        UniqueKey = result.UniqueKey.IsEmpty() ? Globals.EstablishmentKey : result.UniqueKey,
                        Id = result.IDApi,
                        IdERP = result.IDERP,
                        PayConditionId = result.PayConditionID,
                        UserId = result.UserID ?? 0,
                        RegionId = result.RegionID,
                        IsActive = true,
                        CategoryId = result.CategoryId,
                        GroupId = result.GroupId,
                        Classification = classification,
                        ClassifEmpresa = result.ClassifEmpresa,
                        FiscalGroupId = result.FiscalGroupID,
                        CodPrVenda = result.CodPrVenda,
                        IE = result.IE.IsEmpty() ? "N/INFO" : result.IE,
                        Status = status
                    };

                    if (Enterprises.FiscalGroupId > 0)
                    {
                        Enterprises.GEEnterpriseFiscalGroup = new GEEnterpriseFiscalGroupVM
                        {
                            Id = 0,
                            UniqueKey = Guid.NewGuid().ToString(),
                            EstablishmentKey = Globals.EstablishmentKey,
                            Description = result.FSGrpFisEmpDescricao,
                            IdERP = (long)result.FiscalGroupID,
                            Type = result.FSGrpFisEmpType
                        };
                    }

                    if (Enterprises.CategoryId > 0)
                    {
                        Enterprises.GEEnterpriseCategory = new GEEnterpriseCategoryVM
                        {
                            Id = 0,
                            UniqueKey = Guid.NewGuid().ToString(),
                            EstablishmentKey = Globals.EstablishmentKey,
                            Description = result.CRCategoriaDescricao,
                            IdERP = (long)result.CategoryId
                        };
                    }

                    if (Enterprises.GroupId > 0)
                    {
                        Enterprises.GEEnterpriseGroup = new GEEnterpriseGroupVM
                        {
                            Id = 0,
                            UniqueKey = Guid.NewGuid().ToString(),
                            EstablishmentKey = Globals.EstablishmentKey,
                            Description = result.CRGrupoDescricao,
                            IdERP = (long)result.GroupId
                        };
                    }

                    var Addresses = context.GEVWSincEmpresasGeo
                         .Where(r => r.CodEmpresaERP == result.IDERP &&
                         r.CodEstab == Globals.CodEstab)
                        .ToArray();

                    foreach (var Address in Addresses)
                    {
                        var NewEnterpriseGeo = new GEEnterpriseGeoVM();

                        var typeAddress = EAddressType.Commercial;

                        switch (Address.Type)
                        {
                            case 1:
                                typeAddress = EAddressType.Delivery;
                                break;
                            case 2:
                                typeAddress = EAddressType.Levy;
                                break;
                            case 3:
                                typeAddress = EAddressType.Residential;
                                break;
                        }

                        NewEnterpriseGeo.Id = 0;
                        NewEnterpriseGeo.EstablishmentKey = Globals.EstablishmentKey;
                        NewEnterpriseGeo.UniqueKey = result.UniqueKey.IsEmpty() ? Globals.EstablishmentKey : Guid.NewGuid().ToString();
                        NewEnterpriseGeo.EnterpriseID = Enterprises.Id;
                        NewEnterpriseGeo.IE = Address.InscEstadual.IsEmpty() ? "N/INFO" : Address.InscEstadual;
                        NewEnterpriseGeo.Type = typeAddress;
                        NewEnterpriseGeo.CNPJCPF = Address.CpfCnpj;
                        NewEnterpriseGeo.RG = "";
                        NewEnterpriseGeo.CellPhone = "";
                        if (PhoneRegex.IsMatch(Address.CellPhone ?? ""))
                            NewEnterpriseGeo.CellPhone = Address.CellPhone;
                        NewEnterpriseGeo.Phone = "";
                        if (PhoneRegex.IsMatch(Address.Phone ?? ""))
                            NewEnterpriseGeo.Phone = Address.Phone;
                        NewEnterpriseGeo.Email = "";
                        if (EmailRegex.IsMatch(Address.Email ?? ""))
                            NewEnterpriseGeo.Email = Address.Email;

                        NewEnterpriseGeo.Address = Address.Endereco;
                        NewEnterpriseGeo.Complement = Address.Complemento;
                        NewEnterpriseGeo.District = Address.Bairro;
                        NewEnterpriseGeo.Num = Address.Num.ToString();
                        NewEnterpriseGeo.ZipCode = Address.ZipCode;
                        NewEnterpriseGeo.Site = Address.Site;
                        NewEnterpriseGeo.CountryIni = "BR";
                        NewEnterpriseGeo.CountryCode = "BR";
                        NewEnterpriseGeo.CountryName = "BR";
                        NewEnterpriseGeo.UF = Address.UF;
                        NewEnterpriseGeo.StateName = Address.StateName;
                        NewEnterpriseGeo.IBGE = Address.CodIBGE;
                        NewEnterpriseGeo.CityName = Address.CityName;
                        NewEnterpriseGeo.DisplayLat = Address.DisplayLat;
                        NewEnterpriseGeo.DisplayLng = Address.DisplayLng;
                        NewEnterpriseGeo.NavLat = Address.NavLat;
                        NewEnterpriseGeo.NavLng = Address.NavLng;
                        NewEnterpriseGeo.CodBacen = Address.CodBacen;
                        NewEnterpriseGeo.InscSuframa = Address.InscSuframa;

                        Enterprises.GEEnterpriseGeo.Add(NewEnterpriseGeo);
                    }

                    return Enterprises;
                }
            }

            return null;
        }
        #endregion

        #region Get Order data to upload
        private async Task<VEOrdersVM> GetOrderDataToUpload(long pCodPedVenda)
        {
            using (var context = new HinoContext())
            {
                var result = context.VEVWSincCabecPedido.Where(r => r.CodEstab == Globals.CodEstab && r.IDERP == pCodPedVenda)
                    .FirstOrDefault();
                if (result != null)
                {
                    var freightPaidBy = EFreightPaidBy.SemFrete;
                    switch (result.FreightPaidBy)
                    {
                        case 0:
                            freightPaidBy = EFreightPaidBy.Emitente;
                            break;
                        case 1:
                            freightPaidBy = EFreightPaidBy.Destinatario;
                            break;
                        case 2:
                            freightPaidBy = EFreightPaidBy.Terceiros;
                            break;
                        case 3:
                            freightPaidBy = EFreightPaidBy.ProprioEmitente;
                            break;
                        case 4:
                            freightPaidBy = EFreightPaidBy.ProprioDestinatario;
                            break;
                    }
                    var redispatchPaidBy = EFreightPaidBy.SemFrete;
                    switch (result.RedispatchPaidBy)
                    {
                        case 0:
                            redispatchPaidBy = EFreightPaidBy.Emitente;
                            break;
                        case 1:
                            redispatchPaidBy = EFreightPaidBy.Destinatario;
                            break;
                        case 2:
                            redispatchPaidBy = EFreightPaidBy.Terceiros;
                            break;
                        case 3:
                            redispatchPaidBy = EFreightPaidBy.ProprioEmitente;
                            break;
                        case 4:
                            redispatchPaidBy = EFreightPaidBy.ProprioDestinatario;
                            break;
                    }


                    var Order = new VEOrdersVM
                    {
                        StatusSinc = 2,
                        EstablishmentKey = Globals.EstablishmentKey,
                        UniqueKey = result.UniqueKey.IsEmpty() ? Globals.EstablishmentKey : result.UniqueKey,
                        Id = result.ID,
                        IdERP = result.IDERP,
                        NumPedMob = 0,
                        Status = result.Status,
                        StatusCRM = result.StatusCRM,
                        CarrierID = result.CarrierID,
                        RedispatchID = result.RedispatchID,
                        ClientOrder = result.ClientOrder,
                        CodPedVenda = result.IDERP,
                        EnterpriseID = result.EnterpriseID,
                        IsProposal = result.IsProposal,
                        DeliveryDate = result.DeliveryDate,
                        FreightPaidBy = freightPaidBy,
                        RedispatchPaidBy = redispatchPaidBy,
                        FreightValue = result.FreightValue,
                        MainOrderID = result.MainOrderID,
                        UserID = result.UserID,
                        TypePaymentID = result.TypePaymentID,
                        OrderVersion = result.OrderVersion,
                        Note = result.Note,
                        OriginOrderID = result.OriginOrderID,
                        PayConditionID = result.PayConditionID,
                        PercCommission = result.PercCommission,
                        PercDiscount = result.PercDiscount,
                        InnerNote = result.InnerNote,
                        OnlyOnDate = result.SomenteData == 1,
                        AllowPartial = result.FaturarParc == 1,
                        FinancialTaxes = result.FinancialTaxes,
                        ContactPhone = result.ContatoFone,
                        ContactEmail = result.ContatoEmail,
                        Contact = result.Contato,
                        DigitizerID = result.DigitizerID,
                        Sector = result.Sector.ToContactSectors(),
                        RevisionReason = result.RevisionReason,
                        UserCreatedID = result.DigitizerID,
                        InPerson = result.InPerson,
                        PaymentDueDate = result.PaymentDueDate,
                        StatusPay = result.StatusPay,
                        PaidAmount = result.PaidAmount,
                        AdvanceAmount = result.AdvanceAmount
                    };

                    var Items = context.VEVWSincItemPedido.AsNoTracking().Where(r => r.IDERP == result.IDERP &&
                        r.CodEstab == Globals.CodEstab).ToList();

                    foreach (var item in Items)
                    {
                        var NewOrderItem = new VEOrderItemsVM
                        {
                            OrderID = Order.Id,
                            ProductID = item.ProductID,
                            FiscalOperID = item.FiscalOperID,
                            TableValue = item.TableValue,
                            Value = item.Value,
                            Quantity = item.Quantity,
                            QuantityReference = item.QuantityReference,
                            PercDiscount = item.PercDiscount,
                            Note = item.Note,
                            Item = item.Item,
                            ItemLevel = item.ItemLevel,
                            ClientOrder = item.ClientOrder,
                            ClientItem = item.ClientItem,
                            DeliveryDate = item.DeliveryDate,
                            PercCommission = item.PercCommission,
                            PercCommissionHead = item.PercCommissionHead,
                            PercDiscountHead = item.PercDiscountHead,
                            ShippingDays = item.ShippingDays,
                            AltDescription = item.AltDescription,
                            QuantityReturned = item.QuantityReturned,
                            IdERP = item.IDERP,
                            Id = item.ID,
                            EstablishmentKey = Globals.EstablishmentKey,
                            UniqueKey = item.UniqueKey.IsEmpty() ? Globals.EstablishmentKey : item.UniqueKey,
                            FSFiscalOper = new FSFiscalOperVM
                            {
                                Id = 0,
                                IdERP = item.FiscalOperID,
                                EstablishmentKey = Globals.EstablishmentKey,
                                UniqueKey = Guid.NewGuid().ToString(),
                                Description = item.FSNatOperDescricao
                            }
                        };

                        Order.VEOrderItems.Add(NewOrderItem);
                    }
                    var OrderService = new OrdersService();
                    await OrderService.LoadOrderTaxesAsync(context, Order);
                    await OrderService.LoadOrderCommissionAsync(context, Order);

                    return Order;
                }
            }

            return null;
        }
        #endregion

        #region Get Product data to upload
        private VESalePriceVM GetSalePriceDataToUpload(long pCodPrVenda, string pCodProduto)
        {
            using (var context = new HinoContext())
            {
                var result = context.VEVWSincTabPreco.Where(r => r.CodEstab == Globals.CodEstab && 
                                                                 r.CodProduto == pCodProduto &&
                                                                 r.CodPrvenda == pCodPrVenda)
                    .FirstOrDefault();
                if (result != null)
                {
                    return new VESalePriceVM
                    {
                        Id = result.IDApi,
                        ProductKey = result.CodProduto,
                        CodPrVenda = result.CodPrvenda,
                        Description = result.Descricao,
                        EstablishmentKey = Globals.EstablishmentKey,
                        ProductId = result.CodProdutoApi,
                        RegionId = result.CodRegiaoApi,
                        Value = result.ValorUnitario,
                        UniqueKey = result.UniqueKey ?? Globals.EstablishmentKey
                    };
                }
            }

            return null;
        }
        #endregion
   
        private VETransactionsVM GetTransactionDataToUpload(long pCodTransacao)
        {
            using (var context = new HinoContext())
            {
                var result = context.VEVWSincTransacoes.Where(r => r.CodEstab == Globals.CodEstab && r.CodTransacao == pCodTransacao)
                    .FirstOrDefault();
                if (result != null)
                {
                    return new VETransactionsVM
                    {
                        Id = result.IdApi,
                        AcquirerName = result.AcquirerName,
                        AdminCode = result.AdminCode,
                        Amount = result.Valor,
                        AuthCode = result.AuthCode,
                        AuthDate = result.AuthDate,
                        CardBrand = result.CardBrand,
                        CardLastDigits = result.CardLastDigits,
                        CustomerReceipt = result.CustomerReceipt,
                        Description = result.Descricao,
                        EstablishmentKey = Globals.EstablishmentKey,
                        Installments = result.Parcelas,
                        IsActive = true,
                        MerchantReceipt = result.MerchantReceipt,
                        NSU = result.Nsu,
                        IdERP = result.CodTransacao,
                        OrderID = result.OrderId,
                        Origin = (ETransactionOrigin)result.Origem,
                        Reversed = result.Estornado == 1,
                        Status = (ETransactionStatus)result.Status,
                        StatusSinc = EStatusSinc.Sincronized,
                        Type = (ETransactionType)result.Tipo,
                        UniqueKey = result.UniqueKey
                    };
                }
            }

            return null;
        }
        #endregion

        #region Update Order UniqueKey/IdApi
        private async Task UpdateOrderUniqueKeyApiAsync(VEOrdersVM[] pOrders, VEPedidoSinc pItem)
        {
            if (pOrders.Any())
            {
                Logger.Info($"Atualizando UniqueKey e IdApi do pedido {pItem.CodPedVenda}");
                try
                {

                    using (var context = new HinoContext())
                    {
                        using (var cmd = context.Database.Connection.CreateCommand())
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = @"UPDATE VEPEDVENDA
                                                   SET UNIQUEKEY = NVL(UNIQUEKEY, :pUNIQUEKEY),
                                                       IDAPI = :pIDAPI,
                                                       CODINTEGRACAO = :pCODINTEGRACAO,
                                                       CODPEDINTEGRACAO = :pCODPEDINTEGRACAO
                                                 WHERE CODESTAB = :pCODESTAB
                                                   AND CODPEDVENDA = :pCODPEDVENDA";
                            var uniqueKeyParam = new OracleParameter("pUNIQUEKEY", OracleDbType.Varchar2, pOrders[0].UniqueKey, ParameterDirection.Input);
                            var idAPIParam = new OracleParameter("pIDAPI", OracleDbType.Int64, pOrders[0].Id, ParameterDirection.Input);
                            var idCodIntegracao = new OracleParameter("pCODINTEGRACAO", OracleDbType.Varchar2, $"{pOrders[0].Id}/{pOrders[0].OrderVersion}", ParameterDirection.Input);
                            var idCodPedIntegracao = new OracleParameter("pCODPEDINTEGRACAO", OracleDbType.Varchar2, $"{pOrders[0].Id}/{pOrders[0].OrderVersion}" , ParameterDirection.Input);
                            var codEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, Globals.CodEstab, ParameterDirection.Input);
                            var codProduto = new OracleParameter("pCODPRODUTO", OracleDbType.Varchar2, pItem.CodPedVenda, ParameterDirection.Input);

                            cmd.Parameters.AddRange(new[] {
                                uniqueKeyParam,
                                idAPIParam,
                                idCodIntegracao,
                                idCodPedIntegracao,
                                codEstabParam,
                                codProduto
                            });

                            cmd.Connection.Open();
                            await cmd.ExecuteNonQueryAsync();
                            cmd.Connection.Close();
                        }

                        VEOrdersAPI ordersAPI = new VEOrdersAPI();
                        var order = await ordersAPI.GetByIdAsync(pOrders[0].UniqueKey, pOrders[0].Id);
                        foreach (var item in order.VEOrderItems.Where(r => r.IdERP > 0))
                        {
                            using (var cmd = context.Database.Connection.CreateCommand())
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandText = @"UPDATE VEITEMPEDDET
                                                   SET UNIQUEKEY = NVL(UNIQUEKEY, :pUNIQUEKEY),
                                                       IDAPI = :pIDAPI
                                                 WHERE CODESTAB = :pCODESTAB
                                                   AND CODPEDVENDA = :pCODPEDVENDA
                                                   AND CODPRODUTO = :pCODPRODUTO
                                                   AND DATAPROGRAMA = :pDATAPROGRAMA
                                                   AND IDAPI = 0";
                                var uniqueKeyParam = new OracleParameter("pUNIQUEKEY", OracleDbType.Varchar2, item.UniqueKey, ParameterDirection.Input);
                                var idAPIParam = new OracleParameter("pIDAPI", OracleDbType.Int64, item.Id, ParameterDirection.Input);
                                var codEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, Globals.CodEstab, ParameterDirection.Input);
                                var codPedVendaParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pItem.CodPedVenda, ParameterDirection.Input);
                                var codProdutoParam = new OracleParameter("pCODPRODUTO", OracleDbType.Varchar2, item.GEProducts.ProductKey, ParameterDirection.Input);
                                var dataProgramaParam = new OracleParameter("pDATAPROGRAMA", OracleDbType.Date, item.DeliveryDate, ParameterDirection.Input);

                                cmd.Parameters.AddRange(new[] {
                                    uniqueKeyParam,
                                    idAPIParam,
                                    codEstabParam,
                                    codPedVendaParam,
                                    codProdutoParam,
                                    dataProgramaParam
                                });

                                cmd.Connection.Open();
                                await cmd.ExecuteNonQueryAsync();
                                cmd.Connection.Close();
                            }
                        }

                    }
                }
                catch (Exception ex)
                {
                    var ExceptionCode = Guid.NewGuid().ToString();

                    Logger.Error("Não foi possível atualizar os dados UniqueKey e IdApi.", ex, ExceptionCode);
                }

                foreach (var item in pOrders[0].VEOrderItems)
                {
                    Logger.Info($"Atualizando UniqueKey e IdApi do pedido - Item { item.Id }");

                    try
                    {
                        using (var context = new HinoContext())
                        {
                            using (var cmd = context.Database.Connection.CreateCommand())
                            {
                                cmd.CommandType = CommandType.Text;
                                cmd.CommandText = @"UPDATE VEITEMPEDDET
                                                   SET UNIQUEKEY = NVL(UNIQUEKEY, :pUNIQUEKEY),
                                                       IDAPI = :pIDAPI
                                                 WHERE CODESTAB = :pCODESTAB
                                                   AND CODPRODUTO = :pCODPRODUTO";
                                var uniqueKeyParam = new OracleParameter("pUNIQUEKEY", OracleDbType.Varchar2, item.UniqueKey, ParameterDirection.Input);
                                var idAPIParam = new OracleParameter("pIDAPI", OracleDbType.Int64, item.Id, ParameterDirection.Input);
                                var codEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, Globals.CodEstab, ParameterDirection.Input);
                                var codProduto = new OracleParameter("pCODPRODUTO", OracleDbType.Varchar2, pItem.CodPedVenda, ParameterDirection.Input);

                                cmd.Parameters.AddRange(new[] {
                                uniqueKeyParam,
                                idAPIParam,
                                codEstabParam,
                                codProduto
                            });

                                cmd.Connection.Open();
                                await cmd.ExecuteNonQueryAsync();
                                cmd.Connection.Close();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        var ExceptionCode = Guid.NewGuid().ToString();

                        Logger.Error("Não foi possível atualizar os dados UniqueKey e IdApi dos itens.", ex, ExceptionCode);
                    }
                }
            }
        }
        #endregion

        #region Update Enterprise UniqueKey/IdApi
        private async Task UpdateEnterpriseUniqueKeyApiAsync(GEEnterprisesVM[] pEnterprises, GESincEmpresa pItem)
        {
            if (pEnterprises.Any())
            {
                Logger.Info($"Atualizando UniqueKey e IdApi da empresa {pItem.CodEmpresa}");
                try
                {

                    using (var context = new HinoContext())
                    {
                        using (var cmd = context.Database.Connection.CreateCommand())
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = @"UPDATE GEEMPRESAPARAMESTAB
                                               SET UNIQUEKEY = NVL(UNIQUEKEY, :pUNIQUEKEY),
                                                   IDAPI = :pIDAPI,
                                                   CODINTEGRACAO = :pCODINTEGRACAO
                                             WHERE CODESTAB = :pCODESTAB
                                               AND CODEMPRESA = :pCODEMPRESA";
                            var uniqueKeyParam = new OracleParameter("pUNIQUEKEY", OracleDbType.Varchar2, pEnterprises[0].UniqueKey, ParameterDirection.Input);
                            var idAPIParam = new OracleParameter("pIDAPI", OracleDbType.Int64, pEnterprises[0].Id, ParameterDirection.Input);
                            var idCodIntegracao = new OracleParameter("pCODINTEGRACAO", OracleDbType.Int64, pEnterprises[0].Id, ParameterDirection.Input);
                            var codEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, Globals.CodEstab, ParameterDirection.Input);
                            var codProduto = new OracleParameter("pCODPRODUTO", OracleDbType.Varchar2, pItem.CodEmpresa, ParameterDirection.Input);

                            cmd.Parameters.AddRange(new[] {
                                uniqueKeyParam,
                                idAPIParam,
                                idCodIntegracao,
                                codEstabParam,
                                codProduto
                            });

                            cmd.Connection.Open();
                            await cmd.ExecuteNonQueryAsync();
                            cmd.Connection.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    var ExceptionCode = Guid.NewGuid().ToString();

                    Logger.Error("Não foi possível atualizar os dados UniqueKey e IdApi.", ex, ExceptionCode);
                }
            }
        }
        #endregion

        #region Update Product UniqueKey/IdApi
        private async Task UpdateProductUniqueKeyApiAsync(GEProductsVM[] pProducts, FSSincProduto pItem)
        {
            if (pProducts.Any())
            {
                Logger.Info($"Atualizando UniqueKey e IdApi do produto {pItem.CodProduto}");
                try
                {

                    using (var context = new HinoContext())
                    {
                        using (var cmd = context.Database.Connection.CreateCommand())
                        {
                            cmd.CommandType = CommandType.Text;
                            cmd.CommandText = @"UPDATE FSPRODUTOPARAMESTAB
                                               SET UNIQUEKEY = NVL(UNIQUEKEY, :pUNIQUEKEY),
                                                   IDAPI = :pIDAPI
                                             WHERE CODESTAB = :pCODESTAB
                                               AND CODPRODUTO = :pCODPRODUTO";
                            var uniqueKeyParam = new OracleParameter("pUNIQUEKEY", OracleDbType.Varchar2, pProducts[0].UniqueKey, ParameterDirection.Input);
                            var idAPIParam = new OracleParameter("pIDAPI", OracleDbType.Int64, pProducts[0].Id, ParameterDirection.Input);
                            var codEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, Globals.CodEstab, ParameterDirection.Input);
                            var codProduto = new OracleParameter("pCODPRODUTO", OracleDbType.Varchar2, pItem.CodProduto, ParameterDirection.Input);

                            cmd.Parameters.AddRange(new[] {
                                uniqueKeyParam,
                                idAPIParam,
                                codEstabParam,
                                codProduto
                            });

                            cmd.Connection.Open();
                            await cmd.ExecuteNonQueryAsync();
                            cmd.Connection.Close();
                        }
                    }
                }
                catch (Exception ex)
                {
                    var ExceptionCode = Guid.NewGuid().ToString();

                    Logger.Error("Não foi possível atualizar os dados UniqueKey e IdApi.", ex, ExceptionCode);
                }
            }
        }
        #endregion

        #region Update Sale Price UniqueKey/IdApi
        private Task UpdateSalePriceUniqueKeyApiAsync(VESalePriceVM[] pSalePrice, VESincPrVenda pItem)
        {
            return Task.FromResult(false);
        }
        #endregion

        #region UploadDataToAPIBySyncItem
        private async Task UploadDataToAPIBySyncItem(ISincData pItem, string EntryName)
        {
            var result = false;
            var ExceptionCode = "";
            switch (EntryName.ToUpper())
            {
                case Constants.VEPedidoSinc:
                    try
                    {
                        var order = await GetOrderDataToUpload(((VEPedidoSinc)pItem).CodPedVenda);
                        if (order == null)
                            throw new Exception($"Dados do pedido: {((VEPedidoSinc)pItem).CodPedVenda} não localizados.");
                        var _IVEOrdersAPI = new VEOrdersAPI();
                        var resultApi = await PostOrderERPAsync(_IVEOrdersAPI, new VEOrdersVM[] { order });
                        if (_IVEOrdersAPI.Errors.Any())
                            throw new Exception(string.Join(",", _IVEOrdersAPI.Errors[0].Messages));

                        await UpdateOrderUniqueKeyApiAsync(resultApi, (VEPedidoSinc)pItem);

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ExceptionCode = ex.Message.Contains("não localizados") ? "not-found" : Guid.NewGuid().ToString();

                        Logger.Error("Não foi possível atualizar os dados na API.", ex, ExceptionCode);

                        result = false;
                    }
                    break;
                case Constants.GESincEmpresa:
                    try
                    {
                        var enterprise = GetEnterpriseDataToUpload(((GESincEmpresa)pItem).CodEmpresa);
                        if (enterprise == null)
                            throw new Exception($"Dados da empresa: {((GESincEmpresa)pItem).CodEmpresa} não localizados.");
                        var _IGEEnterprisesAPI = new GEEnterprisesAPI();
                        var resultApi = await PostEnterpriseERPAsync(_IGEEnterprisesAPI, enterprise);
                        if (_IGEEnterprisesAPI.Errors.Any())
                            throw new Exception(string.Join(",", _IGEEnterprisesAPI.Errors[0].Messages));

                        await UpdateEnterpriseUniqueKeyApiAsync(resultApi, (GESincEmpresa)pItem);

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ExceptionCode = ex.Message.Contains("não localizados") ? "not-found" : Guid.NewGuid().ToString();

                        Logger.Error("Não foi possível atualizar os dados na API.", ex, ExceptionCode);

                        result = false;
                    }
                    break;
                case Constants.FSSincProduto:
                    try
                    {
                        var product = GetProductDataToUpload(((FSSincProduto)pItem).CodProduto);
                        if (product == null)
                            throw new Exception($"Dados do produto: {((FSSincProduto)pItem).CodProduto} não localizados.");
                        var _GEProductsAPI = new GEProductsAPI();
                        var resultApi = await PostProductAsync(_GEProductsAPI, new GEProductsVM[] { product });
                        if (_GEProductsAPI.Errors.Any())
                            throw new Exception(string.Join(",", _GEProductsAPI.Errors[0].Messages));

                        await UpdateProductUniqueKeyApiAsync(resultApi, (FSSincProduto)pItem);

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ExceptionCode = ex.Message.Contains("não localizados") ? "not-found" : Guid.NewGuid().ToString();
                        if (pItem.CodSinc > 0)
                            ExceptionCode = ex.Message.Contains("NCM é de preenchimento obrigatório.") ? "not-found" : ExceptionCode;

                        Logger.Error("Não foi possível atualizar os dados na API.", ex, ExceptionCode);

                        result = false;
                    }
                    break;
                case Constants.VESincPrVenda:
                    try
                     {
                        var itemSinc = (VESincPrVenda)pItem;
                        Logger.Info($"Sincronizando dados do produto: { itemSinc.CodProduto } antes de sinc. a tabela de preço");
                        var prodSinc = new FSSincProduto
                        {
                            CodProduto = itemSinc.CodProduto
                        };

                        if (!itemSinc.FSProdutoParamEstab.PermSinc)
                        {
                            Logger.Info($"Produto: { itemSinc.CodProduto } não está marcado para ser sincronizado (Produto|Permite sincronizar)");
                            throw new Exception($"Produto: { itemSinc.CodProduto } não está marcado para ser sincronizado (Produto|Permite sincronizar)");
                        }
                        await UploadDataToAPIBySyncItem(prodSinc, Constants.FSSincProduto);
                        if (prodSinc.Sincronizado)
                        {
                            var salePrice = GetSalePriceDataToUpload(itemSinc.CodPrVenda, itemSinc.CodProduto);

                            if (salePrice == null)
                                throw new Exception($"Dados da tabela de preço: { itemSinc.CodPrVenda} - { itemSinc.CodProduto } não localizados.");
                       
                            Logger.Info($"Sincronização dos dados do produto: { itemSinc.CodProduto } finalizada com sucesso");

                            Logger.Info($"Sincronizando tabela de preço do produto: { itemSinc.CodProduto } ");

                            var _VESalePriceAPI = new VESalePriceAPI();
                            var resultApi = await PostSalePriceAsync(_VESalePriceAPI, new VESalePriceVM[] { salePrice });

                            if (_VESalePriceAPI.Errors.Any())
                                throw new Exception(string.Join(",", _VESalePriceAPI.Errors[0].Messages));

                            await UpdateSalePriceUniqueKeyApiAsync(resultApi, (VESincPrVenda)pItem);

                            result = true;
                        }
                        else
                        {
                            result = false;
                            throw new Exception($"Produto: { itemSinc.CodProduto } não foi sincronizado com o portal.");
                        }
                    }
                    catch (Exception ex)
                    {
                        ExceptionCode = ex.Message.Contains("não localizados") ? "not-found" : Guid.NewGuid().ToString();
                        ExceptionCode = ex.Message.Contains("(Produto|Permite sincronizar)") ? "not-found" : ExceptionCode;
                        ExceptionCode = ex.Message.Contains("não foi sincronizado com o portal") ? "not-found" : ExceptionCode;

                        Logger.Error("Não foi possível atualizar os dados na API.", ex, ExceptionCode);

                        result = false;
                    }
                    break;
                case Constants.VESincTransacoes:
                    try
                    {
                        var transaction = GetTransactionDataToUpload(((VESincTransacoes)pItem).CodTransacao);
                        if (transaction == null)
                            throw new Exception($"Dados da transação: {((VESincTransacoes)pItem).CodTransacao} não localizados.");
                        var _IVETransactionsAPI = new VETransactionsAPI();
                        var resultApi = await PostTransactionERPAsync(_IVETransactionsAPI, new VETransactionsVM[] { transaction } );
                        if (_IVETransactionsAPI.Errors.Any())
                            throw new Exception(string.Join(",", _IVETransactionsAPI.Errors[0].Messages));

                        //await UpdateTransactionUniqueKeyApiAsync(resultApi, (VESincTransacoes)pItem);

                        result = true;
                    }
                    catch (Exception ex)
                    {
                        ExceptionCode = ex.Message.Contains("não localizados") ? "not-found" : Guid.NewGuid().ToString();

                        Logger.Error("Não foi possível atualizar os dados na API.", ex, ExceptionCode);

                        result = false;
                    }
                    break;
                default:
                    break;
            }

            if (result || ExceptionCode == "not-found")
            {
                pItem.Sincronizado = true;
                pItem.DataSinc = DateTime.Now;
            }

            try
            {
                if (pItem.CodSinc > 0)
                {
                    Logger.Info("Atualizando item na fila");
                    using (var context = new HinoContext())
                    {
                        context.Entry(pItem).State = EntityState.Detached;
                        context.Entry(pItem).State = EntityState.Modified;
                        await context.SaveChangesAsync();
                    }
                }
            }
            catch (Exception ex)
            {
                pItem.Sincronizado = false;
                Logger.Error("Não foi possível atualizar o item na fila", ex);
            }
        }
        #endregion

        #region UploadDataToAPIByQueueItem
        private async Task UploadDataToAPIByQueueItem(GEQueueItem pItem)
        {
            var result = false;
            /*switch (pItem.EntryName.ToUpper())
            {
                case Constants.GEEnterprises:
                    result = await UploadEnterpriseAsync(pItem);
                    break;
                case Constants.VEOrders:
                    break;
                case Constants.GEEnterpriseCategory:
                    result = await UploadEnterpriseCategoryAsync(pItem);
                    break;
                case Constants.GEEnterpriseGroup:
                    result = await UploadEnterpriseGroupAsync(pItem);
                    break;
                default:
                    break;
            }*/

            pItem.Modified = DateTime.Now;
            pItem.Uploaded = result;

            try
            {
                Logger.Info("Atualizando item na fila");
                using (var context = new HinoContext())
                {
                    context.Entry(pItem).State = EntityState.Detached;
                    context.Entry(pItem).State = EntityState.Modified;
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Não foi possível atualizar o item na fila", ex);
            }
        }
        #endregion

        #region Enterprise
        #region Upload Enterprises
        private async Task<bool> UploadEnterpriseAsync(GEQueueItem pItem, GEEnterprisesVM pEnterprise)
        {
            try
            {
                using (var context = new HinoContext())
                {
                    var _IGEEnterprisesAPI = new GEEnterprisesAPI();
                    Logger.Info("Buscando dados da empresa na CREnterprises.");
                    var NewEnterprise = context.CREnterprises.Where(r => r.Id == pItem.IdReference).FirstOrDefault();

                    if (NewEnterprise.IdERP > 0)
                    {
                        Logger.Info($"IdERP: {NewEnterprise.IdERP}.");

                        var Enterprise = await _IGEEnterprisesAPI.GetByIdAsync(pItem.UniqueKey, pItem.IdReference);
                        Enterprise.IdERP = NewEnterprise.IdERP;
                        Enterprise.StatusSinc = EStatusSinc.Integrated;
                        Enterprise.IE = Enterprise.IE.IsEmpty() ? "N/INFO" : Enterprise.IE;
                        foreach (var item in Enterprise.GEEnterpriseGeo)
                            item.IE = Enterprise.IE.IsEmpty() ? "N/INFO" : Enterprise.IE;

                        await PostEnterpriseAsync(_IGEEnterprisesAPI, Enterprise);
                    }
                }
                pItem.Note = "Dados da API Atualizados com sucesso.";
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível atualizar os dados na API.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;
                return false;
            }
            return true;
        }
        #endregion

        #region Post enterprise
        private async Task PostEnterpriseAsync(GEEnterprisesAPI pIGEEnterprisesAPI, GEEnterprisesVM pEnterprise)
        {
            pEnterprise.StatusSinc = EStatusSinc.Integrated;
            await pIGEEnterprisesAPI.PostSyncDataAsync(new GEEnterprisesVM[] { pEnterprise });
        }

        private async Task<GEEnterprisesVM[]> PostEnterpriseERPAsync(GEEnterprisesAPI pIGEEnterprisesAPI, GEEnterprisesVM pEnterprise)
        {
            pEnterprise.StatusSinc = EStatusSinc.Integrated;
            return await pIGEEnterprisesAPI.PostSyncDataERPAsync(new GEEnterprisesVM[] { pEnterprise });
        }
        #endregion

        #endregion

        #region Enterprise Category
        #region Upload Enterprises Category
        private async Task<bool> UploadEnterpriseCategoryAsync(GEQueueItem pItem, GEEnterpriseCategoryVM pBaseEntity)
        {
            try
            {
                using (var context = new HinoContext())
                {
                    var _GEEnterprisesCategoryAPI = new GEEnterprisesCategoryAPI();
                    Logger.Info("Buscando dados da categoria de empresa.");
                    var NewCategories = await context.CRCategoria.Where(r => r.CodCategoria == pBaseEntity.IdERP).FirstAsync();

                    if (NewCategories != null)
                    {
                        NewCategories.Uploaded = true;
                        await PostEnterpriseCategoryAsync(_GEEnterprisesCategoryAPI, new[] { pBaseEntity });
                        context.SaveChanges();
                    }
                }
                pItem.Note = "Dados da API Atualizados com sucesso.";
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível atualizar os dados na API.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;
                return false;
            }
            return true;
        }
        #endregion

        #region Post enterprise Category
        private async Task PostEnterpriseCategoryAsync(GEEnterprisesCategoryAPI pIGEEnterprisesAPI, GEEnterpriseCategoryVM[] pEnterpriseCategories)
        {
            await pIGEEnterprisesAPI.PostSyncDataAsync(pEnterpriseCategories);
        }
        #endregion

        #endregion

        #region Enterprise Group
        #region Upload Enterprises Group
        private async Task<bool> UploadEnterpriseGroupAsync(GEQueueItem pItem, GEEnterpriseGroupVM pEnterpriseGroup)
        {
            try
            {
                using (var context = new HinoContext())
                {
                    var _GEEnterpriseGroupAPI = new GEEnterpriseGroupAPI();
                    Logger.Info("Buscando dados do grupo da empresa.");
                    var NewCategories = await context.CRGrupo.Where(r => r.CodGrupo == pEnterpriseGroup.IdERP).FirstAsync();

                    if (NewCategories != null)
                    {
                        NewCategories.Uploaded = true;
                        await PostEnterpriseGroupAsync(_GEEnterpriseGroupAPI, new[] { pEnterpriseGroup });
                        context.SaveChanges();
                    }
                }
                pItem.Note = "Dados da API Atualizados com sucesso.";
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível atualizar os dados na API.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;
                return false;
            }
            return true;
        }
        #endregion

        #region Post enterprise Group
        private async Task PostEnterpriseGroupAsync(GEEnterpriseGroupAPI pIGEEnterprisesAPI, GEEnterpriseGroupVM[] pEnterpriseGroups)
        {
            await pIGEEnterprisesAPI.PostSyncDataAsync(pEnterpriseGroups);
        }
        #endregion

        #endregion

        #region Products
        #region Upload Products
        private async Task<bool> UploadProductAsync(GEQueueItem pItem, GEProductsVM pProduct)
        {
            try
            {
                using (var context = new HinoContext())
                {
                    var _GEProductsAPI = new GEProductsAPI();
                    Logger.Info("Buscando dados do grupo da empresa.");
                    var NewProduct = context.CRProducts.Where(r => r.Id == pItem.IdReference).FirstAsync();

                    if (NewProduct != null)
                    {

                        await PostProductAsync(_GEProductsAPI, new GEProductsVM[] { pProduct });
                    }
                }
                pItem.Note = "Dados da API Atualizados com sucesso.";
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível atualizar os dados na API.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;
                return false;
            }
            return true;
        }
        #endregion

        #region Post Product
        private async Task<GEProductsVM[]> PostProductAsync(GEProductsAPI pIGEEnterprisesAPI, GEProductsVM[] pProduct) =>
            await pIGEEnterprisesAPI.PostSyncDataAsync(pProduct);
        #endregion
        #endregion

        #region Orders
        #region Upload Order
        private async Task<bool> UploadOrderAsync(GEQueueItem pItem, VEOrdersVM pOrder)
        {
            try
            {
                using (var context = new HinoContext())
                {
                    var _VEOrdersAPI = new VEOrdersAPI();
                    Logger.Info("Buscando dados do pedido.");
                    var NewOrder = context.CROrders
                        .Include(r => r.VEOrderItems)
                        .Where(r => r.Id == pItem.IdReference).FirstOrDefault();

                    if (NewOrder != null && NewOrder.IdERP > 0)
                    {
                        Logger.Info($"IdERP: {NewOrder.IdERP}.");
                        var order = await _VEOrdersAPI.GetByIdAsync(pItem.UniqueKey, pItem.IdReference);
                        order.IdERP = NewOrder.IdERP;
                        order.CodPedVenda = NewOrder.CodPedVenda;
                        order.StatusSinc = 2;
                        order.GEEnterprises = null;
                        order.GECarriers = null;
                        order.GERedispatch = null;
                        order.GEUsers = null;
                        order.GEPaymentType = null;
                        order.GEPaymentCondition = null;
                        order.GEPaymentType = null;

                        foreach (var item in order.VEOrderItems)
                        {
                            item.GEProducts = null;
                            item.FSFiscalOper = null;
                            item.IdERP = NewOrder.IdERP;
                        }

                        foreach (var item in order.VETransactions)
                            item.VEOrders = null;

                        Logger.Info("Carregando impostos no pedido");
                        var OrderService = new OrdersService();
                        await OrderService.LoadOrderTaxesAsync(context, order);
                        await OrderService.LoadOrderCommissionAsync(context, order);

                        await PostOrderAsync(_VEOrdersAPI, new VEOrdersVM[] { order });
                    }
                }
                pItem.Note = "Dados da API Atualizados com sucesso.";
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível atualizar os dados na API.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;
                return false;
            }
            return true;
        }
        #endregion

        #region Post Order
        private async Task PostOrderAsync(VEOrdersAPI pIVEOrdersAPI, VEOrdersVM[] pOrder)
        {
            await pIVEOrdersAPI.PostSyncDataAsync(pOrder);
        }
        #endregion

        #region Post Order ERP
        private async Task<VEOrdersVM[]> PostOrderERPAsync(VEOrdersAPI pIVEOrdersAPI, VEOrdersVM[] pOrder) =>
            await pIVEOrdersAPI.PostSyncDataERPAsync(pOrder);
        #endregion

        #endregion

        #region Transaction
        #region Upload Transaction
        private async Task<bool> UploadTransactionAsync(GEQueueItem pItem, VETransactionsVM pTransaction)
        {
            try
            {
                using (var context = new HinoContext())
                {
                    var _IVETransactionsAPI = new VETransactionsAPI();
                    Logger.Info("Buscando dados da transação na CRTransactions.");
                    var NewTransaction = context.CRTransactions.Where(r => r.Id == pItem.IdReference).FirstOrDefault();

                    if (NewTransaction?.IdERP > 0)
                    {
                        Logger.Info($"IdERP: {NewTransaction.IdERP}.");

                        var Transaction = await _IVETransactionsAPI.GetByIdAsync(pItem.UniqueKey, pItem.IdReference);
                        Transaction.IdERP = NewTransaction.IdERP;
                        Transaction.StatusSinc = EStatusSinc.Integrated;

                        await PostTransactionAsync(_IVETransactionsAPI, Transaction);
                    }
                }
                pItem.Note = "Dados da API Atualizados com sucesso.";
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível atualizar os dados na API.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;
                return false;
            }
            return true;
        }
        #endregion

        #region Post enterprise
        private async Task PostTransactionAsync(VETransactionsAPI pIVETransactionAPI, VETransactionsVM pTransaction)
        {
            pTransaction.StatusSinc = EStatusSinc.Integrated;
            await pIVETransactionAPI.PostSyncDataAsync(new VETransactionsVM[] { pTransaction });
        }

        /*private async Task<GEEnterprisesVM[]> PostTransactionERPAsync(VETransactionsAPI pIVETransactionsAPI, VETransactionsVM pTransaction)
        {
            pTransaction.StatusSinc = EStatusSinc.Integrated;
            return await pIVETransactionsAPI.PostSyncDataERPAsync(new VETransactionsVM[] { pTransaction });
        }*/
        #endregion

        #region Post Transaction ERP
        private async Task<VETransactionsVM[]> PostTransactionERPAsync(VETransactionsAPI pIVETransactionsAPI, VETransactionsVM[] pOrder) =>
            await pIVETransactionsAPI.PostSyncDataERPAsync(pOrder);
        #endregion

        #endregion

        #region Sale Price
        #region Upload Sale Price
        private async Task<bool> UploadSalePriceAsync(GEQueueItem pItem, VESalePriceVM pSalePrice)
        {
            try
            {
                using (var context = new HinoContext())
                {
                    var _VESalePriceAPI = new VESalePriceAPI();
                    Logger.Info("Buscando dados do grupo da empresa.");
                    var NewProduct = context.CRProducts.Where(r => r.Id == pItem.IdReference).FirstAsync();

                    if (NewProduct != null)
                    {

                        await PostSalePriceAsync(_VESalePriceAPI, new VESalePriceVM[] { pSalePrice });
                    }
                }
                pItem.Note = "Dados da API Atualizados com sucesso.";
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível atualizar os dados na API.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;
                return false;
            }
            return true;
        }
        #endregion

        #region Post Sale Price
        private async Task<VESalePriceVM[]> PostSalePriceAsync(VESalePriceAPI pIVESalePriceAPI, VESalePriceVM[] pSalePrice) =>
            await pIVESalePriceAPI.PostSyncDataAsync(pSalePrice);
        #endregion
        #endregion

        #region Download Data

        #region Download Data by queue item
        private async Task<BaseVM> DownloadByQueueItem(GEQueueItem pItem)
        {
            BaseVM BaseEntity = null;
            switch (pItem.EntryName.ToUpper())
            {
                case Constants.GEEnterprises:
                    if (pItem.Type == "NEW")
                        await DownloadEnterprise(pItem);
                    else
                        await DeleteEnterprise(pItem);
                    break;
                case Constants.GEEnterpriseCategory:
                    if (pItem.Type == "NEW")
                        BaseEntity = await DownloadEnterpriseCategory(pItem);
                    else
                        await DeleteEnterpriseCategory(pItem);
                    break;
                case Constants.GEEnterpriseGroup:
                    if (pItem.Type == "NEW")
                        await DownloadEnterpriseGroup(pItem);
                    else
                        await DeleteEnterpriseGroup(pItem);
                    break;
                case Constants.VEOrders:
                    break;
                default:
                    break;
            }

            pItem.Modified = DateTime.Now;
            pItem.Processed = true;

            try
            {
                Logger.Info("Atualizando item na fila");
                using (var context = new HinoContext())
                {
                    context.Entry(pItem).State = EntityState.Detached;
                    context.Entry(pItem).State = EntityState.Modified;
                    await context.SaveChangesAsync();
                }
            }
            catch (Exception ex)
            {
                Logger.Error("Não foi possível atualizar o item na fila", ex);
            }

            return BaseEntity;
        }
        #endregion

        #region Enterprise
        #region Delete Enterprise
        private async Task DeleteEnterprise(GEQueueItem pItem)
        {
            Logger.Info("Removendo empresa.");
            try
            {
                using (var context = new HinoContext())
                {
                    await context.Database.ExecuteSqlCommandAsync("BEGIN PCKG_INTEGRACAO.DELETEENTERPRISE(:pQUEUEID); END;",
                       new OracleParameter("pQUEUEID", OracleDbType.Int64, pItem.Id, ParameterDirection.Input)
                       );

                    var Item = await context.GEQueueItem.Where(r => r.Id == pItem.Id).FirstAsync();
                    pItem.Note = Item.Note;
                    pItem.Uploaded = true;
                }
                Logger.Info("Empresa removida com sucesso.");
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível remover a empresa.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return;
            }
        }
        #endregion

        #region Download Enterprise
        private async Task<GEEnterprisesVM> DownloadEnterprise(GEQueueItem pItem)
        {
            Logger.Info("Buscando dados da empresa.");
            GEEnterprisesVM Enterprise = null;
            var _IGEEnterprisesAPI = new GEEnterprisesAPI();
            try
            {
                Enterprise = await _IGEEnterprisesAPI.GetByIdAsync(pItem.UniqueKey, pItem.IdReference);
            }
            catch (Exception ex)
            {
                var ExceptionCode = ex.Message.Contains("Recurso não encontrado") ? "not-found" : Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível buscar os dados da empresa.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return Enterprise;
            }

            if (Enterprise == null)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível buscar os dados da empresa.", _IGEEnterprisesAPI.Errors, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return Enterprise;
            }

            Logger.Info("Dados da empresa encontrados.");

            try
            {

                Logger.Info("Salvando na tabela intermediária.");
                CREnterprises NewEnterprise;
                using (var context = new HinoContext())
                {
                    var OldEnterprise = context.CREnterprises.Where(r => r.Id == Enterprise.Id).FirstOrDefault();

                    NewEnterprise = new CREnterprises();
                    if (OldEnterprise != null)
                        NewEnterprise = OldEnterprise;

                    NewEnterprise.Id = Enterprise.Id;
                    NewEnterprise.EstablishmentKey = Enterprise.EstablishmentKey;
                    NewEnterprise.UniqueKey = Enterprise.UniqueKey;
                    NewEnterprise.Created = Enterprise.Created;
                    NewEnterprise.Modified = Enterprise.Modified;
                    NewEnterprise.IsActive = Enterprise.IsActive;
                    NewEnterprise.RazaoSocial = Enterprise.RazaoSocial;
                    NewEnterprise.NomeFantasia = Enterprise.NomeFantasia;
                    NewEnterprise.CNPJCPF = Enterprise.CNPJCPF;
                    NewEnterprise.IE = Enterprise.IE;
                    NewEnterprise.Type = (int)Enterprise.Type;
                    NewEnterprise.Status = (int)Enterprise.Status;
                    NewEnterprise.StatusSinc = (int)Enterprise.StatusSinc;
                    NewEnterprise.IdERP = Enterprise.IdERP;
                    NewEnterprise.RegionId = Enterprise.RegionId;
                    NewEnterprise.Classification = (int)Enterprise.Classification;
                    NewEnterprise.UserId = Enterprise.UserId;
                    NewEnterprise.CreatedById = Enterprise.CreatedById;
                    NewEnterprise.PayConditionId = Enterprise.PayConditionId;
                    NewEnterprise.ClassifEmpresa = Enterprise.ClassifEmpresa;
                    NewEnterprise.CodPrVenda = Enterprise.CodPrVenda;
                    NewEnterprise.QueueId = pItem.Id;
                    NewEnterprise.FiscalGroupId = null;
                    NewEnterprise.CategoryId = null;
                    NewEnterprise.GroupId = null;

                    if (Enterprise.GEEnterpriseFiscalGroup != null && Enterprise.GEEnterpriseFiscalGroup.IdERP > 0)
                        NewEnterprise.FiscalGroupId = Enterprise.GEEnterpriseFiscalGroup.IdERP;

                    if (Enterprise.GEEnterpriseCategory != null && Enterprise.GEEnterpriseCategory.IdERP > 0)
                        NewEnterprise.CategoryId = Enterprise.GEEnterpriseCategory.IdERP;

                    if (Enterprise.GEEnterpriseGroup != null && Enterprise.GEEnterpriseGroup.IdERP > 0)
                        NewEnterprise.GroupId = Enterprise.GEEnterpriseGroup.IdERP;

                    if (OldEnterprise == null)
                    {
                        NewEnterprise.Id = Enterprise.Id;
                        context.CREnterprises.Add(NewEnterprise);
                    }
                    else
                    {
                        // context.CREnterprises.Add(NewEnterprise);
                        context.Entry(NewEnterprise).State = EntityState.Modified;
                    }

                    foreach (var item in Enterprise.GEEnterpriseGeo)
                    {
                        var OldEnterpriseGeo = context.CREnterpriseGeo.Where(r => r.Id == item.Id && r.Enterpriseid == Enterprise.Id).FirstOrDefault();

                        var NewEnterpriseGeo = new CREnterpriseGeo();
                        if (OldEnterpriseGeo != null)
                            NewEnterpriseGeo = OldEnterpriseGeo;

                        NewEnterpriseGeo.Id = item.Id;
                        NewEnterpriseGeo.EstablishmentKey = Enterprise.EstablishmentKey;
                        NewEnterpriseGeo.UniqueKey = item.UniqueKey;
                        NewEnterpriseGeo.Created = item.Created;
                        NewEnterpriseGeo.Modified = item.Modified;
                        NewEnterpriseGeo.IsActive = item.IsActive;
                        NewEnterpriseGeo.Enterpriseid = NewEnterprise.Id;
                        NewEnterpriseGeo.IE = item.IE;
                        NewEnterpriseGeo.Type = (int)item.Type;
                        NewEnterpriseGeo.CNPJCPF = item.CNPJCPF;
                        NewEnterpriseGeo.RG = item.RG;
                        NewEnterpriseGeo.CellPhone = item.CellPhone;
                        NewEnterpriseGeo.Phone = item.Phone;
                        NewEnterpriseGeo.Email = item.Email;
                        NewEnterpriseGeo.EmailNFE = item.EmailNFE;
                        NewEnterpriseGeo.Address = item.Address;
                        NewEnterpriseGeo.Complement = item.Complement;
                        NewEnterpriseGeo.District = item.District;
                        NewEnterpriseGeo.Num = item.Num;
                        NewEnterpriseGeo.ZipCode = item.ZipCode;
                        NewEnterpriseGeo.Site = item.Site;
                        NewEnterpriseGeo.CountryIni = item.CountryIni;
                        NewEnterpriseGeo.CountryCode = item.CountryCode;
                        NewEnterpriseGeo.CountryName = item.CountryName;
                        NewEnterpriseGeo.UF = item.UF;
                        NewEnterpriseGeo.StateName = item.StateName;
                        NewEnterpriseGeo.IBGE = item.IBGE;
                        NewEnterpriseGeo.CityName = item.CityName;
                        NewEnterpriseGeo.DisplayLat = item.DisplayLat;
                        NewEnterpriseGeo.DisplayLng = item.DisplayLng;
                        NewEnterpriseGeo.NavLat = item.NavLat;
                        NewEnterpriseGeo.NavLng = item.NavLng;
                        NewEnterpriseGeo.InscSuframa = item.InscSuframa;

                        Logger.Info("Verificando dados do endereço.");
                        if ((item.CountryIni.IsEmpty() || item.CountryCode.IsEmpty() || item.UF.IsEmpty()
                            || item.CityName.IsEmpty() || item.StateName.IsEmpty()) && !item.ZipCode.IsEmpty())
                        {
                            Logger.Info("Buscando dados do CEP.");
                            var cep = new CEPAPI();
                            var cepModel = await cep.GetCepAsync(NewEnterpriseGeo.ZipCode);
                            if (cepModel != null)
                            {
                                NewEnterpriseGeo.IBGE = cepModel.ibge;
                                NewEnterpriseGeo.CityName = cepModel.localidade;
                                NewEnterpriseGeo.UF = cepModel.uf;
                            }
                        }

                        if (OldEnterpriseGeo == null)
                            context.CREnterpriseGeo.Add(NewEnterpriseGeo);
                        else
                            context.Entry(NewEnterpriseGeo).State = EntityState.Modified;
                    }

                    await context.SaveChangesAsync();
                }

                if (Enterprise.GroupId != null)
                {
                    Logger.Info($"Salvando grupo da empresa.");
                    var QueueItem = new GEQueueItem
                    {
                        Id = 0,
                        EstablishmentKey = NewEnterprise.EstablishmentKey,
                        UniqueKey = Enterprise.GEEnterpriseGroup.UniqueKey,
                        IdReference = Enterprise.GEEnterpriseGroup.Id,
                        EntryName = Constants.GEEnterpriseGroup,
                        Processed = false,
                        Uploaded = false,
                        IsActive = true,
                        Note = "",
                        Type = "NEW",
                        ExceptionCode = "",
                        Created = DateTime.Now,
                        Modified = DateTime.Now
                    };

                    var EnterpriseGroup = await DownloadEnterpriseGroup(QueueItem);
                    Logger.Info("Atualizando API com os dados gerados.");
                    await UploadEnterpriseGroupAsync(pItem, EnterpriseGroup);

                    NewEnterprise.GroupId = EnterpriseGroup.IdERP;
                }

                if (Enterprise.CategoryId != null)
                {
                    Logger.Info($"Salvando categoria da empresa.");
                    var QueueItem = new GEQueueItem
                    {
                        Id = 0,
                        EstablishmentKey = NewEnterprise.EstablishmentKey,
                        UniqueKey = Enterprise.GEEnterpriseCategory.UniqueKey,
                        IdReference = Enterprise.GEEnterpriseCategory.Id,
                        EntryName = Constants.GEEnterpriseCategory,
                        Processed = false,
                        Uploaded = false,
                        IsActive = true,
                        Note = "",
                        Type = "NEW",
                        ExceptionCode = "",
                        Created = DateTime.Now,
                        Modified = DateTime.Now
                    };

                    var EnterpriseCategory = await DownloadEnterpriseCategory(QueueItem);
                    Logger.Info("Atualizando API com os dados gerados.");
                    await UploadEnterpriseCategoryAsync(pItem, EnterpriseCategory);

                    NewEnterprise.CategoryId = EnterpriseCategory.IdERP;
                }

                using (var context = new HinoContext())
                {
                    var contacts = context.CREnterpriseContacts.Where(r => r.EnterpriseId == Enterprise.Id);
                    foreach (var item in contacts)
                        context.CREnterpriseContacts.Remove(item);

                    await context.SaveChangesAsync();
                }

                using (var context = new HinoContext())
                {
                    foreach (var item in  Enterprise.GEEnterpriseContacts)
                    {
                        //var OldEnterpriseContact = context.CREnterpriseContacts.Where(r => r.Id == item.Id && r.EnterpriseId == Enterprise.Id).FirstOrDefault();

                        var NewEnterpriseContact = new CREnterpriseContacts
                        {
                            /*if (OldEnterpriseContact != null)
                                NewEnterpriseContact = OldEnterpriseContact;*/

                            Id = item.Id,
                            EstablishmentKey = Enterprise.EstablishmentKey,
                            UniqueKey = item.UniqueKey,
                            Created = item.Created,
                            Modified = item.Modified,
                            IsActive = item.IsActive,
                            EnterpriseId = NewEnterprise.Id,
                            Email = item.Email,
                            Contact = item.Contact,
                            Ramal = item.Ramal,
                            Phone = item.Phone,
                            Note = item.Note,
                            ReceptivityIndex = item.ReceptivityIndex,
                            Sector = item.Sector
                        };

                        context.CREnterpriseContacts.Add(NewEnterpriseContact);
                    }

                    await context.SaveChangesAsync();
                }

                using (var context = new HinoContext())
                {
                    await context.Database.ExecuteSqlCommandAsync("BEGIN PCKG_INTEGRACAO.INSERTENTERPRISE(:pQUEUEID); END;",
                       new OracleParameter("pQUEUEID", OracleDbType.Int64, pItem.Id, ParameterDirection.Input));

                    var Item = await context.GEQueueItem.Where(r => r.Id == pItem.Id).FirstAsync();
                    pItem.Note = Item.Note;

                    Logger.Info(pItem.Note);
                }

                pItem.ExceptionCode = null;
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível salvar os dados na tabela intermediária.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return Enterprise;
            }
            return Enterprise;
        }
        #endregion
        #endregion

        #region Enterprise Category
        #region Delete Enterprise Category
        private async Task DeleteEnterpriseCategory(GEQueueItem pItem)
        {
            Logger.Info("Removendo empresa.");
            try
            {
                using (var context = new HinoContext())
                {
                    await context.Database.ExecuteSqlCommandAsync("BEGIN PCKG_INTEGRACAO.DELETEENTERPRISE(:pQUEUEID); END;",
                       new OracleParameter("pQUEUEID", OracleDbType.Int64, pItem.Id, ParameterDirection.Input)
                       );

                    var Item = await context.GEQueueItem.Where(r => r.Id == pItem.Id).FirstAsync();
                    pItem.Note = Item.Note;
                    pItem.Uploaded = true;
                }
                Logger.Info("Empresa removida com sucesso.");
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível remover a empresa.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return;
            }
        }
        #endregion

        #region Download Enterprise Category
        private async Task<GEEnterpriseCategoryVM> DownloadEnterpriseCategory(GEQueueItem pItem)
        {
            Logger.Info("Buscando dados da categoria da empresa.");
            GEEnterpriseCategoryVM EnterpriseCategory = null;
            var _GEEnterpriseCategoryAPI = new GEEnterprisesCategoryAPI();
            try
            {
                EnterpriseCategory = await _GEEnterpriseCategoryAPI.GetByIdAsync(pItem.UniqueKey, pItem.IdReference);
            }
            catch (Exception ex)
            {
                var ExceptionCode = ex.Message.Contains("Recurso não encontrado") ? "not-found" : Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível buscar os dados da categoria da empresa.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return EnterpriseCategory;
            }

            if (EnterpriseCategory == null)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível buscar os dados da categoria da empresa.", _GEEnterpriseCategoryAPI.Errors, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return EnterpriseCategory;
            }

            Logger.Info("Dados da categoria da empresa encontrados.");

            try
            {

                Logger.Info("Salvando na tabela intermediária.");

                using (var context = new HinoContext())
                {
                    var OldCategory = context.CRCategoria.Where(r => r.CodCategoria == EnterpriseCategory.IdERP).FirstOrDefault();

                    var NewCategory = new CRCategoria();
                    if (OldCategory != null)
                        NewCategory = OldCategory;

                    NewCategory.Descricao = EnterpriseCategory.Description.ToUpper();
                    NewCategory.Uploaded = true;

                    if (OldCategory == null)
                    {
                        NewCategory.CodCategoria = Convert.ToInt32(context.NextSequence<CRCategoria>()); //EnterpriseCategory.Identifier;

                        context.CRCategoria.Add(NewCategory);
                    }
                    else
                    {
                        // context.CREnterprises.Add(NewEnterprise);
                        context.Entry(NewCategory).State = EntityState.Modified;
                    }

                    EnterpriseCategory.IdERP = NewCategory.CodCategoria;

                    await context.SaveChangesAsync();

                    if (pItem.Id > 0)
                    {
                        var Item = await context.GEQueueItem.Where(r => r.Id == pItem.Id).FirstAsync();
                        pItem.Note = Item.Note;

                        Logger.Info(pItem.Note);
                    }
                }

                pItem.ExceptionCode = null;
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível salvar os dados na tabela intermediária.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return EnterpriseCategory;
            }

            return EnterpriseCategory;
        }
        #endregion
        #endregion

        #region Enterprise Group
        #region Delete Enterprise Group
        private async Task DeleteEnterpriseGroup(GEQueueItem pItem)
        {
            Logger.Info("Removendo empresa.");
            try
            {
                using (var context = new HinoContext())
                {
                    await context.Database.ExecuteSqlCommandAsync("BEGIN PCKG_INTEGRACAO.DELETEENTERPRISE(:pQUEUEID); END;",
                       new OracleParameter("pQUEUEID", OracleDbType.Int64, pItem.Id, ParameterDirection.Input)
                       );

                    var Item = await context.GEQueueItem.Where(r => r.Id == pItem.Id).FirstAsync();
                    pItem.Note = Item.Note;
                    pItem.Uploaded = true;
                }
                Logger.Info("Empresa removida com sucesso.");
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível remover a empresa.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return;
            }
        }
        #endregion

        #region Download Enterprise Group
        private async Task<GEEnterpriseGroupVM> DownloadEnterpriseGroup(GEQueueItem pItem)
        {
            Logger.Info("Buscando dados do grupo de empresa.");
            GEEnterpriseGroupVM EnterpriseGroup = null;
            var _GEEnterpriseGroupAPI = new GEEnterpriseGroupAPI();
            try
            {
                EnterpriseGroup = await _GEEnterpriseGroupAPI.GetByIdAsync(pItem.UniqueKey, pItem.IdReference);
            }
            catch (Exception ex)
            {
                var ExceptionCode = ex.Message.Contains("Recurso não encontrado") ? "not-found" : Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível buscar os dados do grupo de empresa.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return EnterpriseGroup;
            }

            if (EnterpriseGroup == null)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível buscar os dados do grupo de empresa.", _GEEnterpriseGroupAPI.Errors, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return EnterpriseGroup;
            }

            Logger.Info("Dados da categoria do grupo de empresa encontrados.");

            try
            {

                Logger.Info("Salvando na tabela intermediária.");

                using (var context = new HinoContext())
                {
                    var OldGroup = context.CRGrupo.Where(r => r.CodGrupo == EnterpriseGroup.IdERP).FirstOrDefault();

                    var NewGroup = new CRGrupo();
                    if (OldGroup != null)
                        NewGroup = OldGroup;

                    NewGroup.Descricao = EnterpriseGroup.Description.ToUpper();
                    NewGroup.Uploaded = true;

                    if (OldGroup == null)
                    {
                        NewGroup.CodGrupo = Convert.ToInt32(context.NextSequence<CRGrupo>());

                        context.CRGrupo.Add(NewGroup);
                    }
                    else
                    {
                        // context.CREnterprises.Add(NewEnterprise);
                        context.Entry(NewGroup).State = EntityState.Modified;
                    }
                    EnterpriseGroup.IdERP = NewGroup.CodGrupo;

                    await context.SaveChangesAsync();
                    if (pItem.Id > 0)
                    {
                        var Item = await context.GEQueueItem.Where(r => r.Id == pItem.Id).FirstAsync();
                        pItem.Note = Item.Note;

                        Logger.Info(pItem.Note);
                    }
                }

                pItem.ExceptionCode = null;
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível salvar os dados na tabela intermediária.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return EnterpriseGroup;
            }

            return EnterpriseGroup;
        }
        #endregion
        #endregion

        #region Product
        #region Delete Product
        private async Task DeleteProductAsync(GEQueueItem pItem)
        {
            Logger.Info("Removendo empresa.");
            try
            {
                using (var context = new HinoContext())
                {
                    await context.Database.ExecuteSqlCommandAsync("BEGIN PCKG_INTEGRACAO.DELETEENTERPRISE(:pQUEUEID); END;",
                       new OracleParameter("pQUEUEID", OracleDbType.Int64, pItem.Id, ParameterDirection.Input)
                       );

                    var Item = await context.GEQueueItem.Where(r => r.Id == pItem.Id).FirstAsync();
                    pItem.Note = Item.Note;
                    pItem.Uploaded = true;
                }
                Logger.Info("Empresa removida com sucesso.");
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível remover a empresa.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return;
            }
        }
        #endregion

        #region Download Product
        private async Task<GEProductsVM> DownloadProductAsync(GEQueueItem pItem)
        {
            Logger.Info("Buscando dados do produto.");
            GEProductsVM Product = null;
            var _GEProductsAPI = new GEProductsAPI();
            try
            {
                Product = await _GEProductsAPI.GetByIdAsync(pItem.UniqueKey, pItem.IdReference);
            }
            catch (Exception ex)
            {
                var ExceptionCode = ex.Message.Contains("Recurso não encontrado") ? "not-found" : Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível buscar os dados do produto.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return Product;
            }

            if (Product == null)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível buscar os dados do produto.", _GEProductsAPI.Errors, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return Product;
            }

            Logger.Info("Dados da categoria do grupo de empresa encontrados.");

            try
            {
                Logger.Info("Salvando na tabela intermediária.");

                using (var context = new HinoContext())
                {
                    var OldProduct = context.CRProducts.Where(r => r.Id == Product.Id).FirstOrDefault();

                    var NewProduct = new CRProducts();
                    if (OldProduct != null)
                        NewProduct = OldProduct;


                    NewProduct.Created = Product.Created;
                    NewProduct.Modified = Product.Modified;
                    NewProduct.IsActive = Product.IsActive;
                    NewProduct.Id = Product.Id;
                    NewProduct.UniqueKey = Product.UniqueKey;
                    NewProduct.EstablishmentKey = Globals.EstablishmentKey;
                    NewProduct.ProductKey = Product.ProductKey;
                    NewProduct.Image = Product.Image;
                    NewProduct.Name = Product.Name;
                    NewProduct.Description = Product.Description;
                    NewProduct.TypeId = Product.TypeId;
                    if (Product.GEProductsType != null)
                        NewProduct.TypeProd = Product.GEProductsType.Description.Split('-')[0].Trim();
                    NewProduct.FamilyId = Product.FamilyId;
                    if (Product.GEProductsFamily != null)
                        NewProduct.FamilyProd = Product.GEProductsFamily.Description.Split('-')[0].Trim();
                    NewProduct.PercIPI = Product.PercIPI;
                    NewProduct.PercMaxDiscount = Product.PercMaxDiscount;
                    NewProduct.Value = Product.Value;
                    NewProduct.StockBalance = Product.StockBalance;
                    NewProduct.Status = Product.Status;
                    NewProduct.NCM = Product.NCM;
                    NewProduct.NCMId = Product.NCMId;
                    NewProduct.Weight = Product.Weight;
                    NewProduct.PackageQTD = Product.PackageQTD;
                    NewProduct.SaleUnitId = Product.SaleUnitId;
                    NewProduct.SaleFactor = Product.SaleFactor;
                    NewProduct.CodOrigMerc = Product.CodOrigMerc;
                    NewProduct.QueueId = pItem.Id;
                    NewProduct.UnitId = Product.UnitId;

                    if (Product.GEProductAplic != null)
                        NewProduct.AplicationId = Product.GEProductAplic.IdERP;

                    if (Product.GEProductsUnit != null)
                        NewProduct.Unit = Product.GEProductsUnit.Unit;

                    if (Product.GEProductsSaleUnit != null)
                        NewProduct.SaleUnit = Product.GEProductsSaleUnit.Unit;

                    if (OldProduct == null)
                    {
                        NewProduct.Id = Product.Id;
                        context.CRProducts.Add(NewProduct);
                    }
                    else
                        context.Entry(NewProduct).State = EntityState.Modified;

                    await context.SaveChangesAsync();

                    await context.Database.ExecuteSqlCommandAsync("BEGIN PCKG_INTEGRACAO.INSERTPRODUCT(:pQUEUEID); END;",
                       new OracleParameter("pQUEUEID", OracleDbType.Int64, pItem.Id, ParameterDirection.Input));

                    var Item = await context.GEQueueItem.Where(r => r.Id == pItem.Id).FirstAsync();
                    pItem.Note = Item.Note;

                    Logger.Info(pItem.Note);
                }

                pItem.ExceptionCode = null;
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível salvar os dados na tabela intermediária.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return Product;
            }

            return Product;
        }
        #endregion

        #endregion

        #region Orders
        #region Delete Order
        private async Task DeleteOrderAsync(GEQueueItem pItem)
        {
            Logger.Info("Removendo pedido.");
            try
            {
                using (var context = new HinoContext())
                {
                    await context.Database.ExecuteSqlCommandAsync("BEGIN PCKG_INTEGRACAO.DELETEENTERPRISE(:pQUEUEID); END;",
                       new OracleParameter("pQUEUEID", OracleDbType.Int64, pItem.Id, ParameterDirection.Input)
                       );

                    var Item = await context.GEQueueItem.Where(r => r.Id == pItem.Id).FirstAsync();
                    pItem.Note = Item.Note;
                    pItem.Uploaded = true;
                }
                Logger.Info("Pedido removido com sucesso.");
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível remover o pedido.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return;
            }
        }
        #endregion

        #region Download Order
        private async Task<VEOrdersVM> DownloadOrderAsync(GEQueueItem pItem)
        {
            Logger.Info("Buscando dados do pedido.");
            VEOrdersVM Order = null;
            var _VEOrdersAPI = new VEOrdersAPI();
            try
            {
                Order = await _VEOrdersAPI.GetByIdAsync(pItem.UniqueKey, pItem.IdReference);
            }
            catch (Exception ex)
            {
                var ExceptionCode = ex.Message.Contains("Recurso não encontrado") ? "not-found" : Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível buscar os dados do pedido.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return Order;
            }

            if (Order == null)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível buscar os dados do pedido.", _VEOrdersAPI.Errors, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return Order;
            }

            Logger.Info("Dados do pedido encontrados.");

            try
            {
                Logger.Info("Salvando na tabela intermediária.");

                using (var context = new HinoContext())
                {
                    var OldOrder = context.CROrders.Where(r => r.Id == Order.Id).FirstOrDefault();

                    var NewOrder = new CROrders();
                    if (OldOrder != null)
                        NewOrder = OldOrder;

                    NewOrder.QueueId = pItem.Id;
                    NewOrder.Id = Order.Id;
                    NewOrder.EstablishmentKey = Order.EstablishmentKey;
                    NewOrder.UniqueKey = Order.UniqueKey;
                    NewOrder.Created = Order.Created;
                    NewOrder.Modified = Order.Modified;
                    NewOrder.IsActive = Order.IsActive;
                    NewOrder.CodPedVenda = Order.CodPedVenda;
                    NewOrder.EnterpriseID = Order.EnterpriseID;
                    NewOrder.CarrierID = Order.CarrierID;
                    NewOrder.RedispatchID = Order.RedispatchID;
                    NewOrder.UserID = Order.UserID;
                    NewOrder.TypePaymentID = Order.TypePaymentID;
                    NewOrder.PayConditionID = Order.PayConditionID;
                    NewOrder.CodPedVenda = Order.CodPedVenda;
                    NewOrder.NumPedMob = Order.NumPedMob;
                    NewOrder.DeliveryDate = Order.DeliveryDate;
                    NewOrder.Note = Order.Note;
                    NewOrder.InnerNote = Order.InnerNote;
                    NewOrder.Status = Order.Status;
                    NewOrder.StatusCRM = Order.StatusCRM;
                    NewOrder.StatusSinc = Order.StatusSinc;
                    NewOrder.IsProposal = Order.IsProposal;
                    NewOrder.FreightPaidBy = Order.FreightPaidBy;
                    NewOrder.RedispatchPaidBy = Order.RedispatchPaidBy;
                    NewOrder.FreightValue = Order.FreightValue;
                    NewOrder.ClientOrder = Order.ClientOrder;
                    if (NewOrder.IdERP <= 0)
                        NewOrder.IdERP = Order.IdERP;
                    NewOrder.OriginOrderID = Order.OriginOrderID;
                    NewOrder.MainOrderID = Order.MainOrderID;
                    NewOrder.OrderVersion = Order.OrderVersion;
                    NewOrder.PercDiscount = Order.PercDiscount;
                    NewOrder.PercCommission = Order.PercCommission;

                    NewOrder.ContactPhone = Order.ContactPhone;
                    NewOrder.ContactEmail = Order.ContactEmail;
                    NewOrder.Contact = Order.Contact;
                    NewOrder.Sector = Order.Sector;
                    NewOrder.DigitizerID = Order.DigitizerID;
                    NewOrder.FinancialTaxes = Order.FinancialTaxes;
                    NewOrder.OnlyOnDate = Order.OnlyOnDate;
                    NewOrder.AllowPartial = Order.AllowPartial;
                    NewOrder.RevisionReason = Order.RevisionReason;
                    NewOrder.InPerson = Order.InPerson;
                    NewOrder.PaymentDueDate = Order.PaymentDueDate;
                    NewOrder.StatusPay = Order.StatusPay;
                    NewOrder.PaidAmount = Order.PaidAmount;
                    NewOrder.AdvanceAmount = Order.AdvanceAmount;

                    if (OldOrder == null)
                    {
                        NewOrder.Id = Order.Id;
                        context.CROrders.Add(NewOrder);
                    }
                    else
                    {
                        context.Entry(NewOrder).State = EntityState.Modified;
                    }

                    var itemsFromOrder = context.CROrderItems.Where(r => r.OrderID == Order.Id);

                    // Itens que estão na tabela de integração, mas foram removidos do pedido "real"
                    foreach (var item in itemsFromOrder)
                    {
                        if (!Order.VEOrderItems.Any(r => r.Id == item.Id))
                            context.CROrderItems.Remove(item);
                    }

                    foreach (var item in Order.VEOrderItems)
                    {
                        var OldOrderItems = context.CROrderItems.Where(r => r.Id == item.Id && r.OrderID == Order.Id).FirstOrDefault();

                        var NewOrderItems = new CROrderItems();
                        if (OldOrderItems != null)
                            NewOrderItems = OldOrderItems;

                        NewOrderItems.Id = item.Id;
                        NewOrderItems.EstablishmentKey = Order.EstablishmentKey;
                        NewOrderItems.UniqueKey = item.UniqueKey;
                        NewOrderItems.Created = item.Created;
                        NewOrderItems.Modified = item.Modified;
                        NewOrderItems.IsActive = item.IsActive;

                        NewOrderItems.OrderID = item.OrderID;
                        NewOrderItems.ProductID = item.ProductID;
                        NewOrderItems.FiscalOperID = item.FSFiscalOper.IdERP;
                        NewOrderItems.TableValue = item.TableValue;
                        NewOrderItems.Value = item.Value;
                        NewOrderItems.Quantity = item.Quantity;
                        NewOrderItems.QuantityReference = item.QuantityReference;
                        NewOrderItems.PercDiscount = item.PercDiscount;
                        NewOrderItems.Note = item.Note;
                        NewOrderItems.Item = item.Item;
                        NewOrderItems.ItemLevel = item.ItemLevel;
                        NewOrderItems.IdERP = item.IdERP;
                        NewOrderItems.ClientOrder = item.ClientOrder;
                        NewOrderItems.ClientItem = item.ClientItem;
                        NewOrderItems.DeliveryDate = item.DeliveryDate;
                        NewOrderItems.PercDiscountHead = item.PercDiscountHead;
                        NewOrderItems.PercCommission = item.PercCommission;
                        NewOrderItems.PercCommissionHead = item.PercCommissionHead;
                        NewOrderItems.ShippingDays = item.ShippingDays;
                        NewOrderItems.AltDescription = item.AltDescription;

                        if (OldOrderItems == null)
                            context.CROrderItems.Add(NewOrderItems);
                        else
                            context.Entry(NewOrderItems).State = EntityState.Modified;
                    }

                    await context.SaveChangesAsync();
                }

                if (Order.GEEnterprises != null)
                {
                    using (var context = new HinoContext())
                    {
                        var EnterpriseService = new EnterpriseService();
                        var exists = await EnterpriseService.EnterpriseExists(context, Order.GEEnterprises.Id);

                        if (!exists)
                        {
                            var itemEnterprises = new GEQueueItem
                            {
                                Id = context.NextSequence<GEQueueItem>(),
                                IdReference = Order.EnterpriseID,
                                UniqueKey = Order.GEEnterprises.UniqueKey,
                                EntryName = "GEEnterprises",
                                Type = "NEW",
                                EstablishmentKey = Order.EstablishmentKey,
                                IsActive = true,
                                Created = DateTime.Now,
                                Uploaded = false,
                                Processed = false,
                                Modified = DateTime.Now,
                                Note = "",
                                ExceptionCode = ""
                            };
                            context.GEQueueItem.Add(itemEnterprises);

                            await context.SaveChangesAsync();
                            await DownloadAndUploadData(itemEnterprises);
                        }
                    }
                }

                using (var context = new HinoContext())
                {
                    var CrOrder = context.CROrders.Where(r => r.Id == Order.Id).FirstOrDefault();
                    var OrderService = new OrdersService();
                    var orderGenerated = await OrderService.GenerateOrderCabecAsync(context, CrOrder);
                    CROrders orderItemGenerated;
                    var msErro = orderGenerated.MsErro;
                    CrOrder.CodPedVenda = orderGenerated.CodPedVenda;

                    if (msErro.IsEmpty())
                        orderItemGenerated = await OrderService.GenerateOrderItemsAsync(context, CrOrder);

                    msErro = CrOrder.MsErro;

                    if (orderGenerated.CodPedVenda > 0 && msErro.IsEmpty())
                    {
                        CrOrder.IdERP = orderGenerated.CodPedVenda;
                        await OrderService.GenerateTaxValuesAsync(context, CrOrder.IdERP);
                        await OrderService.TotalizeOrderValuesAsync(context, CrOrder.IdERP);
                        await OrderService.GenerateOrderCommissionAsync(context, CrOrder.IdERP);
                    }

                    context.Entry(CrOrder).State = EntityState.Modified;
                    await context.SaveChangesAsync();

                    if (!msErro.IsEmpty())
                        throw new Exception(msErro.SubStr(0, 255));

                    pItem.Note = $"Pedido {CrOrder.CodPedVenda} gerado";

                    Logger.Info(pItem.Note);
                }

                pItem.ExceptionCode = null;
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível salvar os dados na tabela intermediária.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return Order;
            }

            return Order;
        }
        #endregion

        #endregion

        #region Transaction
        #region Delete Transaction
        private async Task DeleteTransaction(GEQueueItem pItem)
        {
            Logger.Info("Removendo transação.");
            try
            {
                using (var context = new HinoContext())
                {
                    await context.Database.ExecuteSqlCommandAsync("BEGIN PCKG_INTEGRACAO.DELETETRANSACTION(:pQUEUEID); END;",
                       new OracleParameter("pQUEUEID", OracleDbType.Int64, pItem.Id, ParameterDirection.Input)
                       );

                    var Item = await context.GEQueueItem.Where(r => r.Id == pItem.Id).FirstAsync();
                    pItem.Note = Item.Note;
                    pItem.Uploaded = true;
                }
                Logger.Info("Transação removida com sucesso.");
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível remover a transação.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return;
            }
        }
        #endregion

        #region Download Transaction
        private async Task<VETransactionsVM> DownloadTransaction(GEQueueItem pItem)
        {
            Logger.Info("Buscando dados da transação.");
            VETransactionsVM Transaction = null;
            var _IVEtransactionAPI = new VETransactionsAPI();
            try
            {
                Transaction = await _IVEtransactionAPI.GetByIdAsync(pItem.UniqueKey, pItem.IdReference);
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível buscar os dados da transação.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return Transaction;
            }

            if (Transaction == null)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível buscar os dados da transação.", _IVEtransactionAPI.Errors, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return Transaction;
            }

            Logger.Info("Dados da transação encontrados.");

            try
            {

                Logger.Info("Salvando na tabela intermediária.");

                using (var context = new HinoContext())
                {
                    var OldTransaction = context.CRTransactions.Where(r => r.Id == Transaction.Id).FirstOrDefault();

                    var NewTransaction = new CRTransactions();
                    if (OldTransaction != null)
                        NewTransaction = OldTransaction;

                    NewTransaction.Id = Transaction.Id;
                    NewTransaction.EstablishmentKey = Transaction.EstablishmentKey;
                    NewTransaction.UniqueKey = Transaction.UniqueKey;
                    NewTransaction.Created = Transaction.Created;
                    NewTransaction.Modified = Transaction.Modified;
                    NewTransaction.IsActive = Transaction.IsActive;
                    NewTransaction.Type = Transaction.Type;
                    NewTransaction.OrderID = Transaction.OrderID;
                    NewTransaction.AuthCode = Transaction.AuthCode;
                    NewTransaction.AuthDate = Transaction.AuthDate;
                    NewTransaction.AcquirerName = Transaction.AcquirerName;
                    NewTransaction.NSU = Transaction.NSU;
                    NewTransaction.NSUHost = Transaction.NSUHost;
                    NewTransaction.AdminCode = Transaction.AdminCode;
                    NewTransaction.CardBrand = Transaction.CardBrand;
                    NewTransaction.CardLastDigits = Transaction.CardLastDigits;
                    NewTransaction.Installments = Transaction.Installments;
                    NewTransaction.Amount = Transaction.Amount;
                    NewTransaction.Description = Transaction.Description;
                    NewTransaction.Status = Transaction.Status;
                    NewTransaction.MerchantReceipt = Transaction.MerchantReceipt;
                    NewTransaction.CustomerReceipt = Transaction.CustomerReceipt;
                    NewTransaction.Reversed = Transaction.Reversed;
                    NewTransaction.StoreId = Transaction.StoreId;
                    NewTransaction.TerminalId = Transaction.StoreId;
                    NewTransaction.Operator = Transaction.Operator;
                    NewTransaction.Origin = Transaction.Origin;
                    NewTransaction.QueueId = pItem.Id;

                    if (OldTransaction == null)
                    {
                        NewTransaction.Id = Transaction.Id;
                        context.CRTransactions.Add(NewTransaction);
                    }
                    else
                    {
                        context.Entry(NewTransaction).State = EntityState.Modified;
                    }

                    await context.SaveChangesAsync();

                    await context.Database.ExecuteSqlCommandAsync("BEGIN PCKG_INTEGRACAO.INSERTTRANSACTION(:pQUEUEID); END;",
                       new OracleParameter("pQUEUEID", OracleDbType.Int64, pItem.Id, ParameterDirection.Input));

                    var Item = await context.GEQueueItem.Where(r => r.Id == pItem.Id).FirstAsync();
                    pItem.Note = Item.Note;

                    Logger.Info(pItem.Note);
                }

                pItem.ExceptionCode = null;
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível salvar os dados na tabela intermediária.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return Transaction;
            }
            return Transaction;
        }
        #endregion
        #endregion

        #region Tabela de preço
        #region Delete Tabela de preço
        private async Task DeleteSalePrice(GEQueueItem pItem)
        {
            Logger.Info("Removendo preço de venda.");
            try
            {
                using (var context = new HinoContext())
                {
                    await context.Database.ExecuteSqlCommandAsync("BEGIN PCKG_INTEGRACAO.DELETETRANSACTION(:pQUEUEID); END;",
                       new OracleParameter("pQUEUEID", OracleDbType.Int64, pItem.Id, ParameterDirection.Input)
                       );

                    var Item = await context.GEQueueItem.Where(r => r.Id == pItem.Id).FirstAsync();
                    pItem.Note = Item.Note;
                    pItem.Uploaded = true;
                }
                Logger.Info("Preço de venda removida com sucesso.");
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível remover o preço de venda.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return;
            }
        }
        #endregion

        #region Download Enterprise
        private async Task<VESalePriceVM> DownloadSalePrice(GEQueueItem pItem)
        {
            Logger.Info("Buscando dados da tabela de preço.");
            VESalePriceVM SalePrice = null;
            var _IVESalePriceAPI = new VESalePriceAPI();
            try
            {
                SalePrice = await _IVESalePriceAPI.GetByIdAsync(pItem.UniqueKey, pItem.IdReference);
            }
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível buscar os dados da tabela de preço.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return SalePrice;
            }

            if (SalePrice == null)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível buscar os dados da tabela de preço.", _IVESalePriceAPI.Errors, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return SalePrice;
            }

            Logger.Info("Dados da tabela de preço encontrados.");

            try
            {

                Logger.Info("Salvando na tabela intermediária.");

                using (var context = new HinoContext())
                {
                    var OldSalePrice = context.CRSalePrice.Where(r => r.Id == SalePrice.Id).FirstOrDefault();

                    var NewSalePrice = new CRSalePrice();
                    if (OldSalePrice != null)
                        NewSalePrice = OldSalePrice;

                    NewSalePrice.Id = SalePrice.Id;
                    NewSalePrice.EstablishmentKey = SalePrice.EstablishmentKey;
                    NewSalePrice.UniqueKey = SalePrice.UniqueKey;
                    NewSalePrice.Created = SalePrice.Created;
                    NewSalePrice.Modified = SalePrice.Modified;
                    NewSalePrice.IsActive = SalePrice.IsActive;
                    NewSalePrice.CodPrVenda = SalePrice.CodPrVenda;
                    NewSalePrice.Description = SalePrice.Description;
                    NewSalePrice.ProductId = SalePrice.ProductId;
                    NewSalePrice.RegionId = SalePrice.RegionId;
                    NewSalePrice.Value = SalePrice.Value;

                    NewSalePrice.QueueId = pItem.Id;

                    if (OldSalePrice == null)
                    {
                        NewSalePrice.Id = SalePrice.Id;
                        context.CRSalePrice.Add(NewSalePrice);
                    }
                    else
                    {
                        context.Entry(NewSalePrice).State = EntityState.Modified;
                    }

                    await context.SaveChangesAsync();

                    await context.Database.ExecuteSqlCommandAsync("BEGIN PCKG_INTEGRACAO.INSERTSALEPRICE(:pQUEUEID); END;",
                       new OracleParameter("pQUEUEID", OracleDbType.Int64, pItem.Id, ParameterDirection.Input));

                    var Item = await context.GEQueueItem.Where(r => r.Id == pItem.Id).FirstAsync();
                    pItem.Note = Item.Note;

                    Logger.Info(pItem.Note);
                }

                pItem.ExceptionCode = null;
            }   
            catch (Exception ex)
            {
                var ExceptionCode = Guid.NewGuid().ToString();

                var ErrorLog = Logger.Error("Não foi possível salvar os dados na tabela intermediária.", ex, ExceptionCode);

                pItem.Note = ErrorLog.SubStr(0, 255);
                pItem.ExceptionCode = ExceptionCode;

                return SalePrice;
            }
            return SalePrice;
        }
        #endregion
        #endregion

        #endregion
    }
}
