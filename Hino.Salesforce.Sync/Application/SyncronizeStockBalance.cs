﻿using Hino.Salesforce.Application.ViewModels.Auth;
using Hino.Salesforce.Application.ViewModels.General;
using Hino.Salesforce.Sync.API.General;
using Hino.Salesforce.Sync.Application.Auth;
using Hino.Salesforce.Sync.Data;
using Hino.Salesforce.Sync.Models.General;
using Hino.Salesforce.Sync.Models.Views;
using Hino.Salesforce.Sync.Utils;
using Oracle.ManagedDataAccess.Client;
using System;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Application
{
    public class SyncronizeStockBalance
    {
        public bool Synchonizing { get; private set; }

        #region Authenticate User
        private async Task<bool> AuthenticateUser()
        {
            var _AuthenticationService = new AuthenticationService();
            return await _AuthenticationService.AuthenticateAsync();
        }
        #endregion

        #region Upload Stock Balance
        public async Task UploadStockBalance()
        {
            Logger.Info("Processando dados do saldo de estoque");
            GESincConfig[] ListConfig;
            GEEstab[] ListEstab;
            GEVWSincEstoqueSaldo[] Saldos;
            using (var context = new HinoContext())
            {
                ListEstab = context.GEEstab.ToArray();
                ListConfig = context.GESincConfig.ToArray();
            }

            Synchonizing = true;
            foreach (var config in ListConfig)
            {
                var Estab = ListEstab.Where(r => r.codestab == config.codestab).FirstOrDefault();

                Logger.Info($"Percorrendo: {Estab.codestab} - {Estab.razaosocial}");
                Globals.EstablishmentKey = config.establishmentkey;
                Globals.CodEstab = Estab.codestab;
                using (var context = new HinoContext())
                    Saldos = context.GEVWSincEstoqueSaldo.AsNoTracking().Where(r => r.EstablishmentKey == Globals.EstablishmentKey &&
                        r.CodEstab == Globals.CodEstab).ToArray();

                if (Saldos.Length > 0)
                {
                    Logger.Info($"Itens de saldo para sincronizar { Saldos.Length }.");

                    if (!await AuthenticateUser())
                    {
                        Synchonizing = false;
                        return;
                    }

                    var Pages = Math.Ceiling((double)(Saldos.Length / 100)) + 1;

                    Logger.Info($"Paginas para sincronizar {Pages}.");

                    for (int i = 0; i < Pages; i++)
                    {
                        try
                        {
                            Logger.Info($"Sincronizando pagina: {i + 1}/{Pages}.");
                            var Items = Saldos.Skip(i * 100).Take(100);

                            var GEProducts = Items.Select(r => new GEProductsVM
                            {
                                ProductKey = r.CodProduto,
                                StockBalance = r.SaldoAtual,
                                EstablishmentKey = Globals.EstablishmentKey,
                                UniqueKey = r.UniqueKey,
                                Id = r.IDApi,
                                IsActive = true
                            });

                            Logger.Info($"Enviando dados do estoque para API.");
                            var _GEProductsAPI = new GEProductsAPI();
                            await PostProductStockBalanceAsync(_GEProductsAPI, GEProducts.ToArray());
                        }
                        catch (Exception e)
                        {
                            Logger.Error("Problema ao sincronizar saldo de estoque", e);
                        }
                    }
                    if (Saldos.Length > 0)
                        await SyncProductsStockBalanceUpdateDB(Estab.codestab);
                }
                else
                    Logger.Info("Não há itens de saldo de estoque para sincronizar");
            }
            Synchonizing = false;
        }
        #endregion

        #region Post Order
        private async Task PostProductStockBalanceAsync(GEProductsAPI pIGEProductsAPI, GEProductsVM[] pProducts)
        {
            await pIGEProductsAPI.PostSyncStockBalanceAsync(pProducts);
        }
        #endregion

        #region Atualiza saldo de estoque sincronizado
        public async Task SyncProductsStockBalanceUpdateDB(int pCodEstab)
        {
            try
            {
                Logger.Info($"Salvando o saldo de estoque.");
                using (var context = new HinoContext())
                    await context.Database.ExecuteSqlCommandAsync("BEGIN PCKG_INTEGRACAO.UPDATEESTOQUESINC(:pQUEUEID); END;",
                           new OracleParameter("pCODESTAB", OracleDbType.Int32, pCodEstab, ParameterDirection.Input));
                Logger.Info($"Saldo de estoque salvo com sucesso.");
            }
            catch (Exception e)
            {
                Logger.Info($"Problema ao salvar o saldo de estoque");
                Logger.Info($"Motivo: {e.Message}");
                Logger.Error("Motivo:", e);
            }
        }
        #endregion

    }
}
