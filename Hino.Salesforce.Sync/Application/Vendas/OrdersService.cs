﻿using Hino.Salesforce.Application.ViewModels.Sales;
using Hino.Salesforce.Sync.Data;
using Hino.Salesforce.Sync.Models.Vendas;
using Hino.Salesforce.Sync.Models.Views;
using Hino.Salesforce.Sync.Utils;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Application.Vendas
{
    public class OrdersService
    {

        public async Task UpdateUniqueKeyIdAPI(HinoContext pContext, VEOrdersVM pOrder, long pCodPedVenda)
        {
            using (var cmd = pContext.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"UPDATE VEPEDVENDA
                                       SET UNIQUEKEY   = :pUNIQUEKEY
                                           IDAPI       = :pIDAPI 
                                     WHERE CODPEDVENDA = :pCODPEDVENDA 
                                       AND CODESTAB    = :pCODESTAB";

                var uniqueKeyParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, pOrder.UniqueKey, ParameterDirection.Input);
                var idAPIParam = new OracleParameter("pIDAPI", OracleDbType.Int64, pOrder.Id, ParameterDirection.Input);
                var codPedVendaParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pCodPedVenda, ParameterDirection.Input);
                var codEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, Globals.CodEstab, ParameterDirection.Input);

                cmd.Parameters.AddRange(new[] {
                    uniqueKeyParam,
                    idAPIParam,
                    codPedVendaParam,
                    codEstabParam
                });
                cmd.Connection.Open();
                var result = await cmd.ExecuteNonQueryAsync();
                cmd.Connection.Close();
            }

            foreach (var item in pOrder.VEOrderItems)
            {
                using (var cmd = pContext.Database.Connection.CreateCommand())
                {
                    cmd.CommandType = CommandType.Text;
                    cmd.CommandText = @"UPDATE VEPEDVENDA
                                       SET UNIQUEKEY   = :pUNIQUEKEY
                                           IDAPI       = :pIDAPI 
                                     WHERE CODPEDVENDA = :pCODPEDVENDA 
                                       AND CODESTAB    = :pCODESTAB";

                    var uniqueKeyParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, pOrder.UniqueKey, ParameterDirection.Input);
                    var idAPIParam = new OracleParameter("pIDAPI", OracleDbType.Int64, pOrder.Id, ParameterDirection.Input);
                    var codPedVendaParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pCodPedVenda, ParameterDirection.Input);
                    var codEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, Globals.CodEstab, ParameterDirection.Input);

                    cmd.Parameters.AddRange(new[] {
                        uniqueKeyParam,
                        idAPIParam,
                        codPedVendaParam,
                        codEstabParam
                    });
                    cmd.Connection.Open();
                    var result = await cmd.ExecuteNonQueryAsync();
                    cmd.Connection.Close();
                }
            }
        }

        public async Task<CROrders> GenerateOrderCabecAsync(HinoContext pContext, CROrders pOrder)
        {
            var codPedVenda = 0L;
            var msErro = "";

            using (var cmd = pContext.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_INTEGRACAO.INTEGRACABECPEDIDO";

                var returnParam = new OracleParameter("nRETURN", OracleDbType.Int64, ParameterDirection.ReturnValue);
                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, Globals.CodEstab, ParameterDirection.Input);
                var queueIdParam = new OracleParameter("pQUEUEID", OracleDbType.Int64, pOrder.Id, ParameterDirection.Input);
                var codPedVendaOutParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, ParameterDirection.Output);
                var msErroOutParam = new OracleParameter("pMSERRO", OracleDbType.Clob, ParameterDirection.Output);

                cmd.Parameters.AddRange(new[] {
                    returnParam,
                    codeEstabParam,
                    queueIdParam,
                    codPedVendaOutParam,
                    msErroOutParam
                });
                cmd.Connection.Open();
                var result = await cmd.ExecuteNonQueryAsync();

                codPedVenda = Convert.ToInt64((((OracleDecimal)(codPedVendaOutParam.Value)).Value));
                msErro = (((OracleClob)(msErroOutParam.Value)).Value).ToString();
                msErro = msErro == "*OK*" ? "" : msErro;
                cmd.Connection.Close();
            }

            pOrder.CodPedVenda = codPedVenda;
            pOrder.MsErro = msErro;

            return pOrder;
        }

        public async Task<CROrders> GenerateOrderItemsAsync(HinoContext pContext, CROrders pOrder)
        {
            var msErro = "";

            using (var cmd = pContext.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_INTEGRACAO.INTEGRAITEMPEDIDO";

                var returnParam = new OracleParameter("nRETURN", OracleDbType.Int64, ParameterDirection.ReturnValue);
                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, Globals.CodEstab, ParameterDirection.Input);
                var queueIdParam = new OracleParameter("pQUEUEID", OracleDbType.Int64, pOrder.Id, ParameterDirection.Input);
                var codPedVendaParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pOrder.CodPedVenda, ParameterDirection.Input);
                var msErroOutParam = new OracleParameter("pMSERRO", OracleDbType.Clob, ParameterDirection.Output);

                cmd.Parameters.AddRange(new[] {
                    returnParam,
                    codeEstabParam,
                    queueIdParam,
                    codPedVendaParam,
                    msErroOutParam
                });
                cmd.Connection.Open();
                var result = await cmd.ExecuteNonQueryAsync();

                msErro = (((OracleClob)(msErroOutParam.Value)).Value).ToString();
                msErro = msErro == "*OK*" ? "" : msErro;
                cmd.Connection.Close();
            }

            pOrder.MsErro = msErro;

            return pOrder;
        }

        public async Task GenerateTaxValuesAsync(HinoContext pContext, long pCodPedVenda)
        {
            using (var cmd = pContext.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_INTEGRACAO.GERARIMPOSTOSPEDIDO";

                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, Globals.CodEstab, ParameterDirection.Input);
                var queueIdParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pCodPedVenda, ParameterDirection.Input);

                cmd.Parameters.AddRange(new[] {
                    codeEstabParam,
                    queueIdParam
                });
                cmd.Connection.Open();
                await cmd.ExecuteNonQueryAsync();
                cmd.Connection.Close();
            }
        }

        public async Task TotalizeOrderValuesAsync(HinoContext pContext, long pCodPedVenda)
        {
            using (var cmd = pContext.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_INTEGRACAO.TOTALIZAPEDIDO";

                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, Globals.CodEstab, ParameterDirection.Input);
                var queueIdParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pCodPedVenda, ParameterDirection.Input);

                cmd.Parameters.AddRange(new[] {
                    codeEstabParam,
                    queueIdParam
                });
                cmd.Connection.Open();
                await cmd.ExecuteNonQueryAsync();
                cmd.Connection.Close();
            }
        }

        public async Task GenerateOrderCommissionAsync(HinoContext pContext, long pCodPedVenda)
        {
            using (var cmd = pContext.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.CommandText = "PCKG_INTEGRACAO.GERARCOMISSAOPEDIDO";

                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, Globals.CodEstab, ParameterDirection.Input);
                var queueIdParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pCodPedVenda, ParameterDirection.Input);

                cmd.Parameters.AddRange(new[] {
                    codeEstabParam,
                    queueIdParam
                });
                cmd.Connection.Open();
                await cmd.ExecuteNonQueryAsync();
                cmd.Connection.Close();
            }
        }

        public async Task LoadOrderTaxesAsync(HinoContext pContext, VEOrdersVM pOrder)
        {
            var Taxes = new List<VEItemPedImp>();
            using (var cmd = pContext.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT FSIMPOSTOS.CLASSIFICACAO, 
                                           VEITEMPEDIMP.ALIQUOTA, VEITEMPEDIMP.BASECALCULO, VEITEMPEDIMP.CODESTAB, 
                                           FSIMPOSTOS.CODIMPOSTO, VEITEMPEDIMP.CODPEDVENDA, VEITEMPEDIMP.CODPRODUTO, 
                                           VEITEMPEDIMP.MVA, VEITEMPEDIMP.PERREDBC, VEITEMPEDIMP.VALORIMPOSTO, 
                                           VEITEMPEDIMP.VALORISENTO, VEITEMPEDIMP.VALOROBSERVACAO, VEITEMPEDIMP.VALOROUTROS, 
                                           VEITEMPEDIMP.ZONAFRANCA, NVL(FSPRODUTOPARAMESTAB.IDAPI, 0) PRODUCTID
                                      FROM VEITEMPEDIMP,
                                           FSIMPOSTOS,
                                           FSPRODUTOPARAMESTAB
                                     WHERE VEITEMPEDIMP.CODESTAB          = :pCODESTAB
                                       AND VEITEMPEDIMP.CODPEDVENDA       = :pCODPEDVENDA
                                       AND FSIMPOSTOS.CODIMPOSTO          = VEITEMPEDIMP.CODIMPOSTO
                                       AND FSPRODUTOPARAMESTAB.CODPRODUTO = VEITEMPEDIMP.CODPRODUTO
                                       AND FSPRODUTOPARAMESTAB.CODESTAB   = VEITEMPEDIMP.CODESTAB";

                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, Globals.CodEstab, ParameterDirection.Input);
                var codPedVendaParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pOrder.IdERP, ParameterDirection.Input);

                cmd.Parameters.AddRange(new[] {
                    codeEstabParam,
                    codPedVendaParam
                });
                cmd.Connection.Open();
                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    Taxes = new List<VEItemPedImp>(
                             reader.Select(r =>
                             new VEItemPedImp
                             {
                                 ProductId = Convert.ToInt64(r["PRODUCTID"]),
                                 codpedvenda = Convert.ToInt64(r["CODPEDVENDA"]),
                                 codestab = Convert.ToInt32(r["CODESTAB"]),
                                 codproduto = Convert.ToString(r["CODPRODUTO"]),
                                 codimposto = Convert.ToInt16(r["CLASSIFICACAO"]),
                                 zonafranca = Convert.ToDecimal(r["ZONAFRANCA"]),
                                 aliquota = Convert.ToDecimal(r["ALIQUOTA"]),
                                 basecalculo = Convert.ToDecimal(r["BASECALCULO"]),
                                 valorimposto = Convert.ToDecimal(r["VALORIMPOSTO"]),
                                 valorisento = Convert.ToDecimal(r["VALORISENTO"]),
                                 valoroutros = Convert.ToDecimal(r["VALOROUTROS"]),
                                 valorobservacao = Convert.ToDecimal(r["VALOROBSERVACAO"]),
                                 mva = Convert.ToDecimal(r["MVA"]),
                                 perredbc = Convert.ToDecimal(r["PERREDBC"]),
                             }));
                }
                cmd.Connection.Close();
            }

            foreach (var orderItem in pOrder.VEOrderItems)
            {
                orderItem.VEOrderTaxes = new HashSet<VEOrderTaxesVM>(
                    Taxes.Where(r => r.ProductId == orderItem.ProductID)
                    .Select(r => new VEOrderTaxesVM
                    {
                        EstablishmentKey = pOrder.EstablishmentKey,
                        UniqueKey = pOrder.UniqueKey,
                        Created = DateTime.Now,
                        Modified = DateTime.Now,
                        IsActive = true,
                        OrderItemID = orderItem.Id,
                        Type = r.codimposto,
                        FreeZone = r.zonafranca,
                        Aliquot = r.aliquota,
                        Basis = r.basecalculo,
                        Value = r.valorimposto,
                        ExemptValue = r.valorisento,
                        OtherValue = r.valoroutros,
                        NoteValue = r.valorobservacao,
                        MVA = r.mva,
                        PerRedBC = r.perredbc,
                    }));
            }
        }

        public async Task LoadOrderCommissionAsync(HinoContext pContext, VEOrdersVM pOrder)
        {
            var Items = new List<VEItemPedido>();
            using (var cmd = pContext.Database.Connection.CreateCommand())
            {
                cmd.CommandType = CommandType.Text;
                cmd.CommandText = @"SELECT VEITEMPEDIDO.CODPEDVENDA, VEITEMPEDIDO.CODESTAB,
                                           VEITEMPEDIDO.CODPRODUTO, VEITEMPEDIDO.PERCOMISSAO,
                                           NVL(FSPRODUTOPARAMESTAB.IDAPI, 0) PRODUCTID
                                      FROM VEITEMPEDIDO,
                                           FSPRODUTOPARAMESTAB
                                     WHERE VEITEMPEDIDO.CODESTAB = :pCODESTAB
                                       AND VEITEMPEDIDO.CODPEDVENDA = :pCODPEDVENDA
                                       AND FSPRODUTOPARAMESTAB.CODESTAB = VEITEMPEDIDO.CODESTAB
                                       AND FSPRODUTOPARAMESTAB.CODPRODUTO = VEITEMPEDIDO.CODPRODUTO";

                var codeEstabParam = new OracleParameter("pCODESTAB", OracleDbType.Int32, Globals.CodEstab, ParameterDirection.Input);
                var codPedVendaParam = new OracleParameter("pCODPEDVENDA", OracleDbType.Int64, pOrder.IdERP, ParameterDirection.Input);

                cmd.Parameters.AddRange(new[] {
                    codeEstabParam,
                    codPedVendaParam
                });
                cmd.Connection.Open();
                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    Items = new List<VEItemPedido>(
                                 reader.Select(r =>
                                 new VEItemPedido
                                 {
                                     ProductId = Convert.ToInt64(r["PRODUCTID"]),
                                     codpedvenda = Convert.ToInt64(r["CODPEDVENDA"]),
                                     codestab = Convert.ToInt32(r["CODESTAB"]),
                                     codproduto = Convert.ToString(r["CODPRODUTO"]),
                                     percomissao = (float)Convert.ToDecimal(r["PERCOMISSAO"])
                                 }));
                }
                cmd.Connection.Close();
            }

            foreach (var orderItem in pOrder.VEOrderItems)
            {
                try
                {
                    var itemDb = Items.Where(r => r.ProductId == orderItem.ProductID).FirstOrDefault();

                    if (itemDb != null)
                        orderItem.PercCommission = itemDb.percomissao;
                }
                catch (Exception)
                {
                    orderItem.PercCommission = 0;
                }
            }
        }
    }
}
