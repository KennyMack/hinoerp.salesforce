﻿using Hino.Salesforce.Infra.Cross.Utils.Settings;
using Oracle.ManagedDataAccess.Client;
using System.Data.Entity;

namespace Hino.Salesforce.Sync.Data
{
    public class ConnectionStringBuilder
    {
        public static string Construct()
        {
            var newConnStringBuilder = new OracleConnectionStringBuilder
            {
                UserID = AppSettings.ORCL_UserID,
                Password = AppSettings.ORCL_Password,
                DataSource = AppSettings.ORCL_DataSource
            };

            return newConnStringBuilder.ConnectionString;
        }
    }
    public class AppDbContextConfig : DbConfiguration
    {
        public AppDbContextConfig()
        {
            this.AddInterceptor(new ConnectionInterceptor());
        }
    }
}
