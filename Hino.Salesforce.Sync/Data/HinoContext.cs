﻿using Hino.Salesforce.Infra.Cross.Utils.Settings;
using Hino.Salesforce.Sync.Models;
using Hino.Salesforce.Sync.Models.Fiscal;
using Hino.Salesforce.Sync.Models.General;
using Hino.Salesforce.Sync.Models.Vendas;
using Hino.Salesforce.Sync.Models.Views;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Data
{
    [DbConfigurationType(typeof(AppDbContextConfig))]
    public class HinoContext : DbContext
    {
        public virtual DbSet<CREnterprises> CREnterprises { get; set; }
        public virtual DbSet<CREnterpriseGeo> CREnterpriseGeo { get; set; }
        public virtual DbSet<CREnterpriseContacts> CREnterpriseContacts { get; set; }
        public virtual DbSet<CRGrupo> CRGrupo { get; set; }
        public virtual DbSet<CRCategoria> CRCategoria { get; set; }
        public virtual DbSet<CROrders> CROrders { get; set; }
        public virtual DbSet<CROrderItems> CROrderItems { get; set; }
        public virtual DbSet<CRProducts> CRProducts { get; set; }
        public virtual DbSet<CRTransactions> CRTransactions { get; set; }
        public virtual DbSet<CRSalePrice> CRSalePrice { get; set; }
        public virtual DbSet<FSProdutoParamEstab> FSProdutoParamEstab { get; set; }

        public virtual DbSet<GESyncLog> GESyncLog { get; set; }
        public virtual DbSet<GESincConfig> GESincConfig { get; set; }
        public virtual DbSet<GEEstab> GEEstab { get; set; }
        public virtual DbSet<GEQueueItem> GEQueueItem { get; set; }
        public virtual DbSet<VEPedidoSinc> VEPedidoSinc { get; set; }
        public virtual DbSet<GESincEmpresa> GESincEmpresa { get; set; }
        public virtual DbSet<FSSincProduto> FSSincProduto { get; set; }
        public virtual DbSet<VESincPrVenda> VESincPrVenda { get; set; }
        public virtual DbSet<VESincTransacoes> VESincTransacoes { get; set; }
        public virtual DbSet<GEVWPaymentCondInstallments> GEVWPaymentCondInstallments { get; set; }
        public virtual DbSet<GEVWPaymentCondition> GEVWPaymentCondition { get; set; }
        public virtual DbSet<GEVWPaymentType> GEVWPaymentType { get; set; }
        public virtual DbSet<GEVWEnterpriseFiscalGroup> GEVWEnterpriseFiscalGroup { get; set; }
        public virtual DbSet<GEVWEnterpriseContacts> GEVWEnterpriseContacts { get; set; }
        public virtual DbSet<GEVWEnterpriseCategory> GEVWEnterpriseCategory { get; set; }
        public virtual DbSet<GEVWEnterpriseGroup> GEVWEnterpriseGroup { get; set; }
        public virtual DbSet<GEVWSincEmpresasGeo> GEVWSincEmpresasGeo { get; set; }
        public virtual DbSet<GEVWSincEmpresas> GEVWSincEmpresas { get; set; }
        public virtual DbSet<FSVWFiscalOper> FSVWFiscalOper { get; set; }
        public virtual DbSet<GEVWSincProduto> GEVWSincProduto { get; set; }
        public virtual DbSet<GEVWProductAplic> GEVWProductAplic { get; set; }
        public virtual DbSet<GEVWProductsType> GEVWProductsType { get; set; }
        public virtual DbSet<GEVWProductsFamily> GEVWProductsFamily { get; set; }
        public virtual DbSet<GEVWProductsUnit> GEVWProductsUnit { get; set; }
        public virtual DbSet<GEVWSincEstoqueSaldo> GEVWSincEstoqueSaldo { get; set; }
        public virtual DbSet<VEVWSincTransacoes> VEVWSincTransacoes { get; set; }
        public virtual DbSet<VEVWSincCabecPedido> VEVWSincCabecPedido { get; set; }
        public virtual DbSet<VEVWSincItemPedido> VEVWSincItemPedido { get; set; }
        public virtual DbSet<VEVWSincTabPreco> VEVWSincTabPreco { get; set; }
        public virtual DbSet<VEVWSincRegiaoVenda> VEVWSincRegiaoVenda { get; set; }
        public virtual DbSet<VEVWRegionSaleUF> VEVWRegionSaleUF { get; set; }


        public HinoContext() : base("name=HinoContext")
        {
            Database.SetInitializer<HinoContext>(null);
            // this.Database.Log = Console.Write;
            this.Configuration.LazyLoadingEnabled = false;
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(AppSettings.DEFAULTSCHEMA);

            modelBuilder.Types().Configure(entity => entity.ToTable(entity.ClrType.Name.ToUpper()));

            modelBuilder.Properties().Configure(c =>
            {
                c.HasColumnName(c.ClrPropertyInfo.Name.ToUpper());
            });


            base.OnModelCreating(modelBuilder);
        }

        void DefaultData()
        {
            var props = ChangeTracker.Entries()
                             .Where(p =>
                             p.Entity.GetType().GetProperty("Created") != null
                             );

            foreach (var item in props)
            {
                switch (item.State)
                {
                    case EntityState.Detached:
                        break;
                    case EntityState.Unchanged:
                        break;
                    case EntityState.Added:
                        item.Property("Created").CurrentValue = DateTime.Now;
                        item.Property("Modified").CurrentValue = DateTime.Now;
                        item.Property("IsActive").CurrentValue = true;
                        /*var sql = $"SELECT SQ_{item.Entity.GetType().Name}.NEXTVAL FROM DUAL";


                        item.Property("Id").CurrentValue =
                            base.Database.SqlQuery<long>(sql).First();*/
                        break;
                    case EntityState.Deleted:
                        // item.Property("IsActive").CurrentValue = false;
                        // item.State = EntityState.Modified;
                        break;
                    case EntityState.Modified:
                        item.Property("Created").IsModified = false;
                        item.Property("Modified").CurrentValue = DateTime.Now;
                        break;
                    default:
                        break;
                }
            }

        }
        DbRawSqlQuery<long> GetNextSequency<T>()
        {
            var sql = $"SELECT SEQ_{typeof(T).Name}.NEXTVAL FROM DUAL";

            return this.Database.SqlQuery<long>(sql);
        }

        public long NextSequence<T>() =>
            GetNextSequency<T>().First();

        public override Task<int> SaveChangesAsync()
        {
            // DefaultData();
            return base.SaveChangesAsync();
        }

        public override int SaveChanges()
        {
            // DefaultData();
            return base.SaveChanges();
        }
    }
}
