﻿using Newtonsoft.Json;

namespace Hino.Salesforce.Sync
{
    public static class Globals
    {
        public static int CodEstab { get; set; }
        public static string EstablishmentKey { get; set; }
        public static string USER_TOKEN { get; set; }
        public static string REFRESH_TOKEN { get; set; }
        public static string PATHTOHINOCONN { get; set; }
        public static bool Orders { get; set; }
        public static string DEFAULTSCHEMA { get; set; }
        public static string Email { get; set; }
        public static string Password { get; set; }
        public static string BASEURL { get; set; }
        public static string RabbitHostName { get; set; }
        public static string RabbitUserName { get; set; }
        public static string RabbitPassword { get; set; }
        public static string RabbitPort { get; set; }

        public static JsonSerializerSettings JsonSerializerSettings = new JsonSerializerSettings
        {
            Culture = new System.Globalization.CultureInfo("pt-BR"),
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            DateFormatString = "dd/MM/yyyy HH:mm:ss"
        };
        public static JsonSerializer JsonSerializer => new JsonSerializer
        {
            Culture = new System.Globalization.CultureInfo("pt-BR"),
            ReferenceLoopHandling = ReferenceLoopHandling.Ignore,
            DateFormatString = "dd/MM/yyyy HH:mm:ss"
        };

    }
}
