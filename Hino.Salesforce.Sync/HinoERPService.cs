﻿using Hino.Salesforce.Application.ViewModels.Auth;
using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Sync.Application;
using Hino.Salesforce.Sync.Application.Auth;
using Hino.Salesforce.Sync.Data;
using Hino.Salesforce.Sync.Models;
using Hino.Salesforce.Sync.Models.General;
using Hino.Salesforce.Sync.Queue;
using Hino.Salesforce.Sync.Utils;
using log4net;
using log4net.Config;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync
{
    public partial class HinoERPService : ServiceBase
    {
        public List<Communication> CommunicationData = new List<Communication>();

        private readonly System.Timers.Timer timer = new System.Timers.Timer();
        private static readonly ILog Logger = LogManager.GetLogger(typeof(HinoERPService));

        public HinoERPService()
        {
            InitializeComponent();
            XmlConfigurator.Configure();
            timer.Elapsed += new System.Timers.ElapsedEventHandler(OnElapsedTime);
        }

        #region Método para iniciar o serviço em modo console
        public void Console(string[] args)
        {
            System.Console.WriteLine("Servico iniciado em modo console.");
            System.Console.WriteLine("Pressione qualquer tecla para sair.");
            this.OnStart(args);
            System.Console.ReadKey();
            this.OnStop();
        }
        #endregion

        public async Task<bool> AuthenticateUser()
        {
            var _AuthenticationService = new AuthenticationService();
            return await _AuthenticationService.AuthenticateAsync();
        }

        async Task ExecuteSinc()
        {
            try
            {
                timer.Stop();
            }
            catch (Exception)
            {
            }

            var Sync = new Syncronize();
            await Sync.DownloadQueue();
            // await Sync.UploadQueue();
            await Sync.UploadSync();
            var Stock = new SyncronizeStockBalance();
            if (!Stock.Synchonizing)
                await Stock.UploadStockBalance();

            try
            {
                timer.Start();
            }
            catch (Exception)
            {
            }
        }

        private async void OnElapsedTime(object source, System.Timers.ElapsedEventArgs e)
        {
            Logger.Info("Processando fila.");
            await ExecuteSinc();
        }

        protected async override void OnStop()
        {
            try
            {
                if (!Globals.USER_TOKEN.IsEmpty())
                {
                    var _AuthenticationService = new AuthenticationService();
                    await _AuthenticationService.SaveTokenToDbAsync();
                }
            }
            catch (Exception)
            {
            }
        }

        protected async override void OnStart(string[] args)
        {
            Logger.Info("Servico iniciado.");

            Logger.Info("Realizando conexao com o banco de dados.");

            List<GESincConfig> ListConfig = new List<GESincConfig>();
            List<GEEstab> ListEstab = new List<GEEstab>();
            using (var context = new HinoContext())
            {
                ListEstab = context.GEEstab.ToList();
                ListConfig = context.GESincConfig.ToList();
            }

            Logger.Info("Processando download dos dados.");

            foreach (var item in ListConfig)
            {
                var Estab = ListEstab.Where(r => r.codestab == item.codestab).FirstOrDefault();

                Logger.Info($"Percorrendo: {Estab.codestab} - {Estab.razaosocial}");
                Globals.EstablishmentKey = item.establishmentkey;
                Globals.CodEstab = Estab.codestab;
                Globals.Email = item.email;
                Globals.Password = item.senha.Decrypt();
                Globals.REFRESH_TOKEN = item.refreshtoken;
                Globals.USER_TOKEN = item.usertoken;

                if (!await AuthenticateUser())
                    continue;

                Logger.Info("Conectando na lista.");
                var comm = new Communication
                {
                    HostName = Globals.RabbitHostName,
                    UserName = Globals.RabbitUserName,
                    Password = Globals.RabbitPassword,
                    QueueName = $"{ item.establishmentkey }",
                    ExchangeName = item.establishmentkey
                };

                try
                {
                    comm.CreateConnection();
                    comm.NewMessage += Comm_NewMessage;
                    Logger.Info("Conectando com sucesso.");
                    CommunicationData.Add(comm);
                    Logger.Info("Aguardando mensagens.");
                    timer.Interval = 300000;
                    timer.Enabled = true;
                    timer.Start();
                }
                catch (Exception ex)
                {
                    Logger.Error("Problema ao conectar na lista.", ex);
                }
            }
        }

        private async void Comm_NewMessage(object sender, RabbitMQ.Client.Events.BasicDeliverEventArgs e)
        {
            Logger.Info("Mensagem recebida.");
            try
            {
                var Body = e.Body.ToArray();
                Logger.Info("Decodificando a mensagem.");
                var Message = Encoding.UTF8.GetString(Body);
                Logger.Info("Gerando JSON.");
                var Item = JsonConvert.DeserializeObject<GEQueueItem>(Message, Globals.JsonSerializerSettings);

                Logger.Info($"Salvando a mensagem no banco. Id. {Item.Id} - Id. Ref. {Item.IdReference}");
                await SaveMessage(Item);

                Logger.Info($"Gerando ACK na mensagem. Id. {Item.Id} - Id. Ref. {Item.IdReference}");
                ((Communication)sender).AckMessage(e.DeliveryTag);

                Logger.Info($"Processando a mensagem. Id. {Item.Id} - Id. Ref. {Item.IdReference}");
                var Sync = new Syncronize();
                await Sync.DownloadQueue();
                await ExecuteSinc();
            }
            catch (Exception ex)
            {
                Logger.Error("Problema ao salvar/processar a mensagem recebida.", ex);
            }
        }

        #region Save Message
        private async Task SaveMessage(GEQueueItem pItem)
        {
            using (var context = new HinoContext())
            {
                pItem.IdReference = pItem.Id;
                pItem.Id = context.NextSequence<GEQueueItem>();
                pItem.Created = DateTime.Now;
                pItem.Modified = DateTime.Now;
                context.GEQueueItem.Add(pItem);

                await context.SaveChangesAsync();
            }
        }
        #endregion
    }
}
