﻿using Hino.Salesforce.Sync.Models.Interfaces;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models
{
    public class BaseEntity : IBaseEntity
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long Id { get; set; }
        public string EstablishmentKey { get; set; }
        public string UniqueKey { get; set; }
        public DateTime Created { get; set; }
        public DateTime Modified { get; set; }

        public bool IsActive { get; set; }
    }
}
