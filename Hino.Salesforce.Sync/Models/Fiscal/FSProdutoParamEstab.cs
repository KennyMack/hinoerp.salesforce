﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Fiscal
{
    [Table("FSPRODUTOPARAMESTAB")]
    public class FSProdutoParamEstab
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(Order = 1)]
        public string CodProduto { get; set; }
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        [Column(Order = 0)]
        public long CodEstab { get; set; }
        public bool PermSinc { get; set; }
    }
}
