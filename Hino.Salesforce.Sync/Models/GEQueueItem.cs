﻿using Hino.Salesforce.Sync.Models.General;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models
{
    public class GEQueueItem : BaseEntity
    {
        public GEQueueItem()
        {
            CREnterprises = new HashSet<CREnterprises>();
        }
        public long IdReference { get; set; }
        public bool Processed { get; set; }
        public bool Uploaded { get; set; }
        public string EntryName { get; set; }
        public string Type { get; set; }
        public string Note { get; set; }
        public string ExceptionCode { get; set; }

        [NotMapped]
        public int Priority
        {
            get
            {
                switch (EntryName)
                {
                    case Constants.GEEnterpriseCategory:
                        return 0;
                    case Constants.GEEnterpriseGroup:
                        return 1;
                    case Constants.GEEnterprises:
                        return 2;
                    case Constants.GEProducts:
                        return 3;
                    case Constants.VESincPrVenda:
                        return 4;
                    case Constants.VESalePrice:
                        return 5;
                    case Constants.VEOrders:
                        return 6;
                    case Constants.VETransactions:
                        return 7;
                    default:
                        return 999;
                }
            }
        }

        public virtual ICollection<CREnterprises> CREnterprises { get; set; }
    }
}
