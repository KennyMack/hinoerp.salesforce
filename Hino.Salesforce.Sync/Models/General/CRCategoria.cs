﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.General
{
    [Table("CRCATEGORIA")]
    public class CRCategoria
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int CodCategoria { get; set; }
        public string Descricao { get; set; }
        public string CodClasse { get; set; }
        public bool Uploaded { get; set; }
    }
}
