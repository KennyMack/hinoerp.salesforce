﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.General
{
    public class CREnterprises : BaseEntity
    {
        public CREnterprises()
        {
            this.GEEnterpriseGeo = new HashSet<CREnterpriseGeo>();
            this.GEEnterpriseContacts = new HashSet<CREnterpriseContacts>();
        }

        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string CNPJCPF { get; set; }
        public string IE { get; set; }
        public int Type { get; set; }
        public int Status { get; set; }
        public int StatusSinc { get; set; }
        public long IdERP { get; set; }
        public long RegionId { get; set; }
        public int Classification { get; set; }
        public long? UserId { get; set; }
        public long? FiscalGroupId { get; set; }
        public long? PayConditionId { get; set; }
        public long? CategoryId { get; set; }
        public long? GroupId { get; set; }
        public string ClassifEmpresa { get; set; }
        public long? CreatedById { get; set; }
        public long? CodPrVenda { get; set; }
        [ForeignKey("GEQueueItem")]
        public long QueueId { get; set; }
        public virtual GEQueueItem GEQueueItem { get; set; }
        public virtual ICollection<CREnterpriseGeo> GEEnterpriseGeo { get; set; }
        public virtual ICollection<CREnterpriseContacts> GEEnterpriseContacts { get; set; }
    }
}
