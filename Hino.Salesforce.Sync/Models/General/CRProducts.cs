﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.General
{
    public class CRProducts : BaseEntity
    {
        public string ProductKey { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public long TypeId { get; set; }
        public string TypeProd { get; set; }
        public long FamilyId { get; set; }
        public string FamilyProd { get; set; }
        public double PercIPI { get; set; }
        public double PercMaxDiscount { get; set; }
        public double Value { get; set; }
        public double StockBalance { get; set; }
        public int Status { get; set; }
        public string NCM { get; set; }
        public long? NCMId { get; set; }
        public long? UnitId { get; set; }
        public string Unit { get; set; }
        public decimal Weight { get; set; }
        public decimal PackageQTD { get; set; }
        public long? SaleUnitId { get; set; }
        public string SaleUnit { get; set; }
        public decimal SaleFactor { get; set; }
        public long? AplicationId { get; set; }
        public int CodOrigMerc { get; set; }
        [ForeignKey("GEQueueItem")]
        public long QueueId { get; set; }
        public virtual GEQueueItem GEQueueItem { get; set; }
    }
}
