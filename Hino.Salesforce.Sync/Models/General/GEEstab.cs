﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.General
{
    [Table("GEESTAB")]
    public class GEEstab
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int codestab { get; set; }
        public string razaosocial { get; set; }
    }
}
