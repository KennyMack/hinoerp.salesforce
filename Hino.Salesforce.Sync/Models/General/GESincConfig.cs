﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.General
{
    [Table("GESINCCONFIG")]
    public class GESincConfig
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int codestab { get; set; }

        public string email { get; set; }
        public string senha { get; set; }
        public string establishmentkey { get; set; }
        public string usertoken { get; set; }
        public string refreshtoken { get; set; }
        public DateTime ultimasincronia { get; set; }
        public DateTime ultimabusca { get; set; }
        public DateTime sincformapgto { get; set; }
        public DateTime sinccondpgto { get; set; }
        public DateTime sincregioes { get; set; }
        public DateTime sincempresas { get; set; }
        public DateTime sincprodutos { get; set; }
        public DateTime sinctabpreco { get; set; }
        public DateTime sincpedidos { get; set; }
        public DateTime sincsaldoestoque { get; set; }
        public DateTime sinctransacoes { get; set; }
        public DateTime buscaformapgto { get; set; }
        public DateTime buscacondpgto { get; set; }
        public DateTime buscaregioes { get; set; }
        public DateTime buscaempresas { get; set; }
        public DateTime buscaprodutos { get; set; }
        public DateTime buscatabpreco { get; set; }
        public DateTime buscapedidos { get; set; }
        public DateTime buscasaldoestoque { get; set; }
        public DateTime buscatransacoes { get; set; }
    }
}
