﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.General
{
    [Table("GESYNCLOG")]
    public class GESyncLog
    {
        [Key]
        [Column(Order = 1)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int codestab { get; set; }
        [Key]
        [Column(Order = 2)]
        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public long codlog { get; set; }
        public DateTime datelog { get; set; }
        public int tipolog { get; set; }
        public string tabelalog { get; set; }
        public string logerro { get; set; }
        public string bodyerro { get; set; }
        public int tentativas { get; set; }
        public int concluido { get; set; }
    }
}
