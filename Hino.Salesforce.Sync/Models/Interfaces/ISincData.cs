﻿using System;

namespace Hino.Salesforce.Sync.Models.Interfaces
{
    public interface ISincData
    {
        long CodSinc { get; set; }
        long CodEstab { get; set; }
        DateTime DataAlt { get; set; }
        DateTime? DataSinc { get; set; }
        bool Sincronizado { get; set; }
        string UniqueKey { get; set; }
        long? IdApi { get; set; }
        int Tipo { get; set; }
    }
}
