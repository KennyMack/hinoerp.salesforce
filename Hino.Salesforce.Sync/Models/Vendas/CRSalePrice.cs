﻿using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Vendas
{
    [Table("CRSALEPRICE")]
    public class CRSalePrice : BaseEntity
    {
        public long CodPrVenda { get; set; }
        public long RegionId { get; set; }

        public string Description { get; set; }

        public long ProductId { get; set; }

        public decimal Value { get; set; }

        [ForeignKey("GEQueueItem")]
        public long QueueId { get; set; }
        //public long IdERP { get; set; }
        public virtual GEQueueItem GEQueueItem { get; set; }
    }
}
