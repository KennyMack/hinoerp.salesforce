﻿using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    public class CROrderItems : BaseEntity
    {
        [ForeignKey("CROrders")]
        public long OrderID { get; set; }
        public virtual CROrders CROrders { get; set; }

        public long ProductID { get; set; }
        public long FiscalOperID { get; set; }
        public decimal TableValue { get; set; }
        public float Value { get; set; }
        public float Quantity { get; set; }
        public float QuantityReference { get; set; }
        public float PercDiscount { get; set; }
        public string Note { get; set; }
        public int Item { get; set; }
        public int ItemLevel { get; set; }
        public long IdERP { get; set; }
        public string ClientOrder { get; set; }
        public string ClientItem { get; set; }
        public System.DateTime DeliveryDate { get; set; }
        public float PercDiscountHead { get; set; }
        public float PercCommission { get; set; }
        public float PercCommissionHead { get; set; }
        public int ShippingDays { get; set; }
        public bool AltDescription { get; set; }
    }
}
