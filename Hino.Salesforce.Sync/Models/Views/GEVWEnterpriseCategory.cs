using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("GEVWENTERPRISECATEGORY")]
    public class GEVWEnterpriseCategory
    {
        [Key]
        public long? IDApi { get; set; }
        public string UniqueKey { get; set; }
        public long Identifier { get; set; }
        public string Description { get; set; }
    }
}