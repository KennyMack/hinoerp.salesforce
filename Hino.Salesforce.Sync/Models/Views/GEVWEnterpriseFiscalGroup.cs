using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("GEVWENTERPRISEFISCALGROUP")]
    public class GEVWEnterpriseFiscalGroup
    {
        public long? IDApi { get; set; }
        public string UniqueKey { get; set; }
        [Key]
        public long IDERP { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public decimal RemICMSPisCofins { get; set; }
    }
}