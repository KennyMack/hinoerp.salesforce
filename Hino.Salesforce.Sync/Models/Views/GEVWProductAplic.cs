using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("GEVWPRODUCTAPLIC")]
    public class GEVWProductAplic
    {
        public string ID { get; set; }
        public string UniqueKey { get; set; }
        public string Description { get; set; }
        public decimal Classification { get; set; }
        [Key]
        public decimal IDERP { get; set; }
    }
}