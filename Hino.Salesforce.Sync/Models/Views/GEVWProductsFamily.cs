using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("GEVWPRODUCTSFAMILY")]
    public class GEVWProductsFamily
    {
        [Key]
        public string ID { get; set; }
        public string UniqueKey { get; set; }
        public string Description { get; set; }
        public string GroupDescription { get; set; }
        public string CatDescription { get; set; }
    }
}