﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("GEVWSINCEMPRESAS")]
    public class GEVWSincEmpresas
    {
        public long IDApi { get; set; }
        public string UniqueKey { get; set; }
        public string RazaoSocial { get; set; }
        public string NomeFantasia { get; set; }
        public string CpfCnpj { get; set; }
        public int TipoCli { get; set; }
        public int Status { get; set; }
        public int StatusSinc { get; set; }
        public long? PayConditionID { get; set; }
        public long RegionID { get; set; }
        public long? UserID { get; set; }
        public int Classification { get; set; }
        public string IE { get; set; }
        public int? FiscalGroupID { get; set; }
        public string FSGrpFisEmpDescricao { get; set; }
        public string FSGrpFisEmpExcICMSBasePISCOF { get; set; }
        public string FSGrpFisEmpType { get; set; }
        public string ClassifEmpresa { get; set; }
        [Key]
        public long IDERP { get; set; }
        public int CodEstab { get; set; }
        public DateTime DataModificacao { get; set; }
        public long? CategoryId { get; set; }
        public string CRCategoriaDescricao { get; set; }
        public long? GroupId { get; set; }
        public string CRGrupoDescricao { get; set; }
        public long? CodPrVenda { get; set; }
    }
}
