using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("GEVWSINCEMPRESASGEO")]
    public class GEVWSincEmpresasGeo
    {
        [Key]
        public long IDApi { get; set; }
        public long CodEmpresaERP { get; set; }
        public string UniqueKey { get; set; }
        public long EnterpriseID { get; set; }
        public int Type { get; set; }
        public string CpfCnpj { get; set; }
        public string InscEstadual { get; set; }
        public string CellPhone { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Endereco { get; set; }
        public string Complemento { get; set; }
        public string Bairro { get; set; }
        public int? Num { get; set; }
        public string ZipCode { get; set; }
        public string Site { get; set; }
        public string CodBacen { get; set; }
        public string CountryIni { get; set; }
        public string CountryCode { get; set; }
        public string CountryName { get; set; }
        public string UF { get; set; }
        public string StateName { get; set; }
        public string CodIBGE { get; set; }
        public string CityName { get; set; }
        public decimal DisplayLat { get; set; }
        public decimal DisplayLng { get; set; }
        public decimal NavLat { get; set; }
        public decimal NavLng { get; set; }
        public string InscSuframa { get; set; }
        public int CodEstab { get; set; }
        public DateTime DataModificacao { get; set; }
    }
}