using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("GEVWSINCESTOQUESALDO")]
    public class GEVWSincEstoqueSaldo
    {
        public long IDApi { get; set; }
        public string EstablishmentKey { get; set; }
        public string UniqueKey { get; set; }
        public DateTime DataModificacao { get; set; }
        [Key]
        public string CodProduto { get; set; }
        public int CodEstab { get; set; }
        public string Codestoque { get; set; }
        public double SaldoAtual { get; set; }
        public double SaldoAnterior { get; set; }
    }
}