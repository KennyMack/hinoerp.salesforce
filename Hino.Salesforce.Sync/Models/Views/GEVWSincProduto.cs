﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("GEVWSINCPRODUTO")]
    public class GEVWSincProduto
    {
        public long IDApi { get; set; }
        public string UniqueKey { get; set; }
        public DateTime DataModificacao { get; set; }
        [Key]
        public string CodProduto { get; set; }
        public string Image { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public double PercIPI { get; set; }
        public double LimiteDesconto { get; set; }
        public double ValorUnitario { get; set; }
        public double Saldo { get; set; }
        public int Status { get; set; }
        public string NCM { get; set; }
        public string CodUnidade { get; set; }
        public decimal PesoBruto { get; set; }
        public decimal QtdePorEmb { get; set; }
        public string UmVenda { get; set; }
        public decimal FatorUmVenda { get; set; }
        public string Family { get; set; }
        public string TypeProd { get; set; }
        public string SaleUnitID { get; set; }
        public string UnitID { get; set; }
        public string NCMID { get; set; }
        public int? AplicationId { get; set; }
        public int CodOrigMerc { get; set; }
        public int CodEstab { get; set; }
        public int StatusSinc { get; set; }
    }
}
