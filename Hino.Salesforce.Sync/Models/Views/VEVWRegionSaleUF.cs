using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("VEVWREGIONSALEUF")]
    public class VEVWRegionSaleUF
    {
        public int CodEstab { get; set; }
        [Key]
        public long IDApi { get; set; }
        public string UniqueKey { get; set; }
        public int RegionID { get; set; }
        public string UF { get; set; }
        public DateTime DataModificacao { get; set; }
    }
}