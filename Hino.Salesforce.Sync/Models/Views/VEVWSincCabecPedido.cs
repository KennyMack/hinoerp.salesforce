using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("VEVWSINCCABECPEDIDO")]
    public class VEVWSincCabecPedido
    {
        public long ID { get; set; }
        public long EnterpriseID { get; set; }
        public long UserID { get; set; }
        [Key]
        public long IDERP { get; set; }
        public decimal NumPedMob { get; set; }
        public string UniqueKey { get; set; }
        public long TypePaymentID { get; set; }
        public long PayConditionID { get; set; }
        public DateTime DeliveryDate { get; set; }
        public string Note { get; set; }
        public string InnerNote { get; set; }
        public string Status { get; set; }
        public string StatusCRM { get; set; }
        public int StatusSinc { get; set; }
        public bool IsProposal { get; set; }
        public long? CarrierID { get; set; }
        public int FreightPaidBy { get; set; }
        public decimal FreightValue { get; set; }
        public string ClientOrder { get; set; }
        public int RedispatchPaidBy { get; set; }
        public long? RedispatchID { get; set; }
        public DateTime DataModificacao { get; set; }
        public int CodEstab { get; set; }
        public long? OriginOrderID { get; set; }
        public long? MainOrderID { get; set; }
        public int OrderVersion { get; set; }

        public int SomenteData { get; set; }
        public int FaturarParc { get; set; }
        public string ContatoFone { get; set; }
        public string ContatoEmail { get; set; }
        public string Contato { get; set; }
        public int Sector { get; set; }
        public decimal FinancialTaxes { get; set; }
        public string RevisionReason { get; set; }
        public long DigitizerID { get; set; }

        public float PercDiscount { get; set; }
        public float PercCommission { get; set; }

        public Boolean InPerson { get; set; }
        public DateTime? PaymentDueDate { get; set; }
        public int StatusPay { get; set; }
        public float PaidAmount { get; set; }
        public float AdvanceAmount { get; set; }
        public int? CodTipoVenda { get; set; }
        public long? TypeSaleId { get; set; }
    }
}