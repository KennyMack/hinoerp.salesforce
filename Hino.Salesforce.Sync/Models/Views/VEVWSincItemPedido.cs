using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("VEVWSINCITEMPEDIDO")]
    public class VEVWSincItemPedido
    {
        [Key]
        public long ID { get; set; }
        public long OrderID { get; set; }
        public long ProductID { get; set; }
        public decimal TableValue { get; set; }
        public float Value { get; set; }
        public float Quantity { get; set; }
        public float QuantityReference { get; set; }
        public float PercDiscount { get; set; }
        public string Note { get; set; }
        public long IDERP { get; set; }
        public int Item { get; set; }
        public int ItemLevel { get; set; }
        public string ClientOrder { get; set; }
        public string ClientItem { get; set; }
        public DateTime DeliveryDate { get; set; }
        public int FiscalOperID { get; set; }
        public float PercDiscountHead { get; set; }
        public float PercCommission { get; set; }
        public float PercCommissionHead { get; set; }
        public int CodEstab { get; set; }
        public DateTime DataModificacao { get; set; }
        public string UniqueKey { get; set; }
        public string FSNatOperDescricao { get; set; }
        public int ShippingDays { get; set; }
        public bool AltDescription { get; set; }
        public float QuantityReturned { get; set; }
    }
}