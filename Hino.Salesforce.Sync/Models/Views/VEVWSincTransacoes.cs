﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforce.Sync.Models.Views
{
    [Table("VEVWSINCTRANSACOES")]
    public class VEVWSincTransacoes
    {
        [Key]
        public long CodTransacao { get; set; }
        public long CodPedVenda { get; set; }
        public int CodEstab { get; set; }
        public int Tipo { get; set; }
        public string AuthCode { get; set; }
        public DateTime? AuthDate { get; set; }
        public string AcquirerName { get; set; }
        public string Nsu { get; set; }
        public string AdminCode { get; set; }
        public string CardBrand { get; set; }
        public string CardLastDigits { get; set; }
        public int? Parcelas { get; set; }
        public decimal Valor { get; set; }
        public string Descricao { get; set; }
        public int Status { get; set; }
        public string CustomerReceipt { get; set; }
        public string MerchantReceipt { get; set; }
        public int Estornado { get; set; }
        public long IdApi { get; set; }
        public string UniqueKey { get; set; }
        public int Origem { get; set; }
        public long OrderId { get; set; }
    }
}
