﻿using Hino.Salesforce.Infra.Cross.Utils.Settings;
using Hino.Salesforce.Sync.Utils;
using System.Configuration;
using System.ServiceProcess;

namespace Hino.Salesforce.Sync
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main(params string[] args)
        {
            System.Threading.Thread.CurrentThread.CurrentCulture = new System.Globalization.CultureInfo("pt-BR");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("pt-BR");

            var console = 0;
            if ((args.Length > 0) && (args[0].ToLower() == "-console"))
                console = 1;

            ConfigurationManager.AppSettings.LoadSettingsSync();

            Globals.BASEURL = AppSettings.BaseURL;
            Globals.Email = AppSettings.Email;
            Globals.Password = AppSettings.Password;

            Globals.Orders = AppSettings.Orders;
            Globals.PATHTOHINOCONN = AppSettings.PATH_TO_HINO_CONN;
            Globals.DEFAULTSCHEMA = AppSettings.DEFAULTSCHEMA;

            Globals.RabbitHostName = AppSettings.RABBIT_URL;
            Globals.RabbitUserName = AppSettings.RABBIT_USER;
            Globals.RabbitPassword = AppSettings.RABBIT_PASS;
            Globals.RabbitPort = AppSettings.RABBIT_PORT;

            try
            {
                if (console == 1)
                {
                    HinoERPService srv = new HinoERPService();
                    srv.Console(args);
                }
                else
                {
                    ServiceBase[] ServicesToRun;
                    ServicesToRun = new ServiceBase[]
                    {
                    new HinoERPService()
                    };
                    ServiceBase.Run(ServicesToRun);
                }

            }
            catch (System.Exception ex)
            {
                Logger.Error("Ocorreu um erro na execução.", ex, "99999");
                throw;
            }
        }
    }
}
