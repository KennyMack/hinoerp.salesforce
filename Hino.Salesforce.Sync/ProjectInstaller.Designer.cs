﻿
namespace Hino.Salesforce.Sync
{
    partial class ProjectInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.spiHinoERPInstaler = new System.ServiceProcess.ServiceProcessInstaller();
            this.siHinoERP = new System.ServiceProcess.ServiceInstaller();
            // 
            // spiHinoERPInstaler
            // 
            this.spiHinoERPInstaler.Account = System.ServiceProcess.ServiceAccount.LocalService;
            this.spiHinoERPInstaler.Password = null;
            this.spiHinoERPInstaler.Username = null;
            // 
            // siHinoERP
            // 
            this.siHinoERP.DelayedAutoStart = true;
            this.siHinoERP.Description = "Sincronizador de informações do CRM para o ERP";
            this.siHinoERP.DisplayName = "Hino Sync";
            this.siHinoERP.ServiceName = "HinoERPSync";
            // 
            // ProjectInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.siHinoERP,
            this.spiHinoERPInstaler});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller spiHinoERPInstaler;
        private System.ServiceProcess.ServiceInstaller siHinoERP;
    }
}