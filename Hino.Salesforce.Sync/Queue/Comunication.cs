﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;

namespace Hino.Salesforce.Sync.Queue
{
    public class Communication
    {
        public string ExchangeName { get; set; }
        public string QueueName { get; set; }
        public string HostName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }

        public ConnectionFactory ConnectionCFG { get; private set; }
        public IConnection Connection { get; private set; }
        public IModel Channel { get; private set; }
        public EventingBasicConsumer ConsumerEvent { get; private set; }

        public event EventHandler<BasicDeliverEventArgs> NewMessage;

        #region Cria Connexão
        public bool CreateConnection()
        {
            var retorno = true;

            ConnectionCFG = new ConnectionFactory
            {
                HostName = HostName,
                UserName = UserName,
                Password = Password
            };

            Connection = ConnectionCFG.CreateConnection();

            Channel = Connection.CreateModel();
            Channel.ExchangeDeclare(
                exchange: ExchangeName,
                type: "topic",
                durable: true,
                autoDelete: true);

            Channel.QueueDeclare(
                queue: QueueName,
                durable: true,
                exclusive: false,
                autoDelete: true,
                arguments: null);

            Channel.QueueBind(
                queue: QueueName,
                exchange: ExchangeName,
                routingKey: ExchangeName);

            ConsumerEvent = new EventingBasicConsumer(Channel);

            ConsumerEvent.Received += ConsumerEvent_Received;
            Channel.BasicConsume(
                queue: QueueName,
                autoAck: false,
                consumer: ConsumerEvent);

            return retorno;
        }

        #endregion

        #region Evento de mensagem nova
        private void ConsumerEvent_Received(object sender, BasicDeliverEventArgs e) =>
            NewMessage?.Invoke(this, e);
        #endregion

        #region Ack message
        public void AckMessage(ulong pDeliveryTag) =>
            Channel.BasicAck(deliveryTag: pDeliveryTag, multiple: false);
        #endregion

    }
}
