﻿using Hino.Salesforce.Infra.Cross.Utils;
using Hino.Salesforce.Infra.Cross.Utils.Exceptions;
using log4net;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Hino.Salesforce.Sync.Utils
{
    public static class Logger
    {
        private static readonly ILog Log = LogManager.GetLogger(typeof(HinoERPService));

        public static void Info(string pText) =>
            Log.Info(pText);

        public static string Error(string pText, List<ModelException> pException, string ExceptionCode = "")
        {
            var ErrorLog = string.Join(", ", pException.Select(r => string.Join(", ", r.Messages)).ToArray());
            if (ExceptionCode.IsEmpty())
                ExceptionCode = Guid.NewGuid().ToString();
            Log.Error(pText);
            Log.Error($"Erro: { ExceptionCode }");
            Log.Error($"Motivo: { ErrorLog }");

            return ErrorLog;
        }

        public static string Error(string pText, Exception pException, string ExceptionCode = "")
        {
            var ErrorLog = pException.Message;
            if (ExceptionCode.IsEmpty())
                ExceptionCode = Guid.NewGuid().ToString();
            Log.Error(pText);
            Log.Error($"Erro: { ExceptionCode }");
            Log.Error($"Motivo: { ErrorLog }");
            Log.Error($"Stack Inicio -----------------------------------");
            Log.Error(pException);
            if (pException.InnerException != null)
            {
                Log.Error($"InnerException Stack Inicio -----------------------------------");
                Log.Error(pException.InnerException);
                Log.Error($"InnerException Stack Termino -----------------------------------");
            }
            Log.Error($"Stack Termino -----------------------------------");

            return ErrorLog;
        }

    }
}
