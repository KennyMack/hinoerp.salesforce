﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Hino.Salesforce.Test.Startup))]
namespace Hino.Salesforce.Test
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
