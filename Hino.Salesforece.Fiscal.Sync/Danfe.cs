﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NFe.Classes;
using NFe.Danfe.Base.NFe;
using NFe.Danfe.Fast.Standard.NFe;

namespace Hino.Salesforece.Invoice.Sync
{
    public class Danfe
    {
        private const string SOFTWARE_HOUSE_NAME = "Hino Sistemas de Gestão";

        private ConfiguracaoDanfeNfe _danfeConfig;
        public Danfe()
        {
            Configure();
        }

        private void Configure()
        {
            _danfeConfig = new ConfiguracaoDanfeNfe(logomarca: null);
        }

        public Byte[] CreateDanfe(String xml)
        {
            var proc = new nfeProc().CarregarDeXmlString(xml);
            var danfe = new DanfeFrNfe(proc, _danfeConfig, SOFTWARE_HOUSE_NAME);
            var report = danfe.Relatorio;

            report.Compressed = false;
            report.Prepare();
            report.TextQuality = FastReport.TextQuality.ClearType;

            FastReport.Export.PdfSimple.PDFSimpleExport export = new FastReport.Export.PdfSimple.PDFSimpleExport();
            export.ImageDpi = 150;
            export.JpegQuality = 70;
            using (MemoryStream rptStream = new MemoryStream())
            {
                report.Export(export, rptStream);
                //export.Export(danfe.Relatorio, rptStream);
                return rptStream.ToArray();
            }

            //return rptStream.ToArray();
        }
    }
}
