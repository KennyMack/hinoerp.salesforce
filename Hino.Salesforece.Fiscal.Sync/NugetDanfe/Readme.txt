﻿* Instrução sobre o Nuget Package *
Foi instalado o pacote DFe Zeus para impressão da NFe via Net5. O nuget package oficial está com problema de compatibilidade com a parte da impressão da Danfe.
Nesse caso, o recomendado é buildar o pacote desejado manualmente. Segue os passos necessários:
1 - Baixar o projeto do github (https://github.com/ZeusAutomacao/DFe.NET)
2 - Mover os arquivos dentro de NugetDanfe/Build para uma pasta dentro do projeto baixado(respeitar um local que encontre os arquivos mencionados no .nuspec)
3 - Utilizar os arquivos dentro de NugetDanfe/Build para buildar o pacote nuget (comando "dotnet pack *nome do .csproj*)
4 - Obter os arquivos Zeus.Net.Danfe.1.0.1.nupkg e Zeus.Net.NFe.NFCe.1.0.1.nupkg dentro da pasta bin do local onde foi realizado o build
5 - Referenciar os arquivos .nupkg no projeto a ser usado

Obs¹: Os pacotes construídos estão disponibilizados na pasta NugetDanfe para facilitar o uso.
Obs²: Futuramente o projeto do github lançará um novo nuget package reestruturo para corrigir o problema da impressão da danfe, nesse momento será possível instalar direto do repositório nuget.
