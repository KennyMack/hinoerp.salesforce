﻿using Hino.Salesforce.Sync.Core.Configuration;
using Hino.Salesforce.Sync.Core.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Hino.Salesforece.Invoice.Sync
{
    public class UploadFile
    {
        private AppSettingsParameters AppSettings { get; }
        //private long totalBytes;
        public UploadFile(AppSettingsParameters AppSettings)
        {
            this.AppSettings = AppSettings;
        }

        public async Task PostAsync(string pId, string establishmentKey, string fileName)
        {
            using (WebClient client = new WebClient())
            {
                var url = AppSettings.BaseURL;
                //var estab = AppSettings.ServiceSettings.EstablishmentKey;

                var uri = $"{url}/api/General/Files/Path/{establishmentKey}/orders/key/{establishmentKey}/id/{pId}/upload";
                LogData.Info("Tentando subir o arquivo");
                LogData.Info($"Arquivo: {fileName}");
                //totalBytes = file.Length;
                client.UploadProgressChanged += Client_UploadProgressChanged;
                client.UploadFileCompleted += Client_UploadFileCompleted;
                await client.UploadFileTaskAsync(uri, fileName);
            }
        }

        private void Client_UploadFileCompleted(object sender, UploadFileCompletedEventArgs e)
        {
            LogData.Info($"Upload concluído");
            if (e.Error != null)
            {
                LogData.Info($"Problema ao upload");
                LogData.Info($"{e.Error.Message}");
            }
        }

        private void Client_UploadProgressChanged(object sender, UploadProgressChangedEventArgs e)
        {
            //decimal percentual = Math.Round(decimal.Divide(e.BytesSent, totalBytes) * 100, 0);

            //LogData.Info($"bytes enviados: {e.BytesSent} de {totalBytes} upload: {percentual}/100%");
        }
    }
}
