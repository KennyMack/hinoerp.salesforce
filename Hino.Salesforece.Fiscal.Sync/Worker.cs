using Hino.Salesforce.Sync.Core.Configuration;
using Hino.Salesforce.Sync.Core.Logging;
using Hino.Salesforce.Sync.Core.Utils;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Hino.Salesforce.Sync.Core.DataBase;

namespace Hino.Salesforece.Invoice.Sync
{
    public class Worker : BackgroundService
    {
        private IConfiguration Configuration { get; }
        private AppSettingsParameters AppSettings { get; }
        private string PathUpload { get; set; }

        public Worker(IConfiguration config)
        {
            Configuration = config;
            AppSettings = Configuration.Get<AppSettingsParameters>();
        }

        public override async Task StartAsync(CancellationToken cancellationToken)
        {
            // DO YOUR STUFF HERE
            await base.StartAsync(cancellationToken);
        }

        public override async Task StopAsync(CancellationToken cancellationToken)
        {
            // DO YOUR STUFF HERE
            await base.StopAsync(cancellationToken);
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            PathUpload = $"{AppSettings.InvoicePath}\\upload";
            while (!stoppingToken.IsCancellationRequested)
            {
                if (!FileManager.PathExists(PathUpload))
                    FileManager.CreatePath(PathUpload);

                LogData.Info("Iniciando processamento");

                foreach (var configErp in AppSettings.ServiceSettings.DataBases)
                {
                    LogData.Info($"Percorrendo a empresa: {configErp.Description}");

                    LogData.Info($"Conectando ao banco de dados do ERP: {configErp.Description}");

                    var contextFactory = new ERPContextFactory();
                    using var ERPContext = (contextFactory.CreateDbContext(configErp.GetParameters()));

                    LogData.Info($"Conex�o realizada: {configErp.Description} ");

                    // Obter quais notas precisam enviar para o portal
                    var listInvoiceSinc = ERPContext.FSVWSincDanfe
                                                    .Where(r => r.Xml != null)
                                                    .ToList();

                    LogData.Info($"Existem { listInvoiceSinc.Count } para sincronizar.");

                    foreach (var invoice in listInvoiceSinc)
                    {
                        // Gerar NF
                        Danfe danfe = new Danfe();
                        Byte[] danfeRpt = danfe.CreateDanfe(invoice.Xml);
                        String danfePath = PathUpload + $"\\NF-{invoice.notaFiscal}.pdf";
                        await File.WriteAllBytesAsync(danfePath, danfeRpt);

                        var Upload = new UploadFile(AppSettings);
                        await Upload.PostAsync(invoice.IdApi.ToString(), configErp.Name, danfePath);

                        FileManager.RemoveFile(danfePath);

                        var erpInvoice = ERPContext.FSNfSaida.FirstOrDefault(r => r.IndiceNfSai == invoice.IndiceNfSai);
                        erpInvoice.Sincronizado = true;
                        await ERPContext.SaveChangesAsync();

                        await Task.Delay(5000, stoppingToken);

                        // Postar na API
                        LogData.Info($"NF {invoice.IndiceNfSai} sincronizada.");
                    }

                    LogData.Info($"Finalizando integra��o: {configErp.Description}");
                }
                await Task.Delay(AppSettings.Interval, stoppingToken);
            }
        }

        /*
        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            PathUpload = $"{AppSettings.InvoicePath}\\upload";
            while (!stoppingToken.IsCancellationRequested)
            {
                LogData.Info($"Iniciando processamento");

                var Files = FileManager.GetPathFiles(AppSettings.InvoicePath, "*.pdf");

                foreach (var item in Files)
                {
                    LogData.Info($"arquivo: {item.FullName} - extens�o: {item.Extension}");
                    var Invoice = item.Name.Substring(0, item.Name.IndexOf('-'));

                    var FileName = item.Name.Substring(item.Name.IndexOf('-') + 1, (item.Name.Length - (item.Name.IndexOf('-') + 1)) - 4).Split(",");

                    if (!FileManager.PathExists(PathUpload))
                        FileManager.CreatePath(PathUpload);

                    var newUploadFile = $"{PathUpload}\\{item.Name}";

                    if (FileManager.FileExists(newUploadFile))
                        FileManager.RemoveFile(newUploadFile);

                    FileManager.CopyFile(item.FullName, newUploadFile);

                    var newFileName = $"{PathUpload}\\NF-{Invoice}{item.Extension}";

                    if (FileManager.FileExists(newFileName))
                        FileManager.RemoveFile(newFileName);

                    var FileToUpload = FileManager.FileMove(newUploadFile, newFileName);

                    foreach (var file in FileName)
                    {
                        var Upload = new UploadFile(AppSettings);
                        await Upload.PostAsync(file, FileToUpload);
                        await Task.Delay(5000, stoppingToken);
                    }

                    FileManager.RemoveFile(FileToUpload.FullName);
                    FileManager.RemoveFile(item.FullName);
                }

                await Task.Delay(AppSettings.Interval, stoppingToken);
            }
        }
        */
    }
}
