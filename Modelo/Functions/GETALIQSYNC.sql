-- OBTER A AL�QUOTA PARA UM DETERMINADO IMPOSTO
CREATE OR REPLACE FUNCTION GETALIQIPISYNC
(
  pCODCFGTRIB    IN  NUMBER,   -- C�DIGO DA CONFIGURA��O TRIBUT�RIA
  pCODIMPOSTO    IN  NUMBER,   -- CODIGO DO IMPOSTO  
  pCODESTAB      IN  NUMBER,   -- CODIGO DO ESTABELECIMENTO
  pDATA          IN  DATE,     -- DATA BASE PARA LOCALIZA��O DA AL�QUOTA
  pCODESTADOORIG IN  NUMBER,   -- CODIGO DO ESTADO DE ORIGEM DO ATO
  pCODESTADODEST IN  NUMBER,   -- CODIGO DO ESTADO DE DESTINO DO ATO
  pCODEMPRESA    IN  NUMBER,   -- CODIGO DA EMPRESA
  pCODPRODUTO    IN  VARCHAR2 -- CODIGO DO PRODUTO
) RETURN NUMBER               -- AL�QUOTA A SER UTILZADA
IS
nALIQCFGTRIB NUMBER;
nALIQSIMPLES NUMBER;
nALIQLEGTRIB NUMBER;
nCODREGTRIB NUMBER; 
cMSERROALIQ CLOB;  
NRET NUMBER;
BEGIN
  NRET := 0;
  BEGIN
    NRET := PCKG_FISCAL.GETALIQIMPOSTO 
    (
      pCODCFGTRIB, -- C�DIGO DA CONFIGURA��O TRIBUT�RIA
      pCODIMPOSTO,  -- CODIGO DO IMPOSTO  
      pCODESTAB,   -- CODIGO DO ESTABELECIMENTO
      pDATA,         -- DATA BASE PARA LOCALIZA��O DA AL�QUOTA
      pCODESTADOORIG,  -- CODIGO DO ESTADO DE ORIGEM DO ATO
      pCODESTADODEST,  -- CODIGO DO ESTADO DE DESTINO DO ATO      
      pCODEMPRESA,     -- CODIGO DA EMPRESA
      pCODPRODUTO, -- C�DIGO DO PRODUTO
      nALIQCFGTRIB,    -- AL�QUOTA - CONFIGURA��O TRIBUT�RIA
      nALIQSIMPLES,    -- AL�QUOTA - SIMPLES NACIONAL
      nALIQLEGTRIB,    -- AL�QUOTA - LEGISLA��O TRIBUT�RIA
      nCODREGTRIB,     -- REGIME TRIBUT�RIO DO ESTABELECIMENTO  
      cMSERROALIQ      -- MENSAGEM DE ERRO
    );
  EXCEPTION
    WHEN OTHERS THEN
      NRET := 0;
  END;
  RETURN NRET;
END;
