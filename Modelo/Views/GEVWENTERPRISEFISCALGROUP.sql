CREATE OR REPLACE FORCE VIEW GEVWENTERPRISEFISCALGROUP
(
  IDAPI,
  UNIQUEKEY,
  IDERP,
  DESCRIPTION,
  TYPE,
  REMICMSPISCOFINS
)
AS
(
    SELECT 0 IDAPI, NULL UNIQUEKEY, FSGRPFISEMP.CODGRPFISEMP IDERP,
           SUBSTR(FSGRPFISEMP.CODGRPFISEMP || ' - ' || FSGRPFISEMP.DESCRICAO, 0, 255) DESCRIPTION,
           FSGRPFISEMP.CLASSIFEMPRESA TYPE, FSGRPFISEMP.EXCICMSBASEPISCOF REMICMSPISCOFINS
      FROM FSGRPFISEMP
     WHERE FSGRPFISEMP.CLASSIFEMPRESA IN ('C', 'T', 'P', 'X')
);