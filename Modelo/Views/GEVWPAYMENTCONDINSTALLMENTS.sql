CREATE OR REPLACE FORCE VIEW GEVWPAYMENTCONDINSTALLMENTS
(
  IDAPI,
  UNIQUEKEY,
  CONDPAYID,
  PERCENT,
  DAYS
)
AS
(
    SELECT 0 IDAPI,
           '' UNIQUEKEY,
           VECONDPGTO.IDAPI CONDPAYID,
           VEDETCONDPGTO.PORCENT PERCENT,
           VEDETCONDPGTO.QTDEDIAS DAYS
      FROM VECONDPGTO,
           VEFORMAPGTO,
           VEDETCONDPGTO
     WHERE VEFORMAPGTO.CODFORMAPGTO  = VECONDPGTO.CODFORMAPGTO
       AND VEDETCONDPGTO.CODCONDPGTO = VECONDPGTO.CODCONDPGTO
       AND VEFORMAPGTO.IDAPI         > 0
       AND VECONDPGTO.IDAPI          > 0
);