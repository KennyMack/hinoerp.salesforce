﻿using Hino.Salesforce.Infra.DataBase.Context;
using Hino.Salesforce.Infra.DataBase.Repositories.Sales;
using System;
using System.Data.Entity;

namespace Test.console
{
    class Program
    {
        static void Main(string[] args)
        {
            using (var ctx = new AppDbContext())
            {
                // var OrderDB = ctx.VEOrders.Where(r =>
                //     r.Id == 37 &&
                //     r.EstablishmentKey == "e6f16b59-37c3-479d-b4fc-10c2afceb869" &&
                //     r.UniqueKey == "724c23f0-dcbd-4284-a134-5c1b766aa184"
                // ).AsNoTracking()
                //     .FirstOrDefault();

                var ve = new VEOrdersRepository(ctx);
                // 
                var OrderDB = ve.GetByIdToUpdate(37, "e6f16b59-37c3-479d-b4fc-10c2afceb869",
                    "724c23f0-dcbd-4284-a134-5c1b766aa184");
                OrderDB.GEEnterprises = null;

                OrderDB.CarrierID = 7492; // pOrder.CarrierID;
                OrderDB.GECarriers = null;

                OrderDB.GERedispatch = null;

                OrderDB.GEUsers = null;

                OrderDB.GEPaymentType = null;

                OrderDB.GEPaymentCondition = null;

                ctx.Entry(OrderDB).State = EntityState.Detached;
                ctx.Entry(OrderDB).State = EntityState.Modified;

                // ve.Update(OrderDB);
                ctx.SaveChanges();

            }

            Console.ReadLine();

            /*var json = "{\"atividade_principal\":[{\"text\":\"Fundição de ferro e aço\",\"code\":\"24.51-2-00\"}],\"data_situacao\":\"15/05/2004\",\"complemento\":\"KM 100 OESTE\",\"nome\":\"GRAMOLA FUNDICAO - EIRELI\",\"uf\":\"SP\",\"telefone\":\"(19) 3456-1887/ (19) 3456-0675\",\"email\":\"contato@controleconsultoria.com\",\"qsa\":[{\"qual\":\"65-Titular Pessoa Física Residente ou Domiciliado no Brasil\",\"nome\":\"GILDO DE OLIVEIRA\"}],\"situacao\":\"ATIVA\",\"bairro\":\"JARDIM NOVA LIMEIRA\",\"logradouro\":\"ROD ENGENHEIRO JOAO TOSELLO (LIMEIRA-MOGI MIRIM)\",\"numero\":\"S/N\",\"cep\":\"13.486-264\",\"municipio\":\"LIMEIRA\",\"porte\":\"DEMAIS\",\"abertura\":\"28/08/1992\",\"natureza_juridica\":\"230-5 - Empresa Individual de Responsabilidade Limitada (de Natureza Empresária)\",\"fantasia\":\"GRAMOLA FUNDICAO\",\"cnpj\":\"68.418.599/0001-11\",\"ultima_atualizacao\":\"2020-01-27T16:21:14.507Z\",\"status\":\"OK\",\"tipo\":\"MATRIZ\",\"efr\":\"\",\"motivo_situacao\":\"\",\"situacao_especial\":\"\",\"data_situacao_especial\":\"\",\"atividades_secundarias\":[{\"code\":\"00.00-0-00\",\"text\":\"Não informada\"}],\"capital_social\":\"93700.00\",\"extra\":{},\"billing\":{\"free\":true,\"database\":true}}";
            byte[] toBytes = Encoding.ASCII.GetBytes(json);

            using (var ctx = new AppDbContext())
            {
                using (var conn = new OracleConnection())
                {
                    conn.ConnectionString = ctx.Database.Connection.ConnectionString;
                    conn.Open();
                    using (Oracle.ManagedDataAccess.Client.OracleCommand cmd = new Oracle.ManagedDataAccess.Client.OracleCommand())
                    {
                        cmd.Connection = conn;
                        cmd.CommandText = "INSERT INTO PEDIDOINTEG(CODPEDIDO,TEXTJSON) VALUES (:pCODPEDIDO, :pTEXTJSON)";

                        OracleParameter param1 = cmd.Parameters.Add("pCODPEDIDO", OracleDbType.Int32);
                        param1.Direction = ParameterDirection.Input;
                        param1.Value = new Random().Next(1, 99);

                        OracleParameter param2 = cmd.Parameters.Add("pTEXTJSON", OracleDbType.Blob);
                        param2.Direction = ParameterDirection.Input;
                        param2.Value = toBytes;

                        cmd.ExecuteNonQuery();

                    }
                }
            }*/
        }

        //static void Main(string[] args)
        //{
        //    try
        //    {


        //        using (var ctx = new AppDbContext())
        //        {
        //            using (var veOrder = new GEEstablishmentsRepository(ctx))
        //            {
        //                var results = Task.Run(async () =>
        //                   await veOrder.QueryAsync(r => r.IsActive == true)).Result;



        //                //  IEnumerable<VEOrderItems> resultItems = results.Results.Select(r => r.VEOrderItems);
        //            }
        //        }
        //    }
        //    catch (Exception e)
        //    {

        //        Console.WriteLine(e.Message);
        //        Console.WriteLine(e.StackTrace);
        //        Console.WriteLine(e.Source);
        //        Console.WriteLine(e);
        //    }


        //    //Database.SetInitializer(new DropCreateDatabaseAlways<AppDbContext>());

        //    /*var EstablishmentKey = Guid.NewGuid().ToString().Substring(0, 32);

        //    using (var ctx = new AppDbContext())
        //    {
        //        using (var uow = new UnitOfWork(ctx))
        //        {
        //            var repoCountry = new GECountriesRepository(ctx);
        //            var repoStates = new GEStatesRepository(ctx);
        //            var repoCities = new GECitiesRepository(ctx);


        //            ctx.Database.Log = Console.Write;
        //            var country = new GECountries
        //            {
        //                EstablishmentKey = EstablishmentKey,
        //                UniqueKey = EstablishmentKey,
        //                Name = "BRASIL",
        //                Initials = "BRA",
        //                DDI = "55",
        //                BACEN = "01058"
        //            };

        //            repoCountry.Add(country);

        //           // var country = repoCountry.Query(r => r.Id == 10).FirstOrDefault();
        //            // ctx.SaveChanges();

        //            var state = new GEStates
        //            {
        //                EstablishmentKey = EstablishmentKey,
        //                UniqueKey = EstablishmentKey,
        //                Name = "SÃO PAULO",
        //                Initials = "SP",
        //                IBGE = "03",
        //                CountryID = country.Id
        //            };

        //            repoStates.Add(state);

        //            var city = new GECities
        //            {
        //                EstablishmentKey = EstablishmentKey,
        //                UniqueKey = EstablishmentKey,
        //                Name = "Limeira",
        //                IBGE = "025",
        //                StateID = state.Id
        //            };

        //            repoCities.Add(city);

        //            //ctx.SaveChanges();

        //            var ab = city.GEStates;

        //            var ss = country.GEStates;

        //            var r = repoCountry.GetAllPaged(1, 10);

        //            var s = repoCountry.Query( H => H.Initials == "BRA",
        //               O => O.GEEnterprises);

        //            uow.Commit();
        //        }
        //    }*/

        //    /*using (var cache = new SessionTokenCache(new Hino.Salesforce.Infra.Cross.Cache.RedisConfigurationSection()))
        //    {
        //        cache.Set("chave", "chave nova", new TimeSpan(0, 0, 1, 40, 0));
        //    }*/


        //    var str = Metis.CryptPass("12345678");

        //    var valido = Metis.ValidatePass("Validado", str);
        //    var validos = Metis.ValidatePass("Validados", str);

        //    Console.WriteLine(str);
        //    Console.ReadKey();


        //    /*using (var ctx = new AppDbContext())
        //    {
        //        var user = new GEUser
        //        {
        //            ControlToken = "ControlToken",
        //            ClientToken = "ClientToken",
        //            Created = DateTime.Now,
        //            Modified = DateTime.Now,
        //            IsActive = true,
        //            UserToken = "UserToken",
        //            Username = "UserToken",
        //            Email = "UserToken",
        //            Password = "UserToken",
        //            LastLogin = DateTime.Now,
        //            UserType = Hino.Salesforce.Domain.Base.Enums.EUserType.Sallesman
        //        };


        //        //ctx.GEUSERS.Add(user);

        //       // var rs = ((IQueryable<GELogin>)ctx.GELOGINS).Where(r=>r.Id == 2).ToList();
        //        //ctx.GELOGINS.Remove(rs.FirstOrDefault());

        //        var log = new GELogin
        //        {
        //            ControlToken = "ControlToken",
        //            ClientToken = "ClientToken",
        //            Created = DateTime.Now,
        //            Modified = DateTime.Now,
        //            IsActive = true,
        //            DateLogin = DateTime.Now
        //        };

        //        //ctx.GELOGINS.Add(log);


        //        ctx.SaveChanges();

        //    }*/
        //}
    }
}
